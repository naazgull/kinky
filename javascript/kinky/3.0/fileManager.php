<?php
$javascript = '';
if (count($_POST) != 0) {
	var_dump($_FILES); die;
}
if (count($_FILES) != 0) {
	foreach ($_FILES as $name => $props) {
		$fileNamePart = time() . $props['name'];
		$newName = $_GET['tmp'] . $fileNamePart;
		move_uploaded_file($props['tmp_name'], $newName);
		$javascript .= 'KFileManager.addFile(parentID, \'' . $props['name'] . '\', \'' . $newName . '\');';
	}
}

?>
<html>
<head>
<script type="text/javascript">
		KFileManager.prototype.constructor = KFileManager;
		KFileManager.maxFile = 1;
		KFileManager.prompt = true;

		function KFileManager(parentID) {
			this.parent = parentID;
		};

		KFileManager.prototype.draw = function() {
			var form = window.document.getElementById('file-' + this.parent + 'FileUploadForm');

			var file = window.document.createElement('input');
			file.name = 'addFile';
			file.type = 'file';
			file.id = 'file-' + this.parent;
			file.style.fontSize = '600%';
			file.style.cursor = 'pointer';
			file.style.position = 'absolute';
			file.style.right = '0px';
			file.style.top = '0px';

			form.appendChild(file);
			if (window.attachEvent) {
				file.attachEvent('onchange', KFileManager.uploadFile);
			}
			else {
				file.addEventListener('change', KFileManager.uploadFile, false);
			}

			window.document.body.appendChild(form);

		};

		KFileManager.counter = 0;
		KFileManager.uploadFile = function(event) {
			var parent = window.parent.document.getElementById(parentID);
			var files = parent.getElementsByTagName('input');
			if (files.length == KFileManager.maxFile) {
				if (KFileManager.maxFile == 1) {
					if (KFileManager.prompt && !confirm('Deseja substituir o ficheiro introduzido?')) {
						return;
					}
					else {
						files[0].parentNode.parentNode.removeChild(files[0].parentNode);
					}
				}
				else {
					alert('O n\u00famero m\u00e1ximo de ficheiros \u00e9 ' + KFileManager.maxFile + '.');
				}
			}

			window.document.getElementById('file-' + parentID + 'FileUploadForm').submit();
			return false;
		};

		KFileManager.fireEvent = function(element, eventName) {
			if (window.document.createEventObject) {
				var evt = window.document.createEventObject();
				return element.fireEvent('on' + eventName, evt);
			}
			else {
				var evt = document.createEvent("HTMLEvents");
				evt.initEvent(eventName, true, true);
				return !element.dispatchEvent(evt);
			}
		};

		KFileManager.addFile = function(parentID, fileName, filePath) {
			var parent = window.parent.document.getElementById('container' + parentID);
			for (var idx = 0; idx != parent.childNodes.length; idx++) {
				if (parent.childNodes[idx].className.indexOf("KFileDefaultText") != -1) {
					parent.removeChild(parent.childNodes[idx]);
					break;
				}
			}

			var newFileContainer = window.document.createElement('div');

			var newFileLabel = window.document.createElement('label');
			newFileLabel.appendChild(window.document.createTextNode(fileName));
			newFileContainer.appendChild(newFileLabel);

			var newFile = window.document.createElement('input');
			newFile.type = 'hidden';
			newFile.name = parentID + '[]';
			newFile.id = parentID + '_' + KFileManager.counter;
			newFile.value = filePath;
			newFileContainer.appendChild(newFile);

			var newFileRemove = window.document.createElement('button');
			newFileRemove.setAttribute('type', 'button');
			newFileRemove.appendChild(window.document.createTextNode('x'));
			newFileContainer.appendChild(newFileRemove);

			if (window.attachEvent) {
				newFileRemove.attachEvent('onclick', function(event) {
					var target = null;
					event = (event == null ? window.event : event);
					if (event.target) {
						target = event.target;
					}
					else if (event.srcElement) {
						target = event.srcElement;
					}
					target.parentNode.parentNode.removeChild(target.parentNode);

					var element = parent.parentNode.parentNode;
					var eventName = 'change';
					var evt = window.document.createEventObject();
					return element.fireEvent('on' + eventName, evt);
				});
			}
			else {
				newFileRemove.addEventListener('click', function(event) {
					var target = null;
					event = (event == null ? window.event : event);
					if (event.target) {
						target = event.target;
					}
					else if (event.srcElement) {
						target = event.srcElement;
					}
					target.parentNode.parentNode.removeChild(target.parentNode);

					var element = parent.parentNode.parentNode;
					var eventName = 'change';
					var evt = document.createEvent("HTMLEvents");
					evt.initEvent(eventName, true, true);
					element.dispatchEvent(evt);

				}, false);
			}

			newFileContainer.appendChild(window.document.createElement('br'));
			parent.appendChild(newFileContainer);
			KFileManager.fireEvent(window.parent.document.getElementById(parentID + '_button'), 'click');
			KFileManager.counter++;
		};
	</script>
</head>
	<body marginheight="0" marginwidth="0">
		<form method="post" enctype="multipart/form-data" id="file-<?=$_GET['id']?>FileUploadForm">
		</form>

		<script type="text/javascript">
			var parentID = '<?=$_GET["id"]?>';
			var maxFile = <?=$_GET["maxFile"]?>;
			var prompt = <?=$_GET["prompt"]?>;

			var fileManager = new KFileManager(parentID);
			KFileManager.maxFile = maxFile;
			KFileManager.prompt = prompt;
			KFileManager.isLeaf = <?=array_key_exists('isLeaf', $_GET) ? $_GET['isLeaf'] : 'true'?>;
			fileManager.draw();

			<?=$javascript?>
		</script>

	</body>
</html>
