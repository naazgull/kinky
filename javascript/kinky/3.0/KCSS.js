function KCSS() {
}
{
	KCSS.prototype.constructor = KCSS;
	KCSS.styles = new Object();
	
	KCSS.getCSS = function(regex) {
		if (typeof regex == 'string') {
			regex = new RegExp(regex);
		}
		for ( var k = window.document.styleSheets.length - 1; k != -1; k--) {
			var rules = window.document.styleSheets.item(k);
			try {
				for ( var r = 0; r != rules.cssRules.length; r++) {
					var rule = rules.cssRules[r];
					if (regex.test(rule.selectorText)) {
						return rule;
					}
				}
			}
			catch (e) {
			}
		}
	};
	
	KCSS.clearBoth = function(noIE) {
		var toReturn = window.document.createElement('div');
		if (!noIE) {
			toReturn.className = 'ClearBothIe';
		}
		toReturn.style.clear = 'both';
		return toReturn;
	};
	
	KCSS.br = function() {
		var toReturn = window.document.createElement('br');
		return toReturn;
	};
	
	KCSS.img = function(src, alt, title) {
		var toReturn = window.document.createElement('img');
		toReturn.src = src;
		if (alt) {
			toReturn.alt = alt;
		}
		if (title) {
			toReturn.title = title;
		}
		return toReturn;
	};
	
	KCSS.getComputedStyle = function(domElement) {
		var computedStyle;
		if (typeof domElement.currentStyle != 'undefined') {
			computedStyle = domElement.currentStyle;
		}
		else {
			computedStyle = document.defaultView.getComputedStyle(domElement, null);
		}
		
		return computedStyle;
	};
	
	KCSS.addCSSClass = function(cssClass, target) {
		target.className += ' ' + cssClass + ' ';
	};
	
	KCSS.removeCSSClass = function(cssClass, target) {
		var regex = new RegExp(' ' + cssClass + ' ', 'g');
		target.className = target.className.replace(regex, '');
	};
	
	KCSS.setStyle = function(cssStyle, domElements) {
		
		for ( var index in domElements) {
			var domElement = domElements[index];
			for ( var property in cssStyle) {
				if (cssStyle[property] == null) {
					continue;
				}
				switch (property) {
					case 'clear': {
						domElement.appendChild(KCSS.clearBoth());
						break;
					}
					case 'cssFloat': {
						if (KBrowserDetect.browser == 2) {
							domElement.style.cssFloat = '\'' + cssStyle[property] + '\'';
						}
						else {
							domElement.style.cssFloat = '\'' + cssStyle[property] + '\'';
						}
						break;
					}
					case 'opacity': {
						switch (KBrowserDetect.browser) {
							case 1:
								domElement.style.MozOpacity = cssStyle[property];
								domElement.style.opacity = cssStyle[property];
								break;
							case 2:
								if (!KCSS.NO_IE_OPACITY) {
									domElement.style.MsFilter = '"progid:DXImageTransform.Microsoft.Alpha(Opacity=' + Math.round(cssStyle[property] * 100) + ')"';
									domElement.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + Math.round(cssStyle[property] * 100) + ')';
								}
								domElement.style.opacity = cssStyle[property];
								break;
							case 3:
								domElement.style.opacity = cssStyle[property];
								break;
							case 4:
								domElement.style.KhtmlOpacity = cssStyle[property];
								domElement.style.opacity = cssStyle[property];
								break;
							case 5:
								domElement.style.opacity = cssStyle[property];
								break;
						}
						break;
					}
					default: {
						try {
							eval('domElement.style.' + property + ' = \'' + cssStyle[property] + '\'');
						}
						catch (err) {
							if (Kinky.DEV) {
								alert(property + "=" + cssStyle[property] + " " + err.toString());
							}
						}
						break;
					}
				}
			}
		}
	};
	
	KCSS.normalizBackgroundPosition = function(value) {
		var splited = value.split(/ /);
		
		return {
			x : KDOM.normalizePixelValue(splited[0]),
			y : KDOM.normalizePixelValue(splited[1])
		};
	};
	
	KCSS.NO_IE_OPACITY = false;
	
	KSystem.included('KCSS');
}