function KConnectionHTTP(url, dataURL, headers) {
	if (url && dataURL) {
		if (url.charAt(url.length - 1) == '/') {
			url = url.substring(0, url.length - 1);
		}
		KConnection.call(this, url, dataURL, headers);
	}
}
{
	KConnectionsPool.registerConnector('https', 'KConnectionHTTP');
	KConnectionsPool.registerConnector('http', 'KConnectionHTTP');
	
	KConnectionHTTP.prototype = new KConnection();
	KConnectionHTTP.prototype.constructor = KConnectionHTTP;
	
	KConnectionHTTP.prototype.connect = function(request, callback) {
		var channel = this.openChannel();
		return channel;
	};
	
	KConnectionHTTP.prototype.getServiceParams = function(request, params, channel) {
		var ret = '?';
		for ( var paramName in params) {
			if (ret.length != 1) {
				ret += '&';
			}
			ret += paramName + '=' + encodeURIComponent(params[paramName]);
		}
		if (ret.length != 1) {
			ret += '&';
		}

		var req = {
			channel : channel,
			request : request
		};
		var index = KConnectionHTTP.stack.push(req) - 1;
		req.receive = function(data) {
			eval(request.callback[0] + '(data, request)' );
			channel.parentNode.removeChild(channel);
			KConnectionHTTP.stack.splice(index, 1);
			var widget = Kinky.getWidget(request.id);
			if (widget && widget.resume) {
				widget.resume(true);
			}
		};

		return ret + 'callback=' + encodeURIComponent('KConnectionHTTP.stack[' + index + '].receive');
	}; 
	
	KConnectionHTTP.prototype.send = function(request, params, headers) {
		params = params || {};

		var channel = this.connect(request);
		channel.charset = 'utf-8';

		var src = request.type + ':' + request.service + this.getServiceParams(request, params, channel);
		channel.src = src;
		
		window.document.getElementsByTagName('head')[0].appendChild(channel);
	};
	
	KConnectionHTTP.prototype.openChannel = function() {
		return window.document.createElement('script');
	};
	
	KConnectionHTTP.stack = [];

	KSystem.included('KConnectionHTTP');
}