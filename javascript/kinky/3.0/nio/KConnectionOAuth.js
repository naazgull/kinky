function KConnectionOAuth(url, dataURL, headers) {
	if (url && dataURL) {
		if (url.charAt(url.length - 1) == '/') {
			url = url.substring(0, url.length - 1);
		}
		KConnectionREST.call(this, url, dataURL, headers);
	}
}
KSystem.include([
	'KConnectionREST'
], function() {
	KConnectionsPool.registerConnector('oauth', 'KConnectionOAuth');
	
	KConnectionOAuth.prototype = new KConnectionREST();
	KConnectionOAuth.prototype.constructor = KConnectionOAuth;
	
	KSystem.included('KConnectionOAuth');
});