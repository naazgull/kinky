function KConnectionPHP(url, dataURL, headers) {
	if (url && dataURL) {
		KConnection.call(this, url, dataURL, headers);
	}
}

{
	KConnectionPHP.prototype = new KConnection();
	KConnectionPHP.prototype.constructor = KConnectionPHP;
	
	KConnectionsPool.registerConnector('php', 'KConnectionPHP');
	
	KConnectionPHP.prototype.send = function(request, params, headers) {
		var channel = this.connect(request);
		var method = 'POST';
		var url = this.urlBase;
		var content = this.getServiceParams(request, params);
		var setHeader = true;
		
		channel.open(method, url, true);
		if (setHeader) {
			channel.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		}
		this.setHeaders(headers, channel);
		channel.send(content);
	};
	
	KSystem.included('KConnectionPHP');
}
