function KConnectionREST(url, dataURL, headers) {
	if (url && dataURL) {
		if (url.charAt(url.length - 1) == '/') {
			url = url.substring(0, url.length - 1);
		}
		KConnection.call(this, url, dataURL, headers);
	}
}
{
	KConnectionsPool.registerConnector('rest', 'KConnectionREST');
	
	KConnectionREST.prototype = new KConnection();
	KConnectionREST.prototype.constructor = KConnectionREST;
	
	KConnectionREST.prototype.setURL = function(url) {
		if (url.charAt(url.length - 1) == '/') {
			url = url.substring(0, url.length - 1);
		}
		KConnection.prototype.setURL.call(this, url);
	};
	
	KConnectionREST.prototype.getServiceParams = function(request, params) {
		var toSend = '';
		if (params) {
			for ( var paramName in params) {
				if (/format/.test(paramName)) {
					continue;
				}
				if (toSend.length != 0) {
					toSend += '&';
				}
				else {
					toSend += '?';
				}
				toSend += paramName + '=' + encodeURIComponent(params[paramName]);
			}
		}
		return toSend;
	};
	
	KConnectionREST.prototype.send = function(request, params, headers) {
		var channel = this.connect(request);
		var content = null;
		var url = this.urlBase + request.service;
		var method = request.action.toUpperCase();
		

		switch (request.action) {
			case 'head':
			case 'get': {
				url += this.getServiceParams(null, params);
				break;
			}
			default: {
				content = JSON.stringify(params);
				break;
			}
		}
		
		channel.open(method, url, true);
		channel.setRequestHeader('Accept', 'application/json');
		if (content) {
			channel.setRequestHeader('Content-Type', 'application/json');
		}
		this.setHeaders(headers, channel);
		channel.send(content);
	};
	
	KSystem.included('KConnectionREST');
}