function KConnectionJSON(url, dataURL, headers) {
	if (url && dataURL) {
		KConnection.call(this, url, dataURL, headers);
	}
}

{
	KConnectionsPool.registerConnector('js', 'KConnectionJSON');
	
	KConnectionJSON.prototype = new KConnection();
	KConnectionJSON.prototype.constructor = KConnectionJSON;
	
	KConnectionJSON.prototype.send = function(request, params, headers) {
		var channel = this.connect(request);
		var content = null;
		var url = null;
		var method = 'GET';
		
		var queryString = (request.service.indexOf('?') == -1 ? '' : request.service.substr(request.service.indexOf('?')));
		var file = (request.service.indexOf('?') == -1 ? request.service : request.service.substr(0, request.service.indexOf('?')));
		url = this.dataUrlBase + (request.namespace != ':' ? request.namespace + '/' : '') + file + KConnectionJSON.processPagination(queryString) + KConnectionJSON.JSON_FILE_EXTENSION + queryString;
		
		channel.open(method, url, true);
		this.setHeaders(headers, channel);
		channel.send(content);
	};
	
	KConnectionJSON.processPagination = function(query) {
		if ((query.indexOf('pageSize') != -1) && (query.indexOf('pageStartIndex') != -1)) {
			var pageSize = 0;
			var pageStartIndex = 0;
			var parts = query.substr(1).split('&');
			for ( var p in parts) {
				if (parts[p].indexOf('pageSize') != -1) {
					var vals = parts[p].split('=');
					pageSize = parseInt(vals[1]);
				}
				else if (parts[p].indexOf('pageStartIndex') != -1) {
					var vals = parts[p].split('=');
					pageStartIndex = parseInt(vals[1]);
				}
			}
			
			return Math.round(pageStartIndex / pageSize);
		}
		return '';
	};
	
	KConnectionJSON.JSON_FILE_EXTENSION = '.json';
	
	KSystem.included('KConnectionJSON');
}
