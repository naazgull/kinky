function KConnectionJSONP(url, dataURL) {
	this.urlBase = url;
	this.dataUrlBase = dataURL;
	this.requestCount = 0;
	this.channels = {};
}

{
	
	KConnectionJSONP.prototype = new KConnection();
	KConnectionJSONP.prototype.constructor = KConnectionJSONP;
	
	KConnectionsPool.registerConnector('jsonp', 'KConnectionJSONP');
	
	KConnectionJSONP.prototype.connect = function(request, callback) {
		var channel = this.openChannel();
		return channel;
	};
	
	KConnectionJSONP.prototype.getServiceParams = function(request, params) {
		if (request) {
			if (params) {
				for ( var paramName in params) {
					request[paramName] = params[paramName];
				}
			}
			params.requestID = this.requestCount++;
			return 'id=' + params.requestID + '&request=' + encodeURIComponent(Base64.encode(JSON.stringify(request))) + '&callback=KConnection.getConnection(\'jsonp\').receive(' + params.requestID + ', $$)';
			
		}
		return null;
	};
	
	KConnectionJSONP.prototype.send = function(request, params, headers) {
		params = params || {};
		var accessToken = (!!headers && !!headers.access_token ? headers.access_token : KOAuth.getAccessToken());
		var src = this.urlBase + '?' + this.getServiceParams(request, params) + (!!accessToken ? '&access_token=' + accessToken : '');
		this.channels[params.requestID] = this.connect(request);
		this.channels[params.requestID].charset = 'utf-8';
		this.channels[params.requestID].src = src;
		
		window.document.getElementsByTagName('head')[0].appendChild(this.channels[params.requestID]);
	};
	
	KConnectionJSONP.prototype.openChannel = function() {
		return window.document.createElement('script');
	};
	
	KConnectionJSONP.prototype.receive = function(requestID, response) {
		var widget = Kinky.getWidget(response.id);
		
		if (response.httpStatus && (response.httpStatus != 200)) {
			this.processNonOk(widget, response);
			return;
		}
		
		Kinky.bunnyMan.loggedUser = response.user || Kinky.bunnyMan.loggedUser;
		if (widget != null) {
			if (widget.resume) {
				widget.resume(true);
			}
		}
		
		eval(response.callback + '(response, { href : this.channels[requestID].src });');
		
		window.document.getElementsByTagName('head')[0].removeChild(this.channels[requestID]);
		delete this.channels[requestID];
	};
	
	KSystem.included('KConnectionJSONP');
}
