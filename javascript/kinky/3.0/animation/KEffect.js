function KEffect(widget, tween, element) {
	if (widget) {
		this.widget = widget;
		this.type = tween.type;
		this.tween = tween;
		this.tween.delay = this.tween.delay == null ? 0 : this.tween.delay;
		this.stop = false;
		this.element = element;
		this.id = null;
		this.timerCode = function() {
		};
	}
}

{
	KEffect.prototype.constructor = KEffect;
	
	KEffect.prototype.config = function() {
	};
	
	KEffect.prototype.go = function() {
	};
	
	KEffect.prototype.visible = function() {
		this.widget.visible();
	};
	
	KEffect.getEventEffect = function(event) {
		event = event || window.event;
		if (event == null) {
			return null;
		}
		var element = event.target || event.srcElement;
		var effect = null;
		while ((effect = KEffects.getEffectByElement(element)) == null) {
			element = element.parentNode;
			if (element == null) {
				break;
			}
		}
		
		return effect;
	};
	
	KEffect.getEffect = function(domElement) {
		var element = domElement;
		var effect = null;
		while ((effect = KEffects.getEffectByElement(element)) == null) {
			element = element.parentNode;
			if (element == null) {
				break;
			}
		}
		
		return effect;
	};
	
	KEffect.prototype.cancel = function(noOnComplete) {
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var id = this.id;
			KCSS.setStyle({
				'MozTransition' : 'none',
				'WebkitTransition' : 'none',
				'OTransition' : 'none'
			}, [
				KEffects.effecting[id].tween.target
			]);
		}
		
		this.stop = true;
		this.cancel = function() {
			return;
		};
		
		var tween = this.tween;
		var widget = this.widget;
		KEffects.stopEffect(this);
		if (!noOnComplete && (tween.onComplete != null)) {
			tween.onComplete(widget, tween);
		}
		delete tween;
	};
	
	KSystem.included('KEffect');
}