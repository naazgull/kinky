function KEffectFontResize(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectFontResize.prototype = new KEffect();
	KEffectFontResize.prototype.constructor = KEffectFontResize;
	
	KEffects.installEffect('font-resize', 'KEffectFontResize');
	
	KEffectFontResize.prototype.config = function() {
		var id = this.id;
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		if (!!this.tween.from && !!this.tween.from.fontSize) {
			this.widget.setStyle({
				fontSize : '' + this.tween.from.fontSize
			}, this.tween.target);
			this.tween.fontSize = this.tween.from.fontSize;
		}
		
		this.visible();
		
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowResize();
			}
		};
		
		this.tween.unit = this.tween.unit || 'em';
		
		this.tween.original = {
			overflow : this.tween.target.style.overflow,
			position : this.tween.target.style.position
		};
		
		this.tween.fontSize = parseInt(this.tween.target.style.fontSize.replace(/px|pt|em/, ''));
		if (isNaN(this.tween.fontSize) || (this.tween.fontSize == null)) {
			this.tween.fontSize = 8;
		}
		
	};
	
	KEffectFontResize.prototype.go = function() {
		var id = this.id;
		
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var style = {
				'MozTransitionDuration' : this.tween.duration + 'ms',
				'WebkitTransitionDuration' : this.tween.duration + 'ms',
				'OTransitionDuration' : this.tween.duration + 'ms',
				'MozTransitionTimingFunction' : this.tween.f,
				'WebkitTransitionTimingFunction' : this.tween.f,
				'MozTransitionProperty' : 'font-size',
				'WebkitTransitionProperty' : 'font-size',
				'OTransitionProperty' : 'font-size'
			};
			
			style.fontSize = this.tween.go.fontSize + this.tween.unit;
			KCSS.setStyle(style, [
				this.tween.target
			]);
			
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KEffects.effecting[id].cancel();
				}
			}, this.tween.duration + 20);
			return;
		}
		
		if (!Kinky.ALLOW_EFFECTS) {
			this.tween.target.style.fontSize = this.tween.go.fontSize + this.tween.unit;
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				fontSize : this.tween.fontSize
			},
			go : {
				fontSize : this.tween.go.fontSize
			},
			from : {
				fontSize : this.tween.fontSize
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectFontResize.prototype.slowResize = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var fontSize = this.fValue.value.fontSize;
		
		this.tween.fontSize = fontSize;
		this.tween.target.style.fontSize = this.tween.fontSize + this.tween.unit;
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectFontResize');
});