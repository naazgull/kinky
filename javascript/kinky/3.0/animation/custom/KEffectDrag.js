function KEffectDrag(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectDrag.prototype = new KEffect();
	KEffectDrag.prototype.constructor = KEffectDrag;
	
	KEffects.installEffect('drag', 'KEffectDrag');
	
	KEffectDrag.prototype.config = function() {
		this.widget.drag = this;
		this.tween.target = this.tween.applyToElement ? this.tween.target : this.widget.panel;
		if (this.tween.dragndrop) {
			KDOM.addEventListener(this.element, 'mousedown', function(event) {
				var mouseEvent = KDOM.getEvent(event);
				
				if (mouseEvent.preventDefault) {
					mouseEvent.preventDefault();
				}
				else {
					mouseEvent.returnValue = false;
				}
				if (mouseEvent.button == 2) {
					return;
				}

				var effect = KDOM.getEventWidget(event).drag;
				if (!effect) {
					return;
				}
				var drag = effect.tween;
				var element = KDOM.getEventTarget(event);
				drag.stillDown = true;
				KDOM.addEventListener(element, 'mouseup', KEffectDrag.timeOutDown);
				
				KSystem.addTimer(function() {
					if (drag.stillDown) {
						KDOM.removeEventListener(element, 'mouseup', KEffectDrag.timeOutDown);
						KEffectDrag.startDrag(event);
					}
				}, 200);
			});
		}
		else {
			KDOM.addEventListener(this.element, 'mousedown', KEffectDrag.startDrag);
		}
		
	};
	
	KEffectDrag.prototype.go = function() {
	};
	
	KEffectDrag.timeOutDown = function(event) {
		var effect = KDOM.getEventWidget(event).drag;
		var drag = effect.tween;
		drag.stillDown = false;
	};
	
	KEffectDrag.startDrag = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var effect = KDOM.getEventWidget(event).drag;
		var drag = effect.tween;
		KEffectDrag.dragging = effect;
		
		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		if (mouseEvent.button == 2) {
			return;
		}

		drag.x = (mouseEvent.pageX ? mouseEvent.pageX : (window.document.documentElement ? mouseEvent.clientX + window.document.documentElement.scrollLeft : mouseEvent.clientX + window.document.body.scrollLeft));
		drag.y = (mouseEvent.pageY ? mouseEvent.pageY : (window.document.documentElement ? mouseEvent.clientY + window.document.documentElement.scrollTop : mouseEvent.clientY + window.document.body.scrollTop));
		
		if (drag.onStart != null) {
			drag.onStart(effect.widget, drag);
		}
		
		drag.animating = true;
		drag.original = {
			x : KDOM.normalizePixelValue(drag.target.style.left),
			y : KDOM.normalizePixelValue(drag.target.style.top),
			left : KDOM.normalizePixelValue(drag.target.style.left),
			top : KDOM.normalizePixelValue(drag.target.style.top)
		};
		
		var left = KDOM.normalizePixelValue(drag.target.style.left);
		var top = KDOM.normalizePixelValue(drag.target.style.top);
		if (drag.dragndrop) {
			left = effect.widget.offsetLeft();
			top = effect.widget.offsetTop();

			drag.original.target = drag.target;
			if (!!effect.widget.prepareDrag) {
				drag.target = effect.widget.prepareDrag();
			}
			else {
				drag.target = drag.target.cloneNode(true);
			}
			drag.target.style.position = 'fixed';
			drag.target.style.left = drag.x + 'px';
			drag.target.style.top = drag.y + 'px';
			drag.target.id = null;
			effect.widget.getShell().panel.appendChild(drag.target);
		}
		
		if (isNaN(left) && (!!drag.from && !!drag.from.x != null)) {
			drag.target.style.left = drag.from.x + 'px';
			left = drag.from.x;
		}
		if (isNaN(top) && (!!drag.from && drag.from.y != null)) {
			drag.target.style.top = drag.from.y + 'px';
			top = drag.from.y;
		}
		
		drag.left = left;
		drag.top = top;
		
		KDOM.addEventListener(window.document, 'mouseup', KEffectDrag.stopDrag);
		KDOM.addEventListener(window.document, 'mousemove', KEffectDrag.drag);
	};
	
	KEffectDrag.drag = function(event) {
		var effect = KEffectDrag.dragging;
		var mouseEvent = KDOM.getEvent(event);
		
		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		var drag = effect.tween;
		
		var y = null;
		var x = null;
		if (!drag.lock || !drag.lock.y) {
			y = (mouseEvent.pageY ? mouseEvent.pageY : (window.document.documentElement ? mouseEvent.clientY + window.document.documentElement.scrollTop : mouseEvent.clientY + window.document.body.scrollTop));
			var top = (parseInt(drag.target.style.top.replace(/px/, '')) + (y - drag.y));
			if (drag.rectangle) {
				if (top < drag.rectangle[0]) {
					top = drag.rectangle[0];
				}
				else if (top > drag.rectangle[2]) {
					top = drag.rectangle[2];
				}
			}
			
			drag.top = top;
			drag.target.style.top = drag.top + 'px';
		}
		if (!drag.lock || !drag.lock.x) {
			x = (mouseEvent.pageX ? mouseEvent.pageX : (window.document.documentElement ? mouseEvent.clientX + window.document.documentElement.scrollLeft : mouseEvent.clientX + window.document.body.scrollLeft));
			var left = (parseInt(drag.target.style.left.replace(/px/, '')) + (x - drag.x));
			if (drag.rectangle) {
				if (left < drag.rectangle[1]) {
					left = drag.rectangle[1];
				}
				else if (left > drag.rectangle[3]) {
					left = drag.rectangle[3];
				}
			}
			drag.left = left;
			drag.target.style.left = drag.left + 'px';
		}
		
		drag.x = x;
		drag.y = y;
		if (drag.onAnimate != null) {
			KSystem.addTimer(function() {
				drag.onAnimate(effect.widget, drag);
			}, 1);
		}
	};
	
	KEffectDrag.stopDrag = function(event) {
		var effect = KEffectDrag.dragging;
		var drag = effect.tween;
		
		if ((drag == null) || !drag.animating) {
			return;
		}
		
		KDOM.removeEventListener(window.document, 'mouseup', KEffectDrag.stopDrag);
		KDOM.removeEventListener(window.document, 'mousemove', KEffectDrag.drag);
		drag.animating = false;
		KEffectDrag.dragging = null;
		
		if (drag.onComplete != null) {
			drag.onComplete(effect.widget, drag);
		}
		if (drag.dragndrop) {
			drag.target.parentNode.removeChild(drag.target);
			drag.target = drag.original.target;
		}
	};
	
	KSystem.included('KEffectDrag');
});