function KEffectBackgroundMove(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectBackgroundMove.prototype = new KEffect();
	KEffectBackgroundMove.prototype.constructor = KEffectBackgroundMove;
	
	KEffects.installEffect('background-move', 'KEffectBackgroundMove');
	
	KEffectBackgroundMove.prototype.config = function() {
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		var id = this.id;
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowMove();
			}
		};
		
		this.tween.unit = this.tween.unit || 'px';
		
		if (!!this.tween.from && !!this.tween.from.y) {
			this.tween.y = this.tween.from.y;
		}
		else {
			this.tween.y = parseInt(KDOM.normalizePixelValue(this.tween.target.style.backgroundPosition.split(' ')[1]));
			if (isNaN(this.tween.y)) {
				this.tween.y = 0;
			}
		}
		
		if (!!this.tween.from && !!this.tween.from.x) {
			this.tween.x = this.tween.from.x;
		}
		else {
			this.tween.x = parseInt(KDOM.normalizePixelValue(this.tween.target.style.backgroundPosition.split(' ')[0]));
			if (isNaN(this.tween.x)) {
				this.tween.x = 0;
			}
		}
		
		this.tween.target.style.backgroundPosition = this.tween.x + 'px ' + this.tween.y + 'px';
		
		this.visible();
	};
	
	KEffectBackgroundMove.prototype.go = function() {
		if (!Kinky.ALLOW_EFFECTS) {
			this.tween.target.style.backgroundPosition = this.tween.go.x + 'px ' + this.tween.go.y + 'px';
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				x : this.tween.x,
				y : this.tween.y
			},
			go : {
				x : this.tween.go.x || 0,
				y : this.tween.go.y || 0
			},
			from : {
				x : this.tween.x,
				y : this.tween.y
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectBackgroundMove.prototype.slowMove = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var x = this.fValue.value.x = Math.round(this.fValue.value.x);
		var y = this.fValue.value.y = Math.round(this.fValue.value.y);
		
		if (!this.tween.lock || !this.tween.lock.y) {
			this.tween.y = y;
		}
		if (!this.tween.lock || !this.tween.lock.x) {
			this.tween.x = x;
		}
		
		this.tween.target.style.backgroundPosition = this.tween.x + 'px ' + this.tween.y + 'px';
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectBackgroundMove');
});