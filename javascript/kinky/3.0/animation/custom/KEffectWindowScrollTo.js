function KEffectWindowScrollTo(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectWindowScrollTo.prototype = new KEffect();
	KEffectWindowScrollTo.prototype.constructor = KEffectWindowScrollTo;
	
	KEffects.installEffect('window-scroll-to', 'KEffectWindowScrollTo');
	
	KEffectWindowScrollTo.prototype.config = function() {
		var id = this.id;
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowScrollTo();
			}
		};
		
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		if (!!this.tween.from && !!this.tween.from.x) {
			this.tween.x = this.tween.from.x;
		}
		if (!!this.tween.from && !!this.tween.from.y) {
			this.tween.y = this.tween.from.y;
		}
		
		if (this.tween.applyToElement) {
			this.tween.x = this.element.scrollLeft;
			this.tween.y = this.element.scrollTop;
		}
		else {
			this.tween.x = window.pageXOffset || window.scrollLeft || document.body.scrollLeft || 0;
			this.tween.y = window.pageYOffset || window.scrollTop || document.body.scrollTop || 0;
		}
	};
	
	KEffectWindowScrollTo.prototype.go = function() {
		if (!Kinky.ALLOW_EFFECTS) {
			window.scrollTo(this.tween.go.x || 0, this.tween.go.y || 0);
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				x : this.tween.x,
				y : this.tween.y
			},
			go : {
				x : this.tween.go.x,
				y : this.tween.go.y
			},
			from : {
				x : this.tween.x,
				y : this.tween.y
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectWindowScrollTo.prototype.slowScrollTo = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var x = this.fValue.value.x = Math.round(this.fValue.value.x);
		var y = this.fValue.value.y = Math.round(this.fValue.value.y);
		
		if (this.tween.applyToElement) {
			this.element.scrollTop = y;
			this.element.scrollLeft = x;
		}
		else {
			window.scrollTo(x || 0, y || 0);
		}
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectWindowScrollTo');
});