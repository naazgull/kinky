function KEffectMove(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectMove.prototype = new KEffect();
	KEffectMove.prototype.constructor = KEffectMove;
	
	KEffects.installEffect('move', 'KEffectMove');
	
	KEffectMove.prototype.config = function() {
		var id = this.id;
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		this.yAxis = this.tween.gravity && this.tween.gravity.y ? this.tween.gravity.y : 'top';
		this.xAxis = this.tween.gravity && this.tween.gravity.x ? this.tween.gravity.x : 'left';
		
		this.timerCode = function() {
			if (!!KEffects.effecting[id] && !KEffects.effecting[id].stop) {
				KEffects.effecting[id].slowMove();
			}
		};
		
		this.tween.unit = this.tween.unit || 'px';
		
		if (!!this.tween.from && !!this.tween.from.x) {
			eval('this.tween.target.style.' + this.xAxis + ' = \'' + this.tween.from.x + '' + this.tween.unit + '\';');
			this.tween.x = this.tween.from.x;
		}
		else {
			eval('this.tween.x = parseInt(KDOM.normalizePixelValue(this.tween.target.style.' + this.xAxis + '));');
			if (isNaN(this.tween.x)) {
				this.tween.x = 0;
				if (!this.tween.lock || !this.tween.lock.x) {
					eval('this.tween.target.style.' + this.xAxis + ' = \'0\';');
				}
			}
		}
		
		if (!!this.tween.from && !!this.tween.from.y) {
			eval('this.tween.target.style.' + this.yAxis + ' = \'' + this.tween.from.y + '' + this.tween.unit + '\';');
			this.tween.y = this.tween.from.y;
		}
		else {
			eval('this.tween.y = parseInt(KDOM.normalizePixelValue(this.tween.target.style.' + this.yAxis + '));');
			if (isNaN(this.tween.y)) {
				this.tween.y = 0;
				if (!this.tween.lock || !this.tween.lock.y) {
					eval('this.tween.target.style.' + this.yAxis + ' = \'0\';');
				}
			}
		}
		
		this.visible();
		
	};
	
	KEffectMove.prototype.go = function() {
		var id = this.id;
		
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var style = {
				'MozTransitionDuration' : this.tween.duration + 'ms',
				'WebkitTransitionDuration' : this.tween.duration + 'ms',
				'OTransitionDuration' : this.tween.duration + 'ms',
				'MozTransitionTimingFunction' : this.tween.f,
				'WebkitTransitionTimingFunction' : this.tween.f,
				'MozTransitionProperty' : this.yAxis + ',' + this.xAxis,
				'WebkitTransitionProperty' : this.yAxis + ',' + this.xAxis,
				'OTransitionProperty' : this.yAxis + ',' + this.xAxis
			};
			
			if (!this.tween.lock || !this.tween.lock.x) {
				eval('this.tween.target.style.' + this.yAxis + ' = this.tween.go.y + \'px\';');
			}
			
			if (!this.tween.lock || !this.tween.lock.y) {
				eval('this.tween.target.style.' + this.xAxis + ' = this.tween.go.x + \'px\';');
			}
			
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KEffects.effecting[id].cancel();
				}
			}, this.tween.duration + 20);
			return;
		}
		
		if (!Kinky.ALLOW_EFFECTS) {
			if (!this.tween.lock || !this.tween.lock.y) {
				eval('this.tween.target.style.' + this.yAxis + ' = this.tween.go.y + \'px\';');
			}
			if (!this.tween.lock || !this.tween.lock.x) {
				eval('this.tween.target.style.' + this.xAxis + ' = this.tween.go.x + \'px\';');
			}
			this.tween.y = this.tween.go.y;
			this.tween.x = this.tween.go.x;
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration + 10,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				x : this.tween.x,
				y : this.tween.y
			},
			go : {
				x : this.tween.go.x || 0,
				y : this.tween.go.y || 0
			},
			from : {
				x : this.tween.x,
				y : this.tween.y
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectMove.prototype.slowMove = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var x = this.fValue.value.x = Math.round(this.fValue.value.x);
		var y = this.fValue.value.y = Math.round(this.fValue.value.y);
		
		if (!this.tween.lock || !this.tween.lock.y) {
			this.tween.y = y;
			eval('this.tween.target.style.' + this.yAxis + ' = Math.ceil(this.tween.y) + \'px\';');
		}
		if (!this.tween.lock || !this.tween.lock.x) {
			this.tween.x = x;
			eval('this.tween.target.style.' + this.xAxis + ' = Math.ceil(this.tween.x) + \'px\';');
		}
		
		if (!this.fValue.stop && !this.stop) {
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectMove');
});