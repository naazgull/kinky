function KEffectFade(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectFade.prototype = new KEffect();
	KEffectFade.prototype.constructor = KEffectFade;
	
	KEffects.installEffect('fade', 'KEffectFade');
	
	KEffectFade.prototype.config = function() {
		var id = this.id;
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowFade();
			}
		};

		if (!!this.tween.go && (this.tween.go.alpha != null) && this.tween.go.alpha > 1) {
			this.tween.go.alpha = 1;
		}

		if (!!this.tween.from && (this.tween.from.alpha != null)) {
			this.widget.setStyle({
				opacity : this.tween.from.alpha
			}, this.tween.target);
		}
		else {
			var value = undefined;
			if (!!this.tween.target.style.opacity && this.tween.target.style.opacity != '') {
				value = parseFloat(this.tween.target.style.opacity);
			}
			else {
				value = this.tween.go.alpha ^ 1;
			}

			this.tween.from = {
				alpha :  value
			}
		}
		this.visible();
		
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
	};
	
	KEffectFade.prototype.go = function() {
		var id = this.id;
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var style = {
				'MozTransitionDuration' : this.tween.duration + 'ms',
				'WebkitTransitionDuration' : this.tween.duration + 'ms',
				'OTransitionDuration' : this.tween.duration + 'ms',
				'MozTransitionTimingFunction' : this.tween.f,
				'WebkitTransitionTimingFunction' : this.tween.f,
				'MozTransitionProperty' : 'opacity',
				'WebkitTransitionProperty' : 'opacity',
				'OTransitionProperty' : 'opacity'
			};
			
			style.opacity = this.tween.go.alpha;
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KCSS.setStyle(style, [
						KEffects.effecting[id].tween.target
					]);
				}
			}, 15);
			
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KEffects.effecting[id].cancel();
				}
			}, this.tween.duration + 20);
			return;
		}
		
		if (!Kinky.ALLOW_EFFECTS) {
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				alpha : this.tween.fakeAlpha ? 1 - (this.tween.from.alpha) : this.tween.from.alpha
			},
			go : {
				alpha : this.tween.fakeAlpha ? 1 - (this.tween.go.alpha) : this.tween.go.alpha
			},
			from : {
				alpha : this.tween.fakeAlpha ? 1 - (this.tween.from.alpha) : this.tween.from.alpha
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectFade.prototype.slowFade = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var opacity = Math.round(this.fValue.value.alpha * Math.pow(10, 2)) / Math.pow(10, 2);
		this.fValue.value.alpha = opacity;
		
		this.widget.setStyle({
			opacity : opacity
		}, this.tween.target);
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectFade');
});