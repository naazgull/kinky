function KEffectRGB(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectRGB.prototype = new KEffect();
	KEffectRGB.prototype.constructor = KEffectRGB;
	
	KEffects.installEffect('rgb', 'KEffectRGB');
	
	KEffectRGB.prototype.config = function() {
		var id = this.id;
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowRGB();
			}
		};
		
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		if (!!this.tween.from && !!this.tween.from.rgb) {
			this.tween.color = this.tween.from.rgb;
			
			var color_info_from = KDOM.extractColorInfo(this.tween.color);
			this.tween.red = color_info_from.red;
			this.tween.green = color_info_from.green;
			this.tween.blue = color_info_from.blue;
			
			this.tween.target.style.color = this.tween.from.rgb;
		}
		else {
			this.tween.color = this.tween.target.style.color || '#000000';
			
			var color_info_from = KDOM.extractColorInfo(this.tween.color);
			this.tween.red = color_info_from.red;
			this.tween.green = color_info_from.green;
			this.tween.blue = color_info_from.blue;
		}
		
		if (!!this.tween.go.rgb) {
			var color_info_to = KDOM.extractColorInfo(this.tween.go.rgb);
			this.tween.go.red = color_info_to.red;
			this.tween.go.green = color_info_to.green;
			this.tween.go.blue = color_info_to.blue;
			
			delete this.tween.go.rgb;
		}
		
		this.visible();
	};
	
	KEffectRGB.prototype.go = function() {
		var id = this.id;
		
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var style = {
				'MozTransitionDuration' : this.tween.duration + 'ms',
				'WebkitTransitionDuration' : this.tween.duration + 'ms',
				'OTransitionDuration' : this.tween.duration + 'ms',
				'MozTransitionTimingFunction' : this.tween.f,
				'WebkitTransitionTimingFunction' : this.tween.f,
				'MozTransitionProperty' : 'opacity',
				'WebkitTransitionProperty' : 'opacity',
				'OTransitionProperty' : 'opacity'
			};
			
			style.color = 'rgb(' + this.tween.go.red + ', ' + this.tween.go.green + ', ' + this.tween.go.blue + ')';
			KCSS.setStyle(style, [
				this.tween.target
			]);
			
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KEffects.effecting[id].cancel();
				}
			}, this.tween.duration + 20);
			return;
		}
		
		if (!Kinky.ALLOW_EFFECTS) {
			this.widget.setStyle({
				color : 'rgb(' + this.tween.go.red + ', ' + this.tween.go.green + ', ' + this.tween.go.blue + ')'
			}, this.tween.target);
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				red : this.tween.red,
				green : this.tween.green,
				blue : this.tween.blue
			},
			go : {
				red : this.tween.go.red,
				green : this.tween.go.green,
				blue : this.tween.go.blue
			},
			from : {
				red : this.tween.red,
				green : this.tween.green,
				blue : this.tween.blue
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectRGB.prototype.slowRGB = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var red = this.fValue.value.red = Math.round(this.fValue.value.red);
		var green = this.fValue.value.green = Math.round(this.fValue.value.green);
		var blue = this.fValue.value.blue = Math.round(this.fValue.value.blue);
		
		this.widget.setStyle({
			color : 'rgb(' + red + ', ' + green + ', ' + blue + ')'
		}, this.tween.target);
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectRGB');
});