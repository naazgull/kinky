function KEffectResize(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectResize.prototype = new KEffect();
	KEffectResize.prototype.constructor = KEffectResize;
	
	KEffects.installEffect('resize', 'KEffectResize');
	
	KEffectResize.prototype.config = function() {
		var id = this.id;
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowResize();
			}
		};
		
		this.tween.unit = this.tween.unit || 'px';
		
		this.tween.original = {
			overflow : this.tween.target.style.overflow,
			position : this.tween.target.style.position
		};
		
		this.tween.gravity = this.tween.gravity || {
			yAxis : 'height',
			xAxis : 'width'
		};
		
		if (!!this.tween.from && !!this.tween.from.width) {
			this.tween.width = this.tween.from.width;
			this.tween.target.style.width = this.tween.width + 'px';
		}
		else {
			this.tween.width = parseInt(this.tween.target.style.width.replace(/px/, ''));
			if (isNaN(this.tween.width) || (this.tween.width == null)) {
				this.tween.width = 0;
				if (!this.tween.lock || !this.tween.lock.width) {
					this.tween.target.style.width = '0px';
				}
			}
		}
		
		if (!!this.tween.from && !!this.tween.from.height) {
			this.tween.height = this.tween.from.height;
			this.tween.target.style.height = this.tween.height + 'px';
		}
		else {
			this.tween.height = parseInt(this.tween.target.style.height.replace(/px/, ''));
			if (isNaN(this.tween.height) || (this.tween.height == null)) {
				this.tween.height = 0;
				if (!this.tween.lock || !this.tween.lock.height) {
					this.tween.target.style.height = '0px';
				}
			}
		}
		
		this.visible();
	};
	
	KEffectResize.prototype.go = function() {
		var id = this.id;
		
		if (this.tween.lock && this.tween.lock.height) {
			this.tween.height = this.tween.go.height = 0;
		}
		if (this.tween.lock && this.tween.lock.width) {
			this.tween.width = this.tween.go.width = 0;
		}
		
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var style = {
				'MozTransitionDuration' : this.tween.duration + 'ms',
				'WebkitTransitionDuration' : this.tween.duration + 'ms',
				'OTransitionDuration' : this.tween.duration + 'ms',
				'MozTransitionTimingFunction' : this.tween.f,
				'WebkitTransitionTimingFunction' : this.tween.f,
				'MozTransitionProperty' : 'width,height',
				'WebkitTransitionProperty' : 'width,height',
				'OTransitionProperty' : 'width,height'
			};
			
			if (!this.tween.lock || !this.tween.lock.width) {
				style.width = this.tween.go.width + 'px';
			}
			
			if (!this.tween.lock || !this.tween.lock.height) {
				style.height = this.tween.go.height + 'px';
			}
			KCSS.setStyle(style, [
				this.tween.target
			]);
			
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KEffects.effecting[id].cancel();
				}
			}, this.tween.duration + 20);
			return;
		}
		
		if (!Kinky.ALLOW_EFFECTS) {
			if (!this.tween.lock || !this.tween.lock.width) {
				this.tween.target.style.width = this.tween.go.width + this.tween.unit;
			}
			if (!this.tween.lock || !this.tween.lock.height) {
				this.tween.target.style.height = this.tween.go.height + this.tween.unit;
			}
			
			this.widget.setStyle(this.tween.original, this.tween.target);
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				width : this.tween.width,
				height : this.tween.height
			},
			go : {
				width : this.tween.go.width || 0,
				height : this.tween.go.height || 0
			},
			from : {
				width : this.tween.width,
				height : this.tween.height
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectResize.prototype.slowResize = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var width = this.fValue.value.width = Math.round(this.fValue.value.width);
		var height = this.fValue.value.height = Math.round(this.fValue.value.height);
		
		if (!this.tween.lock || !this.tween.lock.width) {
			this.tween.width = width;
			this.tween.target.style.width = Math.ceil(this.tween.width) + this.tween.unit;
		}
		if (!this.tween.lock || !this.tween.lock.height) {
			this.tween.height = height;
			this.tween.target.style.height = Math.ceil(this.tween.height) + this.tween.unit;
		}
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectResize');
});