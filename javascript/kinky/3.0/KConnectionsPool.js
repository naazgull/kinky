function KConnectionsPool() {
}
{
	
	KConnectionsPool.getConnection = function(type, shell) {
		if (!shell) {
			throw Error('No Shell defined for connection pool');
		}
		if (!KConnectionsPool.connections[shell]) {
			KConnectionsPool.connections[shell] = {};
		}
		var connection = KConnectionsPool.connections[shell][type];
		if (!connection) {
			KConnectionsPool.connections[shell][type] = connection = KSystem.construct({
				jsClass : KConnectionsPool.connectors[type],
				args : {
					servicesURL : !!Kinky.SHELL_CONFIG[shell].providers[type] ? Kinky.SHELL_CONFIG[shell].providers[type].services : undefined,
					dataURL : !!Kinky.SHELL_CONFIG[shell].providers[type] ? Kinky.SHELL_CONFIG[shell].providers[type].data : undefined,
					headers : Kinky.SHELL_CONFIG[shell].headers
				}
			});
			connection.shell = shell;
		}
		if (Kinky.getShellConfig(shell)) {
			connection.headers = Kinky.getShellConfig(shell).headers;
		}
		return connection;
		
	};
	
	KConnectionsPool.registerConnector = function(type, connectorClass) {
		KConnectionsPool.connectors[type] = connectorClass;
	};
	
	KConnectionsPool.connectors = {};
	KConnectionsPool.connections = {};
	
	KConnectionsPool.registerConnector('php', 'KConnectionPHP');
	KConnectionsPool.registerConnector('jsonp', 'KConnectionJSONP');
	KConnectionsPool.registerConnector('js', 'KConnectionJSON');
	KConnectionsPool.registerConnector('rest', 'KConnectionREST');
	KConnectionsPool.registerConnector('oauth', 'KConnectionOAuth');
	KConnectionsPool.registerConnector('https', 'KConnectionHTTP');
	KConnectionsPool.registerConnector('http', 'KConnectionHTTP');
	
	KSystem.included('KConnectionsPool');
}