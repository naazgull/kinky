function KFBConnect(parent) {
	if (parent != null) {
		KButton.call(this, parent, '', 'fb-connect', KFBConnect.connect, 'button');
		this.addActionListener(KFBConnect.actions, [
			'/fb-connect'
		]);
	}
}

KSystem.include([
	'KButton'
], function() {
	KFBConnect.prototype = new KButton();
	KFBConnect.prototype.constructor = KFBConnect;
	
	KFBConnect.prototype.go = function() {
		if (!this.activated()) {
			this.load();
		}
		else {
			this.redraw();
		}
	};
	
	KFBConnect.prototype.load = function() {
		KFBOpenGraph.queryFB(this, {
			hash : '/me',
			args : {
				fields : 'id,first_name,last_name,name,link,about,birthday,work,education,email,website,hometown,location,bio,quotes,gender,interested_in,meeting_for,relationship_status,religion,political,verified,significant_other,timezone,picture',
				type : 'small'
			}
		}, 'onLoad');
	};
	
	KFBConnect.prototype.onLoad = function(data, request) {
		if (data.error) {
		}
		else {
			Kinky.setLoggedFB(data);
			this.onAuthorize({
				code : Kinky.getLoggedUser() ? 200 : 401
			});
		}
		this.draw();
	};
	
	KFBConnect.prototype.onAuthorize = function(data, request) {
	};
	
	KFBConnect.connect = function(event) {
		var button = KDOM.getEventWidget(event);
		KBreadcrumb.dispatchEvent(button.id, {
			action : '/fb-connect'
		});
	};
	
	KFBConnect.actions = function(widget, action) {
		if (widget.activated()) {
			switch (action) {
				case '/fb-connect': {
					if (Kinky.getLoggedFB()) {
						var pup = window.open(Kinky.getLoggedFB().link);
						KSystem.popup.check(pup, Kinky.getShell());
					}
					else {
						var url = encodeURIComponent(window.document.location.href);
						window.document.location = KFBOpenGraph.FB_APP_AUTH_URL + '&redirect_uri=' + url;
					}
					break;
				}
			}
		}
	};
	
	KSystem.included('KFBConnect');
});