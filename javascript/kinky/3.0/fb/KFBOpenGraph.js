function KFBOpenGraph() {
}
{
	KFBOpenGraph.prototype.constructor = KFBOpenGraph;

	KFBOpenGraph.queryFB = function(widget, params, callback) {
		if (!KFBOpenGraph.FB_ACCESS_TOKEN) {
			KFBOpenGraph.FB_ACCESS_TOKEN = KOAuth.getAccessToken();
		}
		params.id = widget.id;
		params.callback = callback;
		KFBOpenGraph.send(params);

	};

	KFBOpenGraph.postFB = function(widget, params, callback) {
		if (!KFBOpenGraph.FB_ACCESS_TOKEN) {
			KFBOpenGraph.FB_ACCESS_TOKEN = KOAuth.getAccessToken();
		}
		params.args = params.args || {};
		params.args.access_token = KFBOpenGraph.FB_ACCESS_TOKEN;
		Kinky.bunnyMan.get(widget, 'php:Facebook:Post', params, callback);
	};

	KFBOpenGraph.send = function(params) {
		var url = (params.url || 'https://graph.facebook.com') + (params.hash || '') + '?' + (!params.noToken && KFBOpenGraph.FB_ACCESS_TOKEN ? 'access_token=' + KFBOpenGraph.FB_ACCESS_TOKEN : '') + (params.callback ? '&callback=' + encodeURIComponent('Kinky.bunnyMan.widgets[\'' + params.id + '\'].' + params.callback) : '&callback=void');

		for ( var index in params.args) {
			url += '&' + index + '=' + params.args[index];
		}

		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = url;
		head.appendChild(script);
	};

	KFBOpenGraph.setAccessToken = function(token) {
		KFBOpenGraph.FB_ACCESS_TOKEN = token;
		if (KFBOpenGraph.FB_PERSISTENT_SESSION) {

		}
	};

	KFBOpenGraph.FB_APP_ID = null;
	KFBOpenGraph.FB_APP_URL = null;
	KFBOpenGraph.FB_APP_AUTH_URL = null;
	KFBOpenGraph.FB_ACCESS_TOKEN = null;
	KFBOpenGraph.FB_PERSISTENT_SESSION = true;

	KSystem.included('KFBOpenGraph');
}