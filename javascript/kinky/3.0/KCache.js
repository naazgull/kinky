function KCache() {
	this.cache = {};
}
{
	KCache.prototype = {};
	KCache.prototype.constructor = KCache;
	
	KCache.commit = function(key, value) {
		KCache.bumbleBee.cache[key] = value;
	};
	
	KCache.restore = function(key) {
		return KCache.bumbleBee.cache[key];
	};
	
	KCache.clear = function(key) {
		delete KCache.bumbleBee.cache[key];
	};
	
	KCache.find = function(regex, action) {
		if (action instanceof Function) {
			for ( var key in KCache.bumbleBee.cache) {
				if (regex.test(key)) {
					action(key, KCache.bumbleBee.cache[key]);
				}
			}
			return null;
		}
		else {
			var ret = [];
			for ( var key in KCache.bumbleBee.cache) {
				if (regex.test(key)) {
					ret.push(KCache.bumbleBee.cache[key]);
				}
			}
			return ret;
		}
	};
	
	KCache.bumbleBee = new KCache();
	
	KSystem.included('KCache');
}
