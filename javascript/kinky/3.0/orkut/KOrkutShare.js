function KOrkutShare(parent, params) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.hash = '/orkut-share' + params.tweetPageHash;
		this.data = {
			hash : params.sharePageHash,
			url : params.siteURL,
			size : params.size,
			title : params.title ? params.title : null,
			body : params.body ? params.body : null,
			thumbnail : params.thumbnail ? params.thumbnail : null
		};
		this.isStatic = false;
	}
}

KSystem.include([
	'KPanel'
], function() {
	KOrkutShare.prototype = new KPanel();
	KOrkutShare.prototype.constructor = KOrkutShare;

	KOrkutShare.prototype.draw = function() {
		if (!this.isStatic){
			this.addLocationListener(KOrkutShare.shareMe);
		}

		this.onChangeURL(this.data.hash);

		this.setStyle({
			width : this.data.size.width + 'px',
			height : this.data.size.height + 'px',
			overflow : 'hidden'
		});
		KPanel.prototype.draw.call(this);
	};

	KOrkutShare.prototype.onChangeURL = function(hash) {

		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.async = false;
		script.src = 'http://www.google.com/jsapi';
		head.appendChild(script);

		this.content.innerHTML = '<a href="http://promote.orkut.com/preview?nt=orkut.com&tt='+ this.data.title +'&du=' + encodeURIComponent(this.data.url + this.data.hash.substring(1)) + (this.data.body != null ? '&cn=' + encodeURIComponent(this.data.body) : '') + '" target="_blank"><img src="' + this.data.thumbnail + '" border=0></a>';

	};

	KOrkutShare.shareMe = function(widget, hash) {
		if (widget.activated()) {
			widget.onChangeURL(hash);
		}
	};

	KSystem.included('KOrkutShare');
});
