function KException(invoker, message) {
	if (invoker != null) {
		if (typeof invoker == 'string') {
			this.message = invoker + '.' + message;
		}
		else {
			this.message = (KSystem.getObjectClass(invoker)) + (!!invoker.id ? '[' + invoker.id + ']' : '') + '.' + message;
		}
	}
}
{
	KException.prototype = new Error();
	KException.prototype.constructor = KException;
	
	KException.prototype.printStackTrace = function() {
		var callstack = new Array();
		
		if (this.stack) { // Firefox
			var lines = this.stack.split("\n");
			for ( var i = 0, len = lines.length; i != len; i++) {
				if (lines[i] != '') {
					callstack.push(lines[i]);
				}
			}
		}
		else if (window.opera && this.message) { // Opera
			var lines = this.message.split("\n");
			for ( var i = 0, len = lines.length; i != len; i++) {
				if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\(/)) {
					var entry = lines[i];
					if (lines[i + 1]) {
						entry += " at " + lines[i + 1];
						i++;
					}
					callstack.push(entry);
				}
			}
		}
		else {
			var currentFunction = arguments.callee.caller;
			while (currentFunction) {
				var fn = currentFunction.toString();
				var fname = fn.substring(fn.indexOf("function") + 8, fn.indexOf("(")) || "anonymous";
				callstack.push(fname);
				currentFunction = currentFunction.caller;
			}
		}
		
		var list = window.document.createElement('ol');
		for ( var index in callstack) {
			var error = list.appendChild(window.document.createElement('li'));
			error.appendChild(window.document.createTextNode(callstack[index]));
		}
		return list;
	};
	
	KSystem.included('KException');
}

function KNoExtensionException(invoker, methodName) {
	if (invoker) {
		KException.call(this, invoker, KSystem.getObjectClass(invoker) + '[' + invoker.id + ']' + ': ' + methodName + ': child class must implement this method.');
	}
}
{
	KNoExtensionException.prototype = new KException();
	KNoExtensionException.prototype.constructor = KNoExtensionException;
	
	KSystem.included('KNoExtensionException');
}