function Kinky() {
	this.resizeListeners = {};
	this.widgetResizeListeners = {};
	this.lastWidth = 0;
	this.lastHeight = 0;
	
	this.widgets = new Object();
	this.widgetCount = 0;
	
	this.request = null;
	this.loggedUser = null;
	this.loggedFB = null;
	this.currentState = [
		'default'
	];
}
{
	Kinky.prototype.constructor = Kinky;
	
	Kinky.getInstance = function() {
		return Kinky.bunnyMan;
	};
	
	Kinky.config = function() {
		if (KSystem.hasPendingImports()) {
			KSystem.addTimer(Kinky.config, 500);
			return;
		}

		if (Kinky.bunnyMan.started) {
			return;
		}
		Kinky.bunnyMan.started = true;
		
		for ( var shell in Kinky.SHELL_CONFIG) {
			Kinky.SHELL_CONFIG[shell].id = shell;
		}
		
		Kinky.bunnyMan.widgets['proxy'] = Kinky.bunnyMan;
		Kinky.bunnyMan.widgets['picker'] = KOAuth.lockPicker;
		
		KDOM.addEventListener(window.document.body, 'click', function(event) {
			var mouseEvent = KDOM.getEvent(event);
			Kinky.BROWSER_CLICK = (!!Kinky.BROWSER_CLICK ? Kinky.BROWSER_CLICK : {});
			Kinky.BROWSER_CLICK.x = mouseEvent.clientX;
			Kinky.BROWSER_CLICK.y = mouseEvent.clientY;
			Kinky.BROWSER_CLICK.w = KDOM.getEventWidget(event);
			
			Kinky.releaseModal(event, true);
		});
		
		if (window.addEventListener) {
			window.addEventListener('DOMMouseScroll', Kinky.releaseModal, false);
		}
		window.document.onmousewheel = window.onmousewheel = Kinky.releaseModal;
		
		Kinky.init();
		window.kinky = Kinky.bunnyMan;
	};
	
	Kinky.init = function() {
		if (Kinky.ALLOW_EFFECTS) {
			KEffects.loadTimmingFunctions();
		}
		
		KBreadcrumb.sparrow = new KBreadcrumb();
		KSystem.addTimer(KBreadcrumb.onLocationChange, 0);
		KSystem.addTimer(Kinky.onResize, 0);
		KSystem.addTimer(Kinky.onResizeWidget, 0);
		
		var index = -1;
		if ((index = window.document.location.href.indexOf('access_token=')) != -1) {
			var indexOfE = window.document.location.href.indexOf('&', index);
			var accessToken = null;
			if (indexOfE == -1) {
				accessToken = window.document.location.href.substr(index + 13);
			}
			else {
				accessToken = window.document.location.href.substring(index + 13, indexOfE);
			}
			if (window.document.location.href.indexOf('#access_token=') != -1) {
				// REMOVED DUE TO PROBLEMS WHEN CONNECTING TO SEVERAL BACKENDS -> DO NOT UNCOMMENT
				/*KOAuth.session({
					authentication : 'OAuth2.0 ' + accessToken
				}, {}, 'GLOBAL');*/
				KBreadcrumb.dispatchURL({
					hash : ''
				});
			}
		}
		
		if (!!Kinky.SHELL_CONFIG) {
			for ( var shell in Kinky.SHELL_CONFIG) {
				if (!!Kinky.SHELL_CONFIG[shell].auth && !!Kinky.SHELL_CONFIG[shell].auth.service) {
					KOAuth.start(shell, !!Kinky.SHELL_CONFIG[shell].href);
				}
				else {
					if (!!Kinky.SHELL_CONFIG[shell].href) {
						Kinky.loadShell(Kinky.SHELL_CONFIG[shell]);
					}
					else {
						Kinky.SHELL_CONFIG[shell].id = shell;
						Kinky.SHELL_CONFIG[shell].href = shell;
						Kinky.SHELL_CONFIG[shell].requestTypes = [
							'application/vnd.kinky.' + Kinky.SHELL_CONFIG[shell].className
						];
						Kinky.bunnyMan.start(Kinky.SHELL_CONFIG[shell]);
					}
				}
			}
		}
	};
	
	Kinky.loadShell = function(shellConfig, callbacks) {
		var cbs = [
			'Kinky.bunnyMan.start'
		];
		if (!!callbacks) {
			if (typeof callbacks == 'string') {
				cbs.push('Kinky.bunnyMan.' + callbacks);
			}
			else {
				for ( var cb in callbacks) {
					cbs.push('Kinky.getWidget(\'' + cb + '\').' + callbacks[cb]);
				}
			}
		}
		var service = shellConfig.preferredProvider + ':' + shellConfig.href;
		var request = {
			type : service.substring(0, service.indexOf(':')),
			namespace : service.substring(service.indexOf(':') + 1, service.lastIndexOf(':')),
			service : service.substring(service.lastIndexOf(':') + 1),
			action : 'get',
			id : 'proxy',
			callback : cbs
		};
		
		try {
			KConnectionsPool.getConnection(request.type, shellConfig.id).send(request, null, shellConfig.headers);
		}
		catch (e) {
			KSystem.include(KConnectionsPool.connectors[request.type], function() {
				KConnectionsPool.getConnection(request.type, shellConfig.id).send(request, null, shellConfig.headers);
			});
		}
	};
	
	Kinky.setModal = function(widget, withScroll) {
		Kinky.MODAL_WIDGET.unshift({
			w : widget,
			s : !!withScroll
		});
	};
	
	Kinky.releaseModal = function(event, notWithScroll) {
		if (Kinky.MODAL_WIDGET.length != 0) {
			var widget = KDOM.getEventWidget(event);
			if (!widget || (widget.className == 'KFloatable')) {
				return;
			}
			
			var parent = KDOM.getEventWidget(event, Kinky.MODAL_WIDGET[0].w.className);
			if ((!parent || ((!!parent && (parent.id != Kinky.MODAL_WIDGET[0].w.id)))) && (notWithScroll || (!notWithScroll == Kinky.MODAL_WIDGET[0].s))) {
				var modal = Kinky.MODAL_WIDGET[0].w;
				if (!!modal.data && !!modal.data.params && !modal.data.params.modal) {
					return;
				}
				Kinky.MODAL_WIDGET.shift();
				modal.blur();
			}
		}
	};
	
	Kinky.getActiveModalWidget = function() {
		if (Kinky.MODAL_WIDGET.length == 0) {
			return null;
		}
		return Kinky.MODAL_WIDGET[0].w;
	};
	
	Kinky.unsetModal = function(widget) {
		if ((Kinky.MODAL_WIDGET.length != 0) && ((!widget) || (widget.id == Kinky.MODAL_WIDGET[0].w.id))) {
			var modal = Kinky.MODAL_WIDGET[0].w;
			Kinky.MODAL_WIDGET.shift();
			modal.blur();
			if (Kinky.MODAL_WIDGET.length != 0) {
				Kinky.MODAL_WIDGET[0].w.focus();
			}
		}
	};
	
	Kinky.getShell = function(shellName) {
		if (!!shellName) {
			return Kinky.shell[shellName];
		}
		else {
			if (KSystem.isIncluded('KShell')) {
				for ( var w in Kinky.bunnyMan.widgets) {
					if (Kinky.bunnyMan.widgets[w] instanceof KShell) {
						return Kinky.bunnyMan.widgets[w];
					}
				}
			}
			return null;
		}
	};
	
	Kinky.getDefaultShell = function() {
		if (!Kinky.SHELL_CONFIG || !Kinky.shell) {
			return undefined;
		}
		for (var shellName in Kinky.SHELL_CONFIG) {
			return Kinky.shell[shellName];
		}
	};
	
	Kinky.getShellConfig = function(shellName) {
		return Kinky.SHELL_CONFIG[shellName];
	};
	
	Kinky.dispatchState = function(stateName) {
		Kinky.bunnyMan.currentState.push(stateName);
		for ( var widget in Kinky.bunnyMan.widgets) {
			if (/widget([0-9]+)/.test(widget)) {
				Kinky.bunnyMan.widgets[widget].dispatchState(stateName);
			}
		}
		
	};
	
	Kinky.getWidget = function(widgetID) {
		return Kinky.bunnyMan.getWidget(widgetID);
	};
	
	Kinky.clearWidgets = function(shell) {
		return Kinky.bunnyMan.clearWidgets(shell);
	};
	
	Kinky.getLoggedUser = function() {
		return Kinky.bunnyMan.loggedUser;
	};
	
	Kinky.setLoggedFB = function(data) {
		Kinky.bunnyMan.loggedFB = data;
	};
	
	Kinky.getLoggedFB = function() {
		return Kinky.bunnyMan.loggedFB;
	};
	
	Kinky.clearSession = function(shellName) {
		delete Kinky.bunnyMan.loggedUser;
		delete Kinky.bunnyMan.loggedFB;
		KOAuth.destroy();
		if (!!shellName) {
			KOAuth.destroy(shellName);
		}
	};
	
	Kinky.startSession = function(userData) {
		if (userData) {
			Kinky.bunnyMan.loggedUser = userData;
		}
	};
	
	Kinky.onResize = function() {
		try {
			Kinky.bunnyMan.dispatchResize();
		}
		catch (e) {
		}
	};
	
	Kinky.onResizeWidget = function() {
		try {
			Kinky.bunnyMan.dispatchResizeWidget();
		}
		catch (e) {
			throw e;
		}
	};
	
	Kinky.prototype.start = function(config) {
		if (!Kinky.shell) {
			Kinky.shell = {};
		}
		var feClass = KSystem.getWRMLClass(config);
		KSystem.include(feClass, function() {
			var shell = KSystem.construct(config, -1);
			shell.parent = document.getElementById(Kinky.SHELL_CONFIG[config.id].parent);
			shell.data = config;
			
			var auth = KOAuth.getAuthorization();
			// var auth = KOAuth.getAuthorization(config.href);
			if (!auth) {
				auth = KOAuth.getAuthorization(config.id);
				// auth = KOAuth.getAuthorization();
			}
			if (!!auth) {
				if (!Kinky.SHELL_CONFIG[config.id].headers) {
					Kinky.SHELL_CONFIG[config.id].headers = {};
				}
				Kinky.SHELL_CONFIG[config.id].headers['Authorization'] = auth.authentication;
			}
			
			shell.start(Kinky.SHELL_CONFIG[config.id].startPage);
			Kinky.shell[config.id] = shell;
		}, (!!config.includeroot ? config.includeroot : null));
	};
	
	Kinky.prototype.dispatchResize = function() {
		var width = KDOM.getBrowserWidth();
		var height = KDOM.getBrowserHeight();
		if ((width != this.lastWidth) || (height != this.lastHeight)) {
			this.lastHeight = height;
			this.lastWidth = width;
			for ( var widget in this.resizeListeners) {
				for ( var index in this.resizeListeners[widget]) {
					this.resizeListeners[widget][index].call(null, this.widgets[widget], {
						width : width,
						height : height
					});
				}
			}
		}
		KSystem.addTimer(Kinky.onResize, 200);
	};
	
	Kinky.prototype.dispatchResizeWidget = function() {
		for ( var widget in this.widgetResizeListeners) {
			if (!this.widgets[widget].activated() || !this.widgets[widget].display) {
				continue;
			}
			var width = this.widgets[widget].getWidth();
			var height = this.widgets[widget].getHeight();
			
			if ((width != this.widgets[widget].lastWidth) || (height != this.widgets[widget].lastHeight)) {
				this.widgets[widget].lastHeight = height;
				this.widgets[widget].lastWidth = width;
				for ( var index in this.widgetResizeListeners[widget]) {
					this.widgetResizeListeners[widget][index].call(null, this.widgets[widget], {
						width : width,
						height : height
					});
				}
			}
		}
		KSystem.addTimer(Kinky.onResizeWidget, 200);
	};
	
	Kinky.prototype.addWindowResizeListener = function(widget, callback) {
		if (this.resizeListeners[widget.id]) {
			this.resizeListeners[widget.id].push(callback);
		}
		else {
			this.resizeListeners[widget.id] = new Array();
			this.resizeListeners[widget.id].push(callback);
		}
	};
	
	Kinky.prototype.addResizeListener = function(widget, callback) {
		if (this.widgetResizeListeners[widget.id]) {
			this.widgetResizeListeners[widget.id].push(callback);
		}
		else {
			this.widgetResizeListeners[widget.id] = new Array();
			this.widgetResizeListeners[widget.id].push(callback);
		}
	};
	
	// >>>
	// HTTP response handlers
	Kinky.prototype.onError = function(data, request) {
		console.log(data);
	};
	
	Kinky.prototype.onOk = function(data, request) {
	};
	
	Kinky.prototype.onHead = function(data, request) {
	};
	
	Kinky.prototype.onCreated = function(data, request) {
	};
	
	Kinky.prototype.onAccepted = function(data, request) {
	};
	
	Kinky.prototype.onNoContent = function(data, request) {
	};
	
	Kinky.prototype.onBadRequest = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onUnauthorized = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onForbidden = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onNotFound = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onMethodNotAllowed = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onPreConditionFailed = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onInternalError = function(data, request) {
		this.onError(data);
	};
	// <<<
	
	Kinky.prototype.addWidget = function(widget, prefix) {
		var widgetID = (!!prefix ? prefix : 'widget') + (++this.widgetCount);
		this.widgets[widgetID] = widget;
		return widgetID;
	};
	
	Kinky.prototype.removeWidget = function(widgetID) {
		delete KBreadcrumb.sparrow.byWidget.location[widgetID];
		delete KBreadcrumb.sparrow.byWidget.query[widgetID];
		delete KBreadcrumb.sparrow.byWidget.action[widgetID];
		delete this.widgetResizeListeners[widgetID];
		delete this.resizeListeners[widgetID];
		delete this.widgets[widgetID];
	};
	
	Kinky.prototype.getWidget = function(widgetID) {
		return this.widgets[widgetID];
	};
	
	Kinky.prototype.clearWidgets = function(shell) {
		for ( var c in this.widgets) {
			if (!!this.widgets[c] && (!shell || (!!shell && (this.widgets[c].shell == shell)))) {
				this.widgets[c].unload();
			}
		}
	};
	
	Kinky.prototype.perform = function(widget, performative, service, content, headers, callback) {
		var type = service.substring(0, service.indexOf(':'));
		var callbacks = [];
		
		if (typeof callback == 'string') {
			callbacks.push('Kinky.getWidget(\'' + widget.id + '\').' + callback);
		}
		else {
			for ( var cb in callback) {
				callbacks.push('Kinky.getWidget(\'' + cb + '\').' + callback[cb]);
			}
		}
		
		if (KSystem.isIncluded(KConnectionsPool.connectors[type])) {
			var request = {
				type : service.substring(0, service.indexOf(':')),
				namespace : service.substring(service.indexOf(':') + 1, service.lastIndexOf(':')),
				service : service.substring(service.lastIndexOf(':') + 1),
				action : performative,
				id : widget.id,
				callback : callbacks
			};
			if (widget instanceof KWidget) {
				widget.wait();
			}
			widget.getConnection(request.type).send(request, content, headers);
		}
		else {
			KSystem.include(KConnectionsPool.connectors[type], function() {
				var request = {
					type : service.substring(0, service.indexOf(':')),
					namespace : service.substring(service.indexOf(':') + 1, service.lastIndexOf(':')),
					service : service.substring(service.lastIndexOf(':') + 1),
					action : performative,
					id : widget.id,
					callback : callbacks
				};
				if (widget instanceof KWidget) {
					widget.wait();
				}
				widget.getConnection(request.type).send(request, content, headers);
			});
		}
	};
	
	Kinky.prototype.get = function(widget, service, content, headers, callback) {
		this.perform(widget, 'get', service, content, headers, callback || 'onLoad');
	};
	
	Kinky.prototype.save = function(widget, service, content, headers, callback) {
		this.perform(widget, 'post', service, content, headers, callback || 'onSave');
	};
	
	Kinky.prototype.remove = function(widget, service, content, headers, callback) {
		this.perform(widget, 'delete', service, content, headers, callback || 'onRemove');
	};
	
	Kinky.prototype.put = function(widget, service, content, headers, callback) {
		this.perform(widget, 'put', service, content, headers, callback || 'onPut');
	};
	
	Kinky.prototype.post = function(widget, service, content, headers, callback) {
		this.perform(widget, 'post', service, content, headers, callback || 'onPost');
	};
	
	Kinky.prototype.head = function(widget, service, content, headers, callback) {
		this.perform(widget, 'head', service, content, headers, callback || 'onHead');
	};
	
	Kinky.MODAL_WIDGET = [];
	Kinky.INCLUDER_URL = null;
	Kinky.BASE_TEMP_URL = null;
	Kinky.BASE_TEMP_DIR = null;
	Kinky.SHELL_CONFIG = null;
	Kinky.ENTITIES = false;
	Kinky.URL_ENCODED = false;
	Kinky.SHOW_INSTANTIATIONS_ERRORS = false;
	Kinky.ALLOW_EFFECTS = false;
	Kinky.PERSISTENT_SESSION = false;
	Kinky.FB_APP_ID = null;
	Kinky.FB_APP_COOKIE_NAME = null;
	Kinky.FB_APP_URL = null;
	Kinky.BROWSER_CLICK = {
		w : null,
		x : 0,
		y : 0
	};
	Kinky.TITLE_CLEAR_BOTH = false;
	Kinky.DEV_MODE = true;
	
	Kinky.shell = null;
	Kinky.bunnyMan = new Kinky();
	Kinky.HTML_READY = 'html4';
	Kinky.CSS_READY = 'css2';
	Kinky.DUMP_MODE = 'console';
	Kinky.IMAGE_PROXY = null;
	Kinky.LOADER_IMAGE = '/images/loader.gif';
	
	KSystem.included('Kinky');
}