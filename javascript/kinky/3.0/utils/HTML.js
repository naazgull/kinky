var HTML = {
	
	stripTags : function(data, tag) {
		if (!data) {
			return '';
		}
		var regex = null;
		if (tag) {
			regex = new RegExp("<" + tag + "([^>]*)>|<\/" + tag + "([^>]*)>", 'g');
		}
		else {
			regex = /<([^>]*)>|<\/([^>]*)>/g;
		}
		data = data.replace(regex, function(wholematch, match) {
			switch (match) {
				case 'p':
					return '\n';
					
				case 'br':
					return String.fromCharCode(161);
			}
			return "";
		});
		return data.replace(/^\s*/, "").replace(/\s*$/, "");
	}

};

KSystem.included('HTML');