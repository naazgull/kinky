function Quicksort() {
}

{
	Quicksort.prototype = {};
	Quicksort.prototype.constructor = Quicksort;
	
	Quicksort.quicksort = function(list, compareCallback) {
		if (list.length <= 1) {
			return list;
		}
		var middle = Math.floor(list.length / 2);
		var pivot = list.splice(middle, 1);
		
		var less = [];
		var greater = [];
		
		for ( var index in list) {
			if (compareCallback(list[index], pivot[0]) <= 0) {
				less.push(list[index]);
			}
			else {
				greater.push(list[index]);
			}
		}
		
		return [].concat(Quicksort.quicksort(less, compareCallback), pivot, Quicksort.quicksort(greater, compareCallback));
	};
	
	Quicksort.quicksortKeys = function(object, compareCallback) {
		var realList = [];
		for ( var index in object) {
			realList.push(index);
		}
		
		if (realList.length <= 1) {
			return realList;
		}
		
		return Quicksort.quicksort(realList, compareCallback);
	};
	
	KSystem.included('Quicksort');
}