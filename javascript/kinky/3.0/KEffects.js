function KEffects() {
}
{
	KEffects.prototype.constructor = KEffects;
	
	KEffects.effecting = new Object();
	KEffects.installed = new Object();
	
	KEffects.installEffect = function(typeName, className) {
		KEffects.installed[typeName] = className;
	};
	
	KEffects.stopEffect = function(effect) {
		delete KEffects.effecting[effect.id];
	};
	
	KEffects.cancelEffect = function(elementOrID, effectType) {
		if (!!effectType) {
			if (KEffects.effecting[elementOrID.id + effectType]) {
				KEffects.effecting[elementOrID.id + effectType].cancel();
			}
		}
		else {
			if (KEffects.effecting[elementOrID]) {
				KEffects.effecting[elementOrID].cancel();
			}
		}
	};
	
	KEffects.getEffectByElement = function(element, effectType) {
		if (!element || !element.id) {
			return null;
		}
		return KEffects.effecting[element.id + effectType];
	};
	
	KEffects.getEffectById = function(id) {
		return KEffects.effecting[id];
	};
	
	KEffects.addEffect = function(element, tweenToAdd, targetW) {
		var widget = targetW || ((element instanceof KWidget || (!!element.className && KSystem.isAddOn(element.className))) ? element : KDOM.getWidget(element, tweenToAdd.targetClass));
		var tweens = null;
		
		if (tweenToAdd instanceof Array) {
			tweens = tweenToAdd;
		}
		else {
			tweens = new Array();
			tweens.push(tweenToAdd);
		}
		
		var includes = [];
		for ( var index in tweens) {
			includes.push(KEffects.installed[tweens[index].type]);
		}
		
		KSystem.include(includes, function() {
			if (!element || element == undefined) {
				return;
			}

			for ( var index in tweens) {
				var tween = tweens[index];
				var elementid = null;
				
				if (element instanceof KWidget) {
					tween.target = widget.panel;
					elementid = element.id;
				}
				else {
					tween.target = element;
					if (!element.id || (element.id == '')) {
						if (!widget || widget == undefined) {
							return;
						}
						elementid = 'child' + widget.id.replace(/widget/, '') + '.' + widget.childrenid++;
						element.id = elementid;
					}
					else {
						elementid = element.id;
					}
					
				}
				var effectid = elementid + tween.type;
				tween.id = effectid;
				
				if (!!KEffects.effecting[effectid]) {
					KEffects.effecting[effectid].cancel(true);
				}
				
				KEffects.loadEffect(tween, element, widget);
			}
		});
	};
	
	KEffects.loadEffect = function(tween, element, widget) {
		var effect = null;
		
		if (tween.type != 'drag') {
			if (!tween.duration) {
				throw new KException('KEffects', 'loadEffect: "duration" parameter must be a number greater than 0');
			}
			if (!tween.go) {
				throw new KException('KEffects', 'loadEffect: the "go" parameter must be instantiated');
			}
		}

		eval('effect = new ' + KEffects.installed[tween.type] + '(widget, tween, element);');
		if (!effect.tween) {
			return;
		}
		effect.id = effect.tween.id;
		KEffects.effecting[effect.id] = effect;
		effect.start = (new Date()).getTime();
		effect.config();
		effect.go();
	};
	
	KEffects.loadTimmingFunctions = function() {
		KEffects.linear = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			// c*t/d + b
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : c * t / d + b;
			}
			return toReturn;
		} : 'linear';
		
		KEffects.easeOutCubic = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			// c*t/d + b
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : c * ((t = t / d - 1) * t * t + 1) + b;
			}
			return toReturn;
		} : 'ease-out';
		
		KEffects.easeOutQuart = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			
			// -c * ((t=t/d-1)*t*t*t - 1) + b
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : -c * ((t = t / d - 1) * t * t * t - 1) + b;
			}
			return toReturn;
		} : 'ease-out';
		
		KEffects.easeOutSine = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			
			// c * Math.sin(t/d * (Math.PI/2)) + b;
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : c * Math.sin(t / d * (Math.PI / 2)) + b;
			}
			return toReturn;
		} : 'ease-out';
		
		KEffects.easeOutExpo = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			
			// (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) +
			// 1) + b;
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : c * (-Math.pow(2, -10 * t / d) + 1) + b;
			}
			return toReturn;
		} : 'ease-out';
		
		KEffects.easeInExpo = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
			}
			
			return toReturn;
		} : 'ease-in';
		
		KEffects.easeInOutExpo = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			
			var t = now - time;
			var d = duration;
			var n_t = (t / (d / 2));
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				
				if (stop) {
					toReturn.value[index] = targetValue[index];
				}
				else if (t == 0) {
					toReturn.value[index] = b;
				}
				else if (t == d) {
					toReturn.value[index] = b + c;
				}
				else if (n_t < 1) {
					toReturn.value[index] = c / 2 * Math.pow(2, 10 * (n_t - 1)) + b;
				}
				else {
					toReturn.value[index] = c / 2 * (-Math.pow(2, -10 * --n_t) + 2) + b;
				}
			}
			
			return toReturn;
		} : 'ease-in-out';
		
	};
	
	KEffects.installEffect('background-move', 'KEffectBackgroundMove');
	KEffects.installEffect('drag', 'KEffectDrag');
	KEffects.installEffect('fade', 'KEffectFade');
	KEffects.installEffect('font-resize', 'KEffectFontResize');
	KEffects.installEffect('move', 'KEffectMove');
	KEffects.installEffect('resize', 'KEffectResize');
	KEffects.installEffect('rgb', 'KEffectRGB');
	KEffects.installEffect('window-scroll-to', 'KEffectWindowScrollTo');
	
	KSystem.included('KEffects');
}
