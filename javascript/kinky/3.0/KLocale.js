function KLocale() {
	this.translations = {};
	this.failures = '';
}
{
	KLocale.prototype = {};
	KLocale.prototype.constructor = KLocale;
	
	KLocale.set = function(lang, key, value) {
		if (!KLocale.speeky.translations[lang]) {
			KLocale.speeky.translations[lang] = {};
		}
		KLocale.speeky.translations[lang][key] = value;
	};
	
	KLocale.get = function(lang, key) {
		if (!KLocale.speeky.translations[lang] || !KLocale.speeky.translations[lang][key]) {
			KLocale.speeky.failures += 'settext(\'' + key + '\', \'\', \'' + lang + '\');\n';
			return key;
		}
		return KLocale.speeky.translations[lang][key];
	};
	
	KLocale.speeky = new KLocale();
	KLocale.LANG = 'pt';
	KLocale.LOCALE = 'pt_PT';
	KLocale.DATE_FORMAT = 'Y-M-d H:i';

	if (!!window.top && !!window.top.document && !!window.top.document.location) {
		var uriparts = window.top.document.location.href.split('/');
		for(var index = uriparts.length - 1; index != 0; index--) {
			if (uriparts[index].length == 2) {
				KLocale.LANG = uriparts[index];
			}
		}
	}

	KSystem.included('KLocale');
}

function gettext(key, lang) {
	if (!lang) {
		lang = KLocale.LANG;
	}
	return KLocale.get(lang, key);
}

function settext(key, value, lang) {
	return KLocale.set(lang, key, value);
}

function getlocale(obj, field, locale) {
	if (!locale) {
		locale = KLocale.LOCALE;
	}
	if (!!obj.locales) {
		var value = undefined;
		try {
			eval('value = obj.locales[locale]' + (!!field ? '.' + field : ''));
		}
		catch(e) {}
		
		return value;
	}
	return undefined;
}

