function KTree(parent) {
	if (parent) {
		KPanel.call(this, parent);
		this.addCSSClass('KTree');
		this.toggleImgClose = null;
		this.toggleImgOpen = null;
	}
}

KSystem.include( [ 'KPanel' ], function() {
	KTree.prototype = new KPanel();
	KTree.prototype.constructor = KTree;

	KTree.prototype.draw = function() {
		KPanel.prototype.draw.call(this);
	};

	KTree.prototype.createPageTree = function(subPages, level) {

		var toReturn = document.createElement('div');
		toReturn.style.display = (level > 1 ? 'none' : 'block');

		if (subPages.length > 0) {

			for ( var indexSubPages in subPages) {
				var elementUl = window.document.createElement('ul');
				var elementLI = window.document.createElement('li');

				if (subPages[indexSubPages].pages.length > 0) {
					var toggleImg = window.document.createElement('img');
					toggleImg.src = this.toggleImgClose;
					toggleImg.addEventListener('click', function(e) {
						KTree.toggleDiv(e);
					}, false);
					elementLI.appendChild(toggleImg);
				}

				var elementParentLink = window.document.createElement('a');
				elementParentLink.href = '#' + subPages[indexSubPages].urlHashText;
				elementParentLink.appendChild(document.createTextNode(subPages[indexSubPages].titleText));
				elementLI.appendChild(elementParentLink);

				elementUl.appendChild(elementLI);

				if (subPages[indexSubPages].pages.length > 0) {
					elementUl.appendChild(this.createPageTree(subPages[indexSubPages].pages, level + 1));
				}

				toReturn.appendChild(elementUl);
			}
		}
		return toReturn;
	};

	KTree.toggleDiv = function(event) {
		var element = KDOM.getEventTarget(event);
		var widget = KDOM.getEventWidget(event);

		if (element.parentNode.parentNode.getElementsByTagName('div')[0].style.display == 'block') {
			element.parentNode.getElementsByTagName('img')[0].src = widget.toggleImgClose;
			element.parentNode.parentNode.getElementsByTagName('div')[0].style.display = 'none';
		} else {
			element.parentNode.getElementsByTagName('img')[0].src = widget.toggleImgOpen;
			element.parentNode.parentNode.getElementsByTagName('div')[0].style.display = 'block';
		}
	};

	KSystem.included('KTree');
});