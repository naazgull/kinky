function KFileDropPanel(parent) {
	if (parent != null) {
		KDropPanel.call(this, parent);
	}
}

KSystem.include([
	'KDropPanel'
], function() {
	KFileDropPanel.prototype = new KDropPanel();
	KFileDropPanel.prototype.constructor = KFileDropPanel;
	
	KFileDropPanel.prototype.draw = function() {
		KPanel.prototype.draw.call(this);
		this.addEventListener('dragover', KFileDropPanel.preventDefault, false);
		this.addEventListener('dragenter', KFileDropPanel.preventDefault, false);
		this.addEventListener('dragleave', KFileDropPanel.preventDefault, false);
		this.addEventListener('drop', KFileDropPanel.droppedFile, false);
	};
	
	KFileDropPanel.prototype.onDrop = function(fileDropped) {
		this.kinky.save(this, KFileDropPanel.BASE_UPLOAD_SERVICE, fileDropped);
	};
	
	KFileDropPanel.prototype.onSave = function(uploadedFile) {
		for ( var file in uploadedFile.files) {
			this.droppedData[uploadedFile.files[file].name] = uploadedFile.files[file];
			
			var clone = window.document.createElement('div');
			clone.className = ' KFileDropped ';
			clone.style.position = 'relative';
			
			var mime = uploadedFile.files[file].mime.split('/');
			var image = window.document.createElement('img');
			image.src = (mime[0] == 'image' ? uploadedFile.files[file].uploadURL : 'images/mime/' + uploadedFile.files[file].mime.replace(/\//g, '-') + '.png');
			image.alt = image.title = uploadedFile.files[file].name;
			clone.appendChild(image);
			
			var removeButton = window.document.createElement('button');
			removeButton.className = ' KFileDropPanelRemoveButton ';
			removeButton.name = uploadedFile.files[file].name;
			removeButton.setAttribute('type', 'button');
			removeButton.appendChild(window.document.createTextNode('\u00d7'));
			clone.style.overflow = 'visible';
			clone.appendChild(removeButton);
			KDOM.addEventListener(removeButton, 'click', KDropPanel.removeElement);
			
			this.childDiv(KWidget.CONTENT_DIV).appendChild(clone);
		}
	};
	
	KFileDropPanel.preventDefault = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		if (mouseEvent.preventDefault) {
			mouseEvent.stopPropagation();
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
	};
	
	KFileDropPanel.droppedFile = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		if (mouseEvent.preventDefault) {
			mouseEvent.stopPropagation();
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		
		var widget = KDOM.getEventWidget(event);
		var mouseEvent = KDOM.getEvent(event);
		var params = {
			length : 0,
			files : []
		};
		for ( var i = 0; i != mouseEvent.dataTransfer.files.length; i++) {
			var droppedFile = mouseEvent.dataTransfer.files[i];
			if (!widget.droppedData[droppedFile.fileName]) {
				params.length++;
				KFileDropPanel.addFile(widget, droppedFile, params);
			}
		}
	};
	
	KFileDropPanel.addFile = function(widget, droppedFile, params) {
		var file = {
			name : droppedFile.fileName,
			size : droppedFile.fileSize,
			mime : droppedFile.type
		};
		var reader = new FileReader();
		reader.onload = function(evt) {
			var fileEvent = KDOM.getEvent(evt);
			file.rawData = fileEvent.target.result.split(',')[1];
			params.files.push(file);
			if (params.files.length == params.length) {
				widget.onDrop(params);
			}
		};
		reader.readAsDataURL(droppedFile);
	};
	
	KFileDropPanel.BASE_UPLOAD_SERVICE = null;
	
	KSystem.included('KFileDropPanel');
});