function KDropPanel(parent) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.droppedData = {};
	}
}

KSystem.include([
	'KDraggable',
	'KPanel'
], function() {
	KDropPanel.prototype = new KPanel();
	KDropPanel.prototype.constructor = KDropPanel;
	
	KDropPanel.prototype.draw = function() {
		KPanel.prototype.draw.call(this);
		this.addEventListener('mouseover', KDropPanel.onMouseOver);
		this.addEventListener('mousemove', KDropPanel.onMouseOver);
		this.addEventListener('mouseout', KDropPanel.onMouseOut);
		this.addEventListener('mouseup', KDropPanel.onMouseOut);
	};
	
	KDropPanel.prototype.getDropped = function() {
		return this.droppedData;
	};
	
	KDropPanel.prototype.reset = function() {
		var h2 = null;
		if (this.content.getElementsByTagName('h2').length != 0) {
			h2 = this.content.getElementsByTagName('h2')[0];
		}
		this.content.innerHTML = '';
		if (!!h2) {
			this.content.appendChild(h2);
		}
		delete this.droppedData;
		this.droppedData = new Object();
	};
	
	KDropPanel.prototype.onDrop = function(widgetDropped) {
		this.droppedData[widgetDropped.id] = widgetDropped.data;
		var clone = null;
		if (!!widgetDropped.prepareDrop) {
			clone = widgetDropped.prepareDrop(this);
		}
		else {
			clone = widgetDropped.panel.cloneNode(true);
		}
		clone.name = widgetDropped.id;
		clone.id = null;

		var removeButton = window.document.createElement('button');
		removeButton.className = ' KDropPanelRemoveButton ';
		removeButton.name = widgetDropped.id;
		removeButton.setAttribute('type', 'button');
		removeButton.appendChild(window.document.createTextNode('\u00d7'));
		clone.style.overflow = 'visible';
		clone.appendChild(removeButton);
		KDOM.addEventListener(removeButton, 'click', KDropPanel.removeElement);

		KDOM.addEventListener(clone, 'mousedown', function(event) {
			clone.id = clone.name;
		});
		KDOM.addEventListener(clone, 'mouseup', function(event) {
			clone.id = null;
		});
		KEffects.addEffect(clone, {
			type : 'drag',
			dragndrop : true,
			applyToElement : true,
			onStart : function(widget, tween) {
				KDraggable.dragging = true;
			},
			onComplete : function(widget, tween) {
				tween.original.target.parentNode.removeChild(tween.original.target);
				widget.dropped();
				KDraggable.dragging = false;
			}
		}, widgetDropped);
		
		this.childDiv(KWidget.CONTENT_DIV).appendChild(clone);
	};
	
	KDropPanel.removeElement = function(event) {
		var target = KDOM.getEventTarget(event);
		var widget = KDOM.getEventWidget(event, 'KDropPanel');
		
		widget.content.removeChild(target.parentNode);
		delete widget.droppedData[target.name];
	};
	
	KDropPanel.onMouseOver = function(event) {
		var widget = KDOM.getEventWidget(event);
		KDraggable.target = widget;
		if (KDraggable.dragging) {
			widget.addCSSClass(widget.className + 'Over');
		}
	};
	
	KDropPanel.onMouseOut = function(event) {
		var widget = KDOM.getEventWidget(event);
		var relTarget = KDOM.getEventRelatedTarget(event);
		var relWidget = KDOM.getWidget(relTarget);
		
		if (!KDraggable.dragging || ((relWidget != null) && (widget.id != relWidget.id) && (KDraggable.target.id == widget.id))) {
			KDraggable.target = null;
		}
		if (!!widget) {
			widget.removeCSSClass(widget.className + 'Over');
		}
	};
	
	KDropPanel.prototype.droppedIt = function(widgetDropped) {		
		if ((!this.droppedData[widgetDropped.id])) {
			this.onDrop(widgetDropped);
		}
	};
	
	KSystem.included('KDropPanel');
});