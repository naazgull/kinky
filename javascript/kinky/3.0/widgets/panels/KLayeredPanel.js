function KLayeredPanel(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.layout = KLayeredPanel.LINEAR;
		this.selected = null;
		this.addActionListener(KLayeredPanel.actions);
		
		this.title = window.document.createElement('h2');
		this.panel.insertBefore(this.title, this.content);
		
		this.nPanels = 0;
	}
}

KSystem.include([
	'KWidget'
], function() {
	KLayeredPanel.prototype = new KWidget();
	KLayeredPanel.prototype.constructor = KLayeredPanel;
	
	KLayeredPanel.prototype.setTitle = function(title, isHTML) {
		this.title.innerHTML = title;
	};
	
	KLayeredPanel.prototype.appendTitleText = function(title, isHTML) {
		this.title.innerHTML += title;
	};
	
	KLayeredPanel.prototype.focus = function(dispatcher) {
		this.onChangeTab(dispatcher.hash);
		KWidget.prototype.focus.call(this, this);
	};
	
	KLayeredPanel.prototype.visible = function() {
		KWidget.prototype.visible.call(this);
		if (!!this.def) {
			var _this = this;
			KSystem.addTimer(function()  {
				KBreadcrumb.dispatchEvent(_this.id, {
					action : _this.def
				});
				_this.def = null;
			}, (!!this.getParent('KLayeredPanel') ? 300 : 100));
		}
	};

	KLayeredPanel.prototype.visibleTab = function(child) {
	};	
	
	KLayeredPanel.prototype.invisibleTab = function(child) {
	};	
	
	KLayeredPanel.prototype.draw = function() {
		if (!!this.header) {
			var width = 0;
			for ( var n = 0; n != this.header.childNodes.length; n++) {
				width += this.header.childNodes[n].offsetWidth;
			}
			var computed = this.header.offsetWidth;
			var offset = computed - width;
			
			if (offset < 0) {
				offset -= 40;
				var height = '0px';
				
				this.headerclipper = window.document.createElement('div');
				{
					KCSS.setStyle({
						position : 'relative',
						width : width + 'px',
						top : '0',
						left : '0px'
					}, [
						this.headerclipper
					]);
					height = this.header.offsetHeight + 'px';
					for (; this.header.childNodes.length != 0;) {
						this.headerclipper.appendChild(this.header.childNodes[0]);
					}
				}
				
				var outter = window.document.createElement('div');
				{
					KCSS.setStyle({
						position : 'absolute',
						overflow : 'hidden',
						top : '0',
						left : '20px',
						width : (this.header.offsetWidth - 40) + 'px',
						height : height
					}, [
						outter
					]);
				}
				
				this.header.appendChild(outter);
				outter.appendChild(this.headerclipper);
				
				KCSS.setStyle({
					height : height
				}, [
					this.header
				]);
				
				var back = window.document.createElement('button');
				{
					back.className = 'KLayeredPantlTabNavBack';
					back.innerHTML = '&lt;';
					KCSS.setStyle({
						width : '20px',
						height : height,
						position : 'absolute',
						top : '0',
						left : '0'
					}, [
						back
					]);
					var backtimer = 0;
					KDOM.addEventListener(back, 'mousedown', function(event) {
						var clipper = KDOM.getEventWidget(event).headerclipper;
						backtimer = KSystem.addTimer(function() {
							var left = KDOM.normalizePixelValue(clipper.style.left);
							if (left < 0) {
								clipper.style.left = (Math.abs(left) > 5 ? left + 5 : 0) + 'px';
							}
							else {
								KSystem.removeTimer(forwardimer);
							}
						}, 10, true);
					});
					KDOM.addEventListener(back, 'mouseup', function(event) {
						KSystem.removeTimer(backtimer);
					});
				}
				this.header.appendChild(back);
				
				var forward = window.document.createElement('button');
				{
					forward.className = 'KLayeredPantlTabNavForward';
					forward.innerHTML = '&gt;';
					KCSS.setStyle({
						width : '20px',
						height : height,
						position : 'absolute',
						top : '0',
						right : '0'
					}, [
						forward
					]);
					var forwardimer = 0;
					KDOM.addEventListener(forward, 'mousedown', function(event) {
						var clipper = KDOM.getEventWidget(event).headerclipper;
						forwardimer = KSystem.addTimer(function() {
							var left = KDOM.normalizePixelValue(clipper.style.left);
							if (left > offset) {
								clipper.style.left = (Math.abs(left - offset) > 5 ? left - 5 : left - (left - offset)) + 'px';
							}
							else {
								KSystem.removeTimer(backtimer);
							}
						}, 10, true);
					});
					KDOM.addEventListener(forward, 'mouseup', function(event) {
						KSystem.removeTimer(forwardimer);
					});
				}
				this.header.appendChild(forward);
				
				KCSS.setStyle({
					position : 'relative'
				}, [
					this.header
				]);
				
			}
		}
		
		KWidget.prototype.draw.call(this);
	};
	
	KLayeredPanel.prototype.addHeaderSeparator = function(content, css) {
		var sep = window.document.createElement('i');
		sep.className = ' KLayeredPanelTabNavigationSeparator ' + this.className + 'TabNavigationSeparator ' + (!!css ? css + ' ' : '');
		if (!!content) {
			sep.innerHTML = content;
		}
		if (this.activated() && !!this.headerclipper) {
			this.headerclipper.appendChild(sep);
		}
		else {
			this.header.appendChild(sep);
		}
	};

	KLayeredPanel.prototype.addPanel = function(element, title, isDefault) {
		var activator = null;
		element.opened = KLayeredPanel.opened;
		
		if (this.layout == KLayeredPanel.TABS) {
			if (!this.header) {
				this.header = window.document.createElement(this.tagNames[Kinky.HTML_READY][KLayeredPanel.HEADER_DIV]);
				this.header.className = ' KLayeredPanelTabNavigation ';
				this.panel.insertBefore(this.header, this.content);
			}
			
			activator = window.document.createElement('a');
			{
				activator.href = 'javascript:void(0)';
				KDOM.addEventListener(activator, 'click', function(event) {
					var widget = KDOM.getEventWidget(event, 'KLayeredPanel');
					KBreadcrumb.dispatchEvent(widget.id, {
						action : element.hash
					});
				});
				activator.style.cursor = 'pointer';
				activator.innerHTML = title;
				this.addCSSClass('KLayeredPanelTabTitle', activator);
				element.selector = activator;
				element.setStyle({
					display : 'none'
				});
			}
			if (this.activated() && !!this.headerclipper) {
				this.headerclipper.appendChild(activator);
			}
			else {
				this.header.appendChild(activator);
			}
			element.addCSSClass('KLayeredPanelItemContent');
		}
		else {
			activator = window.document.createElement('a');
			{
				activator.href = 'javascript:void(0)';
				KDOM.addEventListener(activator, 'click', function(event) {
					var widget = KDOM.getEventWidget(event, 'KLayeredPanel');
					KBreadcrumb.dispatchEvent(widget.id, {
						action : element.hash
					});
				});
				activator.style.cursor = 'pointer';
				activator.innerHTML = title;
				this.addCSSClass('KLayeredPanelItemTitle', activator);
				element.selector = activator;
			}
			if (!!this.container) {
				this.container.appendChild(activator);
			}
			else {
				this.content.appendChild(activator);
			}
			element.setStyle({
				overflow : 'hidden',
				height : '0px'
			});
			
			element.addCSSClass('KLayeredPanelTabContent');
		}
		element.layeredPanePosition = this.nChildren;
		this.nPanels++;
		
		element.abort = function() {
			if (this.parent.layout == KLayeredPanel.LINEAR) {
				if (!!this.parent.container) {
					this.parent.container.removeChild(this.selector);
				}
				else {
					this.parent.content.removeChild(this.selector);
				}
			}
			else {
				if (!!this.parent.headerclipper) {
					this.parent.headerclipper.removeChild(this.selector);
				}
				else {
					this.parent.header.removeChild(this.selector);
				}
			}
			eval(this.className + '.prototype.abort.call(this);');
		};
		
		this.appendChild(element);
		if (this.activated()) {
			element.go();
		}
		else {
			if (isDefault) {
				this.def = element.hash;
			}
		}
	};
	
	KLayeredPanel.prototype.transition_tab_show = function(element, tween, direction) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.tab_show) {
			tween = this.data.tween.tab_show;
		}
		if (this.layout == KLayeredPanel.TABS) {
			if (!direction) {
				direction = 1;
			}
			var width = this.getWidth();
			if (!tween) {
				element.setStyle({
					left : (-width * direction) + 'px'
				});
				tween = [{
					f : KEffects.easeOutExpo,
					type : 'move',
					duration : 400,
					lock : {
						y : true
					},
					from : {
						x : -width * direction
					},
					go : {
						x : 0
					},
					onComplete : function(w, tween) {
						w.parent.visibleTab(w);
					}
				},
				{
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 800,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				}];
			}
			element.setStyle({
				display : 'block'
			});
			KEffects.addEffect(element, tween);
		}
		else {
			var height = element.content.offsetHeight;
			if (!tween) {
				element.setStyle({
					height : '0px'
				});
				tween = {
					f : KEffects.easeOutExpo,
					type : 'resize',
					duration : 300,
					lock : {
						width : true
					},
					from : {
						height : 0
					},
					go : {
						height : height
					},
					onComplete : function(w, tween) {
						w.parent.visibleTab(w);
					}
				};
			}
			KEffects.addEffect(element, tween);
			
		}
	};
	
	KLayeredPanel.prototype.transition_tab_hide = function(element, tween, direction) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.tab_hide) {
			tween = this.data.tween.tab_hide;
		}
		if (this.layout == KLayeredPanel.TABS) {
			if (!direction) {
				direction = 1;
			}
			var width = this.getWidth();
			if (!tween) {
				tween = [{
					f : KEffects.easeOutExpo,
					type : 'move',
					duration : 400,
					lock : {
						y : true
					},
					from : {
						x : 0
					},
					go : {
						x : width * direction
					},
					onComplete : function(widget, t) {
						widget.setStyle({
							display : 'none'
						});
						widget.invisible();
						widget.parent.invisibleTab(widget);
					}
				},
				{
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 200,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}];
			}
			KEffects.addEffect(element, tween);
		}
		else {
			var height = element.content.offsetHeight;
			if (!tween) {
				tween = {
					f : KEffects.easeOutExpo,
					type : 'resize',
					duration : 300,
					lock : {
						width : true
					},
					from : {
						height : height
					},
					go : {
						height : 0
					},
					onComplete : function(widget, t) {
						widget.invisible();
						widget.parent.invisibleTab(widget);
					}
				};
			}
			KEffects.addEffect(element, tween);
			
		}
	};
	
	KLayeredPanel.prototype.dispatchPrevious = function() {		
		if (!this.selected) {
			return;
		}
		var index = this.indexOf(this.childWidget(this.selected));
		var prev = undefined;
		while(index != -1 && !((prev = this.childAt(--index)) instanceof KPanel)) {
			prev = undefined;
		}
		if (!prev) {
			return;
		}

		KBreadcrumb.dispatchEvent(this.id, {
			action : prev.hash
		});
	};

	KLayeredPanel.prototype.dispatchNext = function() {		
		if (!this.selected) {
			return;
		}
		var index = this.indexOf(this.childWidget(this.selected));
		var next = undefined;
		while(index != this.nChildren && !((next = this.childAt(++index)) instanceof KPanel)) {
			next = undefined;
		}
		if (!next) {
			return;
		}

		KBreadcrumb.dispatchEvent(this.id, {
			action : next.hash
		});
	};

	KLayeredPanel.prototype.onChangeTab = function(action) {
		var child = this.childWidget(action);
		if (!!child) {
			if (this.selected == action) {
				if (this.layout == KLayeredPanel.TABS) {
					return;
				}
				
				child.removeCSSClass((this.layout == KLayeredPanel.TABS ? 'KLayeredPanelTabTitleSelected' : 'KLayeredPanelItemTitleSelected'), child.selector);
				this.transition_tab_hide(child);
				this.selected = null;
			}
			else {
				var direction = 0;
				
				var last = this.childWidget(this.selected);
				this.selected = action;
				
				child.addCSSClass((this.layout == KLayeredPanel.TABS ? 'KLayeredPanelTabTitleSelected' : 'KLayeredPanelItemTitleSelected'), child.selector);
				
				if (!!last) {
					last.removeCSSClass((this.layout == KLayeredPanel.TABS ? 'KLayeredPanelTabTitleSelected' : 'KLayeredPanelItemTitleSelected'), last.selector);
					direction = (this.indexOf(last) - this.indexOf(child) > 0 ? 1 : -1);
					
					this.transition_tab_hide(last, null, direction);
				}
				else {
					direction = 1;
				}
				this.transition_tab_show(child, null, direction);
			}
		}
	};
	
	KLayeredPanel.opened = function() {
		var lp = this.getParent('KLayeredPanel');
		return lp.selected == this.hash;
	};

	KLayeredPanel.actions = function(widget, action) {
		if (widget.childWidget(action)) {
			widget.onChangeTab(action);
		}
	};
	
	KLayeredPanel.closePanel = function(event) {
		var div = KDOM.getEventTarget(event).parentNode;
		var id = div.alt;
		var pane = KDOM.getEventWidget(event);
		pane.removeChild(pane.childWidget(id));
		div.parentNode.removeChild(div);
	};
	
	KLayeredPanel.TABS = 1;
	KLayeredPanel.LINEAR = 2;
	KLayeredPanel.HEADER_DIV = 'header';
	
	KLayeredPanel.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div',
			header : 'div'
		},
		html5 : {
			panel : 'section',
			content : 'div',
			background : 'div',
			header : 'header'
		}
	};
	
	KSystem.included('KLayeredPanel');
});