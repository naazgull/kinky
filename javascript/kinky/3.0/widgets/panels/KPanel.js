function KPanel(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.isLeaf = false;
	}
}

KSystem.include([
	'KWidget'
], function() {
	KPanel.prototype = new KWidget();
	KPanel.prototype.constructor = KPanel;
	
	KPanel.prototype.addElement = function(element) {
		if (element instanceof KWidget) {
			this.appendChild(element);
		}
		else {
			throw new KNoExtensionException(this, "addElement");
		}
	};
	
	KSystem.included('KPanel');
});