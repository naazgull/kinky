function KButton(parent, label, id, callback) {
	if (parent != null) {
		KWidget.call(this, parent);
		
		this.callback = callback;
		this.label = label;
		
		this.input = window.document.createElement('button');
		this.input.className = ' KButtonInput ';
		this.input.setAttribute('type', 'button');
		this.inputID = this.input.id = id;
		
		KDOM.addEventListener(this.input, 'click', callback);
		
		var labelDiv = window.document.createElement('div');
		labelDiv.className = ' KButtonInputLabel ';
		labelDiv.appendChild(window.document.createTextNode(label));
		this.input.appendChild(labelDiv);
		
		this.INPUT_ELEMENT = 'input';
	}
}

KSystem.include([
	'KTooltip',
	'KBaloon',
	'KWidget'
], function() {
	KButton.prototype = new KWidget();
	KButton.prototype.constructor = KButton;
	KButton.prototype.isLeaf = true;
	
	KButton.prototype.getInputID = function() {
		return this.inputID;
	};
	
	KButton.prototype.focus = function(dispatcher) {
		if (!!dispatcher) {
			return;
		}
		//KDOM.fireEvent(this.input, 'focus');
		this.input.focus();
		this.parent.focus(this);
	};

	KButton.prototype.enable = function() {
		this.input.disabled = '';
	};
	
	KButton.prototype.disable = function() {
		this.input.disabled = 'disabled';
	};
	
	KButton.prototype.go = function() {
		this.gone = true;
		if (!this.activated()) {
			this.draw();
		}
		else {
			this.redraw();
		}
	};
	
	KButton.prototype.setCallback = function(callback) {
		KDOM.removeEventListener(this.input, 'click', this.callback);
		KDOM.addEventListener(this.input, 'click', callback);
		this.callback = callback;
	};
	
	KButton.prototype.setText = function(text, html) {
		var labelDiv = this.input.childNodes[0];
		labelDiv.innerHTML = '';
		if (html) {
			labelDiv.innerHTML = text;
		}
		else {
			labelDiv.appendChild(window.document.createTextNode(text));
		}
	};
	
	KButton.prototype.getText = function(html) {
		if (html) {
			return this.input.childNodes[0].innerHTML;
		}
		else {
			return this.input.childNodes[0].textContent;
		}
	};
	
	KButton.prototype.setHelpText = function(text) {
		this.helpText = text;
	};
	
	KButton.prototype.draw = function() {
		if (this.helpText) {
			KTooltip.make(this, {
				text : this.helpText,
				offsetX : 15,
				offsetY : 25
			});
		}
		
		this.content.appendChild(this.input);
		this.activate();
	};
	
	KButton.BUTTON_ELEMENT = 'input';
	
	KSystem.included('KButton');
});