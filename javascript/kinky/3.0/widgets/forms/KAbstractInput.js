function KAbstractInput(parent, label, id) {
	if (/^widget([0-9]+)$/.test(id)) {
		throw Error('KAbstractInput: input id cannot be of the form "widget([0-9]+)".');
	}
	if (parent != null) {
		KWidget.call(this, parent);
		this.addCSSClass('KAbstractInput');
		
		this.labelText = label;
		this.inputID = id;
		this.validations = new Array();
		this.validationIndex = null;
		this.errorArea = null;
		this.noEncryption = false;
		this.inputListeners = new Array();
		this.onEnterSubmit = true;
		this.parentForm = null;
		
		this.input = window.document.createElement('input');
		{
			this.input.setAttribute('type', 'hidden');
			this.input.id = this.input.name = this.inputID + "Value";
		}
		this.panel.appendChild(this.input);
		
		this.innerLabel = window.document.createElement('div');
		{
			KCSS.setStyle({
				position : 'absolute',
				top : '0',
				left : '0'
			}, [
				this.innerLabel
			]);
		}
		
		this.inputContainer = null;
		this.addEventListener('blur', KAbstractInput.onBlur);
		this.addEventListener('blur', KAbstractInput.onBlur);
	}
}

KSystem.include([
	'SHA1',
	'HTMLEntities',
	'KWidget'
], function() {
	KAbstractInput.prototype = new KWidget();
	KAbstractInput.prototype.constructor = KAbstractInput;
	
	KAbstractInput.prototype.validate = function() {
		if (this.validations.length != 0) {
			var value = this.getValue(true);
			value = !!value ? value : '';
			if ((typeof value == 'object') || (value instanceof Array)) {
				value = JSON.stringify(value);
			}
			for ( var index in this.validations) {
				if (!!this.validations[index].operator && this.validations[index].options.comparison) {
					var result = true;
					if (this.validations[index].options.comparison instanceof KAbstractInput) {
						var cvalue = this.validations[index].options.comparison.getValue();
						if (!!cvalue) {
							eval('result = (value ' + this.validations[index].operator + ' cvalue)');
						}
					}
					else {
						eval('result = (value ' + this.validations[index].operator + ' this.validations[index].options.comparison)');
					}
					
					if (!result) {
						this.validationIndex = index;
						this.addCSSClass('KAbstractInputError');
						return this.validations[index].message;
					}					
				}
				else if (!this.validations[index].regex.test(value)) {
					this.validationIndex = index;
					this.addCSSClass('KAbstractInputError');
					return this.validations[index].message;
				}
				
				if (!!this.errorArea) {
					this.errorArea.innerHTML = '';
				}
				this.removeCSSClass('KAbstractInputError');
			}
		}
		return true;
	};
	
	KAbstractInput.prototype.addValidator = function(test, message, options) {
		if (typeof(test) == 'string' && (!!options && !!options.comparison)) {
			this.validations.push({
				operator : test,
				message : message,
				options : options
			});
		}
		else if (!(test instanceof RegExp)) {
			if (typeof test != 'string') {
				return;
			}
			test = new RegExp(test);
			this.validations.push({
				regex : test,
				message : message,
				options : options
			});
		}
		else {
			this.validations.push({
				regex : test,
				message : message,
				options : options
			});
		}
	};
	
	KAbstractInput.prototype.removeValidator = function(test) {
		for ( var index in this.validations) {
			if (this.validations[index].regex.toString() == test.toString()) {
				delete this.validations[index];
			}
		}
	};
	
	KAbstractInput.prototype.addEventListener = function(eventName, callback, target) {
		if (!target) {
			target = this.INPUT_ELEMENT;
		}
		if (!this.activated()) {
			this.inputListeners.push({
				event : eventName,
				callback : callback,
				target : target
			});
		}
		else {
			KWidget.prototype.addEventListener.call(this, eventName, callback, target);
		}
	};
	
	KAbstractInput.prototype.addEventListeners = function() {
		for ( var index in this.inputListeners) {
			KWidget.prototype.addEventListener.call(this, this.inputListeners[index].event, this.inputListeners[index].callback, this.inputListeners[index].target);
		}
	};
	
	KAbstractInput.prototype.focus = function(dispatcher) {
		var inputChildDiv = this.childDiv(this.INPUT_ELEMENT);
		this.parent.focus(this);
		if ((inputChildDiv.style.display != 'none') && (inputChildDiv.style.visibility != 'hidden') && !inputChildDiv.disabled && (inputChildDiv.type != 'hidden') && (KCSS.getComputedStyle(inputChildDiv).offsetWidth != 0)) {
			inputChildDiv.focus();
			//this.scrollIntoView();
			var v = inputChildDiv.value;
			inputChildDiv.value = '';
			inputChildDiv.value = v;
		}
	};
	
	KAbstractInput.prototype.getInputID = function() {
		return this.inputID;
	};
	
	KAbstractInput.prototype.getInput = function() {
		return this.childDiv(this.INPUT_ELEMENT);
	};
	
	KAbstractInput.prototype.setHelp = function(text, baloon) {
		this.helpText = text;
		if (!baloon) {
			this.helpParams = {
				gravity : {
					x : 'left',
					y : 'top'
				},
				offsetX : 0,
				offsetY : 0
			};
		}
		else {
			this.helpParams = baloon;
		}
	};
	
	KAbstractInput.prototype.setLabel = function(text) {
		this.labelText = text;
		if (this.activated()) {
			var label = this.content.getElementsByTagName('label')[0];
			label.removeChild(label.childNodes[0]);
			if (label.childNodes.length != 0) {
				label.insertBefore(window.document.createTextNode(text), label.childNodes[label.childNodes.length - 1]);
			}
			else {
				label.appendChild(window.document.createTextNode(text));
			}
		}
	};
	
	KAbstractInput.prototype.setInnerLabel = function(text) {
		this.innerLabel.innerHTML = text;
		this.innerLabel.className = 'InnerLabel';
		KDOM.addEventListener(this.innerLabel, 'click', KAbstractInput.onFocus);
	};
	
	KAbstractInput.prototype.getErrorMessage = function(index) {
		return this.validations[index].message;
	};
	
	KAbstractInput.prototype.showErrorMessage = function(params) {
		if (!params || !params.tooltip) {
			this.errorArea.innerHTML = '';
			if (params && params.isHtml) {
				this.errorArea.innerHTML += '* ' + (this.validationIndex ? this.getErrorMessage(this.validationIndex) : '');
			}
			else {
				this.errorArea.appendChild(window.document.createTextNode('* ' + (this.validationIndex ? this.getErrorMessage(this.validationIndex) : '')));
			}
		}
		else if (params && params.tooltip) {
			KDOM.addEventListener(this.childDiv(this.INPUT_ELEMENT), 'mouseover', KAbstractInput.onMouseOver);
		}
	};
	
	KAbstractInput.onMouseOver = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var widget = KDOM.getEventWidget(mouseEvent);
		
		KTooltip.showTooltip(widget.inputs[0], {
			text : '* ' + (widget.validationIndex ? widget.getErrorMessage(widget.validationIndex) : ''),
			isHTML : true,
			offsetX : 0,
			offsetY : 15,
			left : KDOM.mouseX(mouseEvent),
			top : (KDOM.mouseY(mouseEvent) + 15),
			cssClass : 'KAbstractInputTooltip'
		});
	};
	
	KAbstractInput.prototype.setTitle = function() {
	};
	
	KAbstractInput.prototype.go = function() {
		this.draw();
	};
	
	KAbstractInput.prototype.clear = function() {
		this.input.value = '';
	};
	
	KAbstractInput.prototype.createInput = function() {
		var input = window.document.createElement('input');
		{
			input.setAttribute('type', 'text');
			input.className = ' KAbstractInputElement ' + this.className + 'InputElement ';
			input.name = (!!this.inputID ? this.inputID : this.hash);
		}
		return input;
	};
	
	KAbstractInput.prototype.createLabel = function() {
		var label = window.document.createElement('label');
		{
			label.className = ' KAbstractInputLabelElement ' + this.className + 'LabelElement ';
			label.setAttribute('for', this.inputID);
			label.innerHTML = this.labelText;
		}
		return label;
	};
	
	KAbstractInput.prototype.createHelp = function() {
		if (!!this.helpText) {
			var help = window.document.createElement('div');
			{
				help.className = ' KAbstractInputHelpElement ' + this.className + 'HelpElement ';
				var params = {
					text : this.helpText,
					delay : 300,
					gravity : this.helpParams.gravity,
					max : this.helpParams.max,
					offsetX : this.helpParams.offsetX,
					offsetY : this.helpParams.offsetY,
					zoom : this.helpParams.zoom,
					offsetParent: help,
					target : help,
					cssClass : !!this.helpParams.cssClass ? this.helpParams.cssClass : 'KInputBaloon '
				};
				KBaloon.make(this, params);
			}
			return help;
		}
		return null;
	};
	
	KAbstractInput.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		this.inputContainer = this.inputContainer || window.document.createElement('div');

		var label = this.createLabel();
		var input = this.createInput();
		var help = this.createHelp();
		
		this.inputContainer.appendChild(input);
		this.inputContainer.appendChild(this.innerLabel);
		
		this.content.appendChild(label);
		this.content.appendChild(this.inputContainer);
		if (!!help) {
			label.appendChild(help);
		}
		
		this.errorArea = window.document.createElement('div');
		this.errorArea.className = 'KAbstractInputErrorArea ' + this.className + 'ErrorArea';
		this.content.appendChild(this.errorArea);
		
		if (this.onEnterSubmit) {
			this.addEventListener('keyup', KAbstractInput.onEnterPressed, this.childDiv(this.INPUT_ELEMENT));
		}
		this.addEventListener('keydown', KAbstractInput.onTabPressed, this.childDiv(this.INPUT_ELEMENT));
		
		this.addEventListeners();
		this.activate();
	};
	
	KAbstractInput.prototype.selectNextWidget = function(up) {
		var index = this.parent.indexOf(this);
		var next = undefined;

		if (up) {
			if (index == 0) {
				next = this.parent.childAt(this.parent.nChildren - 1);
			}
			else {
				next = this.parent.childAt(index - 1);
			}
		}
		else {
			if (index < this.parent.nChildren - 1) {
				next = this.parent.childAt(index + 1);
			}
			else {
				next = this.parent.childAt(0);
			}
		}

		if (next instanceof KFile) {
			return next.selectNextWidget(up);
		}
		if (next instanceof KHidden) {
			return next.selectNextWidget(up);
		}
		return next;
	};
	
	KAbstractInput.prototype.actOnEnter = function() {
		return true;
	};
	
	KAbstractInput.onEnterPressed = function(event) {
		var widget = KDOM.getEventWidget(event);
		if (widget.actOnEnter() && KSystem.isIncluded('KForm') && widget.parent instanceof KForm) {
			KDOM.stopEvent(event);
			if (KForm.onEnterPressed(event)) {
				KForm.goSubmit(event);
			}
		}
	};
	
	KAbstractInput.onTabPressed = function(event) {
		event = KDOM.getEvent(event);
		var keyCode = event.keyCode || event.which;
		if (keyCode == 9) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event);
			var next = widget.selectNextWidget(event.shiftKey);
			if (!!next) {
				next.focus();
			}
			return;
		}
	};

	KAbstractInput.onFocus = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		mouseEvent.returnValue = false;

		if (KSystem.isIncluded('KScroll') && KScroll.activeScroll) {
			KScroll.disableKeys();
		}

		var widget = KDOM.getEventWidget(event);
		if (widget.innerLabel.innerHTML != '') {
			widget.innerLabel.style.display = 'none';
			widget.focus();
		}
	};
	
	KAbstractInput.onBlur = function(event) {
		if (KSystem.isIncluded('KScroll') && KScroll.activeScroll) {
			KScroll.enableKeys();
		}
		var widget = KDOM.getEventWidget(event);
		if ((widget.innerLabel.innerHTML != '') && (widget.getValue() == '')) {
			widget.innerLabel.style.display = 'block';
		}
	};
	
	KAbstractInput.prototype.getValue = function() {
		if (this.input.value != '') {
			return JSON.parse(this.input.value);
		}
		else {
			return '';
		}
	};
	
	KAbstractInput.prototype.setValue = function(value) {
		if (value === undefined || value === null) {
			this.input.value = '';
		}
		else {
			this.input.value = JSON.stringify(value);
		}
	};
	
	KAbstractInput.prototype.INPUT_ELEMENT = 'input';
	KAbstractInput.LABEL_CONTAINER = 'label';
	KAbstractInput.INPUT_ERROR_CONTAINER = 'errorArea';
	
	KSystem.included('KAbstractInput');
});