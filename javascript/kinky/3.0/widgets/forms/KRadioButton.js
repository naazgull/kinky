function KRadioButton(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		
		this.selectedOption = null;
		
		this.radiobutton = window.document.createElement('a');
		this.radiobutton.href = 'javascript:void(0)';
		this.radiobutton.style.cursor = 'normal';
		this.radiobutton.className = ' KRadioButtonOptionsContent ';
		this.radiobutton.style.overflow = 'visible';
		this.radiobutton.style.position = 'relative';
		
		this.INPUT_ELEMENT = 'radiobutton';
		
		this.lastElement = null;
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KRadioButton.prototype = new KAbstractInput();
	KRadioButton.prototype.constructor = KRadioButton;
	
	KRadioButton.prototype.clear = function() {
		this.selectedOption = null;
		this.setValue(null);
		this.radiobutton.innerHTML = '';
		KAbstractInput.prototype.clear.call(this);
	};
	
	KRadioButton.prototype.addOption = function(value, caption, selected) {
		var checkBoxItemContainer = window.document.createElement('div');
		checkBoxItemContainer.className = ' KRadioButtonOption KRadioButtonClickableOption ';
		
		var check = window.document.createElement('button');
		check.src = selected ? this.imageOn : this.image;
		check.innerHTML = '&nbsp;';
		check.setAttribute('type', 'button');
		check.value = JSON.stringify(value);
		check.id = this.inputID + this.radiobutton.childNodes.length;
		checkBoxItemContainer.appendChild(check);
		
		var label = window.document.createElement('label');
		label.setAttribute('for', check.id);
		label.alt = check.value;
		label.name = check.id;
		label.innerHTML = caption;
		
		KDOM.addEventListener(checkBoxItemContainer, 'click', KRadioButton.selectOption);
		
		KDOM.addEventListener(checkBoxItemContainer, 'mouseover', KRadioButton.mouseOver);
		KDOM.addEventListener(checkBoxItemContainer, 'mouseout', KRadioButton.mouseOut);
		checkBoxItemContainer.appendChild(label);
		this.radiobutton.appendChild(checkBoxItemContainer);
		
		if (selected) {
			this.setValue(value);
		}
		
		return true;
	};
	
	KRadioButton.prototype.addEventListener = function(eventName, callback, target) {
		if (eventName == 'change') {
			eventName = 'propertychange';
		}
		KAbstractInput.prototype.addEventListener.call(this, eventName, callback, target);
	};

	KRadioButton.prototype.createInput = function() {
		return this.radiobutton;
	};
	
	KRadioButton.prototype.check = function(idx) {
		KDOM.fireEvent(this.radiobutton.childNodes[idx], 'click');
	};
	
	KRadioButton.prototype.uncheck = function(idx) {
		KDOM.fireEvent(this.radiobutton.childNodes[idx], 'click');
	};
	
	KRadioButton.prototype.select = function(element, force) {
		if (!element) {
			this.selectedOption = null;
			this.input.value = '';
			for ( var i = 0; i != this.childDiv(this.INPUT_ELEMENT).childNodes.length; i++) {
				var child = this.childDiv(this.INPUT_ELEMENT).childNodes[i];
				child.childNodes[0].innerHTML = '&nbsp;';
			}
			return;
		}
		
		if (element.tagName.toLowerCase() == 'label') {
			element = element.previousSibling;
		}
		if (element.tagName.toLowerCase() == 'div') {
			element = element.childNodes[0];
		}
		
		if (element.parentNode.className.indexOf('KRadioButtonOptionSelected') != -1) {
			if (!force) {
				this.selectedOption = null;
				KCSS.removeCSSClass('KRadioButtonOptionSelected', element.parentNode);
				element.innerHTML = '&nbsp;';
			}
		}
		else {
			for ( var i = 0; i != this.childDiv(this.INPUT_ELEMENT).childNodes.length; i++) {
				var child = this.childDiv(this.INPUT_ELEMENT).childNodes[i];
				KCSS.removeCSSClass('KRadioButtonOptionSelected', child);
			}
			KCSS.addCSSClass('KRadioButtonOptionSelected', element.parentNode);
			this.selectedOption = JSON.parse(element.value);
			element.innerHTML = '&nbsp;';
			
		}
		
		KDOM.fireEvent(this.childDiv(this.INPUT_ELEMENT), 'propertychange');
	};
	
	KRadioButton.prototype.setValue = function(value) {
		if (!value) {
			delete this.selectedOption;
			this.selectedOption = null;
		}
		else {
			this.selectedOption = value;
		}
		KAbstractInput.prototype.setValue.call(this, value);
		
		for ( var i = 0; i != this.childDiv(this.INPUT_ELEMENT).childNodes.length; i++) {
			var child = this.childDiv(this.INPUT_ELEMENT).childNodes[i];
			if (child.childNodes[0].value == this.input.value) {
				this.select(child.childNodes[0], true);
				break;
			}
		}
	};
	
	KRadioButton.prototype.getValue = function() {
		if (!!this.selectedOption) {
			return this.selectedOption;
		}
		else {
			return '';
		}
	};
	
	KRadioButton.mouseOver = function(event) {
		var element = KDOM.getEventTarget(event);
		while(element.className.indexOf('KRadioButtonClickableOption') == -1) {
			element = element.parentNode;
		}		
		if (!element) {
			return;
		}
		if (element.className.indexOf('KRadioButtonOptionSelected') == -1) {
			KCSS.addCSSClass('KRadioButtonOptionOver', element);
		}
	};
	
	KRadioButton.mouseOut = function(event) {
		var element = KDOM.getEventTarget(event);		
		while(element.className.indexOf('KRadioButtonClickableOption') == -1) {
			element = element.parentNode;
		}		
		if (!element) {
			return;
		}
		KCSS.removeCSSClass('KRadioButtonOptionOver', element);
	};
	
	KRadioButton.selectOption = function(event) {
		KDOM.stopEvent(event);

		var element = KDOM.getEventTarget(event);		
		while(element.className.indexOf('KRadioButtonClickableOption') == -1) {
			element = element.parentNode;
		}		
		if (!element) {
			return;
		}

		var widget = KDOM.getEventWidget(event);
		widget.select(element);
	};
	
	KRadioButton.updateValue = function(event) {
		var widget = KDOM.getEventWidget(event);
		widget.onKeyPressed();
	};
	
	KRadioButton.OPTION_CONTAINER_DIV = 'radiobutton';
	KRadioButton.OPENED = null;
	
	KSystem.included('KRadioButton');
});