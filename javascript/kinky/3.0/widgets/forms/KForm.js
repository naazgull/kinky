function KForm(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.method = 'post';
		
		this.form = window.document.createElement('form');
		this.form.method = 'post';
		this.content.appendChild(this.form);
		
		this.info = window.document.createElement('div');
		this.info.className = 'KFormInfoArea';
		this.content.appendChild(this.info);
		
		this.originalContent = this.content;
		
		this.content = this.fieldset = window.document.createElement('fieldset');
		this.form.appendChild(this.content);
		
		this.action = null;
		this.stopOnFirstError = false;
		this.inputByID = new Object();
		
	}
}

KSystem.include([
	'KWidget'
], function() {
	KForm.prototype = new KWidget();
	KForm.prototype.constructor = KForm;
	
	KForm.prototype.reset = function() {
		for ( var element in this.inputByID) {
			this.inputByID[element].setValue(null);
		}
	};
	
	KForm.prototype.setDefaultData = function(data) {
		this.entered = data;
	};
	
	KForm.prototype.addInput = function(input, targetDiv) {
		if (input instanceof KAbstractInput) {
			if (input.parent.id == this.id) {
				this.appendChild(input, targetDiv);
			}
			this.inputByID[input.inputID] = input;
			if (!!this.entered) {
				var defaultValue = null;
				eval('defaultValue = this.entered.' + input.inputID + ';');
				if (!!defaultValue) {
					input.setValue(defaultValue);
				}
			}
			input.parentForm = this;
		}
		else {
			throw new KNoExtensionException(this, "addInput");
		}
	};
	
	KForm.prototype.getInput = function(inputID) {
		return this.inputByID[inputID];
	};
	
	KForm.prototype.removeInput = function(inputID) {
		var input = this.childWidget(inputID);
		if (input) {
			delete this.inputByID[inputID];
			this.removeChild(input);
		}
	};
	
	KForm.prototype.setMethod = function(method) {
		this.method = method;
	};
	
	KForm.onEnterPressed = function(event) {
		event = KDOM.getEvent(event);
		var keyCode = event.keyCode || event.which;
		if (keyCode != 13) {
			return false;
		}
		return true;
	};
	
	KForm.prototype.showInfo = function(data) {
		this.info.innerHTML = data;
	};
	
	KForm.prototype.onValidate = function(validationText, params, status) {
		throw new KNoExtensionException(this, "onValidate");
	};
	
	KForm.prototype.onSuccess = function(data, request) {
		throw new KNoExtensionException(this, "onSuccess");
	};
	
	KForm.prototype.onError = function(error) {
	};
	
	KForm.prototype.draw = function() {
		KWidget.prototype.draw.call(this);
		
		this.childAt(0).focus(this);
		KDOM.addEventListener(this.form, 'submit', function(event) {
			KForm.goSubmit(event);
			return false;
		});
	};
	
	KForm.prototype.submit = function() {
		var params = this.validate();
		if ((params = this.onValidate(this.action, params, this.validation)) != false) {
			this.kinky[this.method.toLowerCase()](this, this.action, params, null, 'onSuccess');
		}
		return false;
	};
	
	KForm.prototype.validate = function() {
		var params = new Object();
		var wrongParams = new Object();
		var isValid = true;
		
		for ( var element in this.inputByID) {
			var widget = this.inputByID[element];
			if (!(widget instanceof KAbstractInput)) {
				continue;
			}
			var validation = widget.validate();
			if (validation == true) {
				var name = widget.inputID.replace(/[\[\]]/g, '');
				var names = name.split('.');
				var current = '';
				for ( var i = 0; i != names.length - 1; i++) {
					current += '.' + names[i];
					var exists = false;
					eval('exists = !!params' + current + '');
					if (!exists) {
						eval('params' + current + ' = {}');
					}
				}
				eval('params.' + name + ' = widget.getValue()');
			}
			else {
				wrongParams[widget.inputID.replace(/[\[\]]/g, '')] = widget;
				isValid = false;
				if (this.stopOnFirstError) {
					break;
				}
			}
		}
		this.validation = isValid;
		return (isValid ? params : wrongParams);
	};
	
	KForm.prototype.showAllErrors = function(errors) {
		for ( var param in errors) {
			errors[param].showErrorMessage();
		}
	};
	
	KForm.goSubmit = function(event) {
		var target = KDOM.getEventTarget(event);
		var form = KDOM.getWidget(target.form || target);
		while (!(form instanceof KForm)) {
			form = form.parent;
		}
		var params = form.validate();
		
		if ((params = form.onValidate(form.action, params, form.validation)) != false) {
			var method = form.method.toLowerCase();
			form.kinky[(method == 'delete' ? 'remove' : method)](form, form.action, params, null, 'onSuccess');
		}
		
		return false;
	};
	
	KForm.FORM_ELEMENT = 'form';
	
	KSystem.included('KForm');
});