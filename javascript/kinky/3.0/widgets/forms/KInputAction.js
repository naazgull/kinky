function KInputAction(parent, callback, label) {
	if (parent != null) {
		KAddOn.call(this, parent);
		this.callback = callback;
		this.label = label;
		this.parent.addCSSClass('KInputActionParent');
	}
}

KSystem.include([
	'KAddOn',
	'KAbstractInput'
], function() {
	KInputAction.prototype = new KAddOn();
	KInputAction.prototype.constructor = KInputAction;
	
	KInputAction.prototype.setDialogProps = function(props) {
		this.dialogProps = props;
	};
	
	KInputAction.prototype.draw = function() {
		var kmlID = this.id;
		var actionButton = window.document.createElement('button');
		actionButton.className = ' KInputActionButton ';
		actionButton.innerHTML = this.label || gettext('$KMEDIALIST_OPEN');
		KDOM.addEventListener(actionButton, 'click', this.callback || this.default_callback);
		var self = this;
		KDOM.addEventListener(this.parent, 'keypress', function(event) {
			event = KDOM.getEvent(event);
			if (event.keyCode == 13) {
				KDOM.stopEvent(event);
				var callback = self.callback || self.default_callback;
				callback(event);
			}			
		});
		
		if (this.parent.activated()) {
			this.parent.content.appendChild(actionButton);
		}
		else {
			this.parent.container.appendChild(actionButton);
		}
	};

	KInput.prototype.default_callback = function(event) {
		KDOM.stopEvent(event);
		var input = KDOM.getEventWidget(event);
		if (input instanceof KAbstractInput) {
			input.validate();
		}
	};
	
	KSystem.registerAddOn('KInputAction');
	KSystem.included('KInputAction');
}, Kinky.ZEPP_INCLUDER_URL);