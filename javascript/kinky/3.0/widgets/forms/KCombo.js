function KCombo(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.optionsToggled = false;
		
		this.combo = window.document.createElement('a');
		{
			this.combo.id = id;
			this.combo.href = 'javascript:void(0)';
			this.combo.style.cursor = 'pointer';
			this.combo.style.textDecoration = 'none';
			this.combo.className = ' KComboValue ';
			this.combo.style.overflow = 'visible';
			this.combo.style.position = 'relative';

			KDOM.addEventListener(this.combo, 'click', KCombo.toggleOptions);
		}
		this.INPUT_ELEMENT = 'combo';
		
		this.selectedOption = null;
		this.comboText = window.document.createElement('div');
		{
			this.comboText.className = ' ' + this.className + 'Text KComboText ';
		}
		this.combo.appendChild(this.comboText);
		
		this.optionsPanel = window.document.createElement('div');
		{
			this.optionsPanel.className = ' ' + this.className + 'OptionsPanel KComboOptionsPanel ';
			this.optionsPanel.style.overflow = 'visible';
			this.optionsPanel.style.position = 'fixed';
			this.optionsPanel.name = this.id;
		}
		
		this.optionsContainer = window.document.createElement('div');
		{
			this.optionsContainer.className = ' KComboOptionsContainer_' + id.toUpperCase() + ' ' + this.className + 'OptionsContainer KComboOptionsContainer ';
			this.optionsContainer.style.overflow = 'hidden';
			this.optionsContainer.style.position = 'relative';
		}
		this.optionsPanel.appendChild(this.optionsContainer);
		
		this.optionsContent = window.document.createElement('div');
		{
			this.optionsContent.className = ' KComboOptionsContent_' + id.toUpperCase() + ' ' + this.className + 'OptionsContent KComboOptionsContent ';
			this.optionsContent.style.overflow = 'visible';
			this.optionsContent.style.position = 'absolute';
			this.optionsContent.style.left = '0';
			this.optionsContent.style.right = '0';
		}
		this.optionsContainer.appendChild(this.optionsContent);
		
		this.dropButton = window.document.createElement('button');
		{
			this.dropButton.innerHTML = '&nbsp;';
			this.dropButton.setAttribute('type', 'button');
			this.dropButton.className = ' ' + this.className + 'DropButton KComboDropButton ';

			KDOM.addEventListener(this.dropButton, 'click', KCombo.toggleOptions);
		}
		
		this.currentIndex = -1;
	}
}

KSystem.include([
	'KAbstractInput',
	'HTML'
], function() {
	KCombo.prototype = new KAbstractInput();
	KCombo.prototype.constructor = KCombo;
	
	KCombo.prototype.clear = function() {
		this.selectedOption = null;
		this.setValue(null);
		this.comboText.innerHTML = '';
		this.optionsContent.innerHTML = '';
		KAbstractInput.prototype.clear.call(this);
	};
	
	KCombo.prototype.focus = function(dispatcher) {
		KAbstractInput.prototype.focus.call(this, dispatcher);
	};
	
	KCombo.prototype.markNext = function() {
		var beginIndex = this.currentIndex;

		for ( var index = beginIndex + 1; index < this.optionsContent.childNodes.length; index++) {
			if (this.optionsContent.childNodes[index].style.display != 'none') {
				if (this.currentIndex >= 0) {
					this.removeCSSClass('KComboOptionOver', this.optionsContent.childNodes[this.currentIndex]);
				}
				this.currentIndex = index;
				KCombo.selectOption(null, this.optionsContent.childNodes[index], this, true);
				this.optionsPanel.scrollTop = this.optionsContent.childNodes[index].offsetTop;
				this.addCSSClass('KComboOptionOver', this.optionsContent.childNodes[index]);
				return this.currentIndex;
			}
		}
		return -1;
	};
	
	KCombo.prototype.markPrevious = function() {
		var beginIndex = this.currentIndex;

		for ( var index = beginIndex - 1; index > -1; index--) {
			if (this.optionsContent.childNodes[index].className && /KComboOption/.test(this.optionsContent.childNodes[index].className) && ((this.optionsContent.childNodes[index].style.display != 'none'))) {
				if (this.currentIndex >= 0) {
					this.removeCSSClass('KComboOptionOver', this.optionsContent.childNodes[this.currentIndex]);
				}
				this.currentIndex = index;
				KCombo.selectOption(null, this.optionsContent.childNodes[index], this, true);
				this.optionsPanel.scrollTop = this.optionsContent.childNodes[index].offsetTop;
				this.addCSSClass('KComboOptionOver', this.optionsContent.childNodes[index]);
				return this.currentIndex;
			}
		}
		return -1;
	};
	
	KCombo.prototype.addEventListener = function(eventName, callback, target) {
		if (eventName == 'change') {
			target = this.input;
		}
		KAbstractInput.prototype.addEventListener.call(this, eventName, callback, target);
	};
	
	KCombo.prototype.blur = function() {
		KCSS.removeCSSClass('KComboOpened', this.dropButton);
		KCSS.removeCSSClass('KComboOpened', this.content);
		KCSS.removeCSSClass('KComboFocused', this.combo);

		if (this.optionsPanel.style.opacity != '0') {
			this.optionsPanel.style.opacity = '0';
		}
		this.optionsPanel.parentNode.removeChild(this.optionsPanel);
		this.optionsToggled = false;
	};
	
	KCombo.prototype.onKeyPressed = function(event) {
		var keyCode = event.keyCode || event.which;
		switch (keyCode) {
			case 40: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markNext();
				break;
			}
			case 38: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markPrevious();
				break;
			}
			case 8: {
				KDOM.stopEvent(event);
				var text = '' + this.innerLabel.innerHTML;
				if (text.length != 0) {
					text = text.substr(0, text.length - 1);
					this.innerLabel.innerHTML = text;
					this.filter(this.innerLabel.textContent);
				}
				break;
			}
			case 18: 
			case 17: 
			case 16: {
				break;
			}
			case 9: {
				Kinky.unsetModal(widget);
				break;
			}
			case 27: {
				KDOM.stopEvent(event);
				this.innerLabel.innerHTML = '';
				this.filter(this.innerLabel.textContent);
				KCombo.toggleOptions(null, this, true);
				break;
			}
			default: {
				var key = (event.charCode != 0 ? event.charCode : keyCode);
				if (key != 0) {
					if (!this.optionsToggled) {
						KCombo.toggleOptions(null, this);
					}
					this.innerLabel.style.visibility = 'hidden';
					this.innerLabel.innerHTML += String.fromCharCode(key);
					this.filter(this.innerLabel.textContent);
				}
			}
		}
	};
	
	KCombo.prototype.filter = function(search) {
		search = search.toLowerCase();
		for ( var i = 0; i != this.optionsContent.childNodes.length; i++) {
			if (!!this.optionsContent.childNodes[i].tagName && this.optionsContent.childNodes[i].tagName.toLowerCase() == 'a') {
				var v = this.optionsContent.childNodes[i].textContent.toLowerCase();
				if (v.indexOf(search) == 0) {
					this.optionsPanel.scrollTop = this.optionsContent.childNodes[i].offsetTop;
					this.currentIndex = i;
					break;
				}
			}
		}
	};
	
	KCombo.prototype.select = function(element, noToggle) {
		if (!this.input) {
			return;
		}
		if (!!element) {
			this.selectedOption = JSON.parse(element.alt);
			if (this.input.value != element.alt) {
				KDOM.fireEvent(this.input, 'change');
			}
			if (!this.input) {
				return;
			}
			this.input.value = element.alt;
			this.comboText.innerHTML = element.innerHTML;
			this.validate();
		}
		else {
			this.selectedOption = null;
			this.input.value = '';
			this.comboText.innerHTML = '';
		}
		this.innerLabel.innerHTML = '';
		this.innerLabel.style.visibility = 'visible';
		if (!noToggle) {
			KCombo.toggleOptions(null, this, true);
		}
		
	};
	
	KCombo.prototype.addOption = function(value, caption, selected) {
		var label = window.document.createElement('a');
		label.className = ' KComboOption ';
		label.href = 'javascript:void(0)';
		label.alt = JSON.stringify(value);
		label.name = this.inputID;
		label.innerHTML = caption;
		
		KDOM.addEventListener(label, 'click', KCombo.selectOption);
		this.optionsContent.appendChild(label);
		
		if (selected) {
			this.setValue(value);
		}
	};
	
	KCombo.prototype.createLabel = function() {
		var label = KAbstractInput.prototype.createLabel.call(this);
		this.addEventListener('click', function(event) {
			var widget = KDOM.getEventWidget(event);
			widget.combo.focus();
		}, label);
		return label;
	};
	
	KCombo.prototype.createInput = function() {
		return this.combo;
	};
	
	KCombo.prototype.setValue = function(value) {
		if (!value) {
			this.select();
			return;
		}
		
		this.selectedOption = value;
		KAbstractInput.prototype.setValue.call(this, value);
		
		for ( var i = 0; i != this.optionsContent.childNodes.length; i++) {
			if (this.optionsContent.childNodes[i].alt == this.input.value) {
				this.select(this.optionsContent.childNodes[i]);
			}
		}
	};

	KCombo.prototype.getValueLabel = function() {
		if (this.selectedOption != null) {
			return this.comboText.innerHTML;
		}
		else {
			return '';
		}
	};	
	
	KCombo.prototype.getValue = function() {
		if (this.selectedOption != null) {
			return this.selectedOption;
		}
		else {
			return '';
		}
	};
	
	KCombo.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);
		
		this.inputContainer.appendChild(this.dropButton);
		this.optionsPanel.style.overflow = 'auto';
		this.optionsContainer.style.overflow = 'visible';
		this.optionsContent.style.position = 'relative';
		
		this.addEventListener('keydown', KCombo.keyPressed, this.INPUT_ELEMENT);
	};
	
	KCombo.prototype.destroy = function() {
		Kinky.unsetModal(this);
		KWidget.prototype.destroy.call(this);
	};
	
	KCombo.keyPressed = function(event) {
		event = KDOM.getEvent(event);
		var widget = KDOM.getEventWidget(event);
		widget.onKeyPressed(event);
	};
	
	KCombo.selectOption = function(event, element, widget, noToggle) {
		if (!element) {
			KDOM.stopEvent(event);
			element = KDOM.getEventTarget(event);
			widget = KDOM.getEventWidget(event);
			if (!element) {
				return;
			}
		}
		
		element = KCombo.getLabel(element);
		if (!!element && ((element.alt != null)) && ((element.alt != ''))) {
			widget.select(element, noToggle);
		}
		else {
			widget.select(null, noToggle);
		}
	};
	
	KCombo.getLabel = function(element) {
		var current = element;
		while (!!current && ((current.tagName.toLowerCase() != 'a'))) {
			if (!!current && !!current.className && /KComboOption/.test(current.className)) {
				return current.childNodes[current.childNodes.length - 1];
			}
			current = current.parentNode;
		}
		return current;
	};
	
	KCombo.toggleOptions = function(event, widget, forceClose) {
		if (!widget) {
			KDOM.stopEvent(event);
			widget = KDOM.getEventWidget(event);
		}
		
		if (widget.optionsToggled || forceClose) {
			Kinky.unsetModal(widget);
			KCSS.removeCSSClass('KComboFocused', widget.combo);
		}
		else {
			KCSS.addCSSClass('KComboOpened', widget.dropButton);
			KCSS.addCSSClass('KComboOpened', widget.content);
			widget.childDiv(KWidget.ROOT_DIV).appendChild(widget.optionsPanel);


			var top = (widget.offsetTop() - widget.scrollTop() - window.pageYOffset + widget.getHeight());

			if (widget.optionsPanel.offsetHeight > KDOM.getBrowserHeight() - top) {
				widget.optionsPanel.style.maxHeight = (KDOM.getBrowserHeight() - top - 30) + 'px';
			}

			var left = widget.offsetLeft();
			var width = widget.getWidth();
			var delta = Math.round(widget.optionsPanel.offsetHeight / 2);

			KCSS.setStyle({
				opacity : '0',
				width : width + 'px',
				top : (top - delta) + 'px',
				left : left + 'px'
			}, [
				widget.optionsPanel
			]);
			widget.optionsToggled = true;
			var opened = Kinky.getActiveModalWidget();
			if (!!opened && opened instanceof KCombo) {
				Kinky.unsetModal(opened);
			}
			Kinky.setModal(widget, true);

			KEffects.addEffect(widget.optionsPanel,  [ 
				{
					f : KEffects.easeOutExpo,
					type : 'move',
					duration : 200,
					applyToElement : true,
					lock : {
						x : true
					},
					go : {
						y : top
					},
					from : {
						y : top - delta
					}
				},
				{
					f : KEffects.easeOutExpo,
					type : 'fade',
					applyToElement : true,
					duration : 500,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				}
			]);

			var v = widget.comboText.textContent;
			if(!!v && v != '') {
				for (var i = 0; i != widget.optionsContent.childNodes.length; i++) {
					var ov = widget.optionsContent.childNodes[i].textContent;
					if (ov != '' && ov == v) {
						widget.optionsPanel.scrollTop = widget.optionsContent.childNodes[i].offsetTop;
						widget.currentIndex = i;
						return;
					}
				}
			}
			KCSS.addCSSClass('KComboFocused', widget.combo);
		}
	};

	KCombo.focused = function(e) {
		KDOM.stopEvent(e);
		var self = KDOM.getEventWidget(e);
		KCSS.addCSSClass('KComboFocused', self.combo);
	};
	
	KCombo.blured = function(e) {
		KDOM.stopEvent(e);
		KCSS.removeCSSClass('KComboFocused', KDOM.getEventWidget(e).combo);
	};
	
	KCombo.OPTIONS_DIV = 'optionsPanel';
	KCombo.OPTIONS_CONTENT_DIV = 'optionsContent';
	KCombo.OPTIONS_CONTAINER_DIV = 'optionsContainer';
	
	KSystem.included('KCombo');
});