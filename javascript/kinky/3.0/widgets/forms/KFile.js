function KFile(parent, label, id, options) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.maxFile = 1;
		this.noPrompt = true;
		this.options = options || { }
		
		this.contentContainer = window.document.createElement('div');
		
		this.fileListContainer = window.document.createElement('div');
		this.fileListContainer.className = 'KFileListContainer';
		this.fileListContainer.id = 'container' + this.id;
		
		this.button = null;
		
		this.errorArea = window.document.createElement('div');
		this.errorArea.className = 'KAbstractInputErrorArea KFileErrorArea';
		this.fileref = null;
		this.multiple = false;

		this.aditional_args = (!!this.options.aditional_args ? this.options.aditional_args : undefined);

		this.progresswin = window.document.createElement('div');
		{
			this.progresswin.className = ' KButtonProgress ';
			KCSS.setStyle({
				position: 'fixed',
				zIndex : '9999'
			}, [
			this.progresswin
			]);

			this.progressbar = window.document.createElement('div');
			{
				this.progressbar.className = ' KButtonProgressBar ';
				KCSS.setStyle({
					width : '0'
				}, [
				this.progressbar
				]);
			}
			this.progresswin.appendChild(this.progressbar);

			this.progresstext = window.document.createElement('div');
			{
				this.progresstext.className = ' KButtonProgressText ';
			}
			this.progresswin.appendChild(this.progresstext);

			var label = window.document.createElement('div');
			{
				label.className = ' KButtonProgressLabel ';
				label.innerHTML = gettext('$KUPLOADBUTTON_UPLOADING_TEXT')
			}
			this.progresswin.appendChild(label);
		}
		this.INITIAL_BUTTON_ALPHA = '0.3';
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	
	KFile.prototype = new KAbstractInput();
	KFile.prototype.constructor = KFile;

	KFile.prototype.visible = function() {
		KAbstractInput.prototype.visible.call(this);
		/*var otherbutton = this.content.getElementsByTagName('button');
		for (var ob = 0; ob != otherbutton.length; ob++) {
		}*/		
	}

	KFile.prototype.onLoad = function(data, request) {
		this.data = data;
		this.draw();
	};
	
	KFile.prototype.addEventListener = function(eventName, callback, target) {
		if ((!target || (target == KWidget.ROOT_DIV)) && (eventName == 'change')) {
			if (!this.button) {
				this.event_to_add = callback;
			}
			else {		
				KDOM.addEventListener(this.button, 'click', callback);
			}
		}
		else {
			KAbstractInput.prototype.addEventListener.call(this, eventName, callback, target);
		}
	};
	
	KFile.prototype.showErrorMessage = function(params) {
		if (!params || !params.tooltip) {
			KAbstractInput.prototype.showErrorMessage.call(this, params);
		}
		else if (params && params.tooltip) {
			KDOM.addEventListener(this.inputFile, 'mouseover', KFile.onMouseOver);
		}
	};
	
	KFile.onMouseOver = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var widget = KDOM.getEventWidget(mouseEvent);
		
		KTooltip.showTooltip(widget.inputFile, {
			text : '* ' + (widget.validationIndex ? widget.getErrorMessage(widget.validationIndex) : ''),
			isHTML : true,
			offsetX : 0,
			offsetY : 15,
			left : KDOM.mouseX(mouseEvent),
			top : (KDOM.mouseY(mouseEvent) + 15),
			cssClass : 'KAbstractInputTooltip'
		});
	};
	
	KFile.prototype.createButton = function() {
		var inputFile = window.document.createElement('input');
		inputFile.setAttribute('type', 'file');
		if (this.multiple) {
			inputFile.setAttribute('multiple', 'multiple');
		}
		inputFile.className = 'KFileButton';
		inputFile.id = this.id + '_input_file';
		KCSS.setStyle({
			position : 'absolute',
			top : '0',
			left : '0',
			opacity : '0'
		}, [
			inputFile
		]);

		KDOM.addEventListener(inputFile, 'change', KFile.changed);
		
		var toReturn = window.document.createElement('button');
		toReturn.setAttribute('type', 'button');
		toReturn.className = 'KFileButton';
		if (!!this.data && !!this.data.buttonText) {
			toReturn.innerHTML = this.data.buttonText;
		}
		else {
			toReturn.innerHTML = gettext('$KFILE_BUTTON_UPLOAD_TEXT');
		}
		toReturn.id = this.id + '_button';
		KCSS.setStyle({
			position : 'absolute',
			top : '0',
			left : '0'
		}, [
			toReturn
		]);
		
		return [toReturn, inputFile];
	};
	
	KFile.prototype.createButtonLabel = function() {
		var toReturn = window.document.createElement('label');
		toReturn.className = 'KFileButtonLabel';
		if (!!this.data && !!this.data.buttonLabelText) {
			toReturn.innerHTML = this.data.buttonLabelText;
		}
		else {
			toReturn.innerHTML = gettext('$KFILE_BUTTON_LABEL_ADD_TEXT');
		}
		
		return toReturn;
	};
	
	KFile.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		var label = this.createLabel();
		this.content.appendChild(label);
		var help = this.createHelp();
		if (!!help) {
			label.appendChild(help);
		}
		
		var elements = this.createButton();
		this.button = elements[0];
		this.inputFile = elements[1];
		if (!!this.event_to_add) {
			KDOM.addEventListener(this.button, 'click', this.event_to_add);
			this.event_to_add = null;
		}
		
		var buttonContainer = window.document.createElement('div');
		buttonContainer.className = 'KFileButtonContainer';
		KCSS.setStyle({
			position : 'relative'
		}, [
			buttonContainer
		]);
		buttonContainer.appendChild(this.button);
		buttonContainer.appendChild(this.inputFile);
		
		this.buttonLabel = this.createButtonLabel();
		
		this.maxFile = (!!this.data && !!this.data.maxFile ? this.data.maxFile : this.maxFile);
		this.noPrompt = (!!this.data && !!this.data.noPrompt ? this.data.noPrompt : this.noPrompt);
		//this.iframe.src = (!!Kinky.FILE_MANAGER_UPLOAD ? Kinky.FILE_MANAGER_UPLOAD : "/zepp/filemanager/form") + "?id=" + this.id + '&ref=' + (!!this.fileref ? this.fileref : this.id);
		
		this.contentContainer.appendChild(this.fileListContainer);
		this.contentContainer.appendChild(this.buttonLabel);
		this.contentContainer.appendChild(buttonContainer);
		this.contentContainer.appendChild(this.errorArea);
		
		this.content.appendChild(this.contentContainer);
		if (this.className == 'KFile') {
			this.addCSSClass(gettext('$KFILE_NO_IMAGE_THUMB'), this.content);
		}
		
		KDOM.addEventListener(this.inputFile, 'mouseover', function(event) {
			var widget = KDOM.getEventWidget(event);
			widget.addCSSClass('KFileButtonOver', widget.button);
		});
		KDOM.addEventListener(this.inputFile, 'mouseout', function(event) {
			var widget = KDOM.getEventWidget(event);
			widget.removeCSSClass('KFileButtonOver', widget.button);
		});
		KDOM.addEventListener(this.button, 'click', KFile.changed);
		
		this.activate();
		this.updateValue();
	};
	
	KFile.prototype.setSize = function(width, height) {
		KCSS.setStyle({
			width : width + 'px',
			height : height + 'px'
		}, [
			this.panel,
			this.content,
			this.button,
			this.inputFile,
			this.iframe
		]);
	};
	
	KFile.prototype.setButtonSize = function(width, height) {
		KCSS.setStyle({
			width : width + 'px',
			height : height + 'px'
		}, [
			this.button,
			this.inputFile,
			this.iframe
		]);
	};
	
	KFile.prototype.getFileName = function() {
		var toReturn = new Array();
		var labels = this.fileListContainer.getElementsByTagName('label');
		
		for ( var label = 0; label != labels.length; label++) {
			toReturn.push(labels[label].innerHTML);
		}
		
		return toReturn;
	};
	
	KFile.prototype.setValue = function(value) {
		if (!value) {
			KAbstractInput.prototype.setValue.call(this, value);
		}
		else if (value instanceof Array) {
			for ( var n in value) {
				var name = value[n].substr(value[n].lastIndexOf('/') + 1);
				this.addFile(name, value[n]);
			}
		}
		else {
			var name = value.substr(value.lastIndexOf('/') + 1);
			this.addFile(name, value);
		}
	};
	
	KFile.changed = function(event) {
		var widget = KDOM.getEventWidget(event);
		
		var files = event.target.files; // FileList object
		if (files === undefined) {
			return;
		}

		widget.disable();

		for (var i = 0, f; f = files[i]; i++) {
			widget.progressbar.style.width = '0%';
			widget.progresstext.innerHTML = '0%';
			window.document.body.appendChild(widget.progresswin);

			var reader = new FileReader();
			reader.file = f;
			reader.onprogress = function(e) {
				KUploadButton.progress(e, widget);
			};

			reader.onload = (function(theFile) {
				return function(e) {
					widget.upload(e.target.result, e.target.file, KFile.uploaded);
				};
			})(f);

			reader.readAsDataURL(f);
		}
	};

	KFile.progress = function(evt, widget, upload) {

		if (evt.lengthComputable) {
			var percentLoaded = Math.round((evt.loaded / evt.total) * 100);

			if (percentLoaded < 100) {
				widget.progressbar.style.width = percentLoaded + '%';
				widget.progresstext.innerHTML = percentLoaded + '%';
			}
			else if (!upload) {
				window.document.body.removeChild(widget.progresswin);				
			}
		}
	};

	KFile.prototype.getUploadedData = function() {
		return this.uploaded_data;
	};

	KFile.prototype.upload = function(data, file, callback) {
		var widget = this;
		var channel = null;
		if (window.XMLHttpRequest) { 
			channel = new window.XMLHttpRequest();
		}
		else if (window.ActiveXObject) { 
			try {
				channel = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) {
				try {
					channel = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1) {
				}
			}
		}

		channel.onreadystatechange = function() {
			if (channel.readyState == 4) {
				if (!!channel.upload) {
					window.document.body.removeChild(widget.progresswin);				
				}
				widget.enable();

				if (channel.status == 0) {
					return;
				}

				if (channel.status == 200) {
					var resp = JSON.parse(channel.responseText);
					widget.uploaded_data = resp;
					widget.addFile(file.name, resp.href);
				}

			}
		};

		var boundary = "----KFILEUpload" + (new Date()).getTime();

		channel.open('POST', (!!this.options.endpoint ? this.options.endpoint : Kinky.FILE_MANAGER_UPLOAD) + (!!this.fileref ? '?ref=' + encodeURIComponent(this.fileref) : '') + (!!this.aditional_args ? (!!this.fileref ? '&' : '?') + this.aditional_args : ''), true);
		channel.setRequestHeader("Authorization", !!this.options.access_token ? 'OAuth2.0 ' + this.options.access_token : KOAuth.getAuthorization().authentication);
		channel.setRequestHeader("X-Requested-With", "XHR");
		channel.setRequestHeader("Pragma", "no-cache");
		channel.setRequestHeader("Cache-Control", "no-store");
		channel.setRequestHeader("Content-Type", "multipart/form-data; boundary=" + boundary);

		var content = '--' + boundary + '\r\n';
		content += 'Content-Encoding: Base64\r\n';
		content += 'Content-Disposition: form-data; name="addFile"; filename="' + file.name + '"\r\n';
		content += 'Content-Type: ' + file.type + '\r\n\r\n';
		content += data.split(',')[1] + '\r\n';
		content += '--' + boundary + '--'

		try {
			if (!!channel.upload) {
				widget.progressbar.style.width = '0%';
				widget.progresstext.innerHTML = '0%';
				window.document.body.appendChild(widget.progresswin);
				channel.upload.addEventListener("progress", function(e) {
					KUploadButton.progress(e, widget, true);
				}, false);
			}
			channel.send(content);
		}
		catch (e) {
		}

	};
	
	KFile.prototype.updateValue = function() {
		var toReturn = new Array();
		var inputs = this.fileListContainer.getElementsByTagName('input');
		
		for ( var input = 0; input != inputs.length; input++) {
			toReturn.push(inputs[input].value);
		}
		
		if (toReturn.length == 0) {
			this.input.value = '';
		}
		else if (toReturn.length > 1) {
			this.input.value = JSON.stringify(toReturn);
		}
		else {
			this.input.value = JSON.stringify(toReturn[0]);
		}
		if (this.className == 'KFile') {
			if (toReturn.length == 0) {
				this.addCSSClass(gettext('$KFILE_NO_IMAGE_THUMB'), this.content);
				this.buttonLabel.innerHTML = gettext('$KFILE_BUTTON_LABEL_ADD_TEXT');
			}
			else {
				this.removeCSSClass(gettext('$KFILE_NO_IMAGE_THUMB'), this.content);
				this.buttonLabel.innerHTML = gettext('$KFILE_BUTTON_LABEL_CHANGE_TEXT');
			}			
		}
	};
	
	KFile.prototype.addFile = function(fileName, filePath) {
		var inputs = this.fileListContainer.getElementsByTagName('input');
		var parent = this.fileListContainer;
		for ( var idx = 0; idx != parent.childNodes.length; idx++) {
			if (parent.childNodes[idx].className.indexOf("KFileDefaultText") != -1) {
				parent.removeChild(parent.childNodes[idx]);
				break;
			}
		}
		
		var newFileContainer = window.document.createElement('div');
		
		var newFileLabel = window.document.createElement('div');
		newFileLabel.appendChild(window.document.createTextNode(fileName));
		newFileContainer.appendChild(newFileLabel);
		
		var newFile = window.document.createElement('input');
		newFile.type = 'hidden';
		newFile.name = this.id + '[]';
		newFile.id = this.id + '_' + inputs.length;
		newFile.value = filePath;
		newFileContainer.appendChild(newFile);
		
		var newFileRemove = window.document.createElement('button');
		newFileRemove.setAttribute('type', 'button');
		newFileContainer.appendChild(newFileRemove);
		
		KDOM.addEventListener(newFileRemove, 'click', function(event) {
			var widget = KDOM.getEventWidget(event);
			var target = KDOM.getEventTarget(event);
			target.parentNode.parentNode.removeChild(target.parentNode);
			widget.updateValue();
			
		});
		
		newFileContainer.appendChild(window.document.createElement('br'));
		parent.appendChild(newFileContainer);
		if (this.activated()) {
			this.updateValue();
			KDOM.fireEvent(this.button, 'click');
		}
		
	};
	
	KFile.prototype.setDefaultText = function(text) {
		var newFileContainer = window.document.createElement('div');
		KCSS.addCSSClass('KFileDefaultText', newFileContainer);
		
		var newFileLabel = window.document.createElement('label');
		newFileLabel.appendChild(window.document.createTextNode(text));
		newFileContainer.appendChild(newFileLabel);
		
		this.fileListContainer.appendChild(newFileContainer);
	};
	
	KFile.prototype.getFileInput = function() {
		var doc = this.iframe.contentDocument || this.iframe.document;
		var inputs = doc.getElementsByTagName('input');
		
		for ( var i = 0; i != inputs.length; i++) {
			if (inputs[i].type == 'file') {
				return inputs[i];
			}
		}
		
	};
	
	KFile.BUTTON_ELEMENT = 'button';
	
	KSystem.included('KFile');
	
});
