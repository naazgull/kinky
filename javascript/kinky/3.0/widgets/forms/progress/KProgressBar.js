function KProgressBar(parent, label, id) {
	if (parent != null) {
		this.isLeaf = true;
		KAbstractInput.call(this, parent, 'text', label, id);
		this.editable = false;
		this.totalWidth = 100;
		this.percentWidth = 1;
		this.percentValue = 0.1;
		this.reversed = false;
	}
}

KSystem.include( [ 'KAbstractInput' ], function() {
	KProgressBar.prototype = new KAbstractInput();
	KProgressBar.prototype.constructor = KProgressBar;

	KProgressBar.prototype.setTotalWidth = function(width) {
		if (this.activated()) {
			this.setStyle( {
				width : width + 'px'
			}, [ KAbstractInput.INPUT_CONTAINER ]);
		}
		this.totalWidth = width;
	};

	KProgressBar.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);
		this.setStyle( {
			width : this.totalWidth + 'px'
		}, [ KAbstractInput.INPUT_CONTAINER ]);
		this.setStyle( {
			width : (this.percentWidth > this.totalWidth ? this.totalWidth : this.percentWidth) + 'px'
		}, KAbstractInput.INPUT_ELEMENT);
		this.paint(this.percentValue);
	};

	KProgressBar.prototype.setValue = function(value, index) {
		KAbstractInput.prototype.setValue.call(this, (Math.round(value * 10000) / 100) + '%', index);

		this.percentValue = value;
		this.percentWidth = Math.round(value * this.totalWidth);

		if (this.activated()) {
			this.setStyle( {
				width : (this.percentWidth > this.totalWidth ? this.totalWidth : this.percentWidth) + 'px'
			}, KAbstractInput.INPUT_ELEMENT);
			this.paint(this.percentValue);
		}
	};

	KProgressBar.prototype.paint = function(value) {
		var colors = null;
		if (this.reversed) {
			colors = [].concat(KProgressBar.colors);
			colors.reverse();
		} else {
			colors = KProgressBar.colors;
		}
		if (value <= 0.25) {
			this.setStyle( {
				backgroundColor : colors[0].back,
				color : colors[0].fore
			}, KAbstractInput.INPUT_ELEMENT);
		} else if (value <= 0.5) {
			this.setStyle( {
				backgroundColor : colors[1].back,
				color : colors[1].fore
			}, KAbstractInput.INPUT_ELEMENT);
		} else if (value <= 0.75) {
			this.setStyle( {
				backgroundColor : colors[2].back,
				color : colors[2].fore
			}, KAbstractInput.INPUT_ELEMENT);
		} else if (value <= 1) {
			this.setStyle( {
				backgroundColor : colors[3].back,
				color : colors[3].fore
			}, KAbstractInput.INPUT_ELEMENT);
		} else if (value > 1) {
			this.setStyle( {
				backgroundColor : KProgressBar.overflowColor.back,
				color : KProgressBar.overflowColor.fore
			}, KAbstractInput.INPUT_ELEMENT);
		}
	};

	KProgressBar.prototype.getValue = function(rawValue) {
		return this.percentValue;
	};

	KProgressBar.colors = [ {
		back : '#CC0000',
		fore : '#FFFFFF'
	}, {
		back : '#EDD200',
		fore : '#000000'
	}, {
		back : '#40AFFF',
		fore : '#000000'
	}, {
		back : '#009C03',
		fore : '#FFFFFF'
	} ];

	KProgressBar.overflowColor = {
		back : '#FF0000',
		fore : '#FFFFFF'
	};

	KSystem.included('KProgressBar');
});
