function KDate(parent, label, id, dateFormat, parseFormat) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.format = dateFormat;
		this.fparse = parseFormat;
		this.inputContainer = window.document.createElement('div');
		this.addDateElements();
		this.focused = -1;
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KDate.prototype = new KAbstractInput();
	KDate.prototype.constructor = KDate;

	KDate.prototype.focus = function(dispatcher) {
		if (!!dispatcher) {
			return;
		}
		this.focused = 0;
		this.inputContainer.childNodes[this.focused].focus();
		this.parent.focus(this);
	};	

	KDate.prototype.selectNextWidget = function(up) {
		if ((up && this.focused == 0) || (!up && this.focused >= this.inputContainer.childNodes.length - 4)) {
			return KAbstractInput.prototype.selectNextWidget.call(this, up);
		}
		else {
			this.focused = (up ? this.focused - 2 : this.focused + 2);
			this.inputContainer.childNodes[this.focused].focus();
			return undefined;
		}
	};
	
	KDate.prototype.addDateElements = function() {
		var escape = false;
		var widget = this;
		var format = '' + this.format;
		var input = null;
		format.replace(/(\w+)|%/g, function(wholematch, match) {
			if (wholematch == '%') {
				escape = true;
				return '';
			}
			if (escape) {
				escape = false;
				return match;
			}
			switch (match) {
				case 'Y': {
					input = widget.addYear();
					break;
				}
				case 'M':
				case 'MM':
				case 'MMM': {
					input = widget.addMonth();
					break;
				}
				case 'D':
				case 'DD':
				case 'd': {
					input = widget.addDay();
					break;
				}
				case 'H': {
					input = widget.addHour();
					break;
				}
				case 'i': {
					input = widget.addMinute();
					break;
				}
				case 's': {
					input = widget.addSecond();
					break;
				}
				default: {
				}
			}
			
			if (!!input) {
				KDOM.addEventListener(input, 'focus', KDate.labelFocus);
				KDOM.addEventListener(input, 'blur', KDate.labelBlur);
				KDOM.addEventListener(input, 'change', KDate.valueChange);
				KDOM.addEventListener(input, 'keyup', KAbstractInput.onEnterPressed);
				KDOM.addEventListener(input, 'keydown', KAbstractInput.onTabPressed);
			}
			return match;
		});
	};

	KDate.prototype.setSeparator = function(cssClass) {
		var sep = window.document.createElement('i');
		sep.className = ' ' + cssClass + 'Separator ' + cssClass + 'Separator' + KLocale.LANG.toUpperCase() + ' KDateSeparator ';
		this.inputContainer.appendChild(sep);
	};

	KDate.prototype.addDay = function() {
		this.day = window.document.createElement('input');
		this.day.className += ' ' + this.className + 'Day ';
		this.day.name = 'day';
		this.day.maxLength = 2;
		this.day.value = 'DD';
		this.day.alt = 'DD';
		this.inputContainer.appendChild(this.day);
		this.setSeparator(this.className + 'Day');
		return this.day;
	};
	
	KDate.prototype.addMonth = function() {
		this.month = window.document.createElement('input');
		this.month.className += ' ' + this.className + 'Month ';
		this.month.name = 'month';
		this.month.maxLength = 2;
		this.month.value = 'MM';
		this.month.alt = 'MM';
		this.inputContainer.appendChild(this.month);
		this.setSeparator(this.className + 'Month');
		return this.month;
	};
	
	KDate.prototype.addYear = function() {
		this.year = window.document.createElement('input');
		this.year.className += ' ' + this.className + 'Year ';
		this.year.name = 'year';
		this.year.maxLength = 4;
		this.year.value = 'YYYY';
		this.year.alt = 'YYYY';
		this.inputContainer.appendChild(this.year);
		this.setSeparator(this.className + 'Year');
		return this.year;
	};
	
	KDate.prototype.addHour = function() {
		this.hour = window.document.createElement('input');
		this.hour.className += ' ' + this.className + 'Hour ';
		this.hour.name = 'hour';
		this.hour.value = 'hh';
		this.hour.alt = 'hh';
		this.inputContainer.appendChild(this.hour);
		this.setSeparator(this.className + 'Hour');
		return this.hour;
	};
	
	KDate.prototype.addMinute = function() {
		this.minute = window.document.createElement('input');
		this.minute.className += ' ' + this.className + 'Minute ';
		this.minute.name = 'minute';
		this.minute.value = 'mm';
		this.minute.alt = 'mm';
		this.inputContainer.appendChild(this.minute);
		this.setSeparator(this.className + 'Minute');
		return this.minute;
	};
	
	KDate.prototype.addSecond = function() {
		this.second = window.document.createElement('input');
		this.second.className += ' ' + this.className + 'Second ';
		this.second.name = 'second';
		this.second.value = 'ss';
		this.second.alt = 'ss';
		this.inputContainer.appendChild(this.second);
		this.setSeparator(this.className + 'Second');
		return this.second;
	};
	
	KDate.prototype.validate = function() {
		var regexValidation = KAbstractInput.prototype.validate.call(this);
		if (regexValidation != true) {
			return regexValidation;
		}
		
		if ((this.day.value == 'DD') && (this.month.value == 'MM') && (this.year.value == 'YYYY')) {
			return true;
		}
		
		// var date = new Date(1900, this.month.value, 1);
		var day = null;
		// var day = parseInt(this.day.value);
		
		if (this.day.value.charAt(0) == 0) {
			day = parseInt(this.day.value.charAt(1));
		}
		else {
			day = parseInt(this.day.value);
		}
		
		// var month = date.getMonth() || this.month.value;
		// alert(this.month.value.charAt(0));
		var month = null;
		if (this.month.value.charAt(0) == 0) {
			month = parseInt(this.month.value.charAt(1));
		}
		else {
			month = parseInt(this.month.value);
		}
		
		if (!month || (month < 1) || (month > 12)) {
			this.addCSSClass('KAbstractInputError');
			return this.defaultErrorMessage || 'data inv\u00e1lida';
		}
		
		if (!(/\d{4}/.test(this.year.value))) {
			this.addCSSClass('KAbstractInputError');
			return this.defaultErrorMessage || 'data inv\u00e1lida';
		}
		
		var availableDays = KDate.getDaysInMonth(month - 1, parseInt(this.year.value));
		if (!day || !(day < (availableDays + 1))) {
			this.addCSSClass('KAbstractInputError');
			return this.defaultErrorMessage || 'data inv\u00e1lida';
		}
		
		if (this.hasHours) {
			var hour = parseInt(this.hour.value);
			if (!hour || isNaN(hour) || (hour < 0) || (hour > 23)) {
				this.addCSSClass('KAbstractInputError');
				return this.defaultErrorMessage || 'hora inv\u00e1lida';
			}
			
			var minute = parseInt(this.minute.value);
			if (!minute || isNaN(minute) || (minute < 0) || (minute > 59)) {
				this.addCSSClass('KAbstractInputError');
				return this.defaultErrorMessage || 'minutos inv\u00e1lidos';
			}
		}
		
		this.removeCSSClass('KAbstractInputError');
		this.errorArea.innerHTML = '';
		return true;
	};
	
	KDate.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);
	};
	
	KDate.prototype.getValue = function() {
		var ret = KAbstractInput.prototype.getValue.call(this);
		if (ret == '') {
			return ret;
		}
		return ret.date;
	};
	
	KDate.prototype.setValue = function(value, dontUpdate) {
		if (!value) {
			KAbstractInput.prototype.setValue.call(this, '');
			return;
		}

		if (typeof value == 'string') {
			value = KSystem.parseDate(!!this.fparse ? this.fparse : this.format, value);
		}
		
		if (!!this.day && (!dontUpdate || (dontUpdate && (this.day.value != this.day.alt)))) {
			var day = value.getDate();
			if (day < 10) {
				this.day.value = '0' + day;
			}
			else {
				this.day.value = day;
			}
		}
		if (!!this.month && (!dontUpdate || (dontUpdate && (this.month.value != this.month.alt)))) {
			var month = value.getMonth() + 1;
			if (month < 10) {
				this.month.value = '0' + month;
			}
			else {
				this.month.value = month;
			}
		}
		if (!!this.year && (!dontUpdate || (dontUpdate && (this.year.value != this.year.alt)))) {
			this.year.value = value.getFullYear();
		}
		if (!!this.hour && (!dontUpdate || (dontUpdate && (this.hour.value != this.hour.alt)))) {
			this.hour.value = value.getHours();
		}
		if (!!this.minute && (!dontUpdate || (dontUpdate && (this.minute.value != this.minute.alt)))) {
			this.minute.value = value.getMinutes();
		}
		if (!!this.second && (!dontUpdate || (dontUpdate && (this.second.value != this.second.alt)))) {
			this.second.value = value.getSeconds();
		}
		
		KAbstractInput.prototype.setValue.call(this, {
			date : value
		});
	};
	
	KDate.labelFocus = function(event) {
		var target = KDOM.getEventTarget(event);
		if ((target.value == null) || isNaN(parseInt(target.value))) {
			target.value = '';
		}
		else {
			target.select();
		}
	};
	
	KDate.labelBlur = function(event) {
		var target = KDOM.getEventTarget(event);
		if ((target.value == null) || (target.value == '')) {
			target.value = target.alt;
		}
	};
	
	KDate.valueChange = function(event) {
		var widget = KDOM.getEventWidget(event);
		var date = (widget.getValue() != '' ? new Date(widget.getValue()) : new Date());
		var hasAllValues = true;
		var hasNoValues = true;
		if (!!widget.day && (widget.day.value != '') && (widget.day.value != widget.day.alt)) {
			var day = 0;
			if (widget.day.value.indexOf('0') == 0) {
				day = parseInt(widget.day.value.replace('0', ''));
			}
			else {
				day = parseInt(widget.day.value);
			}
			date.setDate(day);
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.day;
		}
		if (!!widget.month && (widget.month.value != '') && (widget.month.value != widget.month.alt)) {
			var month = 0;
			if (widget.month.value.indexOf('0') == 0) {
				month = parseInt(widget.month.value.replace('0', ''));
			}
			else {
				month = parseInt(widget.month.value);
			}
			date.setMonth(month - 1);
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.month;
		}
		if (!!widget.year && (widget.year.value != '') && (widget.year.value != widget.year.alt)) {
			date.setFullYear(parseInt(widget.year.value));
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.year;
		}
		if (!!widget.hour && (widget.hour.value != '') && (widget.hour.value != widget.hour.alt)) {
			date.setHours(parseInt(widget.hour.value));
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.hour;
		}
		if (!!widget.minute && (widget.minute.value != '') && (widget.minute.value != widget.minute.alt)) {
			date.setMinutes(parseInt(widget.minute.value));
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.minute;
		}
		if (!!widget.second && (widget.second.value != '') && (widget.second.value != widget.second.alt)) {
			date.setSeconds(parseInt(widget.second.value));
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.second;
		}
		if (hasAllValues) {
			widget.setValue(date, true);
		}
		else if (hasNoValues) {
			widget.setValue(undefined);
		}
		KDOM.fireEvent(widget.input, 'change');
	};
	
	KDate.getDaysInMonth = function(month, year) {
		var m = [
			31,
			28,
			31,
			30,
			31,
			30,
			31,
			31,
			30,
			31,
			30,
			31
		];
		if (month != 1) {
			return m[month];
		}
		if (year % 4 != 0) {
			return m[1];
		}
		if ((year % 100 == 0) && (year % 400 != 0)) {
			return m[1];
		}
		return m[1] + 1;
	};
	
	KSystem.included('KDate');
});