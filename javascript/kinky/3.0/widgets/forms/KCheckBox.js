function KCheckBox(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		
		this.selectedOptions = {};
		
		this.checkbox = window.document.createElement('a');
		this.checkbox.href = 'javascript:void(0)';
		this.checkbox.style.cursor = 'normal';
		this.checkbox.className = ' KCheckBoxOptionsContent ';
		this.checkbox.style.overflow = 'visible';
		this.checkbox.style.position = 'relative';
		
		this.INPUT_ELEMENT = 'checkbox';
		
		this.lastElement = null;
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KCheckBox.prototype = new KAbstractInput();
	KCheckBox.prototype.constructor = KCheckBox;
	
	KCheckBox.prototype.clear = function() {
		this.checkbox.innerHTML = '';
		this.setValue(null);
		this.selectedOptions = {};
		KAbstractInput.prototype.clear.call(this);
	};
	
	KCheckBox.prototype.validate = function() {
		if (this.validations.length != 0) {
			var value = this.getValue(true);
			for ( var index in this.validations) {
				if (((this.validations[index].regex.source == '(.+)') || (this.validations[index].regex.source == '.+')) && (value.length == 0)) {
					this.validationIndex = index;
					this.addCSSClass('KAbstractInputError');
					return this.validations[index].message;
				}
				for ( var k in value) {
					if (!this.validations[index].regex.test(value[k])) {
						this.validationIndex = index;
						this.addCSSClass('KAbstractInputError');
						return this.validations[index].message;
					}
				}
				
				this.errorArea.innerHTML = '';
				this.removeCSSClass('KAbstractInputError');
			}
		}
		return true;
	};
	
	KCheckBox.prototype.addOption = function(value, caption, selected) {
		var checkBoxItemContainer = window.document.createElement('div');
		checkBoxItemContainer.className = ' KCheckBoxOption ';
		
		var check = window.document.createElement('button');
		check.src = selected ? this.imageOn : this.image;
		check.innerHTML = '&nbsp;';
		check.setAttribute('type', 'button');
		check.value = JSON.stringify(value);
		check.id = this.inputID + this.checkbox.childNodes.length;
		checkBoxItemContainer.appendChild(check);
		
		var label = window.document.createElement('label');
		label.setAttribute('for', check.id);
		label.alt = check.value;
		label.name = check.id;
		label.innerHTML = caption;
		
		KDOM.addEventListener(checkBoxItemContainer, 'click', KCheckBox.selectOption);
		KDOM.addEventListener(checkBoxItemContainer, 'mouseover', KCheckBox.mouseOver);
		KDOM.addEventListener(checkBoxItemContainer, 'mouseout', KCheckBox.mouseOut);
		checkBoxItemContainer.appendChild(label);
		this.checkbox.appendChild(checkBoxItemContainer);
		
		if (selected) {
			this.selectedOptions[check.value] = true;
			KCSS.addCSSClass('KCheckBoxOptionSelected', checkBoxItemContainer);
		}
		
		return true;
	};
	
	KCheckBox.prototype.addEventListener = function(eventName, callback, target) {
		if (eventName == 'change') {
			eventName = 'propertychange';
		}
		KAbstractInput.prototype.addEventListener.call(this, eventName, callback, target);
	};
	
	KCheckBox.prototype.createInput = function() {
		return this.checkbox;
	};
	
	KCheckBox.prototype.check = function(idx) {
		KDOM.fireEvent(this.checkbox.childNodes[idx], 'click');
	};
	
	KCheckBox.prototype.uncheck = function(idx) {
		KDOM.fireEvent(this.checkbox.childNodes[idx], 'click');
	};
	
	KCheckBox.prototype.select = function(element) {
		if (!element) {
			this.selectedOption = null;
			this.input.value = '';
			for ( var i = 0; i != this.childDiv(this.INPUT_ELEMENT).childNodes.length; i++) {
				var child = this.childDiv(this.INPUT_ELEMENT).childNodes[i];
				child.childNodes[0].innerHTML = '&nbsp;';
			}
			return;
		}
		
		if (element.tagName.toLowerCase() == 'label') {
			element = element.previousSibling;
		}
		if (element.tagName.toLowerCase() == 'div') {
			element = element.childNodes[0];
		}
		
		if (element.parentNode.className.indexOf('KCheckBoxOptionSelected') != -1) {
			delete this.selectedOptions[element.value];
			KCSS.removeCSSClass('KCheckBoxOptionSelected', element.parentNode);
			element.innerHTML = '&nbsp;';
		}
		else {
			KCSS.addCSSClass('KCheckBoxOptionSelected', element.parentNode);
			this.selectedOptions[element.value] = true;
			element.innerHTML = '&nbsp;';
			
		}
		
		KDOM.fireEvent(this.childDiv(this.INPUT_ELEMENT), 'propertychange');
	};
	
	KCheckBox.prototype.getValue = function() {
		var toReturn = new Array();
		
		for ( var index in this.selectedOptions) {
			toReturn.push(JSON.parse(index));
		}
		
		return toReturn;
	};
	
	KCheckBox.prototype.setValue = function(value) {
		if (!value) {
			delete this.selectedOptions;
			this.selectedOptions = new Object();
		}
		else {
			this.selectedOptions[JSON.stringify(value)] = true;
		}
		KAbstractInput.prototype.setValue.call(this, value);
		for ( var i = 0; i != this.childDiv(this.INPUT_ELEMENT).childNodes.length; i++) {
			var child = this.childDiv(this.INPUT_ELEMENT).childNodes[i];
			if (child.childNodes[0].value == this.input.value) {
				this.select(child.childNodes[0]);
			}
		}
	};
	
	KCheckBox.mouseOver = function(event) {
		var element = KDOM.getEventTarget(event);
		if ((element.tagName.toLowerCase() == 'label') || (element.tagName.toLowerCase() == 'button')) {
			element = element.parentNode;
		}
		if (element.className.indexOf('KCheckBoxOptionSelected') == -1) {
			KCSS.addCSSClass('KCheckBoxOptionOver', element);
		}
	};
	
	KCheckBox.mouseOut = function(event) {
		var element = KDOM.getEventTarget(event);
		if ((element.tagName.toLowerCase() == 'label') || (element.tagName.toLowerCase() == 'button')) {
			element = element.parentNode;
		}
		KCSS.removeCSSClass('KCheckBoxOptionOver', element);
	};
	
	KCheckBox.selectOption = function(event) {
		KDOM.stopEvent(event);
		var element = KDOM.getEventTarget(event);
		var widget = KDOM.getEventWidget(event);
		
		widget.select(element);
	};
	
	KCheckBox.updateValue = function(event) {
		var widget = KDOM.getEventWidget(event);
		widget.onKeyPressed();
	};
	
	KCheckBox.OPTION_CONTAINER_DIV = 'checkbox';
	KCheckBox.OPENED = null;
	
	KSystem.included('KCheckBox');
});
