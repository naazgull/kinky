function KJSONText(parent, label, id) {
	if (!!parent) {
		KTextArea.call(this, parent, label, id);
	}
}

KSystem.include([
	'KTextArea',
	'KAbstractInput'
], function() {
	
	KJSONText.prototype = new KTextArea();
	KJSONText.prototype.constructor = KJSONText;
	
	KJSONText.prototype.getValue = function() {
		return JSON.parse(this.textarea.value);
	};
	
	KJSONText.prototype.setValue = function(value) {
		if (!value) {
			this.textarea.value = '';
		}
		else {
			this.textarea.value = JSON.stringify(value);
		}
		KAbstractInput.prototype.setValue.call(this, value);
	};
	
	KSystem.included('KJSONText');
});