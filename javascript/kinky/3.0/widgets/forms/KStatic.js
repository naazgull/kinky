function KStatic(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.statici = this.createInput();
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KStatic.prototype = new KAbstractInput();
	KStatic.prototype.constructor = KStatic;

	KStatic.prototype.setInnerLabel = function(text) {
		this.innerLabel.innerHTML = text;
		this.innerLabel.className = 'InnerLabel';
	};

	KStatic.prototype.setValue = function(value) {
		KAbstractInput.prototype.setValue.call(this, value);
		this.statici.innerHTML = value;
	};

	KStatic.prototype.getValue = function() {
		var ret = KAbstractInput.prototype.getValue.call(this);
		return ret;
	};

	KStatic.prototype.createInput = function() {
		var input = window.document.createElement('div');
		{
			input.className = ' ' + this.className + 'InputElement ';
			input.name = (!!this.inputID ? input.inputID : input.hash);
		}
		return input;
	};

	KStatic.prototype.draw = function() {
		this.inputContainer = window.document.createElement('div');

		var label = this.createLabel();

		this.inputContainer.appendChild(this.statici);

		this.content.appendChild(label);
		this.content.appendChild(this.inputContainer);

		this.errorArea = window.document.createElement('div');
		this.errorArea.className = 'KAbstractInputErrorArea ' + this.className + 'ErrorArea';
		this.content.appendChild(this.errorArea);

		this.activate();
	};

	KStatic.prototype.INPUT_ELEMENT = 'input';
	KStatic.LABEL_CONTAINER = 'label';
	KStatic.INPUT_ERROR_CONTAINER = 'errorArea';

	KSystem.included('KStatic');
});