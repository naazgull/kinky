function KAutoCompletion(parent, label, id) {
	if (parent) {
		KCombo.call(this, parent, label, id);
		this.resultsArrived = true;
		this.changeCallback = KAutoCompletion.dropResults;
		this.search_changed = false;

		this.combo.style.cursor = 'text';
		KDOM.removeEventListener(this.combo, 'click', KCombo.toggleOptions);
		this.comboText.setAttribute('contenteditable', 'true');

		this.addActionListener(KAutoCompletion.actions, [
			'/no-content'
		]);
	}
}

KSystem.include([
	'KCombo'
], function() {
	KAutoCompletion.prototype = new KCombo();
	KAutoCompletion.prototype.constructor = KAutoCompletion;
	
	KAutoCompletion.prototype.focus = function(dispatcher) {
		KCSS.addCSSClass('KComboFocused', this.combo);
		this.comboText.focus();
	};	

	KAutoCompletion.prototype.setSearchParams = function(params) {
		this.searchParams = params;
	};
	
	KAutoCompletion.prototype.releaseSearchInput = function() {
		this.combo.contenteditable = true;
		this.resultsArrived = true;
	};
	
	KAutoCompletion.prototype.blockSearchInput = function() {
		this.combo.contenteditable = false;
		this.resultsArrived = false;
	};
	
	KAutoCompletion.prototype.draw = function() {
		this.addCSSClass('KCombo');
		this.addCSSClass('KComboContent', this.content);

		if (!!this.searchParams) {
			KDOM.removeEventListener(this.dropButton, 'click', KCombo.toggleOptions);
			KDOM.addEventListener(this.dropButton, 'click', KAutoCompletion.sendSearch);
		}
		KDOM.addEventListener(this.comboText, 'focus', KAutoCompletion.focusSearch);
		KCombo.prototype.draw.call(this);
	};

	KAutoCompletion.prototype.actOnEnter = function() {
		if (!this.getValue()) {
			return false;
		}
		return true;
	};	

	KAutoCompletion.prototype.onKeyPressed = function(event) {
		var keyCode = event.keyCode || event.which;
		switch (keyCode) {
			case 40: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markNext();
				this.releaseSearchInput();
				break;
			}
			case 38: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markPrevious();
				this.releaseSearchInput();
				break;
			}
			case 13: {
				if (!!this.getValue() && this.optionsToggled) {
					break;
				}
				KDOM.stopEvent(event);
				if (this.comboText.textContent != '') {
					this.blockSearchInput();
					this.changeCallback(this, this.comboText.textContent);
					if (!this.searchParams && this.optionsToggled) {
						KCombo.toggleOptions(null, this);
					}
				}
				break;
			}
			case 18: 
			case 17: 
			case 16: {
				break;
			}
			case 9: {
				this.blur();
				break;
			}
			case 27: {
				KDOM.stopEvent(event);
				this.selectedOption.value = '';
				this.combo.value = '';
				this.releaseSearchInput();
				this.combo.blur();
				if (!!this.optionsToggled) {
					KCombo.toggleOptions(null, this);					
				}
				break;
			}
			default: {
				if (!this.onEnterPressed && !this.searchParams) {
					var self = this;
					if (this.changeCallbackTimer !== undefined) {
						KSystem.removeTimer(this.changeCallbackTimer);
					}
					this.changeCallbackTimer = KSystem.addTimer(function() {
						self.changeCallbackTimer = undefined;
						self.blockSearchInput();
						self.changeCallback(self, self.comboText.textContent);
					}, 100);
				}
			}
		}
	};
	
	KAutoCompletion.prototype.searchServer = function(search) {
		if (!!this.searchParams) {
			this.kinky.get(this, 'rest:' + this.searchParams.service + '?' + this.searchParams.field + '=m/' + search + '/i', {}, null, 'onChange');
		}
		else {
			if (!this.optionsToggled) {
				KCombo.toggleOptions(null, this);
			}
			this.filter(search);
		}
	};
	
	KAutoCompletion.prototype.onChange = function(data, request) {
		this.optionsContent.innerHTML = '';
		
		for ( var index in data.elements) {
			var value = null;
			var caption = null;
			var first = true;

			eval('value = data.elements[index].' + this.searchParams.value_field + ';');
			if (!!this.searchParams.icon_field) {
				eval('caption = \'<img style="display: inline-block; vertical-align: middle; width: auto; height: ' + (this.comboText.offsetHeight) +  'px" src="\' + data.elements[index].' + this.searchParams.icon_field + ' + \'"></img> \' + data.elements[index].' + this.searchParams.label_field + ';');
			}
			else {
				eval('caption = data.elements[index].' + this.searchParams.label_field + ';');
			}
			this.addOption(value, caption, false);
		}
		this.releaseSearchInput();
		if (!this.optionsToggled) {
			KCombo.toggleOptions(null, this);
		}
	};

	KAutoCompletion.focusSearch = function(event) {
		var widget = KDOM.getEventWidget(event);
		KDOM.moveToEndOfContenteditable(widget.comboText);
		KCSS.addCSSClass('KComboFocused', widget.combo);
	};	

	KAutoCompletion.sendSearch = function(event) {
		var widget = KDOM.getEventWidget(event);
		if (widget.comboText.textContent != '') {
			widget.blockSearchInput();
			widget.changeCallback(widget, widget.comboText.textContent);
		}
	};
	
	KAutoCompletion.dropResults = function(widget, search) {
		widget.searchServer(search);
	};
		
	KAutoCompletion.actions = function(widget, action) {
		widget.releaseSearchInput();
		widget.optionsContent.innerHTML = '';
		widget.options.splice(1, widget.options.length - 1);
		widget.setValue('');
	};
	
	KSystem.included('KAutoCompletion');
});