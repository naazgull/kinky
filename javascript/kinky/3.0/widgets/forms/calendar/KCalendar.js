function KCalendar(parent, label, id) {
	if (parent) {
		this.isLeaf = false;
		KAbstractInput.call(this, parent, label, id);
		this.currentDate = new Date();
		this.days = new Array();
		this.editable = false;
		this.calendarToggled = false;
		this.contentDiv = window.document.createElement('div');
		this.contentDiv.className = ' KCalendarContentDiv_' + id.toUpperCase() + ' KCalendarContentDiv ';

		this.setValue(this.currentDate);
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	
	KCalendar.prototype = new KAbstractInput();
	KCalendar.prototype.constructor = KCalendar;
	
	KCalendar.prototype.setValue = function(value) {
		if (!value) {
			return;
		}
		
		if (value != '') {
			var formattedDate = value.getDate() + '/' + (value.getMonth() + 1) + '/' + value.getFullYear();
			KAbstractInput.prototype.setValue.call(this, formattedDate);
			this.currentDate = value;
		}
	};
	
	KCalendar.prototype.draw = function() {		
		KAbstractInput.prototype.draw.call(this);
		
		this.inputContainer.childNodes[0].setAttribute('type', 'text');
		this.inputContainer.childNodes[0].setAttribute('readonly', 'readonly');

		var calendarButton = window.document.createElement('button');
		calendarButton.setAttribute('type', 'button');
		KDOM.addEventListener(calendarButton, 'click', KCalendar.toggleCalendar);
		this.content.appendChild(calendarButton);

		var table = window.document.createElement('table');
		var tHead = table.appendChild(window.document.createElement('thead'));
		var tHeadNav = tHead.appendChild(window.document.createElement('tr'));
		var tHeadDays = tHead.appendChild(window.document.createElement('tr'));
		var tBody = table.appendChild(window.document.createElement('tbody'));
		var monthDate = new Date();
		monthDate.setMonth(this.currentDate.getMonth());
		monthDate.setDate(1);
		
		this.contentDiv.appendChild(table);
		table.className = ' KCalendarTable ';
		tHead.className = ' KCalendarHeader ';
		tBody.className = ' KCalendarBody ';
		
		this.titlePreviousMonth = tHeadNav.appendChild(window.document.createElement('th')).appendChild(window.document.createElement('button'));
		this.titlePreviousMonth.setAttribute('type', 'button');
		this.titlePreviousMonth.name = this.id;
		this.titlePreviousMonth.appendChild(window.document.createTextNode('<'));
		this.titlePreviousMonth.className = ' KCalendarPrevButton ';
		this.titleText = tHeadNav.appendChild(window.document.createElement('th'));
		this.titleText.colSpan = '5';
		this.titleText.className = ' KCalendarTitle ';
		this.titleText.appendChild(window.document.createTextNode(' '));
		this.titleNextMonth = tHeadNav.appendChild(window.document.createElement('th')).appendChild(window.document.createElement('button'));
		this.titleNextMonth.setAttribute('type', 'button');
		this.titleNextMonth.name = this.id;
		this.titleNextMonth.appendChild(window.document.createTextNode('>'));
		this.titleNextMonth.className = ' KCalendarNextButton ';
		
		KDOM.addEventListener(this.titlePreviousMonth, 'click', function(e) {
			var element = KDOM.getEventTarget(e);
			var widget = Kinky.getWidget(element.name);
			widget.showMonth(widget.currentDate.getMonth() - 1, widget.currentDate.getFullYear());
		});
		KDOM.addEventListener(this.titleNextMonth, 'click', function(e) {
			var element = KDOM.getEventTarget(e);
			var widget = Kinky.getWidget(element.name);
			widget.showMonth(widget.currentDate.getMonth() + 1, widget.currentDate.getFullYear());
		});
		
		for ( var i = 0; i != 6; i++) {
			var tRow = tBody.appendChild(window.document.createElement('tr'));
			this.days[i] = new Array();
			for ( var k = 0; k != 7; k++) {
				if (i == 0) {
					var th = window.document.createElement('th');
					th.className = ' KCalendarDayName ';
					var abbr = window.document.createElement('abbr');
					abbr.title = KCalendar.localization['weekDayName'][k];
					abbr.appendChild(window.document.createTextNode(KCalendar.localization['weekDayName'][k].charAt(0)));
					th.appendChild(abbr);
					tHeadDays.appendChild(th);
				}
				this.days[i][k] = tRow.appendChild(window.document.createElement('td')).appendChild(window.document.createElement('button'));
				this.days[i][k].setAttribute('type', 'button');
				this.days[i][k].name = this.id;
				tRow.style.textAlign = 'right';
				this.days[i][k].appendChild(window.document.createTextNode(' '));
				this.days[i][k].className = ' KCalendarDay ';
				KDOM.addEventListener(this.days[i][k], 'click', function(e) {
					var element = KDOM.getEventTarget(e);
					var widget = Kinky.getWidget(element.name);
					
					widget.currentDate.setDate(element.innerHTML);
					widget.setValue(widget.currentDate);
					widget.inputContainer.childNodes[0].value = widget.currentDate.getDate() + '/' + (widget.currentDate.getMonth() + 1) + '/' + widget.currentDate.getFullYear();
					KCalendar.toggleCalendar(null, widget);
				});
			}
		}
		this.showMonth(this.currentDate.getMonth(), this.currentDate.getFullYear());
	};
	
	KCalendar.prototype.showMonth = function(monthToShow, monthYear) {
		if (monthToShow > 11) {
			monthToShow = 0;
			monthYear++;
		}
		if (monthToShow < 0) {
			monthToShow = 11;
			monthYear--;
		}
		var monthDate = new Date();
		var nDayInMonth = this.getDaysInMonth(monthToShow, monthYear);
		monthDate.setDate(1);
		monthDate.setMonth(monthToShow);
		monthDate.setFullYear(monthYear);
		this.currentDate = monthDate;
		
		if (this.titleText.childNodes.length != 0) {
			this.titleText.removeChild(this.titleText.childNodes[0]);
		}
		
		this.titleText.appendChild(window.document.createTextNode(' ' + KCalendar.localization['monthName'][monthToShow] + ' ' + monthYear + ' '));
		
		for ( var i = 0, dayOfMonth = 0; i != 6; i++) {
			for ( var k = 0; k != 7; k++) {
				if ((dayOfMonth == 0) && (k == monthDate.getDay())) {
					dayOfMonth = 1;
				}
				if (this.days[i][k].childNodes.length != 0) {
					this.days[i][k].removeChild(this.days[i][k].childNodes[0]);
				}
				this.days[i][k].style.visibility = ((dayOfMonth == 0) || (dayOfMonth > nDayInMonth) ? 'hidden' : 'visible');
				this.days[i][k].appendChild(window.document.createTextNode((dayOfMonth == 0) || (dayOfMonth > nDayInMonth) ? '0' : '' + dayOfMonth++));
			}
		}
	};
	
	KCalendar.prototype.getDaysInMonth = function(month, year) {
		var m = [
			31,
			28,
			31,
			30,
			31,
			30,
			31,
			31,
			30,
			31,
			30,
			31
		];
		if (month != 1) {
			return m[month];
		}
		if (year % 4 != 0) {
			return m[1];
		}
		if ((year % 100 == 0) && (year % 400 != 0)) {
			return m[1];
		}
		return m[1] + 1;
	};
	
	KCalendar.toggleCalendar = function(event, cal) {
		var widget = cal || KDOM.getEventWidget(event);
		
		var top = (widget.offsetTop() - widget.scrollTop() - window.pageYOffset + widget.getHeight());

		widget.setStyle({
			position : 'absolute',
			top : top + 'px',
			right : (KDOM.getBrowserWidth() - (widget.offsetLeft() + widget.panel.offsetWidth)) + 'px'
		}, widget.contentDiv);
		if (widget.calendarToggled) {
			window.document.body.removeChild(widget.contentDiv);
			widget.calendarToggled = false;
			KCalendar.OPENED = null;
		}
		else {
			window.document.body.appendChild(widget.contentDiv);
			widget.calendarToggled = true;
			KCalendar.OPENED = widget;
		}
	};
	
	KCalendar.localization = KCalendar.localization || {};
	KCalendar.OPENED = null;
	
	KCalendar.clickOut = function() {
		KDOM.addEventListener(window.document.body, 'click', function(event) {
			var widget = KDOM.getEventWidget(event);
			if (!!widget) {
				if (widget.parent instanceof KCalendar && (widget.parent.id == KCalendar.OPENED.id)) {
					return;
				}
			}
			else {
				var element = KDOM.getEventTarget(event);
				widget = Kinky.getWidget(element.name);
			}
			
			if (KCalendar.OPENED && (!widget || (widget && (widget.id != KCalendar.OPENED.id)))) {
				window.document.body.removeChild(KCalendar.OPENED.contentDiv);
				KCalendar.OPENED.calendarToggled = false;
				KCalendar.OPENED = null;
			}
		});
	};
	
	if (window.attachEvent) {
		window.attachEvent('onload', KCalendar.clickOut);
	}
	else {
		window.addEventListener('load', KCalendar.clickOut, false);
	}
	
	KSystem.included('KCalendar');
});