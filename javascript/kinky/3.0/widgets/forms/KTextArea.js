function KTextArea(parent, label, id) {
	if (!!parent) {
		KAbstractInput.call(this, parent, label, id);
		this.onEnterSubmit = false;
		
		this.textarea = window.document.createElement('textarea');
		this.textarea.name = this.textarea.id = id;
		this.INPUT_ELEMENT = 'textarea';
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	
	KTextArea.prototype = new KAbstractInput;
	KTextArea.prototype.constructor = KTextArea;
	
	KTextArea.prototype.createInput = function() {
		if (!!this.height) {
			this.textarea.style.height = this.textarea.style.maxHeight = this.height + 'px';
		}
		return this.textarea;
	};
	
	KTextArea.prototype.getValue = function() {
		return this.textarea.value;
	};
	
	KTextArea.prototype.setValue = function(value) {
		this.textarea.value = '';
		this.textarea.innerHTML = this.textarea.value = value;
		KAbstractInput.prototype.setValue.call(this, value);
	};
	
	KSystem.included('KTextArea');
});