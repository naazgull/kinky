function KRichText(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		
		this.editorInstance = null;
		this.waitForVisibility = -1;
		this.configs = undefined;

		this.textarea = window.document.createElement('div');
		this.textarea.name = this.id + '_textarea';
		this.textarea.id = this.id + '_textarea';
		this.textarea.setAttribute('contenteditable', 'true');
		this.addCSSClass(this.className + 'ContentEditable KRichTextContentEditable', this.textarea);
		this.height = 200;
		
		this.INPUT_ELEMENT = 'textarea';

		this.content.appendChild(this.textarea);
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KRichText.prototype = new KAbstractInput();
	KRichText.prototype.constructor = KRichText;
	
	/*KRichText.prototype.focus = function() {
		KAbstractInput.prototype.focus.call(this);
		this.textarea.focus();
	};*/

	KRichText.prototype.createInput = function() {
		return this.textarea;
	};
	
	KRichText.prototype.getValue = function() {
		return this.textarea.innerHTML;
	};
	
	KRichText.prototype.setValue = function(value) {
		this.textarea.innerHTML = value;
	};
	
	KRichText.prototype.setConfig = function(config) {
		this.configs = config;
	};

	KRichText.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);
		this.waitForVisibility = KSystem.addTimer('Kinky.getWidget(\'' + this.id + '\').editor()', KRichText.CKE_DELAY);
	};
	
	KRichText.prototype.editor = function() {
		var p = this.panel;
		while (p.tagName.toLowerCase() != 'body') {
			if (p.style.display == 'none' || p.style.visibility == 'hidden') {
				this.waitForVisibility = KSystem.addTimer('Kinky.getWidget(\'' + this.id + '\').editor()', KRichText.CKE_DELAY);
				return;
			}
			p = p.parentNode;
		}
		
		this.waitForVisibility = -1;
		if (window.CKEDITOR) {
			this.editorInstance = CKEDITOR.inline(this.textarea, this.configs || {
				
				language : 'pt',
				coreStyles_bold : {
					element : 'b'
				},
				coreStyles_italic : {
					element : 'i'
				},
				fontSize_sizes : 'xx-small/1;x-small/2;small/3;medium/4;large/5;x-large/6;xx-large/7',
				fontSize_style : {
					element : 'font',
					attributes : {
						'size' : '#(size)'
					}
				},
				height : this.height + 'px',
				toolbar : [
					[
						'Bold',
						'Italic',
						'Underline'
					],
					[
						'NumberedList',
						'BulletedList',
						'Outdent', 
						'Indent',
						'-',
						'Blockquote'
					],
					[
						'Source',
						'-',
						'Undo',
						'Redo'
					],
					[
						'Link',
						'Unlink',
						'HorizontalRule', 
						'SpecialChar',
						'-',
						'RemoveFormat',
						'FontSize',
						'Format'
					],
					[
						'JustifyLeft',
						'JustifyCenter',
						'JustifyBlock',
						'JustifyRight'
					]
				]
			});
		}
	};
	
	KRichText.prototype.destroy = function() {
		if (this.waitForVisibility != -1) {
			KSystem.removeTimer(this.waitForVisibility);
		}
		if (!!this.editorInstance) {
			this.editorInstance.destroy(true);
		}
		KAbstractInput.prototype.destroy.call(this);
	};
	
	KRichText.prototype.actOnEnter = function() {
		return false;
	};	

	KRichText.prototype.INPUT_ELEMENT = 'input';
	KRichText.LABEL_CONTAINER = 'label';
	KRichText.INPUT_ERROR_CONTAINER = 'errorArea';
	KRichText.CKE_DELAY = 750;
	
	KSystem.included('KRichText');
});