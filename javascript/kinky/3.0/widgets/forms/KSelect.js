function KSelect(parent, label, id) {
	if (parent != null) {
		KCombo.call(this, parent, label, id);
		this.selectedOption = {};
	}
}

KSystem.include([
	'KCombo'
], function() {
	
	KSelect.prototype = new KCombo();
	KSelect.prototype.constructor = KSelect;
	
	KSelect.prototype.blur = function() {
		KCSS.removeCSSClass('KComboOpened', this.content);
		this.childDiv(KWidget.ROOT_DIV).removeChild(this.optionsPanel);
		this.optionsToggled = false;
	};
	
	KSelect.prototype.validate = function() {
		if (this.validations.length != 0) {
			var value = this.getValue(true);
			for ( var index in this.validations) {
				if (((this.validations[index].regex.source == '(.+)') || (this.validations[index].regex.source == '.+')) && (value.length == 0)) {
					this.validationIndex = index;
					this.addCSSClass('KAbstractInputError');
					return this.validations[index].message;
				}
				for ( var k in value) {
					if (!this.validations[index].regex.test(value[k])) {
						this.validationIndex = index;
						this.addCSSClass('KAbstractInputError');
						return this.validations[index].message;
					}
				}
				
				this.errorArea.innerHTML = '';
				this.removeCSSClass('KAbstractInputError');
			}
		}
		return true;
	};
	
	KSelect.prototype.onKeyPressed = function(event) {
		switch (event.keyCode) {
			case 40: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markNext();
				break;
			}
			case 38: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markPrevious();
				break;
			}
			case 8: {
				KDOM.stopEvent(event);
				if (this.comboText.innerHTML.length != 0) {
					this.comboText.innerHTML = this.comboText.innerHTML.substr(0, this.comboText.innerHTML.length - 1);
					this.filter();
				}
				break;
			}
			case 27: {
				KDOM.stopEvent(event);
				this.innerLabel.innerHTML = '';
				this.filter();
				KCombo.toggleOptions(null, this, true);
				break;
			}
			default: {
				var key = (event.charCode != 0 ? event.charCode : event.keyCode);
				if (key != 0) {
					if (!this.optionsToggled) {
						KCombo.toggleOptions(null, this);
					}
					this.innerLabel.innerHTML += String.fromCharCode(key);
					this.filter();
				}
			}
		}
	};
	
	KSelect.prototype.filter = function() {
		var regex = new RegExp(this.comboText.innerHTML, 'i');
		for ( var i = 0; i != this.optionsContent.childNodes.length; i++) {
			if (this.optionsContent.childNodes[i].tagName.toLowerCase() == 'label') {
				if (!regex.test(this.optionsContent.childNodes[i].innerHTML.replace(/\$/, '\\$'))) {
					this.optionsContent.childNodes[i].style.display = 'none';
				}
				else {
					this.optionsContent.childNodes[i].style.display = 'block';
				}
			}
		}
	};
	
	KSelect.prototype.select = function(element) {
		if (!!element && !!this.selectedOption[element.alt]) {
			this.innerLabel.innerHTML = '';
			return;
		}
		
		if (!!element) {
			this.selectedOption[element.alt] = true;
			
			var selected = window.document.createElement('div');
			{
				selected.alt = element.alt;
				selected.innerHTML = element.innerHTML;
				var removeButton = window.document.createElement('button');
				{
					removeButton.innerHTML = 'x';
					this.addEventListener('click', KSelect.removeSelection, removeButton);
				}
				selected.appendChild(removeButton);
			}
			this.comboText.appendChild(selected);
		}
		else {
			delete this.selectedOption;
			this.selectedOption = new Object();
			this.comboText.innerHTML = '';
		}
		this.innerLabel.innerHTML = '';
		
		KCombo.toggleOptions(null, this, true);
		
		this.currentIndex = -1;
		KDOM.fireEvent(this.combo, 'change');
	};
	
	KSelect.prototype.getValue = function() {
		var toReturn = new Array();
		
		for ( var index in this.selectedOption) {
			toReturn.push(JSON.parse(index));
		}
		
		return toReturn;
	};
	
	KSelect.prototype.setValue = function(value) {
		if (!value) {
			this.select();
			return;
		}
		KAbstractInput.prototype.setValue.call(this, value);
		
		for ( var i = 0; i != this.optionsContent.childNodes.length; i++) {
			if (this.optionsContent.childNodes[i].alt == this.input.value) {
				this.select(this.optionsContent.childNodes[i]);
			}
		}
	};
	
	KSelect.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);
		
		this.inputContainer.appendChild(this.dropButton);
		if (KSystem.isIncluded('KScroll') && ((this.scrollOps || KCombo.SCROLL_OPS))) {
			try {
				this.addScroll(KScroll.VSCROLL, this.scrollOps || KCombo.SCROLL_OPS, KCombo.OPTIONS_CONTENT_DIV, KCombo.OPTIONS_CONTAINER_DIV, KCombo.OPTIONS_DIV);
			}
			catch (e) {
			}
		}
		else {
			this.optionsPanel.style.overflow = 'auto';
			this.optionsContainer.style.overflow = 'visible';
			this.optionsContent.style.position = 'relative';
		}
		
		this.addEventListener('keypress', KCombo.keyPressed, this.INPUT_ELEMENT);
	};
	
	KSelect.removeSelection = function(event) {
		var button = KDOM.getEventTarget(event);
		var widget = KDOM.getEventWidget(event);
		delete widget.selectedOption[button.parentNode.alt];
		button.parentNode.parentNode.removeChild(button.parentNode);
	};
	
	KSystem.included('KSelect');
});