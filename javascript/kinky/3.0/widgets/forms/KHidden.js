function KHidden(parent, id) {
	if (/^widget/.test(id)) {
		throw Error('KHidden: input id cannot start with "widget".');
	}
	if (parent != null) {
		KWidget.call(this, parent);
		this.inputID = id;
		this.parentForm = null;
		this.validations = new Array();
		this.errorArea = {};
		
		this.input = window.document.createElement('input');
		{
			this.input.setAttribute('type', 'hidden');
			this.input.id = this.input.name = this.inputID + "Value";
		}
		this.panel.appendChild(this.input);
	}
}

KSystem.include([
	'KWidget'
], function() {
	KHidden.prototype = new KWidget();
	KHidden.prototype.constructor = KHidden;
	
	KHidden.prototype.validate = function() {
		if (this.validations.length != 0) {
			var value = this.getValue(true);
			value = !!value ? value : '';
			if ((typeof value == 'object') || (value instanceof Array)) {
				value = JSON.stringify(value);
			}
			for ( var index in this.validations) {
				if (!this.validations[index].regex.test(value)) {
					this.validationIndex = index;
					this.addCSSClass('KAbstractInputError');
					return this.validations[index].message;
				}				
				this.removeCSSClass('KAbstractInputError');
			}
		}
		return true;
	};
	
	KHidden.prototype.addValidator = function(test, message, options) {
		if (!(test instanceof RegExp)) {
			if (typeof test != 'string') {
				return;
			}
			test = new RegExp(test);
		}
		this.validations.push({
			regex : test,
			message : message,
			options : options
		});
	};
	
	KHidden.prototype.removeValidator = function(test) {
	};
	
	KHidden.prototype.addEventListener = function(eventName, callback, target) {
	};
	
	KHidden.prototype.focus = function(dispatcher) {
	};
	
	KHidden.prototype.getInputID = function() {
		return this.inputID;
	};
	
	KHidden.prototype.getInput = function() {
		return this.childDiv(this.INPUT_ELEMENT);
	};
	
	KHidden.prototype.setLabel = function(text) {
	};

	KHidden.prototype.selectNextWidget = function(up) {
		var index = this.parent.indexOf(this);
		var next = undefined;

		if (up) {
			if (index == 0) {
				next = this.parent.childAt(this.parent.nChildren - 1);
			}
			else {
				next = this.parent.childAt(index - 1);
			}
		}
		else {
			if (index < this.parent.nChildren - 1) {
				next = this.parent.childAt(index + 1);
			}
			else {
				next = this.parent.childAt(0);
			}
		}

		if (next instanceof KFile) {
			return next.selectNextWidget(up);
		}
		if (next instanceof KHidden) {
			return next.selectNextWidget(up);
		}
		return next;
	};

	
	KHidden.prototype.setInnerLabel = function(text) {
	};
	
	KHidden.prototype.getErrorMessage = function(index) {
		return "";
	};
	
	KHidden.prototype.showErrorMessage = function(params) {
	};
	
	KHidden.prototype.go = function() {
		this.draw();
	};
	
	KHidden.prototype.clear = function() {
		this.input.value = '';
		KWidget.prototype.clear.call(this);
	};
	
	KHidden.prototype.createInput = function() {
		return null;
	};
	
	KHidden.prototype.createLabel = function() {
		return null;
	};
	
	KHidden.prototype.draw = function() {
		this.activate();
	};
	
	KHidden.prototype.setHelp = function(text, baloon) {
	};
	
	KHidden.prototype.getValue = function(rawValue) {
		if (this.input.value != '') {
			return JSON.parse(this.input.value);
		}
		else {
			return '';
		}
	};
	
	KHidden.prototype.setValue = function(value) {
		if (!value) {
			this.input.value = '';
		}
		else {
			this.input.value = JSON.stringify(value);
		}
	};
	
	KHidden.prototype.INPUT_ELEMENT = 'input';
	
	KSystem.included('KHidden');
});