function KOAuthForm(parent, config) {
	if (parent != null) {
		KForm.call(this, parent);
		this.oauth = config;
		if (!this.oauth) {
			this.oauth = new Object();
		}
		if (!this.oauth.response_type) {
			this.oauth.response_type = 'token';
		}
		this.oauth.client_id = window.document.location.host;
	}
}

KSystem.include([
	'KForm',
	'KInput',
	'KPassword',
	'KButton'
], function() {
	KOAuthForm.prototype = new KForm();
	KOAuthForm.prototype.constructor = KOAuthForm;
	
	KOAuthForm.prototype.draw = function() {
		this.user = new KInput(this, gettext('$LOGIN_FORM_USER'), 'id');
		this.user.addCSSClass('KOAuthFormUserInput');
		this.user.addValidator(/(.+)/, gettext('$LOGIN_FORM_USER_ERR_MSG'));
		this.addInput(this.user);
		
		this.password = new KPassword(this, gettext('$LOGIN_FORM_PASS'), 'password');
		this.user.addCSSClass('KOAuthFormPasswordInput');
		this.user.addValidator(/(.+)/, gettext('$LOGIN_FORM_PASS_ERR_MSG'));
		this.addInput(this.password);
		
		this.submit = new KButton(this, gettext('$LOGIN_FORM_SUBMIT'), 'submit', KForm.goSubmit);
		this.user.addCSSClass('KOAuthFormSubmitButton');
		this.appendChild(this.submit);
		
		KForm.prototype.draw.call(this);
	};
	
	KOAuthForm.prototype.onSuccess = function(data, request) {
		
		this.oauth.access_token = KOAuth.getToken(this.shell);
		
		var vars = '';
		for ( var att in this.oauth) {
			if (vars.length == 0) {
				vars += '?';
			}
			else {
				vars += '&';
			}
			vars += att + '=' + encodeURIComponent(this.oauth[att]);
		}
		var shell = this.getShellConfig();
		window.document.location = shell.providers.rest.services + 'oauth/authorize' + vars;
	};
	
	KOAuthForm.prototype.onValidate = function(action, params, status) {
		if (status) {
			this.action = 'rest:/oauth/' + this.user.getValue() + '/login';
			return {
				password : this.password.getValue()
			};
		}
		else {
			this.showAllErrors(params);
		}
		return false;
	};
	
	KOAuthForm.FORM_ELEMENT = 'form';
	
	KSystem.included('KOAuthForm');
});