function KPassword(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.panel.removeChild(this.input);
		delete this.input;
		
		this.input = window.document.createElement('input');
		this.input.setAttribute('type', 'password');
		this.input.name = id;
		this.input.id = id;
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KPassword.prototype = new KAbstractInput();
	KPassword.prototype.constructor = KPassword;
	
	KPassword.prototype.createInput = function() {
		return this.input;
	};
	
	KPassword.prototype.getValue = function() {
		if (!this.input.value || (this.input.value == '')) {
			return '';
		}
		else {
			return SHA1(this.input.value);
		}
	};
	
	KPassword.prototype.getRawValue = function() {
		if (!this.input.value || (this.input.value == '')) {
			return '';
		}
		else {
			return this.input.value;
		}
	};
	
	KPassword.prototype.setValue = function(value) {
		if (!value) {
			this.input.value = '';
		}
		else {
			this.input.value = value;
		}
	};
	
	KPassword.prototype.INPUT_ELEMENT = 'input';
	KPassword.LABEL_CONTAINER = 'label';
	KPassword.INPUT_ERROR_CONTAINER = 'errorArea';
	
	KSystem.included('KPassword');
});