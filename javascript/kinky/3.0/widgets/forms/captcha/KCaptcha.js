function KCaptcha(parent, label, id, params) {
	if (parent != null) {
		KAbstractInput.call(this, parent, 'text', label, id, true);

		this.image = new Image();
		this.image.className = 'KCaptchaImage';

		this.button = window.document.createElement('button');
		this.button.setAttribute('type', 'button');
		this.button.className = 'KCaptchaButton';
		this.button.innerHTML = 'Reload';
		KDOM.addEventListener(this.button, 'click', KCaptcha.reloadImage);

		this.data = params;
	}
}
KSystem.include( [ 'KAbstractInput' ], function() {

	KCaptcha.prototype = new KAbstractInput;
	KCaptcha.prototype.constructor = KCaptcha;

	KCaptcha.prototype.go = function() {
		this.load();
	};

	KCaptcha.prototype.load = function() {
		this.draw();
	};

	KCaptcha.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);

		var url = '';
		for ( var index in this.data) {
			url += '&' + index + '=' + this.data[index];
		}

		this.image.src = KCaptcha.CAPTCHA_SERVICE_URL + "?date=" + new Date().getMilliseconds() + url;
		this.inputContainer.appendChild(this.button);
		this.inputContainer.appendChild(this.image);
	};

	KCaptcha.reloadImage = function(event) {
		var widget = KDOM.getEventWidget(event);
		widget.refresh();
	};

	KCaptcha.IMAGE_ELEMENT = 'image';
	KCaptcha.CAPTCHA_SERVICE_URL = null;

	KSystem.included('KCaptcha');

});