function KMediaList(parent, params) {
	if (parent != null) {
		KAddOn.call(this, parent);
		KMediaList.selectedServer = '';
		KMediaList.selectedRoot = '';
		this.dialogProps = {
			modal : true,
			width : 800,
			scale : {
				height : 0.8
			},
			buttons : 'close'
		};
		this.params = params;
	}
}

KSystem.include([
	'KPanel',
	'KList',
	'KConfirmDialog',
	'KLink'
], function() {
	KMediaList.prototype = new KAddOn();
	KMediaList.prototype.constructor = KMediaList;
	
	KMediaList.prototype.setDialogProps = function(props) {
		this.dialogProps = props;
	};
	
	KMediaList.prototype.draw = function() {
		var kmlID = this.id;
		var mediaButton = window.document.createElement('button');
		mediaButton.className = 'KMediaListButton';
		mediaButton.innerHTML = gettext('$KMEDIALIST_OPEN');
		KDOM.addEventListener(mediaButton, 'click', function(event) {
			KDOM.stopEvent(event);
			Kinky.getWidget(kmlID).drawDialog();
		});
		
		if (this.parent.activated()) {
			this.parent.content.appendChild(mediaButton);
		}
		else {
			this.parent.container.appendChild(mediaButton);
		}
	};
	
	KMediaList.prototype.drawDialog = function() {
		var kmlID = this.id;
		
		var panel = new KPanel(this.getShell());
		panel.parentID = kmlID;
		panel.addCSSClass('KMediaList');
		
		panel.breadcrumbPanel = window.document.createElement('div');
		{
			panel.breadcrumbPanel.className = ' KMediaListHeader ';
		}
		panel.content.appendChild(panel.breadcrumbPanel);
		
		panel.actions = new KPanel(panel);
		panel.appendChild(panel.actions);
		this.createActionsPanel(panel.actions);
		
		panel.fileOptions = new KPanel(panel);
		panel.appendChild(panel.fileOptions);
		this.createFileOptionsPanel(panel.fileOptions);
		
		var ypURL = this.params.ypURL;
		var serverList = new KList(panel);
		serverList.setTitle(gettext('$KMEDIALIST_TITLE'));
		serverList.load = function() {
			this.kinky.get(this, ypURL, {});
		};
		
		serverList.onLoad = function(data, request) {
			this.data = {
				serverData : data
			};
			this.draw();
		};
		
		serverList.draw = function() {
			for ( var index in this.data.serverData) {
				if (index == 'http') {
					continue;
				}
				var serverLink = new KLink(this);
				serverLink.setLinkText(this.data.serverData[index]);
				serverLink.data = {
					serverIndex : index
				};
				serverLink.addEventListener('click', function(e) {
					var widget = KDOM.getEventWidget(e);
					var parentWidget = KDOM.getEventWidget(e, 'KPanel');
					
					if (KMediaList.selectedServer != widget.data.serverIndex) {
						KMediaList.selectedServer = widget.data.serverIndex;
						if (widget.parent.selectedServer) {
							widget.parent.selectedServer.removeCSSClass('Selected');
						}
						parentWidget.actions.setStyle({
							display : 'block'
						});
						widget.parent.selectedServer = widget;
						widget.addCSSClass('Selected');
						KMediaList.selectedRoot = '';
						parentWidget.fileList = parentWidget.fileList.refresh();
						parentWidget.actions.addFile.aditional_args = 'mediaServer=' + KMediaList.selectedServer + '&folderPath=' + KMediaList.selectedRoot;
					}
				});
				this.appendChild(serverLink);
			}
			KList.prototype.draw.call(this);
		};
		panel.appendChild(serverList);
		
		panel.invisible = function() {
			KMediaList.selectedServer = '';
			KMediaList.selectedRoot = '';
			if (!this.display) {
				return;
			}
			this.display = false;
		};
		
		panel.fileList = new KMediaFileList(panel);
		panel.fileList.config = {
			jsClass : 'KMediaFileList'
		};
		panel.appendChild(panel.fileList);
		
		KMediaList.parent = this.parent;
		var dialog = KFloatable.make(panel, this.dialogProps);
		{
			dialog.addCSSClass('KMediaFloatableDialog');
		}
		KFloatable.show(dialog);
	};
	
	KMediaList.prototype.createActionsPanel = function(container) {
		var widget = this;
		var kmlID = this.id;
		
		container.setStyle({
			display : 'none'
		});
		container.addCSSClass('KMediaListActions');
		{
			container.addFile = new KFile(container, gettext('$KMEDIALIST_NEWFILE'), 'newFile');
			container.addFile.setSize(206, 40);
			container.addFile.addFile = function(fileName, filePath) {
				KFile.prototype.addFile.call(this, fileName, filePath);
				widget.refreshFileList();
			}
			container.appendChild(container.addFile);
			
			var addFolderInput = new KInput(container, gettext('$KMEDIALIST_NEWFOLDER'), 'newFolder');
			var addFolderBt = window.document.createElement('button');
			addFolderBt.innerHTML = gettext('$KMEDIALIST_CREATEFOLDER');
			KDOM.addEventListener(addFolderBt, 'click', function(event) {
				var widget = KDOM.getEventWidget(event);
				if (widget.getValue() != '') {
					widget.removeCSSClass('KAbstractInputError');
					Kinky.getWidget(kmlID).createFolder(widget.getValue());
					widget.setValue('');
				}
				else {
					widget.addCSSClass('KAbstractInputError');
				}
			});
			container.appendChild(addFolderInput);
			addFolderInput.content.appendChild(addFolderBt);
		}
	};
	
	KMediaList.prototype.createFileOptionsPanel = function(container) {
		
		var kmlID = this.id;
		
		container.setTitle(gettext('$KMEDIALIST_SELECTEDFILES_OPTIONS'));
		container.setStyle({
			display : 'none'
		});
		container.addCSSClass('KMediaListFileOptions');
		{
			var removeBT = window.document.createElement('button');
			removeBT.className = 'KMediaListFileOptionsButton';
			removeBT.innerHTML = gettext('$KMEDIALIST_REMOVE');
			KDOM.addEventListener(removeBT, 'click', function(event) {
				var fileList = KDOM.getEventWidget(event, 'KPanel').parent.fileList;
				KDOM.stopEvent(event);
				Kinky.getWidget(kmlID).deleteFiles(fileList.elementsSelected);
			});
			container.content.appendChild(removeBT);
			
			container.externalBT = window.document.createElement('button');
			container.externalBT.className = 'KMediaListFileOptionsButton';
			container.externalBT.innerHTML = gettext('$KMEDIALIST_EXTERNAL');
			KDOM.addEventListener(container.externalBT, 'click', function(event) {
				var element = KDOM.getEventTarget(event);
				var widget = KDOM.getEventWidget(event);
				
				KDOM.stopEvent(event);
				
				var externalLink = new KInput(widget.getShell(), gettext('$KMEDIALIST_EXTERNAL_TEXT'), 'external');
				externalLink.addCSSClass('ExternalLink');
				externalLink.setValue(element.name);
				externalLink.input.readOnly = true;
				externalLink.addEventListener('click', function(ev) {
					var widget = KDOM.getEventWidget(ev);
					widget.input.select();
				});
				
				var dialog = KFloatable.make(externalLink, {
					modal : true,
					width : 300,
					scale : {
						height : 0.95
					}
				});
				{
					dialog.addCSSClass('KMediaListFloatableOptions');
				}
				KFloatable.show(dialog);
			});
			container.content.appendChild(container.externalBT);
			
			container.renameBT = window.document.createElement('button');
			container.renameBT.className = 'KMediaListFileOptionsButton';
			container.renameBT.innerHTML = gettext('$KMEDIALIST_RENAME');
			KDOM.addEventListener(container.renameBT, 'click', function(event) {
				var parentWidget = KDOM.getEventWidget(event);
				var element = KDOM.getEventTarget(event);
				element.style.display = 'none';
				parentWidget.childWidget('/media-actions-rename').setStyle({
					display : 'block'
				});
			});
			container.content.appendChild(container.renameBT);
			
			var renameInput = new KInput(container, gettext('$KMEDIALIST_RENAME'), 'rename');
			renameInput.hash = '/media-actions-rename';
			var renameInputBt = window.document.createElement('button');
			renameInputBt.innerHTML = gettext('$KMEDIALIST_RENAME_BT');
			KDOM.addEventListener(renameInputBt, 'click', function(event) {
				var widget = KDOM.getEventWidget(event);
				var fileList = KDOM.getEventWidget(event, 'KPanel').parent.fileList;
				var fileToRename = '';
				for ( var index in fileList.elementsSelected) {
					fileToRename = fileList.elementsSelected[index];
				}
				if (widget.getValue() != fileToRename) {
					Kinky.getWidget(kmlID).renameFile(fileToRename, widget.getValue());
				}
			});
			container.appendChild(renameInput);
			renameInput.content.appendChild(renameInputBt);
			renameInput.setStyle({
				display : 'none'
			});
		}
	};
	
	KMediaList.prototype.createFolder = function(name) {
		var params = {
			serverIndex : KMediaList.selectedServer,
			fileRoot : KMediaList.selectedRoot,
			folderName : name
		};
		Kinky.bunnyMan.post(this, 'rest:' + (!!Kinky.FILE_MANAGER_NEWFOLDER ? Kinky.FILE_MANAGER_NEWFOLDER : '/zepp/filemanager/newfolder'), params, null, 'refreshFileList');
	};
	
	KMediaList.prototype.deleteFiles = function(filesToDelete) {
		var self = this;

		KConfirmDialog.confirm({
			question : gettext('$KMEDIALIST_DELETE_CONFIRM'),
			callback : function() {
				var params = {
					serverIndex : KMediaList.selectedServer,
					fileRoot : KMediaList.selectedRoot,
					files : filesToDelete
				};
				Kinky.bunnyMan.post(self, 'rest:' + (!!Kinky.FILE_MANAGER_DELETE ? Kinky.FILE_MANAGER_DELETE : '/zepp/filemanager/deletefile'), params, null, 'refreshFileList');
			},
			shell : self.shell,
			modal : true,
			width : 400
		});
	};
	
	KMediaList.prototype.renameFile = function(fileName, newName) {
		var params = {
			serverIndex : KMediaList.selectedServer,
			fileRoot : KMediaList.selectedRoot,
			fileName : fileName,
			newName : newName
		};
		Kinky.bunnyMan.post(this, 'rest:' + (!!Kinky.FILE_MANAGER_RENAME ? Kinky.FILE_MANAGER_RENAME : '/zepp/filemanager/renamefile'), params, null, 'refreshFileList');
	};
	
	KMediaList.prototype.refreshFileList = function(data) {
		KBreadcrumb.dispatchEvent(null, {
			action : '/refresh-list'
		});
	};
	
	KSystem.registerAddOn('KMediaList');
	KSystem.included('KMediaList');
}, Kinky.ZEPP_INCLUDER_URL);

function KMediaFileList(parent) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.addActionListener(KMediaFileList.refreshList, [
			'/refresh-list'
		]);
		this.elementsSelected = {};
	}
}

KSystem.include([
	'KPanel',
	'KButton'
], function() {
	KMediaFileList.prototype = new KPanel();
	KMediaFileList.prototype.constructor = KMediaFileList;
	
	KMediaFileList.refreshList = function(widget) {
		widget.parent.fileList = widget.refresh();
	};
	
	KMediaFileList.prototype.load = function() {
		this.fileListData = [];
		if (!KMediaList.selectedServer || (!KMediaList.selectedServer != '')) {
			this.draw();
			return;
		}
		this.kinky.get(this, 'rest:' + (!!Kinky.FILE_MANAGER_LIST ? Kinky.FILE_MANAGER_LIST : '/zepp/filemanager/filelist'), {
			serverIndex : KMediaList.selectedServer,
			fileRoot : KMediaList.selectedRoot
		});
	};
	
	KMediaFileList.prototype.refresh = function() {
		var widget = KWidget.prototype.refresh.call(this);
		widget.content.style.visibility = 'hidden';
		widget.container.style.visibility = 'hidden';
		widget.manageSelected(null, false);
		return widget;
	};
	
	KMediaFileList.prototype.onLoad = function(data) {
		if (!!data) {
			this.fileListData = data;
		}
		this.draw();
	};
	
	KMediaFileList.prototype.draw = function() {
		this.content.style.visibility = 'visible';
		this.container.style.visibility = 'visible';
		
		if (!KMediaList.selectedServer) {
			var noItems = window.document.createElement('div');
			noItems.className = ' ZeppListNoItems ';
			noItems.appendChild(window.document.createTextNode(gettext('$KMEDIALIST_NO_SERVER')));
			this.content.appendChild(noItems);
		}
		else {
			this.refreshBreadcrumb();
			
			if (this.fileListData.length > 0) {
				for ( var index in this.fileListData) {
					if (!!this.fileListData[index].mime && /directory/.test(this.fileListData[index].mime)) {
						var element = new KButton(this, '', 'filesystem' + index, function(event) {
							var widget = KDOM.getEventWidget(event);
							widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
						});
						element.setText('<i class="directory fa fa-folder-open-o"></i> ' + this.fileListData[index].title, true);
						
						element.addEventListener('dblclick', function(event) {
							var widget = KDOM.getEventWidget(event);
							widget.parent.changeRoot(widget.data.fileURL);
						});
						element.data = {
							fileURL : this.fileListData[index].url
						};
						element.hash = '/list-element/' + element.id;
						this.appendChild(element);
					}
				}
				for ( var index in this.fileListData) {
					if (!!this.fileListData[index].mime && !(/directory/.test(this.fileListData[index].mime))) {
						var element = null;
						var category = this.fileListData[index].mime.split(';')[0].split('/');
						var type = category[1];
						category = category[0];
						
						switch (category) {
							case 'image': {
								element = new KImage(this);
								element.setDescription(this.fileListData[index].title);
								element.setImage(this.fileListData[index].url);
								element.addEventListener('click', function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								break;
							}
							case 'audio': {
								var element = new KButton(this, '', 'filesystem' + index, function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								element.setText('<i class="audio fa-music"></i> ' + this.fileListData[index].title, true);
								break;
							}
							case 'video': {
								var element = new KButton(this, '', 'filesystem' + index, function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								element.setText('<i class="video fa fa-film"></i> ' + this.fileListData[index].title, true);
								break;
							}
							case 'text': {
								var element = new KButton(this, '', 'filesystem' + index, function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								element.setText('<i class="office fa fa-file-o"></i> ' + this.fileListData[index].title, true);
								break;
							}
							case 'application': {
								var element = new KButton(this, '', 'filesystem' + index, function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								switch (type) {
									case 'msword' :
									case 'vnd.ms-powerpoint' :
									case 'vnd.ms-excel' :
									case 'pdf' : {
										element.setText('<i class="office fa fa-file-o"></i> ' + this.fileListData[index].title, true);
										break;
									}
									default : {
										element.setText('<i class="general fa fa-file"></i> ' + this.fileListData[index].title, true);
									}
								}
								break;
							}
							default: {
								var element = new KButton(this, '', 'filesystem' + index, function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								element.setText('<i class="general fa fa-file"></i> ' + this.fileListData[index].title, true);
								break;
							}
						}
						element.addEventListener('dblclick', function(event) {
							KDOM.stopEvent(event);
							var widget = KDOM.getEventWidget(event);
							widget.parent.selectFile(widget.data.fileURL);
						});
						element.data = {
							fileURL : this.fileListData[index].url
						};
						element.hash = '/list-element/' + element.id;
						this.appendChild(element);
					}
				}
			}
			else {
				var noItems = window.document.createElement('div');
				noItems.className = ' ZeppListNoItems ';
				noItems.appendChild(window.document.createTextNode(gettext('$ZEPP_LIST_NO_ITEMS')));
				this.content.appendChild(noItems);
			}
		}
		
		KPanel.prototype.draw.call(this);
	};
	
	KMediaFileList.prototype.selectFile = function(fileURL) {
		KMediaList.parent.setValue(fileURL);
	};
	
	KMediaFileList.prototype.breadcrumbNavigation = function(place) {
		var newRoot = '';
		var splittedBC = KMediaList.selectedRoot.split('/');
		for ( var index in splittedBC) {
			if (index > 0) {
				newRoot += '/' + splittedBC[index];
			}
			if (splittedBC[index] == place) {
				KMediaList.selectedRoot = newRoot;
				this.parent.actions.addFile.aditional_args = 'mediaServer=' + KMediaList.selectedServer + '&folderPath=' + KMediaList.selectedRoot;
				this.parent.fileList = this.refresh();
				break;
			}
		}
	};
	
	KMediaFileList.prototype.upInRoot = function() {
		var newRoot = KMediaList.selectedRoot.substring(0, KMediaList.selectedRoot.lastIndexOf('/'));
		if (KMediaList.selectedRoot != '') {
			this.parent.actions.addFile.aditional_args = 'mediaServer=' + KMediaList.selectedServer + '&folderPath=' + KMediaList.selectedRoot;
			KMediaList.selectedRoot = newRoot;
			this.parent.fileList = this.refresh();
		}
	};
	
	KMediaFileList.prototype.changeRoot = function(root) {
		KMediaList.selectedRoot += root.substring(root.lastIndexOf('/'), root.length);
		this.parent.actions.addFile.aditional_args = 'mediaServer=' + KMediaList.selectedServer + '&folderPath=' + KMediaList.selectedRoot;
		this.parent.fileList = this.refresh();
	};
	
	KMediaFileList.prototype.refreshBreadcrumb = function() {
		var parent = this.parent;
		parent.breadcrumbPanel.innerHTML = '';
		var upInRoot = window.document.createElement('button');
		upInRoot.innerHTML = gettext('$KMEDIALIST_BACK');
		KDOM.addEventListener(upInRoot, 'click', function(event) {
			var parentWidget = KDOM.getEventWidget(event, 'KPanel');
			parentWidget.fileList.upInRoot();
		});
		parent.breadcrumbPanel.appendChild(upInRoot);
		var splittedBC = KMediaList.selectedRoot.split('/');
		
		var breadcrumbDiv = window.document.createElement('div');
		for ( var index in splittedBC) {
			var link = window.document.createElement('a');
			link.innerHTML = '/' + (index == 0 ? gettext('$KMEDIALIST_ROOT') : splittedBC[index]);
			link.alt = (index == 0 ? '' : splittedBC[index]);
			KDOM.addEventListener(link, 'click', function(event) {
				var element = KDOM.getEventTarget(event);
				var widgetParent = KDOM.getEventWidget(event, 'KPanel');
				widgetParent.fileList.breadcrumbNavigation(element.alt);
			});
			breadcrumbDiv.appendChild(link);
		}
		parent.breadcrumbPanel.appendChild(breadcrumbDiv);
	};
	
	KMediaFileList.prototype.manageSelected = function(widget, controlFlag) {
		var count = 0;
		for ( var selected in this.elementsSelected) {
			if (!!this.elementsSelected[selected]) {
				count++;
			}
		}
		
		if (!!widget) {
			if (this.elementsSelected[widget.hash]) {
				if (!controlFlag) {
					this.elementsSelected = {};
					if (count != 1) {
						var fileName = widget.data.fileURL.substring(widget.data.fileURL.lastIndexOf('/') + 1, widget.data.fileURL.length);
						this.elementsSelected[widget.hash] = fileName;
					}
				}
				else {
					delete this.elementsSelected[widget.hash];
				}
			}
			else {
				if (!controlFlag) {
					this.elementsSelected = {};
				}
				var fileName = widget.data.fileURL.substring(widget.data.fileURL.lastIndexOf('/') + 1, widget.data.fileURL.length);
				this.elementsSelected[widget.hash] = fileName;
			}
			var siblling = widget.parent.childWidgets();
			for ( var index in siblling) {
				if (this.elementsSelected[siblling[index].hash]) {
					siblling[index].addCSSClass(siblling[index].className + 'Selected');
				}
				else {
					siblling[index].removeCSSClass(siblling[index].className + 'Selected');
				}
			}
			count = 0;
			for ( var selected in this.elementsSelected) {
				if (!!this.elementsSelected[selected]) {
					count++;
				}
			}
		}
		
		if (count > 1) {
			this.parent.fileOptions.setStyle({
				display : 'block'
			});
			this.parent.fileOptions.externalBT.style.display = 'none';
			this.parent.fileOptions.renameBT.style.display = 'none';
		}
		else if (count == 1) {
			this.parent.fileOptions.setStyle({
				display : 'block'
			});
			
			for ( var index in this.elementsSelected) {
				var externalLink = widget.parent.childWidget(index).data.fileURL;
				this.parent.fileOptions.externalBT.name = externalLink;
				
				var fileName = this.elementsSelected[index];
				this.parent.fileOptions.childWidget('/media-actions-rename').setValue(fileName);
				break;
			}
			this.parent.fileOptions.renameBT.style.display = 'block';
			this.parent.fileOptions.externalBT.style.display = 'block';
			this.parent.fileOptions.childWidget('/media-actions-rename').setStyle({
				display : 'none'
			});
		}
		else {
			this.parent.fileOptions.setStyle({
				display : 'none'
			});
		}
	};
	
	KSystem.included('KMediaFileList');
}, Kinky.ZEPP_INCLUDER_URL);