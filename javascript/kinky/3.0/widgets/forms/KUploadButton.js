function KUploadButton(parent, label, id, callback, options) {
	if (parent != null) {
		KWidget.call(this, parent);
		
		this.callback = callback;
		this.label = label;
		this.options = options || { };

		this.addCSSClass('KButton', this.panel);
		this.addCSSClass('KButton', this.content);
		this.addCSSClass('KButton', this.container);

		this.input = window.document.createElement('button');
		this.input.className = ' KButtonInput ';
		this.input.setAttribute('type', 'button');
		KCSS.setStyle({
			top : '0',
			left : '0'
		}, [
			this.input
		]);
		KDOM.addEventListener(this.input, 'click', callback);
		this.inputID = this.input.id = id;

		this.uploader = window.document.createElement('input');
		this.uploader.setAttribute('type', 'file');
		if (this.multiple) {
			this.uploader.setAttribute('multiple', 'multiple');
		}
		this.uploader.id = this.id + '_uploader';
		KCSS.setStyle({
			position : 'absolute',
			top : '0',
			left : '0',
			opacity : '0'
		}, [
			this.uploader
		]);

		KDOM.addEventListener(this.uploader, 'change', KUploadButton.changed);
		KDOM.addEventListener(this.uploader, 'mouseover', function(event) {
			var widget = KDOM.getEventWidget(event);
			widget.addCSSClass('KButtonOver', widget.input);
		});
		KDOM.addEventListener(this.uploader, 'mouseout', function(event) {
			var widget = KDOM.getEventWidget(event);
			widget.removeCSSClass('KButtonOver', widget.input);
		});
		
		var labelDiv = window.document.createElement('div');
		labelDiv.className = ' KButtonInputLabel ';
		labelDiv.appendChild(window.document.createTextNode(label));
		this.input.appendChild(labelDiv);

		this.progresswin = window.document.createElement('div');
		{
			this.progresswin.className = ' KButtonProgress ';
			KCSS.setStyle({
				position: 'fixed',
				zIndex : '9999'
			}, [
			this.progresswin
			]);

			this.progressbar = window.document.createElement('div');
			{
				this.progressbar.className = ' KButtonProgressBar ';
				KCSS.setStyle({
					width : '0'
				}, [
				this.progressbar
				]);
			}
			this.progresswin.appendChild(this.progressbar);

			this.progresstext = window.document.createElement('div');
			{
				this.progresstext.className = ' KButtonProgressText ';
			}
			this.progresswin.appendChild(this.progresstext);

			var label = window.document.createElement('div');
			{
				label.className = ' KButtonProgressLabel ';
				label.innerHTML = gettext('$KUPLOADBUTTON_UPLOADING_TEXT')
			}
			this.progresswin.appendChild(label);
		}
		
		this.INPUT_ELEMENT = 'input';
	}
}

KSystem.include([
	'KWidget'
], function() {
	KUploadButton.prototype = new KWidget();
	KUploadButton.prototype.constructor = KUploadButton;
	KUploadButton.prototype.isLeaf = true;
	
	KUploadButton.prototype.getInputID = function() {
		return this.inputID;
	};
	
	KUploadButton.prototype.enable = function() {
		this.input.disabled = '';
	};
	
	KUploadButton.prototype.disable = function() {
		this.input.disabled = 'disabled';
	};
	
	KUploadButton.prototype.setSize = function(width, height) {
		KCSS.setStyle({
			width : width + 'px',
			height : height + 'px'
		}, [
			this.panel,
			this.content,
			this.input,
			this.uploader
		]);
	};

	KUploadButton.prototype.go = function() {
		this.gone = true;
		if (!this.activated()) {
			this.draw();
		}
		else {
			this.redraw();
		}
	};
	
	KUploadButton.prototype.setCallback = function(callback) {
		KDOM.removeEventListener(this.input, 'click', this.callback);
		KDOM.addEventListener(this.input, 'click', callback);
		this.callback = callback;
	};
	
	KUploadButton.prototype.setText = function(text, html) {
		var labelDiv = this.input.childNodes[0];
		labelDiv.innerHTML = '';
		if (html) {
			labelDiv.innerHTML = text;
		}
		else {
			labelDiv.appendChild(window.document.createTextNode(text));
		}
	};
	
	KUploadButton.prototype.getText = function(html) {
		if (html) {
			return this.input.childNodes[0].innerHTML;
		}
		else {
			return this.input.childNodes[0].textContent;
		}
	};
	
	KUploadButton.prototype.setHelpText = function(text) {
		this.helpText = text;
	};
	
	KUploadButton.prototype.draw = function() {
		this.content.appendChild(this.input);
		this.content.appendChild(this.uploader);
		this.activate();
	};
	
	KUploadButton.prototype.upload = function(data, file, callback) {
		var widget = this;
		var channel = null;
		if (window.XMLHttpRequest) { 
			channel = new window.XMLHttpRequest();
		}
		else if (window.ActiveXObject) { 
			try {
				channel = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) {
				try {
					channel = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1) {
				}
			}
		}

		channel.onreadystatechange = function() {
			if (channel.readyState == 4) {
				if (!!channel.upload) {
					window.document.body.removeChild(widget.progresswin);				
				}
				widget.enable();

				if (channel.status == 0) {
					return;
				}

				if (channel.status == 200) {
					var resp = JSON.parse(channel.responseText);
					widget.uploaded_data = resp;
					KDOM.fireEvent(widget.input, 'click');
				}
			}
		};

		var boundary = "----KFILEUpload" + (new Date()).getTime();

		channel.open('POST', (!!this.options.endpoint ? this.options.endpoint : Kinky.FILE_MANAGER_UPLOAD) + '?ref=' + encodeURIComponent(this.fileref) + (!!this.options.aditional_args ? '&' + this.options.aditional_args : ''), true);
		channel.setRequestHeader("Authorization", !!this.options.access_token ? 'OAuth2.0 ' + this.options.access_token : KOAuth.getAuthorization().authentication);
		channel.setRequestHeader("X-Requested-With", "XHR");
		channel.setRequestHeader("Pragma", "no-cache");
		channel.setRequestHeader("Cache-Control", "no-store");
		channel.setRequestHeader("Content-Type", "multipart/form-data; boundary=" + boundary);

		var content = '--' + boundary + '\r\n';
		content += 'Content-Encoding: Base64\r\n';
		content += 'Content-Disposition: form-data; name="addFile"; filename="' + file.name + '"\r\n';
		content += 'Content-Type: ' + file.type + '\r\n\r\n';
		content += data.split(',')[1] + '\r\n';
		content += '--' + boundary + '--'

		try {
			if (!!channel.upload) {
				widget.progressbar.style.width = '0%';
				widget.progresstext.innerHTML = '0%';
				window.document.body.appendChild(widget.progresswin);
				channel.upload.addEventListener("progress", function(e) {
					KUploadButton.progress(e, widget, true);
				}, false);
			}
			channel.send(content);
		}
		catch (e) {
		}

	};
	
	KUploadButton.progress = function(evt, widget, upload) {

		if (evt.lengthComputable) {
			var percentLoaded = Math.round((evt.loaded / evt.total) * 100);

			if (percentLoaded < 100) {
				widget.progressbar.style.width = percentLoaded + '%';
				widget.progresstext.innerHTML = percentLoaded + '%';
			}
			else if (!upload) {
				window.document.body.removeChild(widget.progresswin);				
			}
		}
	};

	KUploadButton.changed = function(event) {
		var widget = KDOM.getEventWidget(event);
		
		var files = event.target.files; // FileList object
		if (files === undefined) {
			return;
		}

		widget.disable();

		for (var i = 0, f; f = files[i]; i++) {
			widget.progressbar.style.width = '0%';
			widget.progresstext.innerHTML = '0%';
			window.document.body.appendChild(widget.progresswin);
			
			var reader = new FileReader();
			reader.file = f;
			reader.onprogress = function(e) {
				KUploadButton.progress(e, widget);
			};

			reader.onload = (function(theFile) {
				return function(e) {
					widget.upload(e.target.result, e.target.file, KFile.uploaded);
				};
			})(f);

			reader.readAsDataURL(f);
		}
	};

	KUploadButton.BUTTON_ELEMENT = 'input';
	
	KSystem.included('KUploadButton');
});