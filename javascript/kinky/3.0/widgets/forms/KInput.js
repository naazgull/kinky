function KInput(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.panel.removeChild(this.input);
		delete this.input;
		
		this.input = window.document.createElement('input');
		this.input.setAttribute('type', 'text');
		this.input.name = id;
		this.input.id = id;
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KInput.prototype = new KAbstractInput();
	KInput.prototype.constructor = KInput;
	
	KInput.prototype.setLength = function(length) {
		this.input.size = this.input.maxLength = length;
		this.addCSSClass('KInput' + (length < 5 ? 'Small' : (length < 9 ? 'Medium' : 'Big')) + 'Length');
	};
	
	KInput.prototype.createInput = function() {
		return this.input;
	};
	
	KInput.prototype.getValue = function() {
		return this.input.value;
	};
	
	KInput.prototype.setValue = function(value) {
		if (!value) {
			this.input.value = '';
		}
		else {
			this.input.value = value;
		}
	};
	
	KInput.prototype.INPUT_ELEMENT = 'input';
	KInput.LABEL_CONTAINER = 'label';
	KInput.INPUT_ERROR_CONTAINER = 'errorArea';
	
	KSystem.included('KInput');
});