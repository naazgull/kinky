function KImageUpload(parent, label, id, options) {
	if (parent != null) {
		KFile.call(this, parent, label, id, options);
		this.maxFile = 1;
		this.noPrompt = true;
		this.metadata = undefined;
		this.onChangeCallback = [];
	}
}

KSystem.include([
	'KFile'
], function() {
	
	KImageUpload.prototype = new KFile();
	KImageUpload.prototype.constructor = KImageUpload;
	
	KImageUpload.prototype.visible = function() {
		KFile.prototype.visible.call(this);
		this.updateImage();
	};

	KImageUpload.prototype.removeEventListener = function(eventName, callback, target) {
		if (eventName == 'change') {
			if (this.onChangeCallback.length != 0) {
				for (var c = 0; c != this.onChangeCallback.length; c++) {
					if (this.onChangeCallback[c].toString() == callback.toString()) {
						this.onChangeCallback.splice(c, 1);
						break;		
					}
				}
				if (this.onChangeCallback.length == 0) {
					KDOM.removeEventListener(this.button, 'click', KImageUpload.onChangeCallback);
				}
			}
		}
		else if (eventName == 'imageload') {
			KFile.prototype.removeEventListener.call(this, 'propertychange', callback, this.button)
		}
		else {
			KFile.prototype.removeEventListener.call(this, eventName, callback, target);
		}
	};

	KImageUpload.prototype.addEventListener = function(eventName, callback, target) {
		if (eventName == 'change') {
			if (this.onChangeCallback.length == 0) {
				KFile.prototype.addEventListener.call(this, eventName, KImageUpload.onChangeCallback, target);
			}
			this.onChangeCallback.push(callback);
		}
		else if (eventName == 'imageload') {
			KFile.prototype.addEventListener.call(this, 'propertychange', callback, this.button)
		}
		else {
			KFile.prototype.addEventListener.call(this, eventName, callback, target);
		}
	};

	KImageUpload.prototype.setMetadata = function(metadata) {
		this.metadata = metadata;
	};
	
	KImageUpload.prototype.getMetadata = function() {
		return this.metadata;
	};
	
	KImageUpload.prototype.draw = function() {		
		this.addCSSClass(gettext('$KIMAGEUPLOAD_NO_IMAGE_THUMB'), this.content);
		KFile.prototype.draw.call(this);
		
		this.button.innerHTML = gettext('$KIMAGEUPLOAD_BUTTON_UPLOAD_TEXT');
		
	};
	
	KImageUpload.prototype.updateValue = function() {
		KFile.prototype.updateValue.call(this);

		if (!this.getValue() || (this.getValue() == '')) {
			this.addCSSClass(gettext('$KIMAGEUPLOAD_NO_IMAGE_THUMB'), this.content);
			this.buttonLabel.innerHTML = gettext('$KFILE_BUTTON_LABEL_ADD_TEXT');
		}
		else {
			this.removeCSSClass(gettext('$KIMAGEUPLOAD_NO_IMAGE_THUMB'), this.content);
			this.buttonLabel.innerHTML = gettext('$KFILE_BUTTON_LABEL_CHANGE_TEXT');
		}
		this.updateImage();
	};

	KImageUpload.prototype.updateImage = function() {
		var w = this;
		this.width = 0;
		if (!!this.loadTimer) {
			KSystem.removeTimer(this.loadTimer);
			delete this.loadTimer;
		}
		this.loadTimer = KSystem.addTimer(function() {
			if (!w.fileListContainer) {
				if (!!w.contentContainer) {
					w.contentContainer.style.width = 'auto';
				}
				return;
			}

			var imgs = w.fileListContainer.getElementsByTagName('img');
			for (var i = 0; i != imgs.length; i++) {
				var s = KCSS.getComputedStyle(imgs[i]);
				var width = Math.round(KDOM.normalizePixelValue(s.width));
				var height = Math.round(KDOM.normalizePixelValue(s.height));
				if (!width || !height || width <= 0 || height <= 0) {
					return;
				}
				w.width = Math.max(w.width, width);
			}

			KSystem.removeTimer(w.loadTimer);
			delete w.loadTimer;
			w.contentContainer.style.width = (w.width != 0 ? w.width + 'px' : 'auto');
			KDOM.fireEvent(w.button, 'propertychange');
		}, 500, true);
	};
	
	KImageUpload.prototype.addFile = function(fileName, filePath) {
		var inputs = this.fileListContainer.getElementsByTagName('input');
		var parent = this.fileListContainer;
		for (; parent.childNodes.length != 0;) {
			parent.removeChild(parent.childNodes[0]);
		}
		
		var newFileContainer = window.document.createElement('div');
		
		var newFileLabel = window.document.createElement('div');
		{
			var img = window.document.createElement('img');
			img.src = filePath + '?etag=' + (new Date()).getTime();
			newFileLabel.appendChild(img);
		}
		newFileContainer.appendChild(newFileLabel);
		
		var newFile = window.document.createElement('input');
		{
			newFile.type = 'hidden';
			newFile.name = this.id + '[]';
			newFile.id = this.id + '_' + inputs.length;
			newFile.value = filePath;
		}
		newFileContainer.appendChild(newFile);
		
		var newFileRemove = window.document.createElement('button');
		{
			newFileRemove.setAttribute('type', 'button');
		}
		newFileContainer.appendChild(newFileRemove);
		
		KDOM.addEventListener(newFileRemove, 'click', function(event) {
			var widget = KDOM.getEventWidget(event);
			var target = KDOM.getEventTarget(event);
			target.parentNode.parentNode.removeChild(target.parentNode);
			widget.setValue(undefined);
			widget.updateValue();
		});
		
		newFileContainer.appendChild(window.document.createElement('br'));
		parent.appendChild(newFileContainer);
		if (this.activated()) {
			this.updateValue();
			KDOM.fireEvent(this.button, 'click');
		}
	};

	KImageUpload.onChangeCallback = function(e) {
		var self = KDOM.getEventWidget(e);
		for (var c in self.onChangeCallback) {
			self.onChangeCallback[c].call(null, e);
		}
	};

	KSystem.included('KImageUpload');	
});
