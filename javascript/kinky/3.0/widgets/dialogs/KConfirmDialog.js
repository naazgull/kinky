function KConfirmDialog(target, params) {
	if (target != null) {
		KFloatable.call(this, target, params);
	}
}
function KConfirmPanel(params) {
	if (params != null) {
		KPanel.call(this, Kinky.getShell(params.shell));
		this.config = params;
	}
}

KSystem.include([
	'KFloatable',
	'KButton',
	'KPanel'
], function() {
	KConfirmDialog.prototype = new KFloatable();
	KConfirmDialog.prototype.constructor = KConfirmDialog;
	
	KConfirmDialog.confirm = function(params) {
		params.modal = true;
		
		var target = new KConfirmPanel(params);
		var dialog = KFloatable.make(target, params);
		dialog.addCSSClass('KConfirmDialog');
		KFloatable.show(dialog);
	};
	
	KSystem.included('KConfirmDialog');
	
	KConfirmPanel.prototype = new KPanel();
	KConfirmPanel.prototype.constructor = KConfirmPanel;
	
	KConfirmPanel.prototype.onOpen = function() {
		this.childWidget('/buttons/ok').focus();
	};

	KConfirmPanel.prototype.invisible = function() {
		if (!!this.config.onClose) {
			this.config.onClose();
		}
		KPanel.prototype.invisible.call(this);
	};

	KConfirmPanel.prototype.load = function() {
		this.draw();
	};
	
	KConfirmPanel.prototype.draw = function() {
		var text = window.document.createElement('div');
		{
			if (!!this.config.html) {
				text.innerHTML = this.config.question;
			}
			else {
				text.appendChild(window.document.createTextNode(this.config.question));
			}
			text.className = ' KConfirmDialogQuestionText ';
		}
		this.container.appendChild(text);

		var nogo = new KButton(this, gettext('$KCONFIRMDIALOG_CANCEL_BUTTON_LABEL'), 'nogo', function(event) {
			KDOM.stopEvent(event);
			
			KDOM.getEventWidget(event).parent.parent.closeMe();
		});
		{
			nogo.hash = '/buttons/cancel';
			nogo.addCSSClass('KConfirmDialogCancelButton');
		}
		this.appendChild(nogo);

		var go = new KButton(this, gettext('$KCONFIRMDIALOG_OK_BUTTON_LABEL'), 'go', function(event) {
			KDOM.stopEvent(event);
			
			var panel = KDOM.getEventWidget(event).parent;
			panel.config.callback();
			panel.parent.closeMe();
		});
		{
			go.hash = '/buttons/ok';
			go.addCSSClass('KConfirmDialogOKButton');
		}
		this.appendChild(go);

		KPanel.prototype.draw.call(this);
	};
	
	KSystem.included('KConfirmPanel');
});