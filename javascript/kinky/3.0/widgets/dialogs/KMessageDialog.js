function KMessageDialog(target, params) {
	if (target != null) {
		KFloatable.call(this, target, params);
	}
}
function KMessagePanel(params) {
	if (params != null) {
		KPanel.call(this, Kinky.getShell(params.shell));
		this.config = params;
	}
}

KSystem.include([
	'KFloatable',
	'KButton',
	'KPanel'
], function() {
	KMessageDialog.prototype = new KFloatable();
	KMessageDialog.prototype.constructor = KMessageDialog;
	
	KMessageDialog.alert = function(params) {
		params.modal = false;
		
		var target = new KMessagePanel(params);
		var dialog = KFloatable.make(target, params);
		dialog.addCSSClass('KMessageDialog');
		KMessageDialog.showOverlay();
		KFloatable.show(dialog);
	};
	
	KSystem.included('KMessageDialog');
	
	KMessagePanel.prototype = new KPanel();
	KMessagePanel.prototype.constructor = KMessagePanel;

	KMessagePanel.prototype.onOpen = function() {
		this.childWidget('/buttons/ok').focus();
	};

	KMessagePanel.prototype.invisible = function() {
		if (!!this.config.onClose) {
			this.config.onClose();
		}
		KPanel.prototype.invisible.call(this);
		KMessageDialog.hideOverlay();
	};
	
	KMessagePanel.prototype.load = function() {
		this.draw();
	};
	
	KMessagePanel.prototype.draw = function() {
		var text = window.document.createElement('div');
		{
			if (!!this.config.html) {
				text.innerHTML = this.config.message;
			}
			else {
				text.appendChild(window.document.createTextNode(this.config.message));
			}
			text.className = ' KMessageDialogMessageText ';
		}
		this.container.appendChild(text);
		
		var ok = new KButton(this, gettext('$KMESSAGEDIALOG_OK_BUTTON_LABEL'), 'ok', function(event) {
			KDOM.stopEvent(event);
			
			KDOM.getEventWidget(event).parent.parent.closeMe();
		});
		{
			ok.hash = '/buttons/ok';
			ok.addCSSClass('KMessageDialogOKButton');
		}
		this.appendChild(ok);
		
		KPanel.prototype.draw.call(this);
	};
	

	KMessageDialog.showOverlay = function() {
		if (!KMessageDialog.overlay.parentNode) {
			Kinky.getShell().panel.appendChild(KMessageDialog.overlay);
		}
		KMessageDialog.overlay.style.display = 'block';
	};
	
	KMessageDialog.hideOverlay = function(force) {
		KMessageDialog.overlay.style.display = 'none';	
	};

	KMessageDialog.overlay = window.document.createElement('div');
	KMessageDialog.overlay.appendChild(window.document.createTextNode(' '));
	KMessageDialog.overlay.className = ' KShellOverlay KMessageDialogOverlay ';
	KCSS.setStyle({
		width : '100%',
		height : '100%',
		position : 'fixed',
		opacity : '1',
		top : '0',
		left : '0',
		display : 'none'
	}, [ KMessageDialog.overlay ]);

	KSystem.included('KMessagePanel');
});