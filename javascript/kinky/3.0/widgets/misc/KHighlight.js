function KHighlight(parent) {
	if (parent != null) {
		this.isLeaf = true;
		KWidget.call(this, parent);
		
		this.link = window.document.createElement('a');
		this.link.target = '_self';
		this.link.href = 'javascript:void(0)';
		this.title = window.document.createElement('div');
		this.text = window.document.createElement('div');
		this.image = window.document.createElement('img');
	}
}

KSystem.include([
	'KTooltip',
	'KWidget'
], function() {
	KHighlight.prototype = new KWidget;
	KHighlight.prototype.constructor = KHighlight;
	
	KHighlight.prototype.setHighlightTitle = function(text, isHTML) {
		if (text) {
			if (isHTML) {
				this.title.innerHTML = text;
			}
			else {
				this.title.innerHTML = '';
				this.title.appendChild(window.document.createTextNode(text));
			}
		}
	};
	
	KHighlight.prototype.setHighlightText = function(text, isHTML) {
		if (text) {
			if (isHTML) {
				this.text.innerHTML = text;
			}
			else {
				this.text.innerHTML = '';
				this.text.appendChild(window.document.createTextNode(text));
			}
		}
	};
	
	KHighlight.prototype.setHighlightImage = function(imageURL) {
		if (imageURL) {
			this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(imageURL) : imageURL;
		}
	};
	
	KHighlight.prototype.setHighlightURL = function(newURL) {
		if (newURL) {
			this.link.href = newURL;
		}
	};
	
	KHighlight.prototype.setHighlightTarget = function(target) {
		if (target) {
			this.link.target = target;
		}
	};
	
	KHighlight.prototype.setTooltip = function(tooltip) {
		if (tooltip) {
			this.data.tooltip = tooltip;
		}
	};
	
	KHighlight.prototype.initHighlight = function() {
		if (!!this.data.url) {
			this.setHighlightURL(this.data.url);
			
			if (!!this.data.targetWindow) {
				this.setHighlightTarget(this.data.targetWindow);
			}
		}
		
		if (!!this.data.tooltip) {
			this.setTooltip(this.data.tooltip);
		}
		
		if (!!this.data.title) {
			this.setHighlightTitle(this.data.title);
		}
		
		if (!!this.data.description) {
			this.setHighlightText(this.data.description, true);
		}
		
		if (!!this.data.image) {
			this.setHighlightImage(this.data.image);
		}
	};
	
	KHighlight.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		if (!!this.data) {
			this.initHighlight();
		}
		
		if (!!this.data && !!this.data.graphics && !!this.data.graphics.background) {
			this.setBackground(this.data.graphics.background);
		}
		
		this.title.className = ' KHighlightTitle ';
		this.text.className = ' KHighlightText ';
		this.image.className = ' KHighlightImage ';
		
		if ((!!this.data && !!this.data.image) || (this.image.src != '')) {
			this.link.appendChild(this.image);
		}
		
		if ((this.title.innerHTML.length == 0) && (!!this.data && !!this.data.title)) {
			this.setHighlightTitle(this.data.title, true);
		}
		this.link.appendChild(this.title);
		
		if ((this.text.innerHTML.length == 0) && (!!this.data && !!this.data.description)) {
			this.setHighlightText(this.data.description, true);
		}
		this.link.appendChild(this.text);
		
		if (!!this.data && !!this.data.tooltip) {
			this.hasTooltip = true;
			// KDOM.addEventListener(this.link, 'mouseover', KHighlight.onMouseOver);
		}
		
		this.content.appendChild(this.link);
		
		if (!!this.data && !!this.data.actions) {
			var ac;
			
			if (!!this.data.actions.length) {
				ac = this.data.actions;
			}
			else {
				ac = [
					this.data.actions
				];
			}
			
			for ( var link_data_index in ac) {
				var link_data = ac[link_data_index];
				
				var link = new KLink(this);
				var url = (!!link_data.url ? link_data.url : link_data.hash);
				if (!!url) {
					link.setLinkURL(url);
					
					if (!!link_data.title) {
						link.setLinkText(link_data.title);
					}
					
					this.appendChild(link);
					
					link.go();
				}
			}
		}
		
		this.activate();
	};
	
	KHighlight.onMouseOver = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var widget = KDOM.getEventWidget(event);
		
		if (widget.hasTooltip) {
			KTooltip.showTooltip(widget.link, {
				text : widget.data.tooltip,
				isHTML : true,
				offsetX : 15,
				offsetY : 15,
				left : KDOM.mouseX(mouseEvent),
				top : (KDOM.mouseY(mouseEvent) + 15),
				cssClass : 'KHighlightTooltip'
			});
		}
	};
	
	KHighlight.LINK_ELEMENT = 'link';
	KHighlight.IMAGE_ELEMENT = 'image';
	KHighlight.TITLE_ELEMENT = 'title';
	KHighlight.TEXT_ELEMENT = 'text';
	
	KSystem.included('KHighlight');
});