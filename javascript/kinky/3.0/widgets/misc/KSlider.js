function KSlider(parent, label, id, options) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.options = options || {};

		this.draggable = window.document.createElement('i');
		this.draggable.className = ' ' + this.className + 'SliderDrag KSliderDrag fa fa-circle ';
		this.setStyle({
			cursor : 'move',
			display : 'block',
			position: 'absolute',
			top : '0',
			left : '0'
		}, this.draggable);

		this.legend = window.document.createElement('p');
		this.legend.className = ' ' + this.className + 'SliderLegend KSliderLegend ';

		this.min = 0;
		this.max = (!!this.options.percentage ? 100 : 1);
		if (!!this.options.limits && this.options.limits.length == 2) {
			this.min = this.options.limits[0];
			this.max = this.options.limits[1];
		}

	}
}

KSystem.include( [ 
	'KEffects',
	'KAbstractInput' 
], function() {
	KSlider.prototype = new KAbstractInput;
	KSlider.prototype.constructor = KSlider;
		
	KSlider.prototype.createInput = function() {
		return this.input;
	};

	KSlider.prototype.getValue = function() {
		if (this.options.percentage) {
			return Math.round(parseFloat(this.input.value) * 100) + '%';
		}
		else {
			return this.input.value;
		}
	};
	
	KSlider.prototype.getRawValue = function() {
		return parseFloat(this.input.value);
	};

	KSlider.prototype.setValue = function(value) {
		if (!value) {
			this.input.value = 0;
		}
		else {
			if (this.options.percentage) {
				this.input.value = typeof value == 'string' ? parseInt(value.replace('%', '')) / 100 : value;
			}
			else {
				this.input.value = typeof value == 'string' ? parseFloat(value).toFixed(2) : value.toFixed(2);
			}
		}

		if (this.options.percentage) {
			this.legend.innerHTML = Math.round(parseFloat(this.input.value) * 100) + '%';
		}
		else {
			this.legend.innerHTML = parseFloat(this.input.value);
		}

	};

	KSlider.prototype.visible = function() {
		KAbstractInput.prototype.visible.call(this);
		var self = this;
		this.readyTimer = KSystem.addTimer(function() {
			var width = KDOM.normalizePixelValue(KCSS.getComputedStyle(self.inputContainer).width);
			if (isNaN(width) || width == 0) {
				return;
			}
			KSystem.removeTimer(self.readyTimer);
			var value = self.getRawValue();
			value = (value - self.min) / (self.max - self.min);

			self.draggable.style.left = (Math.round(value * width) - Math.round(self.draggable.offsetWidth / 2) + KSlider.getElementCoords(self.inputContainer).x) + 'px';
		}, 100, true);
	};

	KSlider.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this); 

		this.inputContainer.appendChild(this.legend);

		this.inputContainer.appendChild(window.document.createElement('hr'));

		this.inputContainer.style.position = 'relative';
		this.inputContainer.appendChild(this.draggable);

		KDOM.addEventListener(this.draggable, 'mousedown', KSlider.startDrag);
	};

	KSlider.startDrag = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var w = KDOM.getEventWidget(event);

		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		
		KDOM.addEventListener(w.content, 'mouseup', KSlider.stopDrag);
		KDOM.addEventListener(w.content, 'mousemove', KSlider.drag);

		w.width = KDOM.normalizePixelValue(KCSS.getComputedStyle(w.inputContainer).width);
		w.offsetx = KSlider.getElementCoords(w.inputContainer).x;
	};
	
	KSlider.drag = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var w = KDOM.getEventWidget(event);

		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		
		var x = mouseEvent.clientX - w.offsetx;
		if (x < 0 || x > w.width) {
			return;
		}
		w.x = x;
		w.draggable.style.left = (w.x - Math.round(w.draggable.offsetWidth / 2)) + 'px';
		w.setValue(w.min + ((w.x / w.width) * (w.max - w.min)));
	};
	
	KSlider.stopDrag = function(event) {
		var w = KDOM.getEventWidget(event);
		w.offsety = 0;
		w.offsetx = 0;
		KDOM.removeEventListener(w.content, 'mouseup', KSlider.stopDrag);
		KDOM.removeEventListener(w.content, 'mousemove', KSlider.drag);
	};

	KSlider.getElementCoords = function(element) {
		var e = element;
		var ex = 0;
		var ey = 0;
		do {
			ex += e.offsetLeft;
			ey += e.offsetTop;
		} while (e = e.offsetParent);

		return { x : ex, y : ey };
	};

	KSlider.prototype.INPUT_ELEMENT = 'input';
	KSlider.LABEL_CONTAINER = 'label';
	KSlider.INPUT_ERROR_CONTAINER = 'errorArea';

	KSystem.included('KSlider');
});