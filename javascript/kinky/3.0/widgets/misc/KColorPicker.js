function KColorPicker(parent, label, id, options) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.options = options || {};
	}
}

KSystem.include( [ 
	'KEffects',
	'KAbstractInput' 
], function() {
	KColorPicker.prototype = new KAbstractInput;
	KColorPicker.prototype.constructor = KColorPicker;
		
	KColorPicker.prototype.createInput = function() {
		this.legend = window.document.createElement('input');
		{
			this.legend.setAttribute('type', 'text');
			this.legend.className = ' KColorPickerLegend ' + this.className + 'Legend color ';
			this.legend.name = (!!this.inputID ? this.inputID : this.hash);
		}
		return this.legend;
	};

	KColorPicker.prototype.getValue = function() {
		return this.input.value;
	};

	KColorPicker.prototype.setValue = function(value) {
		if (!value) {
			this.input.value = '';
		}
		else {
			this.input.value = ColorConverter.toString(value);
			if (!!this.legend) {
				this.legend.value = ColorConverter.toHex(ColorConverter.toRGB(this.input.value));
			}
		}
	};

	KColorPicker.prototype.visible = function() {
		KAbstractInput.prototype.visible.call(this);
		var self = this;
		this.readyTimer = KSystem.addTimer(function() {
			var width = KDOM.normalizePixelValue(KCSS.getComputedStyle(self.inputContainer).width);
			if (isNaN(width) || width == 0) {
				return;
			}
			var exists = jscolor || 'off'
			if (exists === 'off') {
				return;
			}
			KSystem.removeTimer(self.readyTimer);
			jscolor.bind();
		}, 100, true);
	};

	KColorPicker.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this); 

		if (!!this.input.value && this.input.value != '') {
			this.legend.value = ColorConverter.toHex(ColorConverter.toRGB(this.input.value));
		}

		KDOM.addEventListener(this.legend, 'change', function(e) {
			var element = KDOM.getEventTarget(e);
			var w = KDOM.getEventWidget(e);
			w.input.value = ColorConverter.toString(ColorConverter.toRGB('#' + element.value));
		});

		if (!KColorPicker.external_loader) {
			KColorPicker.external_loader = window.document.createElement('script');
			KColorPicker.external_loader.charset = 'utf-8';
			KColorPicker.external_loader.src = (CODA_VERSION || '') + '/jscolor/jscolor.js';
			window.document.getElementsByTagName('head')[0].appendChild(KColorPicker.external_loader);
		}

	};

	KColorPicker.prototype.INPUT_ELEMENT = 'input';
	KColorPicker.external_loader = undefined;
	KColorPicker.LABEL_CONTAINER = 'label';
	KColorPicker.INPUT_ERROR_CONTAINER = 'errorArea';

	KSystem.included('KColorPicker');
});