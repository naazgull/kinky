function KMap(parent, label, id) {
	if (parent) {
		this.isLeaf = true;

		KWidget.call(this, parent);

		this.map = null;

		this.mapMarkers = {};
		this.mapMarkersCount = 0;

		this.showInfoWindow = true;

		this.latlng = new google.maps.LatLng(38.708324327636795, -9.135032854173915);
		this.mapType = google.maps.MapTypeId.ROADMAP;
		this.initialZoom = 13;
		this.doubleClickZoom = true;
		this.draggable = true;
		this.streetView = false;

		this.draggableMarkers = false;

		this.infowindow = new google.maps.InfoWindow();

		this.linkContainer = document.createElement('div');
		this.mapContainer = document.createElement('div');
	}
}

KSystem.include([ 'KWidget', 'KHighlight' ], function() {
	KMap.prototype = new KWidget();
	KMap.prototype.constructor = KMap;

	KMap.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}

		this.mapContainer.className = 'KMapParent';
		this.content.appendChild(this.mapContainer);

		this.linkContainer.className = 'KMapLinks';
		this.content.appendChild(this.linkContainer);

		this.initializeMap();

		this.activate();
	};

	KMap.prototype.initializeMap = function() {
		if (!!this.data) {
			if (this.data.latitude != null && this.data.longitude != null) {
				eval('this.latlng = new google.maps.LatLng(' + this.data.latitude + ',' + this.data.longitude + ');');
			}

			if (this.data.mapType != null) {
				eval('this.mapType = ' + this.data.mapType + ';');
			}

			if (this.data.initialZoom != null) {
				this.initialZoom = this.data.initialZoom;
			}

			if (this.data.doubleClickZoom != null) {
				this.doubleClickZoom = this.data.doubleClickZoom && this.data.doubleClickZoom != "false";
			}

			if (this.data.draggable != null) {
				this.draggable = this.data.draggable && this.data.draggable != "false";
			}

			if (this.data.streetView != null) {
				this.streetView = this.data.streetView && this.data.streetView != "false";
			}

			if (this.data.draggableMarkers != null) {
				this.draggableMarkers = this.data.draggableMarkers && this.data.draggableMarkers != "false";
			}
		}

		var myOptions = {
			center : this.latlng,
			mapTypeId : this.mapType,
			zoom : this.initialZoom,
			disableDoubleClickZoom : !this.doubleClickZoom,
			draggable : this.draggable,
			streetViewControl : this.streetView,
			mapTypeControlOptions : {
				style : google.maps.MapTypeControlStyle.DROPDOWN_MENU
			},
			navigationControlOptions : {
				style : google.maps.NavigationControlStyle.SMALL
			}
		};

		this.map = new google.maps.Map(this.mapContainer, myOptions);

		if (!!this.data.markers) {
			var ac;

			if (!!this.data.markers.length) {
				ac = this.data.markers;
			} else {
				ac = [ this.data.markers ];
			}

			for ( var index in ac) {
				if (ac[index].latitude != null && ac[index].longitude != null) {
					var latLng = null;
					eval('latLng = new google.maps.LatLng(' + ac[index].latitude + ',' + ac[index].longitude + ');');

					var marker = this.placeMarker(latLng, ac[index]);

					var link = new KHighlight(this);
					link.marker = marker;

					if(ac[index].isDefault) {

						this.activeHighlight = link;
						this.activeHighlight.addCSSClass('KHighlightSelected');
						this.map.setCenter(link.marker.position);
					}

					if (!!ac[index].title)
						link.setHighlightTitle(ac[index].title);

					if (!!ac[index].description)
						link.setHighlightText(ac[index].description, true);

					link.addEventListener('click', function(event) {
						var widget = KDOM.getEventWidget(event);
						var topWidget = widget.getParent('KMap');

						topWidget.map.setCenter(widget.marker.position);

						if (topWidget.activeHighlight != null) {
							topWidget.activeHighlight.removeCSSClass('KHighlightSelected');
						}

						topWidget.activeHighlight = widget;
						topWidget.activeHighlight.addCSSClass('KHighlightSelected');

						if (topWidget.showInfoWindow) {
							if (!topWidget.infowindow)
								topWidget.infowindow = new google.maps.InfoWindow();

							topWidget.infowindow.open(topWidget.map, widget.marker);
						}
					}, KHighlight.LINK_ELEMENT);

					this.appendChild(link, 'linkContainer');

					link.go();
				}
			}
		}
	};

	KMap.prototype.placeMarker = function(latLng, pointObject) {
		var marker = new google.maps.Marker({
			map : this.map,
			position : latLng,
			title : pointObject.title,
			clickable : true,
			draggable : this.draggableMarkers && (pointObject.draggable == null || pointObject.draggable != 0)
		});

		marker.index = this.mapMarkersCount++;
		marker.name = 'marker' + marker.index;

		if (!!pointObject && !!pointObject.icon && pointObject.icon != '')
			marker.setIcon(pointObject.icon);
		else if (!!this.data && !!this.data.icon && this.data.icon != '')
			marker.setIcon(this.data.icon);

		marker.ballon = !!pointObject.ballon ? pointObject.ballon : pointObject.description;

		this.mapMarkers[marker.name] = marker;

		var topWidget = this;
		google.maps.event.addListener(marker, 'click', function() {
			if (topWidget.showInfoWindow) {
				if (!topWidget.infowindow)
					topWidget.infowindow = new google.maps.InfoWindow();

				topWidget.infowindow.setContent(this.ballon);
				topWidget.infowindow.open(topWidget.map, this);
			}
		});

		if (marker.getDraggable()) {
			marker.dragging = false;

			google.maps.event.addListener(marker, 'dragstart', function() {
				marker.dragging = true;
				topWidget.infowindow.close();
			});

			google.maps.event.addListener(marker, 'dragend', function() {
				this.dragging = false;
				topWidget.setCenter(this.position);
			});
		}

		return marker;
	};

	KMap.prototype.clearMarkers = function() {
		this.infowindow.close();

		for ( var i = 0; i < this.mapMarkersCount; i++) {
			this.mapMarkers['marker' + i].setMap(null);
			delete this.mapMarkers['marker' + i];
		}

		this.mapMarkers = new Object();
		this.mapMarkersCount = 0;
	};

	KMap.prototype.visible = function() {
		KWidget.prototype.visible.call(this);

		var center = this.map.getCenter();
		google.maps.event.trigger(this.map, 'resize');
		this.map.setZoom(this.map.getZoom());
		this.map.setCenter(center);
	};

	KMap.LINK_CONTAINER = 'linkContainer';
	KMap.MAP_CONTAINER = 'mapContainer';

	KSystem.included('KMap');
});