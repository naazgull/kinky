function KMinimalScroll(parent, div) {
	if (!!parent) {
		KScroll.call(this, parent, div);
	}
}
KSystem.include([
	'KScroll'
], function() {
	KMinimalScroll.prototype = new KScroll();
	KMinimalScroll.prototype.constructor = KMinimalScroll;
	
	KMinimalScroll.prototype.draw = function() {
		this.parent.addResizeListener(KMinimalScroll.onWidgetResize);
		var root = this.parent[this.divTarget];
		
		this.barV = window.document.createElement('div');
		{
			KCSS.addCSSClass(this.className + 'VerticalBar', this.barV);
			this.barV.id = this.id;
			
			this.scrollerV = window.document.createElement('div');
			{
				KCSS.addCSSClass(this.className + 'VerticalScroller', this.scrollerV);
			}
			this.barV.appendChild(this.scrollerV);
			
			var buttonDiv = window.document.createElement('div');
			{
				KCSS.addCSSClass(this.className + 'VerticalButtons', buttonDiv);
				
				this.upperV = window.document.createElement('button');
				{
					KCSS.addCSSClass(this.className + 'VerticalUpper', this.upperV);
					this.upperV.innerHTML = '&nbsp;';
					KDOM.addEventListener(this.upperV, 'click', KScroll.onUp);
				}
				buttonDiv.appendChild(this.upperV);
				
				this.downerV = window.document.createElement('button');
				{
					KCSS.addCSSClass(this.className + 'VerticalDowner', this.downerV);
					this.downerV.innerHTML = '&nbsp;';
					KDOM.addEventListener(this.downerV, 'click', KScroll.onDown);
				}
				buttonDiv.appendChild(this.downerV);
				
				buttonDiv.style.display = 'none';
			}
			this.scrollerV.appendChild(buttonDiv);
		}
		root.appendChild(this.barV);
		
		this.barH = window.document.createElement('div');
		{
			KCSS.addCSSClass(this.className + 'HorizontalBar', this.barH);
			this.barH.id = this.id;
			
			this.scrollerH = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'HorizontalScroller', this.scrollerH);
				this.scrollerH.innerHTML = '&nbsp;';
			}
			this.barH.appendChild(this.scrollerH);
			
			var buttonDiv = window.document.createElement('div');
			{
				KCSS.addCSSClass(this.className + 'HorizontalButtons', buttonDiv);
				
				this.upperH = window.document.createElement('button');
				{
					KCSS.addCSSClass(this.className + 'HorizontalUpper', this.upperH);
					this.upperH.innerHTML = '&nbsp;';
					KDOM.addEventListener(this.upperH, 'click', KScroll.onLeft);
				}
				buttonDiv.appendChild(this.upperH);
				
				this.downerH = window.document.createElement('button');
				{
					KCSS.addCSSClass(this.className + 'HorizontalDowner', this.downerH);
					this.downerH.innerHTML = '&nbsp;';
					KDOM.addEventListener(this.downerH, 'click', KScroll.onRight);
				}
				buttonDiv.appendChild(this.downerH);
				
				buttonDiv.style.display = 'none';
			}
			this.barH.appendChild(buttonDiv);
		}
		root.appendChild(this.barH);
		
		var rootH = root.style.height;
		if (!rootH) {
			rootH = KDOM.getBrowserHeight() + 'px';
		}
		KCSS.setStyle({
			overflow : 'hidden',
			height : rootH
		}, [
			root
		]);
		
		this.offsetV = this.parent.content.offsetTop;
		this.offsetH = this.parent.content.offsetLeft;
		this.position.pv = this.topbH = 0;
		this.bottombH = 0;
		this.position.ph = this.leftbW = 0;
		this.rightbW = 0;
		
		KDOM.addEventListener(this.scrollerV, 'mouseover', KMinimalScroll.showVertical);
		KDOM.addEventListener(this.barV, 'mouseout', KMinimalScroll.hide);
		KDOM.addEventListener(this.scrollerH, 'mouseover', KMinimalScroll.showHorizontal);
		KDOM.addEventListener(this.barH, 'mouseout', KMinimalScroll.hide);
	};
	
	KMinimalScroll.onWidgetResize = function(scrolledWidget, size) {
		var scroll = KScroll.getScrollFromWidget(scrolledWidget);
		
		scroll.viewportH = KDOM.normalizePixelValue(KCSS.getComputedStyle(scroll.parent.panel).height);
		scroll.viewportW = KDOM.normalizePixelValue(KCSS.getComputedStyle(scroll.parent.panel).width);
		scroll.proportionV = (scroll.viewportH - scroll.topbH - scroll.bottombH - scroll.offsetV) / (scroll.parent.content.offsetHeight + 10);
		scroll.proportionH = (scroll.viewportW - scroll.leftbW - scroll.rightbW - scroll.offsetH) / (scroll.parent.content.offsetWidth + 10);
		
		if (scroll.viewportW < scroll.parent.content.offsetWidth) {
			var width = Math.round(scroll.proportionH * scroll.viewportW);
			var scrollerHX = KDOM.normalizePixelValue(scroll.scrollerH.style.left);
			
			if (isNaN(scrollerHX)) {
				scrollerHX = scroll.leftbW;
			}
			else {
				var contentX = KDOM.normalizePixelValue(scroll.parent.content.style.left);
				if (isNaN(contentX)) {
					contentX = 0;
				}
				scrollerHX = Math.round(-contentX * scroll.proportionH) + scroll.leftbW;
			}
			scroll.position.ph = scrollerHX;
			
			KCSS.setStyle({
				width : scroll.viewportW + 'px',
				display : 'block'
			}, [
				scroll.barH
			]);
			
			KCSS.setStyle({
				width : width + 'px',
			}, [
				scroll.scrollerH
			]);
			
			KEffects.addEffect(scroll.scrollerH, {
				f : KEffects.linear,
				type : 'drag',
				rectangle : [
					0,
					scroll.viewportW - width - scroll.rightbW,
					0,
					scroll.leftbW
				],
				from : {
					x : scrollerHX
				},
				lock : {
					y : true
				},
				onStart : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					var buttonsH = scroll.scrollerH.childNodes[0];
					scroll.dragx = KDOM.normalizePixelValue(buttonsH.style.left) - KDOM.normalizePixelValue(scroll.scrollerH.style.left);
				},
				onComplete : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					scroll.dragging = false;
				},
				onAnimate : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					scroll.dispatchScroll();
					scroll.dragging = true;
					scroll.blockclick = true;
					var buttonsH = scroll.scrollerH.childNodes[0];
					buttonsH.style.left = (KDOM.normalizePixelValue(scroll.scrollerH.style.left) + scroll.dragx) + 'px';
				},
				applyToElement : true
			});
		}
		else {
			KCSS.setStyle({
				width : scroll.viewportW + 'px',
				display : 'none'
			}, [
				scroll.barH
			]);
		}
		
		if (scroll.viewportH < scroll.parent.content.offsetHeight) {
			var height = Math.round(scroll.proportionV * scroll.viewportH);
			var scrollerVY = KDOM.normalizePixelValue(scroll.scrollerV.style.top);
			
			if (isNaN(scrollerVY)) {
				scrollerVY = scroll.topbH;
			}
			else {
				var contentY = KDOM.normalizePixelValue(scroll.parent.content.style.top);
				if (isNaN(contentY)) {
					contentY = 0;
				}
				scrollerVY = Math.round(-contentY * scroll.proportionV) + scroll.topbH;
			}
			
			scroll.position.pv = scrollerVY;
			
			KCSS.setStyle({
				height : scroll.viewportH + 'px',
				display : 'block'
			}, [
				scroll.barV
			]);
			KCSS.setStyle({
				height : height + 'px',
				top : scrollerVY + 'px'
			}, [
				scroll.scrollerV
			]);
			KEffects.addEffect(scroll.scrollerV, {
				f : KEffects.linear,
				type : 'drag',
				rectangle : [
					scroll.topbH,
					0,
					scroll.viewportH - height - scroll.bottombH,
					0
				],
				from : {
					y : scrollerVY
				},
				lock : {
					x : true
				},
				onStart : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					var buttonsV = scroll.scrollerV.childNodes[0];
					scroll.dragy = KDOM.normalizePixelValue(buttonsV.style.top) - KDOM.normalizePixelValue(scroll.scrollerV.style.top);
				},
				onComplete : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					scroll.dragging = false;
				},
				onAnimate : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					scroll.dragging = true;
					scroll.blockclick = true;
					scroll.dispatchScroll();
					var buttonsV = scroll.scrollerV.childNodes[0];
					buttonsV.style.top = (KDOM.normalizePixelValue(scroll.scrollerV.style.top) + scroll.dragy) + 'px';
				},
				applyToElement : true
			});
		}
		else {
			KCSS.setStyle({
				height : scroll.viewportH + 'px',
				display : 'none'
			}, [
				scroll.barV
			]);
		}
	};
	
	KMinimalScroll.showVertical = function(event) {
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.dragging) {
			return;
		}
		var mouseEvent = KDOM.getEvent(event);
		KDOM.stopEvent(event);
		var scroller = KDOM.getEventCurrentTarget(event);
		var target = KDOM.getEventTarget(event);
		
		if (scroller == target) {
			var buttons = scroller.childNodes[0];
			buttons.style.position = 'fixed';
			buttons.style.visibility = 'hidden';
			buttons.style.display = 'block';
			var top = (mouseEvent.clientY - Math.round(buttons.offsetHeight / 2));
			if (top < 0) {
				top = 0;
			}
			else if (top > KDOM.getBrowserHeight()) {
				top = KDOM.getBrowserHeight() - buttons.offsetHeight;
			}
			buttons.style.top = top + 'px';
			
			var left = mouseEvent.clientX - buttons.offsetWidth;
			buttons.style.left = left + 'px';
			
			KEffects.addEffect(buttons, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 300,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				},
				onStart : function() {
					buttons.style.visibility = 'visible';
				},
				applyToElement : true
			});
		}
	};
	
	KMinimalScroll.hide = function(event) {
		if (KScroll.getScrollFromEvent(event).dragging) {
			return;
		}
		var target = KDOM.getEventTarget(event);
		var scrollDiv = KScroll.getScrollDiv(target);
		var relTarget = KDOM.getEventRelatedTarget(event, true);
		var testScrollDiv = KScroll.getScrollDiv(relTarget);
		
		if (!testScrollDiv || (scrollDiv.id != testScrollDiv.id)) {
			KEffects.addEffect(scrollDiv.childNodes[0].childNodes[0], {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 300,
				go : {
					alpha : 0
				},
				from : {
					alpha : 1
				},
				onComplete : function() {
					scrollDiv.childNodes[0].childNodes[0].style.display = 'none';
				},
				applyToElement : true
			});
		}
	};
	
	KMinimalScroll.showHorizontal = function(event) {
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.dragging) {
			return;
		}
		var mouseEvent = KDOM.getEvent(event);
		KDOM.stopEvent(event);
		var scroller = KDOM.getEventCurrentTarget(event);
		var target = KDOM.getEventTarget(event);
		
		if (scroller == target) {
			var buttons = scroller.childNodes[0];
			buttons.style.position = 'fixed';
			buttons.style.visibility = 'hidden';
			buttons.style.display = 'block';
			var left = (mouseEvent.pageX - Math.round(buttons.offsetWidth / 2));
			if (left < 0) {
				left = 0;
			}
			else if (left > scroll.viewportW - buttons.offsetWidth) {
				left = scroll.viewportW - buttons.offsetWidth;
			}
			buttons.style.left = left + 'px';
			KEffects.addEffect(buttons, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 300,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				},
				onStart : function() {
					buttons.style.visibility = 'visible';
				},
				applyToElement : true
			});
		}
	};
	
	KSystem.registerAddOn('KMinimalScroll');
	KSystem.included('KMinimalScroll');
});
