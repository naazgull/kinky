function KScroll(parent, div) {
	if (!!parent) {
		this.prefix = 'scroll';
		this.divTarget = (!!div ? div : 'panel');
		KAddOn.call(this, parent);
		this.listeners = new Array();
		this.enabled = false;
		this.position = new Object();
		this.position.ph = 0;
		this.position.pv = 0;
		this.position.dh = 0;
		this.position.dv = 0;
		this.buttonclick = false;
		KScroll.deactiveScrollKeys = false;

		if (window.attachEvent) {
			parent.panel.attachEvent('onmousewheel', KScroll.mouseWheelActive);
			parent.panel.attachEvent('onkeydown', KScroll.downKey);
		}
		else {
			if (KBrowserDetect.browser == 1) {
				parent.panel.addEventListener('DOMMouseScroll', KScroll.mouseWheelActive, false);
			}
			else {
				parent.panel.addEventListener('mousewheel', KScroll.mouseWheelActive, false);
			}
			parent.panel.addEventListener('keydown', KScroll.downKey, false);
		}

	}
}
KSystem.include([
	'KAddOn'
], function() {
	KScroll.prototype = new KAddOn();
	KScroll.prototype.constructor = KScroll;
	
	KScroll.prototype.addScrollListener = function(callback, widget) {
		this.listeners.push({
			widget : widget || this.parent,
			callback : callback
		});
	};
	
	KScroll.prototype.removeScrollListener = function(widget) {
		for ( var listener in this.listeners) {
			if (this.listeners[listener].widget.id == widget.id) {
				this.listeners.splice(listener, 1);
				break;
			}
		}
	};
	
	KScroll.prototype.removeScrollListeners = function(callback, widget) {
		delete this.listeners;
		this.listeners = new Array();
	};
	
	KScroll.prototype.draw = function() {
		this.parent.addResizeListener(KScroll.onWidgetResize);
		var root = this.parent[this.divTarget];
		
		this.barV = window.document.createElement('div');
		{
			KCSS.addCSSClass(this.className + 'VerticalBar', this.barV);
			this.barV.id = this.id;
			
			this.upperV = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'VerticalUpper', this.upperV);
				this.upperV.innerHTML = '&nbsp;';
				KDOM.addEventListener(this.upperV, 'click', KScroll.onUp);
			}
			this.barV.appendChild(this.upperV);
			
			this.scrollerV = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'VerticalScroller', this.scrollerV);
				this.scrollerV.innerHTML = '&nbsp;';
			}
			this.barV.appendChild(this.scrollerV);
			
			this.downerV = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'VerticalDowner', this.downerV);
				this.downerV.innerHTML = '&nbsp;';
				KDOM.addEventListener(this.downerV, 'click', KScroll.onDown);
			}
			this.barV.appendChild(this.downerV);
		}
		root.appendChild(this.barV);
		
		this.barH = window.document.createElement('div');
		{
			KCSS.addCSSClass(this.className + 'HorizontalBar', this.barH);
			this.barH.id = this.id;
			
			this.upperH = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'HorizontalUpper', this.upperH);
				this.upperH.innerHTML = '&nbsp;';
				KDOM.addEventListener(this.upperH, 'click', KScroll.onLeft);
			}
			this.barH.appendChild(this.upperH);
			
			this.scrollerH = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'HorizontalScroller', this.scrollerH);
				this.scrollerH.innerHTML = '&nbsp;';
			}
			this.barH.appendChild(this.scrollerH);
			
			this.downerH = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'HorizontalDowner', this.downerH);
				this.downerH.innerHTML = '&nbsp;';
				KDOM.addEventListener(this.downerH, 'click', KScroll.onRight);
			}
			this.barH.appendChild(this.downerH);
		}
		root.appendChild(this.barH);
		
		var rootH = KCSS.getComputedStyle(root).height;
		if (!rootH) {
			rootH = KDOM.getBrowserHeight() + 'px';
		}
		KCSS.setStyle({
			overflow : 'hidden',
			height : rootH
		}, [
			root
		]);
		
		this.offsetV = this.parent.content.offsetTop;
		this.offsetH = this.parent.content.offsetLeft;
		this.bottombH = KDOM.normalizePixelValue(KCSS.getComputedStyle(this.downerV).height);
		this.rightbW = KDOM.normalizePixelValue(KCSS.getComputedStyle(this.upperH).width);
		this.position.pv = this.topbH = KDOM.normalizePixelValue(KCSS.getComputedStyle(this.upperV).height);
		if (isNaN(this.position.pv)) {
			this.position.pv = 0;
		}
		this.position.ph = this.leftbW = KDOM.normalizePixelValue(KCSS.getComputedStyle(this.downerH).width);
		if (isNaN(this.position.ph)) {
			this.position.ph = 0;
		}
		
	};
	
	KScroll.prototype.scrollTo = function(pos) {
		this.dispatchScroll({
			h : pos.x || this.position.h,
			v : pos.y || this.position.v
		});
	};
	
	KScroll.prototype.dispatchScroll = function(currentPos) {
		if (!!currentPos) {
			if (!!currentPos.h && (((currentPos.h > this.leftbW)))) {
				if (currentPos.h > this.viewportW - KDOM.normalizePixelValue(this.scrollerH.style.width) - this.rightbW) {
					currentPos.h = (this.viewportW - KDOM.normalizePixelValue(this.scrollerH.style.width) - this.rightbW);
				}
			}
			else {
				currentPos.h = this.leftbW;
			}
			if (!!currentPos.v && (((currentPos.v > this.topbH)))) {
				if (currentPos.v > this.viewportH - KDOM.normalizePixelValue(this.scrollerV.style.height) - this.bottombH) {
					currentPos.v = (this.viewportH - KDOM.normalizePixelValue(this.scrollerV.style.height) - this.bottombH);
				}
			}
			else {
				currentPos.v = this.topbH;
			}
		}
		else {
			currentPos = {
				h : KDOM.normalizePixelValue(this.scrollerH.style.left),
				v : KDOM.normalizePixelValue(this.scrollerV.style.top)
			};
		}
		if (isNaN(currentPos.v)) {
			currentPos.v = 0;
		}
		if (isNaN(currentPos.h)) {
			currentPos.h = 0;
		}
		
		var hasDeltaV = currentPos.v != this.position.pv;
		var hasDeltaH = currentPos.h != this.position.ph;
		if (hasDeltaV || hasDeltaH) {
			
			this.position.dh = currentPos.h - this.position.ph;
			this.position.dv = currentPos.v - this.position.pv;
			this.position.ph = currentPos.h;
			this.position.pv = currentPos.v;
			
			if (this.buttonclick) {
				this.buttonclick = false;
				var scroll = this;
				if (hasDeltaV) {
					KEffects.addEffect(this.scrollerV, {
						f : KEffects.easeOutSine,
						type : 'move',
						duration : 300,
						lock : {
							x : true
						},
						go : {
							y : currentPos.v
						},
						onAnimate : function(widget, tween) {
							scroll.parent.scroll({
								dh : scroll.position.dh,
								dv : scroll.position.dv,
								ph : scroll.position.ph,
								pv : tween.y
							}, scroll);
						}
					
					});
				}
				if (hasDeltaH) {
					KEffects.addEffect(this.scrollerH, {
						f : KEffects.easeOutExpo,
						type : 'move',
						duration : 300,
						lock : {
							y : true
						},
						go : {
							x : currentPos.h
						},
						onAnimate : function(widget, tween) {
							scroll.parent.scroll({
								dh : scroll.position.dh,
								dv : scroll.position.dv,
								pv : scroll.position.pv,
								ph : tween.x
							}, scroll);
						}
					
					});
				}
			}
			else {
				this.parent.scroll(this.position, this);
			}
			
			for ( var listener in this.listeners) {
				KSystem.addTimer('Kinky.getWidget(\'' + this.id + '\').listeners[' + listener + '].callback(Kinky.getWidget(\'' + this.id + '\').listeners[' + listener + '].widget, Kinky.getWidget(\'' + this.id + '\'), ' + JSON.stringify(this.position) + ')', 0);
			}
		}
	};
	
	KScroll.prototype.handleMouseWheelScroll = function(scrollDelta) {
		if (this.viewportH >= this.parent.content.offsetHeight) {
			return;
		}
		this.buttonclick = true;
		if (scrollDelta < 0) {
			this.down();
		}
		else {
			this.up();
		}
	};
	
	KScroll.prototype.handleUpButton = function() {
		this.buttonclick = true;
		this.up(KScroll.PAGE_RATIO);
	};
	
	KScroll.prototype.handleDownButton = function() {
		this.buttonclick = true;
		this.down(KScroll.PAGE_RATIO);
	};
	
	KScroll.prototype.handleLeftButton = function() {
		this.buttonclick = true;
		this.left(KScroll.PAGE_RATIO);
	};
	
	KScroll.prototype.handleRightButton = function() {
		this.buttonclick = true;
		this.right(KScroll.PAGE_RATIO);
	};
	
	KScroll.prototype.up = function(ratio) {
		ratio = (!ratio ? KScroll.WHEEL_RATIO : ratio);
		this.dispatchScroll({
			h : this.position.ph,
			v : this.position.pv - Math.round(ratio * this.viewportH * this.proportionV)
		});
	};
	
	KScroll.prototype.down = function(ratio) {
		ratio = (!ratio ? KScroll.WHEEL_RATIO : ratio);
		this.dispatchScroll({
			h : this.position.ph,
			v : this.position.pv + Math.round(ratio * this.viewportH * this.proportionV)
		});
	};
	
	KScroll.prototype.left = function(ratio) {
		ratio = (!ratio ? KScroll.WHEEL_RATIO : ratio);
		this.dispatchScroll({
			h : this.position.ph - Math.round(ratio * this.viewportW * this.proportionH),
			v : this.position.pv
		});
	};
	
	KScroll.prototype.right = function(ratio) {
		ratio = (!ratio ? KScroll.WHEEL_RATIO : ratio);
		this.dispatchScroll({
			h : this.position.ph + Math.round(ratio * this.viewportW * this.proportionH),
			v : this.position.pv
		});
	};
	
	KScroll.getScrollDiv = function(element) {
		var toReturn = element;
		
		try {
			while (!/^widget([0-9]+)$/.test(toReturn.id) && !/^scroll([0-9]+)$/.test(toReturn.id)) {
				toReturn = toReturn.parentNode;
			}
			if (!/^scroll([0-9]+)$/.test(toReturn.id)) {
				return null;
			}
		}
		catch (e) {
			return null;
		}
		return toReturn;
	};
	
	KScroll.getScrollFromEvent = function(event) {
		return KScroll.getScrollInWidgets(KDOM.getEventWidget(event));
	};
	
	KScroll.getScrollInWidgets = function(scrolledWidget) {
		if (!scrolledWidget || !(scrolledWidget instanceof KWidget)) {
			return null;
		}
		for ( var a in scrolledWidget.addons) {
			if (a.indexOf('scroll') != -1) {
				return scrolledWidget.addons[a];
			}
		}
		return KScroll.getScrollInWidgets(scrolledWidget.parent);
	};
	
	KScroll.getScrollFromWidget = function(scrolledWidget) {
		for ( var a in scrolledWidget.addons) {
			if (a.indexOf('scroll') != -1) {
				return scrolledWidget.addons[a];
			}
		}
		return null;
	};
	
	KScroll.onWidgetResize = function(scrolledWidget, size) {
		var scroll = KScroll.getScrollFromWidget(scrolledWidget);
		
		scroll.viewportH = KDOM.normalizePixelValue(KCSS.getComputedStyle(scroll.parent.panel).height);
		scroll.viewportW = KDOM.normalizePixelValue(KCSS.getComputedStyle(scroll.parent.panel).width);
		scroll.proportionV = (scroll.viewportH - scroll.topbH - scroll.bottombH - scroll.offsetV) / scroll.parent.content.offsetHeight;
		scroll.proportionH = (scroll.viewportW - scroll.leftbW - scroll.rightbW - scroll.offsetH) / scroll.parent.content.offsetWidth;
		
		if (scroll.viewportW < scroll.parent.content.offsetWidth) {
			var width = Math.round(scroll.proportionH * scroll.viewportW);
			var scrollerHX = KDOM.normalizePixelValue(scroll.scrollerH.style.left);
			
			if (isNaN(scrollerHX)) {
				scrollerHX = scroll.leftbW;
			}
			else {
				var contentX = KDOM.normalizePixelValue(scroll.parent.content.style.left);
				if (isNaN(contentX)) {
					contentX = 0;
				}
				scrollerHX = Math.round(-contentX * scroll.proportionH) + scroll.leftbW;
			}
			scroll.position.ph = scrollerHX;
			
			KCSS.setStyle({
				width : scroll.viewportW + 'px',
				display : 'block'
			}, [
				scroll.barH
			]);
			
			KCSS.setStyle({
				width : width + 'px',
			}, [
				scroll.scrollerH
			]);
			
			KEffects.addEffect(scroll.scrollerH, {
				f : KEffects.linear,
				type : 'drag',
				rectangle : [
					0,
					scroll.viewportW - width - scroll.rightbW,
					0,
					scroll.leftbW
				],
				from : {
					x : scrollerHX
				},
				lock : {
					y : true
				},
				onAnimate : function(wx, tween) {
					scroll.dragging = true;
					scroll.blockclick = true;
					KScroll.getScrollFromWidget(wx).dispatchScroll();
				},
				onComplete : function(wx, tween) {
					scroll.dragging = false;
					KScroll.getScrollFromWidget(wx).dispatchScroll();
				},
				applyToElement : true
			});
		}
		else {
			KCSS.setStyle({
				width : scroll.viewportW + 'px',
				display : 'none'
			}, [
				scroll.barH
			]);
		}
		
		if (scroll.viewportH < scroll.parent.content.offsetHeight) {
			var height = Math.round(scroll.proportionV * scroll.viewportH);
			var scrollerVY = KDOM.normalizePixelValue(scroll.scrollerV.style.top);
			
			if (isNaN(scrollerVY)) {
				scrollerVY = scroll.topbH;
			}
			else {
				var contentY = KDOM.normalizePixelValue(scroll.parent.content.style.top);
				if (isNaN(contentY)) {
					contentY = 0;
				}
				scrollerVY = Math.round(-contentY * scroll.proportionV) + scroll.topbH;
			}
			
			scroll.position.pv = scrollerVY;
			
			KCSS.setStyle({
				height : scroll.viewportH + 'px',
				display : 'block'
			}, [
				scroll.barV
			]);
			KCSS.setStyle({
				height : height + 'px',
				top : scrollerVY + 'px'
			}, [
				scroll.scrollerV
			]);
			KEffects.addEffect(scroll.scrollerV, {
				f : KEffects.linear,
				type : 'drag',
				rectangle : [
					scroll.topbH,
					0,
					scroll.viewportH - height - scroll.bottombH,
					0
				],
				from : {
					y : scrollerVY
				},
				lock : {
					x : true
				},
				onAnimate : function(wx, tween) {
					scroll.dragging = true;
					scroll.blockclick = true;
					KScroll.getScrollFromWidget(wx).dispatchScroll();
				},
				onComplete : function(wx, tween) {
					scroll.dragging = false;
					KScroll.getScrollFromWidget(wx).dispatchScroll();
				},
				applyToElement : true
			});
		}
		else {
			KCSS.setStyle({
				height : scroll.viewportH + 'px',
				display : 'none'
			}, [
				scroll.barV
			]);
		}
	};
	
	KScroll.activateScroll = function(event) {
		var widget = null;
		try {
			widget = KDOM.getEventWidget(event);
		}
		catch (e) {
		}
		
		if (!widget) {
			widget = KCombo.getEventWidget(event);
		}
		
		if (widget) {
			var scroll = KScroll.getScroll(widget);
			if (scroll) {
				KScroll.activeScroll = scroll.panel.style.display == 'block' ? scroll : null;
			}
		}
	};
	
	KScroll.deactivateScroll = function(event) {
		var widget = null;
		try {
			widget = KDOM.getEventWidget(event);
		}
		catch (e) {
		}
		
		if (!widget) {
			widget = KCombo.getEventWidget(event);
		}
		
		if (widget) {
			var scroll = KScroll.getScroll(widget);
			if (scroll) {
				KScroll.activeScroll = null;
			}
		}
	};
	
	KScroll.onUp = function(event) {
		event = KDOM.getEvent(event);
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.blockclick) {
			scroll.blockclick = false;
			return;
		}
		scroll.handleUpButton();
	};
	
	KScroll.onDown = function(event) {
		event = KDOM.getEvent(event);
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.blockclick) {
			scroll.blockclick = false;
			return;
		}
		scroll.handleDownButton();
	};
	
	KScroll.onLeft = function(event) {
		event = KDOM.getEvent(event);
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.blockclick) {
			scroll.blockclick = false;
			return;
		}
		scroll.handleLeftButton();
	};
	
	KScroll.onRight = function(event) {
		event = KDOM.getEvent(event);
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.blockclick) {
			scroll.blockclick = false;
			return;
		}
		scroll.handleRightButton();
	};
	
	KScroll.downKey = function(event) {
		if (!KScroll.deactiveScrollKeys) {
			var shell = Kinky.getShell();
			if (!shell) {
				return;
			}
			
			var scroll = KScroll.getScrollFromWidget(shell);
			
			if (!scroll) {
				return;
			}
			
			switch (event.keyCode) {
				case 34: { // PAGE DOWN
					scroll.buttonclick = true;
					scroll.down(KScroll.PAGE_RATIO);
					break;
				}
				case 40: { // DOWN
					scroll.buttonclick = true;
					scroll.down(KScroll.LINE_RATIO);
					break;
				}
				case 33: { // PAGE UP
					scroll.buttonclick = true;
					scroll.up(KScroll.PAGE_RATIO);
					break;
				}
				case 38: { // UP
					scroll.buttonclick = true;
					scroll.up(KScroll.LINE_RATIO);
					break;
				}
			}
		}
	};
	
	KScroll.disableKeys = function() {
		KScroll.deactiveScrollKeys = true;
	};
	
	KScroll.enableKeys = function() {
		KScroll.deactiveScrollKeys = false;
	};
	
	KScroll.mouseWheelActive = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var scroll = KScroll.getScrollFromEvent(event);

		if (!scroll) {
			return;
		}
		var widget = KDOM.getEventWidget(event);
		if (widget.id != scroll.parent.id && widget instanceof KCombo) {
			if (KDOM.getEventTarget(event).className.indexOf('Option') != -1) {
				return;
			}
		}

		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		mouseEvent.returnValue = false;
		
		var delta = 0;
		if (mouseEvent.wheelDelta) {
			delta = mouseEvent.wheelDelta;
		}
		else if (mouseEvent.detail) {
			delta = -mouseEvent.detail;
		}
		
		if (delta) {
			scroll.handleMouseWheelScroll(delta);
		}
	};
	
	KScroll.make = function(parent, selector) {
		var scrollindex = selector.indexOf('Scroll');
		var spaceindex = selector.lastIndexOf('.', scrollindex);
		var scrollClass = selector.substring(spaceindex + 1, scrollindex) + 'Scroll';

		if (KSystem.isAddOn(scrollClass)) {
			var scroll = null;
			eval('scroll = new ' + scrollClass + '(parent)');
			scroll.go();
		}
	};

	KScroll.WHEEL_RATIO = 0.07;
	KScroll.PAGE_RATIO = 0.2;
	KScroll.LINE_RATIO = 0.05;
	
	KScroll.deactiveScrollKeys = true;
	
	KSystem.registerAddOn('KScroll');
	KSystem.included('KScroll');
});
