function KLayout(parent) {
	if (!!parent) {
		this.prefix = 'layout';
		KAddOn.call(this, parent);
	}
}
KSystem.include([
	'KAddOn'
], function() {
	KLayout.prototype = new KAddOn();
	KLayout.prototype.constructor = KLayout;

	KLayout.prototype.addTarget = function(targetName) {
		if (!this.config) {
			this.config = {
				targets : new Object()
			};
		}
		else if (!this.config.targets) {
			this.config.targets = new Object();
		}
		this.config.targets[targetName] = {
			name : targetName
		};
	};

	KLayout.prototype.draw = function() {
		this.parent.addCSSClass(this.config.title, KWidget.CONTENT_DIV);
		this.parent.addCSSClass(this.config.title, this.parent.container);
		if (this.parent.activated()) {
			this.parent[this.config.title] = this.parent.content;
			this.addTargets(this.config.targets, this.parent.content);
		}
		else {
			this.parent[this.config.title] = this.parent.container;
			this.addTargets(this.config.targets, this.parent.container);
		}
	};

	KLayout.prototype.addTargets = function(targets, parentDiv) {
		for ( var target in targets) {
			this.parent[target] = window.document.createElement('div');
			{
				this.parent[target].style.position = 'relative';
				this.parent[target].className = ' ' + targets[target].name + ' ';
				this.parent[target].id = this.parent[target].alt = this.parent[target].name = this.id;
				parentDiv.appendChild(this.parent[target]);
				if (!!targets[target].targets) {
					this.addTargets(targets[target].targets, this.parent[target]);
				}
			}
		}
	};

	KLayout.getEventLayout = function(event) {
		event = event || window.event;
		if (event == null) {
			return null;
		}
		var element = event.target || event.srcElement;
		while ((element != null) && !/^layout([0-9]+)$/.test(element.id)) {
			element = element.parentNode;
		}

		return element;
	};

	KLayout.getLayout = function(domElement) {
		var element = domElement;
		while ((element != null) && !/^layout([0-9]+)$/.test(element.id)) {
			element = element.parentNode;
		}
		return element;
	};

	KSystem.registerAddOn('KLayout');
	KSystem.included('KLayout');
});
