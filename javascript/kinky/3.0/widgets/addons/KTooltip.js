function KTooltip(parent, params) {
	if (!!parent) {
		this.prefix = 'tooltip';
		KAddOn.call(this, parent);
		this.params = params;

		this.parentRootDiv = this.parent.childDiv(KWidget.ROOT_DIV);

		this.panel = window.document.createElement('div');
		this.panel.id = this.id;
		this.panel.className = ' KTooltip ' + this.parent.className + 'Tooltip ';
		this.panel.innerHTML = this.params.text;
		this.panel.style.display = 'none';
		this.panel.style.opacity = 0;

		this.go();
	}
}
KSystem.include([
	'KAddOn'
], function() {
	KTooltip.prototype = new KAddOn();
	KTooltip.prototype.constructor = KTooltip;

	KTooltip.prototype.go = function() {
		var offsetX = (!!this.params.offsetX ? this.params.offsetX : 0);
		var offsetY = (!!this.params.offsetY ? this.params.offsetY : 0);

		var delay = this.params.delay;
		var timer = -1;
		var widget = this;
		KDOM.addEventListener(this.parentRootDiv, 'mouseover', function(event) {
			var targetWidget = KDOM.getEventWidget(event, widget.parent.className);
			if (!!targetWidget && (targetWidget.id == widget.parent.id) && (widget.panel.style.display == 'block')) {
				return;
			}

			if (!!delay) {
				timer = KSystem.addTimer(function() {
					timer = -1;
					KTooltip.show(widget, (KDOM.mouseX(event) + offsetX), (KDOM.mouseY(event) + offsetY));
				}, delay);
				return;
			}
			KTooltip.show(widget, (KDOM.mouseX(event) + offsetX), (KDOM.mouseY(event) + offsetY));
		});
		KDOM.addEventListener(this.parentRootDiv, 'mouseout', function(event) {
			var targetWidget = KDOM.getWidget(KDOM.getEventRelatedTarget(event, true), widget.parent.className);
			if (!!targetWidget && (targetWidget.id == widget.parent.id)) {
				return;
			}

			if (timer != -1) {
				KSystem.removeTimer(timer);
				return;
			}
			widget.transition_hide();
		});
		KDOM.addEventListener(this.parentRootDiv, 'mousemove', function(event) {
			event = KDOM.getEvent(event);
			widget.panel.style.left = (KDOM.mouseX(event) + offsetX) + 'px';
			widget.panel.style.top = (KDOM.mouseY(event) + offsetY) + 'px';
		});

		var func = this.parent.destroy;
		this.parent.destroy = function() {
			widget.panel.style.display = 'none';
			widget.parentRootDiv.appendChild(widget.panel);
			widget.panel.style.position = 'relative';
			func.call(this);
		};

		this.parentRootDiv.appendChild(this.panel);
	};

	KTooltip.show = function(widget, positionX, positionY) {

		widget.panel.style.position = 'absolute';
		window.document.body.appendChild(widget.panel);

		widget.panel.style.left = positionX + 'px';
		widget.panel.style.top = positionY + 'px';

		widget.panel.style.display = 'block';

		widget.transition_show();
	};

	KTooltip.prototype.hide = function() {
		this.panel.style.display = 'none';
		this.parentRootDiv.appendChild(this.panel);
		this.panel.style.position = 'relative';
	};

	KTooltip.prototype.transition_show = function() {
		var defaultEnterTween = {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 1000,
			go : {
				alpha : 1
			},
			from : {
				alpha : 0
			}
		};
		KEffects.addEffect(this, (!!this.params.enterTween ? this.params.enterTween : defaultEnterTween));
	};

	KTooltip.prototype.transition_hide = function() {
		var widget = this;
		var defaultExitTween = {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 1000,
			go : {
				alpha : 0
			},
			from : {
				alpha : 1
			},
			onComplete : function() {
				// caso seja subcarregado este metodo NAO ESQUECER
				widget.hide();
			}
		};
		KEffects.addEffect(this, (!!this.params.exitTween ? this.params.exitTween : defaultExitTween));
	};

	KTooltip.make = function(target, params) {
		var tooltip = new KTooltip(target, params);
		return tooltip;
	};

	KSystem.registerAddOn('KTooltip');
	KSystem.included('KTooltip');
});
