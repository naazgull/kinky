function KAddOn(parent) {
	if (!!parent) {
		this.className = KSystem.getObjectClass(this);
		
		this.parent = parent;
		this.shell = this.parent.shell;
		this.kinky = Kinky.bunnyMan;
		this.id = Kinky.bunnyMan.addWidget(this, this.prefix);
		this.hash = '/' + this.id;
		this.parent.addons[this.id] = this;
		
		if (Kinky.bunnyMan.currentState.length != 1) {
			for ( var i in Kinky.bunnyMan.currentState) {
				this.dispatchState(Kinky.bunnyMan.currentState[i]);
			}
		}
	}
}
{
	KAddOn.prototype = new Object();
	KAddOn.prototype.constructor = KAddOn;
	KAddOn.prototype.prefix = 'addon';
	
	KAddOn.prototype.dispatchState = function(stateName) {
		for ( var func in KWidget.STATE_FUNCTIONS) {
			var exists = false;
			eval('exists = !!KAddOn.prototype.' + stateName + '_' + KWidget.STATE_FUNCTIONS[func] + ';');
			if (exists) {
				eval('this.' + KWidget.STATE_FUNCTIONS[func] + ' = KAddOn.prototype.' + stateName + '_' + KWidget.STATE_FUNCTIONS[func] + ';');
			}
		}
	};
	
	KAddOn.prototype.undispatchState = function(stateName) {
		for ( var func in KWidget.STATE_FUNCTIONS) {
			var exists = false;
			eval('exists = !!KAddOn.prototype.' + stateName + '_' + KWidget.STATE_FUNCTIONS[func] + ';');
			if (exists) {
				eval('this.' + KWidget.STATE_FUNCTIONS[func] + ' = KAddOn.prototype.default_' + KWidget.STATE_FUNCTIONS[func] + ';');
			}
		}
	};
	
	KAddOn.prototype.getShell = function() {
		return Kinky.shell[this.shell];
	};
	
	KAddOn.prototype.unload = function() {
		this.destroy();
	};
	
	KAddOn.prototype.destroy = function() {
		var id = this.id;
		
		for ( var att in this) {
			if (this[att]) {
				delete this[att];
			}
		}
		
		Kinky.bunnyMan.removeWidget(id);
	};
	
	KAddOn.prototype.visible = function() {
		return;
	};
	
	KAddOn.prototype.getConnection = function(type) {
		return KConnectionsPool.getConnection(type, this.shell);
	};
	
	KAddOn.prototype.activated = function() {
		return true;
	};
	
	KAddOn.prototype.setStyle = function(cssStyle) {
		KWidget.prototype.setStyle.call(this, cssStyle, this.panel);
	};
	
	KAddOn.prototype.go = function() {
		if (!!this.config) {
			KCache.commit(this.ref + '/config', this.config);
		}
		else {
			this.config = KCache.restore(this.ref + '/config');
		}
		
		this.draw();
	};
	
	KAddOn.prototype.draw = function() {
	};
	
	KAddOn.STATE_FUNCTIONS = [
		"go",
		"load",
		"draw",
		"focus",
		"blur",
		"enable",
		"disable",
		"visible",
		"invisible",
		"abort",
		"onLoad",
		"onSave",
		"onRemove",
		"onPost",
		"onPut",
		"onError",
		"onOk",
		"onHead",
		"onCreated",
		"onAccepted",
		"onNoContent",
		"onBadRequest",
		"onUnauthorized",
		"onForbidden",
		"onNotFound",
		"onMethodNotAllowed",
		"onPreConditionFailed",
		"onInternalError"
	];
	
	KSystem.included('KAddOn');
}
