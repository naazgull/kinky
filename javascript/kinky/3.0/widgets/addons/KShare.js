function KShare(parent) {
	if (!!parent) {
		this.prefix = 'share';
		KAddOn.call(this, parent);
		this.buzz = new Object();
		
		this.panel = window.document.createElement(this.isAside ? 'aside' : this.tagNames[Kinky.HTML_READY][KWidget.ROOT_DIV]);
		{
			this.panel.id = this.id;
			this.panel.className = ' KWidgetPanel ' + this.className + ' ';
			this.panel.style.position = 'relative';
		}
		
	}
}
KSystem.include([
	'KAddOn'
], function() {
	KShare.prototype = new KAddOn();
	KShare.prototype.constructor = KShare;
	
	KShare.prototype.addSocialLink = function(link) {
		if (!this.config) {
			this.config = {
				links : new Array()
			};
		}
		else if (!this.config.links) {
			this.config.links = new Array();
		}
		this.config.links.push(link);
	};
	
	KShare.prototype.onBuzzFacebook = function(data) {
		if (!!data.shares) {
			if (data.shares > 999999) {
				data.shares = KSystem.formatDouble(data.shares / 1000000, 1) + 'm';
			}
			else if (data.shares > 999) {
				data.shares = KSystem.formatDouble(data.shares / 1000, 1) + 'k';
			}
		}
		else {
			data.shares = 0;
		}
		this.buzz['facebook'].count = data.shares;
		this.buzz['facebook'].element.innerHTML = data.shares;
		
		window.document.getElementsByTagName('head')[0].removeChild(this.facebookp);
	};
	
	KShare.prototype.onBuzzTwitter = function(data) {
		if (!!data.count) {
			if (data.count > 999999) {
				data.count = KSystem.formatDouble(data.count / 1000000, 1) + 'm';
			}
			else if (data.count > 999) {
				data.count = KSystem.formatDouble(data.count / 1000, 1) + 'k';
			}
		}
		else {
			data.count = 0;
		}
		this.buzz['twitter'].count = data.count;
		this.buzz['twitter'].element.innerHTML = data.count;
		window.document.getElementsByTagName('head')[0].removeChild(this.twitterp);
	};
	
	KShare.prototype.draw = function() {
		if (!this.config || !this.config.links) {
			return;
		}
		
		var shellConf = Kinky.getShellConfig(this.shell);
		var lang = (!!shellConf.headers && !!shellConf.headers['Accept-Language'] ? shellConf.headers['Accept-Language'] : (!!KLocale.LANG ? KLocale.LANG : 'pt'));
		
		for ( var l in this.config.links) {
			this.buzz[this.config.links[l].rel] = new Object();
			this.buzz[this.config.links[l].rel].count = 0;
			var button = window.document.createElement(this.config.links[l].rel != 'facebook_like' ? 'a' : 'div');
			{
				button.innerHTML = this.config.links[l].title;
				KCSS.addCSSClass('KShareLink KShareLink_' + this.config.links[l].rel, button);
				if (!!this.config.links[l].href) {
					switch (this.config.links[l].rel) {
						case 'facebook': {
							button.href = 'https://www.facebook.com/sharer.php?u=' + encodeURIComponent(this.config.links[l].href);
							break;
						}
						case 'twitter': {
							button.href = 'http://twitter.com/home?status=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.description ? this.parent.data.metadata.description + ' ' : '') + this.config.links[l].href);
							break;
						}
						case 'orkut': {
							button.href = 'http://promote.orkut.com/preview?nt=orkut.com&tt=' + encodeURIComponent(this.config.links[l].href) + '&cn=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.description ? this.parent.data.metadata.description : '')) + '&du=' + encodeURIComponent(this.config.links[l].href);
							break;
						}
						case 'facebook_like': {
							button.className = ' KShareItem KShareItem_facebook_like ';
							button.innerHTML = '<iframe src="http://www.facebook.com/plugins/like.php?href=' + encodeURIComponent(this.config.links[l].href) + '&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:21px;" allowTransparency="true"></iframe>';
							break;
						}
						default: {
							
						}
					}
				}
				else {
					switch (this.config.links[l].rel) {
						case 'facebook': {
							button.href = 'https://www.facebook.com/sharer.php?u=' + encodeURIComponent(shellConf.providers[shellConf.preferredProvider].services + 'opengraph' + this.parent.config.href + '?lang=' + lang + '&url=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href)));
							break;
						}
						case 'twitter': {
							button.href = 'http://twitter.com/home?status=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.description ? this.parent.data.metadata.description + ' ' : '') + (!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href));
							break;
						}
						case 'orkut': {
							button.href = 'http://promote.orkut.com/preview?nt=orkut.com&tt=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.title ? this.parent.data.metadata.title : '')) + '&cn=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.description ? this.parent.data.metadata.description : '')) + '&du=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href));
							break;
						}
						case 'facebook_like': {
							button.className = ' KShareItem KShareItem_facebook_like ';
							button.innerHTML = '<iframe src="http://www.facebook.com/plugins/like.php?href=' + encodeURIComponent(shellConf.providers[shellConf.preferredProvider].services + 'opengraph' + this.parent.config.href + '?lang=' + lang + '&url=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href))) + '&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:21px;" allowTransparency="true"></iframe>';
							break;
						}
						default: {
							
						}
					}
				}
				if (this.config.links[l].rel != 'facebook_like') {
					button.target = '_blank';
					
					var link = button;
					
					button = window.document.createElement('div');
					KCSS.addCSSClass('KShareItem KShareItem_' + this.config.links[l].rel, button);
					
					button.appendChild(link);
					
					var count = window.document.createElement('div');
					count.className = ' KShareCount KShareCount_' + this.config.links[l].rel + ' ';
					button.appendChild(count);
					
					var pointers = window.document.createElement('s');
					count.appendChild(pointers);
					
					var pointeri = window.document.createElement('i');
					count.appendChild(pointeri);
					
					var number = window.document.createElement('span');
					number.innerHTML = '0';
					count.appendChild(number);
					
					this.buzz[this.config.links[l].rel].element = number;
					
				}
				
			}
			this.panel.appendChild(button);
		}
		
		this.retrieveBuzz();
		
		if (this.parent.activated()) {
			this.parent.content.appendChild(this.panel);
		}
		else {
			this.parent.container.appendChild(this.panel);
		}
	};
	
	KShare.prototype.retrieveBuzz = function() {
		for ( var l in this.config.links) {
			if (!!this.config.links[l].href) {
				switch (this.config.links[l].rel) {
					case 'facebook': {
						this.facebookp = window.document.createElement('script');
						this.facebookp.charset = 'utf-8';
						this.facebookp.src = 'https://graph.facebook.com/?id=' + encodeURIComponent(this.config.links[l].href) + '&callback=Kinky.bunnyMan.widgets.' + this.id + '.onBuzzFacebook';
						window.document.getElementsByTagName('head')[0].appendChild(this.facebookp);
						break;
					}
					case 'twitter': {
						this.twitterp = window.document.createElement('script');
						this.twitterp.charset = 'utf-8';
						this.twitterp.src = 'http://urls.api.twitter.com/1/urls/count.json?url=' + encodeURIComponent(this.config.links[l].href) + '&callback=Kinky.bunnyMan.widgets.' + this.id + '.onBuzzTwitter';
						window.document.getElementsByTagName('head')[0].appendChild(this.twitterp);
						break;
					}
					case 'orkut': {
						break;
					}
					default: {
						
					}
				}
			}
			else {
				switch (this.config.links[l].rel) {
					case 'facebook': {
						this.facebookp = window.document.createElement('script');
						this.facebookp.charset = 'utf-8';
						this.facebookp.src = 'https://graph.facebook.com/?id=' + shellConf.providers[shellConf.preferredProvider].services + 'opengraph' + this.parent.config.href + encodeURIComponent('?lang=' + lang + '&url=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href))) + '&callback=Kinky.bunnyMan.widgets.' + this.id + '.onBuzzFacebook';
						window.document.getElementsByTagName('head')[0].appendChild(this.facebookp);
						break;
					}
					case 'twitter': {
						this.twitterp = window.document.createElement('script');
						this.twitterp.charset = 'utf-8';
						this.twittwep.src = 'http://urls.api.twitter.com/1/urls/count.json?url=' + shellConf.providers[shellConf.preferredProvider].services + 'opengraph' + this.parent.config.href + encodeURIComponent('?lang=' + lang + '&url=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href))) + '&callback=Kinky.bunnyMan.widgets.' + this.id + '.onBuzzTwitter';
						window.document.getElementsByTagName('head')[0].appendChild(this.twitterp);
						break;
					}
					case 'orkut': {
						break;
					}
					default: {
						
					}
				}
			}
		}
	};
	
	KShare.getEventShare = function(event) {
		event = event || window.event;
		if (event == null) {
			return null;
		}
		var element = event.target || event.srcElement;
		while ((element != null) && !/^share([0-9]+)$/.test(element.id)) {
			element = element.parentNode;
		}
		
		return element;
	};
	
	KShare.getShare = function(domElement) {
		var element = domElement;
		while ((element != null) && !/^share([0-9]+)$/.test(element.id)) {
			element = element.parentNode;
		}
		return element;
	};
	
	KShare.prototype.tagNames = {
		html4 : {
			panel : 'div',
		},
		html5 : {
			panel : 'section',
		}
	};
	
	KSystem.registerAddOn('KShare');
	KSystem.included('KShare');
});
