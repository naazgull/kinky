function KBubble(parent, params) {
	if (!!parent) {
		this.prefix = 'bubble';
		KAddOn.call(this, parent);
		this.params = params;
		if (!this.params.gravity) {
			this.params.gravity = {
				x : 'left',
				y : 'top'
			};
		}
		this.parentRootDiv = this.parent.childDiv(!!this.params.target ? this.params.target : KWidget.ROOT_DIV);
		
		this.panel = window.document.createElement('div');
		this.panel.id = this.id;
		this.panel.className = ' KBubble ' + (!!params.cssClass ? params.cssClass : this.parent.className + 'Bubble') + ' ';
		this.panel.innerHTML = this.params.text;
		this.panel.style.display = 'none';
		this.panel.style.opacity = 0;
		
		parent.baloon = this;

		if (!!this.params.callback) {
			this.panel.style.cursor = 'pointer';
			KDOM.addEventListener(this.panel, 'click', this.params.callback);
		}

		KBreadcrumb.addActionListener(this, KBubble.unmark, [ '/bubble/unmark/' + params.id ]);
		this.go();
	}
}
KSystem.include([
	'KAddOn'
], function() {
	KBubble.prototype = new KAddOn();
	KBubble.prototype.constructor = KBubble;

	KBubble.prototype.go = function() {		
		var offsetX = (!!this.params.offsetX ? this.params.offsetX : 0);
		var offsetY = (!!this.params.offsetY ? this.params.offsetY : 0);
		this.params.timer = -1;		
		this.parentRootDiv.appendChild(this.panel);
		KBubble.show(this, offsetX, offsetY, this.params.gravity);

	};
	
	KBubble.unmake = function(baloon) {
		if (!!baloon.panel && !!baloon.panel.parentNode) {
			baloon.panel.parentNode.removeChild(baloon.panel);
			delete baloon.panel;
			baloon.panel = undefined;
		}
		KAddOn.prototype.unload.call(baloon);
	};
	
	KBubble.make = function(target, params) {
		var tooltip = new KBubble(target, params);
		return tooltip;
	};
	
	KBubble.show = function(widget, offsetX, offsetY, gravity) {
		
		widget.panel.style.position = 'absolute';
		widget.panel.style.display = 'block';
		
		var parentOffsetX = 0;
		var parentOffsetY = 0;
		if (!!gravity) {
			if (gravity.x == 'right') {
				widget.panel.style.right = (typeof(offsetX) == 'string' ? offsetX : offsetX + 'px');
			}
			else if (gravity.x == 'center') {
				parentOffsetX = widget.parent.offsetLeft();
				widget.panel.style.left = (parentOffsetX + widget.parent.getWidth() - Math.round(widget.panel.offsetWidth / 2) - offsetX) + 'px';
			}
			else {
				widget.panel.style.left = (typeof(offsetX) == 'string' ? offsetX : offsetX + 'px');
			}
			if (gravity.y == 'bottom') {
				widget.panel.style.bottom = (typeof(offsetY) == 'string' ? offsetY : offsetY + 'px');
			}
			else {
				widget.panel.style.top = (typeof(offsetY) == 'string' ? offsetY : offsetY + 'px');
			}
		}
		else {
			widget.panel.style.left = offsetX + 'px';
			widget.panel.style.top = offsetY + 'px';
		}

		widget.panel.style.opacity = 1;
	};

	KBubble.unmark = function(widget, action) {
		var n = parseInt(widget.panel.innerHTML);
		if (!isNaN(n) && n != 1) {
			widget.panel.innerHTML = '' + (n - 1);
		}
		else {
			KBubble.unmake(widget);
		}
	};

	KSystem.registerAddOn('KBubble');
	KSystem.included('KBubble');
});
