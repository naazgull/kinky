function KBaloon(parent, params) {
	if (!!parent) {
		this.prefix = 'baloon';
		KAddOn.call(this, parent);
		this.hash = '/kbaloon';
		this.params = params;
		if (!this.params.gravity) {
			this.params.gravity = {
				x : 'left',
				y : 'top'
			};
		}
		this.parentRootDiv = this.parent.childDiv(!!this.params.target ? this.params.target : KWidget.ROOT_DIV);
		
		this.panel = window.document.createElement('div');
		this.panel.id = this.id;
		this.panel.className = ' KBaloon ' + (!!params.cssClass ? params.cssClass : this.parent.className + 'Baloon') + ' ';
		this.panel.innerHTML = '<p>' + this.params.text + '</p>';
		this.panel.style.display = 'none';
		this.panel.style.opacity = 0;
		
		parent.baloon = this;

		this.go();
	}
}
KSystem.include([
	'KAddOn'
], function() {
	KBaloon.prototype = new KAddOn();
	KBaloon.prototype.constructor = KBaloon;
	
	KBaloon.prototype.destroy = function() {
		this.hide();
	};

	KBaloon.prototype.blur = function() {
		this.zoomed = false;
		this.getShell().hideOverlay();

		this.hide();

		this.panel.style.position = 'absolute';
		this.panel.style.top = 'auto';
		this.panel.style.left = 'auto';
		this.panel.style.maxWidth = 'auto';
		this.panel.style.maxHeight = 'auto';
		this.panel.style.opacity = '0';

	};
	
	KBaloon.prototype.go = function() {		
		this.params.timer = -1;
		KDOM.addEventListener(this.parentRootDiv, 'mouseover', KBaloon.over);
		KDOM.addEventListener(this.parentRootDiv, 'mouseout', KBaloon.out);
		if (!!this.params.zoom) {
			KDOM.addEventListener(this.parentRootDiv, 'click', KBaloon.click);
		}
		this.parentRootDiv.appendChild(this.panel);
	};

	KBaloon.over = function(event) {
		var widget = KDOM.getEventWidget(event).baloon;
		var offsetX = (!!widget.params.offsetX ? widget.params.offsetX : 0);
		var offsetY = (!!widget.params.offsetY ? widget.params.offsetY : 0);

		if (!!widget.params.delay) {
			widget.params.timer = KSystem.addTimer(function() {
				widget.params.timer = -1;
				KBaloon.show(widget, offsetX, offsetY, widget.params.gravity, KDOM.getMouseCoords(event));
			}, widget.params.delay);
			return;
		}
		KBaloon.show(widget, offsetX, offsetY, null, KDOM.getMouseCoords(event));
	};

	KBaloon.out =  function(event) {
		var widget = KDOM.getEventWidget(event).baloon;
		if (!widget || !!widget.zoomed) {
			return;
		}

		if (widget.params.timer != -1) {
			KSystem.removeTimer(widget.params.timer);
			widget.params.timer = -1;
			return;
		}
		widget.transition_hide();
	};

	KBaloon.click =  function(event) {
		var widget = KDOM.getEventWidget(event).baloon;
		if (!widget) {
			return;
		}
		widget.zoomed = true;
		widget.getShell().showOverlay();
		
		widget.panel.style.position = 'fixed';
		widget.panel.style.maxHeight = !!widget.params.zoom.height ? widget.params.zoom.height + 'px' : (KDOM.getBrowserHeight() - 100) + 'px';
		widget.panel.style.maxWidth = !!widget.params.zoom.width ? widget.params.zoom.width + 'px' : '980px';

		KSystem.addTimer(function() {
			widget.panel.style.top = Math.round((KDOM.getBrowserHeight() - (!!widget.params.zoom.height ? widget.params.zoom.height : widget.panel.offsetHeight)) / 2) + 'px';
			widget.panel.style.left = Math.round((KDOM.getBrowserWidth() - (!!widget.params.zoom.width ? widget.params.zoom.width : widget.panel.offsetWidth)) / 2) + 'px';
			Kinky.setModal(widget);
		}, 100);

	};
	
	KBaloon.unmake = function(baloon) {
		baloon.parentRootDiv.removeChild(baloon.panel);
		KDOM.removeEventListener(baloon.parentRootDiv, 'mouseover', KBaloon.over);
		KDOM.removeEventListener(baloon.parentRootDiv, 'mouseout', KBaloon.out);
		KAddOn.prototype.unload.call(baloon);
	};
	
	KBaloon.make = function(target, params) {
		var tooltip = new KBaloon(target, params);
		return tooltip;
	};
	
	KBaloon.show = function(widget, offsetX, offsetY, gravity, pos) {
		widget.panel.style.position = 'absolute';
		widget.panel.style.display = 'block';
		window.document.body.appendChild(widget.panel);
		
		var MARGIN_W = 60;
		var MARGIN_H = 60;
		var parentOffsetX = 0;
		var parentOffsetY = 0;
		var reference = (!!widget.params && !!widget.params.offsetParent ? widget.params.offsetParent : widget.parent.panel);

		if (!!gravity) {
			if (gravity.x == 'right') {
				widget.panel.style.maxWidth = (!!widget.params && !!widget.params.max && !!widget.params.max.width && widget.params.max.width < (pos.x - MARGIN_W) ? widget.params.max.width : (pos.x - MARGIN_W)) + 'px';
				parentOffsetX = KDOM.offsetLeft(reference);
				widget.panel.style.left = (parentOffsetX + KDOM.getWidth(reference) - widget.panel.offsetWidth - offsetX) + 'px';
			}
			else if (gravity.x == 'center') {
				widget.panel.style.maxWidth = (!!widget.params && !!widget.params.max && !!widget.params.max.width && widget.params.max.width < (pos.x - MARGIN_W) ? widget.params.max.width : (pos.x - MARGIN_W)) + 'px';
				parentOffsetX = KDOM.offsetLeft(reference);
				widget.panel.style.left = (parentOffsetX + KDOM.getWidth(reference) - Math.round(widget.panel.offsetWidth / 2) - offsetX) + 'px';
			}
			else {
				widget.panel.style.maxWidth = (!!widget.params && !!widget.params.max && !!widget.params.max.width && widget.params.max.width < (KDOM.getBrowserWidth() - pos.x - MARGIN_W) ? widget.params.max.width : (KDOM.getBrowserWidth() - pos.x - MARGIN_W)) + 'px';
				parentOffsetX = KDOM.offsetLeft(reference);
				widget.panel.style.left = (parentOffsetX + offsetX) + 'px';
			}
			if (gravity.y == 'bottom') {
				widget.panel.style.maxHeight = (pos.y - MARGIN_H) + 'px';
				parentOffsetY = KDOM.offsetTop(reference);
				widget.panel.style.top = (parentOffsetY + KDOM.getHeight(reference) - widget.panel.offsetHeight - offsetY - KDOM.scrollTop(reference)) + 'px';
			}
			else if (gravity.y == 'middle') {
				widget.panel.style.maxHeight = 'auto';
				parentOffsetY = KDOM.offsetTop(reference);
				widget.panel.style.top = Math.min(KDOM.getBrowserHeight() - Math.round(widget.panel.offsetHeight) - offsetY, Math.max(0, parentOffsetY + KDOM.getHeight(reference) - Math.round(widget.panel.offsetHeight / 2) - offsetY - KDOM.scrollTop(reference))) + 'px';
			}
			else {
				widget.panel.style.maxHeight = (KDOM.getBrowserHeight() - pos.y - MARGIN_H) + 'px';
				parentOffsetY = KDOM.offsetTop(reference);
				widget.panel.style.top = (parentOffsetY + offsetY - KDOM.scrollTop(reference)) + 'px';
			}
		}
		else {
			widget.panel.style.maxWidth = (!!widget.params && !!widget.params.max && !!widget.params.max.width && widget.params.max.width < (KDOM.getBrowserWidth() - pos.x - MARGIN_W) ? widget.params.max.width : (KDOM.getBrowserWidth() - pos.x - MARGIN_W)) + 'px';
			widget.panel.style.maxHeight = (KDOM.getBrowserHeight() - pos.y - MARGIN_H) + 'px';
			parentOffsetX = KDOM.offsetLeft(reference);
			widget.panel.style.left = (parentOffsetX + offsetX) + 'px';
			parentOffsetY = KDOM.offsetTop(reference);
			widget.panel.style.top = (parentOffsetY + offsetY - KDOM.scrollTop(reference)) + 'px';
		}

		widget.transition_show();
	};
	
	KBaloon.prototype.hide = function() {
		this.parentRootDiv.appendChild(this.panel);
		this.panel.style.display = 'none';
		this.panel.style.position = 'relative';
		if (!!this.animating) {
			this.panel.childNodes[0].style.marginTop = '0px';
			KEffects.cancelEffect(this.animating.id);
			delete this.animating;
			this.animating = undefined;
		}
	};
	
	KBaloon.prototype.transition_show = function() {
		var _this = this;
		var defaultEnterTween = {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 1000,
			go : {
				alpha : 1
			},
			from : {
				alpha : 0
			},
		};
		KEffects.addEffect(this, (!!this.params.enterTween ? this.params.enterTween : defaultEnterTween));
	};
	
	KBaloon.prototype.transition_hide = function() {
		var widget = this;
		var defaultExitTween = {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 1000,
			go : {
				alpha : 0
			},
			from : {
				alpha : 1
			},
			onComplete : function() {
				// caso seja subcarregado este metodo NAO ESQUECER
				widget.hide();
			}
		};
		KEffects.addEffect(this, (!!this.params.exitTween ? this.params.exitTween : defaultExitTween));
	};
	
	KSystem.registerAddOn('KBaloon');
	KSystem.included('KBaloon');
});
