function KShell(parent) {
	if (parent != null) {
		this.isLeaf = false;
		KWidget.call(this, parent);
		
		this.homepageURL = null;
		this.autoLoad = false;
		this.elements = {};
		this.parentPanel = null;
		this.usingLoader = 0;
		this.loader = null;
		this.loaderOnTop = true;
		
		this.overlay = window.document.createElement('div');
		{
			this.overlay.appendChild(window.document.createTextNode(' '));
			this.overlay.className = ' KShellOverlay ' + this.className + 'Overlay ';
			this.setStyle({
				width : KDOM.getBrowserWidth() + 'px',
				height : KDOM.getBrowserHeight() + 'px',
				position : 'fixed',
				top : '0',
				left : '0',
				display : 'none'
			}, this.overlay);
		}
		this.overlayWidgets = 0;
		
		this.addLocationListener(KShell.go);
		this.addWindowResizeListener(KShell.resize);
		
		this.center = {
			loader : {
				width : true,
				height : true
			}
		};
		
		this.background = window.document.createElement('div');
		this.background.className = ' KWidgetPanelBackground ' + this.className + 'Background ';
		this.background.style.position = 'absolute';
		this.background.style.top = '0px';
		this.background.style.left = '0px';
		this.panel.insertBefore(this.background, this.content);
		
		this.loader = window.document.createElement('div');
		{
			this.loader.className = ' KWidgetLoader ';
			
			var img = window.document.createElement('img');
			img.src = Kinky.LOADER_IMAGE;
			this.loader.appendChild(img);
			
			KCSS.setStyle({
				display : 'none'
			}, [
				this.loader
			]);
		}
		this.panel.appendChild(this.loader);
	}
}

KSystem.include([
	'KShellPart',
	'KWidget'
], function() {
	KShell.prototype = new KWidget();
	KShell.prototype.constructor = KShell;
	
	KShell.prototype.focus = function(dispatcher) {
	};
	
	KShell.prototype.onChildLoaded = function(childWidget) {
		if ((this.size() <= this.loadedChildren)) {
			if (this.activated()) {
				var div = childWidget.childDiv(KWidget.ROOT_DIV);
				var rootDiv = this.childDiv(childWidget.shellDiv);
				rootDiv.appendChild(div);
			}
			else {
				this.activate();
			}
		}
	};
	
	KShell.prototype.insertBefore = function(element, before, childDiv) {
		if (this.activated() && element instanceof KShellPart) {
			element.shellDiv = (!!childDiv ? childDiv : 'content');
			return KWidget.prototype.insertBefore.call(this, element, before, this.container);
			return element;
		}
		else {
			return KWidget.prototype.insertBefore.call(this, element, before, childDiv);
		}
	};
	
	KShell.prototype.insertAfter = function(element, after, childDiv) {
		if (this.activated() && element instanceof KShellPart) {
			element.shellDiv = (!!childDiv ? childDiv : 'content');
			return KWidget.prototype.insertAfter.call(this, element, after, this.container);
			return element;
		}
		else {
			return KWidget.prototype.insertAfter.call(this, element, after, childDiv);
		}
	};
	
	KShell.prototype.appendChild = function(element, childDiv) {
		if (this.activated()) {
			element.shellDiv = (!!childDiv ? childDiv : 'content');
			KWidget.prototype.appendChild.call(this, element, this.container);
			return element;
		}
		else {
			return KWidget.prototype.appendChild.call(this, element, childDiv);
		}
	};
	
	KShell.prototype.visible = function() {
		this.display = true;
		for (; this.container.childNodes.length != 0;) {
			var div = this.container.childNodes[0];
			this.container.removeChild(div);
			this.content.appendChild(div);
		}
		this.computeScrollbars(this);
	};
	
	KShell.prototype.invisible = function() {
		this.display = false;
		for (; this.content.childNodes.length != 0;) {
			var div = this.content.childNodes[0];
			this.content.removeChild(div);
			this.container.appendChild(div);
		}
	};
	
	KShell.prototype.onLoadChild = function(data, request) {
		var child = KSystem.construct(data, this);
		if (child == null) {
			return;
		}
		this.appendChild(child, (!!data.target ? data.target : null));
	};
	
	KShell.prototype.refresh = function() {
		var config = KSystem.clone(Kinky.getShellConfig(this.shell));
		this.cleanCache();
		this.parent.removeChild(this.panel);
		this.parent = null;
		this.unload();
		Kinky.clearWidgets(this.shell);
		Kinky.loadShell(config);
	};
	
	KShell.prototype.start = function(homeURL) {
		this.shell = this.config.id;
		this.homepageURL = homeURL;
		this.parentPanel = this.parent;
		
		if (!!this.data && !!this.data.links) {
			this.onLoad(this.data);
		}
		else {
			this.load();
		}
		this.parentPanel.innerHTML = '';
		this.parentPanel.appendChild(this.panel);
	};
	
	KShell.prototype.draw = function() {
		if (!!this.data) {
			if (!!this.data && !!this.data['class']) {
				this.addCSSClass(this.data['class']);
			}
			
			if (!!this.data.title) {
				this.setTitle(this.data.title);
			}
			
			if (!!this.data.graphics && !!this.data.graphics.background) {
				this.setBackground(this.data.graphics.background);
			}
		}
		
		if (this.content.nextSibling) {
			this.panel.insertBefore(this.overlay, this.content.nextSibling);
		}
		else {
			this.panel.appendChild(this.overlay);
		}
		
		this.shellparts = {};
		if (!!this.data.children) {
			for ( var link in this.data.children.elements) {
				var jsClass = KSystem.getWRMLClass(this.data.children.elements[link], 'application/vnd.kinky.');
				if (!!jsClass && !KSystem.isAddOn(jsClass)) {
					var child = null;
					try {
						child = KSystem.construct(this.data.children.elements[link], this);
					}
					catch (e) {
						continue;
					}
					if (!(child instanceof KShellPart)) {
						this.appendChild(child, (!!this.data.children.elements[link].target ? this.data.children.elements[link].target : null));
					}
					else {
						this.shellparts[child.hash] = this.data.children.elements[link];
						child.destroy();
						delete child;
					}
				}
			}
		}
		
		for ( var element in this.childWidgets()) {
			var w = this.childWidget(element);
			if (w.silent) {
				this.loadedChildren++;
				this.onChildLoaded(w);
			}
			if (!(w instanceof KShellPart) && !w.activated()) {
				this.childWidget(element).go();
			}
		}
		
		var page = null;
		if (window.document.location.href.indexOf('#') != -1) {
			page = window.document.location.href.split('#')[1];
		}
		if (this.activated()) {
			if (!this.childWidget(page)) {
				this.onHashChange(page);
			}
		}
		else {
			if (this.nChildren == 0) {
				this.activate();
				if (!!page && !this.childWidget(page)) {
					this.onHashChange(page);
					return;
				}
			}
			if (!page) {
				if (!!Kinky.SHELL_CONFIG[this.shell].startPage) {
					KBreadcrumb.dispatchURL({
						hash : Kinky.SHELL_CONFIG[this.shell].startPage
					});
				}
			}
		}
		if (!this.loaderOnTop && this.background) {
			this.background.appendChild(this.loader);
		}
		if (this.center.loader.width) {
			KCSS.setStyle({
				position : 'fixed',
				left : Math.round(KDOM.getBrowserWidth() / 2 - this.loader.offsetWidth / 2) + 'px'
			}, [
				this.loader
			]);
		}
		if (this.center.loader.height) {
			KCSS.setStyle({
				position : 'fixed',
				top : Math.round(KDOM.getBrowserHeight() / 2 - this.loader.offsetHeight / 2) + 'px'
			}, [
				this.loader
			]);
		}
		
	};
	
	KShell.prototype.childShellPart = function(hash) {
		var data = null;
		data = this.shellparts[hash];
		if (!data) {
			for ( var c in this.shellparts) {
				var regex = new RegExp(c + '/(.+)');
				if (regex.test(hash)) {
					if ((!!this.shellparts[c].hash && !(data)) || (!!data && (data.hash.length < c.length))) {
						data = this.shellparts[c];
					}
				}
			}
		}
		return data;
	};
	
	KShell.prototype.showLoading = function() {
		if (this.loader_t === undefined) {
			var self = this;
			this.loader_t = KSystem.addTimer(function() {
				KCSS.setStyle({
					display : 'block',
					opacity : '0'
				}, [
				self.loader
				]);
				KCSS.setStyle({
					opacity : '1',
					left : Math.round(KDOM.getBrowserWidth() / 2 - self.loader.offsetWidth / 2) + 'px',
					top : Math.round(KDOM.getBrowserHeight() / 2 - self.loader.offsetHeight / 2) + 'px'
				}, [
				self.loader
				]);
			}, 350);
		}
		this.usingLoader++;
	};

	KShell.prototype.hideLoading = function() {
		this.usingLoader--;
		if (this.usingLoader <= 0) {
			this.usingLoader = 0;
			if (this.loader_t !== undefined) {
				KSystem.removeTimer(this.loader_t);
				this.loader_t = undefined;
			}
			KCSS.setStyle({
				opacity : '0'
			}, [
				this.loader
			]);
			var self = this;
			KSystem.addTimer(function(){
				KCSS.setStyle({
					display : 'none'
				}, [
				self.loader
				]);
			}, 200);
		}
	};

	KShell.prototype.showOverlay = function() {
		this.overlayWidgets++;
		this.setStyle({
			display : 'block'
		}, this.overlay);
		this.setStyle({
			opacity : '1'
		}, this.overlay);
	};
	
	KShell.prototype.hideOverlay = function(force) {
		this.overlayWidgets--;
		if ((this.overlayWidgets <= 0) || force) {
			this.overlayWidgets = 0;

			KCSS.setStyle({
				opacity : '1'
			}, [
				this.overlay
			]);
			var self = this;
			KSystem.addTimer(function(){
				KCSS.setStyle({
					display : 'none'
				}, [
				self.overlay
				]);
			}, 200);
		}
	};
	
	KShell.prototype.onHashChange = function(hash) {
		var data = this.childShellPart(hash);
		if (!data) {
			KBreadcrumb.dispatchEvent(null, {
				action : '/not-found'
			});
			return;
		}
		
		KBreadcrumb.drilldown = hash.replace(data.hash, '');
		if (KBreadcrumb.drilldown == '') {
			KBreadcrumb.drilldown = null;
		}
		
		var child = this.childWidget(data.hash);
		if (!!child) {
			child.go();
		}
		else {
			this.onLoadChild(data, hash);
		}
	};

	KShell.prototype.popupBlocked = function() {
		alert('Grrrrrrrrrr, a popup blocker is preventing you from continuing your interaction with this page.\nPlease, add this domain has an exception.')
	};

	KShell.go = function(widget, hash) {
		if (widget.activated()) {
			widget.onHashChange(hash);
		}
	};
	
	KShell.resize = function(site) {
		site.setStyle({
			width : KDOM.getBrowserWidth() + 'px',
			height : KDOM.getBrowserHeight() + 'px'
		}, site.overlay);
		
	};
	
	KShell.setWindowTitle = function(text) {
		window.document.title = text;
	};
	
	KShell.current = null;
	KShell.OVERLAY_DIV = 'overlay';
	KShell.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div'
		},
		html5 : {
			panel : 'section',
			content : 'div',
			background : 'div'
		}
	};
	
	KSystem.included('KShell');
});
