function KGallery(parent) {
	if (parent != null) {
		KList.call(this, parent);
		this.addActionListener(KGallery.popup, [
			'/gallery/view-image/*'
		]);
		this.nImage = [
			0
		];
	}
}

KSystem.include([
	'KList',
], function() {
	KGallery.prototype = new KList();
	KGallery.prototype.constructor = KGallery;
	
	KGallery.prototype.appendChild = function(element, context) {
		if (element instanceof KLink) {
			element.isImageLink = true;
		}
		else if (element instanceof KImage) {
			var a = window.document.createElement('a');
			if (this.paginationDispatchType == 'url') {
				a.href = this.getLink({
					action : '/gallery/view-image/' + this.getImageCount()
				});
			}
			else {
				var imageCount = this.getImageCount();
				a.href = 'javascript:void(0)';
				KDOM.addEventListener(a, 'click', function(event) {
					var widget = KDOM.getEventWidget(event);
					KBreadcrumb.dispatchEvent(widget.parent.id, {
						action : '/gallery/view-image/' + imageCount
					});
				});
			}
			element.maxWidth = this.maxWidth;
			element.maxHeight = this.maxHeight;
			element.content.style.overflow = 'hidden';
			element.content.style.height = this.maxHeight + 'px';
			element.content.style.width = this.maxWidth + 'px';
			element.content.appendChild(a);
			element.content = a;
			element.content.appendChild(element.image);
		}
		this.setImageCount();
		KList.prototype.appendChild.call(this, element, context);
	};
	
	KGallery.prototype.getImageCount = function() {
		var context = this.query + this.nPage;
		if (this.hasPaginationBar && (Math.floor(this.nChildren / this.perPage) != this.nPage)) {
			context = this.query + Math.floor(this.nChildren / this.perPage);
		}
		
		if (this.nImage[context] == null) {
			this.nImage[context] = 0;
		}
		return this.nImage[context];
	};
	
	KGallery.prototype.setImageCount = function() {
		var context = this.query + this.nPage;
		if (this.hasPaginationBar && (Math.floor(this.nChildren / this.perPage) != this.nPage)) {
			context = this.query + Math.floor(this.nChildren / this.perPage);
		}
		
		if (this.nImage[context] == null) {
			this.nImage[context] = 0;
		}
		this.nImage[context]++;
	};
	
	KGallery.popup = function(widget, action) {
		if (widget.activated() && widget.getParent('KShellPart') && (widget.getParent('KShellPart').hash == widget.getHash())) {
			var imageID = action.split('/');
			var image = widget.childAt(imageID[3]);
			if (image) {
				var dialog = null;
				if (Kinky.shell.childWidget('/gallery/popup')) {
					dialog = Kinky.shell.childWidget('/gallery/popup');
				}
				else {
					Kinky.shell.showOverlay();
					
					dialog = new KDialog(Kinky.shell);
					dialog.type = KDialog.MODAL;
					dialog.addCSSClass('KGalleryDialog');
					dialog.hash = '/gallery/popup';
					dialog.nextCallback = function(event) {
						var nImage = parseInt(widget.getAction().split('/')[3]);
						var nextImage = widget.childAt(++nImage);
						if (nextImage) {
							KEffects.addEffect(dialog, {
								f : KEffects.easeOutExpo,
								type : 'resize',
								duration : 200,
								go : {
									height : 0
								},
								lock : {
									width : true
								},
								onComplete : function(wDialog, tween) {
									Kinky.shell.overlayWidgets--;
									if (widget.paginationDispatchType == 'url') {
										KBreadcrumb.dispatchURL({
											action : '/gallery/view-image/' + nImage
										});
									}
									else {
										KBreadcrumb.dispatchEvent({
											action : '/gallery/view-image/' + nImage
										});
									}
								}
							});
						}
					};
					dialog.previousCallback = function(event) {
						var nImage = parseInt(widget.getAction().split('/')[3]);
						var prevImage = widget.childAt(--nImage);
						if (prevImage) {
							KEffects.addEffect(dialog, {
								f : KEffects.easeOutExpo,
								type : 'resize',
								duration : 200,
								go : {
									height : 0
								},
								lock : {
									width : true
								},
								onComplete : function(wDialog, tween) {
									Kinky.shell.overlayWidgets--;
									if (widget.paginationDispatchType == 'url') {
										KBreadcrumb.dispatchURL({
											action : '/gallery/view-image/' + nImage
										});
									}
									else {
										KBreadcrumb.dispatchEvent({
											action : '/gallery/view-image/' + nImage
										});
									}
								}
							});
						}
					};
					dialog.closeCallback = function(event) {
						Kinky.shell.removeChild('/gallery/popup');
						KDialog.close(event);
						Kinky.shell.hideOverlay(true);
					};
					dialog.buttons = KWidget.CLOSE | KWidget.NEXT | KWidget.PREVIOUS;
					Kinky.shell.appendChild(dialog);
					Kinky.shell.panel.appendChild(dialog.panel);
				}
				
				if (image.data.popupURL) {
					image.onLoadPopup = function() {
						dialog.setStyle({
							width : this.data.popupWidth + 'px',
							marginTop : Math.round((KDOM.getBrowserHeight() - this.data.popupHeight) / 2) + 'px',
							height : '0px',
							left : Math.round(KDOM.getBrowserWidth() / 2 - this.data.popupWidth / 2) + 'px',
							top : '0px'
						});
						
						dialog.effects = {
							enter : {
								f : KEffects.easeOutExpo,
								type : 'resize',
								duration : 200,
								go : {
									height : this.data.popupHeight
								},
								lock : {
									width : true
								}
							},
							exit : {
								f : KEffects.easeOutExpo,
								type : 'resize',
								duration : 200,
								go : {
									height : 0
								},
								lock : {
									width : true
								},
								onComplete : function(wDialog, tween) {
									Kinky.shell.removeChild(wDialog);
									Kinky.shell.hideOverlay();
									if (widget.paginationDispatchType == 'url') {
										KBreadcrumb.back();
									}
								}
							}
						};
						
						KDialog.show(dialog, {
							title : '',
							content : '<img src="' + this.data.popupURL + '"></img>'
						});
					};
					image.loadPopupImage();
				}
				else {
					dialog.setStyle({
						width : image.data.width + 'px',
						marginTop : Math.round((KDOM.getBrowserHeight() - image.data.height) / 2) + 'px',
						height : '0px',
						left : Math.round(KDOM.getBrowserWidth() / 2 - image.data.width / 2) + 'px',
						top : '0px'
					});
					
					dialog.effects = {
						enter : {
							f : KEffects.easeOutExpo,
							type : 'resize',
							duration : 200,
							go : {
								height : image.data.height
							},
							lock : {
								width : true
							}
						},
						exit : {
							f : KEffects.easeOutExpo,
							type : 'resize',
							duration : 200,
							go : {
								height : 0
							},
							lock : {
								width : true
							},
							onComplete : function(wDialog, tween) {
								Kinky.shell.removeChild(wDialog);
								Kinky.shell.hideOverlay();
								if (widget.paginationDispatchType == 'url') {
									KBreadcrumb.back();
								}
							}
						}
					};
					
					KDialog.show(dialog, {
						title : '',
						content : '<img src="' + image.data.url + '"></img>'
					});
				}
			}
		}
	};
	
	KSystem.included('KGallery');
});