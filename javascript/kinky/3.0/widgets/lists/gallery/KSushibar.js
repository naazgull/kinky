function KSushibar(parent) {
	if (parent != null) {
		this.hasPaginationBar = true;
		KGallery.call(this, parent);
		this.autoScroll = false;
		this.timeStep = 5000;
		this.perPage = 4;
		this.transitionTimer = null;
		
	}
}

KSystem.include([
	'KGallery'
], function() {
	
	KSushibar.prototype = new KGallery();
	KSushibar.prototype.constructor = KSushibar;
	
	KSushibar.prototype.draw = function() {
		var index = 1;
		for ( var element in this.childWidgets()) {
			var child = this.childWidget(element);
			child.addCSSClass('KSushibarItem' + index++);
			child.go();
		}
		
		if (this.autoScroll && (this.transitionTimer == null)) {
			this.transitionTimer = KSystem.addTimer('KSushibar.nextSlide(\'' + this.id + '\')', this.timeStep, true);
		}
		this.activate();
	};
	
	KSushibar.prototype.stopSlideshow = function() {
		KSystem.removeTimer(this.transitionTimer);
	};
	
	KSushibar.nextSlide = function(id) {
		var widget = null;
		widget = Kinky.bunnyMan.widgets['\'' + id + '\''];
		var parent = widget.getParent('KShellPart');
		if (parent.hash == KBreadcrumb.getHash()) {
			KWidget.dispatchNext(null, widget);
		}
	};
	
	KSushibar.prototype.continueSlideshow = function() {
		if (this.autoScroll) {
			this.transitionTimer = KSystem.addTimer('KSushibar.nextSlide(\'' + this.id + '\')', this.timeStep, true);
		}
	};
	
	KSystem.included('KSushibar');
});