function KList(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.addActionListener(KList.actions, [
			'/page/*',
			'/per-page/*'
		]);
		this.isLeaf = false;
		this.pages = {};
		this.page = 0;
		this.lastpage = -1;
		this.pageSize = 0;
		this.totalSize = null;
		this.autoRotate = 0;
		this.autoRotateTimer = null;
		this.noMorePages = false;
		
		this.header = window.document.createElement('div');
		{
			this.header.className = ' ' + this.className + 'ListHeader ';
		}
		this.panel.insertBefore(this.header, this.content);
		
		this.footer = window.document.createElement('div');
		{
			this.footer.className = ' ' + this.className + 'ListFooter ';
		}
		this.panel.appendChild(this.footer);
	}
}

KSystem.include([
	'KWidget',
	'KCombo'
], function() {
	KList.prototype = new KWidget();
	KList.prototype.constructor = KList;
	
	KList.prototype.load = function() {
		if (!!this.config && !!this.config.href) {
			var headers = null;
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + this.config.href, {}, headers);
		}
		else {
			this.draw();
		}
	};
	
	KList.prototype.onNoContent = function(data, request) {
		if (!this.feed_retrieved && !this.no_feed) {
			this.retrieveFeed(data, request);
			return;
		}
		else {
			this.no_feed = !this.data || !this.data.feed || ((this.data.feed.size == 0));
		}
		
		if (this.feed_retrieved && !this.no_children && !this.children_retrieved) {
			this.retrieveChildren(data, request);
			return;
		}
		else {
			this.no_children = !this.data || !this.data.children || ((this.data.children.size == 0));
		}
		
		if (!this.data.children && !this.data.feed) {
			if (this.activated()) {
				this.afterNext();
			}
			else {
				this.abort();
			}
		}
		else {
			this.draw();
		}
	};
	
	KList.prototype.onLoad = function(data, request) {
		if (!!this.data && !!this.data.children && !!this.data.children.size) {
			this.totalSize = this.data.children.size;
		}
		if (!!data.autoRotate && (((data.autoRotate != '0')))) {
			this.autoRotate = parseInt(data.autoRotate);
		}
		if (!!data.pageSize && (((data.pageSize != '0')))) {
			if (!this.activated()) {
				this.silence();
				this.pageSize = parseInt(data.pageSize);
				this.data = data;
				
				KCache.commit(this.ref + '/data', data);
				if (this.pageSize != 0) {
					this.addPagination(this.header, this.totalSize);
					this.addPagination(this.footer, this.totalSize);
				}
			}
			if (!this.no_children && !this.children_retrieved) {
				this.retrieveChildren(data, request);
			}
			else if (!this.no_feed && !this.feed_retrieved) {
				this.retrieveFeed(data, request);
			}
		}
		else {
			KWidget.prototype.onLoad.call(this, data, request);
		}
	};
	
	KList.prototype.retrieveFeed = function(config_data, config_request) {
		var retrieved = this.feed_retrieved;
		var elements = KCache.restore(this.ref + '/feed/' + this.page);
		this.data.feed = elements;
		this.feed_retrieved = true;
		
		if (!elements && !retrieved && !!this.data.template && !!this.data.template.widget && !!this.data.links && !!this.data.links.feed && !!this.data.links.feed.href) {
			if (!KSystem.isIncluded(this.data.template.widget.type)) {
				this.feed_retrieved = false;
				var id = this.id;
				KSystem.include([
					this.data.template.widget.type
				], function() {
					if (!!Kinky.getWidget(id)) {
						Kinky.getWidget(id).retrieveFeed();
					}
				});
				return;
			}
			var headers = null;
			var args = null;
			if (this.pageSize != 0) {
				args = {
					fields : 'elements,size',
					pageSize : this.pageSize,
					pageStartIndex : this.page * this.pageSize
				};
			}
			else {
				args = {
					fields : 'elements,size'
				};
			}
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + KSystem.urlify(this.data.links.feed.href, args), {}, headers, 'onFeed');
		}
		else if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren();
		}
		else {
			if (!!elements) {
				this.totalSize = elements.size;
			}
			this.draw();
		}
		
	};
	
	KList.prototype.retrieveChildren = function(config_data, config_request) {
		var retrieved = this.children_retrieved;
		var elements = KCache.restore(this.ref + '/children/' + this.page);
		this.data.children = elements;
		this.children_retrieved = true;
		
		if (!elements && !this.isLeaf && !retrieved && !!this.data.links && !!this.data.links.children && !!this.data.links.children.href) {
			var headers = null;
			var args = null;
			if (this.pageSize != 0) {
				args = {
					fields : 'elements,size',
					pageSize : this.pageSize,
					pageStartIndex : this.page * this.pageSize
				};
			}
			else {
				args = {
					fields : 'elements,size'
				};
			}
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + KSystem.urlify(this.data.links.children.href, args), {}, headers, 'onChildren');
		}
		else if (!this.no_feed && !this.feed_retrieved) {
			this.retrieveFeed();
		}
		else {
			if (!!elements) {
				this.totalSize = elements.size;
			}
			this.draw();
		}
	};
	
	KList.prototype.onChildren = function(data, request) {
		this.totalSize = data.size;
		this.addPage();
		KWidget.prototype.onChildren.call(this, data, request);
		if (!!request) {
			this.data.children.size = this.totalSize;
			KCache.commit(this.ref + '/children/' + this.page, this.data.children);
		}
	};
	
	KList.prototype.onFeed = function(data, request) {
		this.totalSize = data.size;
		this.addPage();
		this.data.feed = {};
		
		var config = {
			hash : this.hash,
			rel : "\/wrml-relations\/widgets\/{widget_id}",
			requestTypes : [
				"application\/json",
				"application\/vnd.kinky." + this.data.template.widget.type
			],
			responseTypes : [
				"application\/json",
				"application\/vnd.kinky." + this.data.template.widget.type
			],
			links : {
				self : {
					href : this.config.href,
					rel : "\/wrml-relations\/widgets\/{widget_id}"
				}
			}
		};
		
		for ( var l in data.elements) {
			var childConfig = KSystem.clone(config);
			var id = (!!this.pageSize && ((this.pageSize != 0)) ? this.page * this.pageSize + parseInt(l) : l);
			childConfig.hash += '/' + id;
			childConfig.links.self.href += '/' + id;
			
			childConfig.data = new Object();
			childConfig.data.feeditem = data.elements[l];
			for ( var t in this.data.template.translationTable) {
				var name = this.data.template.translationTable[t].srcField;
				var value = data.elements[l][name];
				childConfig.data[this.data.template.translationTable[t].dstField] = value;
				if (!value && ((name == 'id')) && !!data.elements[l]._id) {
					childConfig.data[this.data.template.translationTable[t].dstField] = data.elements[l]._id;
				}
			}
			this.data.feed[childConfig.hash] = childConfig;
		}
		if (!!request) {
			this.data.feed.size = this.totalSize;
			KCache.commit(this.ref + '/feed/' + this.page, this.data.feed);
		}
		
		if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren();
		}
		else {
			this.draw();
		}
	};
	
	KList.prototype.draw = function() {
		KWidget.prototype.draw.call(this);
		this.pageVisible(this.page);
	};
	
	KList.prototype.destroy = function() {
		if (!!this.autoRotateTimer) {
			KSystem.removeTimer(this.autoRotateTimer);
		}
		KWidget.prototype.destroy.call(this);
	};
	
	KList.prototype.addPage = function(n_page) {
		if (n_page == null) {
			n_page = this.page;
		}
		
		var id = 'page' + n_page;
		if (!!this.pages[id]) {
			return;
		}
		
		this.pages[id] = window.document.createElement('div');
		{
			KCSS.addCSSClass('KListPage ' + this.className + 'Page ' + this.className + 'Page' + this.page, this.pages[id]);
			KCSS.setStyle({
				display : 'none',
				position : 'relative'
			}, [
				this.pages[id]
			]);
		}
		if (this.activated()) {
			this.content.appendChild(this.pages[id]);
		}
		else {
			this.container.appendChild(this.pages[id]);
		}
		
		if (!!this.totalSize && ((this.totalSize != 0)) && ((((this.totalSize == this.pageSize)) || ((Math.floor(this.totalSize / this.pageSize) <= n_page))))) {
			this.afterNext();
		}
		else if (n_page < 0) {
			this.beforePrevious();
		}
		
	};
	
	KList.prototype.transition_page_show = function(element, tween, direction) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.page_show) {
			tween = this.data.tween.page_show;
		}
		if (!direction) {
			direction = 1;
		}
		if (!tween) {
			tween = {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 1000,
				from : {
					alpha : 0
				},
				go : {
					alpha : 1
				},
				onStart : function(w, t) {
					KCSS.setStyle({
						display : 'inline'
					}, [
						element
					]);
				},
				applyToElement : true
			};
		}
		
		KEffects.addEffect(element, tween);
	};
	
	KList.prototype.transition_page_hide = function(element, tween, direction) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.page_hide) {
			tween = this.data.tween.page_hide;
		}
		if (!direction) {
			direction = 1;
		}
		if (!tween) {
			tween = {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 1000,
				from : {
					alpha : 1
				},
				go : {
					alpha : 0
				},
				onStart : function(w, t) {
					KCSS.setStyle({
						display : 'none'
					}, [
						element
					]);
				},
				applyToElement : true
			};
		}
		KEffects.addEffect(element, tween);
	};
	
	KList.prototype.pageVisible = function(n_page) {
		if (this.header.childNodes.length > 1) {
			this.header.childNodes[1].childNodes[0].value = n_page + 1;
			this.footer.childNodes[1].childNodes[0].value = n_page + 1;
			
			this.header.childNodes[1].childNodes[2].innerHTML = Math.ceil(this.totalSize / this.pageSize);
			this.footer.childNodes[1].childNodes[2].innerHTML = Math.ceil(this.totalSize / this.pageSize);
		}
		
		if (!!this.totalSize && (this.totalSize != 0) && ((this.totalSize == this.pageSize) || (Math.floor(this.totalSize / this.pageSize) <= n_page))) {
			this.afterNext();
		}
		else if (n_page < 0) {
			this.beforePrevious();
		}
		
		if (this.lastpage >= 0) {
			this.transition_page_hide(this.pages['page' + this.lastpage], null, this.lastpage > this.page ? 1 : -1);
			this.transition_page_show(this.pages['page' + n_page], null, this.lastpage > this.page ? 1 : -1);
		}
		else {
			KCSS.setStyle({
				display : 'inline'
			}, [
				this.pages['page' + n_page]
			]);
		}
		this.lastpage = n_page;
		
		if (this.autoRotate != 0) {
			this.autoRotateTimer = KSystem.addTimer('KList.rotate(\'' + this.id + '\')', this.autoRotate);
		}
		
	};
	
	KList.prototype.appendChild = function(element, childDiv) {
		if (!!childDiv) {
			KWidget.prototype.appendChild.call(this, element, childDiv);
			return;
		}

		var n_page = 0;
		if (this.pageSize != 0 && !this.children_retrieved && !this.feed_retrieved) {
			n_page = Math.floor(this.nChildren / this.pageSize);
		}
		else {
			n_page = this.page;
		}
		this.addPage(n_page);
		
		var appended = KWidget.prototype.appendChild.call(this, element, this.pages['page' + n_page]);
		if (!!appended) {
			element.parentPage = n_page;
		}
	};
	
	KList.prototype.addPagination = function(parent, totalSize) {
		var prevButton = window.document.createElement('button');
		{
			prevButton.setAttribute("type", "button");
			prevButton.className = ' KListPaginationPrev ' + (this.className != 'KList' ? this.className + 'PaginationPrev ' : '');
			prevButton.appendChild(window.document.createTextNode('<'));
			KDOM.addEventListener(prevButton, 'click', KList.dispatchPrevious);
		}
		parent.appendChild(prevButton);
		
		var pagination = window.document.createElement('div');
		{
			pagination.className = ' KListPaginationPages ' + (this.className != 'KList' ? this.className + 'PaginationPages ' : '');
			var paginationNrPage = window.document.createElement('input');
			{
				paginationNrPage.setAttribute("type", "text");
				paginationNrPage.value = (this.page + 1);
				KDOM.addEventListener(paginationNrPage, "focus", function(event) {
					KDOM.getEventTarget(event).select();
				});
				KDOM.addEventListener(paginationNrPage, "keypress", KList.dispatchPage);
			}
			pagination.appendChild(paginationNrPage);
			
			pagination.appendChild(window.document.createTextNode(' / '));
			
			var paginationTotalPages = window.document.createElement('span');
			paginationTotalPages.appendChild(window.document.createTextNode(Math.ceil(this.totalSize / this.pageSize)));
			pagination.appendChild(paginationTotalPages);
		}
		parent.appendChild(pagination);
		
		var nextButton = window.document.createElement('button');
		{
			nextButton.setAttribute("type", "button");
			nextButton.className = ' KListPaginationNext ' + (this.className != 'KList' ? this.className + 'PaginationNext ' : '');
			nextButton.appendChild(window.document.createTextNode('>'));
			KDOM.addEventListener(nextButton, 'click', KList.dispatchNext);
		}
		parent.appendChild(nextButton);
		
		var pageSizeSelection = new KCombo(this, '', this.id + 'PageSize');
		{
			pageSizeSelection.addCSSClass('KListPageSize ' + (this.className != 'KList' ? this.className + 'PageSize' : ''));
			pageSizeSelection.addEventListener('change', KList.dispatchPageSize);
			for ( var index = 1; index != 6; index++) {
				pageSizeSelection.addOption(this.pageSize * index, this.pageSize * index);
			}
		}
		parent.appendChild(pageSizeSelection.panel);
		pageSizeSelection.go();
	};
	
	KList.prototype.gotoPage = function(n_page) {
		
		if (this.page == n_page) {
			return;
		}
		else if (!!this.totalSize && (this.totalSize != 0) && (Math.floor(this.totalSize / this.pageSize) < n_page)) {
			this.afterNext();
			return;
		}
		else if (n_page < 0) {
			this.beforePrevious();
			return;
		}
		
		this.children_retrieved = false;
		this.feed_retrieved = false;
		
		this.page = n_page;
		if (!!this.pages['page' + n_page]) {
			this.pageVisible(this.page);
		}
		else if (!this.noMorePages) {
			this.onLoad(KCache.restore(this.ref + '/data'));
		}
		else {
			this.afterNext();
		}
	};
	
	KList.prototype.onNextPage = function() {
	};
	
	KList.prototype.onPreviousPage = function() {
	};
	
	KList.prototype.afterNext = function() {
	};
	
	KList.prototype.beforePrevious = function() {
	};
	
	KList.prototype.destroy = function() {
		if (!!this.autoRotateTimer) {
			KSystem.removeTimer(this.autoRotateTimer);
		}
		KWidget.prototype.destroy.call(this);
	};
	
	KList.dispatchNext = function(event, widget) {
		if (widget == null) {
			widget = KDOM.getEventWidget(event, 'KList');
		}
		if (!!widget.autoRotateTimer) {
			KSystem.removeTimer(widget.autoRotateTimer);
		}
		if (widget.noMorePages || widget.page + 1 == Math.ceil(widget.totalSize / widget.pageSize)) {
			widget.afterNext();
		}
		else {
			var dispatchPage = widget.page + 1;
			KBreadcrumb.dispatchEvent(widget.id, {
				action : '/page/' + dispatchPage
			});
		}
	};
	
	KList.dispatchPrevious = function(event, widget) {
		if (widget == null) {
			widget = KDOM.getEventWidget(event);
		}
		if (!!widget.autoRotateTimer) {
			KSystem.removeTimer(widget.autoRotateTimer);
		}
		if (widget.page == 0) {
			try {
				widget.beforePrevious();
				return;
			}
			catch (e) {
				return;
			}
		}
		var dispatchPage = widget.page - 1;
		KBreadcrumb.dispatchEvent(widget.id, {
			action : '/page/' + dispatchPage
		});
	};
	
	KList.dispatchLast = function(event, widget) {
		if (widget == null) {
			widget = KDOM.getEventWidget(event, 'KList');
		}
		if (!!widget.autoRotateTimer) {
			KSystem.removeTimer(widget.autoRotateTimer);
		}

		var n_pages = Math.ceil(widget.totalSize / widget.pageSize);
		KBreadcrumb.dispatchEvent(widget.id, {
			action : '/page/' + (n_pages - 1)
		});
	};
	
	KList.dispatchFirst = function(event, widget) {
		if (widget == null) {
			widget = KDOM.getEventWidget(event);
		}
		if (!!widget.autoRotateTimer) {
			KSystem.removeTimer(widget.autoRotateTimer);
		}

		KBreadcrumb.dispatchEvent(widget.id, {
			action : '/page/0'
		});
	};

	KList.dispatchPageSize = function(event, widget) {
		if (widget == null) {
			widget = KDOM.getEventWidget(event);
		}
		if (!!widget.autoRotateTimer) {
			KSystem.removeTimer(widget.autoRotateTimer);
		}
		var select = KDOM.getEventTarget(event);
		KBreadcrumb.dispatchEvent(widget.id, {
			action : '/per-page/' + select.options[select.selectedIndex].value
		});
	};
	
	KList.dispatchPage = function(event) {
		var keyCode = event.keyCode;
		if (!keyCode || (keyCode == 13)) {
			var widget = KDOM.getEventWidget(event, 'KList');
			if (!!widget.autoRotateTimer) {
				KSystem.removeTimer(widget.autoRotateTimer);
			}
			var page = (!!KDOM.getEventTarget(event).value ? KDOM.getEventTarget(event).value : KDOM.getEventTarget(event).innerHTML);
			widget.gotoPage((parseInt(page) - 1));
		}
	};
	
	KList.rotate = function(id) {
		var widget = Kinky.getWidget(id);
		widget.gotoPage(widget.page + 1);
	};
	
	KList.actions = function(widget, action) {
		var baseAction = '/' + action.split('/')[1];
		
		switch (baseAction) {
			case '/per-page': {
				var pageSize = parseInt(action.split('/')[2]);
				widget.page = 0;
				widget.pageSize = pageSize;
				widget.refresh();
				break;
			}
			case '/page': {
				var n_page = parseInt(action.split('/')[2]);
				widget.gotoPage(n_page);
				break;
			}
		}
		return;
	};
	
	KList.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div',
			header : 'div',
			footer : 'div'
		},
		html5 : {
			panel : 'section',
			content : 'div',
			background : 'div',
			header : 'header',
			footer : 'footer'
		}
	};
	
	KSystem.included('KList');
});