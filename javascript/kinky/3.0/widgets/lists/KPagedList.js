function KPagedList(parent) {
	if (parent != null) {
		KList.call(this, parent);
	}
}

KSystem.include([
	'KList'
], function() {
	KPagedList.prototype = new KList();
	KPagedList.prototype.constructor = KPagedList;
	
	KPagedList.prototype.pageVisible = function(n_page) {
		if ((this.header.childNodes.length == 0) && (this.pageSize != 0)) {
			this.addPagination(this.header, this.totalSize);
			this.addPagination(this.footer, this.totalSize);
		}
		
		if ((this.header.childNodes.length != 0) && (this.pageSize != 0)) {
			this.header.childNodes[0].value = '' + (n_page + 1);
		}
		if ((this.footer.childNodes.length != 0) && (this.pageSize != 0)) {
			this.footer.childNodes[0].value = '' + (n_page + 1);
		}

		if (this.lastpage >= 0) {
			this.transition_page_hide(this.pages['page' + this.lastpage], null, this.lastpage > this.page ? 1 : -1);
			this.transition_page_show(this.pages['page' + n_page], null, this.lastpage > this.page ? 1 : -1);
		}
		else {
			if (!!this.pages['page' + n_page]) {
				this.transition_page_show(this.pages['page' + n_page], null, 1);
			}
		}
		this.lastpage = n_page;
		
		if (this.autoRotate != 0) {
			this.autoRotateTimer = KSystem.addTimer('KList.rotate(\'' + this.id + '\')', this.autoRotate);
		}
	};
	
	KPagedList.prototype.addPagination = function(parent, totalSize) {
		if (!!totalSize) {
			var n_pages = Math.ceil(this.totalSize / this.pageSize);

			var pageNr = window.document.createElement('input');
			{
				pageNr.className = ' KPagedListPages ' + (this.className != 'KPagedList' ? this.className + 'ListPages ' : '');
				pageNr.setAttribute('type', 'text');
				KDOM.addEventListener(pageNr, 'focus', function(event) {
					KDOM.getEventTarget(event).select();					
				});
				KDOM.addEventListener(pageNr, 'keyup', KPagedList.dispatchPage);
			}
			parent.appendChild(pageNr);

			var pagesTotal = window.document.createElement('span');
			{
				pagesTotal.className = ' KPagedListPages ' + (this.className != 'KPagedList' ? this.className + 'ListPages ' : '');
				pagesTotal.appendChild(window.document.createTextNode(' / ' + n_pages));
			}
			parent.appendChild(pagesTotal);

			var prev = window.document.createElement('a');
			{
				prev.className = ' KPagedListPagePrev ' + (this.className != 'KPagedList' ? this.className + 'ListPagePrev ' : '');
				prev.href = 'javascript:void(0)';
				prev.innerHTML = gettext('$KPAGEDLIST_PREV_BUTTON');
				KDOM.addEventListener(prev, 'click', KList.dispatchPrevious);
			}
			parent.appendChild(prev);

			var first = window.document.createElement('a');
			{
				first.className = ' KPagedListPagePrev ' + (this.className != 'KPagedList' ? this.className + 'ListPagePrev ' : '');
				first.href = 'javascript:void(0)';
				first.innerHTML = gettext('$KPAGEDLIST_FIRST_BUTTON');
				KDOM.addEventListener(first, 'click', KList.dispatchFirst);
			}
			parent.appendChild(first);

			var last = window.document.createElement('a');
			{
				last.className = ' KPagedListPageNext ' + (this.className != 'KPagedList' ? this.className + 'ListPageNext ' : '');
				last.href = 'javascript:void(0)';
				last.innerHTML = gettext('$KPAGEDLIST_LAST_BUTTON');
				KDOM.addEventListener(last, 'click', KList.dispatchLast);
			}
			parent.appendChild(last);

			var next = window.document.createElement('a');
			{
				next.className = ' KPagedListPageNext ' + (this.className != 'KPagedList' ? this.className + 'ListPageNext ' : '');
				next.href = 'javascript:void(0)';
				next.innerHTML = gettext('$KPAGEDLIST_NEXT_BUTTON');
				KDOM.addEventListener(next, 'click', KList.dispatchNext);
			}
			parent.appendChild(next);

		}
	};

	KPagedList.dispatchPage = function(event) {
		var keyCode = event.keyCode;
		if (!keyCode || (keyCode == 13)) {
			var pageNr = KDOM.getEventTarget(event);
			var widget = KDOM.getEventWidget(event, 'KPagedList');
			var page = (!!pageNr.value ? pageNr.value : pageNr.innerHTML);

			var n_pages = Math.ceil(widget.totalSize / widget.pageSize);
			var n_page = parseInt(page);
			if (!isNaN(n_page)) {
				n_page--;
				if (n_page >= 0 && n_page < n_pages) {
					widget.gotoPage(n_page);
					pageNr.blur();
					return;
				}
			}

			pageNr.select();
		}
		else if (keyCode == 27) {
			var pageNr = KDOM.getEventTarget(event);
			var widget = KDOM.getEventWidget(event, 'KPagedList');
			pageNr.value = '' + (widget.lastpage + 1);
			pageNr.blur();
		}
	};
		
	KSystem.included('KPagedList');
});