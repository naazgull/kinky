function KScrolledList(parent) {
	if (parent) {
		KList.call(this, parent);
		this.nextButton = null;
	}
}

KSystem.include([
	'KList'
], function() {
	KScrolledList.prototype = new KList();
	KScrolledList.prototype.constructor = KScrolledList;
	
	KScrolledList.prototype.afterNext = function() {
		this.nextButton.style.display = 'none';
	};
	
	KScrolledList.prototype.pageVisible = function(n_page) {
		if ((this.footer.childNodes.length == 0) && (this.pageSize != 0)) {
			this.addPagination(this.footer, this.totalSize);
		}
		
		KCSS.setStyle({
			display : 'inline'
		}, [
			this.pages['page' + n_page]
		]);
		
		if (this.autoRotate != 0) {
			this.autoRotateTimer = KSystem.addTimer('KList.rotate(\'' + this.id + '\')', this.autoRotate);
		}
		
		if (!!this.totalSize && (this.totalSize != 0) && ((this.totalSize == this.pageSize) || (Math.floor(this.totalSize / this.pageSize) <= n_page))) {
			this.afterNext();
		}
		else if (n_page < 0) {
			this.beforePrevious();
		}
	};
	
	KScrolledList.prototype.addPagination = function(parent, totalSize) {
		if (parent === this.header) {
			return;
		}
		this.nextButton = window.document.createElement('button');
		{
			this.nextButton.setAttribute("type", "button");
			this.nextButton.className = ' KListPaginationNext ' + (this.className != 'KList' ? this.className + 'PaginationNext ' : '');
			this.nextButton.appendChild(window.document.createTextNode('ver mais'));
			KDOM.addEventListener(this.nextButton, 'click', KList.dispatchNext);
		}
		parent.appendChild(this.nextButton);
	};
	
	KSystem.included('KScrolledList');
});