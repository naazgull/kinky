function KOrderedList(parent, params) {
	if (parent != null) {
		KList.call(this, parent, params);
		this.onCompare = KOrderedList.defaulCompare;
		this.orderedChildren = {};
	}
}

KSystem.include( [ 'KList' ], function() {

	KOrderedList.prototype = new KList();
	KOrderedList.prototype.constructor = KOrderedList;

	KOrderedList.prototype.hasPaginationBar = true;

	KOrderedList.prototype.appendChild = function(element, context, childDiv) {
		context = context || (this.query + this.nPage);
		if (!this.orderedChildren[context]) {
			this.orderedChildren[context] = [];
		}
		element.orderedIndex = this.orderedChildren[context].push(element) - 1;
		KList.prototype.appendChild.call(this, element, context, childDiv);
	};

	KOrderedList.prototype.order = function() {
		var context = this.query + this.nPage;
		if (this.orderedChildren[context]) {
			this.orderedChildren[context] = Quicksort.quicksort(this.orderedChildren[context], this.onCompare);

			for ( var index in this.orderedChildren[context]) {
				this.orderedChildren[context][index].orderedIndex = index;
				this.content.appendChild(this.orderedChildren[context][index].panel);
			}
		}
	};

	KOrderedList.prototype.draw = function() {
		KList.prototype.draw.call(this);
		this.order();
	};

	KOrderedList.prototype.removeAllChildren = function(context) {
		context = context || (this.query + this.nPage);
		KList.prototype.removeAllChildren.call(this, context);
		delete this.orderedChildren[context];
	};

	KOrderedList.prototype.removeChild = function(element, context, notDeleteIt) {
		context = context || (this.query + this.nPage);
		KList.prototype.removeChild.call(this, element, context, notDeleteIt);
		this.orderedChildren[context].splice(element.orderedIndex, 1);
	};

	KOrderedList.defaulCompare = function(widget1, widget2) {
		return parseInt(widget1.id.replace('widget', '')) - parseInt(widget2.id.replace('widget', ''));
	};

	KSystem.included('KOrderedList');
});