function KFeedback(parent) {
	if (parent != null) {
		KLayeredPanel.call(this, parent);
		this.isLeaf = false;
		this.social = window.document.createElement('div');
		{
			this.social.className = ' ' + this.className + 'SocialPanel KFeedbackSocialPanel ';
		}
		this.panel.insertBefore(this.social, this.content);
	}
}

KSystem.include([
	'KLayeredPanel',
	'KFeedbackPanel'
], function() {
	KFeedback.prototype = new KLayeredPanel();
	KFeedback.prototype.constructor = KFeedback;
	
	KFeedback.prototype.hasType = function(type) {
		for ( var t in this.config.types) {
			if (this.config.types[t] == type) {
				return true;
			}
		}
		return false;
	};
	
	KFeedback.prototype.retrieveFeed = function(config_data, config_request) {
		var retrieved = this.feed_retrieved;
		var elements = KCache.restore(this.ref + '/feed/' + this.page);
		this.data.feed = elements;
		this.feed_retrieved = true;
		this.children_retrieved = true;
		this.no_children = true;
		var feedback = (!!this.parent.data.links && !!this.parent.data.links.feedback && !!this.parent.data.links.feedback.href ? this.parent.data.links.feedback.href : this.parent.config.href);
		
		if (!elements && !retrieved && !!feedback) {
			var headers = null;
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + feedback + '/feedback', {}, headers, 'onFeed');
		}
		else {
			this.data.feed = {};
			this.draw();
		}
	};
	
	KFeedback.prototype.onFeed = function(data, request) {
		this.data.feed = data;
		KCache.commit(this.ref + '/feed', this.data.feed);
		this.draw();
	};
	
	KFeedback.prototype.draw = function() {
		var feedback = (!!this.parent.data.links && !!this.parent.data.links.feedback && !!this.parent.data.links.feedback.href ? this.parent.data.links.feedback.href : this.parent.config.href);
		var config = {
			hash : this.hash,
			href : this.parent.config.href,
			rel : "\/wrml-relations\/widgets\/{widget_id}",
			requestTypes : [
				"application\/json",
				"application\/vnd.kinky.KFeedbackPanel"
			],
			responseTypes : [
				"application\/json",
				"application\/vnd.kinky.KFeedbackPanel"
			],
			links : {
				self : {
					href : feedback,
					rel : "\/wrml-relations\/widgets\/{widget_id}"
				},
				feed : {
					href : feedback,
					rel : "\/wrml-relations\/widgets\/{widget_id}"
				}
			}
		};
		
		for ( var t in this.data.types) {
			var type = this.data.types[t];
			var result = this.data.feed[type];
			if (!!result) {
				var total = window.document.createElement('div');
				{
					total.className = ' ' + this.className + 'SocialPanel_' + type + ' KFeedbackSocialPanel_' + type + ' ';
					total.innerHTML = '<span class="KFeedbackSocialPanelCountLabel">' + result.size + '</span><span class="KFeedbackSocialPanelCountLabel">' + gettext(result.size == 1 ? '$KFEEDBACK_SOCIAL' + type.toUpperCase() + '_SINGULAR' : '$KFEEDBACK_SOCIAL' + type.toUpperCase()) + '</span>';
				}
				this.social.appendChild(total);
				
				var childConfig = KSystem.clone(config);
				childConfig.hash += '/' + type;
				childConfig.href += '/' + type;
				childConfig.links.self.href += '/' + type;
				childConfig.links.feed.href += '/' + type;
				childConfig.type = type;
				childConfig.http = this.config.http;
				
				var child = KSystem.construct(childConfig, this);
				this.addPanel(child, gettext('$KFEEDBACK_TITLE_' + type.toUpperCase()), this.nChildren == 0);
			}
		}
		
		KLayeredPanel.prototype.draw.call(this);
	};
	
	KSystem.included('KFeedback');
});