function KFeed(parent) {
	if (parent != null) {
		KScrolledList.call(this, parent);
		this.autoRefresh = false;
		this.silent = true;
	}
}

KSystem.include([
	'KScrolledList',
	'KFeedItem'
], function() {
	
	KFeed.prototype = new KScrolledList();
	KFeed.prototype.constructor = KFeed;
	
	KFeed.prototype.onLoad = function(data, request) {
		if (!!data.autoRotate && (data.autoRotate != '0')) {
			this.autoRotate = parseInt(data.autoRotate);
		}
		if (!!data.pageSize && (data.pageSize != '0')) {
			if (!this.activated()) {
				this.silence();
				this.pageSize = parseInt(data.pageSize);
				this.data = data;
				
				if (this.pageSize != 0) {
					KCache.commit(this.ref + '/data', KSystem.clone(data));
					this.addPagination(this.header);
					this.addPagination(this.footer);
				}
				else {
					KCache.commit(this.ref + '/data', this.data);
				}
			}
			
			if (!!data.links && !!data.links.feed) {
				var elements = KCache.restore(this.ref + '/data/' + this.page);
				if (!!elements) {
					this.onChildren(elements);
				}
				else {
					var headers = null;
					this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + KSystem.urlify(data.links.feed.href, {
						pageSize : this.pageSize,
						pageStartIndex : this.page * this.pageSize
					}), {}, headers, 'onChildren');
				}
			}
		}
		else {
			KWidget.prototype.onLoad.call(this, data, request);
		}
	};
	
	KFeed.prototype.onChildren = function(data, request) {
		if (!!request) {
			KCache.commit(this.ref + '/data/' + this.page, data);
		}
		this.addPage();
		for ( var e in data.elements) {
			var item = new KFeedItem(this, data.elements[e]);
			this.appendChild(item);
		}
		this.draw();
	};
	
	KSystem.included('KFeed');
});
