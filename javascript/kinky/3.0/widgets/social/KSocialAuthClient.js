function KSocialAuthClient(parent) {
	if (parent != null) {
		KShell.call(this, parent);
	}
}

KSystem.include([
	'KConnectionREST',
	'KShell',
	'Base64'
], function() {
	KSocialAuthClient.prototype = new KShell();
	KSocialAuthClient.prototype.constructor = KSocialAuthClient;
	
	KSocialAuthClient.prototype.load = function() {
		this.draw();
	};
	
	KSocialAuthClient.prototype.onLoad = function() {
		this.draw();
	};
	
	KSocialAuthClient.prototype.draw = function() {
		var params = new Object();
		
		var return_service = '';
		
		var query_string = location.search.substr(1).split("&");
		for ( var i = 0; i != query_string.length; ++i) {
			var index = query_string[i].indexOf('=');
			
			var key = query_string[i].substring(0, index);
			var value = query_string[i].substring(index + 1);
			
			if (key == 'return_service') {
				return_service = unescape(value);
				
				// http://stackoverflow.com/questions/4386691/facebook-error-error-validating-verification-code
				// "... you cannot use many special characters in the redirect_url ..."
				if (return_service.indexOf("/") == -1) {
					return_service = Base64.decode(return_service);
				}
			}
			else {
				params[key] = value;
			}
		}
		
		this.kinky.get(this, 'rest:' + return_service, params, 'onSuccess');
		
		KShell.prototype.draw.call(this);
	};
	
	KSocialAuthClient.prototype.onSuccess = function(data) {
		window.opener.KOAuth.setAccessToken(KOAuth.getAccessToken(), false);
		window.opener.Kinky.bunnyMan.loggedUser = Kinky.bunnyMan.loggedUser;
		
		window.opener.KBreadcrumb.dispatchURL({
			action : '/logged-in'
		});
		
		window.close();
	};
	
	KSocialAuthClient.prototype.onCreated = function(data) {
		window.opener.KBreadcrumb.dispatchURL({
			action : '/associated' + data.redirectURL
		});
		
		window.close();
	};
	
	KSocialAuthClient.prototype.onError = function(data) {
		var action = '/bad-request';
		
		if (data.message.indexOf('@') == 0) {
			action = data.message.substring(1);
		}
		
		window.opener.KBreadcrumb.dispatchURL({
			action : action
		});
		
		window.close();
	};
	
	KSocialAuthClient.prototype.onBadRequest = function(data) {
		var action = '/bad-request';
		
		if (data.message.indexOf('@') == 0) {
			action = data.message.substring(1);
		}
		
		window.opener.KBreadcrumb.dispatchURL({
			action : action
		});
		
		window.close();
	};
	
	KSocialAuthClient.prototype.onUnauthorized = function(data) {
		var action = '/unauthorized';
		
		if (data.message.indexOf('@') == 0) {
			action = data.message.substring(1);
		}
		
		window.opener.KBreadcrumb.dispatchURL({
			action : action
		});
		
		window.close();
	};
	
	KSocialAuthClient.prototype.onMethodNotAllowed = function(data) {
		var action = '/bad-request';
		
		if (data.message.indexOf('@') == 0) {
			action = data.message.substring(1);
		}
		
		window.opener.KBreadcrumb.dispatchURL({
			action : action
		});
		
		window.close();
	};
	
	KSystem.included('KSocialAuthClient');
});