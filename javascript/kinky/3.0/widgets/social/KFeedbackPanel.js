function KFeedbackPanel(parent) {
	if (parent != null) {
		KScrolledList.call(this, parent);
		this.isLeaf = true;
		this.add = window.document.createElement('div');
		{
			this.add.className = ' ' + this.className + 'AddPanel KFeedbackPanelAddPanel ';
		}
		this.content.appendChild(this.add);
		
	}
}

KSystem.include([
	'KScrolledList',
	'KFeedbackItem',
	'KTextArea'
], function() {
	KFeedbackPanel.prototype = new KScrolledList();
	KFeedbackPanel.prototype.constructor = KFeedbackPanel;
	
	KFeedbackPanel.prototype.load = function() {
		this.data = {
			template : {
				widget : {
					type : 'KFeedbackItem'
				}
			},
			links : this.config.links
		};
		this.retrieveFeed({});
	};
	
	KFeedbackPanel.prototype.onError = function(data, request) {
		dump(data);
	};
	
	KFeedbackPanel.prototype.onNoContent = function(data, request) {
		this.draw();
	};
	
	KFeedbackPanel.prototype.onCreated = KFeedbackPanel.prototype.onFeedback = function(data, request) {
		this.parent.refresh();
	};
	
	KFeedbackPanel.prototype.sendFeedback = function() {
		var headers = null;
		this.kinky.post(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + this.config.links.self.href, {
			body : this.feedback.getValue()
		}, headers, 'onFeedback');
	};
	
	KFeedbackPanel.prototype.onFeed = function(data, request) {
		var config = {
			hash : this.hash,
			rel : "\/wrml-relations\/widgets\/{widget_id}",
			requestTypes : [
				"application\/json",
				"application\/vnd.kinky.KFeedbackItem"
			],
			responseTypes : [
				"application\/json",
				"application\/vnd.kinky.KFeedbackItem"
			],
			links : {
				self : {
					href : this.config.href,
					rel : "\/wrml-relations\/widgets\/{widget_id}"
				}
			}
		};
		
		this.data.feed = {};
		
		this.totalSize = data.size;
		
		for ( var l in data.elements) {
			var childConfig = KSystem.clone(config);
			childConfig.hash += '/' + l;
			childConfig.links.self.href += '/' + l;
			childConfig.type = this.config.type;
			
			childConfig.data = data.elements[l];
			this.data.feed[childConfig.hash] = childConfig;
		}
		
		this.addPage();
		
		if (this.totalSize != 0) {
			this.data.feed.size = this.totalSize;
		}
		
		if (!!request) {
			KCache.commit(this.ref + '/feed/' + this.page, this.data.feed);
		}
		
		this.draw();
	};
	
	KFeedbackPanel.prototype.draw = function() {
		this.feedback = new KTextArea(this, gettext('$KFEEDBACK_' + this.config.type.toUpperCase() + '_TEXTAREA'), 'submit');
		this.appendChild(this.feedback, this.add);
		
		var button = new KButton(this, gettext('$KFEEDBACK_' + this.config.type.toUpperCase() + '_BUTTON'), 'send', function(event) {
			KDOM.getEventWidget(event).parent.sendFeedback();
		});
		this.appendChild(button, this.add);
		
		if (!!this.data.feed) {
			KScrolledList.prototype.draw.call(this);
		}
		else {
			this.feedback.go();
			button.go();
			this.activate();
		}
	};
	
	KSystem.included('KFeedbackPanel');
});