function KFeedbackItem(parent, config) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.isLeaf = true;
		this.config = config;
		this.data = {};
		
		this.icon = window.document.createElement('img');
		{
			this.addCSSClass('KFeedbackItemIcon', this.icon);
		}
		
		this.user = window.document.createElement('a');
		{
			this.addCSSClass('KFeedbackItemUser', this.user);
		}
		
		this.picture = window.document.createElement('img');
		{
			this.addCSSClass('KFeedbackItemPicture', this.picture);
		}
		
		this.caption = window.document.createElement('div');
		{
			this.addCSSClass('KFeedbackItemCaption', this.caption);
		}
		
		this.lead = window.document.createElement('div');
		{
			this.addCSSClass('KFeedbackItemLead', this.lead);
		}
		
		this.body = window.document.createElement('div');
		{
			this.addCSSClass('KFeedbackItemBody', this.body);
		}
		
		this.link = window.document.createElement('a');
		{
			this.link.href = 'javascript:void(0)';
			this.addCSSClass('KFeedbackItemLink', this.link);
		}
		
		this.report = window.document.createElement('a');
		{
			this.report.href = 'javascript:void(0)';
			this.report.appendChild(window.document.createTextNode(gettext('$KFEEDBACK_ITEM_REPORT_LINK')));
			this.addCSSClass('KFeedbackItemReportLink', this.report);
		}
	}
}

KSystem.include([
	'KPanel'
], function() {
	KFeedbackItem.prototype = new KPanel();
	KFeedbackItem.prototype.constructor = KFeedbackItem;
	
	KFeedbackItem.prototype.setTitle = function() {
		if (!!this.config.data.title) {
			this.link.appendChild(window.document.createTextNode(this.config.data.title));
			this.addCSSClass('KFeedbackItemLink' + this.config.type, this.link);
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setCaption = function() {
		if (!!this.config.data.caption) {
			this.caption.appendChild(window.document.createTextNode(this.config.data.caption));
			this.addCSSClass('KFeedbackItemCaption' + this.config.type, this.caption);
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setLead = function() {
		if (!!this.config.data.lead) {
			this.lead.appendChild(window.document.createTextNode(this.config.data.lead));
			this.addCSSClass('KFeedbackItemLead' + this.config.type, this.lead);
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setBody = function() {
		if (!!this.config.data.body) {
			this.body.appendChild(window.document.createTextNode(this.config.data.body));
			this.addCSSClass('KFeedbackItemBody' + this.config.type, this.body);
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setLink = function() {
		if (!!this.config.data.link) {
			this.link.href = this.config.data.link;
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setReportLink = function() {
		if (this.config.data.state == 'active') {
			KDOM.addEventListener(this.report, 'click', KFeedbackItem.sendReport);
		}
		else {
			this.report.innerHTML = gettext('$KFEEDBACK_ITEM_REPORTED_LINK');
		}
		this.container.appendChild(this.report);
	};
	
	KFeedbackItem.prototype.reportFeedback = function() {
		var headers = null;
		this.kinky.post(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + this.config.data.href + '/reports', {}, headers, 'onReport');
	};
	
	KFeedbackItem.prototype.onCreated = KFeedbackItem.prototype.onReport = function(data, request) {
		KDOM.removeEventListener(this.report, 'click', KFeedbackItem.sendReport);
		this.report.innerHTML = '';
		this.report.appendChild(window.document.createTextNode(gettext('$KFEEDBACK_ITEM_REPORTED_LINK')));
	};
	
	KFeedbackItem.prototype.setPicture = function() {
		if (!!this.config.data.picture) {
			this.picture.src = this.config.data.picture;
			this.addCSSClass('KFeedbackItemPicture' + this.config.type, this.picture);
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setOwner = function() {
		if (!!this.config.data.owner) {
			if (!!this.config.data.owner.picture) {
				this.icon.src = this.config.data.owner.picture;
				this.addCSSClass('KFeedbackItemIcon' + this.config.type, this.icon);
				this.user.appendChild(this.icon);
			}
			else if (!!this.config.data.owner.avatar) {
				this.icon.src = this.config.data.owner.avatar;
				this.addCSSClass('KFeedbackItemIcon' + this.config.type, this.icon);
				this.user.appendChild(this.icon);
			}
			if (!!this.config.data.owner.name) {
				var pattern = this.processPattern(gettext('$KFEEDBACK_ITEM_USER_PATTERN'));
				this.user.appendChild(window.document.createTextNode(pattern));
			}
			
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.go = function() {
		this.gone = true;
		this.config.type = this.config.type.substr(0, 1).toUpperCase() + this.config.type.substr(1);
		this.draw();
	};
	
	KFeedbackItem.prototype.draw = function() {
		var hasLinkTitle = false;
		
		if (this.setOwner()) {
			this.container.appendChild(this.user);
		}
		
		if (this.setTitle()) {
			this.container.appendChild(this.link);
			hasLinkTitle = true;
		}
		
		if (this.setLink()) {
			if (!hasLinkTitle) {
				this.container.appendChild(this.link);
				this.link.appendChild(window.document.createTextNode(this.config.data.link));
			}
		}
		
		if (this.setPicture()) {
			this.container.appendChild(this.picture);
		}
		
		if (this.setCaption()) {
			this.container.appendChild(this.caption);
		}
		
		if (this.setLead()) {
			this.container.appendChild(this.lead);
		}
		
		if (this.setBody()) {
			this.container.appendChild(this.body);
		}
		
		this.setReportLink();
		
		this.activate();
	};
	
	KFeedbackItem.prototype.processPattern = function(pattern) {
		var str = pattern.replace('\$USER_NAME', this.config.data.owner.name);
		str = str.replace('\$SUBMISSION_DATE', this.config.data.date);
		str = str.replace('\$SUBMISSION_TIME', this.config.data.time);
		str = str.replace('\$SUBMISSION_TIMESTAMP', this.config.data.timestamp);
		return str;
	};
	
	KFeedbackItem.sendReport = function(event) {
		KDOM.getEventWidget(event).reportFeedback();
	};
	KSystem.included('KFeedbackItem');
	
});