function KFeedItem(parent, config) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.config = config;
		this.data = {};
		
		this.icon = window.document.createElement('img');
		{
			this.addCSSClass('KFeedItemIcon', this.icon);
		}
		
		this.user = window.document.createElement('a');
		{
			this.addCSSClass('KFeedItemUser', this.user);
		}
		
		this.picture = window.document.createElement('img');
		{
			this.addCSSClass('KFeedItemPicture', this.picture);
		}
		
		this.caption = window.document.createElement('div');
		{
			this.addCSSClass('KFeedItemCaption', this.caption);
		}
		
		this.lead = window.document.createElement('div');
		{
			this.addCSSClass('KFeedItemLead', this.lead);
		}
		
		this.body = window.document.createElement('div');
		{
			this.addCSSClass('KFeedItemBody', this.body);
		}
		
		this.link = window.document.createElement('a');
		{
			this.link.href = 'javascript:void(0)';
			this.addCSSClass('KFeedItemLink', this.link);
		}
	}
}

KSystem.include([
	'KPanel'
], function() {
	KFeedItem.prototype = new KPanel();
	KFeedItem.prototype.constructor = KFeedItem;
	
	KFeedItem.prototype.setTitle = function() {
		if (!!this.config.title) {
			this.link.appendChild(window.document.createTextNode(this.config.title));
			this.addCSSClass('KFeedItemLink' + this.config.type, this.link);
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setCaption = function() {
		if (!!this.config.caption) {
			this.caption.appendChild(window.document.createTextNode(this.config.caption));
			this.addCSSClass('KFeedItemCaption' + this.config.type, this.caption);
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setLead = function() {
		if (!!this.config.lead) {
			this.lead.appendChild(window.document.createTextNode(this.config.lead));
			this.addCSSClass('KFeedItemLead' + this.config.type, this.lead);
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setBody = function() {
		if (!!this.config.body) {
			this.body.appendChild(window.document.createTextNode(this.config.body));
			this.addCSSClass('KFeedItemBody' + this.config.type, this.body);
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setLink = function() {
		if (!!this.config.link) {
			this.link.href = this.config.link;
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setPicture = function() {
		if (!!this.config.picture) {
			this.picture.src = this.config.picture;
			this.addCSSClass('KFeedItemPicture' + this.config.type, this.picture);
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setOwner = function() {
		if (!!this.config.owner) {
			if (!!this.config.owner.picture) {
				this.icon.src = this.config.owner.picture;
				this.addCSSClass('KFeedItemIcon' + this.config.type, this.icon);
				this.user.appendChild(this.icon);
			}
			else if (!!this.config.owner.avatar) {
				this.icon.src = this.config.owner.avatar;
				this.addCSSClass('KFeedItemIcon' + this.config.type, this.icon);
				this.user.appendChild(this.icon);
			}
			if (!!this.config.name) {
				this.user.appendChild(window.document.createTextNode(this.config.name));
			}
			if (!!this.config.href) {
				this.user.href = this.config.href;
			}
			else {
				this.user.src = 'javascrip:void(0)';
			}
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.go = function() {
		this.gone = true;
		this.config.type = this.config.type.substr(0, 1).toUpperCase() + this.config.type.substr(1);
		this.draw();
	};
	
	KFeedItem.prototype.draw = function() {
		var hasLinkTitle = false;
		
		if (this.setOwner()) {
			this.container.appendChild(this.user);
		}
		
		if (this.setTitle()) {
			this.container.appendChild(this.link);
			hasLinkTitle = true;
		}
		
		if (this.setLink()) {
			if (!hasLinkTitle) {
				this.container.appendChild(this.link);
				this.link.appendChild(window.document.createTextNode(this.config.link));
			}
		}
		
		if (this.setPicture()) {
			this.container.appendChild(this.picture);
		}
		
		if (this.setCaption()) {
			this.container.appendChild(this.caption);
		}
		
		if (this.setLead()) {
			this.container.appendChild(this.lead);
		}
		
		if (this.setBody()) {
			this.container.appendChild(this.body);
		}
		
		this.activate();
	};
	
	KSystem.included('KFeedItem');
	
});