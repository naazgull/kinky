KWidgetNotFoundException.prototype = new KException();
KWidgetNotFoundException.prototype.constructor = KWidgetNotFoundException;

function KWidgetNotFoundException(invoker, message) {
	KException.call(invoker, message);
}
