function KFloatable(target, params) {
	if (target != null) {
		KPanel.call(this, target.parent);
		this.target = target;
		target.parent = this;
		this.appendChild(target);
		this.data = new Object();
		this.data.params = params;
	}
}

KSystem.include([
	'KPanel'
], function() {
	KFloatable.prototype = new KPanel();
	KFloatable.prototype.constructor = KFloatable;
	
	KFloatable.prototype.blur = function(dispatcher) {
		if (!!dispatcher) {
			return;
		}
		if (this.data.params.modal) {
			this.getShell().hideOverlay();
			if (Kinky.MODAL_WIDGET.length == 0) {
				window.document.body.style.height = null;
				window.document.body.style.overflow = 'visible';
			}
		}
		KWidget.prototype.blur.call(this);
		this.unload();
	};
	
	KFloatable.prototype.visible = function() {	
		if (!!this.display) {
			return;
		}

		this.setStyle({
			position : 'absolute',
			top : this.top + 'px',
			width : this.width + 'px',
			marginBottom : '2em',
			height : (this.height != -1 ? this.height + 'px' : (this.target.getHeight() != 0 ? this.target.getHeight() + 'px' : 'auto'))
		});
		if (this.right !== undefined) {
			this.setStyle({
				right : this.right + 'px'
			});
		}
		else {
			this.setStyle({
				left : this.left + 'px'
			});			
		}
		if (this.data.params.expandable) {
			this.parentDiv.appendChild(this.panel);
		}
		
		KPanel.prototype.visible.call(this);
		if (this.data.params.expandable) {
			var h = (this.height != -1 ? this.height : (this.target.getHeight() != 0 ? this.target.getHeight() : 100));
			if (h + 20 < KDOM.getBrowserHeight()) {
				h = 0;
			}
			this.margin.style.top = h + 'px';
		}
		this.focus();
	};
	
	KFloatable.prototype.transition_show = function(tween) {
		if (this.data.params.transition_wait_target) {
			return;
		}

		if (tween == -1) {
			tween = null;
		}
		if (!tween && this.data && this.data.params.tween && this.data.params.tween.show) {
			tween = this.data.params.tween.show;
		}
		if (!tween) {
			tween = {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				},
				onStart : function(w, t) {
					if (!!w.target && !!w.target.onOpen) {
						w.target.onOpen();
					}
				}
			};
		}
		else if ((tween instanceof Array && !tween[0].onStart) || !tween.onStart) {
			(tween instanceof Array ? tween[0] : tween).onStart = function(w, t) {
				if (!!w.target && !!w.target.onOpen) {
					w.target.onOpen();
				}
			};
		}
		KPanel.prototype.transition_show.call(this, tween);
	};
	
	KFloatable.prototype.transition_hide = function(tween) {
		if (tween == -1) {
			tween = null;
		}
		if (!tween && this.data && this.data.params.tween && this.data.params.tween.hide) {
			tween = this.data.params.tween.hide;
		}
		if (!tween) {
			tween = {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				go : {
					alpha : 0
				},
				from : {
					alpha : 1
				},
				onComplete : function(w, t) {
					if (w.data.params.expandable) {
						w.parentDiv.parentNode.removeChild(w.parentDiv);
					}
					w.invisible();
					w.destroy();				
				}
			};
		}
		else if ((tween instanceof Array && !tween[0].onComplete) || !tween.onComplete) {
			(tween instanceof Array ? tween[0] : tween).onComplete = function(w, t) {
				if (w.data.params.expandable) {
					w.parentDiv.parentNode.removeChild(w.parentDiv);
				}					
				w.invisible();
				w.destroy();				
			};
		}
		KPanel.prototype.transition_hide.call(this, tween);
	};
	
	KFloatable.prototype.draw = function() {
		if (!!this.data.params) {
			if (!!this.data.params.className) {
				this.addCSSClass(this.data.params.className);
			}
			
			if (!!this.data.params.buttons) {
				var buttons = this.data.params.buttons.split(/,/);
				for ( var i = buttons.length - 1; i != -1; i--) {
					var button = window.document.createElement('button');
					{
						button.className = 'KFloatableButton' + i;
						KDOM.addEventListener(button, 'click', KFloatable[buttons[i]]);
					}
					this.container.appendChild(button);
				}
			}
		}
		
		if (!!this.data.params.width) {
			this.width = this.data.params.width;
		}
		else if (!!this.data.params.scale && !!this.data.params.scale.width) {
			this.width = Math.round(KDOM.getBrowserWidth() * this.data.params.scale.width);
		}
		else {
			this.data.params.width = this.width = Math.round(KDOM.getBrowserWidth() * 0.9);
		}
		if (!!this.data.params.height) {
			this.height = this.data.params.height;
		}
		else if (!!this.data.params.scale && !!this.data.params.scale.height) {
			this.height = Math.round(KDOM.getBrowserHeight() * this.data.params.scale.height);
		}
		else {
			this.data.params.height = this.height = -1;
		}
		
		if (!!this.data.params.top) {
			this.top = this.data.params.top;
		}
		else {
			this.data.params.top = this.top = Math.max(20, Math.round((KDOM.getBrowserHeight() - this.height) / 2));
		}
		
		if (this.data.params.left !== undefined) {
			this.left = this.data.params.left;
		}
		else if (this.data.params.right !== undefined) {
			this.right = this.data.params.right;
		}
		else {
			this.data.params.left = this.left = Math.round((KDOM.getBrowserWidth() - this.width) / 2);
		}
		
		KPanel.prototype.draw.call(this);
		Kinky.setModal(this);
	};
	
	KFloatable.prototype.reposition = function(top) {
		if (!!top) {
			this.data.params.top = this.top = top;
		}
		else {
			top = this.data.params.top = this.top = Math.max(20, Math.round((KDOM.getBrowserHeight() - this.height) / 2));
		}
		this.setStyle({
			top : top + 'px'
		});
		return top;
	};

	KFloatable.prototype.resize = function(height) {
		this.data.params.height = this.height = height;
		this.setStyle({
			height : height + 'px'
		});
		this.reposition();
		this.transition_show();
		if (this.data.params.expandable) {
			if (height < KDOM.getBrowserHeight()) {
				this.margin.style.top = '0px';			
			}
			else {
				this.margin.style.top = height + 'px';
			}
		}
		return height;
	};

	KFloatable.prototype.go = function() {
	};

	KFloatable.prototype.showMe = function() {
		KPanel.prototype.go.call(this);
	};
	
	KFloatable.prototype.closeMe = function() {
		Kinky.unsetModal(this);
	};
	
	KFloatable.close = function(event) {
		var widget = KDOM.getEventWidget(event);
		widget.closeMe();
	};
	
	KFloatable.make = function(target, params) {
		var floatable = new KFloatable(target, params);
		floatable.parent.appendChild(floatable, KWidget.ROOT_DIV);

		if (floatable.data.params.expandable) {
			floatable.parentDiv = window.document.createElement('div');
			floatable.parentDiv.className = ' KFloatableScoller ';
			KCSS.setStyle({
				width : '100%',
				height : '100%',
				position : 'fixed',
				top : '0',
				left : '0',
				zIndex : '901',
				overflowX : 'hidden',
				overflowY : 'auto'
			}, [ floatable.parentDiv ]);
			floatable.parent.childDiv(KWidget.ROOT_DIV).appendChild(floatable.parentDiv);

			floatable.margin = window.document.createElement('p');
			floatable.margin.className = ' KFloatableScollerMargin ';
			floatable.margin.style.position = 'absolute';
			floatable.margin.style.left = '0';
			floatable.margin.style.width = '100%';
			floatable.parentDiv.appendChild(floatable.margin);	
		}
		return floatable;
	};
	
	KFloatable.show = function(floatable) {
		floatable.showMe();
		if (floatable.data.params.modal) {
			floatable.getShell().showOverlay();
			window.document.body.style.height = KDOM.getBrowserHeight() + 'px';
			window.document.body.style.overflow = 'hidden';
		}
		
	};
	
	KFloatable.hide = function(floatable) {
		Kinky.unsetModal(floatable);
	};
	
	KSystem.included('KFloatable');
});