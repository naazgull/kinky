function KDraggable() {
}
{	
	KDraggable.make = function(target, css) {
		target.dropped = function() {
			KDraggable.dragging = false;
			if (!!KDraggable.target) {
				KDraggable.target.droppedIt(this);
			}
		};

		KEffects.addEffect(target, {
			type : 'drag',
			dragndrop : true,
			onStart : function(widget, tween) {
				KDraggable.dragging = true;
			},
			onComplete : function(widget, tween) {
				widget.dropped();
				KDraggable.dragging = false;
			}
		});
	};
	
	KDraggable.dragging = false;
	KDraggable.target = null;
	
	KSystem.included('KDraggable');
}