function KShellPart(parent, hash, service) {
	if (parent != null) {
		this.isLeaf = false;
		KWidget.call(this, parent);
		
		this.height = null;
		this.hash = hash;
		this.really = false;
	}
}

KSystem.include([
	'KWidget'
], function() {
	KShellPart.prototype = new KWidget();
	KShellPart.prototype.constructor = KShellPart;
	
	KShellPart.prototype.go = function() {
		if (this.gone) {
			if (!!KBreadcrumb.drilldown) {
				KBreadcrumb.dispatchEvent(null, {
					query : KBreadcrumb.drilldown
				});
			}
		}
		KWidget.prototype.go.call(this);
	};
	
	KShellPart.prototype.transition_hide = function(tween) {
		if (!!tween) {
			tween.onComplete = function(widget, t) {
				KShell.current.w.transition_show(KShell.current.t);
				widget.invisible();
				widget.destroy();
			};
		}
		else {
			KShell.current.w.transition_show(KShell.current.t);
		}
		KWidget.prototype.transition_hide.call(this, tween);
	};
	
	KShellPart.prototype.transition_show = function(tween) {
		if (!!KShell.current && (KShell.current.w.hash != this.hash)) {
			var last = KShell.current.w;
			delete KShell.current;
			KShell.current = {
				w : this,
				t : tween
			};
			last.unload();
			return;
		}
		
		KShell.current = {
			w : this,
			t : tween
		};
		KWidget.prototype.transition_show.call(this, tween);
	};
	
	KShellPart.prototype.visible = function() {
		if (!!this.parent && !!this.parent.config && !!this.config) {
			KShell.setWindowTitle(this.parent.config.title + ': ' + this.config.title);
			KBreadcrumb.setHistoryTitle(this.config.title);
		}
		
		KBreadcrumb.dispatchEvent(null, {
			action : '/kmenu/mark/item'
		});
		KWidget.prototype.visible.call(this);
		if (!!KBreadcrumb.drilldown) {
			KBreadcrumb.dispatchEvent(null, {
				query : KBreadcrumb.drilldown
			});
		}
	};
	
	KShellPart.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div'
		},
		html5 : {
			panel : 'section',
			content : 'div',
			background : 'div'
		}
	};
	
	KSystem.included('KShellPart');
});