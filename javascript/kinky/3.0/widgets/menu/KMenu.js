function KMenu(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.isLeaf = false;
		this.menu = window.document.createElement('ul');
		{
			this.menu.className = 'KMenuLevel1';
			this.menu.id = this.hash;
		}
		
		this.marked = [];
		this.addActionListener(KMenu.markSelected, [
			'/kmenu/mark/item'
		]);
	}
}

KSystem.include([
	'KWidget'
], function() {
	KMenu.prototype = new KWidget();
	KMenu.prototype.constructor = KMenu;
	
	KMenu.prototype.mark = function(hash) {
		if (!hash) {
			return;
		}
		
		var paths = hash.split('/');
		paths.splice(0, 1);
		
		for ( var level in this.marked) {
			this.removeCSSClass('KMenuMarked', this.marked[level]);
		}
		
		delete this.marked;
		this.marked = [];
		
		var ul = this.menu;
		var cur = '';
		for ( var path in paths) {
			for ( var i = 0; i != ul.childNodes.length; i++) {
				if ((ul.childNodes[i].name == cur + '/' + paths[path]) || (ul.childNodes[i].name == '/' + paths[path])) {
					this.marked.push(ul.childNodes[i]);
					this.addCSSClass('KMenuMarked', ul.childNodes[i]);
					var newUL = ul.childNodes[i].getElementsByTagName('ul');
					if (newUL.length != 0) {
						ul = newUL[0];
					}
					cur += '/' + paths[path];
					break;
				}
			}
		}
	};
	
	KMenu.prototype.loadIndex = function(page, level, topUL, parent) {
		var ul = null;
		if (level == null) {
			ul = topUL = this.menu;
			level = 0;
		}
		else {
			var li = window.document.createElement('li');
			li.name = page.hash;
			try {
				li.className = 'KMenuLevelItem' + level + ' KMenu' + page.hash.replace(/\//g, '_').toUpperCase();
				li.id = page.hash.replace(/\//g, '_').toUpperCase();
			}
			catch (e) {
			}
			
			var a = window.document.createElement('a');
			
			if (page.graphicStyle && page.graphicStyle.textImages && page.graphicStyle.textImages.pageMenuTitle) {
				a.appendChild(KCSS.img(page.graphicStyle.textImages.pageMenuTitle.base, page.menuText, page.titleText));
			}
			else {
				a.appendChild(window.document.createTextNode(page.title));
			}
			a.href = (/http:/.test(page.hash) ? '' : '#') + page.href;
			li.appendChild(a);
			
			ul = window.document.createElement('ul');
			ul.className = 'KMenuLevel' + (level + 1);
			li.appendChild(ul);
			
			topUL.appendChild(li);
		}
		for ( var index in page.pages) {
			this.loadIndex(page.pages[index], level + 1, ul, page);
		}
	};
	
	KMenu.markSelected = function(widget, hash) {
		if (widget.activated()) {
			widget.mark(KBreadcrumb.getHash());
		}
	};
	
	KMenu.prototype.draw = function() {
		KWidget.prototype.draw.call(this);
		this.loadIndex(this.data);
		this.content.appendChild(this.menu);
	};
	
	KMenu.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div'
		},
		html5 : {
			panel : 'aside',
			content : 'nav',
			background : 'div'
		}
	};
	
	KSystem.included('KMenu');
});