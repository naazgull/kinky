function KTable(parent) {
	if ((parent != null) || (parent == -1)) {
		this.className = KSystem.getObjectClass(this);
		
		this.parent = parent;
		this.editMode = false;
		this.isDrawn = false;
		this.data = null;
		this.children = null;
		this.requestedPage = this.nPage = 0;
		this.nChildren = 0;
		this.perPage = 10;
		this.totalCount = null;
		this.display = false;
		
		this.kinky = Kinky.bunnyMan;
		this.breadcrumb = '';
		
		this.id = this.id || this.kinky.addWidget(this);
		this.hash = null;
		
		this.panel = window.document.createElement('div');
		this.panel.id = this.id;
		this.panel.className = ' KWidgetPanel ' + this.className + ' ';
		this.panel.style.overflow = this.overflow;
		this.panel.style.position = 'relative';
		// this.panel.style.clear = 'both';
		
		if (!this.isLeaf) {
			this.topMarker = window.document.createElement('div');
			this.topMarker.style.position = 'relative';
			this.panel.appendChild(this.topMarker);
			
			this.background = window.document.createElement('div');
			this.background.className = ' KWidgetPanelBackground ' + this.className + 'Background ';
			this.background.style.position = 'absolute';
			this.background.style.top = '0px';
			this.background.style.left = '0px';
			this.panel.appendChild(this.background);
			
			this.contentContainer = window.document.createElement('table');
			this.contentContainer.className = ' KWidgetPanelContentContainer ' + this.className + 'ContentContainer ';
			this.contentContainer.style.clear = 'both';
			this.contentContainer.style.position = 'relative';
			this.panel.appendChild(this.contentContainer);
			
			this.widgetTitle = window.document.createElement('thead');
			this.widgetTitle.className = ' KWidgetPanelTitle ' + this.className + 'Title ';
			this.widgetTitle.style.position = 'relative';
			this.contentContainer.appendChild(this.widgetTitle);
			if (Kinky.TITLE_CLEAR_BOTH || this.hasClearOnTitle) {
				this.contentContainer.appendChild(KCSS.clearBoth(true));
			}
		}
		
		this.content = window.document.createElement('tbody');
		this.content.className = ' KWidgetPanelContent ' + this.className + 'Content ';
		if (!this.isLeaf) {
			this.contentContainer.appendChild(this.content);
		}
		else {
			this.panel.appendChild(this.content);
		}
		
		if (!this.isLeaf) {
			this.titleBottomMarker = window.document.createElement('div');
			this.titleBottomMarker.style.position = 'absolute';
			this.titleBottomMarker.style.bottom = '0';
			this.titleBottomMarker.style.clear = 'both';
			this.widgetTitle.appendChild(this.titleBottomMarker);
			
			this.bottomMarker = window.document.createElement('div');
			this.bottomMarker.style.position = 'absolute';
			this.bottomMarker.style.bottom = '0';
			this.bottomMarker.style.clear = 'both';
			this.content.appendChild(this.bottomMarker);
		}
		
		this.window = false;
		this.closeButton = null;
		this.minimizeButton = null;
		this.maximizeButton = null;
		this.buttons = 0;
		this.movable = false;
		this.frame = false;
		this.effects = new Object();
		this.waiting = false;
		
		this.error = null;
		this.request = null;
		this.response = null;
		this.needAuthentication = false;
		
		this.fromListener = false;
		this.lastWidth = 0;
		this.lastHeight = 0;
		this.scroll = {};
	}
}

KSystem.include([
	'KWidget',
	'KCell',
	'KRow'
], function() {
	
	KTable.prototype = new KWidget();
	KTable.prototype.constructor = KTable;
	
	KTable.prototype.go = function() {
		this.draw();
	};
	
	KTable.prototype.draw = function() {
		this.panel.style.overflow = 'visible';
		
		for ( var element in this.childWidgets()) {
			this.childWidget(element).go();
		}
		this.activate();
	};
	
	KTable.prototype.appendChild = function(element) {
		if (element instanceof KCell || element instanceof KRow) {
			KWidget.prototype.appendChild.call(this, element);
		}
	};
	
	KTable.prototype.load = function() {
		var params = new Object();
		if (this.data.feServiceArgs) {
			KSystem.merge(params, this.data.feServiceArgs);
		}
		params.contentView = this.data.contentView;
		params.contentID = this.data.contentID;
		params.offset = this.nPage * this.perPage;
		params.limit = this.perPage;
		this.kinky.get(this, this.data.feService, params);
	};
	
	KSystem.included('KTable');
});