function KText(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.tagName = 'div';
		this.lead = window.document.createElement('div');
		{
			this.addCSSClass('KTextLead', this.lead);
		}
		
		this.body = window.document.createElement('div');
		{
			this.addCSSClass('KTextBody', this.body);
		}
		
	}
}

KSystem.include([
	'KWidget'
], function() {
	KText.prototype = new KWidget;
	KText.prototype.constructor = KText;
	
	KText.prototype.getText = function() {
		return this.body.innerHTML;
	};
	
	KText.prototype.setText = function(text) {
		this.body.innerHTML = text;
	};
	
	KText.prototype.appendText = function(text) {
		this.body.innerHTML += text;
	};
	
	KText.prototype.getLead = function() {
		return this.lead.innerHTML;
	};
	
	KText.prototype.setLead = function(text) {
		this.lead.innerHTML = text;
	};
	
	KText.prototype.appendLead = function(text) {
		this.lead.innerHTML += text;
	};
	
	KText.prototype.draw = function() {
		if (!!this.data && !!this.data.title) {
			this.setTitle(this.data.title);
		}
		if (!!this.data && !!this.data.description) {
			this.setLead(this.data.description);
		}
		if (!!this.data && !!this.data.body) {
			this.setText(this.data.body);
		}
		if (!!this.data && !!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		if (!this.activated()) {
			this.container.appendChild(this.lead);
			this.container.appendChild(this.body);
		}
		else {
			this.content.appendChild(this.lead);
			this.content.appendChild(this.body);
		}
		this.activate();
	};
	
	KSystem.included('KText');
});
