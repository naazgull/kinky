function KFlashWidget(parent, url, service) {
	if (parent != null) {
		KShellPart.call(this, parent, url, service);
	}
}

KSystem.include([
	'KWidget', 'KShellPart'
], function() {

	KFlashWidget.prototype = new KWidget();
	KFlashWidget.prototype.constructor = KFlashWidget;

	KFlashWidget.prototype.onShow = function() {
		if (this.display) {
			return;
		}
	};

	KFlashWidget.prototype.onHide = function() {
		if (this.display && !(this instanceof KFlashWidgetDialog) && Kinky.shell.childWidget(this.getHash()) instanceof KFlashWidgetDialog) {
			return;
		}
	};

	KFlashWidget.prototype.draw = function() {
		this.activate();
	};

	KSystem.included('KFlashWidget');
});