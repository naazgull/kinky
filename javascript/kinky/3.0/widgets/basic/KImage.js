function KImage(parent, imageURL, popupURL) {
	if (parent != null) {
		this.isLeaf = true;
		KWidget.call(this, parent);
		
		this.image = window.document.createElement('img');
		this.image.onload = function() {
			var widget = KDOM.getWidget(this);
			if (!widget) {
				return;
			}
			widget.width = this.width;
			widget.height = this.height;
		};
		this.content.appendChild(this.image);
		
		if (imageURL != null) {
			this.imageURL = imageURL;
		}
		if (popupURL != null) {
			this.popupURL = popupURL;
		}
	}
}

KSystem.include([
	'KWidget'
], function() {
	KImage.prototype = new KWidget();
	KImage.prototype.constructor = KImage;
	
	KImage.prototype.removeEventListener = function(eventName, callback, target) {
		if (eventName == 'load') {
			KDOM.removeEventListener(this.image, 'click', KImage.changeListener);
		}
		else {
			KWidget.prototype.removeEventListener.call(this, eventName, callback, target);
		}
	};
	
	KImage.prototype.addEventListener = function(eventName, callback, target) {
		if (eventName == 'load') {
			this.callback = callback;
			KDOM.addEventListener(this.image, 'click', KImage.changeListener);
		}
		else {
			KWidget.prototype.addEventListener.call(this, eventName, callback, target);
		}
	};
	
	KImage.prototype.go = function() {
		if (!!this.imageURL) {
			this.draw();
		}
		else {
			KWidget.prototype.go.call(this);
		}
	};
	
	KImage.prototype.onLoad = function(data, request) {
		if (data.image) {
			this.imageURL = data.image;
		}
		if (data.description) {
			this.imageDesc = data.description;
		}
		if (data.imageScaled) {
			this.popupURL = data.imageScaled;
		}
		KWidget.prototype.onLoad.call(this, data);
	};
	
	KImage.prototype.setImage = function(imageURL) {
		this.imageURL = imageURL;
		if (this.activated()) {
			this.content.removeChild(this.image);
			this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(this.imageURL) : this.imageURL;
			this.content.appendChild(this.image);
		}
	};
	
	KImage.prototype.setPopupImage = function(popupURL) {
		this.popupURL = popupURL;
	};
	
	KImage.prototype.addMap = function(params) {
		if (this.map == null) {
			this.map = window.document.createElement('map');
			this.map.name = this.map.id = 'map' + this.id;
		}
		
		for ( var index in params.map) {
			var area = window.document.createElement('area');
			area.shape = (!!params.map[index].shape ? params.map[index].shape : 'poly');
			area.coords = params.map[index].coords;
			area.href = params.map[index].url;
			area.target = (!!params.map[index].target ? params.map[index].target : '_self');
			if (params.map[index].onOver) {
				KDOM.addEventListener(area, 'mouseover', params.map[index].onOver);
			}
			if (params.map[index].onOut) {
				KDOM.addEventListener(area, 'mouseout', params.map[index].onOut);
			}
			if (params.map[index].onClick) {
				KDOM.addEventListener(area, 'click', params.map[index].onClick);
			}
			this.map.appendChild(area);
		}
	};
	
	KImage.prototype.setTitle = function(text, isHTML) {
		if (isHTML) {
			this.image.title = this.image.alt = HTMLEntities.decode(text);
		}
		else {
			this.image.title = this.image.alt = text;
		}
	};
	
	KImage.prototype.setDescription = function(text, isHTML) {
		if (!this.p) {
			this.p = window.document.createElement('p');
			this.content.appendChild(this.p);
		}
		if (isHTML) {
			this.p.innerHTML = text;
		}
		else {
			this.p.appendChild(window.document.createTextNode(text));
		}
	};
	
	KImage.prototype.setImageName = function(text) {
		this.imageDesc = text;
	};
	
	KImage.prototype.setDefaultImage = function(imageURL) {
		this.defaultImage = imageURL;
	};
	
	KImage.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		if (this.imageDesc) {
			KDOM.addEventListener(this.image, 'mouseover', KImage.onMouseOver);
		}
		
		if (!!this.data && !!this.data.title) {
			this.setTitle(this.data.title);
		}
		
		if (!!this.data && !!this.data.description) {
			this.setDescription(this.data.title);
		}
		
		if (!!this.data && !!this.data.graphics && !!this.data.graphics.background) {
			this.setBackground(this.data.graphics.background);
		}
		
		if (this.map) {
			this.image.useMap = '#' + this.map.id;
			this.content.appendChild(this.map);
		}
		
		this.activate();
		if (this.popupURL) {
			if ((KBrowserDetect.browser == 2) && (KBrowserDetect.version < 9)) {
				this.popup = new Image();
			}
			else {
				this.popup = window.document.createElement('img');
			}
			this.popup.src = this.popupURL;
		}
		this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(this.imageURL) : this.imageURL;
		KDOM.fireEvent(this.image, 'click');
	};
	
	KImage.onMouseOver = function(event) {
		
	};

	KImage.changeListener = function(e) {
		var w = KDOM.getEventWidget(e);
		w.changeTimer = KSystem.addTimer(function() {
			if (!w.image.width) {
				return;
			}

			KSystem.removeTimer(w.changeTimer);
			delete w.changeTimer;
			w.callback(e);
		}, 500, true);
	};

	
	KImage.IMAGE_ELEMENT = 'image';
	
	KSystem.included('KImage');
});
