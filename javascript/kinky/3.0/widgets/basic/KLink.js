function KLink(parent) {
	if (parent != null) {
		this.isLeaf = true;
		KWidget.call(this, parent);
		
		this.link = window.document.createElement('a');
		this.link.target = '_self';
		this.link.href = 'javascript:void(0)';
		this.text = window.document.createElement('div');
		this.image = window.document.createElement('img');
	}
}

KSystem.include([
	'KTooltip',
	'KBaloon',
	'KWidget'
], function() {
	KLink.prototype = new KWidget;
	KLink.prototype.constructor = KLink;
	
	KLink.prototype.setLinkText = function(text, isHTML) {
		if (text) {
			if (isHTML) {
				this.text.innerHTML = text;
			}
			else {
				this.text.innerHTML = '';
				this.text.appendChild(window.document.createTextNode(text));
			}
		}
	};
	
	KLink.prototype.setLinkImage = function(imageURL) {
		if (imageURL) {
			this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(imageURL) : imageURL;
		}
	};
	
	KLink.prototype.setLinkOverImage = function(imageURL) {
		if (imageURL) {
			this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(imageURL) : imageURL;
		}
	};
	
	KLink.prototype.setTooltip = function(tooltip) {
		if (tooltip) {
			this.data.tooltip = tooltip;
		}
	};
	
	KLink.prototype.setLinkURL = function(newURL) {
		if (newURL) {
			this.link.href = newURL;
		}
	};
	
	KLink.prototype.initLink = function() {
		this.setLinkURL(this.data.url);
		this.setTooltip(this.data.tooltip);
		this.setLinkImage(this.data.image);
		this.setLinkText((!!this.data.description ? this.data.description : this.data.title));
	};
	
	KLink.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		if (!!this.data && !!this.data.url) {
			this.initLink();
		}
		
		if (!!this.data && !!this.data.graphics && !!this.data.graphics.background) {
			this.setBackground(this.data.graphics.background);
		}
		
		if (!!this.data && !!this.data.targetWindow) {
			this.link.target = this.data.targetWindow;
		}
		
		this.text.className = ' KLinkText ';
		this.image.className = ' KLinkImage ';
		
		if (!!this.data && !!this.data.image) {
			this.link.appendChild(this.image);
			this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(this.data.image) : this.data.image;
			this.isImageLink = true;
		}
		
		if ((this.text.innerHTML.length == 0) && (!!this.data && !!this.data.description)) {
			this.setLinkText(this.data.description, true);
		}
		this.link.appendChild(this.text);
		
		if (!!this.data && !!this.data.tooltip) {
			KBaloon.make(this, {
				text : this.data.tooltip
			});
		}
		
		this.content.appendChild(this.link);
		
		this.activate();
	};
	
	KLink.onMouseOver = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var widget = KDOM.getEventWidget(event);
		
		if (widget.hasTooltip) {
			KTooltip.showTooltip(widget.link, {
				text : widget.data.tooltip,
				isHTML : true,
				offsetX : 15,
				offsetY : 15,
				left : KDOM.mouseX(mouseEvent),
				top : (KDOM.mouseY(mouseEvent) + 15),
				cssClass : 'KLinkImageTooltip'
			});
		}
	};
	
	KLink.LINK_ELEMENT = 'link';
	KLink.IMAGE_ELEMENT = 'image';
	KLink.TEXT_ELEMENT = 'text';
	
	KSystem.included('KLink');
});