function KWidget(parent) {
	if ((parent != null) || (parent == -1)) {
		this.className = KSystem.getObjectClass(this);

		this.parent = parent;
		this.parentDiv = null;
		this.isDrawn = false;
		this.data = null;
		this.config = null;
		this.children = new Object();
		this.childrenid = 0;
		this.nChildren = 0;
		this.loadedChildren = 0;
		this.display = false;
		this.gone = false;
		this.shell = (parent != -1 ? this.parent.shell : null);
		this.addons = {};

		this.kinky = Kinky.bunnyMan;

		this.id = (!!this.id ? this.id : this.kinky.addWidget(this));
		this.hash = null;
		this.ref = null;

		if (!this.tagNames) {
			alert(this.className);
		}
		this.panel = window.document.createElement(this.isAside ? 'aside' : this.tagNames[Kinky.HTML_READY][KWidget.ROOT_DIV]);
		{
			this.panel.id = this.id;
			this.panel.className = ' KWidgetPanel ' + this.className + ' ';
			this.panel.style.position = 'relative';
		}

		this.content = window.document.createElement(this.tagNames[Kinky.HTML_READY][KWidget.CONTENT_DIV]);
		{
			this.content.className = ' KWidgetPanelContent ' + this.className + 'Content ';
		}
		this.panel.appendChild(this.content);

		this.container = window.document.createElement('div');
		{
			this.setStyle({
				width : '0px',
				height : '0px',
				overflow : 'hidden',
				display : 'block'
			}, this.container);
			this.container.className = this.content.className;
		}
		this.panel.appendChild(this.container);

		this.waiting = 0;

		this.fromListener = false;
		this.width = 0;
		this.height = 0;
		this.top = 0;
		this.left = 0;
		this.children_retrieved = false;
		this.feed_retrieved = false;
		this.no_feed = false;
		this.no_children = false;

		if (Kinky.bunnyMan.currentState.length != 1) {
			for ( var i in Kinky.bunnyMan.currentState) {
				this.dispatchState(Kinky.bunnyMan.currentState[i]);
			}
		}

	}
}
KSystem.include([
	'KAddOn',
	'KLayout',
	'KScrollBars'
], function() {
	KWidget.prototype.className = null;
	KWidget.prototype.parent = null;
	KWidget.prototype.isDrawn = false;
	KWidget.prototype.data = null;
	KWidget.prototype.children = null;
	KWidget.prototype.kinky = null;
	KWidget.prototype.id = false;
	KWidget.prototype.hash = null;
	KWidget.prototype.isLeaf = true;
	KWidget.prototype.isAside = false;
	KWidget.prototype.silent = false;

	// >>>
	// Widget action handlers
	KWidget.prototype.onLoad = function(data, request) {
		this.data = data;
		KCache.commit(this.ref + '/data', this.data);

		if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren(data, request);
		}
		else if (!this.no_feed && !this.feed_retrieved) {
			this.retrieveFeed(data, request);
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.retrieveFeed = function(config_data, config_request) {
		var retrieved = this.feed_retrieved;
		var elements = KCache.restore(this.ref + '/feed');
		this.feed_retrieved = true;

		if (!elements && !!this.data.feed) {
			this.onFeed(this.data.feed, config_request);
		}
		else if (!elements && !retrieved && !!this.data.template && !!this.data.template.widget && !!this.data.links && !!this.data.links.feed && !!this.data.links.feed.href) {
			if (!KSystem.isIncluded(this.data.template.widget.type)) {
				this.feed_retrieved = false;
				var id = this.id;
				KSystem.include([
					this.data.template.widget.type
				], function() {
					if (!!Kinky.getWidget(id)) {
						Kinky.getWidget(id).retrieveFeed();
					}
				});
				return;
			}
			var headers = null;
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + this.data.links.feed.href, {}, headers, 'onFeed');
		}
		else if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren();
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.retrieveChildren = function(config_data, config_request) {
		var retrieved = this.children_retrieved;
		var elements = KCache.restore(this.ref + '/children');
		this.children_retrieved = true;

		if (!elements && !!this.data.children) {
			this.onChildren(this.data.children, config_request);
		}
		else if (!elements && !this.isLeaf && !retrieved && !!this.data.links && !!this.data.links.children && !!this.data.links.children.href) {
			var headers = null;
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + KSystem.urlify(this.data.links.children.href, {
				fields : 'elements'
			}), {}, headers, 'onChildren');
		}
		else if (!this.no_feed && !this.feed_retrieved) {
			this.retrieveFeed();
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.onFeed = function(data, request) {
		if (!!this.data.template && !!this.data.template.widget && !!this.data.template.translationTable) {
			this.data.feed = {};

			var config = {
				hash : this.hash,
				rel : "\/wrml-relations\/widgets\/{widget_id}",
				requestTypes : [
					"application\/json",
					"application\/vnd.kinky." + this.data.template.widget.type
				],
				responseTypes : [
					"application\/json",
					"application\/vnd.kinky." + this.data.template.widget.type
				],
				links : {
					self : {
						href : this.config.href,
						rel : "\/wrml-relations\/widgets\/{widget_id}"
					}
				}
			};

			for ( var l in data.elements) {
				var childConfig = KSystem.clone(config);
				childConfig.hash += '/' + l;
				childConfig.links.self.href += '/' + l;

				childConfig.data = new Object();
				childConfig.data.feeditem = data.elements[l];
				for ( var t in this.data.template.translationTable) {
					var name = this.data.template.translationTable[t].srcField;
					var value = data.elements[l][name];
					childConfig.data[this.data.template.translationTable[t].dstField] = value;
					if (!value && ((name == 'id')) && !!data.elements[l]._id) {
						childConfig.data[this.data.template.translationTable[t].dstField] = data.elements[l]._id;
					}
				}
				this.data.feed[childConfig.hash] = childConfig;
			}
			this.data.feed.size = data.size;
		}
		else {
			this.data.feed = data;
		}
		KCache.commit(this.ref + '/feed', this.data.feed);
		if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren();
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.onChildren = function(data, request) {
		var includes = [];
		var size = 0;
		if (!this.data.children) {
			this.data.children = data;
		}
		this.addonsdata = [];

		for ( var link in data.elements) {
			size++;
			var jsClass = KSystem.getWRMLClass(data.elements[link], 'vnd.kinky.');
			if (!!jsClass) {
				if (KSystem.isAddOn(jsClass)) {
					try {
						var child = null;
						try {
							child = KSystem.construct(data.elements[link], this);
							this.appendChild(child);
						}
						catch (e) {
							continue;
						}
						this.addonsdata.push(data.elements[link]);
					}
					catch (e) {
					}
				}
				else {
					includes.push(jsClass);
				}
			}
		}
		this.data.children.size = size;
		KCache.commit(this.ref + '/addons', this.addonsdata);
		KCache.commit(this.ref + '/children', this.data.children);

		if (includes.length != 0) {
			var id = this.id;
			KSystem.include(includes, function() {
				var widget = Kinky.getWidget(id);
				if (!widget.no_feed && !widget.feed_retrieved) {
					widget.retrieveFeed();
				}
				else {
					widget.draw();
				}
			});
			return;
		}

		if (!this.no_feed && !this.feed_retrieved) {
			this.retrieveFeed();
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.onSave = function(data, request) {
	};

	KWidget.prototype.onRemove = function(data, request) {
	};

	KWidget.prototype.onPost = function(data, request) {
	};

	KWidget.prototype.onPut = function(data, request) {
	};

	KWidget.prototype.onDestroy = function() {
	};

	KWidget.prototype.onError = function(data, request) {
		this.abort();
	};
	// <<<

	// >>>
	// HTTP response handlers
	KWidget.prototype.onOk = function(data, request) {
		this.onLoad(data, request);
	};

	KWidget.prototype.onHead = function(data, request) {
		this.onLoad(data, request);
	};

	KWidget.prototype.onCreated = function(data, request) {
		this.onLoad(data, request);
	};

	KWidget.prototype.onAccepted = function(data, request) {
		this.onLoad(data, request);
	};

	KWidget.prototype.onNoContent = function(data, request) {
		if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren(data, request);
			return;
		}
		else {
			this.no_children = !this.data || !this.data.children || ((this.data.children.size == 0));
		}

		if (this.children_retrieved && !this.feed_retrieved && !this.no_feed) {
			this.retrieveFeed(data, request);
			return;
		}
		else {
			this.no_feed = !this.data || !this.data.feed || ((this.data.feed.size == 0));
		}

		this.onLoad(data, request);
	};

	KWidget.prototype.onBadRequest = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onUnauthorized = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onForbidden = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onNotFound = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onMethodNotAllowed = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onPreConditionFailed = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onInternalError = function(data, request) {
		this.onError(data, request);
	};
	// <<<

	KWidget.prototype.dispatchState = function(stateName) {
		var refresh = false;
		for ( var func in KWidget.STATE_FUNCTIONS) {
			var exists = false;
			eval('exists = !!' + this.className + '.prototype.' + stateName + '_' + KWidget.STATE_FUNCTIONS[func] + ';');
			refresh |= exists;
			if (exists) {
				eval('this.' + KWidget.STATE_FUNCTIONS[func] + ' = ' + this.className + '.prototype.' + stateName + '_' + KWidget.STATE_FUNCTIONS[func] + ';');
			}
		}
		if (refresh && this.activated()) {
			this.refresh();
		}
	};

	KWidget.prototype.size = function() {
		return this.nChildren;
	};

	KWidget.prototype.insertBefore = function(element, before, childDiv) {
		if (!element) {
			return;
		}

		if (!!childDiv && !this.hasChildDiv(childDiv)) {
			throw new KException(this, ' insertBefore: first add ' + childDiv);
		}

		if (element instanceof KAddOn) {
			element.parent = this;
			element.go();
			return;
		}

		var elementKey = element.key = (!!element.hash ? element.hash : element.id);
		if (!!this.children[elementKey]) {
			return;
		}
		this.children[elementKey] = element;

		element.parentDiv = (!!childDiv ? this.childDiv(childDiv) : this.content);
		if (!childDiv) {
			childDiv = (!this.activated() ? 'container' : 'content');
		}

		this.childDiv(childDiv).insertBefore(element.panel, before.panel);
		this.nChildren++;
		if (this.activated() && !element.gone) {
			element.go();
		}

		return element;
	};

	KWidget.prototype.insertAfter = function(element, after, childDiv) {
		if (!element) {
			return;
		}

		if (!!childDiv && !this.hasChildDiv(childDiv)) {
			throw new KException(this, ' insertAfter: first add ' + childDiv);
		}

		if (element instanceof KAddOn) {
			element.parent = this;
			element.go();
			return;
		}

		var elementKey = element.key = (!!element.hash ? element.hash : element.id);
		if (!!this.children[elementKey]) {
			return;
		}
		this.children[elementKey] = element;

		element.parentDiv = (!!childDiv ? this.childDiv(childDiv) : this.content);
		if (!childDiv) {
			childDiv = (!this.activated() ? 'container' : 'content');
		}

		this.childDiv(childDiv).insertBefore(element.panel, after.panel.nextSibling);
		this.nChildren++;
		if (this.activated() && !element.gone) {
			element.go();
		}

		return element;
	};

	KWidget.prototype.appendChild = function(element, childDiv) {
		if (!element) {
			return;
		}

		if (!!childDiv && !this.hasChildDiv(childDiv)) {
			throw new KException(this, ' appendChild: first add ' + childDiv);
		}

		if (element instanceof KAddOn) {
			element.parent = this;
			element.go();
			return false;
		}

		var elementKey = element.key = (!!element.hash ? element.hash : element.id);
		if (!!this.children[elementKey]) {
			return;
		}
		this.children[elementKey] = element;

		element.parentDiv = (!!childDiv ? this.childDiv(childDiv) : this.content);
		if (!childDiv) {
			childDiv = (!this.activated() ? 'container' : 'content');
		}

		this.childDiv(childDiv).appendChild(element.panel);
		this.nChildren++;
		if (this.activated() && !element.gone) {
			element.go();
		}

		return element;
	};

	KWidget.prototype.hasChildDiv = function(target) {
		if (typeof target == 'string') {
			return !!this[target];
		}
		else {
			return target;
		}
	};

	KWidget.prototype.childDiv = function(target) {
		var domElement = null;
		if (target) {
			if (typeof target == 'string') {
				if (!!this[target]) {
					domElement = this[target];
				}
			}
			else {
				domElement = target;
			}
		}

		return domElement;
	};

	KWidget.prototype.childWidget = function(id) {
		return this.children[id];
	};

	KWidget.prototype.childAt = function(index) {
		if (((index >= 0)) && ((index < this.nChildren))) {
			var pos = 0;
			for ( var child in this.childWidgets()) {
				if (pos == index) {
					return this.childWidget(child);
				}
				pos++;
			}
		}
		return null;
	};

	KWidget.prototype.indexOf = function(widget) {
		try {
			var pos = 0;
			for ( var child in this.childWidgets()) {
				if (widget.id == this.childWidget(child).id) {
					return pos;
				}
				pos++;
			}
		}
		catch (e) {
		}
		return -1;
	};

	KWidget.prototype.childWidgets = function() {
		return this.children;
	};

	KWidget.prototype.focus = function(dispatcher) {
		this.parent.focus(this);
	};

	KWidget.prototype.blur = function(dispatcher) {
		this.parent.blur(this);
	};

	KWidget.prototype.unload = function() {
		if (this.display) {
			this.onHide();
			return;
		}
		this.destroy();
	};

	KWidget.prototype.abort = function() {
		var parent = this.parent;
		this.destroy();
		if (parent && (parent instanceof KWidget)) {
			parent.onChildLoaded();
		}
	};

	KWidget.prototype.destroy = function() {
		this.onDestroy();
		var id = this.id;

		this.removeAllChildren();

		if (!!this.parent) {
			this.parent.removeChild(this);
		}

		for ( var att in this.addons) {
			if (this.addons[att]) {
				this.addons[att].destroy();
			}
		}

		for ( var att in this) {
			if (this[att] && (att != 'data') && (att != 'config')) {
				delete this[att];
			}
		}
		Kinky.bunnyMan.removeWidget(id);
	};

	KWidget.prototype.removeChild = function(element) {
		var elementKey = element.hash || element.id;
		if (!!this.children[elementKey]) {
			if (element.panel.parentNode) {
				element.panel.parentNode.removeChild(element.panel);
			}
			element.parent = undefined;
			element.unload();
			delete this.children[elementKey];
			this.nChildren--;
			return true;
		}

		return false;
	};

	KWidget.prototype.removeAllChildren = function() {
		for ( var elementKey in this.childWidgets()) {
			this.childWidget(elementKey).unload();
		}
		delete this.children;
		this.children = new Object();

		this.nChildren = 0;
		this.loadedChildren = 0;
	};

	KWidget.prototype.disable = function() {
		if (!this.w_overlay) {
			this.w_overlay = window.document.createElement('div');
			this.w_overlay.className = this.className + "Overlay KWidgetDisableOverlay";
		}
		var width = Math.max(this.panel.offsetWidth, this.content.offsetWidth);
		KCSS.setStyle({
			position : 'absolute',
			top : '0',
			left : '0',
			width : (isNaN(width) ? KDOM.getBrowserWidth() : width) + 'px',
			height : Math.max(this.panel.offsetHeight, this.content.offsetHeight) + 'px'
		}, [
		this.w_overlay
		]);
		this.panel.appendChild(this.w_overlay);
		KCSS.setStyle({
			opacity : '1'
		}, [
		this.w_overlay
		]);
	};

	KWidget.prototype.enable = function() {
		if (!!this.w_overlay && !!this.w_overlay.parentNode) {
			KCSS.setStyle({
				opacity : '0'
			}, [
			this.w_overlay
			]);
			var self = this;
			KSystem.addTimer(function(){
				self.panel.removeChild(self.w_overlay);
			}, 200);
		}
	};

	KWidget.prototype.getShell = function() {
		return Kinky.getShell(this.shell);
	};

	KWidget.prototype.getShellConfig = function() {
		return Kinky.getShellConfig(this.shell);
	};

	KWidget.prototype.getConnection = function(type) {
		return KConnectionsPool.getConnection(type, this.shell);
	};

	KWidget.prototype.getHeight = function() {
		if (!this.activated() && !!this.container) {
			return this.container.offsetHeight;
		}
		return this.content.offsetHeight;
	};

	KWidget.prototype.getWidth = function() {
		if (!this.activated() && !!this.container) {
			return this.container.offsetWidth;
		}
		return this.content.offsetWidth;
	};

	KWidget.prototype.getRootHeight = function() {
		return this.panel.offsetHeight;
	};

	KWidget.prototype.getRootWidth = function() {
		return this.panel.offsetWidth;
	};

	KWidget.prototype.getComputedStyle = function(childDiv) {
		var domElement = this.childDiv(childDiv);
		if (!domElement) {
			if (!!this.container) {
				domElement = this.container;
			}
			else {
				domElement = this.content;
			}
		}
		return KCSS.getComputedStyle(domElement);
	};

	KWidget.prototype.scrollTop = function() {
		var parent = this.panel;
		var toReturn = 0;
		while (parent != null) {
			if ((parent.nodeName.toLowerCase() != 'html') && parent.scrollTop !== undefined) {
				toReturn += parent.scrollTop;
			}
			parent = parent.parentNode;
		}
		if (!!window.document.body.style.overflow && ((window.document.body.style.overflow == 'hidden'))) {
			return toReturn - (!!window.pageYOffset ? window.pageYOffset : 0);
		}
		return toReturn;
	};

	KWidget.prototype.offsetTop = function() {
		var parent = this.panel;
		var toReturn = 0;
		while (parent != null) {
			toReturn += parent.offsetTop;
			parent = parent.offsetParent;
		}
		return toReturn;
	};

	KWidget.prototype.offsetLeft = function() {
		var parent = this.panel;
		var toReturn = 0;
		while (parent != null) {
			toReturn += parent.offsetLeft;
			if (((KBrowserDetect.browser == 2)) && ((KBrowserDetect.version == 7))) {
				parent = parent.offsetParent;
			}
			else {
				parent = parent.offsetParent;
			}
		}
		return toReturn;
	};

	KWidget.prototype.offsetRight = function() {
		var parent = this.panel;
		var parentWidget = this;
		var toReturn = 0;
		while (parent != null) {
			toReturn += parent.offsetRight;
			if (((KBrowserDetect.browser == 2)) && ((KBrowserDetect.version == 7))) {
				parent = parentWidget.panel;
				parentWidget = parentWidget.parent;
			}
			else {
				parent = parent.offsetParent;
			}
		}
		return toReturn;
	};

	KWidget.prototype.setBackground = function(bg) {
		KCSS.setStyle({
			backgroundImage : 'url("' + bg + '")'
		}, [
			this.content
		]);
	};

	KWidget.prototype.setTitle = function(title, isHTML) {
		var div = (this.activated() || !this.container ? this.content : this.container);

		if (div.getElementsByTagName('h2').length == 0) {
			var h2 = window.document.createElement('h2');
			if (isHTML) {
				if (typeof title == 'string') {
					h2.innerHTML = title;
				}
				else {
					h2.appendChild(title);
				}
			}
			else {
				h2.appendChild(window.document.createTextNode(title));
			}
			if (div.childNodes.length > 0) {
				div.insertBefore(h2, div.childNodes[0]);
			}
			else {
				div.appendChild(h2);
			}
			h2.className = ' ' + this.className + 'Title ';
			this.title = h2;
			return h2;
		}
		else {
			var h2 = div.getElementsByTagName('h2')[0];
			h2.innerHTML = '';
			if (isHTML) {
				if (typeof title == 'string') {
					h2.innerHTML = title;
				}
				else {
					h2.appendChild(title);
				}
			}
			else {
				h2.appendChild(window.document.createTextNode(title));
			}
			return h2;
		}
	};

	KWidget.prototype.appendTitleText = function(title, isHTML) {
		var div = (this.activated() || !this.container ? this.content : this.container);

		if (div.getElementsByTagName('h2').length == 0) {
			var h2 = window.document.createElement('h2');
			if (isHTML) {
				if (typeof title == 'string') {
					h2.innerHTML = title;
				}
				else {
					h2.appendChild(title);
				}
			}
			else {
				h2.appendChild(window.document.createTextNode(title));
			}
			if (div.childNodes.length > 0) {
				div.insertBefore(h2, div.childNodes[0]);
			}
			else {
				div.appendChild(h2);
			}
			h2.className = ' ' + this.className + 'Title ';
			return h2;
		}
		else {
			var h2 = div.getElementsByTagName('h2')[0];
			if (isHTML) {
				if (typeof title == 'string') {
					h2.innerHTML += title;
				}
				else {
					h2.appendChild(title);
				}
			}
			else {
				h2.appendChild(window.document.createTextNode(title));
			}
			return h2;
		}
	};

	KWidget.prototype.addCSSClass = function(cssClass, target) {
		var domElement = null;
		if (target) {
			if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' addCSSClass: first add ' + target);
				}
				domElement = this[target];
			}
			else {
				domElement = target;
			}
		}
		else {
			domElement = this.panel;
		}
		domElement.className += ' ' + cssClass + ' ';
	};

	KWidget.prototype.removeCSSClass = function(cssClass, target) {
		var domElement = null;
		if (target) {
			if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' removeCSSClass: first add ' + target);
				}
				domElement = this[target];
			}
			else {
				domElement = target;
			}
		}
		else {
			domElement = this.panel;
		}
		if (domElement.className) {
			var regex = new RegExp(' ' + cssClass + ' ', 'g');
			domElement.className = domElement.className.replace(regex, '');
		}
	};

	KWidget.prototype.setStyle = function(cssStyle, target) {
		var domElements = null;
		if (target) {
			if (target instanceof Array) {
				domElements = new Array();
				for ( var index in target) {
					if (typeof target[index] == 'string') {
						if (!this[target[index]]) {
							throw new KException(this, ' setStyle: first add ' + target[index]);
						}
						domElements.push(this[target[index]]);
					}
					else {
						domElements.push(target);
					}
				}
			}
			else if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' setStyle: first add ' + target);
				}
				domElements = [
					this[target]
				];
			}
			else {
				domElements = [
					target
				];
			}
		}
		else {
			domElements = [
				this.panel
			];
		}
		KCSS.setStyle(cssStyle, domElements);
	};

	KWidget.prototype.clearBoth = function(target) {
		if (target) {
			if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' clearBoth: first add ' + target);
				}
				target = this[target];
			}
		}
		else {
			target = (this.activated() || !this.container ? this.content : this.container);
		}
		target.appendChild(KCSS.clearBoth());
	};

	KWidget.prototype.addWindowResizeListener = function(callback) {
		this.kinky.addWindowResizeListener(this, callback);
	};

	KWidget.prototype.addResizeListener = function(callback) {
		this.kinky.addResizeListener(this, callback);
	};

	KWidget.prototype.addLocationListener = function(callback) {
		KBreadcrumb.addLocationListener(this, callback);
	};

	KWidget.prototype.addQueryListener = function(callback, queries) {
		KBreadcrumb.addQueryListener(this, callback, queries);
	};

	KWidget.prototype.addActionListener = function(callback, actions) {
		KBreadcrumb.addActionListener(this, callback, actions);
	};

	KWidget.prototype.addEventListener = function(eventName, callback, target) {
		var domElement = null;
		if (target) {
			if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' addEventListener: first add ' + target);
				}
				domElement = this[target];
			}
			else {
				domElement = target;
			}
		}
		else {
			domElement = this.panel;
		}

		KDOM.addEventListener(domElement, eventName, callback);
	};

	KWidget.prototype.removeEventListener = function(eventName, callback, target) {
		var domElement = null;
		if (target) {
			if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' addEventListener: first add ' + target);
				}
				domElement = this[target];
			}
			else {
				domElement = target;
			}
		}
		else {
			domElement = this.panel;
		}

		KDOM.removeEventListener(domElement, eventName, callback);
	};

	KWidget.prototype.getLink = function(params) {
		return KBreadcrumb.getLink(params);
	};

	KWidget.prototype.load = function() {
		if (!!this.config && !!this.config.href) {
			var headers = null;
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + KSystem.urlify(this.config.href, {
				embed : 'children'
			}), {}, headers);
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.go = function() {
		if (this.gone) {
			return;
		}
		this.gone = true;
		this.ref = (!!this.config && !!this.config.href ? this.config.href : (!!this.hash ? this.hash : '/' + this.id));

		this.addonsdata = KCache.restore(this.ref + '/addons');
		if (!!this.addonsdata) {
			for ( var link in this.addonsdata) {
				var jsClass = KSystem.getWRMLClass(this.addonsdata[link], 'application/vnd.kinky.');
				if (!!jsClass) {
					var child = null;
					try {
						child = KSystem.construct(this.addonsdata[link], this);
						this.appendChild(child);
					}
					catch (e) {
						continue;
					}
				}
			}
		}

		var storedConfig = KCache.restore(this.ref + '/config');
		if (!storedConfig && !!this.config) {
			KCache.commit(this.ref + '/config', this.config);
		}
		this.config = (!!this.config ? this.config : storedConfig);
		var data = (!!this.data && !!this.data.embedded ? this.data : KCache.restore(this.ref + '/data'));

		if (!!this.config && !!data) {
			this.onLoad(data, {});
		}
		else if (!this.activated()) {
			this.load();
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.resume = function(noDisplay) {
		if (this.waiting != 0) {
			this.waiting--;
			if (!!this.getShell()) {
				this.getShell().hideLoading();
			}
		}
	};

	KWidget.prototype.wait = function() {
		if (!!this.getShell()) {
			this.getShell().showLoading();
		}
		this.waiting++;
	};

	KWidget.prototype.deactivate = function() {
		this.isDrawn = false;
	};

	KWidget.prototype.activate = function() {
		if (!this.activated()) {
			this.isDrawn = true;
			KBreadcrumb.dispatchEvent(this.id);
		}
		this.isDrawn = true;
		this.resume();
		if (!this.display) {
			this.onShow();
		}
	};

	KWidget.prototype.activated = function() {
		return this.isDrawn;
	};

	KWidget.prototype.clear = function() {
		for (; this.content.childNodes.length != 0;) {
			this.content.removeChild(this.content.childNodes[0]);
		}
	};

	KWidget.prototype.silence = function() {
		this.silent = true;
		this.parent.loadedChildren++;
		this.parent.onChildLoaded(this);
	};

	KWidget.prototype.onChildLoaded = function(childWidget) {
		if (((this.size() <= this.loadedChildren)) && !this.display) {
			this.activate();
		}
	};

	KWidget.prototype.visible = function() {
		if (this.display) {
			return;
		}

		this.display = true;
		if (!this.container) {
			return;
		}

		for (; this.container.childNodes.length != 0;) {
			var div = this.container.childNodes[0];
			this.content.appendChild(div);
		}

		this.panel.removeChild(this.container);
		delete this.container;
		this.computeScrollbars();
	};

	KWidget.prototype.invisible = function() {
		if (!this.display) {
			return;
		}
		this.display = false;
	};

	KWidget.prototype.onShow = function() {
		if (this.parent && (this.parent instanceof KWidget) && !this.silent) {
			this.parent.loadedChildren++;
			this.parent.onChildLoaded(this);
		}

		this.transition_show();
		return true;
	};

	KWidget.prototype.onHide = function() {
		this.transition_hide();
		return false;
	};

	KWidget.prototype.transition_show = function(tween) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.show) {
			tween = this.data.tween.show;
		}
		if (!!tween) {
			KEffects.addEffect((!!tween.target ? tween.target : this), tween);
		}
		else {
			this.visible();
		}
	};

	KWidget.prototype.transition_hide = function(tween) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.hide) {
			tween = this.data.tween.hide;
		}
		if (!!tween) {
			var toAdd = null;
			if (tween instanceof Array) {
				toAdd = tween[0];
			}
			else {
				toAdd = tween;
			}
			if (!toAdd.onComplete) {
				toAdd.onComplete = function(widget, t) {
					widget.invisible();
					widget.destroy();
				};
			}
			KEffects.addEffect((!!tween.target ? tween.target : this), tween);
		}
		else {
			this.invisible();
			this.unload();
		}
	};


	KWidget.prototype.computeScrollbars = function() {
		var style = this.getComputedStyle(this.panel);
		if (style.overflow == 'auto' || style.overflow == 'scroll' || style.overflowY == 'auto' || style.overflowY == 'scroll' || style.overflowX == 'auto' || style.overflowX == 'scroll') {
			var r = this.panel.className.trim().replace(/([a-zA-Z]+)/g, '\\.$1(.*)\\.((.*)Scroll)').replace(/ /g, '|').replace(/\|\|/g, '|');
			var regex = new RegExp(r);
			var rule = KCSS.getCSS(regex);
			if (!!rule) {
				this.content.style.position = 'relative';
				KScroll.make(this, rule.selectorText);
			}
		}
	};

	KWidget.prototype.scrollIntoView = function() {
		var scrolled = KScroll.getScrollInWidgets(this);
		if (!scrolled) {
			this.panel.scrollIntoView(false);
			return;
		}
		else {
			scrolled.parent.content.offsetScroll = 0;
			scrolled.parent.content.scrollTop = 0;
			if (this.panel.offsetTop - scrolled.position.v > scrolled.viewportH) {
				scrolled.scrollTo({ y : this.panel.offsetTop });
			}
		}
	};

	KWidget.prototype.scroll = function(barpos, scroll) {
		var top = KDOM.normalizePixelValue(this.content.style.top);
		if (isNaN(top)) {
			top = 0;
		}
		var left = KDOM.normalizePixelValue(this.content.style.left);
		if (isNaN(left)) {
			left = 0;
		}

		top = -Math.round((barpos.pv - scroll.topbH) / scroll.proportionV);
		left = -Math.round((barpos.ph - scroll.leftbW) / scroll.proportionH);

		this.content.style.top = top + 'px';
		this.content.style.left = left + 'px';

	};

	KWidget.prototype.draw = function() {
		if (!!this.data) {
			if (!!this.data && !!this.data['class']) {
				this.addCSSClass(this.data['class']);
			}

			if (!!this.data.graphics && !!this.data.graphics.background) {
				this.setBackground(this.data.graphics.background);
			}

			if (!!this.data.feed && !!this.data.template && !!this.data.template.widget && !this.no_feed) {
				for ( var link in this.data.feed) {
					var jsClass = KSystem.getWRMLClass(this.data.feed[link], 'application/vnd.kinky.');
					if (!!jsClass) {
						var child = null;
						try {
							child = KSystem.construct(this.data.feed[link], this);
						}
						catch (e) {
							continue;
						}
						if (!!this.data.feed[link].data) {
							child.data = this.data.feed[link].data;
						}
						this.appendChild(child, (!!this.data.feed[link].target ? this.data.feed[link].target : null));
					}
				}
			}
			if (!!this.data.children && !this.no_children) {
				for ( var link in this.data.children.elements) {
					var jsClass = KSystem.getWRMLClass(this.data.children.elements[link], 'application/vnd.kinky.');
					if (!!jsClass && !KSystem.isAddOn(jsClass)) {
						var child = null;
						try {
							child = KSystem.construct(this.data.children.elements[link], this);
						}
						catch (e) {
							continue;
						}
						if (!!this.data.children.elements[link].embedded) {
							child.data = this.data.children.elements[link];
						}
						this.appendChild(child, (!!this.data.children.elements[link].target ? this.data.children.elements[link].target : null));
					}
				}
			}
		}
		for ( var child in this.childWidgets()) {
			var w = this.childWidget(child);
			if (w.silent) {
				this.loadedChildren++;
				this.onChildLoaded(w);
			}
			if (!w.gone) {
				w.go();
			}
		}
		if (this.nChildren == 0) {
			this.activate();
		}
	};

	KWidget.prototype.cleanCache = function() {
		KCache.find(new RegExp(this.ref + '/children(.*)'), KCache.clear);
		KCache.find(new RegExp(this.ref + '/feed(.*)'), KCache.clear);
		KCache.find(new RegExp(this.ref + '/data(.*)'), KCache.clear);
		KCache.find(new RegExp(this.ref + '/config(.*)'), KCache.clear);
		KCache.clear(this.ref + '/children');
		KCache.clear(this.ref + '/feed');
		KCache.clear(this.ref + '/data');
		KCache.clear(this.ref + '/config');
		delete this.data;

		delete this.config;
		for ( var w in this.children) {
			this.children[w].cleanCache();
		}
	};

	KWidget.prototype.redraw = function() {
		this.clear();
		for ( var element in this.childWidgets()) {
			this.content.appendChild(this.childWidget(element).panel);
		}
		this.draw();
	};

	KWidget.prototype.refresh = function() {
		/*var config = this.config;
		var parent = this.parent;
		var sibling = this.parent.childAt(this.parent.indexOf(this) + 1);

		this.cleanCache();
		this.unload();
		return KWidget.cloneWidget(config, parent, sibling);*/
		throw new Error('kinky warning: widget "' + this.className + '" must implement the refresh() function.');
	};

	KWidget.prototype.getParent = function(parentClass) {
		if (parentClass) {
			if (this.parent == null) {
				return null;
			}
			else if (this.parent.className) {
				var isParent = false;
				eval('isParent = this.parent instanceof ' + parentClass + ';');
				if (isParent) {
					return this.parent;
				}
				else {
					return this.parent.getParent(parentClass);
				}
			}
			else {
				return undefined;
			}
		}
		else {
			return this.parent;
		}
	};

	KWidget.prototype.getURL = function() {
		return KBreadcrumb.getURL();
	};

	KWidget.prototype.getHash = function() {
		return KBreadcrumb.getHash();
	};

	KWidget.prototype.getQuery = function() {
		return KBreadcrumb.getQuery();
	};

	KWidget.prototype.getAction = function() {
		return KBreadcrumb.getAction();
	};

	KWidget.prototype.retain = function(args) {
		this._original = args;
	};

	KWidget.hashToRegex = function(hash) {
		var regex = hash.split(/\{([^\}]+)\}/);
		var result = '';
		for ( var part in regex) {
			if (part % 2 == 0) {
				result += regex[part];
			}
			else {
				result += '([^/]+)';
			}
		}
		return result;
	};


	KWidget.prototype.clone = function() {
		if (!this._original) {
			throw new Error('kinky warning: widget must invoke "this.retain(arguments)" method in the constructor, in order to use the "this.clone()" method.');
		}

		var child = null;
		var toeval = 'child = new ' + this.className + '(this.parent';
		for (var i in this._original) {
			toeval += ', this._original[' + i + ']';
		}
		toeval += ')';

		/*
		var sibling = this.parent.childAt(this.parent.indexOf(this) + 1);
		if (!sibling) {
			parent.appendChild(child, (!!config.target ? config.target : null));
		}
		else {
			parent.insertBefore(child, sibling, (!!config.target ? config.target : null));
		}
		child.go();*/
		eval(toeval);
		return child;
	};

	KWidget.replace = function(self, other) {
		var sibling = self.parent.childAt(self.parent.indexOf(self) + 1);
		if (!sibling) {
			self.parent.appendChild(other, (!!other.config && !!other.config.target ? other.config.target : null));
		}
		else {
			self.parent.insertBefore(other, sibling, (!!other.config && !!config.target ? other.config.target : null));
		}
		self.destroy();
		other.go();
	}

	KWidget.ROOT_DIV = 'panel';
	KWidget.CONTENT_DIV = 'content';

	KWidget.STATE_FUNCTIONS = [
		"go",
		"load",
		"draw",
		"focus",
		"blur",
		"enable",
		"disable",
		"visible",
		"invisible",
		"abort",
		"onLoad",
		"onSave",
		"onRemove",
		"onPost",
		"onPut",
		"onError",
		"onOk",
		"onHead",
		"onCreated",
		"onAccepted",
		"onNoContent",
		"onBadRequest",
		"onUnauthorized",
		"onForbidden",
		"onNotFound",
		"onMethodNotAllowed",
		"onPreConditionFailed",
		"onInternalError"
	];

	KWidget.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div'
		},
		html5 : {
			panel : 'section',
			content : 'div',
			background : 'div'
		}
	};

	KSystem.included('KWidget');
});