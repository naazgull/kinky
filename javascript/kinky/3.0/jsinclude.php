<?php

function exportJS($root, array $packages = null) {
	if (isset($packages)) {
		includePackages($root, $packages, true);
	}
	else {
		includeFolder($root, null, true);
	}
}

function includeJS($root, array $packages = null) {
	if (isset($packages)) {
		includePackages($root, $packages);
	}
	else {
		includeFolder($root);
	}
}

function includePackages($root, array $packages, $pack = false) {
	$allowed = array();
	foreach ($packages as $package) {
		$package = str_replace('kinky', '', str_replace('.', '/*.', $package));
		$dir = glob($root . substr($package, 1, strlen($package)));
		$dir = $dir[0];
		$allowed[] = $dir;
		$allowed[] = $dir . ($dir{strlen($dir) - 1} != '/' ? '/' : '');
	}
	includeFolder($root, $allowed, $pack);
}

function includeFolder($root, $allowed = null, $pack = false) {
	$files = glob($root . ($root{strlen($root) - 1} != '/' ? '/' : '') . '*');
	foreach ($files as $file) {
		if (strpos($file, '.') !== 0 && file_exists($file) && is_dir($file) && (!isset($allowed) || array_search($file, $allowed) !== false)) {
			includeFolder($file, $allowed, $pack);
		}
	}
	$files = glob($root . ($root{strlen($root) - 1} != '/' ? '/' : '') . '*.js');
	foreach ($files as $file) {
		$fileContent = file_get_contents($file);
		if ($pack) {
			echo preg_replace('/[\n\r\f\t]/m', '', $fileContent);
		}
		else {
			echo $fileContent;
			echo "\n";
		}
	}
}

function includeJSClass($jsClass, $baseDir) {
	$root = dirname(__FILE__);
	$js = searchJSClass($root, $jsClass);
	if (!isset($js)) {
		$js = searchJSClass($baseDir, $jsClass);
	}
	echo file_get_contents($js);
}

function searchJSClass($root, $jsClass) {
	$items = glob($root . '/*');

	for ($i = 0; $i < count($items); $i++) {
		if (is_dir($items[$i])) {
			$add = glob($items[$i] . '/*');
			$items = array_merge($items, $add);
		}
		else if (basename($items[$i]) == $jsClass) {
			return $items[$i];
		}
	}
	return null;
}