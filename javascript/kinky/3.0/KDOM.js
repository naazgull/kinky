function KDOM() {
}
{
	KDOM.prototype = {};
	KDOM.prototype.constructor = KDOM;
	
	KDOM.removeEventListener = function(element, eventName, handler) {
		if (element.detachEvent) {
			element.detachEvent('on' + eventName, handler);
		}
		else {
			element.removeEventListener(eventName, handler, false);
		}
	};
	
	KDOM.addEventListener = function(element, eventName, handler) {
		if (element.attachEvent) {
			element.attachEvent('on' + eventName, handler);
		}
		else {
			element.addEventListener(eventName, handler, false);
		}
	};
	
	KDOM.fireEvent = function(element, eventName) {
		if (element.fireEvent) {
			if (element.fireEvent) {
				try {
					element.fireEvent('on' + eventName);
				}
				catch (e) {
				}
			}
			else {
				debug('No fireevent for ' + element.tagName);
			}
		}
		else {
			var evt = document.createEvent("HTMLEvents");
			evt.initEvent(eventName, true, true);
			return !element.dispatchEvent(evt);
		}
	};
	
	KDOM.stopEvent = function(event) {
		if (event.preventDefault) {
			event.preventDefault();
		}
		event.returnValue = false;
		if (event.stopPropagation) {
			event.stopPropagation();
		}
		event.cancelBubble = true;
	};
	
	KDOM.getEventTarget = function(event) {
		event = event || window.event;
		
		if (event == null) {
			return null;
		}
		
		return event.target || event.srcElement;
	};
	
	KDOM.getEventCurrentTarget = function(event) {
		event = event || window.event;
		
		if (event == null) {
			return null;
		}
		
		return event.currentTarget || event.srcElement;
	};
	
	KDOM.isRelatedTarget = function(event, parent, mouseOut) {
		var mouseEvent = KDOM.getEvent(event);
		var element = null;
		if (mouseOut) {
			element = mouseEvent.toElement || (mouseEvent.relatedTarget && mouseEvent.relatedTarget.nodeName ? mouseEvent.relatedTarget : null);
		}
		else {
			element = mouseEvent.fromElement || (mouseEvent.relatedTarget && mouseEvent.relatedTarget.nodeName ? mouseEvent.relatedTarget : null);
		}
		if (!element) {
			return false;
		}
		if (element.id == parent.id) {
			return true;
		}
		if ((element != parent) && (element != null) && (parent != null)) {
			var nodes = parent.getElementsByTagName(element.tagName);
			for ( var i = 0; i != nodes.length; i++) {
				if (nodes[i].id == element.id) {
					return true;
				}
			}
		}
		return false;
	};
	
	KDOM.getMouseCoords = function(event) {
// var posx = 0;
// var posy = 0;
		event = event || window.event;
// if (event.pageX || event.pageY) {
// posx = event.pageX;
// posy = event.pageY;
// }
// else if (event.clientX || event.clientY) {
// posx = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
// posy = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
// }
		return {
			x : event.clientX,
			y : event.clientY
		};
		
	};
	
	KDOM.getEventRelatedTarget = function(event, mouseOut) {
		var mouseEvent = KDOM.getEvent(event);
		var element = null;
		if (mouseOut) {
			element = mouseEvent.toElement || (mouseEvent.relatedTarget && mouseEvent.relatedTarget.nodeName ? mouseEvent.relatedTarget : null);
		}
		else {
			element = mouseEvent.fromElement || (mouseEvent.relatedTarget && mouseEvent.relatedTarget.nodeName ? mouseEvent.relatedTarget : null);
		}
		return element;
	};
	
	KDOM.getEvent = function(event) {
		var toReturn = null;
		toReturn = event || window.event;
		return toReturn;
	};
	
	KDOM.getWidget = function(element, className) {
		var toReturn = element;
		
		if (className != null) {
			while (toReturn != null) {
				if (/^widget([0-9]+)$/.test(toReturn.id)) {
					var object = Kinky.bunnyMan.widgets[toReturn.id];
					if (object) {
						var isParent = false;
						eval('isParent = object instanceof ' + className + ';');
						if (isParent) {
							break;
						}
					}
				}
				toReturn = toReturn.parentNode;
			}
		}
		else {
			try {
				while (!/^widget([0-9]+)$/.test(toReturn.id)) {
					toReturn = toReturn.parentNode;
				}
			}
			catch (e) {
				return null;
			}
		}
		if (toReturn) {
			return Kinky.getWidget(toReturn.id);
		}
		return null;
	};
	
	KDOM.getEventWidget = function(event, className) {
		event = event || window.event;
		if (event == null) {
			return null;
		}
		var element = event.target || event.srcElement;
		if (className != null) {
			while (element != null) {
				if (/^widget([0-9]+)$/.test(element.id)) {
					var object = Kinky.bunnyMan.widgets[element.id];
					if (object) {
						var isParent = false;
						eval('isParent = object instanceof ' + className + ';');
						if (isParent) {
							break;
						}
					}
				}
				element = element.parentNode;
			}
		}
		else {
			while ((element != null) && !/^widget([0-9]+)$/.test(element.id)) {
				element = element.parentNode;
			}
		}
		if (!element) {
			// throw new Error('KDOM', "No Widget for element");
			return null;
		}
		return Kinky.bunnyMan.widgets[element.id];
	};
	
	KDOM.normalizePixelValue = function(value) {
		var v = value.replace(/px/g, '');
		var ret = parseInt(v);
		if (isNaN(ret)) {
			ret = parseFloat(v);
			if (isNaN(ret)) {
				return undefined;
			}
		}
		return ret;
	};
	
	KDOM.extractColorInfo = function(value) {
		try {
			if (value.substr(0, 1) === '#') {
				var info = new Object();
				
				var rgb_from = parseInt(value.replace("#", ""), 16);
				
				var red = (rgb_from & (255 << 16)) >> 16;
				var green = (rgb_from & (255 << 8)) >> 8;
				var blue = (rgb_from & 255) >> 0;
				
				info.red = red;
				info.green = green;
				info.blue = blue;
				
				return info;
				
			}
			else if (value.substr(0, 4) === 'rgb(') {
				var info = new Object();
				
				var digits = /(.*?)rgb\((\d+),(\d+),(\d+)\)/.exec(value.replace(/ /g, ''));
				var red = parseInt(digits[2]);
				var green = parseInt(digits[3]);
				var blue = parseInt(digits[4]);
				
				info.red = red;
				info.green = green;
				info.blue = blue;
				
				return info;
				
			}
			
		}
		catch (e) {
			alert(value + ": " + e.message);
		}
	};
	
	KDOM.getBrowserHeight = function() {
		if (window.innerHeight) {
			return window.innerHeight;
		}
		else {
			if (window.document.documentElement && window.document.documentElement.clientHeight) {
				return window.document.documentElement.clientHeight;
			}
			else if (window.document.body) {
				return document.body.clientHeight;
			}
		}
		return 0;
	};
	
	KDOM.getBrowserWidth = function() {
		if (window.document.documentElement && window.document.documentElement.clientWidth) {
			return window.document.documentElement.clientWidth;
		}
		else if (window.document.body) {
			return document.body.clientWidth;
		}
		else if (window.innerWidth) {
			return window.innerWidth;
		}
		
		return 0;
	};
	
	KDOM.mouseX = function(event) {
		if (event.pageX) {
			return event.pageX;
		}
		else if (event.clientX) {
			return event.clientX + (window.document.documentElement.scrollLeft ? window.document.documentElement.scrollLeft : window.document.body.scrollLeft);
		}
		else {
			return null;
		}
	};
	
	KDOM.mouseY = function(event) {
		if (event.pageY) {
			return event.pageY;
		}
		else if (event.clientY) {
			return event.clientY + (window.document.documentElement.scrollTop ? window.document.documentElement.scrollTop : window.document.body.scrollTop);
		}
		else {
			return null;
		}
	};

	KDOM.offsetTop = function(parent) {
		var toReturn = 0;
		while (parent != null) {
			toReturn += parent.offsetTop;
			parent = parent.offsetParent;
		}
		return toReturn;
	};

	KDOM.offsetLeft = function(parent) {
		var toReturn = 0;
		while (parent != null) {
			toReturn += parent.offsetLeft;
			if (((KBrowserDetect.browser == 2)) && ((KBrowserDetect.version == 7))) {
				parent = parent.offsetParent;
			}
			else {
				parent = parent.offsetParent;
			}
		}
		return toReturn;
	};

	KDOM.getWidth = function(target) {
		return target.offsetWidth;
	};

	KDOM.getHeight = function(target) {
		return target.offsetHeight;
	};

	KDOM.scrollTop = function(parent) {
		var toReturn = 0;
		while (parent != null) {
			if ((parent.nodeName.toLowerCase() != 'html') && parent.scrollTop !== undefined) {
				toReturn += parent.scrollTop;
			}
			parent = parent.parentNode;
		}
		if (!!window.document.body.style.overflow && ((window.document.body.style.overflow == 'hidden'))) {
			return toReturn - (!!window.pageYOffset ? window.pageYOffset : 0);
		}
		return toReturn;
	};
	
	KDOM.moveToEndOfContenteditable = function(contentEditableElement){
		contentEditableElement = contentEditableElement.lastChild;
		if (!contentEditableElement) {
			return;
		}

		var range,selection;
		if(document.createRange) {    
			range = document.createRange();
			range.selectNodeContents(contentEditableElement);
			range.collapse(false);
			selection = window.getSelection();
			selection.removeAllRanges();
			selection.addRange(range);
		}
		else if(document.selection) { 
			range = document.body.createTextRange();
			range.moveToElementText(contentEditableElement);
			range.collapse(false);
			range.select();
		}
	}

	KSystem.included('KDOM');
}
