function KOAuth() {
}
{
	KOAuth.prototype = {};
	KOAuth.prototype.constructor = KOAuth;
	
	KOAuth.start = function(shellName, loadShell) {
		var service = Kinky.SHELL_CONFIG[shellName].auth.service;
		var href = (!!Kinky.SHELL_CONFIG[shellName].id ? Kinky.SHELL_CONFIG[shellName].id : shellName);
		
		var authHeader = KOAuth.getHTTPAuthorizationHeader();
		if (!authHeader) {
			authHeader = KOAuth.getHTTPAuthorizationHeader(shellName);
		}
		var request = {
			type : service.substring(0, service.indexOf(':')),
			namespace : service.substring(service.indexOf(':') + 1, service.lastIndexOf(':')),
			service : service.substring(service.lastIndexOf(':') + 1),
			action : 'get',
			id : 'picker',
			flag : loadShell,
			shell : shellName,
			callback : [
				'KOAuth.lockPicker.onNegotiate'
			]
		};
		try {
			KConnectionsPool.getConnection(request.type, href).send(request, null, authHeader);
		}
		catch (e) {
			KSystem.include(KConnectionsPool.connectors[request.type], function() {
				KConnectionsPool.getConnection(request.type, href).send(request, null, authHeader);
			});
		}
	};
	
	KOAuth.destroy = function(shellName, type) {
		if (!type) {
			type = 'KINKY';
		}
		if (!shellName) {
			shellName = 'GLOBAL';
		}
		else {
			shellName = shellName.replace(/\//g, '_');
		}
		if(typeof(Storage)!=="undefined") {
			delete localStorage[type + '_' + shellName + '_AUTH'];
			delete localStorage[type + '_' + shellName + '_CREDENTIALS'];
		}
		else {
			KSystem.removeCookie(type + '_' + shellName + '_AUTH');
			KSystem.removeCookie(type + '_' + shellName + '_CREDENTIALS');
		}
	};
	
	KOAuth.session = function(authData, request, shellName, type) {
		if (!type) {
			type = 'KINKY';
		}
		if (!shellName) {
			shellName = 'GLOBAL';
		}
		else {
			shellName = shellName.replace(/\//g, '_');
		}
		if(typeof(Storage)!=="undefined") {
			localStorage[type + '_' + shellName + '_AUTH'] = JSON.stringify(authData);
			localStorage[type + '_' + shellName + '_CREDENTIALS'] = JSON.stringify(request);
		}
		else {
			KSystem.setCookie(type + '_' + shellName + '_AUTH', authData);
			KSystem.setCookie(type + '_' + shellName + '_CREDENTIALS', request);
		}
	};
	
	KOAuth.getAuthorization = function(shellName, type) {
		if (!type) {
			type = 'KINKY';
		}
		if (!shellName) {
			shellName = 'GLOBAL';
		}
		else {
			shellName = shellName.replace(/\//g, '_');
		}
		var authCookie = null;
		if(typeof(Storage)!=="undefined") {
			try {
				authCookie = JSON.parse(localStorage[type + '_' + shellName + '_AUTH']);
			}
			catch(e) {
				authCookie = null;
			}
		}
		else {
			authCookie = KSystem.getCookie(type + '_' + shellName + '_AUTH');
		}
		if (!authCookie || !authCookie.authentication) {
			return null;
		}
		return authCookie;
	};
	
	KOAuth.getToken = function(shellName, type) {
		var auth = this.getAuthorization(shellName, type);
		if (!auth || !auth.authentication) {
			return null;
		}
		return auth.authentication.replace('OAuth2.0 ', '');
	};
	
	KOAuth.getHTTPAuthorizationHeader = function(shellName, type) {
		if (!type) {
			type = 'KINKY';
		}
		if (!shellName) {
			shellName = 'GLOBAL';
		}
		else {
			shellName = shellName.replace(/\//g, '_');
		}
		var headers = null;
		var authCookie = KOAuth.getAuthorization(shellName, type);
		if (!!authCookie) {
			headers = {
				"Authorization" : authCookie.authentication
			};
		}
		return headers;
	};
	
	KOAuth.prototype.onLoad = function(data, request) {
	};
	
	KOAuth.prototype.onSave = function(data, request) {
	};
	
	KOAuth.prototype.onRemove = function(data, request) {
	};
	
	KOAuth.prototype.onPost = function(data, request) {
	};
	
	KOAuth.prototype.onPut = function(data, request) {
	};
	
	KOAuth.prototype.onCreated = function(data, request) {
		this.onNegotiate(data, request);
	};
	
	KOAuth.prototype.onAccepted = function(data, request) {
		this.onNegotiate(data, request);
	};
	
	KOAuth.prototype.onNoContent = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onBadRequest = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onUnauthorized = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onForbidden = function(data, request) {
		this.onNegotiate(data, request);
	};
	
	KOAuth.prototype.onNotFound = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onMethodNotAllowed = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onInternalError = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onError = function(data, request) {
		if (!!Kinky.SHELL_CONFIG[request.shell].auth && !!Kinky.SHELL_CONFIG[request.shell].auth.callback && !!Kinky.SHELL_CONFIG[request.shell].auth.callback.fail) {
			Kinky.SHELL_CONFIG[request.shell].auth.callback.fail.call(data);
		}
		
		if (request.flag) {
			Kinky.loadShell(Kinky.SHELL_CONFIG[request.shell]);
		}
		else {
			Kinky.SHELL_CONFIG[request.shell].href = request.shell;
			Kinky.SHELL_CONFIG[request.shell].requestTypes = [
				'application/vnd.kinky.' + Kinky.SHELL_CONFIG[request.shell].className
			];
			Kinky.bunnyMan.start(Kinky.SHELL_CONFIG[request.shell]);
		}
	};
	
	KOAuth.prototype.onNegotiate = function(data, request) {
		Kinky.startSession(data);
		if (!!Kinky.SHELL_CONFIG[request.shell].auth && !!Kinky.SHELL_CONFIG[request.shell].auth.callback && !!Kinky.SHELL_CONFIG[request.shell].auth.callback.success) {
			Kinky.SHELL_CONFIG[request.shell].auth.callback.success.call(data);
		}
		
		if (request.flag) {
			Kinky.loadShell(Kinky.SHELL_CONFIG[request.shell]);
		}
		else {
			Kinky.SHELL_CONFIG[request.shell].id = request.shell;
			Kinky.SHELL_CONFIG[request.shell].requestTypes = [
				'application/vnd.kinky.' + Kinky.SHELL_CONFIG[request.shell].className
			];
			Kinky.bunnyMan.start(Kinky.SHELL_CONFIG[request.shell]);
		}
	};
	
	KOAuth.lockPicker = new KOAuth();
	KOAuth.PROTOCOL_VERSION = '2.0';
	KOAuth.ACCESS_TOKEN_AS_VAR = false;
	
	KSystem.included('KOAuth');
}
