function KConnection(url, dataURL, headers) {
	if (!!url && !!dataURL) {
		this.urlBase = url;
		this.dataUrlBase = dataURL;
		this.headers = headers;
		this.shell = null;
	}
}
{
	KConnection.prototype.connect = function(request, callback) {
		var channel = this.openChannel();
		var connection = this;
		channel.onreadystatechange = function() {
			connection.receive(request, channel);
		};
		return channel;
	};
	
	KConnection.prototype.setURL = function(url) {
		this.urlBase = url;
	};
	
	KConnection.prototype.setDataURL = function(url) {
		this.dataUrlBase = url;
	};
	
	KConnection.prototype.getServiceParams = function(request, params) {
		if (request) {
			if (params) {
				for ( var paramName in params) {
					request[paramName] = params[paramName];
				}
			}
			return 'id=' + request.id + '&request=' + encodeURIComponent(JSON.stringify(request));
			
		}
		return null;
	};
	
	KConnection.prototype.send = function(request, params, user, password) {
		throw new KNoExtensionException(this, 'send');
	};
	
	KConnection.prototype.openChannel = function() {
		return KConnection.createAJAXChannel();
	};
	
	KConnection.createAJAXChannel = function() {
		var channel = null;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			channel = new window.XMLHttpRequest();
		}
		else if (window.ActiveXObject) { // IE
			try {
				channel = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) {
				try {
					channel = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1) {
				}
			}
		}
		return channel;
	};
	
	KConnection.prototype.setHeaders = function(headers, channel) {
		var merged = {};
		for ( var header in this.headers) {
			merged[header] = this.headers[header];
		}
		for ( var header in headers) {
			merged[header] = headers[header];
		}
		for ( var header in merged) {
			channel.setRequestHeader(header, merged[header]);
		}
	};
	
	KConnection.getBasicHTTPAuthentication = function(user, password) {
		var tok = user + ':' + password;
		var hash = Base64.encode(tok);
		return "Basic " + hash;
	};
	
	KConnection.getOAuthHTTPAuthentication = function(access_token) {
		access_token = access_token || KOAuth.getAccessToken();
		if (access_token) {
			return (/OAuth2.0 /.test(access_token) ? access_token : "OAuth2.0 " + access_token);
		}
		else {
			return null;
		}
	};
	
	KConnection.prototype.receive = function(request, ajaxResponse) {
		if (ajaxResponse.readyState == 4) {
			if (ajaxResponse.getResponseHeader('X-Access-Token')) {
				// REMOVED DUE TO PROBLEMS WHEN CONNECTING TO SEVERAL BACKENDS -> DO NOT UNCOMMENT
				/*KOAuth.session({
					authentication : ajaxResponse.getResponseHeader('X-Access-Token')
				}, request, this.shell);*/
			}
			var widget = Kinky.getWidget(request.id);
			
			if (widget) {
				if (ajaxResponse.status == 200) {
					var response = null;
					var contentType = ajaxResponse.getResponseHeader('Content-Type');
					
					if (contentType && ((contentType.indexOf('text/plain') != -1) || (contentType.indexOf('javascript') != -1) || (contentType.indexOf('json') != -1))) {
						try {
							eval('response = ' + ajaxResponse.responseText + ';');
							response.http = new Object();
							response.http.status = 200;
						}
						catch (e) {
							if ((ajaxResponse.responseText == null) || (ajaxResponse.responseText == '')) {
								response = {
									http : {
										status : 200
									}
								};
							}
							else {
								response = {
									http : {
										status : -1
									},
									code : ajaxResponse.status,
									message : "Could not parse string:\n" + ajaxResponse.responseText
								};
							}
						}
					}
					else {
						response = {
							http : {
								status : 200
							},
							text : ajaxResponse.responseText,
							type : contentType
						};
					}
					
					this.includeHeaders(ajaxResponse, response);
					if (!!response.http.headers.Location) {
						if (!!response.links) {
							response.links.redirect = new Object();
							response.links.redirect.href = response.http.headers.Location;
						}
						else {
							response.links = {
								redirect : {
									href : response.http.headers.Location
								}
							};
						}
					}
					
					if (response.http.status && (response.http.status != 200)) {
						this.processNonOk(widget, response, request);
					}
					else {
						Kinky.startSession(request.user, request.secret, response.user || Kinky.bunnyMan.loggedUser);
						
						for ( var callback in request.callback) {
							eval(request.callback[callback] + '(response, request);');
						}
						
						delete request;
					}
				}
				else {
					var response = null;
					var contentType = ajaxResponse.getResponseHeader('Content-Type');
					
					if (contentType && ((contentType.indexOf('text/plain') != -1) || (contentType.indexOf('json') != -1))) {
						try {
							eval('response = ' + ajaxResponse.responseText + ';');
							response.http = new Object();
							response.http.status = ajaxResponse.status;
						}
						catch (e) {
							if ((ajaxResponse.responseText == null) || (ajaxResponse.responseText == '')) {
								response = {
									http : {
										status : ajaxResponse.status
									}
								};
							}
							else {
								response = {
									http : {
										status : ajaxResponse.status
									},
									code : ajaxResponse.status,
									message : "Could not parse string:\n" + ajaxResponse.responseText
								};
							}
						}
					}
					else {
						response = {
							http : {
								status : ajaxResponse.status
							},
							text : ajaxResponse.responseText,
							type : contentType
						};
					}
					
					this.includeHeaders(ajaxResponse, response);
					
					delete request;
					if (!!response.http.headers.Location) {
						if (!!response.links) {
							response.links.redirect = new Object();
							response.links.redirect.href = response.http.headers.Location;
						}
						else {
							response.links = {
								redirect : {
									href : response.http.headers.Location
								}
							};
						}
					}
					
					response.http.status = response.http.status || ajaxResponse.status;
					this.processNonOk(widget, response, request);
				}
				if (widget && widget.resume) {
					widget.resume(true);
				}
			}
			else {
				Kinky.getShell(this.shell).usingLoader--;
				if ((Kinky.getShell(this.shell).usingLoader <= 0) && Kinky.getShell(this.shell).loader) {
					KCSS.setStyle({
						display : 'none'
					}, [
						Kinky.getShell(this.shell).loader
					]);
				}
			}
		}
	};
	
	KConnection.prototype.includeHeaders = function(ajaxResponse, response) {
		response.http.statusLine = ajaxResponse.status + ' ' + ajaxResponse.statusText + ' HTTP/1.1';
		var headers = [
			"X-Access-Token",
			"X-Access-Token-Expires",
			"X-Error-Reason",
			"X-Error",
			"Cache-Control",
			"Content-Length",
			"Content-Type",
			"Content-Language",
			"Expires",
			"Location"
		];
		response.http.headers = {};
		for ( var header in headers) {
			var headerVal = ajaxResponse.getResponseHeader(headers[header]);
			if (headerVal) {
				response.http.headers[headers[header]] = headerVal;
			}
		}
	};
	
	KConnection.prototype.processNonOk = function(widget, nonOk, request) {
		if (widget && widget.resume) {
			widget.resume(true);
		}

		switch (nonOk.http.status) {
			case 200: {
				break;
			}
			case 302: {
				break;
			}
			case 303: {
				alert(JSON.stringify(nonOk));
				window.document.location = nonOk.links.redirect.href;
				break;
			}
			case 307: {
				var regex = new RegExp(Kinky.SITE_URL);
				if (!/http([s]*):\/\//.test(nonOk.links.redirect.href) || regex.test(nonOk.links.redirect.href)) {
					KBreadcrumb.dispatchURL({
						url : nonOk.links.redirect.href.replace(Kinky.SITE_URL, '').replace('#', '')
					});
				}
				else {
					var url = encodeURIComponent(window.document.location.href);
					window.document.location = nonOk.links.redirect.href + (/\?/.test(nonOk.links.redirect.href) ? '&' : '?') + 'redirect_uri=' + url;
				}
				break;
			}
			case 201: {
				widget.onCreated(nonOk, request);
				break;
			}
			case 202: {
				widget.onAccepted(nonOk, request);
				break;
			}
			case 1223:
			case 204: {
				widget.onNoContent(nonOk, request);
				break;
			}
			case 400: {
				nonOk.error = true;
				widget.onBadRequest(nonOk, request);
				break;
			}
			case 401: {
				nonOk.error = true;
				widget.onUnauthorized(nonOk, request);
				break;
			}
			case 403: {
				nonOk.error = true;
				widget.onForbidden(nonOk, request);
				break;
			}
			case 404: {
				nonOk.error = true;
				widget.onNotFound(nonOk, request);
				break;
			}
			case 405: {
				nonOk.error = true;
				widget.onMethodNotAllowed(nonOk, request);
				break;
			}
			case 412: {
				nonOk.error = true;
				widget.onPreConditionFailed(nonOk, request);
				break;
			}
			case 500: {
				nonOk.error = true;
				widget.onInternalError(nonOk, request);
				break;
			}
			default: {
				nonOk.error = true;
				widget.onError(nonOk, request);
				break;
			}
		}
	};
	
	KSystem.included('KConnection');
}