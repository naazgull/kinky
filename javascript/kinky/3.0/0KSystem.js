function KSystem() {
}
{
	KSystem.prototype = {};
	KSystem.prototype.constructor = KSystem;
	
	KSystem.construct = function(jsonObject, parent, throwException) {
		var jsClass = jsonObject.jsClass;
		
		if (!jsClass) {
			jsClass = KSystem.getWRMLClass(jsonObject);
		}
		if (!!jsClass) {
			var object = null;
			var first = true;
			var newString = 'object = new ' + jsClass + "(";
			if (!!parent) {
				newString += 'parent';
				first = false;
			}
			
			for ( var arg in jsonObject.args) {
				if (!first) {
					newString += ', ';
				}
				first = false;
				newString += JSON.stringify(jsonObject.args[arg]);
			}
			newString += ');';
			eval(newString);
			
			object.config = jsonObject;
			if (object.data == null) {
				if (!!object.config.embedded) {
					object.data = object.config;
				}
				else {
					object.data = new Object();
				}
			}
			
			if ((object.config.hash != null) && (object.hash == null)) {
				object.hash = object.config.hash;
			}
			return object;
		}
		else {
			throw new Error('KSystem: can\'t create KWidget from empty source (parentID=\'' + parent.id + '\', parentClass=\'' + KSystem.getObjectClass(parent) + '\', parentHash=\'' + parent.hash + '\').');
		}
	};
	
	KSystem.getWRMLClass = function(wrmlConf, mimePrefix) {
		mimePrefix = (!!mimePrefix ? mimePrefix : 'application/vnd.kinky.');
		var jsClass = null;
		var types = null;
		
		if (wrmlConf.requestTypes) {
			types = wrmlConf.requestTypes;
		}
		else if (wrmlConf.responseTypes) {
			types = wrmlConf.responseTypes;
		}
		
		if (!!types) {
			for ( var type in types) {
				var regex = new RegExp(mimePrefix.replace(/\//, '\\/'));
				if (regex.test(types[type])) {
					var part = types[type].split(regex);
					part = (part.length > 1 ? part[1] : part[0]);
					part = part.split(/\+/);
					jsClass = part[0];
					if (part.length > 1) {
						wrmlConf.args = (!!wrmlConf.args ? wrmlConf.args : []);
						for ( var i = 1; i != part.length; i++) {
							wrmlConf.args.push(part[i]);
						}
					}
					
				}
			}
		}
		return jsClass;
	};
	
	KSystem.getWRMLSchema = function(wrmlConf, mimePrefix) {
		mimePrefix = (!!mimePrefix ? mimePrefix : 'application/wrml;schema="');
		var schemaClass = null;
		var types = null;
		
		if (wrmlConf.requestTypes) {
			types = wrmlConf.requestTypes;
		}
		else if (wrmlConf.responseTypes) {
			types = wrmlConf.responseTypes;
		}
		
		if (!!types) {
			for ( var type in types) {
				var regex = new RegExp(mimePrefix);
				if (regex.test(types[type])) {
					var part = types[type].split(regex)[1];
					part = part.split(/"/);
					schemaClass = part[0];
				}
			}
		}
		return schemaClass;
	};
	
	KSystem.getWRMLForm = function(wrmlSchema, prefix) {
		var jsClass = null;
		var types = wrmlSchema.stateFacts;
		
		if (!!types) {
			for ( var type in types) {
				if (types[type].indexOf("Form(") != -1) {
					jsClass = types[type].split("Form(")[1].split(")")[0];
					jsClass = jsClass.replace(prefix, '');
				}
			}
		}
		return jsClass;
	};
	
	KSystem.include = function(jsClass, callback, includer) {
		if (!(jsClass instanceof Array)) {
			jsClass = [
				jsClass
			];
		}
		for ( var index in jsClass) {
			if (!KSystem.includes[jsClass[index]]) {
				var include = window.document.createElement('script');
				KSystem.includes[jsClass[index]] = {
					include : include
				};
				var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
				include.charset = 'utf-8';
				include.src = url;
				window.document.getElementsByTagName('head')[0].appendChild(include);
			}
		}
		if (callback) {
			KSystem.addTimer(function() {
				KSystem.includeWait(callback, jsClass);
			}, 20);
		}
	};
	
	KSystem.isIncluded = function(jsClass) {
		return !!KSystem.includes[jsClass] && !!KSystem.includes[jsClass].finished;
	};
	
	KSystem.includeWait = function(callback, jsClass) {
		for ( var index in jsClass) {
			if (!KSystem.includes[jsClass[index]].finished) {
				KSystem.addTimer(function() {
					KSystem.includeWait(callback, jsClass);
				}, 20);
				return;
			}
		}
		callback();
		delete jsClass;
	};

	KSystem.hasPendingImports = function() {
		for ( var index in KSystem.includes) {
			if (!KSystem.includes[index].finished) {
				return true;
			}
		}
		return false;
	};
	
	KSystem.included = function(id) {
		if (!KSystem.includes[id]) {
			KSystem.includes[id] = {
				finished : true
			};
		}

		if (!KSystem.includes[id].finished) {
			if (!!KSystem.includes[id].include) {
				window.document.getElementsByTagName('head')[0].removeChild(KSystem.includes[id].include);
				delete KSystem.includes[id].include;
			}
			KSystem.includes[id].finished = true;
		}
		
		if (KSystem.isIncluded('KWidget')) {
			var isFromKinky = false;
			try {
				eval('isFromKinky = typeof ' + id + ' !== \'undefined\'');
			}
			catch (e) {
				isFromKinky = false;
			}
			if (isFromKinky) {
				for ( var func in KWidget.STATE_FUNCTIONS) {
					var exists = false;
					eval('exists = !!' + id + '.prototype && !!' + id + '.prototype.' + KWidget.STATE_FUNCTIONS[func] + ';');
					if (exists) {
						eval(id + '.prototype.default_' + KWidget.STATE_FUNCTIONS[func] + ' = ' + id + '.prototype.' + KWidget.STATE_FUNCTIONS[func] + ';');
					}
				}
			}
		}
		
		if (KSystem.isIncluded('KAddOn') && KSystem.isAddOn(id)) {
			var isFromKinky = false;
			try {
				eval('isFromKinky = typeof ' + id + ' !== \'undefined\'');
			}
			catch (e) {
				isFromKinky = false;
			}
			if (isFromKinky) {
				for ( var func in KAddOn.STATE_FUNCTIONS) {
					var exists = false;
					eval('exists = !!' + id + '.prototype.' + KAddOn.STATE_FUNCTIONS[func] + ';');
					if (exists) {
						eval(id + '.prototype.default_' + KAddOn.STATE_FUNCTIONS[func] + ' = ' + id + '.prototype.' + KAddOn.STATE_FUNCTIONS[func] + ';');
					}
				}
			}
		}
		
	};
	
	KSystem.urlify = function(url, params) {
		var uri = '';
		var first = true;
		for ( var param in params) {
			if (url.indexOf(param + '=') != -1) {
				continue;
			}
			if (!first) {
				uri += '&';
			}
			first = false;
			uri += param + '=' + encodeURIComponent(params[param]);
		}
		if (uri.length != 0) {
			return url + (url.indexOf('?') != -1 ? '&' : '?') + uri;
		}
		return url;
	};
	
	KSystem.clone = function(object, exceptions, fromRecurrence) {
		if (object == null) {
			return fromRecurrence ? null : {};
		}
		
		var toReturn = null;
		if (object instanceof Array) {
			toReturn = new Array();
			for ( var i in object) {
				if (typeof object[i] == 'object') {
					toReturn[i] = KSystem.clone(object[i], exceptions, true);
				}
				else {
					toReturn[i] = object[i];
				}
			}
		}
		else if (typeof object == 'object') {
			toReturn = new Object();
			for ( var i in object) {
				if (exceptions && exceptions.test(i)) {
					continue;
				}
				if (typeof object[i] == 'object') {
					toReturn[i] = KSystem.clone(object[i], exceptions, true);
				}
				else {
					toReturn[i] = object[i];
				}
			}
		}
		else {
			return object;
		}
		return toReturn;
		
	};
	
	KSystem.merge = function(object1, object2) {
		if (object1 instanceof Array && object2 instanceof Array) {
			for ( var i in object2) {
				object1.push(object2[i]);
			}
			return object1;
		}
		for ( var i in object2) {
			object1[i] = (object2[i] != null ? object2[i] : object1[i]);
		}
		return object1;
	};
	
	KSystem.setCookie = function(name, value, expires) {
		KSystem.removeCookie(name);
		var exdate = new Date();
		if (expires) {
			exdate.setTime(expires);
		}
		else {
			exdate.setFullYear(exdate.getFullYear() + 10);
		}
		window.document.cookie = name + "=" + encodeURIComponent(JSON.stringify(value)) + "; expires=" + exdate.toUTCString() + '; path=/';
	};
	
	KSystem.getCookie = function(name) {
		if (document.cookie.length > 0) {
			var start = document.cookie.indexOf(name + "=");
			if (start != -1) {
				start = start + name.length + 1;
				var end = document.cookie.indexOf(";", start);
				if (end == -1) {
					end = document.cookie.length;
				}
				var toReturn = null;
				eval('toReturn = ' + unescape(document.cookie.substring(start, end)) + ';');
				return toReturn || {};
			}
		}
		return {};
	};
	
	KSystem.removeCookie = function(name) {
		var exdate = new Date();
		exdate.setTime(exdate.getTime() - 3600000);
		window.document.cookie = name + "=delete; expires=" + exdate.toUTCString() + '; path=/';
	};
	
	KSystem.addTimer = function(code, miliseconds, nonStop) {
		if (nonStop) {
			return setInterval(code, miliseconds);
		}
		else {
			return setTimeout(code, miliseconds);
		}
	};
	
	KSystem.removeTimer = function(timer) {
		clearInterval(timer);
		clearTimeout(timer);
	};
	
	KSystem.getObjectClass = function(obj) {
		if (obj && obj.constructor && obj.constructor.toString) {
			var arr = obj.constructor.toString().match(/function\s*(\w+)/);
			if (arr && (arr.length == 2)) {
				return arr[1];
			}
		}
		return undefined;
	};
	
	KSystem.formatDouble = function(v, p) {
		var precision = (!!p ? p : 0), neg = v < 0, power = Math.pow(10, precision), value = Math.round(v * power), integral = String((neg ? Math.ceil : Math.floor)(value / power)), fraction = String((neg ? -value : value) % power), padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
		
		return precision ? integral + '.' + padding + fraction : integral;
	};

	KSystem.truncateDouble = function(v, p) {
		var s = "" + v;
		var p = s.split(".");
		if (p.length == 1) {
			return p[0] + '.00';
		}
		else {
			return p[0] + '.' + p[1][0] + (p[1].length > 1 ? p[1][1] : '0');
		}
	};
	
	KSystem.months = [
		'Janeiro',
		'Fevereiro',
		'Mar\u00e7o',
		'Abril',
		'Maio',
		'Junho',
		'Julho',
		'Agosto',
		'Setembro',
		'Outubro',
		'Novembro',
		'Dezembro'
	];
	KSystem.monthsAbrev = [
		'Jan',
		'Fev',
		'Mar',
		'Abr',
		'Mai',
		'Jun',
		'Jul',
		'Ago',
		'Set',
		'Out',
		'Nov',
		'Dez'
	];
	
	KSystem.monthsIndex = {
		'Janeiro' : 0,
		'Fevereiro' : 1,
		'Mar\u00e7o' : 2,
		'Abril' : 3,
		'Maio' : 4,
		'Junho' : 5,
		'Julho' : 6,
		'Agosto' : 7,
		'Setembro' : 8,
		'Outubro' : 9,
		'Novembro' : 10,
		'Dezembro' : 11
	};
	KSystem.monthsAbrevIndex = {
		'Jan' : 0,
		'Fev' : 1,
		'Mar' : 2,
		'Abr' : 3,
		'Mai' : 4,
		'Jun' : 5,
		'Jul' : 6,
		'Ago' : 7,
		'Set' : 8,
		'Out' : 9,
		'Nov' : 10,
		'Dez' : 11
	};
	
	KSystem.days = [
		'Domingo',
		'Segunda-feira',
		'Ter\u00e7a-feira',
		'Quarta-feira',
		'Quinta-feira',
		'Sexta-feira',
		'S\u00e1bado',
		'Domingo'
	];
	KSystem.daysAbrev = [
		'Dom',
		'Seg',
		'Ter',
		'Qua',
		'Qui',
		'Sext',
		'Sab',
		'Dom'
	];
	
	KSystem.daysIndex = {
		'Domingo' : 0,
		'Segunda-feira' : 1,
		'Ter\u00e7a-feira' : 2,
		'Quarta-feira' : 3,
		'Quinta-feira' : 4,
		'Sexta-feira' : 5,
		'S\u00e1bado' : 6,
		'Domingo' : 7
	};
	KSystem.daysAbrevIndex = {
		'Dom' : 0,
		'Seg' : 1,
		'Ter' : 2,
		'Qua' : 3,
		'Qui' : 4,
		'Sex' : 5,
		'Sab' : 6,
		'Dom' : 7
	};
	
	KSystem.formatDate = function(format, date) {
		var toReturn = '';
		var escape = false;
		toReturn = format.replace(/(\w+)|%/g, function(wholematch, match) {
			if (wholematch == '%') {
				escape = true;
				return '';
			}
			if (escape) {
				escape = false;
				return match;
			}
			switch (match) {
				case 'Y': {
					return date.getFullYear();
				}
				case 'M': {
					return (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1);
				}
				case 'MM': {
					return KSystem.monthsAbrev[date.getMonth()];
				}
				case 'MMM': {
					return KSystem.months[date.getMonth()];
				}
				case 'D': {
					return KSystem.daysAbrev[date.getDay()];
				}
				case 'DD': {
					return KSystem.days[date.getDay()];
				}
				case 'd': {
					return (date.getDate() < 10 ? '0' : '') + date.getDate();
				}
				case 'H': {
					return date.getHours();
				}
				case 'i': {
					return (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
				}
				case 's': {
					return (date.getSeconds() < 10 ? '0' : '') + date.getSeconds();
				}
				default: {
					return match;
				}
			}
		});
		return toReturn.replace(/\|/g, '');
	};
	
	KSystem.parseDate = function(format, date) {
		var toReturn = new Date();
		format += ' ';
		var pattIndex = 0;
		var valIndex = 0;
		var day = -1;
		var month = -1;
		
		format.replace(/(\w+)/g, function(wholematch, match) {
			var auxIndex = (format.indexOf(match, pattIndex) + match.length);
			var index = 0;
			if (auxIndex != 0) {
				index = date.indexOf(format.charAt(auxIndex), valIndex);
			}
			else {
				index = date.length;
			}
			pattIndex += match.length + 1;
			var value = date.substring(valIndex, index != -1 ? index : date.length);
			value = value.indexOf('0') == 0 ? value.substr(1) : value;
			
			valIndex = index + 1;
			if (value == '') {
				return value;
			}
			
			switch (match) {
				case 'Y': {
					toReturn.setFullYear(parseInt(value));
					break;
				}
				case 'M': {
					if (day != -1) {
						toReturn.setMonth(parseInt(value) - 1);
					}
					else {
						month = parseInt(value) - 1;
					}
					break;
				}
				case 'MM': {
					toReturn.setMonth(KSystem.monthsAbrevIndex(value));
					break;
				}
				case 'MMM': {
					toReturn.setMonth(KSystem.monthsIndex(value));
					break;
				}
				case 'd': {
					day = parseInt(value);
					toReturn.setDate(day);
					if (month != -1) {
						toReturn.setMonth(month);
					}
					break;
				}
				case 'H': {
					toReturn.setHours(parseInt(value));
					break;
				}
				case 'i': {
					toReturn.setMinutes(parseInt(value));
					break;
				}
				case 's': {
					toReturn.setSeconds(parseInt(value));
					break;
				}
				default: {
					return match;
					break;
				}
			}
			return value;
		});
		return toReturn;
	};
	
	KSystem.registerAddOn = function(addon) {
		KSystem.addons += ' ' + addon;
	};
	
	KSystem.isAddOn = function(addon) {
		return KSystem.addons.indexOf(addon) != -1;
	};

	KSystem.popup = {
		check: function(popup_window, widget){
			var _scope = this;
			if (popup_window) {
				if(/chrome/.test(navigator.userAgent.toLowerCase())){
					setTimeout(function () {
						_scope._is_popup_blocked(widget, popup_window);
					},200);
				}
				else{
					popup_window.onload = function () {
						_scope._is_popup_blocked(widget, popup_window);
					};
				}
			}
			else{
				widget.getShell().popupBlocked();
			}
		},
		_is_popup_blocked: function(scope, popup_window){
			if ((popup_window.innerHeight > 0)==false){ 				
				scope.getShell().popupBlocked(); 
			}
			delete popup_window;
		}
	};
	
	KSystem.deserializeURI = function (str, options) {
		var pairs = str.split(/&amp;|&/i), h = {}, options = options || {};
		for(var i = 0; i < pairs.length; i++) {
			var kv = pairs[i].split('=');
			kv[0] = decodeURIComponent(kv[0]);
			if(!options.except || options.except.indexOf(kv[0]) == -1) {
				if((/^\w+\[\w+\]$/).test(kv[0])) {
					var matches = kv[0].match(/^(\w+)\[(\w+)\]$/);
					if(typeof h[matches[1]] === 'undefined') {
						h[matches[1]] = {};
					}
					h[matches[1]][matches[2]] = decodeURIComponent(kv[1]);
				} 
				else {
					h[kv[0]] = decodeURIComponent(kv[1]);
				}
			}
		}
		return h;
	};

	KSystem.ident = 0;
	KSystem.identation = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	KSystem.includes = {};
	KSystem.addons = '';
	
	KSystem.KVAR = null;
	
}

function debug() {
	try {
		if (console && console.log) {
			console.log(arguments);
			return;
		}
	}
	catch (e) {
		return;
	}
	
	if (!Kinky.DEV_MODE) {
		return;
	}
	alert(JSON.stringify(arguments));
}

function dump() {
	if (!Kinky.DEV_MODE) {
		return;
	}
	
	if (Kinky.DUMP_MODE == 'console') {
		try {
			if (console && console.info) {
				console.info(arguments);
				return;
			}
		}
		catch (e) {
			return;
		}
	}
	else {
		if (KSystem.dump) {
			KSystem.dump(JSON.stringify(arguments));
			return;
		}
		
	}
}

function oalert() {
	if (!Kinky.DEV_MODE) {
		return;
	}
	
	alert(JSON.stringify(arguments));
}

function HSV(h, s, v) {
	if (h <= 0) { h = 0; }
	if (s <= 0) { s = 0; }
	if (v <= 0) { v = 0; }
 
	if (h > 360) { h = 360; }
	if (s > 100) { s = 100; }
	if (v > 100) { v = 100; }
 
	this.h = h;
	this.s = s;
	this.v = v;
}
 
function RGB(r, g, b, a) {
	if (r <= 0) { r = 0; }
	if (g <= 0) { g = 0; }
	if (b <= 0) { b = 0; }
 
	if (r > 255) { r = 255; }
	if (g > 255) { g = 255; }
	if (b > 255) { b = 255; }
 
	this.r = r;
	this.g = g;
	this.b = b;
	this.a = a || 1;
}
 
function CMYK(c, m, y, k) {
	if (c <= 0) { c = 0; }
	if (m <= 0) { m = 0; }
	if (y <= 0) { y = 0; }
	if (k <= 0) { k = 0; }
 
	if (c > 100) { c = 100; }
	if (m > 100) { m = 100; }
	if (y > 100) { y = 100; }
	if (k > 100) { k = 100; }
 
	this.c = c;
	this.m = m;
	this.y = y;
	this.k = k;
}
 
var ColorConverter = {
 
	_RGBtoHSV : function  (RGB) {
		var result = new HSV(0, 0, 0);
 
		r = RGB.r / 255;
		g = RGB.g / 255;
		b = RGB.b / 255;
 
		var minVal = Math.min(r, g, b);
		var maxVal = Math.max(r, g, b);
		var delta = maxVal - minVal;
 
		result.v = maxVal;
 
		if (delta == 0) {
			result.h = 0;
			result.s = 0;
		} else {
			result.s = delta / maxVal;
			var del_R = (((maxVal - r) / 6) + (delta / 2)) / delta;
			var del_G = (((maxVal - g) / 6) + (delta / 2)) / delta;
			var del_B = (((maxVal - b) / 6) + (delta / 2)) / delta;
 
			if (r == maxVal) { result.h = del_B - del_G; }
			else if (g == maxVal) { result.h = (1 / 3) + del_R - del_B; }
			else if (b == maxVal) { result.h = (2 / 3) + del_G - del_R; }
 
			if (result.h < 0) { result.h += 1; }
			if (result.h > 1) { result.h -= 1; }
		}
 
		result.h = Math.round(result.h * 360);
		result.s = Math.round(result.s * 100);
		result.v = Math.round(result.v * 100);
 
		return result;
	},
 
	_HSVtoRGB : function  (HSV) {
		var result = new RGB(0, 0, 0);
 
		var h = HSV.h / 360;
		var s = HSV.s / 100;
		var v = HSV.v / 100;
 
		if (s == 0) {
			result.r = v * 255;
			result.g = v * 255;
			result.v = v * 255;
		} else {
			var_h = h * 6;
			var_i = Math.floor(var_h);
			var_1 = v * (1 - s);
			var_2 = v * (1 - s * (var_h - var_i));
			var_3 = v * (1 - s * (1 - (var_h - var_i)));
 
			if (var_i == 0) {var_r = v; var_g = var_3; var_b = var_1}
			else if (var_i == 1) {var_r = var_2; var_g = v; var_b = var_1}
			else if (var_i == 2) {var_r = var_1; var_g = v; var_b = var_3}
			else if (var_i == 3) {var_r = var_1; var_g = var_2; var_b = v}
			else if (var_i == 4) {var_r = var_3; var_g = var_1; var_b = v}
			else {var_r = v; var_g = var_1; var_b = var_2};
 
			result.r = var_r * 255;
			result.g = var_g * 255;
			result.b = var_b * 255;
 
			result.r = Math.round(result.r);
			result.g = Math.round(result.g);
			result.b = Math.round(result.b);
		}
 
		return result;
	},
 
	_CMYKtoRGB : function (CMYK){
		var result = new RGB(0, 0, 0);
 
		c = CMYK.c / 100;
		m = CMYK.m / 100;
		y = CMYK.y / 100;
		k = CMYK.k / 100;
 
		result.r = 1 - Math.min( 1, c * ( 1 - k ) + k );
		result.g = 1 - Math.min( 1, m * ( 1 - k ) + k );
		result.b = 1 - Math.min( 1, y * ( 1 - k ) + k );
 
		result.r = Math.round( result.r * 255 );
		result.g = Math.round( result.g * 255 );
		result.b = Math.round( result.b * 255 );
 
		return result;
	},
 
	_RGBtoCMYK : function (RGB){
		var result = new CMYK(0, 0, 0, 0);
 
		r = RGB.r / 255;
		g = RGB.g / 255;
		b = RGB.b / 255;
 
		result.k = Math.min( 1 - r, 1 - g, 1 - b );
		result.c = ( 1 - r - result.k ) / ( 1 - result.k );
		result.m = ( 1 - g - result.k ) / ( 1 - result.k );
		result.y = ( 1 - b - result.k ) / ( 1 - result.k );
 
		result.c = Math.round( result.c * 100 );
		result.m = Math.round( result.m * 100 );
		result.y = Math.round( result.y * 100 );
		result.k = Math.round( result.k * 100 );
 
		return result;
	},
 
	toRGB : function (o) {
		if (typeof o == 'string') { 
			if (o.charAt(0)=="#") { return new RGB(this.hexToR(o), this.hexToG(o), this.hexToB(o)); }
			return this.rgbaToRGBA(o);
		}
		if (o instanceof RGB) { return o; }
		if (o instanceof HSV) {	return this._HSVtoRGB(o); }
		if (o instanceof CMYK) { return this._CMYKtoRGB(o); }
	},
 
	toHSV : function (o) {
		if (o instanceof HSV) { return o; }
		if (o instanceof RGB) { return this._RGBtoHSV(o); }
		if (o instanceof CMYK) { return this._RGBtoHSV(this._CMYKtoRGB(o)); }
	},
 
	toCMYK : function (o) {
		if (o instanceof CMYK) { return o; }
		if (o instanceof RGB) { return this._RGBtoCMYK(o); }
		if (o instanceof HSV) { return this._RGBtoCMYK(this._HSVtoRGB(o)); }
	},

	componentToHex : function (c) {
		var hex = c.toString(16);
		return hex.length == 1 ? "0" + hex : hex;
	},

	toHex : function(o) {
		if (o instanceof RGB) { 
			return (this.componentToHex(o.r) + this.componentToHex(o.g) + this.componentToHex(o.b)).toUpperCase();
		}
	},

	toString : function (o) {
		if (o instanceof CMYK) { return 'cmyk(' + o.c + '%, ' + o.m + '%, ' + o.y + '%, ' + o.k + '%)'; }
		if (o instanceof RGB) { return 'rgba(' + o.r + ', ' + o.g + ', ' + o.b + ', ' + o.a + ')'; }
		if (o instanceof HSV) { return 'hsv(' + o.h + ', ' + o.s + ', ' + o.v + ')'; }
	},

	hexToR : function(h) {
		if (h.length < 6) {
			return parseInt((this.cutHex(h)).substring(0,1),16);
		}
		return parseInt((this.cutHex(h)).substring(0,2),16);
	},

	hexToG : function(h) {
		if (h.length < 6) {
			return parseInt((this.cutHex(h)).substring(1,2),16);
		}
		return parseInt((this.cutHex(h)).substring(2,4),16);
	},

	hexToB : function(h) {
		if (h.length < 6) {
			return parseInt((this.cutHex(h)).substring(2,3),16);
		}
		return parseInt((this.cutHex(h)).substring(4,6),16);
	},

	rgbaToRGBA : function(h) {
		if (h.indexOf('rgba') != -1) {
			var parts = h.split('rgba')[1].split('(')[1].split(')')[0].split(',');
			return new RGB(parseInt(parts[0].trim()), parseInt(parts[1].trim()), parseInt(parts[2].trim()), parseInt(parts[3].trim()));
		} 
		else if (h.indexOf('rgb') != -1) {
			var parts = h.split('rgb')[1].split('(')[1].split(')')[0].split(',');
			return new RGB(parseInt(parts[0].trim()), parseInt(parts[1].trim()), parseInt(parts[2].trim()));
		}
	},

	cutHex : function(h) {
		return (h.charAt(0)=="#") ? h.substring(1,7) : h;
	}
 
};
