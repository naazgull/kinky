function KBreadcrumb() {
	this.query = null;
	this.action = null;
	this.hash = null;
	this.url = null;
	this.last = null;
	this.history = new Array();
	this.redirect = new Object();
	this.byWidget = {
		location : {},
		query : {},
		action : {}
	};
	
	this.listeners = {
		location : {},
		query : {},
		action : {}
	};
}

{
	KBreadcrumb.prototype.constructor = KBreadcrumb;
	
	KBreadcrumb.addLocationListener = function(widget, callback, hashes) {
		if (!KBreadcrumb.sparrow.byWidget.location[widget.id]) {
			KBreadcrumb.sparrow.byWidget.location[widget.id] = {};
		}
		
		var listener = {
			widget : widget,
			callback : callback
		};
		
		if (!hashes) {
			hashes = [
				'*'
			];
		}
		
		for ( var hash in hashes) {
			var all = new RegExp('^' + hashes[hash].replace(/\*/g, '(.*)'));
			
			if (KBreadcrumb.sparrow.listeners.location[hashes[hash]] == null) {
				KBreadcrumb.sparrow.listeners.location[hashes[hash]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.listeners.location[hashes[hash]].listeners.push(listener);
			if (widget.activated() && all.test(KBreadcrumb.getHash())) {
				KBreadcrumb.dispatchEvent(widget.id, {
					hash : KBreadcrumb.getHash()
				});
			}
			
			if (!KBreadcrumb.sparrow.byWidget.location[widget.id][hashes[hash]]) {
				KBreadcrumb.sparrow.byWidget.location[widget.id][hashes[hash]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.byWidget.location[widget.id][hashes[hash]].listeners.push(listener);
		}
	};
	
	KBreadcrumb.addQueryListener = function(widget, callback, queries) {
		if (!KBreadcrumb.sparrow.byWidget.query[widget.id]) {
			KBreadcrumb.sparrow.byWidget.query[widget.id] = {};
		}
		
		var listener = {
			widget : widget,
			callback : callback
		};
		
		if (!queries) {
			queries = [
				'*'
			];
		}
		
		for ( var query in queries) {
			var all = new RegExp('^' + queries[query].replace(/\*/g, '(.*)'));
			if (KBreadcrumb.sparrow.listeners.query[queries[query]] == null) {
				KBreadcrumb.sparrow.listeners.query[queries[query]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.listeners.query[queries[query]].listeners.push(listener);
			if (widget.activated() && all.test(KBreadcrumb.getQuery())) {
				KBreadcrumb.dispatchEvent(widget.id, {
					query : KBreadcrumb.getQuery()
				});
			}
			
			if (!KBreadcrumb.sparrow.byWidget.query[widget.id][queries[query]]) {
				KBreadcrumb.sparrow.byWidget.query[widget.id][queries[query]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.byWidget.query[widget.id][queries[query]].listeners.push(listener);
		}
		
	};
	
	KBreadcrumb.addActionListener = function(widget, callback, actions) {
		if (!KBreadcrumb.sparrow.byWidget.action[widget.id]) {
			KBreadcrumb.sparrow.byWidget.action[widget.id] = {};
		}
		
		var listener = {
			widget : widget,
			callback : callback
		};
		
		if (!actions) {
			actions = [
				'*'
			];
		}
		
		for ( var action in actions) {
			var all = new RegExp('^' + actions[action].replace(/\*/g, '(.*)'));
			if (KBreadcrumb.sparrow.listeners.action[actions[action]] == null) {
				KBreadcrumb.sparrow.listeners.action[actions[action]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.listeners.action[actions[action]].listeners.push(listener);
			if (widget.activated() && all.test(KBreadcrumb.getAction())) {
				KBreadcrumb.dispatchEvent(widget.id, {
					action : KBreadcrumb.getAction()
				});
			}
			if (!KBreadcrumb.sparrow.byWidget.action[widget.id][actions[action]]) {
				KBreadcrumb.sparrow.byWidget.action[widget.id][actions[action]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.byWidget.action[widget.id][actions[action]].listeners.push(listener);
		}
	};
	
	KBreadcrumb.back = function(params) {
		
		var nTimes = 1;
		if (params && params.nTimes) {
			nTimes = params.nTimes;
		}
		
		KBreadcrumb.sparrow.history.pop();
		var url = null;
		for ( var time = 0; time != nTimes; time++) {
			
			url = KBreadcrumb.sparrow.history.pop();
			if (url == null) {
				return;
			}
			url = '#' + url.url;
			url = KBreadcrumb.processURL(url);
			params = params || {};
			params.url = params.url;
			params.query = params.query || url.query || '';
			params.action = params.action || url.action || '';
			params.hash = url.hash;
			
		}
		KBreadcrumb.dispatchURL(params);
	};
	
	KBreadcrumb.clearHistory = function(leave) {
		var toLeave = [];
		
		for ( var i = 0; i != leave; i++) {
			var url = KBreadcrumb.sparrow.history.pop();
			toLeave.push(url);
		}
		
		delete KBreadcrumb.sparrow.history;
		KBreadcrumb.sparrow.history = toLeave;
	};
	
	KBreadcrumb.popHistory = function(nTimes) {
		if (nTimes == null) {
			nTimes = 1;
		}
		var toReturn = {};
		
		for ( var i = 0; i != nTimes; i++) {
			var url = KBreadcrumb.sparrow.history.pop();
			toReturn['#' + url.url] = url.title;
		}
		
		return toReturn;
	};
	
	KBreadcrumb.historyLen = function() {
		return KBreadcrumb.sparrow.history.length;
	};
	
	KBreadcrumb.getHistory = function(offset) {
		var url = KBreadcrumb.sparrow.history[KBreadcrumb.sparrow.history.length - (offset || 0) - 1];
		if (url == null) {
			return;
		}
		
		return '#' + url.url;
	};
	
	KBreadcrumb.getHistoryTitle = function(offset) {
		var url = KBreadcrumb.sparrow.history[KBreadcrumb.sparrow.history.length - (offset || 0) - 1];
		if (url == null) {
			return;
		}
		
		return url.title;
	};
	
	KBreadcrumb.setHistoryTitle = function(title, offset) {
		var url = KBreadcrumb.sparrow.history[KBreadcrumb.sparrow.history.length - (offset || 0) - 1];
		if (url == null) {
			return;
		}
		
		url.title = title;
	};
	
	KBreadcrumb.getLink = function(params) {
		if (params.url) {
			return '#' + params.url;
		}
		
		if (params.hash && KBreadcrumb.sparrow.redirect[params.hash]) {
			params.hash = KBreadcrumb.sparrow.redirect[params.hash];
		}
		
		var link = '#' + (params.hash != null ? params.hash : (params.hash == '' ? '' : (KBreadcrumb.sparrow && (KBreadcrumb.sparrow.hash != null) ? KBreadcrumb.sparrow.hash : '')));
		link += (params.query != null ? (params.query == '' ? '' : '/?' + params.query) : (KBreadcrumb.sparrow && (KBreadcrumb.sparrow.query != null) ? '/?' + KBreadcrumb.sparrow.query : ''));
		link += (params.action != null ? (params.action == '' ? '' : '/@' + params.action) : (KBreadcrumb.sparrow && (KBreadcrumb.sparrow.action != null) ? '/@' + KBreadcrumb.sparrow.action : ''));
		return link;
	};
	
	KBreadcrumb.dispatchURL = function(params) {
		KBreadcrumb.sparrow.last = window.document.location.href.split('#')[1];
		window.document.location = window.document.location.href.split('#')[0] + KBreadcrumb.getLink(params);
	};
	
	KBreadcrumb.dispatchEvent = function(widgetID, params) {
		if (!!widgetID && !params) {
			if (!!KBreadcrumb.drilldown) {
				try {
					KBreadcrumb.sendRegex('query', KBreadcrumb.drilldown, widgetID);
				}
				catch (e) {
					throw new KException(KBreadcrumb.sparrow, e.message);
				}
			}
		}
		if (!params) {
			params = {
				hash : KBreadcrumb.getHash(),
				query : KBreadcrumb.getQuery(),
				action : KBreadcrumb.getAction()
			};
		}
		if (params.hash && KBreadcrumb.sparrow.redirect[params.hash]) {
			params.hash = KBreadcrumb.sparrow.redirect[params.hash];
		}
		
		if (params.hash != null) {
			try {
				KBreadcrumb.sendRegex('location', params.hash, widgetID);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
		if (params.query != null) {
			try {
				KBreadcrumb.sendRegex('query', params.query, widgetID);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
		if (params.action != null) {
			try {
				KBreadcrumb.sendRegex('action', params.action, widgetID);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
	};
	
	KBreadcrumb.processURL = function(current) {
		if (!current) {
			current = window.document.location.href.split('#');
		}
		else {
			current = current.split('#');
		}
		
		var breadcrumb = new Object();
		
		if (current.length > 1) {
			breadcrumb.url = current[1];
			
			var hash = breadcrumb.url.split('/?');
			if (hash.length > 1) {
				breadcrumb.hash = hash[0];
				breadcrumb.query = hash[1];
				
				var actions = hash[1].split('/@');
				if (actions.length > 1) {
					breadcrumb.query = actions[0];
					breadcrumb.action = actions[1];
				}
			}
			else {
				breadcrumb.hash = breadcrumb.url;
				var actions = breadcrumb.hash.split('/@');
				if (actions.length > 1) {
					breadcrumb.hash = actions[0];
					breadcrumb.action = actions[1];
				}
			}
		}
		
		return breadcrumb;
	};
	
	KBreadcrumb.sendRegex = function(list, hash, widgetID) {
		
		if (widgetID) {
			if (!KBreadcrumb.sparrow.byWidget[list][widgetID]) {
				return;
			}
			var listenersList = KBreadcrumb.sparrow.byWidget[list][widgetID];
			for ( var pattern in listenersList) {
				if (listenersList[pattern].regex.test(hash)) {
					for ( var listener in listenersList[pattern].listeners) {
						if (!listenersList[pattern].listeners[listener].widget || !Kinky.getWidget(listenersList[pattern].listeners[listener].widget.id)) {
							delete listenersList[pattern].listeners[listener];
							continue;
						}
						try {
							KSystem.addTimer('if (!!KBreadcrumb.sparrow.byWidget.' + list + '[\'' + widgetID + '\']) KBreadcrumb.sendEvent(KBreadcrumb.sparrow.byWidget.' + list + '[\'' + widgetID + '\'][\'' + pattern + '\'].listeners[' + listener + '], \'' + hash + '\')', 1);
						}
						catch (e) {
						}
					}
				}
			}
		}
		else {
			var listenersList = KBreadcrumb.sparrow.listeners[list];
			for ( var pattern in listenersList) {
				if (listenersList[pattern].regex.test(hash)) {
					for ( var listener in listenersList[pattern].listeners) {
						if (!listenersList[pattern].listeners[listener].widget || !Kinky.getWidget(listenersList[pattern].listeners[listener].widget.id)) {
							delete listenersList[pattern].listeners[listener];
							continue;
						}
						try {
							KSystem.addTimer('if (!!KBreadcrumb.sparrow.listeners.' + list + '[\'' + pattern + '\']) KBreadcrumb.sendEvent(KBreadcrumb.sparrow.listeners.' + list + '[\'' + pattern + '\'].listeners[' + listener + '], \'' + hash + '\')', 1);
						}
						catch (e) {
						}
					}
				}
			}
		}
	};
	
	KBreadcrumb.sendEvent = function(listener, event) {
		try {
			listener.widget.fromListener = true;
			listener.callback(listener.widget, event);
		}
		catch (e) {
			throw e;
		}
	};
	
	KBreadcrumb.getHash = function() {
		return KBreadcrumb.sparrow.hash;
	};
	
	KBreadcrumb.getQuery = function() {
		return KBreadcrumb.sparrow.query;
	};
	
	KBreadcrumb.getAction = function() {
		return KBreadcrumb.sparrow.action;
	};
	
	KBreadcrumb.getURL = function() {
		return KBreadcrumb.sparrow.url;
	};
	
	KBreadcrumb.onLocationChange = function() {
		KBreadcrumb.sparrow.processing = true;
		
		var breadcrumb = KBreadcrumb.processURL();
		if (KBreadcrumb.sparrow.url != breadcrumb.url) {
			KBreadcrumb.sparrow.history.push({
				title : null,
				url : breadcrumb.url
			});
			
			KBreadcrumb.sparrow.url = breadcrumb.url;
		}
		else {
			KBreadcrumb.sparrow.last = KBreadcrumb.sparrow.url;
		}
		
		if (breadcrumb.hash != KBreadcrumb.sparrow.hash) {
			KBreadcrumb.sparrow.hash = breadcrumb.hash;
			try {
				KBreadcrumb.sendRegex('location', breadcrumb.hash);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
		if (breadcrumb.query != KBreadcrumb.sparrow.query) {
			KBreadcrumb.sparrow.query = breadcrumb.query;
			try {
				KBreadcrumb.sendRegex('query', breadcrumb.query);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
		if (breadcrumb.action != KBreadcrumb.sparrow.action) {
			KBreadcrumb.sparrow.action = breadcrumb.action;
			try {
				KBreadcrumb.sendRegex('action', breadcrumb.action);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
		KBreadcrumb.sparrow.processing = false;
		KSystem.addTimer(KBreadcrumb.onLocationChange, 80);
	};
	
	KBreadcrumb.sparrow = null;
	KBreadcrumb.DISPATCH_URL = 'url';
	KBreadcrumb.DISPATCH_EVENT = 'event';
	
	KSystem.included('KBreadcrumb');
}