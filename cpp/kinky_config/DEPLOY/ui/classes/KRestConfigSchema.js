function KRestConfigSchema(parent) {
	if (parent != null) {
		KLayeredPanel.call(this, parent);
		this.internalEvent = true;
		this.loadService = true;
	}
}

KSystem.include([
	'KInput',
	'KButton',
	'KLayeredPanel'
], function() {
	KRestConfigSchema.prototype = new KLayeredPanel();
	KRestConfigSchema.prototype.constructor = KRestConfigSchema;
	
	KRestConfigSchema.prototype.onLoad = function(data) {
		KSystem.merge(this.data, data);
		this.edit = true;
		this.kinky.get(this, 'rest:/wrml-schemas', {}, 'onLoadSuperClass');
	};
	
	KRestConfigSchema.prototype.onLoadSuperClass = function(data) {
		var extensions = {};
		if (!!this.data.schema && !!this.data.schema.extends && (this.data.schema.extends.length != 0)) {
			for ( var name in this.data.schema.extends) {
				extensions[this.data.schema.extends[name]] = this.data.schema.extends[name];
			}
		}
		
		var superSchemas = new KCombo(this, 'Extends', 'extends');
		{
			superSchemas.isMultiple = true;
			superSchemas.hash = '/schema-extends';
			for ( var i in data.schema) {
				superSchemas.addOption(data.schema[i], data.schema[i], !!extensions[data.schema[i]]);
			}
		}
		this.appendTitleChild(superSchemas);
		this.setTitle('Fields');
		this.draw();
	};
	
	KRestConfigSchema.prototype.onCreated = KRestConfigSchema.prototype.onAddExtension = function(data) {
		this.refresh();
	};
	
	KRestConfigSchema.prototype.onNoContent = function(data) {
		if (!!this.isSuperClassLoaded) {
			this.draw();
			return;
		}
		this.isSuperClassLoaded = true;
		this.kinky.get(this, 'rest:/wrml-schemas', {}, 'onLoadSuperClass');
	};
	
	KRestConfigSchema.prototype.onError = function(data) {
		this.draw();
	};
	
	KRestConfigSchema.prototype.draw = function() {
		
		var fieldForm = this.createFieldForm();
		fieldForm.hash = this.data.schemaHash + '/add-field';
		this.addPanel(fieldForm, '** Add new field **');
		
		if (this.data.schema) {
			for ( var field in this.data.schema.fields) {
				var fieldForm = this.createFieldForm(this.data.schema.fields[field], field);
				fieldForm.hash = this.data.schemaHash + '/' + field;
				this.addPanel(fieldForm, field.toUpperCase());
			}
		}
		
		this.clearBoth();
		
		var extendSubmit = new KButton(this, 'GO', 'go', function(event) {
			var form = KSystem.getEventWidget(event, 'KRestConfigSchema');
			if (!!form.edit) {
				form.kinky.put(form, 'rest:/wrml-edit/schemas' + form.data.schemaHash, {
					schemaName : form.data.schemaName,
					'extends' : form.childWidget('/schema-extends').getValue()
				}, 'onAddExtension');
			}
			else {
				form.kinky.post(form, 'rest:/wrml-edit/schemas' + form.data.schemaHash, {
					schemaName : form.data.schemaName,
					'extends' : form.childWidget('/schema-extends').getValue()
				}, 'onAddExtension');
			}
		});
		this.appendChild(extendSubmit);
		
		KLayeredPanel.prototype.draw.call(this);
	};
	
	KRestConfigSchema.prototype.createFieldForm = function(data, fieldName) {
		var form = new KForm(this);
		{
			form.action = 'rest:/wrml-edit/schemas' + this.data.schemaHash;
			form.method = (!!data ? 'put' : 'post');
			
			var schemaName = new KInput(form, 'hidden', null, 'schemaName');
			{
				schemaName.hash = '/service-schemaName';
				if (!!this.data.schemaName) {
					schemaName.setValue(this.data.schemaName);
				}
				schemaName.setStyle({
					display : 'none'
				});
			}
			form.addInput(schemaName);
			
			var name = new KInput(form, (!fieldName ? 'text' : 'static'), 'Name', 'name');
			{
				name.hash = '/service-name';
				name.addValidator(/.+/, 'field is mandatory');
				if (!!fieldName) {
					name.setValue(fieldName);
				}
			}
			form.addInput(name);
			
			var type = new KCombo(form, 'Type', 'type');
			{
				
				type.hash = '/service-type';
				type.addValidator(/.+/, 'field is mandatory');
				type.addOption('Boolean', 'Boolean', !!data && !!data.type && (data.type == 'Boolean'));
				type.addOption('Choice', 'Choice', !!data && !!data.type && (data.type == 'Choice'));
				type.addOption('DateTime', 'DateTime', !!data && !!data.type && (data.type == 'DateTime'));
				type.addOption('Double', 'Double', !!data && !!data.type && (data.type == 'Double'));
				type.addOption('Integer', 'Integer', !!data && !!data.type && (data.type == 'Integer'));
				type.addOption('List', 'List', !!data && !!data.type && (data.type == 'List'));
				type.addOption('Schema', 'Schema', !!data && !!data.type && (data.type == 'Schema'));
				type.addOption('Text', 'Text', !!data && !!data.type && (data.type == 'Text'));
			}
			form.addInput(type);
			
			var mimeType = new KCombo(form, 'Mime Type', 'mimeType');
			{
				mimeType.hash = '/service-mime-type';
				if (!!data) {
					for ( var format in data.requestTypes) {
						if (data.requestTypes[format].indexOf('application/wrml') != -1) {
							var indexOfSchema = data.requestTypes[format].indexOf('schema="');
							var indexOfQuote = data.requestTypes[format].indexOf('"', indexOfSchema + 8);
							data.mimeType = 'application/wrml;schema="' + data.requestTypes[format].substring(indexOfSchema + 8, indexOfQuote) + '"';
							data.requestTypes.splice(format, 1);
							break;
						}
					}
					for ( var format in data.responseTypes) {
						if (data.responseTypes[format].indexOf('application/wrml') != -1) {
							data.responseTypes.splice(format, 1);
							break;
						}
					}
				}
				mimeType.addOption(null, '--');
				for ( var schema in Kinky.shell.data.schema) {
					mimeType.addOption('application/wrml;schema="' + Kinky.shell.data.schema[schema] + '"', Kinky.shell.data.schema[schema], !!data && !!data.mimeType && (data.mimeType == 'application/wrml;schema="' + Kinky.shell.data.schema[schema] + '"'));
				}
			}
			form.addInput(mimeType);
			
			var defaultValue = new KInput(form, 'text', 'Default Value', 'defaultValue');
			{
				defaultValue.hash = '/service-default-value';
				if (!!data && !!data.defaultValue) {
					defaultValue.setValue(data.defaultValue);
				}
			}
			form.addInput(defaultValue);
			
			var readOnly = new KInput(form, 'radio', 'Read Only', 'readOnly');
			{
				readOnly.hash = '/service-read-only';
				readOnly.addValidator(/.+/, 'field is mandatory');
				readOnly.addOption('true', 'true', !!data && (data.readOnly == true));
				readOnly.addOption('false', 'false', !data || (data.readOnly != true));
				
			}
			form.addInput(readOnly);
			
			var required = new KInput(form, 'radio', 'Required', 'required');
			{
				required.hash = '/service-required';
				required.addValidator(/.+/, 'field is mandatory');
				required.addOption('true', 'true', !!data && (data.required == true));
				required.addOption('false', 'false', !data || (data.required != true));
			}
			form.addInput(required);
			
			var hidden = new KInput(form, 'radio', 'Hidden', 'hidden');
			{
				hidden.hash = '/service-hidden';
				hidden.addValidator(/.+/, 'field is mandatory');
				hidden.addOption('true', 'true', !!data && (data.hidden == true));
				hidden.addOption('false', 'false', !data || (data.hidden != true));
			}
			form.addInput(hidden);
			
			var constraints = new KInput(form, 'textarea', 'Constraints', 'constraints');
			{
				constraints.hash = '/service-constraints';
				if (!!data && data.constraints) {
					constraints.setValue(JSON.stringify(data.constraints));
				}
				else {
					constraints.setValue('[ ]');
				}
			}
			form.addInput(constraints);
			
			var responseTypes = new KInput(form, 'textarea', 'Response Types', 'responseTypes');
			{
				responseTypes.hash = '/service-responseTypes';
				if (!!data && data.responseTypes) {
					responseTypes.setValue(JSON.stringify(data.responseTypes));
				}
				else {
					responseTypes.setValue('[ "application/json", "text/xml", "text/html", "application/vnd.kinky.KText" ]');
				}
			}
			form.addInput(responseTypes);
			
			var requestTypes = new KInput(form, 'textarea', 'Request Types', 'requestTypes');
			{
				requestTypes.hash = '/service-requestTypes';
				if (!!data && data.requestTypes) {
					requestTypes.setValue(JSON.stringify(data.requestTypes));
				}
				else {
					requestTypes.setValue('[ "application/json", "text/xml", "text/html", "application/vnd.kinky.KInput" ]');
				}
			}
			form.addInput(requestTypes);
			
			var description = new KInput(form, 'textarea', 'Description', 'description');
			{
				description.hash = '/service-description';
				description.addValidator(/.+/, 'field is mandatory');
				if (!!data && data.description) {
					description.setValue(data.description);
				}
			}
			form.addInput(description);
			
			var options = new KInput(form, 'textarea', 'Options', 'options');
			{
				options.hash = '/service-options';
				if (!!data && data.options) {
					options.setValue(JSON.stringify(data.options));
				}
			}
			form.addInput(options);
			
			if ((!!data && !data.extended) || !data) {
				var operations = new KInput(form, 'submit', 'GO', 'go');
				form.addInput(operations);
				
				var del = new KInput(form, 'submit', 'REMOVE', 'del');
				form.appendChild(del);
				
				form.onValidate = function(text, params, status) {
					if (status) {
						this.method = (params.del == 'clicked' ? 'delete' : this.method);
						
						this.disable();
						params.readOnly = params.readOnly[0];
						params.required = params.required[0];
						params.hidden = params.hidden[0];
						
						if (!!params.responseTypes && (params.responseTypes != '')) {
							eval('params.responseTypes = ' + params.responseTypes + ';');
						}
						else {
							delete params.responseTypes;
						}
						if (!!params.requestTypes && (params.requestTypes != '')) {
							eval('params.requestTypes = ' + params.requestTypes + ';');
						}
						else {
							delete params.requestTypes;
						}
						if (!!params.options && (params.options != '')) {
							eval('params.options = ' + params.options + ';');
						}
						else {
							delete params.options;
						}
						if (!!params.constraints && (params.constraints != '')) {
							eval('params.constraints = ' + params.constraints + ';');
						}
						else {
							delete params.constraints;
						}
						
						if (!!params.mimeType && (params.mimeType != '')) {
							if (!params.responseTypes) {
								params.responseTypes = [];
							}
							params.responseTypes.push(params.mimeType);
							if (!params.requestTypes) {
								params.requestTypes = [];
							}
							params.requestTypes.push(params.mimeType);
						}
						delete params.mimeType;
						
						return params;
					}
					else {
						for ( var param in params) {
							params[param].showErrorMessage();
						}
					}
				};
				form.onCreated = form.onSuccess = function(dataS) {
					this.enable();
					this.getParent('KRestConfigSchema').refresh();
				};
			}
		}
		return form;
	};
	
	KSystem.included('KRestConfigSchema');
});
