function KRestConfigForm(parent, hash, service) {
	if (parent != null) {
		KShellPart.call(this, parent, hash, service);
	}
}

KSystem.include([
	'KInput',
	'KShellPart',
	'KRestConfigRelation',
	'KRestConfigSchema'
], function() {
	KRestConfigForm.prototype = new KShellPart();
	KRestConfigForm.prototype.constructor = KRestConfigForm;
	
	KRestConfigForm.prototype.load = function() {
		if (!!this.data.feService) {
			KShellPart.prototype.load.call(this);
		}
		else {
			this.draw();
		}
	};
	
	KRestConfigForm.prototype.onLoad = function(data) {
		KSystem.merge(this.data, data);
		this.draw();
	};
	
	KRestConfigForm.prototype.onNoContent = KRestConfigForm.prototype.onError = function(data) {
		this.draw();
	};
	
	KRestConfigForm.prototype.draw = function() {
		if (!this.data.isSchema) {
			var fields = new KRestConfigRelation(this);
			fields.data = {
				relationHash : this.data.href,
				feService : 'rest:' + this.data.rel
			};
			fields.internalEvent = true;
			this.appendChild(fields);
		}
		else {
			var fields = new KRestConfigSchema(this);
			fields.data = {
				schemaHash : this.data.href,
				schemaName : this.data.title,
				feService : 'rest:' + this.data.rel
			};
			fields.internalEvent = true;
			this.appendChild(fields);
		}
		
		KShellPart.prototype.draw.call(this);
	};
	
	KSystem.included('KRestConfigForm');
});
