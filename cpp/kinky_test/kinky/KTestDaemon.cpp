#include <kinky/KTestDaemon.h>

kinky::KTestDaemon::KTestDaemon(string* testDir, KRestAPI* apiParam) :
		KThread<string>(testDir), api(apiParam) {
}

kinky::KTestDaemon::~KTestDaemon() {
}

void kinky::KTestDaemon::execute() {
	string* target = this->getTarget();
	vector<string> files;
	kinky::utils::filesystem::glob(*target, files, "(.+)\\.http", true);
	std::sort( files.begin(), files.end() );

	ostringstream oss;
	oss << *target << ".csv";
	ofstream out;
	out.open(oss.str().data());
	out << "`TEST FILE`;`REQUEST METHOD`;`REQUEST URL`;`RESPONSE LINE`;`EXPECTED`;`TEST STATUS`" << endl << flush;

	oss.str("");
	oss << *target << ".html";
	ofstream html;
	html.open(oss.str().data());
	html << "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML+RDFa 1.0//EN\" \"http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd\"><html><head><style>body { font-family: \"Courier New\", sans serif; font-size: 11px; } table { border-width: 1px; border-spacing: 0px; border-style: solid; border-color: gray; border-collapse: collapse; background-color: white; } td { border-width: 1px; padding: 5px; border-style: inset; border-color: gray; } th { border-width: 1px; padding: 5px; border-style: inset; border-color: gray; font-size: 18px; text-align: center; font-weight: bold; }</style></head><body><table>" << flush;
	html << "<tr><th>TEST FILE</th><th>REQUEST METHOD</th><th>REQUEST URL</th><th>RESPONSE LINE</th><th>EXPECTED</th><th>TEST STATUS</th></tr>" << flush;

	for (vector<string>::iterator i = files.begin(); i != files.end(); i++) {
		oss.str("");
		kinky::utils::filesystem::load_file(i->data(), oss);
		string content = oss.str();
		kinky::utils::replaceAll(content, "\n", CRLF);

		KHTTPRequest* request = KHTTPRequest::parse(&content);
		KObject* body = NULL;
		if (request->getBody() != NULL) {
			try {
				body = KJSON::parse(request->getBody());
			}
			catch (KParseException* e) {
				delete e;
				body = new KObject();
			}
		}

		KRest::initRequestHeaders(body, request->getHeaders());
		string* stoken = KHTTPRequest::getAuthentication(request);

		KObject* response = KRest::exec(request->getDirective(), this->api->getConfiguration()->get("krest_server_url"), *request->getURL(), stoken, body);

		string* expect = request->getHeader("Expect");
		string file(i->data());
		kinky::utils::replaceAll(file, *target, "");

		if (expect != NULL) {
			if (KHTTPResponse::getStatusLine(response->getStatus()) == *expect) {
				out << "`" << file << "`;`" << KHTTPRequest::getDirective(request->getDirective()) << "`;`" << *request->getURL() << "`;`" << KHTTPResponse::getStatusLine(response->getStatus()) << "`;`" << *expect << "`;`SUCCESS`" << endl << flush;
				html << "<tr style=\"background-color: #E2FFE7;\"><td>" << file << "</td><td>" << KHTTPRequest::getDirective(request->getDirective()) << "</td><td>" << *request->getURL() << "</td><td>" << KHTTPResponse::getStatusLine(response->getStatus()) << "</td><td>" << *expect << "</td><td>SUCCESS</td></tr>" << flush;

			}
			else {
				out << "`" << file << "`;`" << KHTTPRequest::getDirective(request->getDirective()) << "`;`" << *request->getURL() << "`;`" << KHTTPResponse::getStatusLine(response->getStatus()) << "`;`" << *expect << "`;`FAIL`" << endl << flush;
				html << "<tr style=\"background-color: #FFE2E2;\"><td>" << file << "</td><td>" << KHTTPRequest::getDirective(request->getDirective()) << "</td><td>" << *request->getURL() << "</td><td>" << KHTTPResponse::getStatusLine(response->getStatus()) << "</td><td>" << *expect << "</td><td>FAIL</td></tr>" << flush;
			}
		}
		else {
			out << "`" << file << "`;`" << KHTTPRequest::getDirective(request->getDirective()) << "`;`" << *request->getURL() << "`;`" << KHTTPResponse::getStatusLine(response->getStatus()) << "`;`NO EXPECTATION`;`UNKNOWN`" << endl << flush;
			html << "<tr style=\"background-color: #eeeeff;\"><td>" << file << "</td><td>" << KHTTPRequest::getDirective(request->getDirective()) << "</td><td>" << *request->getURL() << "</td><td>" << KHTTPResponse::getStatusLine(response->getStatus()) << "</td><td>NO EXPECTATION</td><td>UNKNOWN</td></tr>" << flush;
		}

		delete request;
		delete response;
	}

	html << "</table></body></html>" << flush;
	html.close();

	out << flush;
	out.close();
}
