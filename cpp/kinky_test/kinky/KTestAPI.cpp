#include <kinky/KTestAPI.h>

#include <kinky/KTestDaemon.h>

kinky::KTestAPI::KTestAPI() {
}

kinky::KTestAPI::~KTestAPI() {
}

void kinky::KTestAPI::init() {
	KTestDaemon* thread4 = new KTestDaemon(new string(this->getConfiguration()->get("ktest_dir")), this);
	thread4->run();
}

kinky::oauth::KToken *kinky::KTestAPI::allowURL(kinky::http::KHTTPRequest *request) {
	string* token = KHTTPRequest::getAuthentication(request);
	kinky::oauth::KToken* ret = NULL;

	if (token != NULL) {
		ret = new kinky::oauth::KToken(*token, this->getConfiguration()->get("ktoken_encryption_private_key"));
		if (ret->isValid()) {
			return ret;
		}
		else {
			return NULL;
		}
	}
	return new kinky::oauth::KToken();
}


extern "C" KRestAPI* initAPI() {
	return new kinky::KTestAPI();
}
