#pragma once

#include <kinky.h>

using namespace std;
using namespace __gnu_cxx;

using namespace kinky::rest;
using namespace kinky::memory;
using namespace kinky::rest::thread;

namespace kinky {

	class KTestDaemon: public KThread<string> {
		public:
			KTestDaemon(string* prioritySeconds, KRestAPI* api);
			virtual ~KTestDaemon();

			void execute();

		private:
			KRestAPI* api;

	};

}
