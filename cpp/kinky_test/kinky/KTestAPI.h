#pragma once

#include <kinky.h>

namespace kinky {
	class KTestAPI : public kinky::rest::KRestAPI {
		public:
			KTestAPI();
			virtual ~KTestAPI();

			virtual void init();

			virtual kinky::oauth::KToken *allowURL(kinky::http::KHTTPRequest *request);
	};
}
