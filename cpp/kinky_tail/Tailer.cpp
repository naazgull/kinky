#define DEBUG 1

#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <sstream>
#include <iostream>
#include <limits>
#include <termios.h>

#define CRLF "\r\n"

using namespace std;
using namespace __gnu_cxx;

const char encodeCharacterTable[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void base64_encode(istream &in, ostringstream &out) {
	char buff1[3];
	char buff2[4];
	uint8_t i = 0, j;

	while (in.readsome(&buff1[i++], 1))
		if (i == 3) {
			out << encodeCharacterTable[(buff1[0] & 0xfc) >> 2];
			out << encodeCharacterTable[((buff1[0] & 0x03) << 4) + ((buff1[1] & 0xf0) >> 4)];
			out << encodeCharacterTable[((buff1[1] & 0x0f) << 2) + ((buff1[2] & 0xc0) >> 6)];
			out << encodeCharacterTable[buff1[2] & 0x3f];
			i = 0;
		}

	if (--i) {
		for (j = i; j < 3; j++)
			buff1[j] = '\0';

		buff2[0] = (buff1[0] & 0xfc) >> 2;
		buff2[1] = ((buff1[0] & 0x03) << 4) + ((buff1[1] & 0xf0) >> 4);
		buff2[2] = ((buff1[1] & 0x0f) << 2) + ((buff1[2] & 0xc0) >> 6);
		buff2[3] = buff1[2] & 0x3f;

		for (j = 0; j < (i + 1); j++)
			out << encodeCharacterTable[(uint8_t) buff2[j]];

		while (i++ < 3)
			out << '=';
	}
	out << flush;
}

void kread(int sockfd) {
	char* buffer = new char[256];
	int nRead = -1;

	int opts = fcntl(sockfd, F_GETFL);
	opts = opts & ~O_NONBLOCK;
	if (fcntl(sockfd, F_SETFL, opts) < 0) {
	}

	do {
		string out;

		bzero(buffer, 256);
		nRead = read(sockfd, buffer, 255);
		if (nRead <= 0) {
			break;
		}
		out.append(buffer, nRead);
		cout << out << flush;
	}
	while (true);

}

void ksend(string rep, int sockfd) {
	try {
		int opts = fcntl(sockfd, F_GETFL);
		opts ^= O_NONBLOCK;
		fcntl(sockfd, F_SETFL, opts);

		while (rep.length() != 0) {
			size_t size = write(sockfd, rep.data(), rep.length());
			if (size == (size_t) -1) {
				return;
			}
			rep.erase(0, size);
		}
	}
	catch (exception& e) {
	}
}

int getch() {
	int ch;
	struct termios t_old, t_new;

	tcgetattr(STDIN_FILENO, &t_old);
	t_new = t_old;
	t_new.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &t_new);

	ch = getchar();

	tcsetattr(STDIN_FILENO, TCSANOW, &t_old);
	return ch;
}

int main(int argc, char* argv[]) {
	if (argc < 3) {
		cout << "usage:\n\t$ kinky_tail <host> <port>\n\t" << endl << flush;
		return -1;
	}

	string server(argv[1]);
	string port(argv[2]);
	int ret = 0;
	struct addrinfo hints, *res;
	int sockfd = 0;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ((ret = getaddrinfo(server.data(), port.data(), &hints, &res))) {
		cout << "could not get address info" << endl << flush;
		return -1;
	}

	if ((sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) < 0) {
		cout << "could not create socket" << endl << flush;
		return -1;
	}

	if ((ret = connect(sockfd, res->ai_addr, res->ai_addrlen)) < 0) {
		cout << "could not connect to host" << endl << flush;
		return -1;
	}

	string user;
	cout << "username: " << flush;
	cin >> noskipws >> user;

	const char BACKSPACE = 127;
	const char RETURN = 10;
	cin.clear();
	cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	cout << "password: " << flush;

	string password;
	unsigned char ch = 0;

	while ((ch = getch()) != RETURN) {
		if (ch == BACKSPACE) {
			if (password.length() != 0) {
			                cout << "\b \b";
				password.resize(password.length() - 1);
			}
		}
		else if (ch == 0 || ch == 224) {
			getch();
			continue;
		}
		else
		{
			password += ch;
		                cout << '*';
		}
	}

	cout << endl;

	ostringstream tss;
	stringstream io;
	io << user << "|" << password << flush;
	base64_encode(io, tss);

	string request("GET /kinky/log HTTP/1.1");
	request.insert(request.length(), CRLF);
	request.insert(request.length(), "Authorization: ");
	request.insert(request.length(), tss.str());
	request.insert(request.length(), CRLF);
	request.insert(request.length(), CRLF);

	ksend(request, sockfd);
	kread(sockfd);
}
