#define DEBUG 1

#include <signal.h>
#include <unistd.h>
#include <iostream>
#include <fstream>

#include <regex.h>
#include <kinky.h>
#include <gperftools/malloc_extension.h>

using namespace kinky::rest;
using namespace kinky::rest::http::ssl;

pthread_t threadID = 0;
key_t semKey = 0;
key_t semKey2 = 0;
key_t semKey3 = 0;
int maxThreads = -1;
string logfile;
string sLogfile;

void sigsev(int sig) {
	if (maxThreads != -1 && semKey != 0) {
		int sem = semget(semKey, maxThreads, 0777);
		semctl(sem, 0, IPC_RMID);
		int sem2 = semget(semKey2, ::ceil((float) (((float) maxThreads) / 5)), 0777);
		semctl(sem2, 0, IPC_RMID);
		semKey = 0;
		semKey2 = 0;
		(*kinky::io::out) << "SIGNAL " << sig << ": SEG FAULT" << endl << flush;
		(*kinky::io::out) << endl << "unloading server..." << flush;
		(*kinky::io::out) << " done" << endl << flush;
	}
	else if (maxThreads == -1) {
		(*kinky::io::out) << "SIGNAL " << sig << ": SEG FAULT" << endl << flush;
		(*kinky::io::out) << endl << "unloading server..." << flush;
		(*kinky::io::out) << " done" << endl << flush;
	}
	int sem3 = semget(semKey3, 1, 0777);
	semctl(sem3, 0, IPC_RMID);
	semKey3 = 0;
	exit(-1);
}

void stop(int sig) {
	if (pthread_self() == threadID) {
		(*kinky::io::out) << endl << "unloading server..." << flush;
		if (maxThreads != -1) {
			int sem = semget(semKey, maxThreads, 0777);
			semctl(sem, 0, IPC_RMID);
			int sem2 = semget(semKey2, ::ceil((float) (((float) maxThreads) / 5)), 0777);
			semctl(sem2, 0, IPC_RMID);
		}
		(*kinky::io::out) << " done" << endl << flush;
		int sem3 = semget(semKey3, 1, 0777);
		semctl(sem3, 0, IPC_RMID);
		semKey3 = 0;
		exit(0);
	}
}


void debug_kobject() {
	string content;
	kinky::utils::filesystem::load_file("/home/pf/Develop/CPP/led-rest-api/led/rest/response.json", &content);

	kinky::memory::KMemoryManager::getMemory()->gc();
	MallocExtension::instance()->ReleaseFreeMemory();
	kinky::memory::KMemoryManager::getMemory()->status();
	KObject* ret = KJSON::parse(content);
	kinky::memory::managed_ptr<KObject>* ptr = new kinky::memory::managed_ptr<KObject>(ret);
	ptr->release();
	MallocExtension::instance()->ReleaseFreeMemory();
	kinky::memory::KMemoryManager::getMemory()->status();
}

void debug_httprequest() {
	string key("some");
	string content;
	kinky::utils::filesystem::load_file("/home/pf/Develop/CPP/led-rest-api/led/rest/request.http", &content);

	kinky::memory::KMemoryManager::getMemory()->gc();
	MallocExtension::instance()->ReleaseFreeMemory();
	kinky::memory::KMemoryManager::getMemory()->status();
	KHTTPRequest* ret = new KHTTPRequest();
	ret->setHeader(key, new string(content));
	delete ret;
	MallocExtension::instance()->ReleaseFreeMemory();
	kinky::memory::KMemoryManager::getMemory()->status();
}

void debug_strmap() {
	string key("some");
	string content;
	kinky::utils::filesystem::load_file("/home/pf/Develop/CPP/led-rest-api/led/rest/response.json", &content);

	kinky::memory::KMemoryManager::getMemory()->gc();
	MallocExtension::instance()->ReleaseFreeMemory();
	kinky::memory::KMemoryManager::getMemory()->status();

	kinky::memory::managed_ptr<string>* ptr = new kinky::memory::managed_ptr<string>(new string(content));
	kinky::memory::str_map< kinky::memory::managed_ptr<string>* >* ret = new kinky::memory::str_map< kinky::memory::managed_ptr<string>* >();
	ret->insert(key, ptr);
	delete ret;
	ptr->release();

	kinky::memory::KMemoryManager::getMemory()->gc();
	MallocExtension::instance()->ReleaseFreeMemory();
	kinky::memory::KMemoryManager::getMemory()->status();
}

int main(int argc, char* argv[]) {
	tzset();
	struct timeval tv;
	gettimeofday(&tv, &kinky::utils::date::current_tz);

	int port = 8080;
	int wait = 0;
	char* file = NULL;
	char* cert = NULL;
	char* key = NULL;
	char* associated_apache = NULL;
	char* cport = NULL;

	int c;
	bool hasSSL = false;
	bool useChain = false;
	bool version = false;
	while ((c = getopt(argc, argv, "m:p:s:i:k:c:l:t:w:v")) != -1) {
		switch (c) {
			case 'm': {
				maxThreads = atoi(optarg);
				break;
			}
			case 'p': {
				cport = optarg;
				port = atoi(optarg);
				break;
			}
			case 'c': {
				file = optarg;
				break;
			}
			case 'l': {
				logfile.assign(optarg);
				break;
			}
			case 's': {
				hasSSL = true;
				cert = optarg;
				break;
			}
			case 'i': {
				hasSSL = true;
				useChain = true;
				cert = optarg;
				break;
			}
			case 'k': {
				key = optarg;
				break;
			}
			case 'v': {
				version = true;
				break;
			}
			case 't': {
				associated_apache = optarg;
				break;
			}
			case 'w': {
				wait = atoi(optarg);
				break;
			}
		}
	}

	// >>>
	// Adds listeners for SIGSEV and SIGABRT.
	{
		signal(SIGPIPE, SIG_IGN );

		struct sigaction action;
		action.sa_handler = sigsev;
		sigemptyset(&action.sa_mask);
		action.sa_flags = 0;
		sigaction(SIGSEGV, &action, 0);
		sigaction(SIGABRT, &action, 0);
		sigaction(SIGKILL, &action, 0);
	}

	// >>>
	// Adds listeners for SIGKILL, for gracefull stop
	{
		struct sigaction action;
		action.sa_handler = stop;
		sigemptyset(&action.sa_mask);
		action.sa_flags = 0;
		sigaction(SIGTERM, &action, 0);
		sigaction(SIGQUIT, &action, 0);
		sigaction(SIGINT, &action, 0);
	}

	sleep(wait);

	threadID = pthread_self();

	KRest* rest = NULL;
	KSocketServer* server = NULL;

	rest = new KRest();
	if (cport != NULL) {
		rest->port = new string(cport);
	}
	else {
		rest->port = new string("8080");
	}
	rest->hostname = new string("localhost");

	if (version) {
		cout << rest->getVersion() << endl << flush;
		exit(0);
	}

	if (optind != argc) {
		file = argv[optind];
	}

	server = NULL;

	if (!hasSSL) {
		server = new KHTTPServer(rest);
	}
	else {
		server = new KHTTPSServer(rest, string(cert), string(key), useChain);
	}
	int now = time(NULL);
	if (maxThreads != -1) {
		semKey = ftok(file, now);
		semKey2 = ftok(file, now + 1000);
		server->initThreadPool(maxThreads, file, now);
	}

	try {
		kinky::log::KLogThread* logger = NULL;
		ostream* ofs = NULL;
		server->connect(port);
		if (logfile.length() != 0) {
			if (logfile != "/dev/null") {
				time_t rawtime;
				struct tm ptm;
				char* buffer = new char[516];
				time(&rawtime);
				localtime_r(&rawtime, &ptm);
				strftime(buffer, 516, "D%Y-%m-%dT%H.%M.%S", &ptm);
				string d(buffer);

				ostringstream oss;
				oss << logfile << "." << d  << flush;
				delete buffer;

				kinky::utils::filesystem::move(logfile, oss.str());
			}
			ofs = new ofstream(logfile.data());
			logger = new kinky::log::KLogThread();
		}
		else {
			ofs = &cout;
			logger = new kinky::log::KLogThread();
		}
		logger->addStream(ofs);
		kinky::io::out = kinky::io::k_logger = new kinky::log::KLogStream(logger);
		logger->setBuffer((kinky::log::KLogStream*) kinky::io::out);
		semKey3 = ftok(file, now - 1000);
		logger->set(0, 1, file, now - 1000);
		logger->run();

		if (associated_apache != NULL) {
			ostringstream apachectl;
			apachectl << "" << associated_apache;
			if (system(apachectl.str().data()) != 0) {
				(*kinky::io::out) << endl << "ERR: could not execute command: " << apachectl.str() << endl << endl;
			}
		}
		rest->registerApplication(file);

		(*kinky::io::out) << endl << "Server started at port " << port << " (config: " << file << ")" << endl << endl;
		(*kinky::io::out) << flush;

		while (true) {
			server->wait();
		}
	}
	catch (KParseException* e) {
		cout << *e << endl << flush;
		exit(-1);
	}
	catch (KRestException* e) {
		cout << *e << endl << flush;
		exit(-1);
	}

	return 0;
}
