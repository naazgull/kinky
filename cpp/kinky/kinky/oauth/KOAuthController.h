#pragma once

#include <time.h>
#include <string>
#include <iostream>
#include <kinky/lang/KObject.h>
#include <kinky/utils/SHA1.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;

	namespace rest {
		class KRestAPI;
	}

	namespace oauth {

		class  KOAuthController {
			public:
				 KOAuthController();
				 virtual ~KOAuthController();

				 virtual kinky::lang::KObject* getClient(string* client_id);

				 virtual kinky::lang::KObject* getScopes(string* ns, string* client_id);

				 virtual kinky::lang::KObject* getOwner(string* owner_id, string* owner_secret);

				 void setAPI(kinky::rest::KRestAPI* api);

				 kinky::rest::KRestAPI* getAPI();

				 static string* encryptClientSecret(string& password);

			private:
				 kinky::rest::KRestAPI* api;

		};
	}
}
