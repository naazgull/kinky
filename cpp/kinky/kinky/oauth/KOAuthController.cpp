#include <kinky/oauth/KOAuthController.h>
#include <kinky/rest/KRestAPI.h>

kinky::oauth::KOAuthController::KOAuthController() {
}

kinky::oauth::KOAuthController::~KOAuthController() {
}

kinky::lang::KObject* kinky::oauth::KOAuthController::getClient(string* client_id) {
	kinky::lang::KObject* ret = KOBJECT(
		"client_id" << "<some id>" <<
		"client_secret" << "<some_secret>" <<
		"client_redirecturl" << "some_redirecturl"
	);
	return ret;
}

kinky::lang::KObject* kinky::oauth::KOAuthController::getScopes(string* ns, string* client_id) {
	kinky::lang::KObject* ret = KOBJECT(
		"owner_id" << "<some id>" <<
		"scope_namespace" << "<some_scope namespace to include in the scope '@scope:'>" <<
		"scopes" << "<comma separated list of scopes>"
	);
	return ret;
}

kinky::lang::KObject* kinky::oauth::KOAuthController::getOwner(string* owner_id, string* owner_secret) {
	kinky::lang::KObject* ret = KOBJECT(
		"owner_id" << "<some id>" <<
		"owner_secret" << "<some_secret>" <<
		"owner_name" << "<some name>" <<
		"owner_email" << "<some email>" <<
		"owner_recovery_hash" << "<some recovery hash>"
	);
	return ret;
}

void kinky::oauth::KOAuthController::setAPI(kinky::rest::KRestAPI* api) {
	this->api = api;
}

kinky::rest::KRestAPI* kinky::oauth::KOAuthController::getAPI() {
	return this->api;
}

string* kinky::oauth::KOAuthController::encryptClientSecret(string& password) {
	CSHA1 sha1;
	sha1.Reset();
	sha1.Update((UINT_8*) password.c_str(), password.size());
	sha1.Final();

	unsigned char encripted_client_secret[1024];
	bzero(encripted_client_secret, 1024);
	sha1.GetHash(encripted_client_secret);

	istringstream in((const char*) encripted_client_secret);
	ostringstream out;

	kinky::utils::base64_encode(in, out);

	return new string(out.str());
}
