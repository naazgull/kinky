#include <kinky/oauth/controllers/TokenController.h>

#include <kinky/utils/KDateTime.h>
#include <kinky/oauth/controllers/AuthorizeController.h>

kinky::oauth::controllers::TokenController::TokenController() :
		KRestOperation("^/oauth/token$") {
}

KObject* kinky::oauth::controllers::TokenController::process(KObject* request, int directive, string* url, KToken* token) {
	// inputs
	string* grant_type = request->getString("grant_type");

	string* client_id = request->getString("client_id");
	string* client_secret = request->getString("client_secret");

	/* grant_type=authorization_code */
	string* code = request->getString("code");
	string* redirect_uri = request->getString("redirect_uri");

	/* grant_type=password */
	string* username = request->getString("username");
	string* password = request->getString("password");
	string* scope = request->getString("scope");

	/* grant_type=client_credentials */
	// apenas client_id e client_secret
	/* grant_type=refresh_token */
	// string* refresh_token = request->getString("refresh_token");
	// outputs
	string access_token = "";
	string token_type = "";
	string real_scope;
	int expires_in = 3600;

	int error_code = KHTTPResponse::OK;
	string error = "";
	string error_description = "";
	string error_uri = "";

	KProperties* config = this->getConfiguration();

	try {
		switch (directive) {
			// TODO remover GET, so para testes
			case KHTTPRequest::GET:
				case KHTTPRequest::POST: {
				string real_client_id;
				string real_client_secret;

				if (grant_type == NULL) {
					error_code = KHTTPResponse::BAD_REQUEST;
					error = "invalid_request";
					error_description = "Missing authorization grant type.";
					break;
				}

				// Por agora não suportamos client_credentials e refresh_token
				if (grant_type != NULL && *grant_type != "authorization_code" && *grant_type != "password"/* && *grant_type != "client_credentials" && *grant_type != "refresh_token"*/) {
					error_code = KHTTPResponse::BAD_REQUEST;
					error = "unsupported_grant_type";
					error_description = "The authorization grant type is not supported by the authorization server.";
					break;
				}

				string* authorization_header = request->getHeader("Authorization");
				if (authorization_header != NULL) {
					if (client_id != NULL || client_secret != NULL) {
						error_code = KHTTPResponse::BAD_REQUEST;
						error = "invalid_request";
						error_description = "The request utilizes more than one mechanism for authenticating the client.";
						break;
					}

					if (authorization_header->find("Basic") == 0) {
						string toUnserialize = authorization_header->substr(6);
						istringstream authorization_basic_in(toUnserialize);
						ostringstream authorization_basic_out;
						kinky::utils::base64_decode(authorization_basic_in, authorization_basic_out);

						size_t div_index = authorization_basic_out.str().find(":");

						if (div_index != string::npos) {
							real_client_id = authorization_basic_out.str().substr(0, div_index - 1);
							real_client_secret = authorization_basic_out.str().substr(div_index + 1);
							string* rcs = KOAuthController::encryptClientSecret(real_client_secret);
							real_client_secret.assign(rcs->data());
							delete rcs;
						}
						else {
							error_code = KHTTPResponse::UNAUTHORIZED;
							error = "invalid_client";
							error_description = "Invalid client authentication.";
							break;
						}
					}
					else {
						error_code = KHTTPResponse::UNAUTHORIZED;
						error = "invalid_client";
						error_description = "Unsupported client authentication.";
						break;
					}
				}
				else {
					if (client_id == NULL) {
						error_code = KHTTPResponse::UNAUTHORIZED;
						error = "invalid_client";
						error_description = "No client authentication included.";
						break;
					}

					real_client_id = *client_id;
					string* rcs = client_secret != NULL ? KOAuthController::encryptClientSecret(*client_secret) : new string("");
					real_client_secret.assign(rcs->data());
					delete rcs;
				}

				KObject* client_db = this->getAPI()->getOAuthController()->getClient(client_id);
				if (client_db == NULL) {
					error_code = KHTTPResponse::UNAUTHORIZED;
					error = "invalid_client";
					error_description = "Invalid client_id.";
					break;
				}

				if (((string) (*client_db)["client_secret"]) != real_client_secret) {
					error_code = KHTTPResponse::UNAUTHORIZED;
					error = "invalid_client";
					error_description = "Client authentication failed.";
					break;
				}

				string real_redirect_uri;

				if (redirect_uri != NULL && redirect_uri->find((string) (*client_db)["client_redirecturl"]) != 0) {
					error = "invalid_request";
					error_description = "Unauthorized redirect_uri.";
					break;
				}
				else if (redirect_uri != NULL) {
					real_redirect_uri.assign(*redirect_uri);
				}
				else {
					real_redirect_uri.assign((string) (*client_db)["client_redirecturl"]);
				}


				string to_process_scope;
				string real_user_id;

				if (*grant_type == "authorization_code") {
					if (code == NULL) {
						error_code = KHTTPResponse::BAD_REQUEST;
						error = "invalid_request";
						error_description = "Missing code parameter.";
						break;
					}

					unsigned int indexOfPipe = code->find('|');
					if (indexOfPipe == string::npos) {
						error_code = KHTTPResponse::BAD_REQUEST;
						error = "invalid_grant";
						error_description = "Invalid code.";
						break;
					}

					string toUnserialize = code->substr(indexOfPipe + 1);
					istringstream in(toUnserialize);
					ostringstream out;
					kinky::utils::base64_decode(in, out);

					pair < string, size_t > *decrypted = kinky::utils::encryption::KXORCipher::decipher(out.str(), config->get("ktoken_encryption_private_key"));

					stringstream ss(decrypted->first);

					string code_client_id;
					string code_redirect_uri;
					string code_scope;
					string code_user_id;

					std::getline(ss, code_client_id, ':');
					std::getline(ss, code_redirect_uri, ':');
					std::getline(ss, code_scope, ':');
					std::getline(ss, code_user_id, ':');

					if (code_redirect_uri.length() > 0) {
						ostringstream out_code;
						kinky::utils::url_decode(code_redirect_uri, out_code);

						code_redirect_uri = out_code.str();
					}

					if (code_scope.length() > 0) {
						istringstream in_code(code_scope);
						ostringstream out_code;
						kinky::utils::base64_decode(in_code, out_code);

						code_scope = out_code.str();
					}

					// TODO Como saber se code já foi usado, para poder enviar invalid_grant?

					if (code_client_id != real_client_id) {
						error_code = KHTTPResponse::BAD_REQUEST;
						error = "invalid_grant";
						error_description = "Code was issued to another client.";
						break;
					}

					if (code_redirect_uri != real_redirect_uri) {
						error_code = KHTTPResponse::BAD_REQUEST;
						error = "invalid_grant";
						error_description = "redirect_uri does not match the redirection URI used in the authorization request.";
						break;
					}

					real_user_id = code_user_id;

					to_process_scope = code_scope;
				}

				if (*grant_type == "password") {
					if (username == NULL) {
						error_code = KHTTPResponse::BAD_REQUEST;
						error = "invalid_request";
						error_description = "Missing username parameter.";
						break;
					}

					if (password == NULL) {
						error_code = KHTTPResponse::BAD_REQUEST;
						error = "invalid_request";
						error_description = "Missing password parameter.";
						break;
					}

					string* encoded_encripted_client_secret = KOAuthController::encryptClientSecret(*password);

					KObject* rs = this->getAPI()->getOAuthController()->getOwner(username, encoded_encripted_client_secret);
					delete encoded_encripted_client_secret;

					if (rs == NULL) {
						error_code = KHTTPResponse::BAD_REQUEST;
						error = "invalid_grant";
						error_description = "Invalid resource owner credentials.";
						break;
					}

					real_user_id.assign((string) (*rs)["owner_id"]);

					if (scope != NULL)
					to_process_scope = *scope;

					delete rs;
				}

				real_scope = AuthorizeController::processScopes(this->getAPI(), to_process_scope, real_user_id);

				if (real_scope.length() == 0) {
					error_code = KHTTPResponse::BAD_REQUEST;
					error = "invalid_scope";
					error_description = "The requested scope is invalid, unknown or malformed.";
					break;
				}

				string client_secret = (string) (*client_db)["client_secret"];

				KToken* ktoken = new KToken(*client_id, client_secret, real_user_id, real_scope, time(NULL) + expires_in);
				ktoken->setPrivateKey(new string(config->get("ktoken_encryption_private_key").data()));

				ostringstream oss;
				oss << *ktoken << flush;
				access_token.assign(oss.str());
				token_type = "bearer";

				delete ktoken;
				delete client_db;
				break;
			}
		}
	}
	catch (std::exception& e) {
		(*kinky::io::out) << "[" << kinky::utils::date::format("") << " ERROR] Caught at " << __FILE__ << ":" << __LINE__ << ": " << e.what();
		error_code = KHTTPResponse::BAD_REQUEST;
		error = "server_error";
		error_description = "Unknown error. ";
		error_description.append(e.what());
	}

	if (error_code == KHTTPResponse::OK) {
		KObject* to_return = new KObject();
		to_return->setStatus(error_code);
		to_return->addHeader("Cache-Control", "no-store");
		to_return->addHeader("Pragma", "no-cache");

		if (access_token.length() > 0) {
			to_return->setString("access_token", access_token);
		}

		if (token_type.length() > 0) {
			to_return->setString("token_type", token_type);
		}

		if (expires_in > 0) {
			to_return->setInteger("expires_in", expires_in);
		}

		if (real_scope.length() > 0) {
			to_return->setString("scope", real_scope);
		}
		return to_return;
	}

	return NULL;
}

/*
 HTTP/1.1 200 OK
 Content-Type: application/json;charset=UTF-8
 Cache-Control: no-store
 Pragma: no-cache

 {
 "access_token":"2YotnFZFEjr1zCsicMWpAA",
 "token_type":"example",
 "expires_in":3600,
 "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA",
 "example_parameter":"example_value"
 }
 */

/*
 HTTP/1.1 400 Bad Request
 Content-Type: application/json;charset=UTF-8
 Cache-Control: no-store
 Pragma: no-cache

 {
 "error":"invalid_request",
 "error_description":"example",
 "error_uri":3600
 }
 */

/*
 invalid_request
 The request is missing a required parameter, includes an
 unsupported parameter value, repeats a parameter,
 includes multiple credentials, utilizes more than one
 mechanism for authenticating the client, or is otherwise
 malformed.

 invalid_client
 Client authentication failed (e.g. unknown client, no
 client authentication included, or unsupported
 authentication method).  The authorization server MAY
 return an HTTP 401 (Unauthorized) status code to indicate
 which HTTP authentication schemes are supported.  If the
 client attempted to authenticate via the "Authorization"
 request header field, the authorization server MUST
 respond with an HTTP 401 (Unauthorized) status code, and
 include the "WWW-Authenticate" response header field
 matching the authentication scheme used by the client.

 invalid_grant
 The provided authorization grant (e.g. authorization
 code, resource owner credentials) is invalid, expired,
 revoked, does not match the redirection URI used in the
 authorization request, or was issued to another client.

 unauthorized_client
 The authenticated client is not authorized to use this
 authorization grant type.

 unsupported_grant_type
 The authorization grant type is not supported by the
 authorization server.

 invalid_scope
 The requested scope is invalid, unknown, malformed, or
 exceeds the scope granted by the resource owner.
 */
