#pragma once

#include <string>
#include <kinky/rest/KRestOperation.h>

using namespace std;
using namespace __gnu_cxx;

using namespace kinky::oauth;
using namespace kinky::lang;
using namespace kinky::utils;
using namespace kinky::rest;
using namespace kinky::rest::http;

namespace kinky {
	namespace oauth {
		namespace controllers {
			class LoginController: public KRestOperation {
				public:
					LoginController();

					virtual ~LoginController() {
					}

					KObject* process(KObject* request, int directive, string* url, KToken* token);
			};
		}
	}
}
