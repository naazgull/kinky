#pragma once

#include <string>
#include <kinky/rest/KRestOperation.h>
#include <kinky/rest/KRestAPI.h>

using namespace std;
using namespace __gnu_cxx;

using namespace kinky::oauth;
using namespace kinky::lang;
using namespace kinky::utils;
using namespace kinky::rest;
using namespace kinky::rest::http;

namespace kinky {
	namespace oauth {
		namespace controllers {
			class AuthorizeController: public KRestOperation {
				public:
					AuthorizeController();

					virtual ~AuthorizeController() {
					}

					KObject* process(KObject* request, int directive, string* url, KToken* token);

					static string processScopes(KRestAPI* api, string scopes, string user_id);
			};
		}
	}
}
