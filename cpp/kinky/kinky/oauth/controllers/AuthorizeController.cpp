#include <kinky/oauth/controllers/AuthorizeController.h>

#include <stdlib.h>
#include <kinky/utils/KDateTime.h>

kinky::oauth::controllers::AuthorizeController::AuthorizeController() :
		KRestOperation("^/oauth/authorize$") {
}

KObject* kinky::oauth::controllers::AuthorizeController::process(KObject* request, int directive, string* url, KToken* token) {
	// inputs
	string* response_type = request->getString("response_type");

	string* client_id = request->getString("client_id");
	string* redirect_uri = request->getString("redirect_uri");
	string* scope = request->getString("scope");
	string* state = request->getString("state");

	string* display = request->getString("display");

	string* denied = request->getString("denied");

	// outputs
	string real_redirect_uri;

	string code = "";

	string access_token = "";
	string token_type = "";
	string real_scope;

	int expires_in = 3600;

	string error = "";
	string error_description = "";
	string error_uri = "";

	KProperties* config = this->getConfiguration();

	try {
		switch (directive) {
			case KHTTPRequest::GET: {
				if (response_type == NULL) {
					error = "invalid_request";
					error_description = "Missing response_type parameter.";
					break;
				}

				if (client_id == NULL) {
					error = "invalid_request";
					error_description = "Missing client_id parameter.";
					break;
				}

				if (*response_type != "token" && *response_type != "code") {
					error = "unsupported_response_type";
					error_description = "The authorization server does not support obtaining an authorization code using this method.";
					break;
				}

				if (display != NULL && *display != "page" && *display != "popup" && *display != "iframe") {
					error = "invalid_request";
					error_description = "Invalid display parameter.";
					break;
				}

				KObject* client_db = this->getAPI()->getOAuthController()->getClient(client_id);
				if (client_db == NULL) {
					error = "unauthorized_client";

					if (*response_type == "token")
					error_description = "The client is not authorized to request an access token.";
					else if (*response_type == "code")
					error_description = "The client is not authorized to request an authorization code.";

					break;
				}

				if ((*client_db)["client_redirecturl"] == NULL_KOBJECT && !(bool) (*client_db)["client_noredirect"]) {
					error = "invalid_request";
					error_description = "Unauthorized redirect_uri.";
					break;
				}
				else if (redirect_uri != NULL && redirect_uri->find((string) (*client_db)["client_redirecturl"]) != 0) {
					error = "invalid_request";
					error_description = "Unauthorized redirect_uri.";
					break;
				}
				else if (redirect_uri != NULL) {
					real_redirect_uri.assign(*redirect_uri);
				}
				else if ((*client_db)["client_redirecturl"] != NULL_KOBJECT) {
					real_redirect_uri.assign((string) (*client_db)["client_redirecturl"]);
				}

				if (denied != NULL) {
					error = "access_denied";
					error_description = "The user denied your request.";
					break;
				}

				if (token == NULL || token->getUserID() == NULL) {
					ostringstream cancel;
					kinky::utils::url_encode(real_redirect_uri, cancel);
					if (*response_type == "token") {
						if (real_redirect_uri.find("#") == string::npos)
						cancel << "%23";
						else
						cancel << "%26";
					}
					else {
						if (real_redirect_uri.find("?") == string::npos)
						cancel << "%3F";
						else
						cancel << "%26";
					}

					cancel << "error%3Daccess_denied%26error_description=";
					kinky::utils::url_encode("The user denied your request.", cancel);
					cancel << flush;

					ostringstream next;
					kinky::utils::url_encode(config->get("base_url"), next);
					kinky::utils::url_encode("/oauth/?response_type=", next);
					next << *response_type;
					next << "%26client_id%3D";
					kinky::utils::url_encode(*client_id, next);
					if (redirect_uri != NULL) {
						next << "%26redirect_uri%3D";
						kinky::utils::url_encode(*redirect_uri, next);
					}
					if (scope != NULL) {
						next << "%26scope%3D";
						kinky::utils::url_encode(*scope, next);
					}
					if (state != NULL) {
						next << "%26state%3D";
						kinky::utils::url_encode(*state, next);
					}
					if (display != NULL) {
						next << "%26display%3D";
						kinky::utils::url_encode(*display, next);
					}

					next << flush;

					ostringstream oss;
					oss << config->get("base_url") << "/login/?client_id=" << *client_id << "&display=" << (display != NULL ? *display : "page");
					oss << "&cancel_url=";
					kinky::utils::url_encode(cancel.str(), oss);
					oss << "&next=";
					kinky::utils::url_encode(next.str(), oss);
					oss << flush;

					KObject* to_return = new KObject();
					to_return->addHeader("Cache-Control", "no-store");
					to_return->addHeader("Pragma", "no-cache");
					to_return->addHeader("Location", oss.str());
					to_return->setStatus(KHTTPResponse::TEMPORARY_REDIRECT);

					return to_return;
				}

				if (scope == NULL) {
					error = "invalid_scope";
					error_description = "The requested scope is invalid, unknown or malformed.";
					break;
				}

				real_scope = AuthorizeController::processScopes(this->getAPI(), *scope, *token->getUserID());

				if (real_scope.length() == 0) {
					error = "invalid_scope";
					error_description = "The requested scope is invalid, unknown or malformed.";
					break;
				}

				if (*response_type == "token") {
					string client_secret = (string) (*client_db)["client_secret"];
					string user_id(*token->getUserID());
					user_id.insert(0, "default:");
					KToken* ktoken = new KToken(*client_id, client_secret, user_id, real_scope, time(NULL) + expires_in);
					ktoken->setPrivateKey(new string(config->get("ktoken_encryption_private_key").data()));

					ostringstream oss;
					oss << *ktoken << flush;
					access_token.assign(oss.str());
					token_type = "bearer";

					delete ktoken;
				}

				if (*response_type == "code") {
					ostringstream data;
					data << *client_id << ":";
					kinky::utils::url_encode(real_redirect_uri, data);
					data << ":";
					if (real_scope.length() > 0) {
						stringstream in(real_scope);
						kinky::utils::base64_encode(in, data);
					}
					data << ":";
					data << *token->getUserID() << ":";
					data.flush();

					string padded = kinky::utils::encryption::KXORCipher::pad(data.str(), 32, ':');

					pair < string, size_t > *encrypted = kinky::utils::encryption::KXORCipher::cipher(padded, config->get("ktoken_encryption_private_key"));
					stringstream in(encrypted->first);
					ostringstream out;
					out << encrypted->second << "|" << flush;
					kinky::utils::base64_encode(in, out);
					out << flush;

					delete encrypted;

					code = out.str();
				}

				delete client_db;
				break;
			}
		}

	}
	catch (std::exception& e) {
		(*kinky::io::out) << "[" << kinky::utils::date::format("") << " ERROR] Caught at " << __FILE__ << ":" << __LINE__ << ": " << e.what();
		error = "server_error";
		error_description = "Unknown error. ";
		error_description.append(e.what());
	}

	if (real_redirect_uri.length() > 0) {
		KObject* to_return = new KObject();
		to_return->addHeader("Cache-Control", "no-store");
		to_return->addHeader("Pragma", "no-cache");

		ostringstream oss;
		oss << real_redirect_uri;
		if (response_type != NULL && *response_type == "token") {
			if (real_redirect_uri.find("#") == string::npos)
			oss << "#";
			else
			oss << "&";
		}
		else {
			if (real_redirect_uri.find("?") == string::npos)
			oss << "?";
			else
			oss << "&";
		}

		if (error.length() > 0) {
			oss << "error=" << error << "&";
		}

		if (error_description.length() > 0) {
			oss << "error_description=" << error_description << "&";
		}

		if (error_uri.length() > 0) {
			oss << "error_uri=" << error_uri << "&";
		}

		if (access_token.length() > 0) {
			oss << "access_token=" << access_token << "&";

			if (token_type.length() > 0) {
				oss << "token_type=" << token_type << "&";
			}

			if (expires_in > 0) {
				oss << "expires_in=" << expires_in << "&";
			}

			if (real_scope.length() > 0) {
				oss << "scope=" << real_scope << "&";
			}
		}

		if (code.length() > 0) {
			oss << "code=" << code << "&";
		}

		if (state != NULL) {
			oss << "state=" << *state;
		}

		oss << flush;

		to_return->addHeader("Location", oss.str());
		to_return->setStatus(KHTTPResponse::TEMPORARY_REDIRECT);

		return to_return;
	}
	else {
		KObject* to_return = new KObject();
		to_return->addHeader("Cache-Control", "no-store");
		to_return->addHeader("Pragma", "no-cache");

		if (access_token.length() > 0) {
			ostringstream oss;
			oss << "OAuth2.0 " << access_token;
			to_return->addHeader("X-Access-Token", oss.str());
		}

		if (code.length() > 0) {
			ostringstream oss;
			oss << "OAuth2.0 " << code;
			to_return->addHeader("X-Access-Code", code);
		}

		to_return->setStatus(KHTTPResponse::OK);

		return to_return;
	}

	return NULL;
	/* response_type=code */
	/*
	 HTTP/1.1 302 Found
	 Location: ...?code=asdasdasd&state=xyz
	 */

	/* response_type=token */
	/*
	 HTTP/1.1 302 Found
	 Location: ...#access_token=2YotnFZFEjr1zCsicMWpAA&token_type=bearer&expires_in=3600&scope=xyz&state=xyz
	 */

	/*
	 HTTP/1.1 302 Found
	 Location: ...(?#)error=access_denied&state=xyz
	 */

	/*
	 invalid_request
	 The request is missing a required parameter, includes an
	 invalid parameter value, or is otherwise malformed.

	 unauthorized_client
	 The client is not authorized to request an authorization
	 code using this method.

	 access_denied
	 The resource owner or authorization server denied the
	 request.

	 unsupported_response_type
	 The authorization server does not support obtaining an
	 authorization code using this method.

	 invalid_scope
	 The requested scope is invalid, unknown, or malformed.

	 server_error
	 The authorization server encountered an unexpected
	 condition which prevented it from fulfilling the request.

	 temporarily_unavailable
	 The authorization server is currently unable to handle
	 the request due to a temporary overloading or maintenance
	 of the server.
	 */
}

string kinky::oauth::controllers::AuthorizeController::processScopes(KRestAPI* api, string scopes, string user_id) {
	map<string, vector<string> > filtered_namespace_roles;
	ostringstream real_scope;
	istringstream iss_scopes(scopes);
	string scope_namespace_roles;

	while (std::getline(iss_scopes, scope_namespace_roles, '@')) {
		if (scope_namespace_roles.length() == 0) {
			continue;
		}

		size_t div_index = scope_namespace_roles.find(":");
		string ns = div_index != string::npos ? scope_namespace_roles.substr(0, div_index) : scope_namespace_roles;


		KObject* oauth_scope_db = api->getOAuthController()->getScopes(&ns, &user_id);

		if (oauth_scope_db == NULL) {
			continue;
		}

		string roles = div_index != string::npos ? scope_namespace_roles.substr(div_index + 1) : (string) (*oauth_scope_db)["scopes"];

		unsigned int role_count = 0;
		string real_roles;

		istringstream iss_roles(roles);
		string role;
		while (std::getline(iss_roles, role, ',')) {
			if (((string) (*oauth_scope_db)["scopes"]).find(role) != string::npos) {
				vector<string>::iterator it_filtered_namespace_roles = find(filtered_namespace_roles[ns].begin(), filtered_namespace_roles[ns].end(), role);
				if (it_filtered_namespace_roles == filtered_namespace_roles[ns].end()) {
					filtered_namespace_roles[ns].push_back(role);
				}

				if (role_count > 0) {
					real_roles.append(",");
				}
				real_roles.append(role);
				++role_count;
			}
		}

		if (real_roles.length() > 0) {
			real_scope << "@" << ns << ":" << real_roles;
		}

		delete oauth_scope_db;
	}

	real_scope << flush;
	return *(new managed_ptr<string>(new string(real_scope.str())))->get();
}
