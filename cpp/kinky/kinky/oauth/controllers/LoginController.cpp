#include <kinky/oauth/controllers/LoginController.h>

#include <kinky/utils/KDateTime.h>
#include <kinky/oauth/controllers/AuthorizeController.h>

kinky::oauth::controllers::LoginController::LoginController() :
	KRestOperation("^/oauth/([0-9a-zA-Z\\-\\:_@\\. ])+/login$") {
}

KObject* kinky::oauth::controllers::LoginController::process(KObject* request, int directive, string* url, KToken* token) {
	size_t user_id_end_index = url->find("/", 7);

	string user_id = url->substr(7, user_id_end_index - 7);

	KObject* to_return = NULL;
	KRestException* to_throw = NULL;

	KObject* user = NULL;

	string* owner_secret = request->getString("password");
	string* redirect_uri = request->getString("redirect_uri");
	bool* persistent = request->getBoolean("persistent");

	try {
		switch (directive) {
			case KHTTPRequest::POST: {
				string* encoded_encripted_owner_secret = KOAuthController::encryptClientSecret(*owner_secret);

				KObject* rs = NULL;

				try {
					rs = this->getAPI()->getOAuthController()->getOwner(&user_id, encoded_encripted_owner_secret);
					if (rs != NULL) {
						user = rs;
					}
					else {
						if (redirect_uri != NULL) {
							to_throw = new KRestException(KHTTPResponse::SEE_OTHER, *redirect_uri, 701);
						}
						else {
							to_throw = new KRestException(KHTTPResponse::UNAUTHORIZED, "invalid login");
						}
					}
				}
				catch (KRestException* e) {
					if (redirect_uri != NULL) {
						to_throw = new KRestException(KHTTPResponse::SEE_OTHER, *redirect_uri, e->getErrorCode());
						delete e;
					}
					else {
						to_throw = e;
					}
				}
				delete encoded_encripted_owner_secret;
				break;
			}

			default:
				ostringstream oss;
				oss << "the provided path '" << *url << "' could not be found" << flush;

				to_throw = new KRestException(KHTTPResponse::METHOD_NOT_ALLOWED, oss.str());
				break;
		}
	}
	catch (std::exception& e) {
		if (user != NULL) {
			delete user;
			user = NULL;
		}
		if (redirect_uri != NULL) {
			to_throw = new KRestException(KHTTPResponse::SEE_OTHER, *redirect_uri, 800);
		}
		else {
			to_throw = new KRestException(KHTTPResponse::INTERNAL_SERVER_ERROR, "UserLoginController.h: Unprocessed and unknown error");
		}
	}

	if (user != NULL) {
		time_t expires_in = persistent != NULL && *persistent ? 0 : time(NULL) + 3600 * 24 * 60;
		string* owner_password = (string*) (*user)["owner_password"];
		string user_id((string) (*user)["owner_id"]);
		user_id.insert(0, "default:");

		KToken* ktoken = new KToken(*token->getClientID(), owner_password != NULL ? *owner_password : "", user_id, this->getConfiguration()->get("led_oauththirdparty_token_scopes"), persistent != NULL && *persistent ? 0 : expires_in);
		ktoken->setPrivateKey(new string(this->getConfiguration()->get("ktoken_encryption_private_key").data()));
		KObject* logged_user = new KObject();

		to_return = new KObject();
		to_return->setObject("user", logged_user);

		to_return->addHeader("Cache-Control", "no-store");
		to_return->addHeader("Pragma", "no-cache");

		ostringstream eiss;
		eiss << (expires_in - 1) << flush;
		if (redirect_uri != NULL) {
			ostringstream oss;

			string redirect = string(curl_unescape(redirect_uri->data(), 0));

			oss << redirect;
			if (redirect.find("?") == string::npos) {
				oss << "?";
			}
			else {
				oss << "&";
			}
			oss << "access_token=";
			ostringstream koss;
			koss << *ktoken << flush;
			koss << "&expires_in=" << flush;
			koss << eiss.str() << flush;
			kinky::utils::url_encode(koss.str(), oss);
			oss << flush;

			to_return->addHeader("Location", oss.str());
			to_return->setStatus(KHTTPResponse::SEE_OTHER);
		}
		else {
			ostringstream oss;
			oss << "OAuth2.0 " << *ktoken << flush;

			to_return->addHeader("X-Access-Token", oss.str());
			to_return->addHeader("X-Access-Token-Expires", eiss.str());
			to_return->setStatus(KHTTPResponse::OK);

			oss.str("");
			oss << *ktoken << flush;
			to_return->setString("user_id", user_id);
			to_return->setString("access_token", oss.str());
			to_return->setString("expires_in", eiss.str());
		}

		delete ktoken;
		delete user;
	}

	if (to_throw != NULL) {
		if (to_return != NULL) {
			delete to_return;
		}
		throw to_throw;
	}

	return to_return;
}
