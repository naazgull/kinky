#include <kinky/oauth/KToken.h>
#include <kinky/utils/KBase64.h>
#include <kinky/utils/KXORCipher.h>
#include <kinky/memory/KMaps.h>
#include <zlib.h>

kinky::oauth::KToken::KToken() :
		clientID(NULL), clientSecret(NULL), userID(NULL), scopes(NULL), expirationDate(-1), ipAddress(NULL), data(NULL), privKey(new string("64ec90eXLh0rkDsK3cBly6dmRDBVnhm41e170a6cbc9189e44cb083e3PD9PKs6xIf1eBkH0ooc")) {
}

kinky::oauth::KToken::KToken(string token, string privKeyParam) :
		clientID(NULL), clientSecret(NULL), userID(NULL), scopes(NULL), expirationDate(-1), ipAddress(NULL), data(NULL), privKey(new string(privKeyParam)) {

	string unserializedToken;
	kinky::utils::encryption::KXORCipher::decrypt(&unserializedToken, token, *this->privKey);

	stringstream ss(unserializedToken);

	string *item = new string();
	std::getline(ss, *item, '|');
	this->setClientID(item);

	item = new string();
	std::getline(ss, *item, '|');
	this->setClientSecret(item);

	item = new string();
	std::getline(ss, *item, '|');
	this->setScopes(item);

	item = new string();
	std::getline(ss, *item, '|');
	this->setUserID(item);

	item = new string();
	std::getline(ss, *item, '|');
	if (item->length() != 0) {
		istringstream tss(*item);
		long timestamp;
		tss >> timestamp;
		this->setExpirationDate(timestamp);
		delete item;

		item = new string();
		std::getline(ss, *item, '|');
		if (item->length() != 0) {
			this->setData(item);
		}
		else {
			delete item;
		}
	}
	else {
		delete item;
	}

}

kinky::oauth::KToken::KToken(string *clientIDParam, string *clientSecretParam, string *userIDParam, string* scopesParam, long expirationDateParam) :
		clientID(clientIDParam), clientSecret(clientSecretParam), userID(userIDParam), scopes(scopesParam), expirationDate(expirationDateParam), ipAddress(NULL), data(NULL), privKey(new string("64ec90eXLh0rkDsK3cBly6dmRDBVnhm41e170a6cbc9189e44cb083e3PD9PKs6xIf1eBkH0ooc")) {
}

kinky::oauth::KToken::KToken(string clientIDParam, string clientSecretParam, string userIDParam, long expirationDateParam) :
		clientID(new string(clientIDParam)), clientSecret(new string(clientSecretParam)), userID(new string(userIDParam)), scopes(NULL), expirationDate(expirationDateParam), ipAddress(NULL), data(NULL), privKey(new string("64ec90eXLh0rkDsK3cBly6dmRDBVnhm41e170a6cbc9189e44cb083e3PD9PKs6xIf1eBkH0ooc")) {
}

kinky::oauth::KToken::KToken(string clientIDParam, string clientSecretParam, string userIDParam, string scopesParam, long expirationDateParam) :
		clientID(new string(clientIDParam)), clientSecret(new string(clientSecretParam)), userID(new string(userIDParam)), scopes(new string(scopesParam)), expirationDate(expirationDateParam), ipAddress(NULL), data(NULL), privKey(new string("64ec90eXLh0rkDsK3cBly6dmRDBVnhm41e170a6cbc9189e44cb083e3PD9PKs6xIf1eBkH0ooc")) {
}

kinky::oauth::KToken::~KToken() {
	if (this->clientID != NULL) {
		delete this->clientID;
	}
	if (this->clientSecret != NULL) {
		delete this->clientSecret;
	}
	if (this->scopes != NULL) {
		delete this->scopes;
	}
	if (this->userID != NULL) {
		delete this->userID;
	}
	if (this->ipAddress != NULL) {
		delete this->ipAddress;
	}
	if (this->data != NULL) {
		delete this->data;
	}
	if (this->privKey != NULL) {
		delete this->privKey;
	}
}
string* kinky::oauth::KToken::getClientID() const {
	return this->clientID;
}

string* kinky::oauth::KToken::getClientSecret() const {
	return this->clientSecret;
}

long kinky::oauth::KToken::getExpirationDate() const {
	return this->expirationDate;
}

/* DEPRECATED: use getScopes instead */
string* kinky::oauth::KToken::getPermissions() const {
	return this->getScopes();
}

string* kinky::oauth::KToken::getScopes() const {
	return this->scopes;
}

string* kinky::oauth::KToken::getUserID() const {
	return this->userID;
}

string* kinky::oauth::KToken::getIPAddress() {
	return this->ipAddress;
}

string* kinky::oauth::KToken::getData() {
	return this->data;
}

void kinky::oauth::KToken::setExpirationDate(long expirationDate) {
	this->expirationDate = expirationDate;
}

/* DEPRECATED: use setScopes instead */
void kinky::oauth::KToken::setPermissions(string* scopes) {
	this->setScopes(scopes);
}

void kinky::oauth::KToken::setScopes(string* scopes) {
	if (this->scopes != NULL) {
		delete this->scopes;
	}
	this->scopes = scopes;
}

void kinky::oauth::KToken::setIPAddress(string* ipAddress) {
	if (this->ipAddress != NULL) {
		delete this->ipAddress;
	}
	this->ipAddress = ipAddress;
}

void kinky::oauth::KToken::setUserID(string* userID) {
	if (this->userID != NULL) {
		delete this->userID;
	}
	this->userID = userID;
}

void kinky::oauth::KToken::setClientID(string* clientID) {
	if (this->clientID != NULL) {
		delete this->clientID;
	}
	this->clientID = clientID;
}

void kinky::oauth::KToken::setClientSecret(string* clientSecret) {
	if (this->clientSecret != NULL) {
		delete this->clientSecret;
	}
	this->clientSecret = clientSecret;
}

void kinky::oauth::KToken::setData(string* data) {
	if (this->data != NULL) {
		delete this->data;
	}
	this->data = data;
}

void kinky::oauth::KToken::setPrivateKey(string* privKey) {
	if (this->privKey != NULL) {
		delete this->privKey;
	}
	this->privKey = privKey;
}

bool kinky::oauth::KToken::isValid() {
	return this->clientID != NULL && this->clientSecret != NULL && this->scopes != NULL && this->userID != NULL;
}

void kinky::oauth::KToken::stringify(ostream& out) {
	ostringstream oss;
	oss << (this->clientID != NULL ? *this->clientID : "") << "|" << (this->clientSecret != NULL ? *this->clientSecret : "");

	oss << "|";
	if(this->scopes != NULL) {
		oss << *this->scopes;
	}

	oss << "|";
	if (this->userID != NULL) {
		oss << *this->userID;
	}

	if (this->expirationDate != -1) {
		oss << "|" << this->expirationDate;
	}

	if (this->data != NULL) {
		oss << (this->expirationDate == -1 ? "|" : "") << "|" << *this->data;
	}
	oss << flush;

	string serializedToken;
	kinky::utils::encryption::KXORCipher::encrypt(&serializedToken, oss.str(), *this->privKey);
	out << serializedToken << flush;
}

void kinky::oauth::KToken::stringify(string* out) {
	ostringstream oss;
	oss << (this->clientID != NULL ? *this->clientID : "") << "|" << (this->clientSecret != NULL ? *this->clientSecret : "");

	oss << "|";
	if(this->scopes != NULL) {
		oss << *this->scopes;
	}

	oss << "|";
	if (this->userID != NULL) {
		oss << *this->userID;
	}

	if (this->expirationDate != -1) {
		oss << "|" << this->expirationDate;
	}

	if (this->data != NULL) {
		oss << (this->expirationDate == -1 ? "|" : "") << "|" << *this->data;
	}
	oss << flush;

	kinky::utils::encryption::KXORCipher::encrypt(out, oss.str(), *this->privKey);
}

string* kinky::oauth::KToken::prettify(string separator) {
	string out;
	ostringstream oss;
	oss << (this->clientID != NULL ? *this->clientID : "") << separator << (this->clientSecret != NULL ? *this->clientSecret : "") << separator << (this->scopes != NULL ? *this->scopes : "") << separator << (this->userID != NULL ? *this->userID : "");
	if (this->expirationDate != -1) {
		oss << separator << this->expirationDate;
	}
	if (this->data != NULL) {
		oss << (this->expirationDate == -1 ? separator : "") << separator << *this->data;
	}
	return (new managed_ptr<string>(new string(oss.str())))->get();
}
