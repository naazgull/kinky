#pragma once

#include <time.h>
#include <string>
#include <iostream>
#include <kinky/rest/http/KHTTPResponse.h>

using namespace std;
using namespace __gnu_cxx;
using namespace kinky::rest;

namespace kinky {

	namespace oauth {

		class KToken {
			public:
				KToken();
				KToken(string token, string privKey = "64ec90eXLh0rkDsK3cBly6dmRDBVnhm41e170a6cbc9189e44cb083e3PD9PKs6xIf1eBkH0ooc");
				KToken(string *clientIDParam, string *clientSecretParam, string *userIDParam, string* scopesParam, long expirationDateParam);
				KToken(string clientIDParam, string clientSecretParam, string userIDParam, long expirationDateParam);
				KToken(string clientIDParam, string clientSecretParam, string userIDParam, string scopesParam, long expirationDateParam);
				virtual ~KToken();

				string *getClientID() const;

				string *getClientSecret() const;

				long getExpirationDate() const;

				/* DEPRECATED: use getScopes instead */
				string* getPermissions() const;

				string* getScopes() const;

				string* getUserID() const;

				string* getIPAddress();

				string* getData();

				void setExpirationDate(long expirationDate);

				/* DEPRECATED: use setScopes instead */
				void setPermissions(string* scopes);

				void setScopes(string* scopes);

				void setIPAddress(string* ipAddress);

				void setUserID(string* userID);

				void setClientID(string* clientID);

				void setClientSecret(string* clientSecret);

				void setData(string* data);

				void setPrivateKey(string* privKey);

				bool isValid();

				void stringify(ostream& out);

				void stringify(string* out);

				string* prettify(string separator = "|");

				friend ostream& operator<<(ostream& os, KToken& f) {
					f.stringify(os);
					return os;
				}

			private:
				string *clientID;
				string *clientSecret;
				string* userID;
				string* scopes;
				long expirationDate;
				string* ipAddress;
				string* data;
				string* privKey;

		};
	}
}
