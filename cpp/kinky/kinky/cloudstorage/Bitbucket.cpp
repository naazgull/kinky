#include <kinky/cloudstorage/Bitbucket.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

kinky::lang::KObject* kinky::cloudstorage::bitbucket::get(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params) {
	string url("https://bitbucket.org/api/1.0");
	url.append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0";
	params->sort();

	params->addHeader("Accept", "application/json");
	KObject* profile_data = kinky::social::oauth1::get(url.data(), params, consumerSecret, tokenSecret);

	delete nonce;
	delete timestamp;

	return profile_data;
}

kinky::lang::KObject* kinky::cloudstorage::bitbucket::get(const char* hash, const char* user, const char* pass, KObject* params) {
	string url("https://bitbucket.org/api/1.0");
	url.append(hash);
	string fields = "";
	bool first = true;

	for (KObject::KObjAttributes::iterator it = params->begin(); it != params->end(); it++) {
		string key = (*it)->key;
		void* val = (*it)->value->pointed();

		string escaped_key = kinky::utils::KUrlEncodeRfc3986::encode(key);
		string escaped_value = kinky::utils::KUrlEncodeRfc3986::encode(static_cast<string*>(val)->data());

		if (!first) {
			fields.append("&");
		}

		first = false;

		fields.append(escaped_key);
		fields.append("=");
		fields.append(escaped_value);
	}

	string buffer;
	char errorBuffer[CURL_ERROR_SIZE];

	url.insert(url.length(), (fields.length() > 0 ? "?" + fields : ""));

	CURL* curl = curl_easy_init();

	OpenSSL_add_all_algorithms();
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_URL, url.data());
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, social::common::curlWriter);

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);

	struct curl_slist* headers = NULL;

	KObject::KObjHeaders* reqheaders = params->getHeaders();
	if (reqheaders != NULL) {
		for (KObject::KObjHeaders::iterator it = reqheaders->begin(); it != reqheaders->end(); it++) {
			ostringstream oss;
			string name(it->first);
			kinky::utils::prettify_header_name(name);
			oss << name << ": " << **(it->second);
			headers = curl_slist_append(headers, oss.str().data());
		}
	}

	string userpass(user);
	userpass.insert(userpass.length(), ":");
	userpass.insert(userpass.length(), pass);
	curl_easy_setopt( curl, CURLOPT_USERPWD, userpass.data());
	headers = curl_slist_append(headers, "Expect:");

	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	CURLcode result = curl_easy_perform(curl);

	if (result == CURLE_OK) {
		string* toParse = new string(buffer);
		KObject* toReturn = kinky::social::common::parseResponse(curl, toParse, params);

		delete toParse;

		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		return toReturn;
	}
	else {
		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		throw new KRestException(result, errorBuffer);
	}
}

kinky::lang::KObject* kinky::cloudstorage::bitbucket::post(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params, KObject* body) {
	string url("https://bitbucket.org/api/1.0");
	url.append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0";
	params->sort();

	if (body != NULL) {
		for (KObject::KObjAttributes::iterator it = body->begin(); it != body->end(); it++) {
			(*params) << (**it).key << (string) (**it);
		}
	}

	params->addHeader("Accept", "application/json");
	KObject* profile_data = kinky::social::oauth1::post(url.data(), params, consumerSecret, tokenSecret, NULL);

	delete nonce;
	delete timestamp;

	return profile_data;
}

kinky::lang::KObject* kinky::cloudstorage::bitbucket::post(const char* hash, const char* user, const char* pass, KObject* params, KObject* body) {
	string url("https://bitbucket.org/api/1.0");
	url.append(hash);
	string fields = "";
	bool first = true;

	if (body != NULL) {
		for (KObject::KObjAttributes::iterator it = body->begin(); it != body->end(); it++) {
			(*params) << (**it).key << (string) (**it);
		}
	}

	for (KObject::KObjAttributes::iterator it = params->begin(); it != params->end(); it++) {
		string key = (*it)->key;
		void* val = (*it)->value->pointed();

		string escaped_key = kinky::utils::KUrlEncodeRfc3986::encode(key);
		string escaped_value = kinky::utils::KUrlEncodeRfc3986::encode(static_cast<string*>(val)->data());

		if (!first) {
			fields.append("&");
		}

		first = false;

		fields.append(escaped_key);
		fields.append("=");
		fields.append(escaped_value);
	}

	string buffer;
	char errorBuffer[CURL_ERROR_SIZE];


	CURL* curl = curl_easy_init();

	OpenSSL_add_all_algorithms();
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_URL, url.data());
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_POST, 1);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, social::common::curlWriter);

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	struct curl_slist* headers = NULL;

	KObject::KObjHeaders* reqheaders = params->getHeaders();
	if (reqheaders != NULL) {
		for (KObject::KObjHeaders::iterator it = reqheaders->begin(); it != reqheaders->end(); it++) {
			ostringstream oss;
			string name(it->first);
			kinky::utils::prettify_header_name(name);
			oss << name << ": " << **(it->second);
			headers = curl_slist_append(headers, oss.str().data());
		}
	}

	curl_easy_setopt(curl, CURLOPT_COPYPOSTFIELDS, fields.data());
	headers = curl_slist_append(headers, "Content-Type: application/x-www-form-urlencoded; charset=UTF-8");

	string userpass(user);
	userpass.insert(userpass.length(), ":");
	userpass.insert(userpass.length(), pass);
	curl_easy_setopt( curl, CURLOPT_USERPWD, userpass.data());
	headers = curl_slist_append(headers, "Expect:");

	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	CURLcode result = curl_easy_perform(curl);

	if (result == CURLE_OK) {
		string* toParse = new string(buffer);
		KObject* toReturn = kinky::social::common::parseResponse(curl, toParse, params);

		delete toParse;

		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		return toReturn;
	}
	else {
		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		throw new KRestException(result, errorBuffer);
	}
}
