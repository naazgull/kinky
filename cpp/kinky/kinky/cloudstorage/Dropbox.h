#pragma once

#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/social/OAuth2.h>
#include <kinky/social/Common.h>
#include <kinky/social/KSocialTokenExpiredException.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest::http;

	namespace cloudstorage {

		namespace dropbox {

			KObject* get(string hash, KObject* params);

			KObject* post(string hash, KObject* params, bool postAccessToken = false);


		}
	}
}
