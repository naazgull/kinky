#include <kinky/cloudstorage/controllers/CopySelf.h>

kinky::cloudstorage::controllers::CopySelf::CopySelf() :
		KRestOperation("^/copy(/(.+))*$") {
}

kinky::cloudstorage::controllers::CopySelf::~CopySelf() {
}

kinky::lang::KObject* kinky::cloudstorage::controllers::CopySelf::process(KObject* request, int directive, string* url, KToken* token) {
	KObject* ret = NULL;
	KRestException* thr = NULL;

	size_t pos = url->find('/', 1);
	string fbURL;
	if (pos == string::npos) {
		thr = new KRestException(KHTTPResponse::PRECONDITION_FAILED, KEXCEPTION_NOT_ENOUGH_PARAMETERS, KOBJECT(
			"required_url_part" << KOBJECT_ARRAY(
				"copy path, e.g. '/copy/user/info"
			)
		));
	}
	else {
		fbURL.assign(url->substr(pos));
	}

	if (fbURL.length() != 0) {

		string access_token;
		string oauth_token_secret;

		if ((*request)["oauth_token"] != NULL_KOBJECT && (*request)["oauth_token_secret"] != NULL_KOBJECT) {
			access_token.assign((string) (*request)["oauth_token"]);
			oauth_token_secret.assign((string) (*request)["oauth_token_secret"]);
			request->remove("oauth_token");
			request->remove("oauth_token_secret");
		}
		else {
			string uid;
			kinky::scopes::KScopeController::extractMe(token, &uid);
			string uri(uid);
			uri.insert(uri.length(), "/accounts?type=copy");
			uid.insert(0, "default:");
			KToken admtoken(*token->getClientID(), "", uid, "admin", time(NULL) + 3600 * 24 * 60);
			KObject* accounts = KRest::exec(KHTTPRequest::GET, this->getConfiguration()->get("krest_server_url"), uri, admtoken, NULL);

			if (accounts != NULL && accounts->getStatus() == KHTTPResponse::OK && (*accounts)["elements"] != NULL_KOBJECT) {
				access_token.assign((string) (*accounts)["elements"][0]["authdata"]["oauth_token"]);
				oauth_token_secret.assign((string) (*accounts)["elements"][0]["authdata"]["oauth_token_secret"]);
			}

			if (accounts != NULL) {
				delete accounts;
			}
		}

		switch (directive) {
			case KHTTPRequest::GET: {
				try {
					KObject* send = request->clone();
					ret = kinky::cloudstorage::copy::get(fbURL.data(), this->getConfiguration()->get("led_oauththirdparty_copy_consumer_key").data(), this->getConfiguration()->get("led_oauththirdparty_copy_consumer_secret").data(), access_token.data(), oauth_token_secret.data(), send);
					delete send;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}

			case KHTTPRequest::POST: {
				try {
					KObject* params = new KObject();
					KObject* send = request->clone();
					ret = kinky::cloudstorage::copy::post(fbURL.data(), this->getConfiguration()->get("led_oauththirdparty_copy_consumer_key").data(), this->getConfiguration()->get("led_oauththirdparty_copy_consumer_secret").data(), access_token.data(), oauth_token_secret.data(), params, send);
					delete send;
					delete params;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}

			default: {
				thr = new KRestException(KHTTPResponse::METHOD_NOT_ALLOWED, KEXCEPTION_INVALID_METHOD);
			}
		}

	}

	if (thr != NULL) {
		throw thr;
	}

	return ret;
}
