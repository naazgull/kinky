#include <kinky/cloudstorage/controllers/BoxSelf.h>

kinky::cloudstorage::controllers::BoxSelf::BoxSelf() :
		KRestOperation("^/box(/(.+))*$") {
}

kinky::cloudstorage::controllers::BoxSelf::~BoxSelf() {
}

kinky::lang::KObject* kinky::cloudstorage::controllers::BoxSelf::process(KObject* request, int directive, string* url, KToken* token) {
	KObject* ret = NULL;
	KRestException* thr = NULL;

	size_t pos = url->find('/', 1);
	string fbURL;
	if (pos == string::npos) {
		thr = new KRestException(KHTTPResponse::PRECONDITION_FAILED, KEXCEPTION_NOT_ENOUGH_PARAMETERS, KOBJECT(
			"required_url_part" << KOBJECT_ARRAY(
				"box paht, e.g. '/box/user/me"
			)
		));
	}
	else {
		fbURL.assign(url->substr(pos));
	}

	if (fbURL.length() != 0) {

		if ((*request)["box_token"] != NULL_KOBJECT) {
			(*request) << "access_token" << (string) (*request)["box_token"];
			request->remove("box_token");
		}
		else {
			string uid;
			kinky::scopes::KScopeController::extractMe(token, &uid);
			string uri(uid);
			uri.insert(uri.length(), "/accounts?type=box");
			uid.insert(0, "default:");
			KToken admtoken(*token->getClientID(), "", uid, "admin", time(NULL) + 3600 * 24 * 60);
			KObject* accounts = KRest::exec(KHTTPRequest::GET, this->getConfiguration()->get("krest_server_url"), uri, admtoken, NULL);

			if (accounts != NULL && accounts->getStatus() == KHTTPResponse::OK && (*accounts)["elements"] != NULL_KOBJECT) {
				(*request) << "access_token" << (string) (*accounts)["elements"][0]["authdata"]["access_token"];
			}

			if (accounts != NULL) {
				delete accounts;
			}
		}

		switch (directive) {
			case KHTTPRequest::GET: {
				try {
					KObject* send = request->clone();
					ret = kinky::cloudstorage::box::get(fbURL.data(), send);
					delete send;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}
			case KHTTPRequest::POST: {
				try {
					KObject* send = request->clone();
					ret = kinky::cloudstorage::box::post(fbURL, send);
					delete send;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}
			case KHTTPRequest::PUT: {
				try {
					KObject* send = request->clone();
					ret = kinky::cloudstorage::box::put(fbURL, send);
					delete send;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}
			default: {
				thr = new KRestException(KHTTPResponse::METHOD_NOT_ALLOWED, KEXCEPTION_INVALID_METHOD);
			}
		}
	}

	if (thr != NULL) {
		throw thr;
	}

	return ret;
}
