#include <kinky/cloudstorage/controllers/DropboxSelf.h>

kinky::cloudstorage::controllers::DropboxSelf::DropboxSelf() :
		KRestOperation("^/dropbox(/(.+))*$") {
}

kinky::cloudstorage::controllers::DropboxSelf::~DropboxSelf() {
}

kinky::lang::KObject* kinky::cloudstorage::controllers::DropboxSelf::process(KObject* request, int directive, string* url, KToken* token) {
	KObject* ret = NULL;
	KRestException* thr = NULL;

	size_t pos = url->find('/', 1);
	string fbURL;
	if (pos == string::npos) {
		thr = new KRestException(KHTTPResponse::PRECONDITION_FAILED, KEXCEPTION_NOT_ENOUGH_PARAMETERS, KOBJECT(
			"required_url_part" << KOBJECT_ARRAY(
				"dropbox paht, e.g. '/dropbox/account/info"
			)
		));
	}
	else {
		fbURL.assign(url->substr(pos));
	}

	if (fbURL.length() != 0) {

		if ((*request)["dropbox_token"] != NULL_KOBJECT) {
			(*request) << "access_token" << (string) (*request)["dropbox_token"];
			request->remove("dropbox_token");
		}
		else {
			string uid;
			kinky::scopes::KScopeController::extractMe(token, &uid);
			string uri(uid);
			uri.insert(uri.length(), "/accounts?type=dropbox");
			uid.insert(0, "default:");
			KToken admtoken(*token->getClientID(), "", uid, "admin", time(NULL) + 3600 * 24 * 60);
			KObject* accounts = KRest::exec(KHTTPRequest::GET, this->getConfiguration()->get("krest_server_url"), uri, admtoken, NULL);

			if (accounts != NULL && accounts->getStatus() == KHTTPResponse::OK && (*accounts)["elements"] != NULL_KOBJECT) {
				(*request) << "access_token" << (string) (*accounts)["elements"][0]["authdata"]["access_token"];
			}

			if (accounts != NULL) {
				delete accounts;
			}
		}

		switch (directive) {
			case KHTTPRequest::GET: {
				try {
					KObject* send = request->clone();
					ret = kinky::cloudstorage::dropbox::get(fbURL.data(), send);
					delete send;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}
			case KHTTPRequest::POST: {
				try {
					KObject* send = request->clone();
					ret = kinky::cloudstorage::dropbox::post(fbURL.data(), send);
					delete send;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}

			default: {
				thr = new KRestException(KHTTPResponse::METHOD_NOT_ALLOWED, KEXCEPTION_INVALID_METHOD);
			}
		}
	}

	if (thr != NULL) {
		throw thr;
	}

	return ret;
}
