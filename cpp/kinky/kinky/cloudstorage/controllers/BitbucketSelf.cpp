#include <kinky/cloudstorage/controllers/BitbucketSelf.h>

kinky::cloudstorage::controllers::BitbucketSelf::BitbucketSelf() :
		KRestOperation("^/bitbucket(/(.+))*$") {
}

kinky::cloudstorage::controllers::BitbucketSelf::~BitbucketSelf() {
}

kinky::lang::KObject* kinky::cloudstorage::controllers::BitbucketSelf::process(KObject* request, int directive, string* url, KToken* token) {
	KObject* ret = NULL;
	KRestException* thr = NULL;

	size_t pos = url->find('/', 1);
	string fbURL;
	if (pos == string::npos) {
		thr = new KRestException(KHTTPResponse::PRECONDITION_FAILED, KEXCEPTION_NOT_ENOUGH_PARAMETERS, KOBJECT(
			"required_url_part" << KOBJECT_ARRAY(
				"bitbucket path, e.g. '/bitbucket/user/info"
			)
		));
	}
	else {
		fbURL.assign(url->substr(pos));
	}

	if (fbURL.length() != 0) {

		string access_token;
		string oauth_token_secret;
		bool usecredentials = false;

		if ((*request)["username"] != NULL_KOBJECT && (*request)["password"] != NULL_KOBJECT) {
			usecredentials = true;
			access_token.assign((string) (*request)["username"]);
			oauth_token_secret.assign((string) (*request)["password"]);
			request->remove("username");
			request->remove("password");
		}
		else if ((*request)["oauth_token"] != NULL_KOBJECT && (*request)["oauth_token_secret"] != NULL_KOBJECT) {
			access_token.assign((string) (*request)["oauth_token"]);
			oauth_token_secret.assign((string) (*request)["oauth_token_secret"]);
			request->remove("oauth_token");
			request->remove("oauth_token_secret");
		}
		else {
			string uid;
			kinky::scopes::KScopeController::extractMe(token, &uid);
			string uri(uid);
			uri.insert(uri.length(), "/accounts?type=bitbucket");
			uid.insert(0, "default:");
			KToken admtoken(*token->getClientID(), "", uid, "admin", time(NULL) + 3600 * 24 * 60);
			KObject* accounts = KRest::exec(KHTTPRequest::GET, this->getConfiguration()->get("krest_server_url"), uri, admtoken, NULL);

			if (accounts != NULL && accounts->getStatus() == KHTTPResponse::OK && (*accounts)["elements"]) {
				if ((*accounts)["elements"][0]["authdata"]["username"]) {
					usecredentials = true;
					access_token.assign((string) (*accounts)["elements"][0]["authdata"]["username"]);
					oauth_token_secret.assign((string) (*accounts)["elements"][0]["authdata"]["password"]);
				}
				else {
					access_token.assign((string) (*accounts)["elements"][0]["authdata"]["oauth_token"]);
					oauth_token_secret.assign((string) (*accounts)["elements"][0]["authdata"]["oauth_token_secret"]);
				}
			}
			else if (this->getConfiguration()->getString("led_oauththirdparty_bitbucket_default_username") != NULL && this->getConfiguration()->getString("led_oauththirdparty_bitbucket_default_secret") != NULL) {
				usecredentials = true;
				access_token.assign(this->getConfiguration()->get("led_oauththirdparty_bitbucket_default_username"));
				oauth_token_secret.assign(this->getConfiguration()->get("led_oauththirdparty_bitbucket_default_secret"));
			}
			else if (this->getConfiguration()->getString("led_oauththirdparty_bitbucket_default_account") != NULL) {
				if (accounts != NULL) {
					delete accounts;
					accounts = NULL;
				}
				KObject* accounts = KRest::exec(KHTTPRequest::GET, this->getConfiguration()->get("krest_server_url"), this->getConfiguration()->get("led_oauththirdparty_bitbucket_default_account"), admtoken, NULL);
				if (accounts != NULL && accounts->getStatus() == KHTTPResponse::OK && (*accounts)["elements"] != NULL_KOBJECT) {
					access_token.assign((string) (*accounts)["elements"][0]["authdata"]["oauth_token"]);
					oauth_token_secret.assign((string) (*accounts)["elements"][0]["authdata"]["oauth_token_secret"]);
				}
			}

			if (accounts != NULL) {
				delete accounts;
			}
		}

		switch (directive) {
			case KHTTPRequest::GET: {
				try {
					KObject* send = request->clone();
					if (!usecredentials) {
						ret = kinky::cloudstorage::bitbucket::get(fbURL.data(), this->getConfiguration()->get("led_oauththirdparty_bitbucket_consumer_key").data(), this->getConfiguration()->get("led_oauththirdparty_bitbucket_consumer_secret").data(), access_token.data(), oauth_token_secret.data(), send);
					}
					else {
						ret = kinky::cloudstorage::bitbucket::get(fbURL.data(), access_token.data(), oauth_token_secret.data(), send);
					}
					delete send;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}

			case KHTTPRequest::POST: {
				try {
					KObject* params = new KObject();
					KObject* send = request->clone();
					if (!usecredentials) {
						ret = kinky::cloudstorage::bitbucket::post(fbURL.data(), this->getConfiguration()->get("led_oauththirdparty_bitbucket_consumer_key").data(), this->getConfiguration()->get("led_oauththirdparty_bitbucket_consumer_secret").data(), access_token.data(), oauth_token_secret.data(), params, send);
					}
					else {
						ret = kinky::cloudstorage::bitbucket::post(fbURL.data(), access_token.data(), oauth_token_secret.data(), params, send);
					}
					delete send;
					delete params;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}

			default: {
				thr = new KRestException(KHTTPResponse::METHOD_NOT_ALLOWED, KEXCEPTION_INVALID_METHOD);
			}
		}

	}

	if (thr != NULL) {
		throw thr;
	}

	return ret;
}
