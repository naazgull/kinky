#include <kinky/cloudstorage/Dropbox.h>

#include <kinky/utils/KDateTime.h>

#define TOKEN_EXPIRED "has expired"
#define CHANGED_PASSWORD "changed the password"

kinky::lang::KObject* kinky::cloudstorage::dropbox::get(string hash, KObject* params) {
	string url("https://api.dropbox.com/1");
	url.append(hash);

	KObject* r = kinky::social::oauth2::get(url.data(), params);

	return r;
}

kinky::lang::KObject* kinky::cloudstorage::dropbox::post(string hash, KObject* params, bool postAccessToken) {
	string url("https://api.dropbox.com/1");
	url.append(hash);

	KObject* r = kinky::social::oauth2::post(url.data(), params, postAccessToken);

	return r;
}

