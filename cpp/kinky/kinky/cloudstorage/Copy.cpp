#include <kinky/cloudstorage/Copy.h>

kinky::lang::KObject* kinky::cloudstorage::copy::get(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params) {
	string* url = new string("https://api.copy.com/rest");
	url->append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0";

	params->sort();

	params->addHeader("X-Api-Version", "1");
	params->addHeader("Accept", "application/json");

	KObject* profile_data = kinky::social::oauth1::get(url->data(), params, consumerSecret, tokenSecret);

	delete nonce;
	delete timestamp;
	delete url;

	return profile_data;
}


kinky::lang::KObject* kinky::cloudstorage::copy::post(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params, KObject* body) {
	string* url = new string("https://api.copy.com/rest");
	url->append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0";

	params->sort();

	ostringstream oss;
	oss << *body << flush;
	string strbody(oss.str());

	params->addHeader("X-Api-Version", "1");
	params->addHeader("Accept", "application/json");

	KObject* profile_data = kinky::social::oauth1::post(url->data(), params, consumerSecret, tokenSecret, &strbody);

	delete nonce;
	delete timestamp;
	delete url;

	return profile_data;
}
