#include <kinky/cloudstorage/Box.h>

#include <kinky/utils/KDateTime.h>

#define TOKEN_EXPIRED "has expired"
#define CHANGED_PASSWORD "changed the password"

kinky::lang::KObject* kinky::cloudstorage::box::get(string hash, KObject* params) {
	string url("https://api.box.com/2.0");
	url.append(hash);

	KObject* r = kinky::social::oauth2::get(url.data(), params, "Bearer");

	return r;
}

kinky::lang::KObject* kinky::cloudstorage::box::post(string hash, KObject* params, bool postAccessToken) {
	string url("https://api.box.com/2.0");
	url.append(hash);

	KObject* r = kinky::social::oauth2::post(url.data(), params, false, "Bearer");

	return r;
}

kinky::lang::KObject* kinky::cloudstorage::box::put(string hash, KObject* params, bool postAccessToken) {
	string url("https://api.box.com/2.0");
	url.append(hash);

	KObject* r = kinky::social::oauth2::put(url.data(), params, false, "Bearer");

	return r;
}
