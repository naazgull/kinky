#pragma once

#include <kinky/rest/KRest.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/social/OAuth1.h>
#include <kinky/social/Common.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest::http;

	namespace cloudstorage {

		namespace bitbucket {

			KObject* get(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params);

			KObject* get(const char* hash, const char* user, const char* pass, KObject* params);

			KObject* post(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params, KObject* body);

			KObject* post(const char* hash, const char* user, const char* pass, KObject* params, KObject* body);

		}
	}
}
