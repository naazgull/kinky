#pragma once

#ifdef USE_SPARSEHASH

#include <string>
#include <memory.h>
#include <stdlib.h>
#include <openssl/crypto.h>
#include <algorithm>
#include <sparsehash/sparse_hash_map>
#include <kinky/memory/KMemoryManager.h>
#include <kinky/lang/KObjectNULLException.h>
#include <kinky/utils/KLocale.h>
#include <kinky/log/KLogThread.h>

using namespace std;
using namespace __gnu_cxx;

#define KOBJECT_ARRAY(z) (&((*(new kinky::lang::KObject(kinky::lang::KObject::ARRAY))) << z))
#define KOBJECT(z) (&((*(new kinky::lang::KObject())) << z))

#define NULL_KOBJECT *kinky::lang::k_null_object

#ifndef KINKY_CURL_TIMEOUT
	#define KINKY_CURL_TIMEOUT 5
#endif

namespace kinky {

	using namespace memory;

	void deinitSSLMutex();
	void initSSLMutex();
	void locking_function(int mode, int n, const char *file, int line);
	unsigned long id_function(void);

	namespace lang {

		class KObject {
			public:

				class trio {
					public:
						int type;
						string key;
						abstract_managed_ptr* value;
						int pos;

						trio(int typeParam, string keyParam);
						trio(int typeParam, string keyParam, abstract_managed_ptr* valueParam);
						virtual ~trio();

						friend ostream& operator<<(ostream& os, trio& f) {
							switch (f.type) {
								case kinky::lang::KObject::OBJECT: {
									os << *(static_cast<kinky::lang::KObject*>(f.value->pointed()));
									break;
								}
								case kinky::lang::KObject::ARRAY: {
									os << *(static_cast<kinky::lang::KObject*>(f.value->pointed()));
									break;
								}
								case kinky::lang::KObject::BOOL: {
									os << *static_cast<bool*>(f.value->pointed());
									break;
								}
								case kinky::lang::KObject::INT: {
									os << *static_cast<int*>(f.value->pointed());
									break;
								}
								case kinky::lang::KObject::DOUBLE: {
									os << *static_cast<double*>(f.value->pointed());
									break;
								}
								case kinky::lang::KObject::STRING: {
									os << *static_cast<string*>(f.value->pointed());
									break;
								}
								case kinky::lang::KObject::NIL: {
									break;
								}
							}
							return os;
						}

						bool operator==(trio* t);

						bool operator==(trio& t);

						bool operator==(KObject& t);

						bool operator==(string& t);

						bool operator==(bool& t);

						bool operator==(int& t);

						bool operator==(double& t);

						bool operator!=(trio* t);

						bool operator!=(trio& t);

						bool operator!=(KObject& t);

						bool operator!=(string& t);

						bool operator!=(bool& t);

						bool operator!=(int& t);

						bool operator!=(double& t);

						operator string();

						operator bool();

						operator int();

						operator double();

						operator string*();

						operator KObject*();

						trio& operator[](int idx);

						trio& operator[](const char* name);

						trio& operator[](string name);
				};

				const static int OBJECT = 0;
				const static int ARRAY = 1;
				const static int BOOL = 2;
				const static int INT = 4;
				const static int DOUBLE = 8;
				const static int STRING = 16;
				const static int NIL = 32;

				typedef google::sparse_hash_map<string, string*> KObjCookie;
				typedef google::sparse_hash_map<string, string*> KObjHeaders;
				typedef  google::sparse_hash_map<string, trio*> KObjAttributeNames;
				typedef vector<trio*> KObjAttributes;

				int type;

				KObject();

				KObject(int typeParam);

				KObject(int typeParam, string baseFormatParam);

				KObject(int typeParam, string* baseFormatParam);

				KObject(int typeParam, int statusParam);

				KObject(int typeParam, string baseFormatParam, int statusParam);

				KObject(string baseFormatParam);

				KObject(string baseFormatParam, int statusParam);

				KObject(string* baseFormatParam);

				KObject(string* baseFormatParam, int statusParam);

				virtual ~KObject();

				kinky::lang::KObject::trio* evalObjectPath(string name);

				string get(string name);

				string get(int idx);

				int getType(string name);

				int getType(int idx);

				string* getQueryString(string params[] = NULL, int length = 0);

				bool* getBoolean(string name);

				int* getInteger(string name);

				double* getDouble(string name);

				string* getString(string name);

				KObject* getObject(string name);

				KObject* getArray(string name);

				bool* getBoolean(int idx);

				int* getInteger(int idx);

				double* getDouble(int idx);

				string* getString(int idx);

				KObject* getObject(int idx);

				KObject* getArray(int idx);

				void setBoolean(string name, bool value);

				void setInteger(string name, int value);

				void setDouble(string name, double value);

				void setString(string name, string value);

				void setObject(string name, KObject* value);

				void setArray(string name, KObject* value);

				void setNil(string name);

				void setBoolean(int idx, bool value);

				void setInteger(int idx, int value);

				void setDouble(int idx, double value);

				void setString(int idx, string value);

				void setObject(int idx, KObject* value);

				void setArray(int idx, KObject* value);

				void setNil(int idx);

				int add(abstract_managed_ptr* value, int type);

				int add(bool value);

				int add(int value);

				int add(double value);

				int add(string value);

				int add(KObject* value);

				int addAt(int idx, abstract_managed_ptr* value, int type);

				int addAt(int idx, bool value);

				int addAt(int idx, int value);

				int addAt(int idx, double value);

				int addAt(int idx, string value);

				int addAt(int idx, KObject* value);

				void clear();

				void remove(int idx);

				void remove(string name);

				void unset(int idx);

				void unset(string name);

				string* base();

				void base(string* base);

				void base(string base);

				KObject::KObjAttributes::iterator begin();

				KObject::KObjAttributes::iterator end();

				void move(string name, int offset);

				void sort();

				void assign(KObject* other);

				KObject* clone();

				int size();

				int getStatus();

				void setStatus(int status);

				KObject::KObjHeaders* getHeaders();

				void addHeader(string name, string value);

				string* getHeader(string name);

				string* getCookie(string name);

				KObject::KObjCookie* getCookies();

				friend kinky::log::KLogStream& operator<<(kinky::log::KLogStream& os, KObject& f) {
					ostringstream* o = os.getBuffer();
					f.prettify(*o);
					return os;
				}

				friend ostream& operator<<(ostream& os, KObject& f) {
					f.stringify(os);
					return os;
				}

				bool operator!=(KObject& t);

				bool operator!=(KObject* t);

				bool operator!=(trio* t);

				bool operator==(KObject& t);

				bool operator==(KObject* t);

				bool operator==(trio* t);

				trio& operator[](int idx);

				trio& operator[](string name);

				friend KObject& operator<<(KObject& os, const char* f) {
					os.put(f);
					return os;
				}

				friend KObject& operator<<(KObject& os, string f) {
					os.put(f);
					return os;
				}

				friend KObject& operator<<(KObject& os, bool f) {
					os.put(f);
					return os;
				}

				friend KObject& operator<<(KObject& os, int f) {
					os.put(f);
					return os;
				}

				friend KObject& operator<<(KObject& os, double f) {
					os.put(f);
					return os;
				}

				friend KObject& operator<<(KObject& os, KObject* f) {
					os.put(f);
					return os;
				}

				friend KObject& operator<<(KObject& os, KObject::KObjHeaders f) {
					os.put(f);
					return os;
				}

				void put(string f);

				void put(const char* f);

				void put(bool value);

				void put(int value);

				void put(double value);

				void put(KObject* value);

				void put(KObject::KObjHeaders& value);

				void stringify(ostream& oss);

				void prettify(ostream& oss);

				static void stringifyJSON(KObject* obj, ostream& oss);

				static void prettifyJSON(KObject* obj, int tabLevel, ostream& oss);

				static void tabbify(int tabs, ostream& oss);

				static void tabbifyHTML(int tabs, ostream& oss);

				static void stringifyXML(KObject* obj, ostream& oss, string& attName);

				static void prettifyXML(KObject* obj, int tabLevel, ostream& oss, string& attName);

				static void stringifyHTML(KObject* obj, int tabLevel, ostream& oss);

			private:
				int status;
				KObject::KObjHeaders* headers;
				KObject::KObjAttributes* atts;
				KObject::KObjAttributeNames* names;
				string* baseFormat;
				KObject::KObjCookie* cookie;

				abstract_managed_ptr* getIt(int idx, int type) throw ();

				abstract_managed_ptr* getIt(string name, int type);

				int insertIt(int idx, int type, abstract_managed_ptr* value);

				int setIt(int idx, int type, abstract_managed_ptr* value);

				int setIt(string name, int type, abstract_managed_ptr* value);

				void deleteIt(int idx);

				void deleteIt(string name);

			public:
				string* temp;
		};

		extern KObject::trio* k_null_object;

	}

}
#endif
