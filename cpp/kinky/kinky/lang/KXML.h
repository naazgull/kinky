#pragma once

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/TransService.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <errno.h>
#include <iconv.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <kinky/lang/KParseException.h>
#include <kinky/lang/KObject.h>
#include <kinky/log/KLogThread.h>

using namespace std;
using namespace __gnu_cxx;
using namespace xercesc;

namespace kinky {

	namespace lang {

		class KXML {
			public:
				static KObject* parse(string* jsonStr);

				static KObject* parse(string jsonStr, int length);

				static KObject* parseFile(string xmlFile);

				static managed_ptr<string>* utf8Encode(string str);

				static managed_ptr<string>* transcode(string encoding, const XMLCh* source);

				static void convert(DOMNode* obj, KObject* kobj, string encoding);

		};
	}
}
