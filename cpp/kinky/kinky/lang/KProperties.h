#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <kinky/lang/KObject.h>

using namespace kinky::lang;

namespace kinky {

	namespace lang {

		class KProperties: public KObject {
			public:
				KProperties();
				virtual ~KProperties();

				static KProperties* parse(string file);
		};

	}

}
