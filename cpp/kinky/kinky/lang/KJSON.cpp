#include <kinky/lang/KJSON.h>

using namespace std;
using namespace __gnu_cxx;

kinky::lang::KObject* kinky::lang::KJSON::parse(std::ifstream& in) {
	if (in.is_open()) {
		in.seekg(0, ios::end);
		size_t length = in.tellg();
		in.seekg(0, ios::beg);
		char* buffer = new char[length];
		memset(buffer, 0, length);
		in.read(buffer, length);
		in.close();
		string toparse(buffer, length);
		KObject* ret = KJSON::parse(toparse);
		delete[] buffer;
		return ret;
	}
	throw new kinky::lang::KParseException(500, new managed_ptr<string>(new string("file is not opened")));
}

kinky::lang::KObject* kinky::lang::KJSON::parse(string* jsonStr) {
	return KJSON::parse(*jsonStr);
}

#ifdef KINKY_JSON_INTERNAL
kinky::lang::KObject* kinky::lang::KJSON::parse(string& jsonStr) {
	char *errorPos = 0;
	char *errorDesc = 0;
	int errorLine = 0;
	block_allocator allocator(1 << 10);

	json_value *root = json_parse((char*) jsonStr.c_str(), &errorPos, &errorDesc, &errorLine, &allocator);
	if (root != NULL) {
		KObject* toReturn = new KObject("json");
		KJSON::convert(root, toReturn);
		return toReturn;
	}
	throw new KParseException(500, new managed_ptr<string>(new string("the provided string is not a valid JSON")));
}

void kinky::lang::KJSON::convert(json_value* obj, KObject* kobj) {
	switch (obj->type) {
		case JSON_NULL: {
			kobj->type = KObject::NIL;
			(*kobj) << "#text" << "null";
			break;
		}
		case JSON_BOOL: {
			kobj->type = KObject::BOOL;
			(*kobj) << "#text" << (obj->int_value ? true : false);

			break;
		}
		case JSON_FLOAT: {
			kobj->type = KObject::DOUBLE;
			(*kobj) << "#text" << (double) obj->float_value;
			break;
		}
		case JSON_INT: {
			kobj->type = KObject::INT;
			(*kobj) << "#text" << obj->int_value;
			break;
		}
		case JSON_STRING: {
			kobj->type = KObject::STRING;
			(*kobj) << "#text" << (string) obj->string_value;
			break;
		}
		case JSON_OBJECT: {
			kobj->type = KObject::OBJECT;
			for (json_value *it = obj->first_child; it; it = it->next_sibling) {
				if (it == NULL) {
					continue;
				}
				string key(it->name);

				switch (it->type) {
					case JSON_NULL: {
						break;
					}
					case JSON_BOOL: {
						(*kobj) << key << (it->int_value ? true : false);
						break;
					}
					case JSON_FLOAT: {
						(*kobj) << key << (double) it->float_value;
						break;
					}
					case JSON_INT: {
						(*kobj) << key << it->int_value;
						break;
					}
					case JSON_STRING: {
						(*kobj) << key << (string) it->string_value;
						break;
					}
					case JSON_OBJECT: {
						KObject* attObj = new KObject(KObject::OBJECT);
						KJSON::convert(it, attObj);
						(*kobj) << key << attObj;
						break;
					}
					case JSON_ARRAY: {
						KObject* attObj = new KObject(KObject::ARRAY);
						KJSON::convert(it, attObj);
						(*kobj) << key << attObj;
						break;
					}
				}
			}
			break;
		}
		case JSON_ARRAY: {
			kobj->type = KObject::ARRAY;

			for (json_value *it = obj->first_child; it; it = it->next_sibling) {
				if (it == NULL) {
					continue;
				}

				switch (it->type) {
					case JSON_NULL: {
						break;
					}
					case JSON_BOOL: {
						(*kobj) << (it->int_value ? true : false);
						break;
					}
					case JSON_FLOAT: {
						(*kobj) << (double) it->float_value;
						break;
					}
					case JSON_INT: {
						(*kobj) << it->int_value;
						break;
					}
					case JSON_STRING: {
						(*kobj) << (string) it->string_value;
						break;
					}
					case JSON_OBJECT: {
						KObject* attObj = new KObject(KObject::OBJECT, "json");
						KJSON::convert(it, attObj);
						(*kobj) << attObj;
						break;
					}
					case JSON_ARRAY: {
						KObject* attObj = new KObject(KObject::ARRAY, "json");
						KJSON::convert(it, attObj);
						(*kobj) << attObj;
						break;
					}
				}
			}
			break;
		}
	}

}
#endif
#ifdef KINKY_JSON_C
kinky::lang::KObject* kinky::lang::KJSON::parse(string jsonStr) {
	json_tokener* tokener = json_tokener_new();
	json_object* obj = json_tokener_parse_ex(tokener, jsonStr.data(), jsonStr.size());
	if (obj == NULL || is_error(obj)) {
		free(obj);
		json_tokener_free(tokener);
		ostringstream oss;
		oss << "the provided string is not a valid json: " << jsonStr << flush;
		throw new KParseException(500, new managed_ptr<string>(new string(oss.str())));
	}
	KObject* toReturn = new KObject("json");
	KJSON::convert(obj, toReturn);
	json_tokener_free(tokener);
	free(obj);
	return toReturn;
}

void kinky::lang::KJSON::convert(json_object* obj, KObject* kobj) {
	enum json_type type = json_object_get_type(obj);

	switch (type) {
		case json_type_null: {
			kobj->type = KObject::NIL;
			kobj->setString("#text", "null");
			break;
		}
		case json_type_boolean: {
			kobj->type = KObject::BOOL;
			kobj->setBoolean("#text", json_object_get_boolean(obj));
			break;
		}
		case json_type_double: {
			kobj->type = KObject::DOUBLE;
			kobj->setDouble("#text", json_object_get_double(obj));
			break;
		}
		case json_type_int: {
			kobj->type = KObject::INT;
			kobj->setInteger("#text", json_object_get_int(obj));
			break;
		}
		case json_type_string: {
			kobj->type = KObject::STRING;
			const char* str = json_object_get_string(obj);
			kobj->setString("#text", string(str));
			break;
		}
		case json_type_object: {
			kobj->type = KObject::OBJECT;
			json_object_object_foreach(obj, key, val) {
				if (val == NULL) {
					continue;
				}
				enum json_type attType = json_object_get_type(val);

				switch (attType) {
					case json_type_null: {
						break;
					}
					case json_type_boolean: {
						kobj->setBoolean(key, json_object_get_boolean(val));
						break;
					}
					case json_type_double: {
						kobj->setDouble(key, json_object_get_double(val));
						break;
					}
					case json_type_int: {
						kobj->setInteger(key, json_object_get_int(val));
						break;
					}
					case json_type_string: {
						const char* str = json_object_get_string(val);
						kobj->setString(key, string(str));
						break;
					}
					case json_type_object: {
						KObject* attObj = new KObject(KObject::OBJECT, "json");
						KJSON::convert(val, attObj);
						kobj->setObject(key, attObj);
						break;
					}
					case json_type_array: {
						KObject* attObj = new KObject(KObject::ARRAY, "json");
						KJSON::convert(val, attObj);
						kobj->setArray(key, attObj);
						break;
					}
				}
			}
			break;
		}
		case json_type_array: {
			kobj->type = KObject::ARRAY;

			json_object_array_foreach(obj, val) {
				if (val == NULL) {
					continue;
				}
				enum json_type attType = json_object_get_type(val);

				switch (attType) {
					case json_type_null: {
						break;
					}
					case json_type_boolean: {
						kobj->add(json_object_get_boolean(val));
						break;
					}
					case json_type_double: {
						kobj->add(json_object_get_double(val));
						break;
					}
					case json_type_int: {
						kobj->add(json_object_get_int(val));
						break;
					}
					case json_type_string: {
						const char* str = json_object_get_string(val);
						kobj->add(string(str));
						break;
					}
					case json_type_object: {
						KObject* attObj = new KObject(KObject::OBJECT, "json");
						KJSON::convert(val, attObj);
						kobj->add(attObj);
						break;
					}
					case json_type_array: {
						KObject* attObj = new KObject(KObject::ARRAY, "json");
						KJSON::convert(val, attObj);
						kobj->add(attObj);
						break;
					}
				}
			}
			break;
		}
	}

}
#endif
#ifdef KINKY_JSON_MONGO
kinky::lang::KObject* kinky::lang::KJSON::parse(string jsonStr) {
	bson::bo root = mongo::fromjson(jsonStr);
	if (!root.isEmpty() && root.isValid()) {
		KObject* toReturn = new KObject("json");
		KJSON::convert(root, toReturn);
		return toReturn;
	}
	throw new KParseException(500, new managed_ptr<string>(new string("the provided string is not a valid JSON")));
}

void kinky::lang::KJSON::convert(bson::bo& nobj, KObject* kobj) {
	kobj->type = KObject::OBJECT;
	for (mongo::BSONObjIterator i = nobj.begin(); i.more();) {
		bson::be it = i.next();
		string key(it.fieldName());

		switch (it.type()) {
			case mongo::jstNULL: {
				(*kobj) << key << "null";
				break;
			}
			case mongo::Bool: {
				(*kobj) << key << it.Bool();

				break;
			}
			case mongo::NumberDouble: {
				(*kobj) << key << it.Double();
				break;
			}
			case mongo::NumberLong:
				case mongo::NumberInt: {
				(*kobj) << key << it.Int();
				break;
			}
			case mongo::String: {
				(*kobj) << key << it.String();
				break;
			}
			case mongo::Object: {
				KObject* attObj = new KObject(KObject::OBJECT);
				KJSON::convert(&it, attObj);
				(*kobj) << key << attObj;
				break;
			}
			case mongo::Array: {
				KObject* attObj = new KObject(KObject::ARRAY);
				KJSON::convert(&it, attObj);
				(*kobj) << key << attObj;
				break;
			}
			default: {
				continue;
			}
		}
	}
}

void kinky::lang::KJSON::convert(bson::be* obj, KObject* kobj) {
	switch (obj->type()) {
		case mongo::jstNULL: {
			kobj->type = KObject::NIL;
			(*kobj) << "#text" << "null";
			break;
		}
		case mongo::Bool: {
			kobj->type = KObject::BOOL;
			(*kobj) << "#text" << obj->Bool();

			break;
		}
		case mongo::NumberDouble: {
			kobj->type = KObject::DOUBLE;
			(*kobj) << "#text" << obj->Double();
			break;
		}
		case mongo::NumberLong:
			case mongo::NumberInt: {
			kobj->type = KObject::INT;
			(*kobj) << "#text" << obj->Int();
			break;
		}
		case mongo::String: {
			kobj->type = KObject::STRING;
			(*kobj) << "#text" << obj->String();
			break;
		}
		case mongo::Object: {
			kobj->type = KObject::OBJECT;
			bson::bo nobj = obj->Obj();
			for (mongo::BSONObjIterator i = nobj.begin(); i.more();) {
				bson::be it = i.next();
				string key(it.fieldName());

				switch (it.type()) {
					case mongo::jstNULL: {
						(*kobj) << key << "null";
						break;
					}
					case mongo::Bool: {
						(*kobj) << key << it.Bool();

						break;
					}
					case mongo::NumberDouble: {
						(*kobj) << key << it.Double();
						break;
					}
					case mongo::NumberLong:
						case mongo::NumberInt: {
						(*kobj) << key << it.Int();
						break;
					}
					case mongo::String: {
						(*kobj) << key << it.String();
						break;
					}
					case mongo::Object: {
						KObject* attObj = new KObject(KObject::OBJECT);
						KJSON::convert(&it, attObj);
						(*kobj) << key << attObj;
						break;
					}
					case mongo::Array: {
						KObject* attObj = new KObject(KObject::ARRAY);
						KJSON::convert(&it, attObj);
						(*kobj) << key << attObj;
						break;
					}
					default: {
						continue;
					}
				}
			}
			break;
		}
		case mongo::Array: {
			kobj->type = KObject::ARRAY;
			vector<bson::be> nobj = obj->Array();

			for (vector<bson::be>::iterator i = nobj.begin(); i != nobj.end(); i++) {
				bson::be it = *i;

				switch (it.type()) {
					case mongo::jstNULL: {
						(*kobj) << "null";
						break;
					}
					case mongo::Bool: {
						(*kobj) << it.Bool();

						break;
					}
					case mongo::NumberDouble: {
						(*kobj) << it.Double();
						break;
					}
					case mongo::NumberLong:
						case mongo::NumberInt: {
						(*kobj) << it.Int();
						break;
					}
					case mongo::String: {
						(*kobj) << it.String();
						break;
					}
					case mongo::Object: {
						KObject* attObj = new KObject(KObject::OBJECT);
						KJSON::convert(&it, attObj);
						(*kobj) << attObj;
						break;
					}
					case mongo::Array: {
						KObject* attObj = new KObject(KObject::ARRAY);
						KJSON::convert(&it, attObj);
						(*kobj) << attObj;
						break;
					}
					default: {
						continue;
					}
				}
			}
			break;
		}
		default: {
			break;
		}
	}

}
#endif
