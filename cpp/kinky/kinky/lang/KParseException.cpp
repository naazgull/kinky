#include <kinky/lang/KParseException.h>

kinky::lang::KParseException::KParseException(int codeParam, kinky::memory::managed_ptr<string>* messageParam) :
		code(codeParam), message(messageParam) {
}

kinky::lang::KParseException::~KParseException() throw () {
	if (this->message != NULL) {
		this->message->release();
	}
}

string* kinky::lang::KParseException::getMessage() {
	return this->message->get();
}

int kinky::lang::KParseException::getCode() {
	return this->code;
}

kinky::memory::managed_ptr<string>* kinky::lang::KParseException::get() {
	return this->message;
}

string kinky::lang::KParseException::what() throw () {
	ostringstream oss;
	oss << *this << endl << flush;
	return **(new kinky::memory::managed_ptr<string>(new string(oss.str())));
}
