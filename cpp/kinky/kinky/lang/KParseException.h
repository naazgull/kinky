#pragma once

#include <exception>
#include <string>
#include <iostream>
#include <sstream>
#include <kinky/log/KLogThread.h>
#include <kinky/memory/KMemoryManager.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace log;

	namespace lang {

		class KParseException: public exception {
			public:
				KParseException(int codeParam, kinky::memory::managed_ptr<string>* messageParam);

				virtual ~KParseException() throw();

				string* getMessage();

				int getCode();

				kinky::memory::managed_ptr<string>* get();

				friend ostream& operator<<(ostream& os, KParseException& f) {
					os << "PARSE EXCEPTION >> code: " << f.code << " || message: " << f.getMessage()->data() << endl << flush;
					return os;
				}

				friend KLogStream& operator<<(KLogStream& os, KParseException& f) {
					os << "PARSE EXCEPTION >> code: " << f.code << " || message: " << f.getMessage()->data() << endl << flush;
					return os;
				}

				virtual string what() throw ();

			private:
				int code;
				kinky::memory::managed_ptr<string>* message;
		};

	}
}
