#pragma once

#include <exception>
#include <string>
#include <iostream>
#include <sstream>
#include <kinky/memory/KMemoryManager.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	namespace lang {

		class KObjectNULLException: public exception {
			public:
				KObjectNULLException();
				virtual ~KObjectNULLException() throw();

				virtual string what() throw ();

				friend ostream& operator<<(ostream& os, kinky::lang::KObjectNULLException& f) {
					os << "PARSE EXCEPTION >> code: 0 || message: targeted attribute is NULL" << endl << flush;
					return os;
				}


		};

	}
}
