#include <kinky/lang/KProperties.h>

kinky::lang::KProperties::KProperties() {

}

kinky::lang::KProperties::~KProperties() {

}

kinky::lang::KProperties* kinky::lang::KProperties::parse(string file) {
	KProperties* toReturn = new KProperties();
	ifstream* confFile = new ifstream();
	confFile->open(file.data(), ios::in);

	if (confFile->is_open()) {
		string line;

		while (confFile->good()) {
			string name;
			std::getline(*confFile, name, '=');
			kinky::utils::trim(name);

			if (name.length() == 0) {
				std::getline(*confFile, line);
				continue;
			}

			string value;
			std::getline(*confFile, value);

			toReturn->setString(name.data(), value.data());
		}
		confFile->close();
	}
	delete confFile;

	return toReturn;
}
