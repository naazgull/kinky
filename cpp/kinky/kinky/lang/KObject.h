#pragma once

#ifndef USE_SPARSEHASH
//#define USE_SPARSEHASH
#endif

#ifdef USE_SPARSEHASH
#include <kinky/lang/KObjectHashMap.h>
#else
#include <kinky/lang/KObjectStrMap.h>
#endif
