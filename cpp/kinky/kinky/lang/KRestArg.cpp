#include <kinky/lang/KRestArg.h>

#include <iostream>
#include <sstream>
#include <kinky/utils/KLocale.h>

kinky::lang::KRestArg::KRestArg() : base(NULL), order(NULL), parts(NULL), pageSize(-1), pageStartIndex(-1), query(NULL) {

}

kinky::lang::KRestArg::~KRestArg() {
	if (this->base != NULL) {
		delete this->base;
	}
	if (this->order != NULL) {
		delete this->order;
	}
	if (this->parts != NULL) {
		for (vector<KRestArg*>::iterator i = this->parts->begin(); i != this->parts->end(); i++) {
			delete (*i);
		}
		delete this->parts;
	}
	if (this->query!= NULL) {
		delete this->query;
	}
}

string* kinky::lang::KRestArg::get() {
	return this->base;
}

string* kinky::lang::KRestArg::getOrder() {
	return this->order;
}

int kinky::lang::KRestArg::getPageSize() {
	return this->pageSize;
}

int kinky::lang::KRestArg::getPageStartIndex() {
	return this->pageStartIndex;
}

void kinky::lang::KRestArg::set(string b) {
	if (this->base != NULL) {
		delete this->base;
	}
	this->base = new string(b.data());
}

void kinky::lang::KRestArg::setOrder(string b) {
	if (this->order != NULL) {
		delete this->order;
	}
	this->order = new string(b.data());
}

void kinky::lang::KRestArg::setPageSize(int ps) {
	this->pageSize = ps;
}

void kinky::lang::KRestArg::setPageStartIndex(int psi) {
	this->pageStartIndex = psi;
}

bool kinky::lang::KRestArg::hasParts() {
	return this->parts != NULL && this->parts->size() != 0;
}

size_t kinky::lang::KRestArg::size() {
	return this->parts != NULL ? this->parts->size() : 0;
}

void kinky::lang::KRestArg::add(kinky::lang::KRestArg* a) {
	if (this->parts == NULL) {
		this->parts = new vector<kinky::lang::KRestArg*>();
	}
	this->parts->push_back(a);
}

void kinky::lang::KRestArg::getRESTfulURI(ostream& uri) {
	bool orderby = false;
	bool pagesize = false;
	bool pagestartindex = false;
	if (this->getOrder() != NULL) {
		uri << "?orderBy=" << *this->getOrder() << flush;
		orderby = true;
	}
	if (this->getPageSize() != -1) {
		uri << (orderby ? "&" : "?") << "pageSize=" << this->getPageSize() << flush;
		pagesize = true;
	}
	if (this->getPageStartIndex() != -1) {
		uri << (orderby || pagesize ? "&" : "?") << "pageStartIndex=" << this->getPageStartIndex() << flush;
		pagestartindex = true;
	}
	if (this->query != NULL) {
		uri << (orderby || pagesize || pagestartindex ? "&" : "?") << *this->query << flush;
	}
}

kinky::lang::KRestArg* kinky::lang::KRestArg::at(size_t i) {
	if (this->parts != NULL && i < this->parts->size()) {
		return this->parts->at(i);
	}
	return NULL;
}

void kinky::lang::KRestArg::stringify(string& arg, string tab) {
	if (this->base != NULL) {
		arg.insert(arg.length(), tab);
		arg.insert(arg.length(), *this->base);
		ostringstream uri;
		if (this->query != NULL) {
			uri << ":" << *this->query << flush;
		}
		if (this->getOrder() != NULL) {
			uri << "{" << *this->getOrder() << "}" << flush;
		}
		if (this->getPageSize() != -1) {
			uri << "[" << this->getPageSize() << flush;
			if (this->getPageStartIndex() != -1) {
				uri << this->getPageStartIndex() << flush;
			}
			uri << "]" << flush;
		}
		arg.insert(arg.length(), uri.str());
	}
	if (this->parts != NULL) {
		arg.insert(arg.length(), "(\n");
		for (size_t i = 0; i != this->parts->size(); i++) {
			if (i != 0) {
				arg.insert(arg.length(), ",\n");
			}
			this->parts->at(i)->stringify(arg, tab + "\t");
		}
		arg.insert(arg.length(), "\n");
		arg.insert(arg.length(), tab);
		arg.insert(arg.length(), ")");
	}
}

kinky::lang::KRestArg* kinky::lang::KRestArg::parse(string arg) {
	KRestArg* ret = new KRestArg();

	vector<string> parts;
	int paren = 0;
	string exp;
	for(size_t i = 0; i != arg.length(); i++) {
		switch (arg.at(i)) {
			case '(' : {
				exp.push_back(arg.at(i));
				paren++;
				break;
			}
			case ')' : {
				exp.push_back(arg.at(i));
				paren--;
				break;
			}
			case ',' : {
				if (paren == 0) {
					parts.push_back(string(exp.data()));
					exp.assign("");
				}
				else {
					exp.push_back(arg.at(i));
				}
				break;
			}
			default : {
				exp.push_back(arg.at(i));
				break;
			}
		}
	}
	if (exp.length() != 0) {
		parts.push_back(string(exp.data()));
	}

	for (size_t i = 0; i != parts.size(); i++) {
		string part = parts.at(i);
		size_t lparen = part.find('(');
		if (lparen == 0) {
			part.assign(part.substr(1, part.length() - 2));
			 lparen = part.find('(');
		}

		KRestArg* sub = NULL;
		if (lparen != string::npos) {
			sub = KRestArg::parse(part.substr(lparen + 1, part.length() - lparen - 2));
			part.assign(part.substr(0, lparen));
		}
		else {
			sub = new KRestArg();
		}

		size_t lbracket =  part.find('{');
		string order;
		if (lbracket != string::npos) {
			size_t rbracket = part.find('}');
			order.assign(part.substr(lbracket + 1, rbracket - lbracket - 1));
			part.erase(part.begin() + lbracket, part.begin() + rbracket + 1);
			if (order[0] == ' ') {
				order[0] = '%';
				order.insert(1, "2B");
			}
		}

		lbracket =  part.find('[');
		int pageSize = -1;
		int pageStartIndex = -1;
		if (lbracket != string::npos) {
			size_t rbracket = part.find(']');
			string sizes;
			sizes.assign(part.substr(lbracket + 1, rbracket - lbracket - 1));
			part.erase(part.begin() + lbracket, part.begin() + rbracket + 1);

			size_t minus = sizes.find('-');
			if (minus != string::npos) {
				char dummy;
				istringstream iss(sizes);
				iss >> pageSize;
				iss >> dummy;
				iss >> pageStartIndex;
			}
			else {
				istringstream iss(sizes);
				iss >> pageSize;
			}
		}

		lbracket =  part.find(':');
		string query;
		if (lbracket != string::npos) {
			query.assign(part.substr(lbracket + 1));
			part.erase(part.begin() + lbracket, part.end());

			kinky::utils::replaceAll(query, "<", "=");
			kinky::utils::replaceAll(query, ">", "&");
			if (query[query.length() - 1] == '&') {
				query.erase(query.end() - 1, query.end());
			}
		}

		if (order.length() != 0) {
			sub->setOrder(order);
		}
		if (query.length() != 0) {
			sub->query = new string(query);
		}
		sub->setPageSize(pageSize);
		sub->setPageStartIndex(pageStartIndex);

		sub->set(part);
		ret->add(sub);
	}

	return ret;
}
