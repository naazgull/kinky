#include <kinky/lang/KObjectNULLException.h>

kinky::lang::KObjectNULLException::KObjectNULLException() {
}

kinky::lang::KObjectNULLException::~KObjectNULLException() throw () {
}

string kinky::lang::KObjectNULLException::what() throw () {
	ostringstream oss;
	oss << *this << endl << flush;
	kinky::memory::managed_ptr<string>* ret = new kinky::memory::managed_ptr<string>(new string(oss.str()));
	return *static_cast<string*>(ret->pointed());
}
