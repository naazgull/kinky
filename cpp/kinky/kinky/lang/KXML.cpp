#include <kinky/lang/KXML.h>

kinky::lang::KObject* kinky::lang::KXML::parse(string* jsonStr) {
	return KXML::parse(*jsonStr, jsonStr->length());
}

kinky::lang::KObject* kinky::lang::KXML::parse(string jsonStr, int length) {
	try {
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		(*kinky::io::out) << "Error during initialization! :\n" << message << "\n";
		XMLString::release(&message);
		return NULL;
	}

	XercesDOMParser* parser = new XercesDOMParser();
	parser->setValidationScheme(XercesDOMParser::Val_Always);
	parser->setDoNamespaces(true);
	//parser->setIncludeIgnorableWhitespace(false);

	MemBufInputSource* buffer = new MemBufInputSource((const XMLByte*) jsonStr.c_str(), jsonStr.length(), "XML string");
	DOMDocument *doc = NULL;

	try {
		parser->parse(*buffer);
		doc = parser->getDocument();
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		(*kinky::io::out) << "Exception message is: \n" << message << "\n";
		XMLString::release(&message);
		return NULL;
	}
	catch (const DOMException& toCatch) {
		char* message = XMLString::transcode(toCatch.msg);
		(*kinky::io::out) << "Exception message is: \n" << message << "\n";
		XMLString::release(&message);
		return NULL;
	}
	catch (...) {
		(*kinky::io::out) << "Unexpected Exception \n";
		return NULL;
	}

	managed_ptr<KObject> *ret = new managed_ptr<KObject>((new KObject("xml")));
	KObject* root = new KObject("xml");
	ostringstream encoding("");
	if (doc->getInputEncoding() != NULL) {
		char * enc = XMLString::transcode(doc->getInputEncoding());
		encoding << enc << flush;
		XMLString::release(&enc);
	}
	else {
		encoding << "UTF-8" << flush;
	}

	if (doc->getFirstChild() == NULL) {
		XMLPlatformUtils::Terminate();
		ostringstream oss;
		oss << "the provided string is not a valid XML: " << jsonStr << flush;
		throw new KParseException(500, new managed_ptr<string>(new string(oss.str())));
	}

	char* nodeName = XMLString::transcode(doc->getFirstChild()->getNodeName());
	(*ret)->setObject((string) nodeName, root);
	XMLString::release(&nodeName);
	KXML::convert(doc->getFirstChild(), root, encoding.str());
	delete buffer;
	delete parser;
	XMLPlatformUtils::Terminate();
	return static_cast<KObject*>(ret->pointed());
}

kinky::lang::KObject* kinky::lang::KXML::parseFile(string xmlFile) {
	try {
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		(*kinky::io::out) << "Error during initialization! :\n" << message << "\n";
		XMLString::release(&message);
		return NULL;
	}

	XercesDOMParser* parser = new XercesDOMParser();
	parser->setValidationScheme(XercesDOMParser::Val_Always);
	parser->setDoNamespaces(true);
	//parser->setIncludeIgnorableWhitespace(false);

	DOMDocument *doc = NULL;

	try {
		parser->parse(xmlFile.data());
		doc = parser->getDocument();
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		(*kinky::io::out) << "Exception message is: \n" << message << "\n";
		XMLString::release(&message);
		return NULL;
	}
	catch (const DOMException& toCatch) {
		char* message = XMLString::transcode(toCatch.msg);
		(*kinky::io::out) << "Exception message is: \n" << message << "\n";
		XMLString::release(&message);
		return NULL;
	}
	catch (...) {
		(*kinky::io::out) << "Unexpected Exception \n";
		return NULL;
	}

	managed_ptr<KObject> *ret = new managed_ptr<KObject>((new KObject("xml")));
	KObject* root = new KObject("xml");
	ostringstream encoding("");
	if (doc->getInputEncoding() != NULL) {
		char* enc = XMLString::transcode(doc->getInputEncoding());
		encoding << enc << flush;
		XMLString::release(&enc);

	}
	else {
		encoding << "UTF-8" << flush;
	}

	if (doc->getFirstChild() == NULL) {
		XMLPlatformUtils::Terminate();
		ostringstream oss;
		oss << "the provided file is not a valid XML: " << xmlFile << flush;
		throw new KParseException(500, new managed_ptr<string>(new string(oss.str())));
	}

	char* nodeName = XMLString::transcode(doc->getFirstChild()->getNodeName());
	(*ret)->setObject((string) nodeName, root);
	XMLString::release(&nodeName);
	KXML::convert(doc->getFirstChild(), root, encoding.str());
	delete parser;
	XMLPlatformUtils::Terminate();
	return static_cast<KObject*>(ret->pointed());
}

kinky::memory::managed_ptr<string>* kinky::lang::KXML::utf8Encode(string str) {
	stringstream oss("");
	for (size_t i = 0; i != str.length(); i++) {
		if (((unsigned char) str[i]) > 127) {
			oss << "\\u00" << hex << ((int) ((unsigned char) str[i])) << flush;
		}
		else {
			oss << str[i] << flush;
		}
	}
	return new managed_ptr<string>(new string(oss.str()));
}

kinky::memory::managed_ptr<string>* kinky::lang::KXML::transcode(string encoding, const XMLCh* source) {
	XMLTranscoder* utf8Transcoder;
	XMLTransService::Codes failReason;
	utf8Transcoder = XMLPlatformUtils::fgTransService->makeNewTranscoderFor("UTF-8", failReason, 16 * 1024);

	int len = XMLString::stringLen(source);

	XMLByte* untranscoded = new XMLByte[len * 4 + 1];
	XMLSize_t eaten;
	unsigned int utf8Len = utf8Transcoder->transcodeTo(source, (unsigned long int) len, untranscoded, (unsigned long int) len * 4, eaten, XMLTranscoder::UnRep_RepChar);

	untranscoded[utf8Len] = '\0';

	string* val = new string((char*) untranscoded);
	kinky::utils::trim(*val);
	managed_ptr<string>* ret = new managed_ptr<string>(val);
	delete utf8Transcoder;
	delete[] untranscoded;

	return ret;
}

void kinky::lang::KXML::convert(DOMNode* obj, KObject* kobj, string encoding) {
	enum DOMNode::NodeType type = obj->getNodeType();

	DOMNamedNodeMap* atts = obj->getAttributes();
	for (XMLSize_t it = 0; it != atts->getLength(); it++) {
		char* keyChar = XMLString::transcode(atts->item(it)->getNodeName());
		string key = (string) keyChar;

		managed_ptr<string>* value = KXML::transcode(encoding, atts->item(it)->getNodeValue());
		if (value == NULL) {
			XMLString::release(&keyChar);
			continue;
		}
		istringstream iss(**value);
		int i = 0;
		iss >> i;
		if (!iss.eof()) {
			iss.str(**value);
			double d = 0;
			iss >> d;
			if (!iss.eof()) {
				string bexpr((**value).data());
				std::transform(bexpr.begin(), bexpr.end(), bexpr.begin(), ::tolower);
				if (bexpr != "true" && bexpr != "false") {
					kobj->setString(key, **value);
				}
				else {
					kobj->setBoolean(key, bexpr == "true");
				}
			}
			else {
				kobj->setDouble(key, d);
			}
		}
		else {
			kobj->setInteger(key, i);
		}
		value->release();
		XMLString::release(&keyChar);
	}

	switch (type) {
		case DOMNode::ELEMENT_NODE: {
			DOMNodeList* children = obj->getChildNodes();
			for (size_t it = 0; it != children->getLength(); it++) {
				char* keyChar = XMLString::transcode(children->item(it)->getNodeName());
				string key = (string) keyChar;
				if (key == "#text") {
					if (((DOMText*) children->item(it))->isIgnorableWhitespace() || children->getLength() != 1 || (it < children->getLength() - 1 && children->item(it + 1)->getNodeType() == DOMNode::ELEMENT_NODE)) {
						continue;
					}
					managed_ptr<string>* value = KXML::transcode(encoding, children->item(it)->getNodeValue());
					if (value == NULL) {
						continue;
					}
					XMLString::release(&keyChar);
					if (kobj->size() == 0) {
						throw new KParseException(0, value);
					}
					else {
						kobj->setString("#text", *value->get());
						value->release();
						continue;
					}
				}

				if ((*kobj)[key] != NULL_KOBJECT) {
					KObject* arrObj = new KObject(KObject::ARRAY, "xml");

					switch ((*kobj)[key].type) {
						case KObject::OBJECT: {
							managed_ptr<KObject>* oldObj = new managed_ptr<KObject>(kobj->getObject(key));
							kobj->unset(key);
							arrObj->add(oldObj, KObject::OBJECT);
							kobj->setArray(key, arrObj);
							break;
						}
						case KObject::ARRAY: {
							delete arrObj;
							arrObj = (*kobj)[key];
							break;
						}
						case KObject::BOOL: {
							bool oldObj = (*kobj)[key];
							kobj->unset(key);
							arrObj->add(oldObj);
							kobj->setArray(key, arrObj);
							break;
						}
						case KObject::INT: {
							int oldObj = (*kobj)[key];
							kobj->unset(key);
							arrObj->add(oldObj);
							kobj->setArray(key, arrObj);
							break;
						}
						case KObject::DOUBLE: {
							double oldObj = (*kobj)[key];
							kobj->unset(key);
							arrObj->add(oldObj);
							kobj->setArray(key, arrObj);
							break;
						}
						case KObject::STRING: {
							managed_ptr<string>* oldObj = new managed_ptr<string>(kobj->getString(key));
							kobj->unset(key);
							arrObj->add(oldObj, KObject::STRING);
							kobj->setArray(key, arrObj);
							break;
						}
						default: {
						}
					}
					enum DOMNode::NodeType attType = children->item(it)->getNodeType();

					switch (attType) {
						case DOMNode::ELEMENT_NODE: {
							KObject* attObj = new KObject("xml");
							try {
								KXML::convert(children->item(it), attObj, encoding);
								arrObj->add(attObj);
							}
							catch (KParseException* e) {
								delete attObj;

								managed_ptr<string>* value = e->get();
								istringstream iss(**value);
								int i = 0;
								iss >> i;
								if (!iss.eof()) {
									iss.str(**value);
									double d = 0;
									iss >> d;
									if (!iss.eof()) {
										string bexpr((**value).data());
										std::transform(bexpr.begin(), bexpr.end(), bexpr.begin(), ::tolower);
										if (bexpr != "true" && bexpr != "false") {
											arrObj->add(value->get());
										}
										else {
											arrObj->add(bexpr == "true");
										}
									}
									else {
										arrObj->add(d);
									}
								}
								else {
									arrObj->add(i);
								}
								delete e;
							}
							break;
						}
						case DOMNode::TEXT_NODE: {
							managed_ptr<string>* value = KXML::transcode(encoding, children->item(it)->getNodeValue());
							if (value == NULL) {
								XMLString::release(&keyChar);
								continue;
							}
							istringstream iss(**value);
							int i = 0;
							iss >> i;
							if (!iss.eof()) {
								iss.str(**value);
								double d = 0;
								iss >> d;
								if (!iss.eof()) {
									string bexpr((**value).data());
									std::transform(bexpr.begin(), bexpr.end(), bexpr.begin(), ::tolower);
									if (bexpr != "true" && bexpr != "false") {
										arrObj->add(value->get());
									}
									else {
										arrObj->add(bexpr == "true");
									}
								}
								else {
									arrObj->add(d);
								}
							}
							else {
								arrObj->add(i);
							}
							value->release();
							break;
						}
						case DOMNode::CDATA_SECTION_NODE: {
							managed_ptr<string>* value = KXML::transcode(encoding, children->item(it)->getNodeValue());
							if (value == NULL) {
								XMLString::release(&keyChar);
								continue;
							}
							arrObj->add(value->get());
							value->release();
							break;
						}
						default: {
							break;
						}
					}

				}
				else {
					enum DOMNode::NodeType attType = children->item(it)->getNodeType();

					switch (attType) {
						case DOMNode::ELEMENT_NODE: {
							KObject* attObj = new KObject("xml");
							try {
								KXML::convert(children->item(it), attObj, encoding);
								kobj->setObject(key, attObj);
							}
							catch (KParseException* e) {
								delete attObj;

								managed_ptr<string>* value = e->get();
								istringstream iss(**value);
								int i = 0;
								iss >> i;
								if (!iss.eof()) {
									iss.str(**value);
									double d = 0;
									iss >> d;
									if (!iss.eof()) {
										string bexpr((**value).data());
										std::transform(bexpr.begin(), bexpr.end(), bexpr.begin(), ::tolower);
										if (bexpr != "true" && bexpr != "false") {
											kobj->setString(key, **value);
										}
										else {
											kobj->setBoolean(key, bexpr == "true");
										}
									}
									else {
										kobj->setDouble(key, d);
									}
								}
								else {
									kobj->setInteger(key, i);
								}
								delete e;
							}
							break;
						}
						case DOMNode::ATTRIBUTE_NODE:
						case DOMNode::TEXT_NODE: {
							managed_ptr<string>* value = KXML::transcode(encoding, children->item(it)->getNodeValue());
							if (value == NULL) {
								XMLString::release(&keyChar);
								continue;
							}
							istringstream iss(**value);
							int i = 0;
							iss >> i;
							if (!iss.eof()) {
								iss.str(**value);
								double d = 0;
								iss >> d;
								if (!iss.eof()) {
									string bexpr((**value).data());
									std::transform(bexpr.begin(), bexpr.end(), bexpr.begin(), ::tolower);
									if (bexpr != "true" && bexpr != "false") {
										kobj->setString(key, **value);
									}
									else {
										kobj->setBoolean(key, bexpr == "true");
									}
								}
								else {
									kobj->setDouble(key, d);
								}
							}
							else {
								kobj->setInteger(key, i);
							}
							value->release();
							break;
						}
						case DOMNode::CDATA_SECTION_NODE: {
							managed_ptr<string>* value = KXML::transcode(encoding, children->item(it)->getNodeValue());
							if (value == NULL) {
								XMLString::release(&keyChar);
								continue;
							}
							kobj->setString(key, **value);
							value->release();
							break;
						}
						default: {
							break;
						}
					}
				}
				XMLString::release(&keyChar);
			}

			break;
		}
		case DOMNode::TEXT_NODE: {
			managed_ptr<string>* value = KXML::transcode(encoding, obj->getNodeValue());
			if (value == NULL) {
				break;
			}
			istringstream iss(**value);
			int i = 0;
			iss >> i;
			if (!iss.eof()) {
				iss.str(**value);
				double d = 0;
				iss >> d;
				if (!iss.eof()) {
					string bexpr((**value).data());
					std::transform(bexpr.begin(), bexpr.end(), bexpr.begin(), ::tolower);
					if (bexpr != "true" && bexpr != "false") {
						kobj->setString("#text", **value);
					}
					else {
						kobj->setBoolean("#text", bexpr == "true");
					}
				}
				else {
					kobj->setDouble("#text", d);
				}
			}
			else {
				kobj->setInteger("#text", i);
			}
			value->release();
			break;
		}
		case DOMNode::CDATA_SECTION_NODE: {
			managed_ptr<string>* value = KXML::transcode(encoding, obj->getNodeValue());
			if (value == NULL) {
				break;
			}
			kobj->setString("#text", **value);
			value->release();
			break;
		}
		default: {
			break;
		}

	}

}
