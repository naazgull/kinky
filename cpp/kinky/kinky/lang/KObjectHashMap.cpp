#include <kinky/lang/KObjectHashMap.h>

#ifdef USE_SPARSEHASH

#include <kinky/lang/KJSON.h>

using namespace std;
using namespace __gnu_cxx;

//GCRY_THREAD_OPTION_PTHREAD_IMPL;

//>>>
// trio nested class
//
//
//

#define K_NOT_EQUALS 1
#define K_LOWER_EQUALS 2
#define K_GREATER_EQUALS 3
#define K_LOWER 4
#define K_GREATER 5
#define K_EQUALS 6

kinky::lang::KObject::trio::trio(int typeParam, string keyParam) :
type(typeParam), key(keyParam), value(NULL), pos(0) {
}

kinky::lang::KObject::trio::trio(int typeParam, string keyParam, kinky::memory::abstract_managed_ptr* valueParam) :
type(typeParam), key(keyParam), value(valueParam), pos(0) {
}

kinky::lang::KObject::trio::~trio() {
	if (this->value != NULL && !KMemoryManager::getMemory()->disposed(this->value)) {
		this->value->release();
	}
	this->value = NULL;
}

bool kinky::lang::KObject::trio::operator==(trio* t) {
	return this == t;
}

bool kinky::lang::KObject::trio::operator==(trio& t) {
	if (t.type != this->type) {
		return false;
	}
	switch (t.type) {
		case KObject::OBJECT: {
			return *static_cast<KObject*>(t.value->pointed()) == *static_cast<KObject*>(this->value->pointed());
		}
		case KObject::ARRAY: {
			return *static_cast<KObject*>(t.value->pointed()) == *static_cast<KObject*>(this->value->pointed());
		}
		case KObject::BOOL: {
			return *static_cast<bool*>(t.value->pointed()) == *static_cast<bool*>(this->value->pointed());
		}
		case KObject::INT: {
			return *static_cast<bool*>(t.value->pointed()) == *static_cast<bool*>(this->value->pointed());
		}
		case KObject::DOUBLE: {
			return *static_cast<bool*>(t.value->pointed()) == *static_cast<bool*>(this->value->pointed());
		}
		case KObject::STRING: {
			return *static_cast<bool*>(t.value->pointed()) == *static_cast<bool*>(this->value->pointed());
		}
		case KObject::NIL: {
			return true;
		}
	}
	return false;
}

bool kinky::lang::KObject::trio::operator==(KObject& t) {
	if (t.type != this->type) {
		return false;
	}
	switch (t.type) {
		case KObject::OBJECT: {
			return t == *static_cast<KObject*>(this->value->pointed());
		}
		case KObject::ARRAY: {
			return t == *static_cast<KObject*>(this->value->pointed());
		}
	}
	return false;
}

bool kinky::lang::KObject::trio::operator==(string& t) {
	if (this->type != KObject::STRING) {
		return false;
	}
	return t == *static_cast<string*>(this->value->pointed());
}

bool kinky::lang::KObject::trio::operator==(bool& t) {
	if (this->type != KObject::BOOL) {
		return false;
	}
	return t == *static_cast<bool*>(this->value->pointed());
}

bool kinky::lang::KObject::trio::operator==(int& t) {
	if (this->type != KObject::INT) {
		return false;
	}
	return t == *static_cast<int*>(this->value->pointed());
}

bool kinky::lang::KObject::trio::operator==(double& t) {
	if (this->type != KObject::DOUBLE) {
		return false;
	}
	return t == *static_cast<double*>(this->value->pointed());
}

bool kinky::lang::KObject::trio::operator!=(trio* t) {
	return this != t;
}

bool kinky::lang::KObject::trio::operator!=(trio& t) {
	return !(*this == t);
}

bool kinky::lang::KObject::trio::operator!=(KObject& t) {
	return !(*this == t);
}

bool kinky::lang::KObject::trio::operator!=(string& t) {
	return !(*this == t);
}

bool kinky::lang::KObject::trio::operator!=(bool& t) {
	return !(*this == t);
}

bool kinky::lang::KObject::trio::operator!=(int& t) {
	return !(*this == t);
}

bool kinky::lang::KObject::trio::operator!=(double& t) {
	return !(*this == t);
}

kinky::lang::KObject::trio::operator string() {
	if (this->type != KObject::STRING) {
		switch (this->type) {
			case KObject::OBJECT: {
				ostringstream oss;
				KObject::stringifyJSON((static_cast<KObject*>(this->value->pointed())), oss);
				oss << flush;
				managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
				return (**ret);
			}
			case KObject::ARRAY: {
				ostringstream oss;
				KObject::stringifyJSON((static_cast<KObject*>(this->value->pointed())), oss);
				oss << flush;
				managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
				return (**ret);
			}
			case KObject::BOOL: {
				ostringstream oss;
				oss << *static_cast<bool*>(this->value->pointed()) << flush;
				managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
				return (**ret);
			}
			case KObject::INT: {
				ostringstream oss;
				oss << *static_cast<int*>(this->value->pointed()) << flush;
				managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
				return (**ret);
			}
			case KObject::DOUBLE: {
				ostringstream oss;
				oss << *static_cast<double*>(this->value->pointed()) << flush;
				managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
				return (**ret);
			}
		}
	}
	if (this->value != NULL) {
		return *static_cast<string*>(this->value->pointed());
	}
	else {
		return "";
	}
}

kinky::lang::trio::operator bool() {
	if (this->type != KObject::BOOL) {
		if (this->type == KObject::STRING) {
			string s = *static_cast<string*>(this->value->pointed());
			return s == "true";
		}
		else if (this->type == KObject::INT) {
			return (bool) *(static_cast<int*>(this->value->pointed()));
		}
		else if (this->type == KObject::DOUBLE) {
			return (bool) *(static_cast<double*>(this->value->pointed()));
		}
		return false;
	}
	return *(static_cast<bool*>(this->value->pointed()));
}

kinky::lang::trio::operator int() {
	if (this->type != KObject::INT) {
		if (this->type == KObject::STRING) {
			istringstream iss(*static_cast<string*>(this->value->pointed()));
			int b = 0;
			iss >> b;
			return b;
		}
		else if (this->type == KObject::DOUBLE) {
			return (int) *(static_cast<double*>(this->value->pointed()));
		}
		else if (this->type == KObject::BOOL) {
			return (int) *(static_cast<bool*>(this->value->pointed()));
		}
		return 0;
	}
	return *(static_cast<int*>(this->value->pointed()));
}

kinky::lang::trio::operator double() {
	if (this->type != KObject::DOUBLE) {
		if (this->type == KObject::STRING) {
			istringstream iss(*static_cast<string*>(this->value->pointed()));
			double b = 0;
			iss >> b;
			return b;
		}
		else if (this->type == KObject::INT) {
			return (double) *(static_cast<int*>(this->value->pointed()));
		}
		else if (this->type == KObject::BOOL) {
			return (double) *(static_cast<bool*>(this->value->pointed()));
		}
		return 0;
	}
	return *(static_cast<double*>(this->value->pointed()));
}

kinky::lang::KObject::trio::operator string*() {
	if (this->type != KObject::STRING) {
		return NULL;
	}
	return static_cast<string*>(this->value->pointed());
}

kinky::lang::KObject::trio::operator KObject*() {
	if (this->type != KObject::OBJECT && this->type != KObject::ARRAY) {
		return NULL;
	}
	return static_cast<KObject*>(this->value->pointed());
}

kinky::lang::KObject::trio& kinky::lang::KObject::trio::operator[](int idx) {
	if ((this->type != KObject::ARRAY && this->type != KObject::OBJECT) || idx >= (int) static_cast<KObject*>(this->value->pointed())->size() || idx < 0) {
		return NULL_KOBJECT;
	}
	return *(*static_cast<KObject*>(this->value->pointed())->atts)[idx];
}

kinky::lang::KObject::trio& kinky::lang::KObject::trio::operator[](const char* name) {
	if (this->type != KObject::ARRAY && this->type != KObject::OBJECT) {
		return NULL_KOBJECT;
	}
	KObjAttributeNames::iterator att = static_cast<KObject*>(this->value->pointed())->names->find(name);
	if (att != static_cast<KObject*>(this->value->pointed())->names->end()) {
		return *att->second;
	}
	return NULL_KOBJECT;
}

kinky::lang::KObject::trio& kinky::lang::KObject::trio::operator[](string name) {
	if (this->type != KObject::ARRAY && this->type != KObject::OBJECT) {
		return NULL_KOBJECT;
	}
	KObjAttributeNames::iterator att = static_cast<KObject*>(this->value->pointed())->names->find(name);
	if (att != static_cast<KObject*>(this->value->pointed())->names->end()) {
		return *att->second;
	}
	return NULL_KOBJECT;
}
//
//
// <<<

// >>>
// KObject class
//
//
//

kinky::lang::KObject::trio* kinky::lang::k_null_object = new kinky::lang::KObject::trio(kinky::lang::KObject::NIL, "#text");

kinky::lang::KObject::KObject() :
type(KObject::OBJECT), status(200), headers(new KObject::KObjHeaders()), atts(new KObject::KObjAttributes()), names(new KObject::KObjAttributeNames()), baseFormat(new string("json")), cookie(NULL), temp(NULL) {
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
}

kinky::lang::KObject::KObject(int typeParam) :
type(typeParam), status(200), headers(new KObject::KObjHeaders()), atts(new KObject::KObjAttributes()), names(new KObject::KObjAttributeNames()), baseFormat(new string("json")), cookie(NULL), temp(NULL) {
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
}

kinky::lang::KObject::KObject(int typeParam, string baseFormatParam) :
type(typeParam), status(200), headers(new KObject::KObjHeaders()), atts(new KObject::KObjAttributes()), names(new KObject::KObjAttributeNames()), baseFormat(new string(baseFormatParam)), cookie(NULL), temp(NULL) {
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
}

kinky::lang::KObject::KObject(int typeParam, string* baseFormatParam) :
type(typeParam), status(200), headers(new KObject::KObjHeaders()), atts(new KObject::KObjAttributes()), names(new KObject::KObjAttributeNames()), baseFormat(new string(*baseFormatParam)), cookie(NULL), temp(NULL) {
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
}

kinky::lang::KObject::KObject(int typeParam, int statusParam) :
type(typeParam), status(statusParam), headers(new KObject::KObjHeaders()), atts(new KObject::KObjAttributes()), names(new KObject::KObjAttributeNames()), baseFormat(new string("json")), cookie(NULL), temp(NULL) {
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
}

kinky::lang::KObject::KObject(int typeParam, string baseFormatParam, int statusParam) :
type(typeParam), status(statusParam), headers(new KObject::KObjHeaders()), atts(new KObject::KObjAttributes()), names(new KObject::KObjAttributeNames()), baseFormat(new string(baseFormatParam)), cookie(NULL), temp(NULL) {
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
}

kinky::lang::KObject::KObject(string baseFormatParam) :
type(KObject::OBJECT), status(200), headers(new KObject::KObjHeaders()), atts(new KObject::KObjAttributes()), names(new KObject::KObjAttributeNames()), baseFormat(new string(baseFormatParam)), cookie(NULL), temp(NULL) {
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
}

kinky::lang::KObject::KObject(string baseFormatParam, int statusParam) :
type(KObject::OBJECT), status(statusParam), headers(new KObject::KObjHeaders()), atts(new KObject::KObjAttributes()), names(new KObject::KObjAttributeNames()), baseFormat(new string(baseFormatParam)), cookie(NULL), temp(NULL) {
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
}

kinky::lang::KObject::KObject(string* baseFormatParam) :
type(KObject::OBJECT), status(200), headers(new KObject::KObjHeaders()), atts(new KObject::KObjAttributes()), names(new KObject::KObjAttributeNames()), baseFormat(new string(*baseFormatParam)), cookie(NULL), temp(NULL) {
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
}

kinky::lang::KObject::KObject(string* baseFormatParam, int statusParam) :
type(KObject::OBJECT), status(statusParam), headers(new KObject::KObjHeaders()), atts(new KObject::KObjAttributes()), names(new KObject::KObjAttributeNames()), baseFormat(new string(*baseFormatParam)), cookie(NULL), temp(NULL) {
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
}

kinky::lang::KObject::~KObject() {
	for (KObject::KObjAttributes::iterator it = this->atts->begin(); it != this->atts->end(); it++) {
		delete (*it);
	}
	for (KObject::KObjHeaders::iterator it = this->headers->begin(); it != this->headers->end(); it++) {
		delete it->second;
	}
	this->headers->clear();
	this->headers->resize(0);
	delete this->headers;
	this->atts->clear();
	this->atts->resize(0);
	delete this->atts;
	this->names->clear();
	delete this->names;
	delete this->baseFormat;

	if (this->cookie != NULL) {
		for (KObject::KObjCookie::iterator k = this->cookie->begin(); k != this->cookie->end(); k++) {
			delete k->second;
		}
		this->cookie->clear();
		this->cookie->resize(0);
		delete this->cookie;
	}

	KMemoryManager::getMemory()->releasePointee(this);
}

kinky::lang::KObject::trio* kinky::lang::KObject::evalObjectPath(string name) {
	istringstream iss;
	iss.str(name);

	bool first = true;
	string part;
	kinky::lang::KObject::trio* cur = NULL;
	while (std::getline(iss, part, '.')) {
		if (part.find('[') != string::npos) {
			size_t lpar = 0;
			string name(part.substr(0, lpar));

			if (first) {
				if (name.length() == 0) {
					cur = new trio(this->type, "this", new managed_ptr<KObject>(this));
					(new managed_ptr<trio>(cur));
					first = false;
				}
				else {
					cur = &((*this)[name]);
					first = false;
				}
			}
			else {
				if (name.length() != 0) {
					cur = &((*cur)[name]);
				}
			}

			if (cur->type == KObject::NIL) {
				return NULL;
			}

			while((lpar = part.find('[')) != string::npos) {
				int index = -1;

				size_t rpar = part.find(']', lpar);
				string exp = part.substr(lpar + 1, rpar - lpar - 1);

				stringstream ss;
				ss << exp << flush;
				ss >> index;

				string rest;
				if (part.length() > rpar) {
					rest.assign(part.substr(rpar + 1));
				}
				part.assign(part.substr(0, lpar));
				part.insert(part.length(), rest);

				if (ss.eof() && index != -1) {
					cur = &((*cur)[index]);

					if (cur->type == KObject::NIL) {
						return NULL;
					}
				}
				else {
					if (cur->type != KObject::OBJECT && cur->type != KObject::ARRAY) {
						return NULL;
					}

					size_t opindex = -1;
					int op = -1;
					string field;
					string value;
					if((opindex = exp.find("!=")) != string::npos) {
						field.assign(exp.substr(0, opindex));
						value.assign(exp.substr(opindex + 2));
						op = K_NOT_EQUALS;
					}
					else if((opindex = exp.find("<=")) != string::npos) {
						field.assign(exp.substr(0, opindex));
						value.assign(exp.substr(opindex + 2));
						op = K_LOWER_EQUALS;
					}
					else if((opindex = exp.find(">=")) != string::npos) {
						field.assign(exp.substr(0, opindex));
						value.assign(exp.substr(opindex + 2));
						op = K_GREATER_EQUALS;
					}
					else if((opindex = exp.find("<")) != string::npos) {
						field.assign(exp.substr(0, opindex));
						value.assign(exp.substr(opindex + 1));
						op = K_LOWER;
					}
					else if((opindex = exp.find(">")) != string::npos) {
						field.assign(exp.substr(0, opindex));
						value.assign(exp.substr(opindex + 1));
						op = K_GREATER;
					}
					else if((opindex = exp.find("=")) != string::npos) {
						field.assign(exp.substr(0, opindex));
						value.assign(exp.substr(opindex + 1));
						op = K_EQUALS;
					}
					kinky::utils::trim(field);
					kinky::utils::trim(value);

					KObject* obj = (KObject*) (*cur);
					KObject* results = new KObject(KObject::ARRAY);
					if (obj->type == KObject::ARRAY) {
						for (KObject::KObjAttributes::iterator i = obj->begin(); i != obj->end(); i++) {
							if ((**i)[field] == NULL_KOBJECT) {
								continue;
							}
							switch (op) {
								case K_NOT_EQUALS: {
									if (((string) (**i)[field]) != value) {
										(*results) << (KObject*) (**i);
									}
									break;
								}
								case K_LOWER_EQUALS: {
									break;
								}
								case K_GREATER_EQUALS: {
									break;
								}
								case K_LOWER: {
									break;
								}
								case K_GREATER: {
									break;
								}
								case K_EQUALS: {
									if (((string) (**i)[field]) == value) {
										(*results) << (KObject*) (**i);
									}
									break;
								}

							}
						}
					}
					else {
						if ((*obj)[field] != NULL_KOBJECT) {
							switch (op) {
								case K_NOT_EQUALS: {
									if (((string) (*obj)[field]) != value) {
										(*results) << obj;
									}
									break;
								}
								case K_LOWER_EQUALS: {
									break;
								}
								case K_GREATER_EQUALS: {
									break;
								}
								case K_LOWER: {
									break;
								}
								case K_GREATER: {
									break;
								}
								case K_EQUALS: {
									if (((string) (*obj)[field]) == value) {
										(*results) << obj;
									}
									break;
								}

							}
						}
					}
					if (results->size() == 0) {
						delete results;
						return NULL;
					}
					else if (results->size() == 1) {
						cur = new trio(KObject::ARRAY, name, new managed_ptr<KObject>((KObject*) (*results)[0]));
						(new managed_ptr<trio>(cur));
						delete results;
					}
					else {
						cur = new trio(KObject::ARRAY, name, new managed_ptr<KObject>(results));
						(new managed_ptr<trio>(cur));
					}
				}
			}
		}
		else {
			if (first) {
				cur = &((*this)[part]);
				first = false;
			}
			else {
				cur = &((*cur)[part]);
			}

			if (cur->type == KObject::NIL) {
				return NULL;
			}
		}
	}
	return cur;
}

string kinky::lang::KObject::get(string name) {
	KObjAttributeNames::iterator att = this->names->find(name);
	if (att != this->names->end()) {
		switch (att->second->type) {
			case KObject::OBJECT: {
				ostringstream oss;
				(static_cast<KObject*>(att->second->value->pointed()))->stringify(oss);
				managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
				return (**ret);
			}
			case KObject::ARRAY: {
				ostringstream oss;
				(static_cast<KObject*>(att->second->value->pointed()))->stringify(oss);
				managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
				return (**ret);
			}
			case KObject::BOOL: {
				ostringstream oss;
				oss << *static_cast<bool*>(att->second->value->pointed()) << flush;
				managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
				return (**ret);
			}
			case KObject::INT: {
				ostringstream oss;
				oss << *static_cast<int*>(att->second->value->pointed()) << flush;
				managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
				return (**ret);
			}
			case KObject::DOUBLE: {
				ostringstream oss;
				oss << *static_cast<double*>(att->second->value->pointed()) << flush;
				managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
				return (**ret);
			}
			case KObject::STRING: {
				return *static_cast<string*>(att->second->value->pointed());
			}
			case KObject::NIL: {
				return "null";
			}
			default: {
				return "undefined";
			}
		}
	}
	return "null";
}

string kinky::lang::KObject::get(int idx) {
	if (idx >= (int) this->atts->size() || idx < 0) {
		return "null";
	}
	switch ((*this->atts)[idx]->type) {
		case KObject::OBJECT: {
			ostringstream oss;
			(static_cast<KObject*>((*this->atts)[idx]->value->pointed()))->stringify(oss);
			managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
			return (**ret);
		}
		case KObject::ARRAY: {
			ostringstream oss;
			(static_cast<KObject*>((*this->atts)[idx]->value->pointed()))->stringify(oss);
			managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
			return (**ret);
		}
		case KObject::BOOL: {
			ostringstream oss;
			oss << *static_cast<bool*>((*this->atts)[idx]->value->pointed()) << flush;
			managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
			return (**ret);
		}
		case KObject::INT: {
			ostringstream oss;
			oss << *static_cast<int*>((*this->atts)[idx]->value->pointed()) << flush;
			managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
			return (**ret);
		}
		case KObject::DOUBLE: {
			ostringstream oss;
			oss << *static_cast<double*>((*this->atts)[idx]->value->pointed()) << flush;
			managed_ptr<string>* ret = new managed_ptr<string>(new string(oss.str()));
			return (**ret);
		}
		case KObject::STRING: {
			return *static_cast<string*>((*this->atts)[idx]->value->pointed());
		}
		case KObject::NIL: {
			return "null";
		}
		default: {
			return "undefined";
		}
	}
}

int kinky::lang::KObject::getType(string name) {
	KObjAttributeNames::iterator att = this->names->find(name);
	if (att != this->names->end()) {
		return att->second->type;
	}
	return KObject::NIL;
}

int kinky::lang::KObject::getType(int idx) {
	if (idx > -1 && idx < (int) this->atts->size()) {
		return (*this->atts)[idx]->type;
	}
	return KObject::NIL;
}

string* kinky::lang::KObject::getQueryString(string params[], int length) {
	string* ret = new string();

	if (length == 0 || params == NULL) {
		for (KObject::KObjAttributes::iterator it = this->begin(); it != this->end(); it++) {
			if (it != this->begin()) {
				ret->insert(ret->length(), "&");
			}
			string value = (string) (*this)[(*it)->key];
			ret->insert(ret->length(), (*it)->key);
			ret->insert(ret->length(), "=");
			ret->insert(ret->length(), value);
		}
	}
	else {
		bool hasOne = false;
		for (int i = 0; i != length; i++) {
			if (hasOne) {
				ret->insert(ret->length(), "&");
			}
			if ((*this)[params[i]] != NULL_KOBJECT) {
				string value = (string) (*this)[params[i]];
				ret->insert(ret->length(), params[i]);
				ret->insert(ret->length(), "=");
				ret->insert(ret->length(), value);
				hasOne = true;
			}
		}
	}

	return ret;
}

bool* kinky::lang::KObject::getBoolean(string name) {
	try {
		return static_cast<bool*>(this->getIt(name, KObject::BOOL)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

int* kinky::lang::KObject::getInteger(string name) {
	try {
		return static_cast<int*>(this->getIt(name, KObject::INT)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

double* kinky::lang::KObject::getDouble(string name) {
	try {
		return static_cast<double*>(this->getIt(name, KObject::DOUBLE)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

string* kinky::lang::KObject::getString(string name) {
	try {
		return static_cast<string*>(this->getIt(name, KObject::STRING)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

kinky::lang::KObject* kinky::lang::KObject::getObject(string name) {
	try {
		return static_cast<KObject*>(this->getIt(name, KObject::OBJECT)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

kinky::lang::KObject* kinky::lang::KObject::getArray(string name) {
	try {
		return static_cast<KObject*>(this->getIt(name, KObject::ARRAY)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

bool* kinky::lang::KObject::getBoolean(int idx) {
	try {
		return static_cast<bool*>(this->getIt(idx, KObject::BOOL)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

int* kinky::lang::KObject::getInteger(int idx) {
	try {
		return static_cast<int*>(this->getIt(idx, KObject::INT)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

double* kinky::lang::KObject::getDouble(int idx) {
	try {
		return static_cast<double*>(this->getIt(idx, KObject::DOUBLE)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

string* kinky::lang::KObject::getString(int idx) {
	try {
		return static_cast<string*>(this->getIt(idx, KObject::STRING)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

kinky::lang::KObject* kinky::lang::KObject::getObject(int idx) {
	try {
		return static_cast<KObject*>(this->getIt(idx, KObject::OBJECT)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

kinky::lang::KObject* kinky::lang::KObject::getArray(int idx) {
	try {
		return static_cast<KObject*>(this->getIt(idx, KObject::ARRAY)->pointed());
	}
	catch (KObjectNULLException* e) {
		delete e;
	}

	return NULL;
}

void kinky::lang::KObject::setBoolean(string name, bool value) {
	this->setIt(name, KObject::BOOL, new managed_ptr<bool>(new bool(value)));
}

void kinky::lang::KObject::setInteger(string name, int value) {
	this->setIt(name, KObject::INT, new managed_ptr<int>(new int(value)));
}

void kinky::lang::KObject::setDouble(string name, double value) {
	this->setIt(name, KObject::DOUBLE, new managed_ptr<double>(new double(value)));
}

void kinky::lang::KObject::setString(string name, string value) {
	this->setIt(name, KObject::STRING, new managed_ptr<string>(new string(value)));
}

void kinky::lang::KObject::setObject(string name, KObject* value) {
	value->base(this->base()->data());
	this->setIt(name, value->type, new managed_ptr<KObject>(value));
}

void kinky::lang::KObject::setArray(string name, KObject* value) {
	value->base(this->base()->data());
	this->setIt(name, KObject::ARRAY, new managed_ptr<KObject>(value));
}

void kinky::lang::KObject::setNil(string name) {
	this->setIt(name, KObject::NIL, NULL);
}

void kinky::lang::KObject::setBoolean(int idx, bool value) {
	this->setIt(idx, KObject::BOOL, new managed_ptr<bool>(new bool(value)));
}

void kinky::lang::KObject::setInteger(int idx, int value) {
	this->setIt(idx, KObject::INT, new managed_ptr<int>(new int(value)));
}

void kinky::lang::KObject::setDouble(int idx, double value) {
	this->setIt(idx, KObject::DOUBLE, new managed_ptr<double>(new double(value)));
}

void kinky::lang::KObject::setString(int idx, string value) {
	this->setIt(idx, KObject::STRING, new managed_ptr<string>(new string(value)));
}

void kinky::lang::KObject::setObject(int idx, KObject* value) {
	value->base(this->base()->data());
	this->setIt(idx, KObject::OBJECT, new managed_ptr<KObject>(value));
}

void kinky::lang::KObject::setArray(int idx, KObject* value) {
	value->base(this->base()->data());
	this->setIt(idx, KObject::ARRAY, new managed_ptr<KObject>(value));
}

void kinky::lang::KObject::setNil(int idx) {
	this->setIt(idx, KObject::NIL, NULL);
}

int kinky::lang::KObject::add(kinky::memory::abstract_managed_ptr* value, int type) {
	if (type == KObject::OBJECT || type == KObject::ARRAY) {
		static_cast<KObject*>(value->pointed())->base(this->base()->data());
	}
	return this->setIt(this->atts->size(), type, value);
}

int kinky::lang::KObject::add(bool value) {
	return this->setIt(this->atts->size(), KObject::BOOL, new managed_ptr<bool>(new bool(value)));
}

int kinky::lang::KObject::add(int value) {
	return this->setIt(this->atts->size(), KObject::INT, new managed_ptr<int>(new int(value)));
}

int kinky::lang::KObject::add(double value) {
	return this->setIt(this->atts->size(), KObject::DOUBLE, new managed_ptr<double>(new double(value)));
}

int kinky::lang::KObject::add(string value) {
	return this->setIt(this->atts->size(), KObject::STRING, new managed_ptr<string>(new string(value)));
}

int kinky::lang::KObject::add(KObject* value) {
	value->base(this->base()->data());
	return this->setIt(this->atts->size(), value->type, new managed_ptr<KObject>(value));
}

int kinky::lang::KObject::addAt(int idx, kinky::memory::abstract_managed_ptr* value, int type) {
	if (type == KObject::OBJECT || type == KObject::ARRAY) {
		static_cast<KObject*>(value->pointed())->base(this->base()->data());
	}
	return this->insertIt(idx, type, value);
}

int kinky::lang::KObject::addAt(int idx, bool value) {
	return this->insertIt(idx, KObject::BOOL, new managed_ptr<bool>(new bool(value)));
}

int kinky::lang::KObject::addAt(int idx, int value) {
	return this->insertIt(idx, KObject::INT, new managed_ptr<int>(new int(value)));
}

int kinky::lang::KObject::addAt(int idx, double value) {
	return this->insertIt(idx, KObject::DOUBLE, new managed_ptr<double>(new double(value)));
}

int kinky::lang::KObject::addAt(int idx, string value) {
	return this->insertIt(idx, KObject::STRING, new managed_ptr<string>(new string(value)));
}

int kinky::lang::KObject::addAt(int idx, KObject* value) {
	value->base(this->base()->data());
	return this->insertIt(idx, value->type, new managed_ptr<KObject>(value));
}

void kinky::lang::KObject::clear() {
	for (KObject::KObjAttributes::iterator it = this->atts->begin(); it != this->atts->end(); it++) {
		delete (*it);
	}
	this->atts->clear();
	this->atts->resize(0);
	delete this->atts;
	this->names->clear();
	delete this->names;

	this->atts = new KObject::KObjAttributes();
	this->names = new KObject::KObjAttributeNames();
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
}

void kinky::lang::KObject::remove(int idx) {
	this->deleteIt(idx);
}

void kinky::lang::KObject::remove(string name) {
	this->deleteIt(name);
}

void kinky::lang::KObject::unset(int idx) {
	if ((this->type != KObject::ARRAY && this->type != KObject::OBJECT) || idx < 0) {
		return;
	}

	if (idx < 0 || idx >= (int) this->atts->size()) {
		return;
	}

	string key = this->atts->at(idx)->key;
	this->names->erase(this->names->find(key));
	this->atts->erase(this->atts->begin() + idx);
	for (KObjAttributeNames::iterator it = this->names->begin(); it != this->names->end(); it++) {
		if (it->second->pos > idx) {
			it->second->pos--;
		}
	}
}

void kinky::lang::KObject::unset(string name) {
	if (this->type != KObject::ARRAY && this->type != KObject::OBJECT) {
		return;
	}

	KObjAttributeNames::iterator att = this->names->find(name);
	if (att != this->names->end()) {
		int idx = att->second->pos;
		this->atts->erase(this->atts->begin() + att->second->pos);
		this->names->erase(att);
		for (KObjAttributeNames::iterator it = this->names->begin(); it != this->names->end(); it++) {
			if (it->second->pos > idx) {
				it->second->pos--;
			}
		}
	}
}

string* kinky::lang::KObject::base() {
	return this->baseFormat;
}

void kinky::lang::KObject::base(string* base) {
	if (this->baseFormat != NULL) {
		delete this->baseFormat;
	}
	this->baseFormat = base;
}

void kinky::lang::KObject::base(string base) {
	if (this->baseFormat != NULL) {
		delete this->baseFormat;
	}
	this->baseFormat = new string(base.c_str());
}

kinky::lang::KObject::KObjAttributes::iterator kinky::lang::KObject::begin() {
	return this->atts->begin();
}

kinky::lang::KObject::KObjAttributes::iterator kinky::lang::KObject::end() {
	return this->atts->end();
}

void kinky::lang::KObject::move(string name, int offset) {
	KObjAttributeNames::iterator att = this->names->find(name);
	if (att == this->names->end()) {
		return;
	}

	if (offset < 0) {
		size_t pos = att->second->pos;
		if (pos <= 0 || (pos + offset <= 0)) {
			return;
		}
		for (size_t i = pos; i != pos + offset; i--) {
			(*this->atts)[i - 1]->pos = i;
			std::swap((*this->atts)[i], (*this->atts)[i - 1]);
		}
		att->second->pos = att->second->pos + offset;
	}
	else {
		size_t pos = att->second->pos;
		if (pos >= this->atts->size() || pos + offset >= this->atts->size()) {
			return;
		}
		for (size_t i = pos; i != pos + offset; i++) {
			(*this->atts)[i + 1]->pos = i;
			std::swap((*this->atts)[i], (*this->atts)[i + 1]);
		}
		att->second->pos = att->second->pos + offset;
	}
}

void kinky::lang::KObject::sort() {
	vector<string>* sorted = new vector<string>();
	for (KObject::KObjAttributeNames::iterator it = this->names->begin(); it != this->names->end(); it++) {
		sorted->push_back(it->first);
	}
	std::sort(sorted->begin(), sorted->end());

	KObject::KObjAttributeNames* unsortedNames = this->names;
	KObject::KObjAttributes* unsortedAtts = this->atts;

	this->names = new KObject::KObjAttributeNames();
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	this->atts = new KObject::KObjAttributes();

	for (vector<string>::iterator it = sorted->begin(); it != sorted->end(); it++) {
		KObjAttributeNames::iterator att = unsortedNames->find(*it);
		if (att != unsortedNames->end()) {
			int pos = this->atts->size();
			att->second->pos = pos;
			this->atts->push_back((*unsortedAtts)[att->second->pos]);
			this->names->insert(pair<string, trio*>(*it, (*unsortedAtts)[att->second->pos]));
		}
	}
	delete unsortedNames;
	delete unsortedAtts;
}

void kinky::lang::KObject::assign(KObject* other) {
	for (KObject::KObjAttributes::iterator it = this->atts->begin(); it != this->atts->end(); it++) {
		delete (*it);
	}
	for (KObject::KObjHeaders::iterator it = this->headers->begin(); it != this->headers->end(); it++) {
		delete it->second;
	}
	delete this->atts;
	delete this->names;
	delete this->headers;

	this->names = new KObject::KObjAttributeNames();
	string keyname = "sparsehash-delete-key";
	this->names->set_deleted_key(keyname);
	this->atts = new KObject::KObjAttributes();

	for (KObject::KObjAttributes::iterator it = other->atts->begin(); it != other->atts->end(); it++) {
		abstract_managed_ptr* ret = NULL;
		switch ((*it)->type) {
			case KObject::OBJECT: {
				ret = new managed_ptr<KObject>((KObject*) (*it)->value->pointed());
			}
			case KObject::ARRAY: {
				ret = new managed_ptr<KObject>((KObject*) (*it)->value->pointed());
			}
			case KObject::BOOL: {
				ret = new managed_ptr<bool>((bool*) (*it)->value->pointed());
			}
			case KObject::INT: {
				ret = new managed_ptr<int>((int*) (*it)->value->pointed());
			}
			case KObject::DOUBLE: {
				ret = new managed_ptr<double>((double*) (*it)->value->pointed());
			}
			case KObject::STRING: {
				ret = new managed_ptr<string>((string*) (*it)->value->pointed());
			}
			case KObject::NIL: {
			}
			default: {
			}

		}
		if (ret != NULL) {
			this->setIt((*it)->key, (*it)->type, ret);
		}
	}

	this->headers = new KObject::KObjHeaders();
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
	for (KObject::KObjHeaders::iterator it = other->headers->begin(); it != other->headers->end(); it++) {
		this->addHeader(it->first.data(), (it->second)->data());
	}
}

kinky::lang::KObject* kinky::lang::KObject::clone() {
	ostringstream oss;
	this->stringify(oss);
	KObject* ret = KJSON::parse(oss.str());
	for (KObject::KObjHeaders::iterator it = this->headers->begin(); it != this->headers->end(); it++) {
		ret->addHeader(it->first.data(), (it->second)->data());
	}
	return ret;
}

int kinky::lang::KObject::size() {
	return this->atts->size();
}

int kinky::lang::KObject::getStatus() {
	return this->status;
}

void kinky::lang::KObject::setStatus(int status) {
	this->status = status;
}

kinky::lang::KObject::KObjHeaders* kinky::lang::KObject::getHeaders() {
	return this->headers;
}

void kinky::lang::KObject::addHeader(string name, string value) {
	std::transform(name.begin(), name.end(), name.begin(), ::tolower);
	kinky::utils::trim(name);

	KObjHeaders::iterator header = this->headers->find(name);
	if (header != this->headers->end()) {
		this->headers->erase(header);
		delete header->second;
	}
	this->headers->insert(pair<string, string*>(name, new string(value)));
}

string* kinky::lang::KObject::getHeader(string name) {
	std::transform(name.begin(), name.end(), name.begin(), ::tolower);
	kinky::utils::trim(name);

	string nameStr(name);
	KObjHeaders::iterator header = this->headers->find(nameStr);
	if (header != this->headers->end()) {
		return header->second;
	}
	return NULL;
}

string* kinky::lang::KObject::getCookie(string name) {
	if (this->cookie == NULL) {
		string* cookie = NULL;
		if ((cookie = this->getHeader("cookie")) != NULL) {
			this->cookie = kinky::utils::strip_object_cookie(*cookie);
		}
		else {
			return NULL;
		}
	}
	KObjCookie::iterator param = this->cookie->find(name);
	if (param != this->cookie->end()) {
		return param->second;
	}
	return NULL;
}

kinky::lang::KObject::KObjCookie* kinky::lang::KObject::getCookies() {
	if (this->cookie == NULL) {
		string* cookie = NULL;
		if ((cookie = this->getHeader("cookie")) != NULL) {
			this->cookie = kinky::utils::strip_object_cookie(*cookie);
		}
		else {
			return NULL;
		}
	}
	return this->cookie;
}

bool kinky::lang::KObject::operator!=(KObject& t) {
	return !(*this == t);
}

bool kinky::lang::KObject::operator!=(KObject* t) {
	return this != t;
}

bool kinky::lang::KObject::operator!=(trio* t) {
	return this != t->value->pointed();
}

bool kinky::lang::KObject::operator==(KObject* t) {
	return this == t;
}

bool kinky::lang::KObject::operator==(trio* t) {
	return this == t->value->pointed();
}

bool kinky::lang::KObject::operator==(KObject& t) {
	if (t.type != this->type) {
		return false;
	}
	switch (t.type) {
		case KObject::OBJECT:
		case KObject::ARRAY: {
			if (this->size() != t.size()) {
				return false;
			}
			for (KObject::KObjAttributes::iterator it = this->begin(); it != this->end(); it++) {
				string key = (*it)->key;
				if (**it != t[key]) {
					return false;
				}
			}
			return true;
		}
		case KObject::BOOL: {
			return t.getBoolean("#text") == this->getBoolean("#text");
		}
		case KObject::INT: {
			return t.getInteger("#text") == this->getInteger("#text");
		}
		case KObject::DOUBLE: {
			return t.getDouble("#text") == this->getDouble("#text");
		}
		case KObject::STRING: {
			return *t.getString("#text") == *this->getString("#text");
		}
		case KObject::NIL: {
			return true;
		}
	}
	return false;
}

kinky::lang::KObject::trio& kinky::lang::KObject::operator[](int idx) {
	if ((this->type != KObject::ARRAY && this->type != KObject::OBJECT) || idx >= (int) this->atts->size() || idx < 0) {
		return NULL_KOBJECT;
	}
	return *(*this->atts)[idx];
}

kinky::lang::KObject::trio& kinky::lang::KObject::operator[](string name) {
	KObjAttributeNames::iterator att = this->names->find(name);
	if (att != this->names->end()) {
		return *att->second;
	}
	return NULL_KOBJECT;

}

void kinky::lang::KObject::put(string f) {
	if (this->temp != NULL) {
		this->setIt(*this->temp, KObject::STRING, new managed_ptr<string>(new string(f.data())));
		delete this->temp;
		this->temp = NULL;
	}
	else {
		if (this->type == KObject::ARRAY) {
			this->add(new managed_ptr<string>(new string(f)), KObject::STRING);
		}
		else {
			this->temp = new string(f.data());
		}
	}
}

void kinky::lang::KObject::put(const char* f) {
	if (this->temp != NULL) {
		this->setIt(*this->temp, KObject::STRING, new managed_ptr<string>(new string(f)));
		delete this->temp;
		this->temp = NULL;
	}
	else {
		if (this->type == KObject::ARRAY) {
			this->add(new managed_ptr<string>(new string(f)), KObject::STRING);
		}
		else {
			this->temp = new string(f);
		}
	}
}

void kinky::lang::KObject::put(bool value) {
	if (this->temp != NULL) {
		this->setIt(*this->temp, KObject::BOOL, new managed_ptr<bool>(new bool(value)));
		delete this->temp;
		this->temp = NULL;
	}
	else {
		if (this->type == KObject::ARRAY) {
			this->add(new managed_ptr<bool>(new bool(value)), KObject::BOOL);
		}
		else {
			ostringstream oss;
			oss << value << flush;
			this->temp = new string(oss.str());
		}
	}
}

void kinky::lang::KObject::put(int value) {
	if (this->temp != NULL) {
		this->setIt(*this->temp, KObject::INT, new managed_ptr<int>(new int(value)));
		delete this->temp;
		this->temp = NULL;
	}
	else {
		if (this->type == KObject::ARRAY) {
			this->add(new managed_ptr<int>(new int(value)), KObject::INT);
		}
		else {
			ostringstream oss;
			oss << value << flush;
			this->temp = new string(oss.str());
		}
	}
}

void kinky::lang::KObject::put(double value) {
	if (this->temp != NULL) {
		this->setIt(*this->temp, KObject::DOUBLE, new managed_ptr<double>(new double(value)));
		delete this->temp;
		this->temp = NULL;
	}
	else {
		if (this->type == KObject::ARRAY) {
			this->add(new managed_ptr<double>(new double(value)), KObject::DOUBLE);
		}
		else {
			ostringstream oss;
			oss << value << flush;
			this->temp = new string(oss.str());
		}
	}
}

void kinky::lang::KObject::put(KObject* value) {
	value->base(*this->base());
	if (this->temp != NULL) {
		this->setIt(*this->temp, value->type, new managed_ptr<KObject>(value));
		delete this->temp;
		this->temp = NULL;
	}
	else {
		if (this->type == KObject::ARRAY) {
			this->add(new managed_ptr<KObject>(value), value->type);
		}
		else {
			ostringstream oss;
			oss << *value << flush;
			this->temp = new string(oss.str());
		}
	}
}

void kinky::lang::KObject::put(KObject::KObjHeaders& value) {
	if (this->temp != NULL) {
		KObject* headers = new KObject(this->baseFormat->data());
		for (KObject::KObjHeaders::iterator it = value.begin(); it != value.end(); it++) {
			string name(it->first.data());
			kinky::utils::prettify_header_name(name);
			(*headers) << name << *it->second;
		}
		this->setIt(*this->temp, KObject::OBJECT, new managed_ptr<KObject>(headers));
		delete this->temp;
		this->temp = NULL;
	}
	else {
		if (this->type == KObject::ARRAY) {
			KObject* headers = new KObject(this->baseFormat->data());
			for (KObject::KObjHeaders::iterator it = value.begin(); it != value.end(); it++) {
				string name(it->first.data());
				kinky::utils::prettify_header_name(name);
				(*headers) << name << *it->second;
			}
			this->add(new managed_ptr<KObject>(headers), KObject::OBJECT);
		}
	}
}

void kinky::lang::KObject::stringify(ostream& oss) {
	if (*this->baseFormat == "json") {
		KObject::stringifyJSON(this, oss);
	}
	else if (*this->baseFormat == "xml") {
		if (this->size() != 1) {
			oss << "<kinky:Root xmlns:kinky=\"http://kinky/Schema\">";
		}
		string s;
		KObject::stringifyXML(this, oss, s);
		if (this->size() != 1) {
			oss << "</kinky:Root>";
		}
	}
	else if (*this->baseFormat == "html") {
		oss << "<html><head>"
		"<style>"
		"body * {font-family: \"Arial\";}li {margin: 3px;}label {font-size: 14px;font-weight: bold;}li>div {padding-left: 5px; font-size : 14px;display: inline;font-size: 14px;}"
		"</style>"
		"<script>"
		"function toggle(event){"
		"event = event || window.event;"
		"if (event == null) {"
		"return;"
		"}"
		"var element = event.target || event.srcElement;"
		"if (element.innerHTML == '+') {"
		"element.parentNode.getElementsByTagName('ul')[0].style.display = 'block';"
		"element.innerHTML = '-';"
		"}"
		"else {"
		"element.parentNode.getElementsByTagName('ul')[0].style.display = 'none';"
		"element.innerHTML = '+';"
		"}"
		"}"
		"</script>"
		"</head><body><ul>";
		KObject::stringifyHTML(this, 0, oss);
		oss << "</ul></body></html>";
	}
	else {
		KObject::stringifyJSON(this, oss);
	}
}

void kinky::lang::KObject::prettify(ostream& oss) {
	if (*this->baseFormat == "json") {
		KObject::prettifyJSON(this, 0, oss);
	}
	else if (*this->baseFormat == "xml") {
		if (this->size() != 1) {
			oss << "<kinky:Root xmlns:kinky=\"http://kinky/Schema\">" << endl;
		}
		string s;
		KObject::prettifyXML(this, 1, oss, s);
		if (this->size() != 1) {
			oss << "</kinky:Root>" << endl;
		}
	}
	else if (*this->baseFormat == "html") {
		oss << "<html><head>"
		"<style>"
		"body * {font-family: \"Arial\";}li {margin: 3px;}label {font-size: 14px;font-weight: bold;}li>div {padding-left: 5px; font-size : 14px;display: inline;font-size: 14px;}"
		"</style>"
		"<script>"
		"function toggle(event){"
		"event = event || window.event;"
		"if (event == null) {"
		"return;"
		"}"
		"var element = event.target || event.srcElement;"
		"if (element.innerHTML == '+') {"
		"element.parentNode.getElementsByTagName('ul')[0].style.display = 'block';"
		"element.innerHTML = '-';"
		"}"
		"else {"
		"element.parentNode.getElementsByTagName('ul')[0].style.display = 'none';"
		"element.innerHTML = '+';"
		"}"
		"}"
		"</script>"
		"</head><body><ul>";
		KObject::stringifyHTML(this, 0, oss);
		oss << "</ul></body></html>";
	}
	else {
		KObject::prettifyJSON(this, 0, oss);
	}
}

kinky::memory::abstract_managed_ptr* kinky::lang::KObject::getIt(int idx, int type) throw () {
	if ((this->type != KObject::ARRAY && this->type != KObject::OBJECT) || idx >= (int) this->atts->size() || idx < 0) {
		throw new KObjectNULLException();
	}
	if (type == -1 || (*this->atts)[idx]->type == type) {
		return (*this->atts)[idx]->value;
	}
	throw new KObjectNULLException();
}

kinky::memory::abstract_managed_ptr* kinky::lang::KObject::getIt(string name, int type) {
	string nameStr(name);
	KObjAttributeNames::iterator att = this->names->find(nameStr);
	if (att != this->names->end()) {
		trio* t = att->second;
		if (type == -1 || t->type == type) {
			return t->value;
		}
	}
	throw new KObjectNULLException();

}

int kinky::lang::KObject::insertIt(int idx, int type, kinky::memory::abstract_managed_ptr* value) {
	if ((this->type != KObject::ARRAY && this->type != KObject::OBJECT && this->type != type) || idx < 0) {
		return -1;
	}

	if (idx < 0 || idx > (int) this->atts->size()) {
		return -1;
	}

	if (idx >= (int) this->atts->size()) {
		return this->setIt(idx, type, value);
	}
	else {
		ostringstream oss;
		int init_pos = (int) this->atts->size() - 1;
		for (int it = init_pos; it >= idx; --it) {
			trio* val = (*this->atts)[it];

			oss << (++val->pos) << flush;

			if(it == init_pos) {
				this->atts->push_back(val);
				this->names->insert(pair<string, trio*>(oss.str(), val));
			}
			else {
				(*this->atts)[val->pos] = val;
				(*this->names)[oss.str()] = val;
			}

			oss.str("");
		}

		oss << idx << flush;
		trio* val = new trio(type, oss.str(), value);
		val->pos = idx;
		(*this->atts)[val->pos] = val;
		(*this->names)[oss.str()] = val;

		return idx;
	}
}

int kinky::lang::KObject::setIt(int idx, int type, kinky::memory::abstract_managed_ptr* value) {
	if ((this->type != KObject::ARRAY && this->type != KObject::OBJECT && this->type != type) || idx < 0) {
		return -1;
	}

	ostringstream oss;
	if (idx >= (int) this->atts->size()) {
		int pos = this->atts->size();
		oss << pos << flush;
		trio* val = new trio(type, oss.str(), value);
		val->pos = pos;
		this->atts->push_back(val);
		this->names->insert(pair<string, trio*>(oss.str(), val));
		oss.str("");
		return pos;
	}

	if (idx > 0) {
		(*this->atts)[idx]->value->release();
		(*this->atts)[idx]->value = value;
		(*this->atts)[idx]->type = type;
		return idx;
	}
	return -1;
}

int kinky::lang::KObject::setIt(string name, int type, kinky::memory::abstract_managed_ptr* value) {
	if (this->type != KObject::ARRAY && this->type != KObject::OBJECT && this->type != type) {
		return -1;
	}

	KObjAttributeNames::iterator att = this->names->find(name);
	if (att != this->names->end()) {
		att->second->value->release();
		att->second->value = value;
		att->second->type = type;
		return att->second->pos;
	}

	int pos = this->atts->size();
	trio* val = new trio(type, name.data(), value);
	val->pos = pos;
	this->atts->push_back(val);
	this->names->insert(pair<string, trio*>(name, val));
	return pos;
}

void kinky::lang::KObject::deleteIt(int idx) {
	if ((this->type != KObject::ARRAY && this->type != KObject::OBJECT) || idx < 0) {
		return;
	}

	if (idx < 0 || idx >= (int) this->atts->size()) {
		return;
	}

	ostringstream oss;
	trio* trio = (*this->atts)[idx];
	delete trio;

	oss << idx << flush;
	this->atts->erase(this->atts->begin() + idx);
	string key = oss.str();
	this->names->erase(this->names->find(key));

	for (KObjAttributeNames::iterator it = this->names->begin(); it != this->names->end(); it++) {
		if (it->second->pos > idx) {
			it->second->pos--;
		}
	}

}

void kinky::lang::KObject::deleteIt(string name) {
	if (this->type != KObject::ARRAY && this->type != KObject::OBJECT) {
		return;
	}

	KObjAttributeNames::iterator att = this->names->find(name);
	if (att != this->names->end()) {
		trio* trio = att->second;

		this->atts->erase(this->atts->begin() + trio->pos);
		this->names->erase(att);

		int idx = trio->pos;
		for (KObjAttributeNames::iterator it = this->names->begin(); it != this->names->end(); it++) {
			if (it->second->pos > idx) {
				it->second->pos--;
			}
		}

		delete trio;
	}
}

void kinky::lang::KObject::stringifyJSON(KObject* obj, ostream& oss) {
	switch (obj->type) {
		case KObject::OBJECT: {
			bool first = true;
			oss << "{";
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				void* val = (*it)->value->pointed();
				int type = (*it)->type;
				string key = (*it)->key;

				if (!first) {
					oss << ", ";
				}
				first = false;
				switch (type) {
					case KObject::OBJECT: {
						oss << "\"" << key << "\" : ";
						KObject::stringifyJSON(static_cast<KObject*>(val), oss);
						break;
					}
					case KObject::ARRAY: {
						oss << "\"" << key << "\" : ";
						KObject::stringifyJSON(static_cast<KObject*>(val), oss);
						break;
					}
					case KObject::BOOL: {
						oss << "\"" << key << "\" : " << (*static_cast<bool*>(val) ? "true" : "false");
						break;
					}
					case KObject::INT: {
						oss << "\"" << key << "\" : " << *static_cast<int*>(val);
						break;
					}
					case KObject::DOUBLE: {
						oss << "\"" << key << "\" : " << *static_cast<double*>(val);
						break;
					}
					case KObject::STRING: {
						oss << "\"" << key << "\" : \"";
						kinky::utils::utf8_encode(*static_cast<string*>(val), oss);
						oss << "\"";
						break;
					}
					case KObject::NIL: {
						oss << "\"" << key << "\" : null ";
						break;
					}
				}
			}
			oss << "}";
			break;
		}
		case KObject::ARRAY: {
			bool first = true;
			oss << "[";
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				void* val = (*it)->value->pointed();
				int type = (*it)->type;
				string key = (*it)->key;

				if (!first) {
					oss << ", ";
				}
				first = false;
				switch (type) {
					case KObject::OBJECT: {
						KObject::stringifyJSON(static_cast<KObject*>(val), oss);
						break;
					}
					case KObject::ARRAY: {
						KObject::stringifyJSON(static_cast<KObject*>(val), oss);
						break;
					}
					case KObject::BOOL: {
						oss << (*static_cast<bool*>(val) ? "true" : "false");
						break;
					}
					case KObject::INT: {
						oss << *static_cast<int*>(val);
						break;
					}
					case KObject::DOUBLE: {
						oss << *static_cast<double*>(val);
						break;
					}
					case KObject::STRING: {
						oss << "\"";
						kinky::utils::utf8_encode(*static_cast<string*>(val), oss);
						oss << "\"";
						break;
					}
					case KObject::NIL: {
						oss << "null";
						break;
					}
				}
			}
			oss << "]";
			break;
		}
		case KObject::BOOL: {
			oss << (*obj->getBoolean("#text") ? "true" : "false");
			break;
		}
		case KObject::INT: {
			oss << *obj->getInteger("#text");
			break;
		}
		case KObject::DOUBLE: {
			oss << *obj->getDouble("#text");
			break;
		}
		case KObject::STRING: {
			oss << "\"";
			kinky::utils::utf8_encode(*obj->getString("#text"), oss);
			oss << "\"";
			break;
		}
		case KObject::NIL: {
			oss << "null";
			break;
		}
	}

	oss << flush;
}

void kinky::lang::KObject::prettifyJSON(KObject* obj, int tabLevel, ostream& oss) {
	switch (obj->type) {
		case KObject::OBJECT: {
			bool first = true;
			oss << "{" << endl;
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				void* val = (*it)->value->pointed();
				int type = (*it)->type;
				string key = (*it)->key;

				if (!first) {
					oss << "," << endl;
				}
				first = false;
				switch (type) {
					case KObject::OBJECT: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "\"" << key << "\" : ";
						KObject::prettifyJSON(static_cast<KObject*>(val), tabLevel + 1, oss);
						break;
					}
					case KObject::ARRAY: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "\"" << key << "\" : ";
						KObject::prettifyJSON(static_cast<KObject*>(val), tabLevel + 1, oss);
						break;
					}
					case KObject::BOOL: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "\"" << key << "\" : " << (*static_cast<bool*>(val) ? "true" : "false");
						break;
					}
					case KObject::INT: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "\"" << key << "\" : " << *static_cast<int*>(val);
						break;
					}
					case KObject::DOUBLE: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "\"" << key << "\" : " << *static_cast<double*>(val);
						break;
					}
					case KObject::STRING: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "\"" << key << "\" : \"";
						kinky::utils::utf8_encode(*static_cast<string*>(val), oss);
						oss << "\"";
						break;
					}
					case KObject::NIL: {
						oss << "\"" << key << "\" : null ";
						break;
					}
				}
			}
			oss << endl;
			KObject::tabbify(tabLevel, oss);
			oss << "}";
			break;
		}
		case KObject::ARRAY: {
			bool first = true;
			oss << "[" << endl;
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				void* val = (*it)->value->pointed();
				int type = (*it)->type;
				string key = (*it)->key;

				if (!first) {
					oss << "," << endl;
				}
				first = false;
				switch (type) {
					case KObject::OBJECT: {
						KObject::tabbify(tabLevel + 1, oss);
						KObject::prettifyJSON(static_cast<KObject*>(val), tabLevel + 1, oss);
						break;
					}
					case KObject::ARRAY: {
						KObject::tabbify(tabLevel + 1, oss);
						KObject::prettifyJSON(static_cast<KObject*>(val), tabLevel + 1, oss);
						break;
					}
					case KObject::BOOL: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << (*static_cast<bool*>(val) ? "true" : "false");
						break;
					}
					case KObject::INT: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << *static_cast<int*>(val);
						break;
					}
					case KObject::DOUBLE: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << *static_cast<double*>(val);
						break;
					}
					case KObject::STRING: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "\"";
						kinky::utils::utf8_encode(*static_cast<string*>(val), oss);
						oss << "\"";
						break;
					}
					case KObject::NIL: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "null";
						break;
					}
				}
			}
			oss << endl;
			KObject::tabbify(tabLevel, oss);
			oss << "]";
			break;
		}
		case KObject::BOOL: {
			KObject::tabbify(tabLevel, oss);
			oss << (*obj->getBoolean("#text") ? "true" : "false");
			break;
		}
		case KObject::INT: {
			KObject::tabbify(tabLevel, oss);
			oss << *obj->getInteger("#text");
			break;
		}
		case KObject::DOUBLE: {
			KObject::tabbify(tabLevel, oss);
			oss << *obj->getDouble("#text");
			break;
		}
		case KObject::STRING: {
			KObject::tabbify(tabLevel, oss);
			oss << "\"";
			kinky::utils::utf8_encode(*(obj->getString("#text")), oss);
			oss << "\"";
			break;
		}
		case KObject::NIL: {
			KObject::tabbify(tabLevel, oss);
			oss << "null";
			break;
		}
	}

	oss << flush;
}

void kinky::lang::KObject::tabbify(int tabs, ostream& oss) {
	for (int i = 0; i != tabs && tabs > 0; i++) {
		oss << "\t";
	}
}

void kinky::lang::KObject::stringifyXML(kinky::lang::KObject* obj, ostream& oss, string& attName) {
	switch (obj->type) {
		case KObject::OBJECT: {
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				void* val = (*it)->value->pointed();
				int type = (*it)->type;
				string key = (*it)->key;

				switch (type) {
					case KObject::OBJECT: {
						oss << "<" << key << ">";
						KObject::stringifyXML(static_cast<KObject*>(val), oss, key);
						oss << "</" << key << ">";
						break;
					}
					case KObject::ARRAY: {
						KObject::stringifyXML(static_cast<KObject*>(val), oss, key);
						break;
					}
					case KObject::BOOL: {
						oss << "<" << key << ">";
						oss << (*static_cast<bool*>(val) ? "true" : "false");
						oss << "</" << key << ">";
						break;
					}
					case KObject::INT: {
						oss << "<" << key << ">";
						oss << *static_cast<int*>(val);
						oss << "</" << key << ">";
						break;
					}
					case KObject::DOUBLE: {
						oss << "<" << key << ">";
						oss << *static_cast<double*>(val);
						oss << "</" << key << ">";
						break;
					}
					case KObject::STRING: {
						oss << "<" << key << ">";
						kinky::utils::html_entities_encode(*static_cast<string*>(val), oss);
						oss << "</" << key << ">";
						break;
					}
					case KObject::NIL: {
						oss << "<" << key << ">";
						oss << "null";
						oss << "</" << key << ">";
						break;
					}
				}
			}
			break;
		}
		case KObject::ARRAY: {
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				void* val = (*it)->value->pointed();
				int type = (*it)->type;
				string key = (*it)->key;

				oss << "<" << attName << ">";
				switch (type) {
					case KObject::OBJECT: {
						KObject::stringifyXML(static_cast<KObject*>(val), oss, key);
						break;
					}
					case KObject::ARRAY: {
						KObject::stringifyXML(static_cast<KObject*>(val), oss, key);
						break;
					}
					case KObject::BOOL: {
						oss << (*static_cast<bool*>(val) ? "true" : "false");
						break;
					}
					case KObject::INT: {
						oss << *static_cast<int*>(val);
						break;
					}
					case KObject::DOUBLE: {
						oss << *static_cast<double*>(val);
						break;
					}
					case KObject::STRING: {
						kinky::utils::html_entities_encode(*static_cast<string*>(val), oss);
						break;
					}
					case KObject::NIL: {
						oss << "null";
						break;
					}
				}
				oss << "</" << attName << ">";
			}
			break;
		}
		case KObject::BOOL: {
			oss << "<kinky:base xmlns:kinky=\"http://kinky/Schema\">";
			oss << (*obj->getBoolean("#text") ? "true" : "false");
			oss << "</kinky:base>";
			break;
		}
		case KObject::INT: {
			oss << "<kinky:base xmlns:kinky=\"http://kinky/Schema\">";
			oss << *obj->getInteger("#text");
			oss << "</kinky:base>";
			break;
		}
		case KObject::DOUBLE: {
			oss << "<kinky:base xmlns:kinky=\"http://kinky/Schema\">";
			oss << *obj->getDouble("#text");
			oss << "</kinky:base>";
			break;
		}
		case KObject::STRING: {
			if (obj->getString("#text") == NULL || obj->getString("#text")->length() == 0) {
				break;
			}
			oss << "<kinky:base xmlns:kinky=\"http://kinky/Schema\">";
			kinky::utils::html_entities_encode(*obj->getString("#text"), oss);
			oss << "</kinky:base>";
			break;
		}
		case KObject::NIL: {
			oss << "<kinky:base xmlns:kinky=\"http://kinky/Schema\">";
			oss << "null";
			oss << "</kinky:base>";
			break;
		}
	}

	oss << flush;
}

void kinky::lang::KObject::prettifyXML(KObject* obj, int tabLevel, ostream& oss, string& attName) {
	switch (obj->type) {
		case KObject::OBJECT: {
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				void* val = (*it)->value->pointed();
				int type = (*it)->type;
				string key = (*it)->key;

				switch (type) {
					case KObject::OBJECT: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "<" << key << ">";
						oss << endl;
						KObject::prettifyXML(static_cast<KObject*>(val), tabLevel + 1, oss, key);
						KObject::tabbify(tabLevel + 1, oss);
						oss << "</" << key << ">";
						break;
					}
					case KObject::ARRAY: {
						KObject::prettifyXML(static_cast<KObject*>(val), tabLevel + 1, oss, key);
						break;
					}
					case KObject::BOOL: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "<" << key << ">";
						oss << (*static_cast<bool*>(val) ? "true" : "false");
						oss << "</" << key << ">";
						break;
					}
					case KObject::INT: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "<" << key << ">";
						oss << *static_cast<int*>(val);
						oss << "</" << key << ">";
						break;
					}
					case KObject::DOUBLE: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "<" << key << ">";
						oss << *static_cast<double*>(val);
						oss << "</" << key << ">";
						break;
					}
					case KObject::STRING: {
						KObject::tabbify(tabLevel + 1, oss);
						oss << "<" << key << ">";
						kinky::utils::html_entities_encode(*static_cast<string*>(val), oss);
						oss << "</" << key << ">";
						break;
					}
					case KObject::NIL: {
						oss << "<" << key << ">";
						oss << "null ";
						oss << "</" << key << ">";
						break;
					}
				}
				oss << endl;
			}
			break;
		}
		case KObject::ARRAY: {
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				void* val = (*it)->value->pointed();
				int type = (*it)->type;
				string key = (*it)->key;

				KObject::tabbify(tabLevel, oss);
				oss << "<" << attName.data() << ">";
				switch (type) {
					case KObject::OBJECT: {
						oss << endl;
						KObject::prettifyXML(static_cast<KObject*>(val), tabLevel + 1, oss, key);
						KObject::tabbify(tabLevel, oss);
						break;
					}
					case KObject::ARRAY: {
						oss << endl;
						KObject::prettifyXML(static_cast<KObject*>(val), tabLevel + 1, oss, key);
						KObject::tabbify(tabLevel, oss);
						break;
					}
					case KObject::BOOL: {
						oss << (*static_cast<bool*>(val) ? "true" : "false");
						break;
					}
					case KObject::INT: {
						oss << *static_cast<int*>(val);
						break;
					}
					case KObject::DOUBLE: {
						oss << *static_cast<double*>(val);
						break;
					}
					case KObject::STRING: {
						oss << "\"";
						kinky::utils::html_entities_encode(*static_cast<string*>(val), oss);
						oss << "\"";
						break;
					}
					case KObject::NIL: {
						oss << "null";
						break;
					}
				}
				oss << "</" << attName.data() << ">";
				oss << endl;
			}
			break;
		}
		case KObject::BOOL: {
			KObject::tabbify(tabLevel, oss);
			oss << "<kinky:base xmlns:kinky=\"http://kinky/Schema\">";
			oss << (*obj->getBoolean("#text") ? "true" : "false");
			oss << "</kinky:base>";
			oss << endl;
			break;
		}
		case KObject::INT: {
			KObject::tabbify(tabLevel, oss);
			oss << "<kinky:base xmlns:kinky=\"http://kinky/Schema\">";
			oss << *obj->getInteger("#text");
			oss << "</kinky:base>";
			oss << endl;
			break;
		}
		case KObject::DOUBLE: {
			KObject::tabbify(tabLevel, oss);
			oss << "<kinky:base xmlns:kinky=\"http://kinky/Schema\">";
			oss << *obj->getDouble("#text");
			oss << "</kinky:base>";
			oss << endl;
			break;
		}
		case KObject::STRING: {
			if (obj->getString("#text") == NULL || obj->getString("#text")->length() == 0) {
				break;
			}
			KObject::tabbify(tabLevel, oss);
			oss << "<kinky:base xmlns:kinky=\"http://kinky/Schema\">";
			kinky::utils::html_entities_encode(*(obj->getString("#text")), oss);
			oss << "</kinky:base>";
			oss << endl;
			break;
		}
		case KObject::NIL: {
			KObject::tabbify(tabLevel, oss);
			oss << "<kinky:base xmlns:kinky=\"http://kinky/Schema\">";
			oss << "null";
			oss << "</kinky:base>";
			break;
		}
	}

	oss << flush;
}

void kinky::lang::KObject::stringifyHTML(KObject* obj, int tabLevel, ostream& oss) {
	switch (obj->type) {
		case KObject::OBJECT: {
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				void* val = (*it)->value->pointed();
				int type = (*it)->type;
				string key = (*it)->key;

				oss << "<li><label>";
				oss << key << ": ";
				oss << "</label>";
				switch (type) {
					case KObject::OBJECT: {
						oss << "<a href=\"javascript:void(0)\" onclick=\"javascript:toggle(event)\">-</a><ul style=\"display: block;\">";
						KObject::stringifyHTML(static_cast<KObject*>(val), tabLevel + 1, oss);
						oss << "</ul>";
						break;
					}
					case KObject::ARRAY: {
						oss << "<a href=\"javascript:void(0)\" onclick=\"javascript:toggle(event)\">-</a><ul style=\"display: block;\">";
						KObject::stringifyHTML(static_cast<KObject*>(val), tabLevel + 1, oss);
						oss << "</ul>";
						break;
					}
					case KObject::BOOL: {
						oss << "<div>";
						oss << (*static_cast<bool*>(val) ? "true" : "false");
						oss << "</div>";
						break;
					}
					case KObject::INT: {
						oss << "<div>";
						oss << *static_cast<int*>(val);
						oss << "</div>";
						break;
					}
					case KObject::DOUBLE: {
						oss << "<div>";
						oss << *static_cast<double*>(val);
						oss << "</div>";
						break;
					}
					case KObject::STRING: {
						oss << "<div>";
						string value = *static_cast<string*>(val);
						if (key == "href" || key == "rel") {
							oss << "<a href=\"";
							//kinky::utils::url_encode(value, oss);
							oss << value;
							oss << "\">";
							kinky::utils::html_entities_encode(value, oss);
							oss << "</a>";
						}
						else if (value.find("/wrml-schemas/") != string::npos) {
							kinky::utils::replaceAll(value, "\"", "");
							oss << "<a href=\"" << value.substr(value.find("/wrml-schemas")) << "\">";
							kinky::utils::html_entities_encode(value, oss);
							oss << "</a>";
						}
						else {
							kinky::utils::html_entities_encode(*static_cast<string*>(val), oss);
						}
						oss << "</div>";
						break;
					}
					case KObject::NIL: {
						oss << "<div>";
						oss << "null";
						oss << "</div>";
						break;
					}
				}
				oss << "</li>";
			}
			break;
		}
		case KObject::ARRAY: {
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				void* val = (*it)->value->pointed();
				int type = (*it)->type;
				string key = (*it)->key;

				oss << "<li><label>";
				oss << key << ": ";
				oss << "</label>";
				switch (type) {
					case KObject::OBJECT: {
						oss << "<a href=\"javascript:void(0)\" onclick=\"javascript:toggle(event)\">-</a><ul style=\"display: block;\">";
						KObject::stringifyHTML(static_cast<KObject*>(val), tabLevel, oss);
						oss << "</ul>";
						break;
					}
					case KObject::ARRAY: {
						oss << "<a href=\"javascript:void(0)\" onclick=\"javascript:toggle(event)\">-</a><ul style=\"display: block;\">";
						KObject::stringifyHTML(static_cast<KObject*>(val), tabLevel, oss);
						oss << "</ul>";
						break;
					}
					case KObject::BOOL: {
						oss << "<div>";
						oss << (*static_cast<bool*>(val) ? "true" : "false");
						oss << "</div>";
						break;
					}
					case KObject::INT: {
						oss << "<div>";
						oss << *static_cast<int*>(val);
						oss << "</div>";
						break;
					}
					case KObject::DOUBLE: {
						oss << "<div>";
						oss << *static_cast<double*>(val);
						oss << "</div>";
						break;
					}
					case KObject::STRING: {
						oss << "<div>";
						string value = *static_cast<string*>(val);
						if (value.find("/wrml-schemas/") != string::npos) {
							kinky::utils::replaceAll(value, "\"", "");
							oss << "<a href=\"" << value.substr(value.find("/wrml-schemas")) << "\">";
							kinky::utils::html_entities_encode(value, oss);
							oss << "</a>";
						}
						else {
							kinky::utils::html_entities_encode(*static_cast<string*>(val), oss);
						}
						oss << "</div>";
						break;
					}
					case KObject::NIL: {
						oss << "<div>";
						oss << "null";
						oss << "</div>";
						break;
					}
				}
				oss << "</li>";
			}
			break;
		}
		case KObject::BOOL: {
			oss << "<li><div>";
			oss << (*obj->getBoolean("#text") ? "true" : "false");
			oss << "</div></li>";
			break;
		}
		case KObject::INT: {
			oss << "<li><div>";
			oss << *obj->getInteger("#text");
			oss << "</div></li>";
			break;
		}
		case KObject::DOUBLE: {
			oss << "<li><div>";
			oss << *obj->getDouble("#text");
			oss << "</div></li>";
			break;
		}
		case KObject::STRING: {
			oss << "<li><div>";
			kinky::utils::html_entities_encode(*obj->getString("#text"), oss);
			oss << "</div></li>";
			break;
		}
		case KObject::NIL: {
			oss << "<li><div>";
			oss << "null";
			oss << "</div></li>";
			break;
		}
	}

	oss << flush;
}

//
//
// <<<
//
pthread_mutex_t** sslmutex;
//
void kinky::initSSLMutex() {
	sslmutex = new pthread_mutex_t*[CRYPTO_num_locks()];

	for (int n = 0; n != CRYPTO_num_locks(); n++) {
		sslmutex[n] = new pthread_mutex_t();
		pthread_mutex_init(sslmutex[n], NULL);
	}
}

void kinky::deinitSSLMutex() {
	for (int n = 0; n != CRYPTO_num_locks(); n++) {
		pthread_mutex_destroy(sslmutex[n]);
		delete sslmutex[n];
	}
	delete sslmutex;
}

//
void kinky::locking_function(int mode, int n, const char *file, int line) {
	cout << "SSL lock/unlock " << mode << " " << file << endl << flush;
	if (mode & CRYPTO_LOCK) {
		pthread_mutex_lock(sslmutex[n]);
	}
	else {
		pthread_mutex_unlock(sslmutex[n]);
	}
}

unsigned long kinky::id_function(void) {
	return ((unsigned long) pthread_self());
}
#endif
