#pragma once

#include <gd.h>
#include <string>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	namespace lang {

		class KRestArg {
			public:
				KRestArg();
				virtual ~KRestArg();

				string* get();

				string* getOrder();

				int getPageSize();

				int getPageStartIndex();

				void set(string b);

				void setOrder(string b);

				void setPageSize(int ps);

				void setPageStartIndex(int psi);

				bool hasParts();

				size_t size();

				void add(kinky::lang::KRestArg* a);

				void getRESTfulURI(ostream& uri);

				KRestArg* at(size_t i);

				void stringify(string& arg, string tab = "");

				static KRestArg* parse(string arg);

			private:
				string* base;
				string* order;
				vector<KRestArg*>* parts;
				int pageSize;
				int pageStartIndex;
				string* query;
		};

	}
}
