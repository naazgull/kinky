#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <memory.h>
#include <kinky/lang/KObject.h>
#include <kinky/lang/KParseException.h>

#ifdef KINKY_JSON_INTERNAL
#include <kinky/lang/json.h>
#endif

#ifdef KINKY_JSON_C
#include <json/json.h>
#include <json/debug.h>
#define json_object_array_foreach(__obj, __val) \
struct json_object *__val = NULL; array_list* __jarr = json_object_get_array(__obj); \
for(int __i = 0, __j = __jarr->length; ({ if (__i != __jarr->length) { __val = json_object_array_get_idx(__obj, __i); } ; __j; }); __i++, __j--)

#endif

#ifdef KINKY_JSON_MONGO
#include <mongo/client/dbclient.h>
#endif

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	namespace lang {

		class KJSON {
			public:
				static KObject* parse(std::ifstream& in);
				static KObject* parse(string* jsonStr);
				static KObject* parse(string& jsonStr);

#ifdef KINKY_JSON_MONGO
				static void convert(bson::bo& obj, KObject* kobj);
#endif
#ifdef KINKY_JSON_INTERNAL
#endif

			private:
#ifdef KINKY_JSON_INTERNAL
				static void convert(json_value* obj, KObject* kobj);
#endif

#ifdef KINKY_JSON_C
				static void convert(struct json_object* obj, KObject* kobj);
#endif

#ifdef KINKY_JSON_MONGO
				static void convert(bson::be* obj, KObject* kobj);
#endif

		};
	}
}
