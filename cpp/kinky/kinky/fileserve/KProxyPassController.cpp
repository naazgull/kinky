#include <kinky/fileserve/KProxyPassController.h>

#include <kinky/rest/KRestAPI.h>
#include <kinky/rest/http/KCurl.h>
#include <magic.h>

kinky::fileserve::KProxyPassController::KProxyPassController() {
}

kinky::fileserve::KProxyPassController::~KProxyPassController() {
}

bool kinky::fileserve::KProxyPassController::pass(KHTTPResponse* reply, string* url, int directive, KHTTPRequest* request) {
	if(this->getAPI()->getConfiguration()->get("krest_proxypass_mode") == "on") {

		size_t protosep = url->find("/", 1);
		string protocol = url->substr(1, protosep - 1);

		size_t serversep = url->find("/", protosep + 1);
		string server = url->substr(protosep + 1, serversep - protosep - 1);
		server.insert(0, "://");
		server.insert(0, protocol);
		string uri = url->substr(serversep);

		ostringstream uriss;
		KHTTPRequest::KHTTPParams* params = request->getParams();
		if (params != NULL) {
			bool first = true;
			for (KHTTPRequest::KHTTPParams::iterator i = params->begin(); i != params->end(); i++) {
				if (!first) {
					uriss << "&";
				}
				first = false;
				uriss << (*i)->first << "=" << *((string*) (*i)->second->pointed()) << flush;
			}
			if (!first) {
				uri.append("?");
				uri.append(uriss.str());
			}
		}
		if (request->getHeader("Accept-Encoding") != NULL) {
			request->removeHeader("Accept-Encoding");
		}
		int responseCode = 0;
		string* replyStr = kinky::rest::http::curl(&responseCode, directive, server, uri, request->getBody(), request->getHeaders(), reply->getHeaders(), false);

		if (reply->getHeader("Transfer-Encoding") != NULL) {
			reply->removeHeader("Transfer-Encoding");
		}
		if (reply->getHeader("Content-Encoding") != NULL) {
			reply->removeHeader("Content-Encoding");
		}

		reply->setCode(responseCode);
		if (replyStr != NULL) {
			reply->setBody(replyStr);
		}

		return true;
	}
	return false;
}

void kinky::fileserve::KProxyPassController::setAPI(kinky::rest::KRestAPI* api) {
	this->api = api;
}

kinky::rest::KRestAPI* kinky::fileserve::KProxyPassController::getAPI() {
	return this->api;
}

