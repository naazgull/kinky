#include <kinky/fileserve/KFileServeController.h>

#include <kinky/rest/KRestAPI.h>
#include <magic.h>

kinky::fileserve::KFileServeController::KFileServeController(string documentRoot, string documentIndex) : docRoot(new string(documentRoot.data())), docIndex(new string(documentIndex.data())), fileServePath(new vector<string*>()) {
	stringstream iss;
	iss << *this->docRoot;

	string line;
	while (iss.good()) {
		std::getline(iss, line, ',');
		string* path = new string(line.data());
		this->fileServePath->push_back(path);
	}

}

kinky::fileserve::KFileServeController::~KFileServeController() {
	delete this->docRoot;
	delete this->docIndex;

	if (this->fileServePath != NULL) {
		for (vector<string*>::iterator i = this->fileServePath->begin(); i != this->fileServePath->end(); i++) {
			delete *i;
		}
		delete this->fileServePath;
	}
}

bool kinky::fileserve::KFileServeController::searchFile(KHTTPResponse* reply, string* url, int directive, KToken* token) {
	if(this->getAPI()->getConfiguration()->get("krest_fileserve_mode") == "on") {
		managed_ptr<string>* filePtr = this->fileFor(url, token);
		if (filePtr != NULL) {
			ifstream file(filePtr->get()->data());
			file.seekg(0, ios::end);
			size_t size = file.tellg();
			file.seekg(0, ios::beg);

			char buffer[size];
			file.read(buffer, size);

			reply->setCode(KHTTPResponse::OK);

			if (url->find(".css") == url->length() - 4) {
				reply->setHeader("Content-Type", "text/css");
			}
			else if (url->find(".js") == url->length() - 3) {
				reply->setHeader("Content-Type", "text/javascript");
			}
			else {
				magic_t myt = magic_open(MAGIC_CONTINUE | MAGIC_ERROR | MAGIC_MIME);
				magic_load(myt, NULL);
				reply->setHeader("Content-Type", magic_file(myt, filePtr->get()->data()));
				magic_close(myt);
			}
			file.close();
			filePtr->release();
			reply->setBody(new string(buffer, size));
			return true;
		}
	}
	return false;
}

string kinky::fileserve::KFileServeController::search(string *url) {
	return this->search(*url);
}

string kinky::fileserve::KFileServeController::search(string url) {
	for (vector<string*>::iterator i = this->fileServePath->begin(); i != this->fileServePath->end(); i++) {
		ostringstream oss;
		oss << (*i)->data() << url << (url == "/" ? this->getAPI()->getConfiguration()->get("krest_directory_index") : "") << flush;
		ifstream* ret = new ifstream(oss.str().data());
		if (ret->is_open()) {
			ret->close();
			delete ret;
			string* f = new string(oss.str());
			(new managed_ptr<string>(f));
			return *f;
		}
		ret->close();
		delete ret;
	}

	string* f = new string("");
	(new managed_ptr<string>(f));
	return *f;
}

kinky::memory::managed_ptr<string>* kinky::fileserve::KFileServeController::fileFor(string *url, KToken* token) {
	for (vector<string*>::iterator i = this->fileServePath->begin(); i != this->fileServePath->end(); i++) {
		ostringstream oss;
		oss << (*i)->data() << *url << (*url == "/" ? this->getAPI()->getConfiguration()->get("krest_directory_index") : "") << flush;
		ifstream* ret = new ifstream(oss.str().data());
		if (ret->is_open()) {
			ret->close();
			delete ret;
			return new managed_ptr<string>(new string(oss.str()));
		}
		ret->close();
		delete ret;
	}

	return NULL;
}

vector<string*>* kinky::fileserve::KFileServeController::getFileServeDir() {
	return this->fileServePath;
}

void kinky::fileserve::KFileServeController::setAPI(kinky::rest::KRestAPI* api) {
	this->api = api;
}

kinky::rest::KRestAPI* kinky::fileserve::KFileServeController::getAPI() {
	return this->api;
}

