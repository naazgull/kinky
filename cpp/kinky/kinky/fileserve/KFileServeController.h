#pragma once

#include <time.h>
#include <string>
#include <iostream>
#include <kinky/lang/KObject.h>
#include <kinky/rest/KRestOperation.h>
#include <kinky/oauth/KToken.h>
#include <kinky/utils/SHA1.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest;
	using namespace oauth;

	namespace rest {
		class KRestAPI;
	}

	namespace fileserve {

		class  KFileServeController {
			public:
				 KFileServeController(string documentRoot, string documentIndex);
				 virtual ~KFileServeController();

				 virtual bool searchFile(KHTTPResponse* op, string* url, int directive, KToken* token = NULL);

				 string search(string url);

				 string search(string *url);

				 virtual kinky::memory::managed_ptr<string>* fileFor(string *url, KToken* token = NULL);

				 virtual vector<string*>* getFileServeDir();

				 void setAPI(kinky::rest::KRestAPI* api);

				 kinky::rest::KRestAPI* getAPI();

			private:
				 kinky::rest::KRestAPI* api;
				 string* docRoot;
				 string* docIndex;
				vector<string*>* fileServePath;

		};
	}
}
