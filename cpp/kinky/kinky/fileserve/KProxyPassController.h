#pragma once

#include <time.h>
#include <string>
#include <iostream>
#include <kinky/lang/KObject.h>
#include <kinky/rest/KRestOperation.h>
#include <kinky/oauth/KToken.h>
#include <kinky/utils/SHA1.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest;
	using namespace oauth;

	namespace rest {
		class KRestAPI;
	}

	namespace fileserve {

		class  KProxyPassController  {
			public:
				 KProxyPassController();
				 virtual ~KProxyPassController();

				 virtual bool pass(kinky::http::KHTTPResponse* op, string* url, int directive, kinky::http::KHTTPRequest* request);

				 void setAPI(kinky::rest::KRestAPI* api);

				 kinky::rest::KRestAPI* getAPI();

			private:
				 kinky::rest::KRestAPI* api;

		};
	}
}
