#include <kinky/media/controllers/KMailSelf.h>

kinky::media::controllers::KMailSelf::KMailSelf() :
		KRestOperation("^/mail/send$", KRestOperation::CONTROLLER) {

	this->setPermissions(KOBJECT(
		"POST" << KOBJECT_ARRAY(
			"admin" <<
			"manage_mail"
		)
	));
}

kinky::media::controllers::KMailSelf::~KMailSelf() {
}

kinky::lang::KObject* kinky::media::controllers::KMailSelf::process(KObject* request, int directive, string* url, KToken* token) {
	if (directive != KHTTPRequest::POST) {
		throw new KRestException(KHTTPResponse::METHOD_NOT_ALLOWED, KEXCEPTION_INVALID_METHOD);
	}

	if (!this->hasPermission(*url, directive, token)) {
		throw new KRestException(KHTTPResponse::UNAUTHORIZED, KEXCEPTION_NOT_ENOUGH_SCOPES);
	}

	if(!(*request)["from"] || !(*request)["subject"] || !(*request)["body"] || !(*request)["recipients"] || (*request)["recipients"].type != KObject::ARRAY || ((KObject*) (*request)["recipients"])->size() == 0) {
		throw new KRestException(KHTTPResponse::PRECONDITION_FAILED, KEXCEPTION_NOT_ENOUGH_PARAMETERS, KOBJECT(
			"required_fields" << KOBJECT_ARRAY(
				"from" <<
				"subject" <<
				"body" <<
				"subject" <<
				"recipients"
			)
		));
	}

	KProperties* config = this->getConfiguration();

	if (!(*config)["smtp_server"]) {
		throw new KRestException(KHTTPResponse::INTERNAL_SERVER_ERROR, KEXCEPTION_NOT_ENOUGH_PARAMETERS, KOBJECT(
			"required_configuration" << KOBJECT_ARRAY(
				"smtp_server"
			)
		));
	}

	if (!(*request)["smtp_server"]) {
		(*request) << "smtp_server" << (string) (*config)["smtp_server"];
	}

	if (!!(*config)["smtp_user"] && !!(*config)["smtp_password"]) {
		if (!(*request)["smtp_user"]) {
			(*request) << "smtp_user" << (string) (*config)["smtp_user"];
		}
		if (!(*request)["smtp_password"]) {
			(*request) << "smtp_password" << (string) (*config)["smtp_password"];
		}
		if (!(*request)["reply_to"]) {
			(*request) << "reply_to" << (string) (*request)["from"];
		}
		if (!!(*request)["from_name"]) {
			(*request) << "reply_to_name" << (string) (*request)["from_name"];
			request->unset("from_name");
		}
		if (!(*request)["from"]) {
			if (!!(*config)["smtp_from"]) {
				(*request) << "from" << (string) (*config)["smtp_from"];
			}
			else {
				(*request) << "from" << (string) (*config)["smtp_user"];
			}
		}
		if (!(*request)["from_name"]) {
			if (!!(*config)["smtp_from_fullname"]) {
				(*request) << "from_name" << (string) (*config)["smtp_from_fullname"];
			}
		}
	}
	if (!!(*config)["smtp_use_ssl"]) {
		(*request) << "smtp_use_ssl" << (string) (*config)["smtp_use_ssl"];
	}
	try {
		kinky::utils::mail::send(request);
	}
	catch(KRestException* e) {
		throw new KRestException(KHTTPResponse::INTERNAL_SERVER_ERROR, KEXCEPTION_UNKNOW_ERROR(e->getMessage()->data()));
		delete e;
	}

	return KHTTP_ACCEPTED();
}
