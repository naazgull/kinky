#pragma once

#include <gd.h>
#include <string>
#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;
using namespace __gnu_cxx;

#define MOBILE_CLIENT_REGEXP "; Android|Mobile|Tablet|IEMobile/|Opera Mobi/|Windows CE|WindowsCE|Linux arm|Puffin|UCBrowser|PalmOS|BOLT/|MIDP-|Kindle/|Fennec/"

namespace kinky {

	namespace media {

		enum BrowserType {
			Firefox,
			Seamonkey,
			Chrome,
			Chromium,
			Safari,
			Opera,
			InternetExplorer
		};

		enum ClientType {
			Mobile,
			Desktop
		};

		BrowserType browser_type(string agent);
		ClientType client_type(BrowserType browser, string agent);
		ClientType client_type(string agent);

	}
}
