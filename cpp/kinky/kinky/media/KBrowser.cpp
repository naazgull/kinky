#include <kinky/media/KBrowser.h>

#include <regex.h>

kinky::media::BrowserType kinky::media::browser_type(string agent) {
	if (agent.find("Chrome/") != string::npos && agent.find("Chromium/") == string::npos) {
		return kinky::media::Chrome;
	}
	if (agent.find("Firefox/") != string::npos && agent.find("Seamonkey/") == string::npos) {
		return kinky::media::Firefox;
	}
	if (agent.find(";MSIE ") != string::npos) {
		return kinky::media::InternetExplorer;
	}
	if (agent.find("Safari/") != string::npos && agent.find("Chrome/") == string::npos && agent.find("Chromium/") == string::npos) {
		return kinky::media::Safari;
	}
	if (agent.find("OPR/") != string::npos || agent.find("Opera/") != string::npos) {
		return kinky::media::Opera;
	}
	if (agent.find("Seamonkey/") != string::npos) {
		return kinky::media::Seamonkey;
	}
	if (agent.find("Chromium/") != string::npos) {
		return kinky::media::Chromium;
	}
	return kinky::media::Firefox;
}

kinky::media::ClientType kinky::media::client_type(BrowserType browser, string agent) {
	switch (browser) {
		case Seamonkey :
		case Firefox : {
			if (agent.find("Mobile") != string::npos || agent.find("Tablet") != string::npos) {
				return kinky::media::Mobile;
			}
			return kinky::media::Desktop;
		}
		case Chrome :
		case Chromium :
		case Opera :
		case Safari : {
			if (agent.find("Mobile Safari") != string::npos) {
				return kinky::media::Mobile;
			}
			return kinky::media::Desktop;
		}
		case InternetExplorer : {
			if (agent.find("IEMobile/") != string::npos) {
				return kinky::media::Mobile;
			}
			return kinky::media::Desktop;
		}
	}
	return kinky::media::Desktop;
}

kinky::media::ClientType kinky::media::client_type(string agent) {

//	cout << endl << endl << "\t\t" << agent << endl << endl << flush;

	regex_t re;
	if (regcomp(&re, MOBILE_CLIENT_REGEXP, REG_EXTENDED | REG_NOSUB) != 0) {
		return kinky::media::Desktop;
	}
	if (regexec(&re, agent.c_str(), (size_t) (0), NULL, 0) == 0) {
		return kinky::media::Mobile;
	}
	return kinky::media::Desktop;
}
