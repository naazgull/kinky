#include <kinky/media/KImage.h>

#include <kinky/utils/KFileSystem.h>
#include <kinky/error_codes.h>
#include <curl/curl.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

size_t kinky::media::curlWriter(char *data, size_t size, size_t nmemb, ofstream *buffer) {
	size_t result = 0;

	if (buffer != NULL) {
		buffer->write(data, size * nmemb);
		result = size * nmemb;
	}

	return result;
}

int kinky::media::donwload(string uri, ofstream& output) {
	char errorBuffer[CURL_ERROR_SIZE];
	string buffer;

	CURL* curl = curl_easy_init();
	CURLcode result;

	OpenSSL_add_all_algorithms();
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
	curl_easy_setopt(curl, CURLOPT_URL, uri.data());
	curl_easy_setopt(curl, CURLOPT_HEADER, 0);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, kinky::media::curlWriter);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &output);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	result = curl_easy_perform(curl);

	EVP_cleanup();
	curl_easy_cleanup(curl);

	if (result != CURLE_OK) {
		return -1;
	}

	return 0;
}

void kinky::media::dimensions(string inPath, size_t* width, size_t* height, size_t* size, string* mime) {
	string inFormat = kinky::utils::filesystem::get_mime(inPath);
	mime->assign(inFormat);

	if (inFormat.find("image/") != string::npos) {
		gdImagePtr im;
		FILE *in = fopen(inPath.data(), "rb");

		if (inFormat.find("jpeg") != string::npos || inFormat.find("jpg") != string::npos) {
			im = gdImageCreateFromJpeg(in);
			if (im == NULL) {
				throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_JPEG);
			}
		}
		else if (inFormat.find("png") != string::npos) {
			im = gdImageCreateFromPng(in);
			if (im == NULL) {
				throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_PNG);
			}
		}
		else if (inFormat.find("gif") != string::npos) {
			im = gdImageCreateFromGif(in);
			if (im == NULL) {
				throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_GIF);
			}
		}
		else {
			im = gdImageCreateFromJpeg(in);
			if (im == NULL) {
				throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_JPEG);
			}
		}

		*width = im->sx;
		*height =im->sy;

		fseek(in, 0L, SEEK_END);
		*size = ftell(in);
		fseek(in, 0L, SEEK_SET);

		fclose(in);
		gdImageDestroy(im);
	}
	else if (inFormat.find("video/") != string::npos) {
		FILE *in = fopen(inPath.data(), "rb");

		if (inFormat.find("mpeg") != string::npos || inFormat.find("mpg") != string::npos || inFormat.find("mp4") != string::npos) {
		}
		else if (inFormat.find("avi") != string::npos) {
		}
		else if (inFormat.find("mkv") != string::npos) {
		}
		else if (inFormat.find("webm") != string::npos) {
		}
		else if (inFormat.find("ogv") != string::npos) {
		}
		else {
		}

		*width = 0;
		*height = 0;

		fseek(in, 0L, SEEK_END);
		*size = ftell(in);
		fseek(in, 0L, SEEK_SET);

		fclose(in);
	}
	else if (inFormat.find("audio/") != string::npos) {
		FILE *in = fopen(inPath.data(), "rb");

		if (inFormat.find("mp3") != string::npos || inFormat.find("mp4") != string::npos) {
		}
		else if (inFormat.find("wav") != string::npos) {
		}
		else if (inFormat.find("ogg") != string::npos) {
		}
		else if (inFormat.find("fla") != string::npos) {
		}
		else {
		}

		*width = 0;
		*height = 0;

		fseek(in, 0L, SEEK_END);
		*size = ftell(in);
		fseek(in, 0L, SEEK_SET);

		fclose(in);
	}
	else {
		FILE *in = fopen(inPath.data(), "rb");

		fseek(in, 0L, SEEK_END);
		*size = ftell(in);
		fseek(in, 0L, SEEK_SET);

		*width = 0;
		*height = 0;

		fclose(in);
	}
}

void kinky::media::crop(kinky::lang::KObject* params, string inPath, string outPath) {
	string inFormat = ((string) (*params)["in_format"]);
	string outFormat = ((string) (*params)["out_format"]);
	int sx = ((int) (*params)["width"]);
	int sy = ((int) (*params)["height"]);
	int cx = (*params)["crop_x"] != NULL_KOBJECT ? ((int) (*params)["crop_x"]) : 0;
	int cy = (*params)["crop_y"] != NULL_KOBJECT ? ((int) (*params)["crop_y"]) : 0;

	gdImagePtr im;
	gdImagePtr dst = gdImageCreateTrueColor(sx, sy);
	FILE *in;
	in = fopen(inPath.data(), "rb");
	FILE *out;
	out = fopen(outPath.data(), "w");

	if (inFormat.find("jpeg") != string::npos || inFormat.find("jpg") != string::npos) {
		im = gdImageCreateFromJpeg(in);
		if (im == NULL) {
			throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_JPEG);
		}
	}
	else if (inFormat.find("png") != string::npos) {
		im = gdImageCreateFromPng(in);
		if (im == NULL) {
			throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_PNG);
		}
	}
	else if (inFormat.find("gif") != string::npos) {
		im = gdImageCreateFromGif(in);
		if (im == NULL) {
			throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_GIF);
		}
	}
	else {
		im = gdImageCreateFromJpeg(in);
		if (im == NULL) {
			throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_JPEG);
		}
	}
	gdImageAlphaBlending(im, true);
	gdImageSaveAlpha(im, true);

	gdImageAlphaBlending(dst, false);
	gdImageSaveAlpha(dst, true);
	gdImageCopy(dst, im, 0, 0, cx, cy, sx, sy);

	if (outFormat.find("jpeg") != string::npos || outFormat.find("jpg") != string::npos) {
		gdImageJpeg(dst, out, 100);
	}
	if (outFormat.find("png") != string::npos) {
		gdImagePng(dst, out);
	}
	if (outFormat.find("gif") != string::npos) {
		gdImageGif(dst, out);
	}
	else {
		gdImageJpeg(dst, out, 100);
	}

	fclose(in);
	fclose(out);
	gdImageDestroy(im);
	gdImageDestroy(dst);

	delete params;
}

void kinky::media::resize(kinky::lang::KObject* params, string inPath, string outPath) {
	string inFormat = ((string) (*params)["in_format"]);
	string outFormat = ((string) (*params)["out_format"]);
	double scale = ((double) (*params)["scale"]);
	int sx = ((int) (*params)["width"]);
	int sy = ((int) (*params)["height"]);

	gdImagePtr im;
	FILE *in;
	in = fopen(inPath.data(), "rb");
	FILE *out;
	out = fopen(outPath.data(), "w");

	if (inFormat.find("jpeg") != string::npos || inFormat.find("jpg") != string::npos) {
		im = gdImageCreateFromJpeg(in);
		if (im == NULL) {
			throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_JPEG);
		}
	}
	else if (inFormat.find("png") != string::npos) {
		im = gdImageCreateFromPng(in);
		if (im == NULL) {
			throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_PNG);
		}
	}
	else if (inFormat.find("gif") != string::npos) {
		im = gdImageCreateFromGif(in);
		if (im == NULL) {
			throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_GIF);
		}
	}
	else {
		im = gdImageCreateFromJpeg(in);
		if (im == NULL) {
			throw new KRestException(500, KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_JPEG);
		}
	}
	gdImageAlphaBlending(im, true);
	gdImageSaveAlpha(im, true);

	int ssx = im->sx;
	int ssy = im->sy;

	if (scale != 0) {
		sx = round(im->sx * scale);
		sy = round(im->sy * scale);
	}
	else if (sx != 0 && sy == 0) {
		double ps = ((double) sx) / ((double) im->sx);
		sy = round(im->sy * ps);
	}
	else if (sy != 0 && sx == 0) {
		double ps = ((double) sy) / ((double) im->sy);
		sx = round(im->sx * ps);
	}

	gdImagePtr dst = gdImageCreateTrueColor(sx, sy);
	gdImageAlphaBlending(dst, false);
	gdImageSaveAlpha(dst, true);
	gdImageCopyResized(dst, im, 0, 0, 0, 0, sx, sy, ssx, ssy);

	if (outFormat.find("jpeg") != string::npos || outFormat.find("jpg") != string::npos) {
		gdImageJpeg(dst, out, 100);
	}
	if (outFormat.find("png") != string::npos) {
		gdImagePng(dst, out);
	}
	if (outFormat.find("gif") != string::npos) {
		gdImageGif(dst, out);
	}
	else {
		gdImageJpeg(dst, out, 100);
	}


	fclose(in);
	fclose(out);
	gdImageDestroy(im);
	gdImageDestroy(dst);

	delete params;
}
