#pragma once

#include <gd.h>
#include <string>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <kinky/lang/KObject.h>

using namespace std;
using namespace __gnu_cxx;

/**
 * Utilities for managing images
 * Includes croping, scaling, croped resizing and proportional resizing
 *
*/
namespace kinky {

	namespace media {

		size_t curlWriter(char *data, size_t size, size_t nmemb, ofstream *buffer);

		int donwload(string uri, ofstream& output);

		void dimensions(string inPath, size_t* width, size_t* height, size_t* size, string* mime);

		/**
		 * Crops the image given in <i>inPath</i> and outputs it to <i>outPath</i>
		 * @param params the configuration for the cropping action
		 * @param inPath the path to the image to be cropped
		 * @param outPath the path to the cropped image
		 *
		 * The configuration for the cropping is given in the <i>params</i> object, which must follow the structure:<br>
		 * <pre>{
		 * 	"in_format" : "jpeg|png|gif", <i>(mandatory)</i>
		 * 	"out_format" : "jpeg|png|gif", <i>(mandatory)</i>
		 * 	"width" : [int], <i>(mandatory)</i>
		 * 	"height" : [int] <i>(mandatory)</i>
		 * }
		 * </pre>
		 * All above parameteres are mandatory<br>
		 * <br>
		 * Code snippet:<br>
		 * <pre>
		 * 	kinky::media::crop(KOBJECT(
		 *		"in_format" << "jpeg" <<
		 *		"out_format" << "png" <<
		 *		"width" << 200 <<
		 *		"height" << 200
 		 *	), "in.jpeg", "out.png");
		 * </pre>
		 */
		void crop(kinky::lang::KObject* params, string inPath, string outPath);

		/**
		 * Resizes the image given in <i>inPath</i> and outputs it to <i>outPath</i>
		 * @param params the configuration for the resizing action
		 * @param inPath the path to the image to be resized
		 * @param outPath the path to the resized image
		 *
		 * The configuration for the resizing is given in the <i>params</i> object, which must follow the structure:<br>
		 * <pre>{
		 * 	"in_format" : "jpeg|png|gif", <i>(mandatory)</i>
		 * 	"out_format" : "jpeg|png|gif", <i>(mandatory)</i>
		 * 	"scale" : [double] <i>(optional)</i>
		 * 	"width" : [int], <i>(optional)</i>
		 * 	"height" : [int] <i>(optional)</i>
		 * }
		 * </pre>
		 * <br>
		 * Code snippet for scaling:<br>
		 * <pre>
		 * 	kinky::media::resize(KOBJECT(
		 *		"in_format" << "jpeg" <<
		 *		"out_format" << "png" <<
		 *		"scale" << 0.5
 		 *	), "in.jpeg", "out.png");
		 * </pre>
		 * <br>
		 * Code snippet for cropped resizing:<br>
		 * <pre>
		 * 	kinky::media::resize(KOBJECT(
		 *		"in_format" << "jpeg" <<
		 *		"out_format" << "png" <<
		 *		"width" << 200 <<		// if both width and height are passed
		 *		"height" << 200			// the funcion first crops to the propotion
 		 *	), "in.jpeg", "out.png");		// passed and than resizes
		 * </pre>
		 * <br>
		 * Code snippet for proportional resizing:<br>
		 * <pre>
		 * 	kinky::media::resize(KOBJECT(
		 *		"in_format" << "jpeg" <<
		 *		"out_format" << "png" <<	// if only width or height is passed,
		 *		"width" << 200 			// the function proportionally assumes
 		 *	), "in.jpeg", "out.png");		// the missing size
		 * </pre>
		 * <br>
		 */
		void resize(kinky::lang::KObject* params, string inPath, string outPath);

	}
}
