#pragma once

#include <string.h>
#include <string>
#include <stdint.h>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <time.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	namespace utils {

		namespace encryption {

			namespace KXORCipher {
					string pad(string decrypted, size_t keySize, char delimiter);

					pair<string, size_t>* cipher(string toBeEncrypted, string sKey);

					pair<string, size_t>* decipher(string toBeDecrypted, string sKey);

					void encrypt(string* encrypted, string toBeEncrypted, string sKey);

					void decrypt(string* decrypted, string toBeDecrypted, string sKey);
			}

		}

	}

}
