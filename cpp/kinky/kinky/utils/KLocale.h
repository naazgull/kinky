/* * kinky::oauth.h
 *
 *  Created on: May 31, 2011
 *      Author: pf
 */

#pragma once

#include <string>
#include <wchar.h>
#include <wctype.h>
#include <stdint.h>
#include <iostream>
#include <errno.h>
#include <sstream>
#include <memory.h>
#include <algorithm>
#include <fstream>
#include <kinky/memory/KMemoryManager.h>
#ifdef USE_SPARSEHASH
#include <sparsehash/sparse_hash_map>
#else
#include <kinky/memory/KMaps.h>
#endif

using namespace std;
using namespace __gnu_cxx;

#define BOM8A 0xEF
#define BOM8B 0xBB
#define BOM8C 0xBF


namespace kinky {

	using namespace memory;

	namespace utils {

		const char encodeCharacterTable[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		const char decodeCharacterTable[256] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
		const char encodeCharacterTableUrl[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
		const char decodeCharacterTableUrl[256] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
		const wstring iso = L"\u00e1\u00e0\u00e2\u00e3\u00e4\u00e9\u00e8\u00ea\u1ebd\u00eb\u00ed\u00ec\u00ee\u0129\u00ef\u00f3\u00f2\u00f4\u00f5\u00f6\u00fa\u00f9\u00fb\u0169\u00fc\u00e7\u00c1\u00c0\u00c2\u00c3\u00c4\u00c9\u00c8\u00ca\u1ebc\u00cb\u00cd\u00cc\u00ce\u0128\u00cf\u00d3\u00d2\u00d4\u00d5\u00d6\u00da\u00d9\u00db\u0168\u00dc\u00c7";
		const wstring ascii = L"aaaaaeeeeeiiiiiooooouuuuucAAAAAEEEEEIIIIIOOOOOUUUUUC";


		void base64_encode(istream &in, ostringstream &out);

		void base64_decode(istringstream &in, ostream &out);

		void base64url_encode(istream &in, ostringstream &out);

		void base64url_decode(istringstream &in, ostream &out);

		typedef unsigned char uchar;

		char* wstring_to_utf8(wstring ws);

		wchar_t* utf8_to_wstring(string s);

		int utf8_length(string s);

		void utf8_encode(wstring s, ostream& out, bool quote = true);

		void utf8_encode(string s, ostream& out, bool quote = true);

		void utf8_encode(wstring s, string* out, bool quote = true);

		void utf8_encode(string s, string* out, bool quote = true);

		void utf8_decode(string s, ostream& out);

		void html_entities_encode(wstring s, ostream& out, bool quote = true, bool tags = false);

		void html_entities_encode(string s, ostream& out, bool quote = true, bool tags = false);

		void html_entities_decode(string s, ostream& out);

		void url_encode(wstring s, ostream& out);

		void url_encode(string s, ostream& out);

		void url_decode(string s, ostream& out);

		void ascii_encode(string s, ostream& out, bool quote = true);

		void trim(string& str);

		int count(string& str, char c);

		void replaceAll(string& str, string find, string replace);

		void extractRegionFromAcceptLanguage(string accept, string* locale);

		void extractLocaleFromAcceptLanguage(string accept, string* locale);

		void process_mem_usage(double& vm_usage, double& resident_set);

		void prettify_header_name(string& name);

#ifdef USE_SPARSEHASH
		google::sparse_hash_map<string, managed_ptr<string>*>* strip_cookie(string cookieStr, google::sparse_hash_map<string, managed_ptr<string>*>* cookie = NULL);

		google::sparse_hash_map<string, string*>* strip_object_cookie(string cookieStr, google::sparse_hash_map<string, string*>* cookie = NULL);
#else
		str_map<managed_ptr<string>*>* strip_cookie(string cookieStr, str_map<managed_ptr<string>*>* cookie = NULL);

		str_map<managed_ptr<string>*>* strip_object_cookie(string cookieStr, str_map<managed_ptr<string>*>* cookie = NULL);
#endif

	}
}
