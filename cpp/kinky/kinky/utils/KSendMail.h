#pragma once

#include <sstream>
#include <string>
#include <curl/curl.h>
#include <kinky/lang/KObject.h>
#include <kinky/rest/KRestException.h>
#include <kinky/utils/KDateTime.h>
#include <kinky/utils/KQuotedPrintable.h>

using namespace std;
using namespace __gnu_cxx;

#ifndef CRLF
#define CRLF "\r\n"
#endif

namespace kinky {

	using namespace lang;
	using namespace kinky::rest;

	namespace utils {

		namespace mail {

			int send(KObject* mail);

			size_t curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer);

			size_t curlReader(void *ptr, size_t size, size_t nmemb, void *userdata);

		}
	}
}

