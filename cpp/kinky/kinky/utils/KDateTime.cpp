#include <kinky/utils/KDateTime.h>

struct timezone kinky::utils::date::current_tz = { 0, 0 };

time_t kinky::utils::date::parse(string date, string strFormat) {
	struct tm tm[1] = { { 0 } };
	strptime(date.data(), strFormat.data(), tm);
	if (strFormat.find("%Z") == string::npos) {
		tm->tm_isdst = kinky::utils::date::current_tz.tz_dsttime;
		tm->tm_gmtoff = kinky::utils::date::timezoneOffset();
		tm->tm_zone =kinky::utils::date::current_tz.tz_dsttime == 0 ? tzname[0] : tzname[1];
	}
	return mktime(tm);
}

string kinky::utils::date::format(string strFormat) {
	time_t rawtime;
	struct tm ptm;
	char* buffer = new char[516];
	time(&rawtime);
	localtime_r(&rawtime, &ptm);
	strftime(buffer, 516, strFormat.data(), &ptm);

	managed_ptr<string>* ret = new managed_ptr<string>(new string(buffer));
	delete[] buffer;
	return **ret;
}

string kinky::utils::date::format(string strFormat, time_t rawtime) {
	struct tm ptm;
	char* buffer = new char[516];
	localtime_r(&rawtime, &ptm);
	strftime(buffer, 516, strFormat.data(), &ptm);

	managed_ptr<string>* ret = new managed_ptr<string>(new string(buffer));
	delete[] buffer;
	return **ret;
}

string kinky::utils::date::format(string strFormat, const int delta[], int length) {
	time_t rawtime;
	struct tm ptm;
	char* buffer = new char[516];
	time(&rawtime);
	for (int i = 0; i != length * 2; i += 2) {
		switch (delta[i]) {
			case K_SECONDS: {
				rawtime += delta[i + 1];
				break;
			}
			case K_MINUTES: {
				rawtime += delta[i + 1] * 60;
				break;
			}
			case K_HOURS: {
				rawtime += delta[i + 1] * 3600;
				break;
			}
			case K_DAYS: {
				rawtime += delta[i + 1] * 24 * 3600;
				break;
			}
			case K_WEEKS: {
				rawtime += delta[i + 1] * 7 * 24 * 3600;
				break;
			}
			case K_MONTHS: {
				rawtime += delta[i + 1] * 30 * 24 * 3600;
				break;
			}
			case K_YEARS: {
				rawtime += delta[i + 1] * 365 * 24 * 3600;
				break;
			}
		}
	}
	localtime_r(&rawtime, &ptm);
	strftime(buffer, 516, strFormat.data(), &ptm);

	managed_ptr<string>* ret = new managed_ptr<string>(new string(buffer));
	delete[] buffer;
	return **ret;
}

string kinky::utils::date::formatGMT(string strFormat) {
	time_t rawtime;
	struct tm ptm;
	char* buffer = new char[516];
	time(&rawtime);
	rawtime = timegm(&ptm);
	gmtime_r(&rawtime, &ptm);
	strftime(buffer, 516, strFormat.data(), &ptm);

	managed_ptr<string>* ret = new managed_ptr<string>(new string(buffer));
	delete[] buffer;
	return **ret;
}

string kinky::utils::date::formatGMT(string strFormat, time_t rawtime) {
	struct tm ptm;
	char* buffer = new char[516];
	gmtime_r(&rawtime, &ptm);
	strftime(buffer, 516, strFormat.data(), &ptm);

	managed_ptr<string>* ret = new managed_ptr<string>(new string(buffer));
	delete[] buffer;
	return **ret;
}

string kinky::utils::date::formatGMT(string strFormat, const int delta[], int length) {
	time_t rawtime;
	struct tm ptm;
	char* buffer = new char[516];
	time(&rawtime);
	for (int i = 0; i != length * 2; i += 2) {
		switch (delta[i]) {
			case K_SECONDS: {
				rawtime += delta[i + 1];
				break;
			}
			case K_MINUTES: {
				rawtime += delta[i + 1] * 60;
				break;
			}
			case K_HOURS: {
				rawtime += delta[i + 1] * 3600;
				break;
			}
			case K_DAYS: {
				rawtime += delta[i + 1] * 24 * 3600;
				break;
			}
			case K_WEEKS: {
				rawtime += delta[i + 1] * 7 * 24 * 3600;
				break;
			}
			case K_MONTHS: {
				rawtime += delta[i + 1] * 30 * 24 * 3600;
				break;
			}
			case K_YEARS: {
				rawtime += delta[i + 1] * 365 * 24 * 3600;
				break;
			}
		}
	}
	gmtime_r(&rawtime, &ptm);
	strftime(buffer, 516, strFormat.data(), &ptm);

	managed_ptr<string>* ret = new managed_ptr<string>(new string(buffer));
	delete[] buffer;
	return **ret;
}

time_t kinky::utils::date::timezoneOffset() {
	time_t t;
	tm *ptr;
	int day;
	ulong num[2];
	t = time(NULL);
	ptr = gmtime(&t); // Standard UTC time
	// Get difference in seconds
	num[0] = (ptr->tm_hour * 3600) + (ptr->tm_min * 60);
	day = ptr->tm_mday;
	ptr = localtime(&t); // Local time w/ time zone
	num[1] = (ptr->tm_hour * 3600) + (ptr->tm_min * 60);
	// If not the same then get difference
	if (day == ptr->tm_mday) { // No date difference
		if (num[0] < num[1]) {
			return (num[1] - num[0]);// Positive ex. CUT +1
		}
		else if (num[0] > num[1]) {
			return -(num[0] - num[1]);// Negative ex. Pacific -8
		}
	}
	else if (day < ptr->tm_mday) {// Ex. 1: 30 am Jan 1 : 11: 30 pm Dec 31
		return (86400 - num[0]) + num[1];
	}
	else {
		return -((86400 - num[1]) + num[0]);// Opposite
	}
	return 0;
}
