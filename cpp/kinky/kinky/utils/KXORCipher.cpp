#include <kinky/utils/KXORCipher.h>
#include <kinky/utils/KBase64.h>
#include <zlib.h>

string kinky::utils::encryption::KXORCipher::pad(string decrypted, size_t keySize, char delimiter) {
	ostringstream oss("");
	string junk = "ZmdpaWwwbG1rY2w6Jy0vLyUlLy8tLyclLyUtLy8lJSUlJSctJy0tLy13PzcwOzpf";
	int paddingSize = -1;
	oss << decrypted << flush;

	if (oss.str().length() < keySize) {
		paddingSize = keySize;
	}
	else if (oss.str().length() > keySize && oss.str().length() < keySize * 2) {
		paddingSize = keySize * 2;
	}
	if (paddingSize != -1) {
		oss << delimiter << flush;
		srand(time(NULL));
		for (int i = oss.str().length(); i != paddingSize; i++) {
			int j = rand() % junk.length();
			oss << junk[j] << flush;
		}
	}
	return oss.str();
}

pair<string, size_t>* kinky::utils::encryption::KXORCipher::cipher(string toBeEncrypted, string sKey) {
	unsigned int iKey = sKey.length(), iIn = toBeEncrypted.length(), x = 0;
	string sEncrypted(toBeEncrypted);

	for (unsigned int i = 0; i < iIn; i++) {
		sEncrypted[i] = toBeEncrypted[i] ^ (sKey[x] & 10);
		if (++x == iKey) {
			x = 0;
		}
	}
	return new pair<string, size_t>(sEncrypted, iIn);
}

pair<string, size_t>* kinky::utils::encryption::KXORCipher::decipher(string toBeDecrypted, string sKey) {
	return kinky::utils::encryption::KXORCipher::cipher(toBeDecrypted, sKey);
}

void kinky::utils::encryption::KXORCipher::encrypt(string* encrypted, string toBeEncrypted, string sKey) {
	Bytef* src = new Bytef[toBeEncrypted.length()];
	size_t destLen = (size_t) (toBeEncrypted.length() * 1.1 + 12);
	Bytef* dest = new Bytef[destLen];

	for (size_t i = 0; i != toBeEncrypted.length(); i++) {
		src[i]  = (Bytef) toBeEncrypted[i];
	}

	compress(dest, &destLen, src, toBeEncrypted.length());

	ostringstream cos;
	for (size_t i = 0; i != destLen; i++) {
		cos << dest[i];
	}
	cos << flush;

	pair<string, size_t>* encryptedPair = kinky::utils::encryption::KXORCipher::cipher(cos.str(), sKey);
	stringstream in(encryptedPair->first);

	ostringstream out;
	out << toBeEncrypted.length() << "|" << flush;

	ostringstream oss;
	kinky::utils::base64_encode(in, oss);
	out << oss.str() << flush;

	delete[] src;
	delete[] dest;
	delete encryptedPair;

	encrypted->assign(out.str());
}

void kinky::utils::encryption::KXORCipher::decrypt(string* decrypted, string toBeDecrypted, string sKey) {
	int indexOfPipe = toBeDecrypted.find('|');
	size_t size = 0;
	stringstream sizes;
	sizes << toBeDecrypted.substr(0, indexOfPipe) << flush;
	sizes >> size;
	if (size == 0) {
		return;
	}

	string toUnserialize = toBeDecrypted.substr(indexOfPipe + 1);
	istringstream in(toUnserialize);
	ostringstream out;
	kinky::utils::base64_decode(in, out);

	pair<string, size_t>* decryptedPair = kinky::utils::encryption::KXORCipher::decipher(out.str(), sKey);

	Bytef* src = new Bytef[decryptedPair->first.length()];
	size_t destLen = size;
	Bytef* dest = new Bytef[destLen];

	for (size_t i = 0; i != decryptedPair->first.length(); i++) {
		src[i]  = (Bytef) decryptedPair->first[i];
	}

	uncompress(dest, &destLen, src, decryptedPair->first.length());

	ostringstream cos;
	for (size_t i = 0; i != destLen; i++) {
		cos << dest[i];
	}
	cos << flush;

	delete[] src;
	delete[] dest;
	delete decryptedPair;

	decrypted->assign(cos.str());
}
