/*
 * kinky::oauth.h
 *
 *  Created on: May 31, 2011
 *      Author: pf
 */

#pragma once

#include <string>
#include <stdint.h>
#include <iostream>
#include <kinky/utils/KLocale.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	namespace utils {

		class KBase64 {
			public:
				static void base64_encode(istream &in, ostringstream &out) {
					kinky::utils::base64_encode(in, out);
				}

				static void base64_decode(istringstream &in, ostream &out) {
					kinky::utils::base64_decode(in, out);
				}

		};
	}
}
