#pragma once

#include <string>
#include <vector>
#include <stdint.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <regex.h>
#include <kinky/rest/KRestException.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	namespace utils {

		namespace filesystem {

			int mkdir_recursive(const char* name, mode_t mode, const char* parent = "");

			int rmdir_recursive(string& dir, string pattern="");

			bool dump_file(const char* name, string data);

			bool load_file(const char* name, ostream& oss);

			bool load_file(const char* name, string* oss);

			char* load_file(const char* name, size_t* length);

			int ls(string& dir, vector<string>& result, bool recursive = false);

			int globRegexp(string& dir, vector<string>& result, regex_t& pattern, bool recursive = false);

			int glob(string& dir, vector<string>& result, string pattern, bool recursive = false);

			int copy(string from, string to);

			int move(string from, string to);

			string get_mime(string filename);

			void normalize_dirname(string& dir, bool with_trailing_slash = false);

			bool file_exists(string filename);
		}

	}
}
