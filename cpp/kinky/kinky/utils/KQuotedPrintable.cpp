#include <kinky/utils/KQuotedPrintable.h>

string kinky::utils::mail::quoted_printable_encode(string toQuote, string charset) {
	ostringstream oss;
	oss << "=?" << charset << "?Q?" << flush;
	string s(toQuote);
	for (size_t i = 0; i != s.length(); i++) {
		if (((unsigned char) s[i]) > 127) {
			oss << "=" << uppercase << hex << ((int) ((unsigned char) s[i]));
		}
		else {
			oss << s[i];
		}
	}
	oss << "?=" << flush;
	return oss.str().data();
}
