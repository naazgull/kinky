#include <kinky/utils/KFileSystem.h>
#include <fstream>
#include <sys/sendfile.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <magic.h>
#include <kinky/memory/KMemoryManager.h>

using namespace std;

int kinky::utils::filesystem::mkdir_recursive(const char* name, mode_t mode, const char* parent) {
	istringstream iss(name);
	string line;
	int count = 0;
	ostringstream dname;
	dname << parent;

	while (iss.good()) {
		std::getline(iss, line, '/');
		string cname(line.data());
		dname << cname << flush;

		if (mkdir(dname.str().data(), 0777) == 0) {
			count++;
		}
		dname << "/" << flush;
	}
	return count;
}

int kinky::utils::filesystem::rmdir_recursive(string& dir, string pattern) {
	bool selfDel = false;
	if (pattern.length() == 0) {
		pattern = ".*";
		selfDel = true;
	}

	if (dir[dir.length() - 1] == '/')
	dir = dir.substr(0, dir.length() - 1);

	bool delInside = false;

	vector<string> result;
	if (kinky::utils::filesystem::glob(dir, result, pattern) == 0) {
		for (int i = 0; i < (int) result.size(); i++) {
			if (remove(result[i].c_str()) != 0) {
				return -1;
			}
		}
		delInside = true;
	}

	if (delInside && selfDel) {
		if (remove(dir.c_str()) != 0) {
			return -1;
		}
	}

	return 0;
}

bool kinky::utils::filesystem::dump_file(const char* name, string data) {
	ofstream ofs;
	ofs.open(name);
	ofs << data << flush;
	ofs.close();
	return true;
}

bool kinky::utils::filesystem::load_file(const char* name, ostream& oss) {
	ifstream in;
	in.open(name);

	if (in.is_open()) {
		in.seekg(0, ios::end);
		size_t length = in.tellg();
		in.seekg(0, ios::beg);
		char buffer[length];
		in.read(buffer, length);
		in.close();
		oss << string(buffer, length);
		oss << flush;
		return true;
	}
	return false;
}

bool kinky::utils::filesystem::load_file(const char* name, string* oss) {
	ifstream in;
	in.open(name);

	if (in.is_open()) {
		in.seekg(0, ios::end);
		size_t length = in.tellg();
		in.seekg(0, ios::beg);
		char buffer[length];
		in.read(buffer, length);
		in.close();
		oss->insert(oss->length(), buffer, length);
		return true;
	}
	return false;
}

char* kinky::utils::filesystem::load_file(const char* name, size_t* length) {
	ifstream in;
	in.open(name);

	if (in.is_open()) {
		in.seekg(0, ios::end);
		*length = in.tellg();
		in.seekg(0, ios::beg);
		char* buffer = new char[*length];
		in.read(buffer, *length);
		in.close();
		return buffer;
	}
	*length = 0;
	return NULL;
}

int kinky::utils::filesystem::ls(string& dir, vector<string>& result, bool recursive) {
	DIR *dp;
	struct dirent *dirp;
	if ((dp = opendir(dir.c_str())) == NULL) {
		return errno;
	}

	while ((dirp = readdir(dp)) != NULL) {
		string cname = string(dirp->d_name);
		if (cname.find('.') != 0) {
			cname.insert(0, "/");
			cname.insert(0, dir);
			result.push_back(cname);
			if (recursive) {
				kinky::utils::filesystem::ls(cname, result, true);
			}

		}
	}

	closedir(dp);

	return 0;
}

int kinky::utils::filesystem::globRegexp(string& dir, vector<string>& result, regex_t& pattern, bool recursive) {
	DIR *dp;
	struct dirent *dirp;
	vector<string> torecurse;

	if ((dp = opendir(dir.c_str())) == NULL) {
		return errno;
	}
	while ((dirp = readdir(dp)) != NULL) {
		string cname = string(dirp->d_name);
		if (cname.find('.') != 0) {
			cname.insert(0, "/");
			cname.insert(0, dir.data());
			if (recursive && dirp->d_type == 4 && cname != dir) {
				torecurse.push_back(cname);
			}
			if (regexec(&pattern, dirp->d_name, (size_t) (0), NULL, 0) == 0) {
				result.insert(result.begin(), cname);
			}

		}
	}
	closedir(dp);

	for (vector<string>::iterator i = torecurse.begin(); i != torecurse.end(); i++) {
		kinky::utils::filesystem::globRegexp(*i, result, pattern, true);
	}

	return 0;
}

int kinky::utils::filesystem::glob(string& dir, vector<string>& result, string pattern, bool recursive) {
	regex_t regexp;
	if (regcomp(&regexp, pattern.data(), REG_EXTENDED | REG_NOSUB) != 0) {
		throw new kinky::rest::KRestException(500, "the regular expression is not well defined.");
	}
	return kinky::utils::filesystem::globRegexp(dir, result, regexp, recursive);
}

int kinky::utils::filesystem::copy(string from, string to) {
	int read_fd;
	int write_fd;
	struct stat stat_buf;

	read_fd = open(from.c_str(), O_RDONLY);
	if (read_fd < 0) {
		return read_fd;
	}
	fstat(read_fd, &stat_buf);
	write_fd = open(to.c_str(), O_WRONLY | O_CREAT, stat_buf.st_mode);
	int error = sendfile(write_fd, read_fd, NULL, stat_buf.st_size);
	close(read_fd);
	close(write_fd);

	return (error == -1 ? -1 : 0);
}

int kinky::utils::filesystem::move(string from, string to) {
	if (kinky::utils::filesystem::copy(from, to) == 0) {
		return std::remove(from.c_str());
	}
	return -1;
}

string kinky::utils::filesystem::get_mime(string filename) {
	magic_t myt = magic_open(MAGIC_CONTINUE | MAGIC_ERROR | MAGIC_MIME);
	magic_load(myt, NULL);
	string* ret = new string(magic_file(myt, filename.data()));
	magic_close(myt);
	(new kinky::memory::managed_ptr<string>(ret));

	return *ret;
}

void kinky::utils::filesystem::normalize_dirname(string& dir, bool with_trailing_slash) {
	if (with_trailing_slash) {
		if (dir[dir.length() - 1] != '/') {
			dir.insert(dir.length(), "/");
		}
	}
	else {
		if (dir[dir.length() - 1] == '/') {
			dir.erase(dir.length() - 1, 1);
		}
	}
}

bool kinky::utils::filesystem::file_exists(string filename) {
	struct stat  buffer;
	return ( stat(filename.data(), &buffer) == 0);
}
