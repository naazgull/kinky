#include <kinky/utils/KLocale.h>

#include <unistd.h>
#include <iomanip>

using namespace std;
using namespace __gnu_cxx;

void kinky::utils::base64_encode(istream &in, ostringstream &out) {
	char buff1[3];
	char buff2[4];
	uint8_t i = 0, j;

	while (in.readsome(&buff1[i++], 1))
		if (i == 3) {
			out << encodeCharacterTable[(buff1[0] & 0xfc) >> 2];
			out << encodeCharacterTable[((buff1[0] & 0x03) << 4) + ((buff1[1] & 0xf0) >> 4)];
			out << encodeCharacterTable[((buff1[1] & 0x0f) << 2) + ((buff1[2] & 0xc0) >> 6)];
			out << encodeCharacterTable[buff1[2] & 0x3f];
			i = 0;
		}

	if (--i) {
		for (j = i; j < 3; j++)
			buff1[j] = '\0';

		buff2[0] = (buff1[0] & 0xfc) >> 2;
		buff2[1] = ((buff1[0] & 0x03) << 4) + ((buff1[1] & 0xf0) >> 4);
		buff2[2] = ((buff1[1] & 0x0f) << 2) + ((buff1[2] & 0xc0) >> 6);
		buff2[3] = buff1[2] & 0x3f;

		for (j = 0; j < (i + 1); j++)
			out << encodeCharacterTable[(uint8_t) buff2[j]];

		while (i++ < 3)
			out << '=';
	}
	out << flush;
}

void kinky::utils::base64_decode(istringstream &in, ostream &out) {
	char buff1[4];
	char buff2[4];
	uint8_t i = 0, j;

	while (in.readsome(&buff2[i], 1) && buff2[i] != '=') {
		if (++i == 4) {
			for (i = 0; i != 4; i++)
				buff2[i] = decodeCharacterTable[(uint8_t) buff2[i]];

			out << (char) ((buff2[0] << 2) + ((buff2[1] & 0x30) >> 4));
			out << (char) (((buff2[1] & 0xf) << 4) + ((buff2[2] & 0x3c) >> 2));
			out << (char) (((buff2[2] & 0x3) << 6) + buff2[3]);

			i = 0;
		}
	}

	if (i) {
		for (j = i; j < 4; j++)
			buff2[j] = '\0';
		for (j = 0; j < 4; j++)
			buff2[j] = decodeCharacterTable[(uint8_t) buff2[j]];

		buff1[0] = (buff2[0] << 2) + ((buff2[1] & 0x30) >> 4);
		buff1[1] = ((buff2[1] & 0xf) << 4) + ((buff2[2] & 0x3c) >> 2);
		buff1[2] = ((buff2[2] & 0x3) << 6) + buff2[3];

		for (j = 0; j < (i - 1); j++)
			out << (char) buff1[j];
	}
	out << flush;
}

void kinky::utils::base64url_encode(istream &in, ostringstream &out) {
	char buff1[3];
	char buff2[4];
	uint8_t i = 0, j;

	while (in.readsome(&buff1[i++], 1))
		if (i == 3) {
			out << encodeCharacterTableUrl[(buff1[0] & 0xfc) >> 2];
			out << encodeCharacterTableUrl[((buff1[0] & 0x03) << 4) + ((buff1[1] & 0xf0) >> 4)];
			out << encodeCharacterTableUrl[((buff1[1] & 0x0f) << 2) + ((buff1[2] & 0xc0) >> 6)];
			out << encodeCharacterTableUrl[buff1[2] & 0x3f];
			i = 0;
		}

	if (--i) {
		for (j = i; j < 3; j++)
			buff1[j] = '\0';

		buff2[0] = (buff1[0] & 0xfc) >> 2;
		buff2[1] = ((buff1[0] & 0x03) << 4) + ((buff1[1] & 0xf0) >> 4);
		buff2[2] = ((buff1[1] & 0x0f) << 2) + ((buff1[2] & 0xc0) >> 6);
		buff2[3] = buff1[2] & 0x3f;

		for (j = 0; j < (i + 1); j++)
			out << encodeCharacterTableUrl[(uint8_t) buff2[j]];

//		while (i++ < 3)
//			out << '=';
	}
	out << flush;
}

void kinky::utils::base64url_decode(istringstream &in, ostream &out) {
	char buff1[4];
	char buff2[4];
	uint8_t i = 0, j;

	while (in.readsome(&buff2[i], 1) && buff2[i] != '=') {
		if (++i == 4) {
			for (i = 0; i != 4; i++)
				buff2[i] = decodeCharacterTableUrl[(uint8_t) buff2[i]];

			out << (char) ((buff2[0] << 2) + ((buff2[1] & 0x30) >> 4));
			out << (char) (((buff2[1] & 0xf) << 4) + ((buff2[2] & 0x3c) >> 2));
			out << (char) (((buff2[2] & 0x3) << 6) + buff2[3]);

			i = 0;
		}
	}

	if (i) {
		for (j = i; j < 4; j++)
			buff2[j] = '\0';
		for (j = 0; j < 4; j++)
			buff2[j] = decodeCharacterTableUrl[(uint8_t) buff2[j]];

		buff1[0] = (buff2[0] << 2) + ((buff2[1] & 0x30) >> 4);
		buff1[1] = ((buff2[1] & 0xf) << 4) + ((buff2[2] & 0x3c) >> 2);
		buff1[2] = ((buff2[2] & 0x3) << 6) + buff2[3];

		for (j = 0; j < (i - 1); j++)
			out << (char) buff1[j];
	}
	out << flush;
}


char* kinky::utils::wstring_to_utf8(wstring ws) {
	string dest;
	dest.clear();
	for (size_t i = 0; i < ws.size(); i++) {
		wchar_t w = ws[i];
		if (w <= 0x7f)
		dest.push_back((char) w);
		else if (w <= 0x7ff) {
			dest.push_back(0xc0 | ((w >> 6) & 0x1f));
			dest.push_back(0x80 | (w & 0x3f));
		}
		else if (w <= 0xffff) {
			dest.push_back(0xe0 | ((w >> 12) & 0x0f));
			dest.push_back(0x80 | ((w >> 6) & 0x3f));
			dest.push_back(0x80 | (w & 0x3f));
		}
		else if (w <= 0x10ffff) {
			dest.push_back(0xf0 | ((w >> 18) & 0x07));
			dest.push_back(0x80 | ((w >> 12) & 0x3f));
			dest.push_back(0x80 | ((w >> 6) & 0x3f));
			dest.push_back(0x80 | (w & 0x3f));
		}
		else
		dest.push_back('?');
	}

	char* c = new char[dest.length() + 1];
	memset(c, 0, dest.length() + 1);
	memcpy(c, dest.c_str(), dest.length());
	return c;
}

wchar_t* kinky::utils::utf8_to_wstring(string s) {
	long b = 0, c = 0;
	const char* string = s.data();

	if ((uchar) string[0] == BOM8A && (uchar) string[1] == BOM8B && (uchar) string[2] == BOM8C) {
		string += 3;
	}

	for (const char *a = string; *a; a++) {
		if (((uchar) *a) < 128 || (*a & 192) == 192) {
			c++;
		}
	}
	wchar_t *res = new wchar_t[c + 1];
	res[c] = 0;
	for (uchar *a = (uchar*) string; *a; a++) {
		if (!(*a & 128)) {
			res[b] = *a;
		}
		else if ((*a & 192) == 128) {
			continue;
		}
		else if ((*a & 224) == 192) {
			res[b] = ((*a & 31) << 6) | (a[1] & 63);
		}
		else if ((*a & 240) == 224) {
			res[b] = ((*a & 15) << 12) | ((a[1] & 63) << 6) | (a[2] & 63);
		}
		else if ((*a & 248) == 240) {
			res[b] = '?';
		}
		b++;
	}
	return res;
}

int kinky::utils::utf8_length(string s) {
	int size = 0;
	for (size_t i = 0; i != s.length(); i++) {
		if (((wchar_t) s[i]) < 0x80) {
			size++;
		}
		else if (((wchar_t) s[i]) < 0x800) {
			size += 2;
		}
		else {
			size += 3;
		}
	}
	return size;
}

void kinky::utils::utf8_encode(wstring s, ostream& out, bool quote) {
	ostringstream oss;

	for (size_t i = 0; i != s.length(); i++) {
		if (((int) s[i]) > 127) {
			oss << "\\u" <<  setfill('0') << setw(4) << hex << ((int) s.at(i));
		}
		else if (quote && (s[i] == '"')) {
			oss << "\\" << ((char) s.at(i));
		}
		else if (s[i] == '\n') {
			oss << "\\n";
		}
		else if (s[i] == '\r') {
			oss << "\\r";
		}
		else if (s[i] == '\f') {
			oss << "\\f";
		}
		else if (s[i] == '\t') {
			oss << "\\t";
		}
		else if (s[i] == '/') {
			oss << "\\/";
		}
		else if (quote && s[i] == '\\') {
			oss << "\\\\";
		}
		else if (((int) s[i]) <= 31) {
			oss << "";
		}
		else {
			oss << ((char) s.at(i));
		}
	}
	oss << flush;
	out << oss.str();
}

void kinky::utils::utf8_encode(string s, ostream& out, bool quote) {
	long b = 0, c = 0;
	const char* string = s.data();

	if ((uchar) string[0] == BOM8A && (uchar) string[1] == BOM8B && (uchar) string[2] == BOM8C) {
		string += 3;
	}

	for (const char *a = string; *a; a++) {
		if (((uchar) *a) < 128 || (*a & 192) == 192) {
			c++;
		}
	}
	wchar_t *res = new wchar_t[c + 1];
	res[c] = 0;
	for (uchar *a = (uchar*) string; *a; a++) {
		if (!(*a & 128)) {
			res[b] = *a;
		}
		else if ((*a & 192) == 128) {
			continue;
		}
		else if ((*a & 224) == 192) {
			res[b] = ((*a & 31) << 6) | (a[1] & 63);
		}
		else if ((*a & 240) == 224) {
			res[b] = ((*a & 15) << 12) | ((a[1] & 63) << 6) | (a[2] & 63);
		}
		else if ((*a & 248) == 240) {
			res[b] = '?';
		}
		b++;
	}

	wstring ws;
	ws.assign(res, c + 1);
	kinky::utils::utf8_encode(ws, out, quote);
	delete[] res;

}

void kinky::utils::utf8_encode(wstring s, string* out, bool quote) {

	for (size_t i = 0; i != s.length(); i++) {
		if (((int) s[i]) > 127) {
			string nr;
			kinky::tostr(nr, ((int) s.at(i)), hex);
			out->insert(out->length(), "\\u00" ); out->insert(out->length(), nr);
		}
		else if (quote && (s[i] == '"')) {
			string nr;
			kinky::tostr(nr, ((char) s.at(i)));
			out->insert(out->length(), "\\"); out->insert(out->length(), nr);
		}
		else if (s[i] == '\n') {
			out->insert(out->length(), "\\n");
		}
		else if (s[i] == '\r') {
			out->insert(out->length(), "\\r");
		}
		else if (s[i] == '\f') {
			out->insert(out->length(), "\\f");
		}
		else if (s[i] == '\t') {
			out->insert(out->length(), "\\t");
		}
		else if (s[i] == '/') {
			out->insert(out->length(), "\\/");
		}
		else if (((int) s[i]) <= 31) {
			out->insert(out->length(), "");
		}
		else {
			string nr;
			kinky::tostr(nr, ((char) s.at(i)));
			out->insert(out->length(), nr);
		}
	}
}

void kinky::utils::utf8_encode(string s, string* out, bool quote) {
	long b = 0, c = 0;
	const char* string = s.data();

	if ((uchar) string[0] == BOM8A && (uchar) string[1] == BOM8B && (uchar) string[2] == BOM8C) {
		string += 3;
	}

	for (const char *a = string; *a; a++) {
		if (((uchar) *a) < 128 || (*a & 192) == 192) {
			c++;
		}
	}
	wchar_t *res = new wchar_t[c + 1];
	res[c] = 0;
	for (uchar *a = (uchar*) string; *a; a++) {
		if (!(*a & 128)) {
			res[b] = *a;
		}
		else if ((*a & 192) == 128) {
			continue;
		}
		else if ((*a & 224) == 192) {
			res[b] = ((*a & 31) << 6) | (a[1] & 63);
		}
		else if ((*a & 240) == 224) {
			res[b] = ((*a & 15) << 12) | ((a[1] & 63) << 6) | (a[2] & 63);
		}
		else if ((*a & 248) == 240) {
			res[b] = '?';
		}
		b++;
	}

	wstring ws;
	ws.assign(res, c + 1);
	kinky::utils::utf8_encode(ws, out, quote);
	delete[] res;

}
void kinky::utils::utf8_decode(string s, ostream& out) {
	wostringstream oss;
	for (size_t i = 0; i != s.length(); i++) {
		if (s[i] == '\\' && s[i + 1] == 'u') {
			stringstream ss;
			ss << s[i + 2] << s[i + 3] << s[i + 4] << s[i + 5];
			int c;
			ss >> hex >> c;
			oss << ((wchar_t) c);
			i += 5;
		}
		else {
			oss << ((wchar_t) s[i]);
		}
	}
	oss << flush;

	char* c = kinky::utils::wstring_to_utf8(oss.str());
	string os(c);
	out << os << flush;

	delete[] c;
}

void kinky::utils::html_entities_encode(wstring s, ostream& out, bool quote, bool tags) {
	ostringstream oss;
	for (size_t i = 0; i != s.length(); i++) {
		if (((unsigned char) s[i]) > 127) {
			oss << "&#" << dec << ((int) s.at(i)) << ";";
		}
		else if (s[i] == '"' && quote) {
			oss << "&quot;";
		}
		else if (s[i] == '<' && tags) {
			oss << "&lt;";
		}
		else if (s[i] == '>' && tags) {
			oss << "&gt;";
		}
		else if (s[i] == '&') {
			oss << "&amp;";
		}
		else {
			oss << ((char) s.at(i));
		}
	}
	oss << flush;
	out << oss.str();
}

void kinky::utils::html_entities_encode(string s, ostream& out, bool quote, bool tags) {
	wchar_t* wc = kinky::utils::utf8_to_wstring(s);
	wstring ws(wc);
	kinky::utils::html_entities_encode(ws, out, quote, tags);
	delete[] wc;
}

void kinky::utils::html_entities_decode(string s, ostream& out) {
	wostringstream oss;
	for (size_t i = 0; i != s.length(); i++) {
		if (s[i] == '&' && s[i + 1] == '#') {
			stringstream ss;
			int j = i + 2;
			while (s[j] != ';') {
				ss << s[j];
				j++;
			}
			int c;
			ss >> c;
			oss << ((wchar_t) c);
			i = j;
		}
		else {
			oss << ((wchar_t) s[i]);
		}
	}
	oss << flush;

	char* c = kinky::utils::wstring_to_utf8(oss.str());
	string os(c);
	out << os << flush;

	delete[] c;
}

void kinky::utils::url_encode(string s, ostream& out) {
	const char DEC2HEX[16 + 1] = "0123456789ABCDEF";
	const unsigned char * pSrc = (const unsigned char *) s.c_str();
	const int SRC_LEN = s.length();
	unsigned char * const pStart = new unsigned char[SRC_LEN * 3];
	unsigned char * pEnd = pStart;
	const unsigned char * const SRC_END = pSrc + SRC_LEN;

	for (; pSrc < SRC_END; ++pSrc) {
		if ((*pSrc > 127) || (*pSrc == '%') || (*pSrc == ' ') || (*pSrc == '&') || (*pSrc == '+') || (*pSrc == '?') || (*pSrc == '#') || (*pSrc == '=') || (*pSrc == '/') || (*pSrc == ':')) {
			*pEnd++ = '%';
			*pEnd++ = DEC2HEX[*pSrc >> 4];
			*pEnd++ = DEC2HEX[*pSrc & 0x0F];
		}
		else {
			*pEnd++ = *pSrc;
		}
	}

	std::string sResult((char *) pStart, (char *) pEnd);
	delete[] pStart;
	out << sResult;
}

void kinky::utils::url_decode(string s, ostream& out) {
	kinky::utils::replaceAll(s, "+", "%20");

	const unsigned char * pSrc = (const unsigned char *) s.c_str();
	const int SRC_LEN = s.length();
	const unsigned char * const SRC_END = pSrc + SRC_LEN;
	const unsigned char * const SRC_LAST_DEC = SRC_END - 2;

	char * const pStart = new char[SRC_LEN];
	char * pEnd = pStart;

	while (pSrc < SRC_LAST_DEC) {
		if (*pSrc == '%') {
//			char dec1 = *(pSrc + 1), dec2 = *(pSrc + 2);
//			*pEnd++ = (dec1 << 4) + dec2;
			stringstream ss;
			ss << *(pSrc + 1) << *(pSrc + 2);
			int c;
			ss >> hex >> c;
			*pEnd++ = (char) c;
			pSrc += 3;
			continue;
		}

		*pEnd++ = *pSrc++;
	}

	// the last 2- chars
	while (pSrc < SRC_END) {
		*pEnd++ = *pSrc++;
	}
	std::string sResult(pStart, pEnd);
	delete[] pStart;
	out << sResult;
}

void kinky::utils::ascii_encode(string s, ostream& out, bool quote) {
	wchar_t* wc = kinky::utils::utf8_to_wstring(s);
	wstring ws(wc);

	for (size_t i = 0; i != iso.length(); i++) {
		std::replace(ws.begin(), ws.end(), iso[i], ascii[i]);
	}

	delete[] wc;
	for (size_t i = 0; i != ws.length(); i++) {
		if (((int) ws[i]) <= 127) {
			out << ((char) ws[i]) << flush;
		}
		else {
			out << " " << flush;
		}
	}
}

void kinky::utils::trim(string& str) {
	if (str.length() == 0) {
		return;
	}

	size_t start, end;

	//ltrim
	for (start = 0; (start != str.length() && ((unsigned char) str[start]) <= 32); start++)
		;

	//rtrim
	for (end = str.length() - 1; (end != 0 && ((unsigned char) str[end]) <= 32); end--)
		;

	if (end < start) {
		str.assign("");
	}
	else {
		if (end < str.length() - 1) {
			str.erase(end + 1);
		}
		if (start <= str.length() - 1) {
			str.erase(0, start);
		}
	}
}

int kinky::utils::count(string& str, char c) {
	if (str.length() == 0) {
		return 0;
	}

	int ret = 0;
	size_t pos = -1;
	while ((pos = str.find(c, pos + 1)) != string::npos) {
		ret++;
	}
	return ret;
}

void kinky::utils::replaceAll(string& str, string find, string replace) {
	if (str.length() == 0) {
		return;
	}

	size_t start = 0;

	while ((start = str.find(find, start)) != string::npos) {
		str.replace(start, find.size(), replace);
		start += replace.length();
	}
}

void kinky::utils::extractRegionFromAcceptLanguage(string accept, string* locale) {
	string line(accept.data());

	size_t index = line.find(",");
	if (index != string::npos) {
		line.assign(line.substr(0, index));
	}
	if ((index = line.find(";")) != string::npos) {
		line.assign(line.substr(0, index));
	}
	if ((index = line.find("-")) != string::npos) {
		line.assign(line.substr(index + 1));
		std::transform(line.begin(), line.end(), line.begin(), ::tolower);
	}
	else if ((index = line.find("_")) != string::npos) {
		line.assign(line.substr(index + 1));
		std::transform(line.begin(), line.end(), line.begin(), ::tolower);
	}

	if (line == "en") {
		locale->assign("us");
	}
	else {
		locale->assign(line);
	}
}

void kinky::utils::extractLocaleFromAcceptLanguage(string accept, string* locale) {
	string line(accept.data());

	size_t index = line.find(",");
	if (index != string::npos) {
		line.assign(line.substr(0, index));
	}
	if ((index = line.find(";")) != string::npos) {
		line.assign(line.substr(0, index));
	}
	kinky::utils::replaceAll(line, "-", "_");

	if (line.find("_") == string::npos) {
		line.assign("all_ALL");
	}

	locale->assign(line);
}

void kinky::utils::process_mem_usage(double& vm_usage, double& resident_set) {
	using std::ios_base;
	using std::ifstream;
	using std::string;

	vm_usage = 0.0;
	resident_set = 0.0;

	// 'file' stat seems to give the most reliable results
	//
	ifstream stat_stream("/proc/self/stat", ios_base::in);

	// dummy vars for leading entries in stat that we don't care about
	//
	string pid, comm, state, ppid, pgrp, session, tty_nr;
	string tpgid, flags, minflt, cminflt, majflt, cmajflt;
	string utime, stime, cutime, cstime, priority, nice;
	string O, itrealvalue, starttime;

	// the two fields we want
	//
	unsigned long vsize;
	long rss;

	stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
			>> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
			>> utime >> stime >> cutime >> cstime >> priority >> nice
			>> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

	stat_stream.close();

	long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
	vm_usage = vsize / 1024.0;
	resident_set = rss * page_size_kb;
}

void kinky::utils::prettify_header_name(string& name) {
	std::transform(name.begin(), name.begin() + 1, name.begin(), ::toupper);

	stringstream iss;
	iss << name;

	char line[256];
	size_t pos = 0;
	while (iss.good()) {
		iss.getline(line, 256, '-');
		pos += iss.gcount();
		std::transform(name.begin() + pos, name.begin() + pos + 1, name.begin() + pos, ::toupper);
	}
}

#ifdef USE_SPARSEHASH
google::sparse_hash_map<string, kinky::memory::managed_ptr<string>*>* kinky::utils::strip_cookie(string cookieStr, google::sparse_hash_map<string, managed_ptr<string>*>* cookie) {
	if (cookie == NULL) {
		cookie = new google::sparse_hash_map<string, managed_ptr<string>*>();
	}
#else
kinky::memory::str_map<kinky::memory::managed_ptr<string>*>* kinky::utils::strip_cookie(string cookieStr, str_map<managed_ptr<string>*>* cookie) {
	if (cookie == NULL) {
		cookie = new str_map<managed_ptr<string>*>();
	}
#endif

	stringstream ss(cookieStr);
	string item;
	size_t separator = 0;

	while (std::getline(ss, item, ';')) {
		separator = item.find("=");
		if (separator != string::npos) {
			string name;
			ostringstream oss;
			kinky::utils::url_decode(item.substr(0, separator), oss);
			name = string(oss.str());
			kinky::utils::trim(name);
			oss.str("");

			kinky::utils::url_decode(item.substr(separator + 1), oss);
			string* value = new string(oss.str());
			kinky::utils::trim(*value);

#ifdef USE_SPARSEHASH
			cookie->insert(pair<string, managed_ptr<string>*>(name, new managed_ptr<string>(value)));
#else
			cookie->insert(name, new managed_ptr<string>(value));
#endif
		}
	}
	return cookie;
}

#ifdef USE_SPARSEHASH
google::sparse_hash_map<string, string*>* kinky::utils::strip_object_cookie(string cookieStr, google::sparse_hash_map<string, string*>* cookie) {
	if (cookie == NULL) {
		cookie = new google::sparse_hash_map<string, string*>();
	}
	stringstream ss(cookieStr);
	string item;
	size_t separator = 0;

	while (std::getline(ss, item, ';')) {
		separator = item.find("=");
		if (separator != string::npos) {
			string name;
			ostringstream oss;

			kinky::utils::url_decode(item.substr(0, separator), oss);
			name = string(oss.str());
			kinky::utils::trim(name);
			oss.str("");

			kinky::utils::url_decode(item.substr(separator + 1), oss);
			string* value = new string(oss.str());
			kinky::utils::trim(*value);

			cookie->insert(pair<string, string*>(name, value));
		}
	}
	return cookie;
}
#endif
