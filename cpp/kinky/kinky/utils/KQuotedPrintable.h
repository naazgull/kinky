#pragma once

#include <sstream>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {
	namespace utils {
		namespace mail {
				string quoted_printable_encode(string toQuote, string charset);
		}
	}
}
