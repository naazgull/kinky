#include <kinky/utils/KSendMail.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

int kinky::utils::mail::send(KObject* mail) {
	char errorBuffer[CURL_ERROR_SIZE];
	string buffer;
	CURL* curl = curl_easy_init();
	CURLcode result;
	ostringstream oss;
	ostringstream ossfrom;
	ostringstream ossto;

	ossfrom.str("");
	if (!!(*mail)["from_name"]) {
		ossfrom << "\"" << mail->get("from_name") << "\"";
	}
	ossfrom << "<" << mail->get("from") << ">" << flush;

	struct curl_slist *recipients = NULL;
	KObject* to = mail->getArray("recipients");
	bool first = true;
	for (KObject::KObjAttributes::iterator it = to->begin(); it != to->end(); it++) {
		if (!first) {
			ossto << ", ";
		}
		if (!!(**it)["to_name"]) {
			ossto << "\"" << (string) (**it)["to_name"] << "\"";
		}
		ossto << "<" << (**it)["to"] << ">" << flush;

		first = false;
		recipients = curl_slist_append(recipients, ((string) (**it)["to"]).data());
	}

	oss.str("");
	oss << "Date: " << kinky::utils::date::format("%a, %d %b %Y %X %Z") << CRLF << flush;
	oss << "From: " << ossfrom.str() << CRLF << flush;
	oss << "Sender: " << ossfrom.str() << CRLF << flush;
	oss << "Subject: " << kinky::utils::mail::quoted_printable_encode(mail->get("subject"), "utf-8") << CRLF << flush;
	if (!!(*mail)["reply_to"]) {
		oss << "Reply-To: ";
		if (!!(*mail)["reply_to_name"]) {
			oss << "\"" << mail->get("reply_to_name") << "\"";
		}
		oss << "<" << mail->get("reply_to") << ">" << CRLF << flush;

	}
	else {
		oss << "Reply-To: " << ossfrom.str() << CRLF << flush;
	}
	oss << "To: " << ossto.str() << CRLF << flush;
	//oss << "Content-Type: text/html; charset=\"utf-8\"" << CRLF << CRLF << flush;

	ostringstream bss;
	timeval tv;
	gettimeofday (&tv, NULL);
	bss << "===============" << time(NULL) << tv.tv_usec << "==" << flush;
	string boundary(bss.str());

	oss << "Content-Type: multipart/alternative; boundary=\"" << boundary << "\"" << CRLF << CRLF << flush;

	oss << "--" << boundary << CRLF << flush;
	oss << "Content-Type: text/plain; charset=\"utf-8\"" << CRLF << flush;
	oss << "MIME-Version: 1.0" << CRLF << flush;
	oss << "Content-Transfer-Encoding: 8bit" << CRLF << CRLF << flush;
	oss << (!(*mail)["body_text_plain"] ? "<no plain text message>" : mail->get("body_text_plain")) << CRLF << CRLF << flush;

	oss << "--" << boundary << CRLF << flush;
	oss << "Content-Type: text/html; charset=\"utf-8\"" << CRLF << flush;
	oss << "MIME-Version: 1.0" << CRLF << flush;
	oss << "Content-Transfer-Encoding: 8bit" << CRLF << CRLF << flush;
	oss << string(*mail->getString("body")) << CRLF << flush;
	oss << "--" << boundary << "--" << CRLF << flush;
	oss << "." << CRLF << flush;

	string* bodyStr = new string(oss.str());

	OpenSSL_add_all_algorithms();
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_URL, mail->get("smtp_server").data());
	curl_easy_setopt(curl, CURLOPT_MAIL_FROM, mail->get("from").data());
	curl_easy_setopt(curl, CURLOPT_MAIL_RCPT, recipients);
	curl_easy_setopt(curl, CURLOPT_READDATA, bodyStr);
	curl_easy_setopt(curl, CURLOPT_READFUNCTION, curlReader);
	curl_easy_setopt(curl, CURLOPT_INFILESIZE, bodyStr->length());
	curl_easy_setopt(curl, CURLOPT_UPLOAD, 1);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
	curl_easy_setopt(curl, CURLOPT_HEADER, 0);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriter);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	if (!!(*mail)["smtp_user"] && !!(*mail)["smtp_password"]) {
		curl_easy_setopt(curl, CURLOPT_USERNAME, ((string) (*mail)["smtp_user"]).data());
		curl_easy_setopt(curl, CURLOPT_PASSWORD, ((string) (*mail)["smtp_password"]).data());
	}
	if (!!(*mail)["smtp_use_ssl"] && (string) (*mail)["smtp_use_ssl"] == "yes") {
		curl_easy_setopt(curl, CURLOPT_USE_SSL, (long) CURLUSESSL_ALL);
	}

	result = curl_easy_perform(curl);
	delete bodyStr;
	//delete mail;

	if (recipients != NULL) {
		curl_slist_free_all(recipients);
	}
	curl_easy_cleanup(curl);
	EVP_cleanup();

	if (result != CURLE_OK) {
		throw new KRestException(result, errorBuffer);
	}

	return 0;
}

size_t kinky::utils::mail::curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer) {
	size_t result = 0;

	if (buffer != NULL) {
		buffer->append(data, size * nmemb);
		result = size * nmemb;
	}

	return result;
}

size_t kinky::utils::mail::curlReader(void *ptr, size_t size, size_t nmemb, void *userdata) {
	if (userdata != NULL) {
		string* buffer = static_cast<string*>(userdata);

		if (buffer != NULL && buffer->length() != 0) {
			size_t result = buffer->length() > size * nmemb ? size * nmemb : buffer->length();
			memcpy(ptr, buffer->c_str(), result);

			buffer->erase(0, result);

			return result;
		}
	}
	return 0;
}
