#pragma once

#include <string>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {
	namespace utils {
		namespace KUrlEncodeRfc3986 {
				string encode(const std::string &c);

				string char2hex(char dec);
		}
	}
}

