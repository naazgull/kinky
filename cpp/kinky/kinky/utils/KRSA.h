

#ifndef KRSA_H_
#define KRSA_H_

#include <string.h>
#include <string>
#include <stdint.h>
#include <iostream>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>

namespace kinky {

	namespace utils {

		namespace encryption {

			class KRSA {
				public:
					static string pad(string decrypted, size_t keySize, char delimiter) {
						ostringstream oss("");
						int paddingSize = -1;
						oss << decrypted << flush;

						if (oss.str().length() < keySize) {
							paddingSize = keySize;
						}
						else if (oss.str().length() > keySize && oss.str().length() < keySize * 2) {
							paddingSize = keySize * 2;
						}
						if (paddingSize != -1) {
							oss << delimiter << flush;
							for (int i = oss.str().length(); i != paddingSize; i++) {
								oss << "_" << flush;
							}
						}
						return oss.str().data();
					}

					static pair<unsigned char*, size_t>* encrypt(string decrypted, size_t decryptedSize, string privateKeyFile) {
						unsigned char* encrypted = new unsigned char[decryptedSize * 2];
						bzero(encrypted, decryptedSize * 2);
						size_t bufSize;

						FILE *keyfile = fopen(privateKeyFile.data(), "r");
						RSA *rsa = PEM_read_RSAPrivateKey(keyfile, NULL, NULL, NULL);
						if (rsa == NULL) {
							string err = ERR_error_string(ERR_get_error(), NULL);
//							fclose(keyfile);
							ostringstream oss;
							oss << "problem during data encryption: " << err << flush;
							throw new KRestException(500, oss.str().data());
						}

						bufSize = RSA_private_encrypt((int) decryptedSize, (const unsigned char*) decrypted.data(), encrypted, rsa, RSA_PKCS1_PADDING);
						if (bufSize == (size_t) -1) {
							string err = ERR_error_string(ERR_get_error(), NULL);
//							fclose(keyfile);
//							RSA_free(rsa);
							ostringstream oss;
							oss << "problem during data encryption: " << err << flush;
							throw new KRestException(500, oss.str().data());
						}

//						RSA_free(rsa);
//						fclose(keyfile);

						(*kinky::io::out) << encrypted << endl << "*************************************************************************" << endl;

						return new pair<unsigned char*, size_t> (encrypted, bufSize);
					}

					static pair<string, size_t>* decrypt(unsigned char* encrypted, size_t encryptedSize, string privateKeyFile) {
						(*kinky::io::out) << encrypted << endl << "*************************************************************************" << endl;

						unsigned char* decrypted = new unsigned char[encryptedSize * 2];
						bzero(decrypted, encryptedSize * 2);
						size_t bufSize;

						FILE *keyfile = fopen(privateKeyFile.data(), "r");
						RSA *rsa = PEM_read_RSAPrivateKey(keyfile, NULL, NULL, NULL);
						if (rsa == NULL) {
							string err = ERR_error_string(ERR_get_error(), NULL);
//							fclose(keyfile);
							ostringstream oss;
							oss << "problem during data decryption: " << err << flush;
							throw new KRestException(500, oss.str().data());
						}

						bufSize = RSA_public_decrypt(encryptedSize, encrypted, decrypted, rsa, RSA_PKCS1_PADDING);
						if (bufSize == (size_t) -1) {
							string err = ERR_error_string(ERR_get_error(), NULL);
//							fclose(keyfile);
							RSA_free(rsa);
							ostringstream oss;
							oss << "problem during data decryption: " << err << flush;
							throw new KRestException(500, oss.str().data());
						}

						RSA_free(rsa);

						return new pair<string, size_t> ((char*) decrypted, bufSize);
					}

			};

		}

	}

}

#endif /* KRSA_H_ */
