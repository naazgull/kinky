#pragma once

#include <time.h>
#include <sys/time.h>
#include <string>
#include <kinky/memory/KMemoryManager.h>

#define K_SECONDS 1
#define K_MINUTES 2
#define K_HOURS 3
#define K_DAYS 4
#define K_WEEKS 5
#define K_MONTHS 6
#define K_YEARS 7

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace memory;

	namespace utils {

		namespace date {

				time_t parse(string date, string strFormat);

				string format(string strFormat);

				string format(string strFormat, time_t rawtime);

				string format(string strFormat, const int delta[], int length);

				string formatGMT(string strFormat);

				string formatGMT(string strFormat, time_t rawtime);

				string formatGMT(string strFormat, const int delta[], int length);

				time_t timezoneOffset();


				extern struct timezone current_tz;
		}

	}

}
