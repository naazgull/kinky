
#define KEXCEPTION_NOT_ENOUGH_SCOPES "The provided access token doesn't have the necessary scopes to execute this operation.", 700
#define KEXCEPTION_USER_DOESNT_HAVE_ACCESS "The provided user doesn't have the necessary permissions to access the requested data in this operation.", 701

#define KEXCEPTION_UNKNOW_ERROR(e) string("An unkonw error was caught while executing this operation: ") + string(e), 800

#define KEXCEPTION_RESOURCE_OPERATION_NOT_FOUND "The requested resource was not found on this backend.", 900
#define KEXCEPTION_RESOURCE_NOT_FOUND "The requested resource was not found by this operation.", 901
#define KEXCEPTION_INVALID_FB_TOKEN string("The provided Facebook access token is invalid or has expired."), 902
#define KEXCEPTION_NO_ACCESS_TOKEN "A valid access token must be provided to access this operation", 903
#define KEXCEPTION_INVALID_METHOD "The provided HTTP method can't be used with this operation.", 904
#define KEXCEPTION_INVALID_GOOGLE_TOKEN string("The provided Google access token is invalid or has expired."), 905
#define KEXCEPTION_INVALID_GOOGLE_SESSION_DATA string("The provided Google session data is invalid or has expired."), 906
#define KEXCEPTION_GOOGLE_OPENID_ACCESS_DENIED "The user refused access to the Google Open IDaccount.", 907
#define KEXCEPTION_FACEBOOK_ACCESS_DENIED "The user refused access to the Facebook account.", 908
#define KEXCEPTION_GOOGLE_OAUTH2_ACCESS_DENIED "The user refused access to the Google OAuth2.0 account.", 909
#define KEXCEPTION_TWITTER_ACCESS_DENIED "The user refused access to the Twitter account.", 910
#define KEXCEPTION_YAHOO_ACCESS_DENIED "The user refused access to the Yahoo account.", 917
#define KEXCEPTION_FLICKR_ACCESS_DENIED "The user refused access to the Flickr account.", 918
#define KEXCEPTION_TUMBLR_ACCESS_DENIED "The user refused access to the Tumblr account.", 919
#define KEXCEPTION_BOX_ACCESS_DENIED "The user refused access to the Box account.", 920
#define KEXCEPTION_COPY_ACCESS_DENIED "The user refused access to the Copy account.", 921
#define KEXCEPTION_DROPBOX_ACCESS_DENIED "The user refused access to the Dropbox account.", 922
#define KEXCEPTION_OAUTH1_ACCESS_DENIED "The user refused access to the OAuth1.0 account.", 923
#define KEXCEPTION_OAUTH2_ACCESS_DENIED "The user refused access to the OAuth2.0 account.", 924
#define KEXCEPTION_INVALID_OAUTH1_SESSION_DATA string("The provided session data is invalid or has expired."), 925
#define KEXCEPTION_NOT_ENOUGH_PARAMETERS "The provided parameters aren't enough to execute this operation.", 926
#define KEXCEPTION_WRONG_PARAMETER_VALUES "The provided parameter values don't comply with this operation specs.", 927
#define KEXCEPTION_UNABLE_TO_EXECUTE_OPERATION "Some requirements are preventing to execute the operation.", 928
#define KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_JPEG "Unable to create image from JPEG.", 929
#define KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_PNG "Unable to create image from PNG.", 930
#define KEXCEPTION_UNABLE_TO_CREATE_IMAGE_FROM_GIF "Unable to create image from GIF.", 931
#define KEXCEPTION_INVALID_METHOD_PUT_NOT_USED_WITH_DOCUMENT "The provided PUT method must be used with a DOCUMENT resource.", 932
#define KEXCEPTION_INVALID_METHOD_NOT_ALLOWED_WITH_CONTROLLER "The provided HTTP method can't be used with a Controller resource.", 933
#define KEXCEPTION_INVALID_METHOD_DELETE_NOT_USED_WITH_DOCUMENT "The provided DELETE method must be used with a DOCUMENT resource.", 934
