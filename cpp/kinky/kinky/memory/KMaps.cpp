#include <kinky/memory/KMaps.h>

unsigned int kinky::memory::djb2(string key) {
    unsigned int hash = 5381;

    for (unsigned int i = 0; i < key.length(); i++)
        hash = ((hash << 5) + hash) + (unsigned int) key[i];

    return hash % HASH_SIZE;
}

void kinky::tostr(string& s, int i){
	char oss[512];
	sprintf(oss,"%i", i);
	s.insert(s.length(), oss);
}

void kinky::tostr(std::string& s, int i, std::ios_base& (&hex)(std::ios_base&)) {
	char oss[512];
	sprintf(oss,"%x", i);
	s.insert(s.length(), oss);
}

void kinky::tostr(string& s, unsigned int i){
	char oss[512];
	sprintf(oss,"%u", i);
	s.insert(s.length(), oss);
}

void kinky::tostr(string& s, size_t i){
	char oss[512];
	sprintf(oss,"%lu", i);
	s.insert(s.length(), oss);
}

void kinky::tostr(string& s, long i){
	char oss[512];
	sprintf(oss,"%ld", i);
	s.insert(s.length(), oss);
}

void kinky::tostr(string& s, long long i){
	char oss[512];
	sprintf(oss,"%lld", i);
	s.insert(s.length(), oss);
}

void kinky::tostr(string& s, float i){
	char oss[512];
	sprintf(oss,"%f", i);
	s.insert(s.length(), oss);
}

void kinky::tostr(string& s, double i){
	char oss[512];
	sprintf(oss,"%lf", i);
	s.insert(s.length(), oss);
}

void kinky::tostr(string& s, char i){
	char oss[512];
	sprintf(oss,"%c", i);
	s.insert(s.length(), oss);
}

void kinky::fromstr(string& s, int* i){
	sscanf(s.data(),"%i", i);
}

void kinky::fromstr(string& s, unsigned int* i){
	sscanf(s.data(),"%u", i);
}

void kinky::fromstr(string& s, size_t* i){
	sscanf(s.data(),"%lu", i);
}

void kinky::fromstr(string& s, long* i){
	sscanf(s.data(),"%ld", i);
}

void kinky::fromstr(string& s, long long* i){
	sscanf(s.data(),"%lld", i);
}

void kinky::fromstr(string& s, float* i){
	sscanf(s.data(),"%f", i);
}

void kinky::fromstr(string& s, double* i){
	sscanf(s.data(),"%lf", i);
}

void kinky::fromstr(string& s, char* i){
	sscanf(s.data(),"%c", i);
}

