#pragma once

#include <string.h>
#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <stdio.h>

#define HASH_SIZE 50

using namespace std;
using namespace __gnu_cxx;

namespace kinky {
	void tostr(string& s, int i);
	void tostr(std::string&, int, std::ios_base& (&)(std::ios_base&));
	void tostr(string& s, unsigned int i);
	void tostr(string& s, size_t i);
	void tostr(string& s, long i);
	void tostr(string& s, long long i);
	void tostr(string& s, float i);
	void tostr(string& s, double i);
	void tostr(string& s, char i);

	void fromstr(string& s, int* i);
	void fromstr(string& s, unsigned int* i);
	void fromstr(string& s, size_t* i);
	void fromstr(string& s, long* i);
	void fromstr(string& s, long long* i);
	void fromstr(string& s, float* i);
	void fromstr(string& s, double* i);
	void fromstr(string& s, char* i);

	namespace memory {

		unsigned int djb2(string key);

		template<class K, class V>
		class duo {
			public:
				duo(K k, V value);
				virtual ~duo();

			public:
				K* _first;
				V second;
				size_t third;
				K& first;
		};

		template<class V>
		bool _compare(duo<string, V>* lh, duo<string, V>* rh);

		template<class V>
		class str_map {
			public:
				str_map(size_t hash_size = HASH_SIZE);
				virtual ~str_map();

				typedef typename vector<vector<duo<string, V>*>*>::iterator hashtable_iterator;
				typedef typename vector<duo<string, V>*>::iterator bucket_iterator;
				typedef bucket_iterator hash_iterator;

				class iterator: public std::iterator<std::random_access_iterator_tag, duo<string, V>*> {

					public:
						inline iterator() {
						}
						inline iterator(const iterator& other) :
							_underlying(other._underlying), _idx(other._idx) {
						}
						inline iterator(iterator& other) :
							_underlying(other._underlying), _idx(other._idx) {
						}
						inline iterator(vector<duo<string, V>*>* other, size_t pos = 0) :
							_underlying(other), _idx(pos) {
						}
						inline ~iterator() {
						}

						inline iterator& operator++() {
							++this->_idx;
							return *this;
						}

						inline iterator operator++(int n) {
							iterator tmp(*this);
							operator++();
							return tmp;
						}

						inline iterator& operator--() {
							--this->_idx;
							return *this;
						}

						inline iterator operator--(int n) {
							iterator tmp(*this);
							operator--();
							return tmp;
						}

						inline iterator& operator+(size_t n) {
							this->_idx += n;
							return *this;
						}

						inline iterator& operator-(size_t n) {
							this->_idx -= n;
							return *this;
						}

						inline bool operator==(const iterator& rhs) {
							return this->_underlying == rhs._underlying && this->_idx == rhs._idx;
						}

						inline bool operator!=(const iterator& rhs) {
							return this->_underlying != rhs._underlying || this->_idx != rhs._idx;
						}

						inline duo<string, V>* operator*() {
							return *(this->_underlying->begin() + this->_idx);
						}

						inline duo<string, V>* operator->() {
							return *(this->_underlying->begin() + this->_idx);
						}

					private:
						vector<duo<string, V>*>* _underlying;
						size_t _idx;

				};

				class ordered_iterator: public std::iterator<std::random_access_iterator_tag, V> {

					public:
						inline ordered_iterator() {
						}
						inline ordered_iterator(const ordered_iterator& other) :
							_underlying(other._underlying), _idx(other._idx) {
						}
						inline ordered_iterator(ordered_iterator& other) :
							_underlying(other._underlying), _idx(other._idx) {
						}
						inline ordered_iterator(vector<duo<string, V>*>* other, size_t pos = 0) :
							_underlying(other), _idx(pos) {
						}
						inline ~ordered_iterator() {
						}

						inline ordered_iterator& operator++() {
							++this->_idx;
							return *this;
						}

						inline ordered_iterator operator++(int n) {
							ordered_iterator tmp(*this);
							operator++();
							return tmp;
						}

						inline ordered_iterator& operator--() {
							--this->_idx;
							return *this;
						}

						inline ordered_iterator operator--(int n) {
							ordered_iterator tmp(*this);
							operator--();
							return tmp;
						}

						inline ordered_iterator& operator+(size_t n) {
							this->_idx += n;
							return *this;
						}

						inline ordered_iterator& operator-(size_t n) {
							this->_idx -= n;
							return *this;
						}

						inline bool operator==(const ordered_iterator& rhs) {
							return this->_underlying == rhs._underlying && this->_idx == rhs._idx;
						}

						inline bool operator!=(const ordered_iterator& rhs) {
							return this->_underlying != rhs._underlying || this->_idx != rhs._idx;
						}

						inline V& operator*() {
							duo<string, V>* d = *(this->_underlying->begin() + this->_idx);
							return d->second;
						}

						inline V* operator->() {
							return &((*(this->_underlying->begin() + this->_idx))->second);
						}

					private:
						vector<duo<string, V>*>* _underlying;
						size_t _idx;

				};

				void set_deleted_key(string k);
				void clear();
				void resize(size_t k);

				iterator begin();
				iterator end();

				iterator begin_by_key();
				iterator end_by_key();

				ordered_iterator begin_by_value();
				ordered_iterator end_by_value();

				V& at(size_t idx);
				iterator find(string key);
				void erase(iterator);
				void insert(pair<string, V> v);
				void insert(pair<size_t, V> v);
				void insert(string k, V v);
				void insert(size_t k, V v);

				void sort();
				void move(string name, int offset);
				void assign(kinky::memory::str_map<V>& other);
				void assign(kinky::memory::str_map<V>* other);

				size_t size();

				V& operator[](int idx);
				V& operator[](string name);

				inline friend ostream& operator<<(ostream& os, iterator& f) {
					os << "iterator pointing to " << f->second;
					return os;
				}

				inline friend ostream& operator<<(ostream& os, ordered_iterator& f) {
					os << "iterator pointing to " << *f;
					return os;
				}

				inline friend ostream& operator<<(ostream& os, str_map<V>& f) {
					f.print(os);
					return os;
				}

				void print(ostream& os = cout);

			private:
				vector<duo<string, V>*>** _hash_table;
				vector<duo<string, V>*>* _hash_values;
				size_t _hash_size;

				void _rellocate(duo<string, V>* v, size_t offset);
				duo<string, V>* _at(size_t idx);
				hash_iterator _find(string key);
		};

	}
}

template<class K, class V>
kinky::memory::duo<K, V>::duo(K kp, V vp) : _first(new K(kp)), second(vp), third(-1), first(*_first) {
}

template<class K, class V>
kinky::memory::duo<K, V>::~duo() {
	delete this->_first;
}

template<class V>
kinky::memory::str_map<V>::str_map(size_t hash_size) :
	_hash_table(new vector<duo<string, V>*>*[hash_size]), _hash_values(new vector<duo<string, V>*>()), _hash_size(hash_size) {
	bzero(this->_hash_table, this->_hash_size * sizeof(vector<duo<string, V>*>*));
}

template<class V>
kinky::memory::str_map<V>::~str_map() {
	for (size_t i = 0; i != this->_hash_size; i++) {
		if (this->_hash_table[i] == NULL) {
			continue;
		}
		delete this->_hash_table[i];
	}
	for (kinky::memory::str_map<V>::bucket_iterator k = this->_hash_values->begin(); k != this->_hash_values->end(); k++) {
		delete *k;
	}
	delete[] this->_hash_table;
	delete this->_hash_values;
}

template<class V>
void kinky::memory::str_map<V>::set_deleted_key(string k) {
}

template<class V>
void kinky::memory::str_map<V>::clear() {
}

template<class V>
void kinky::memory::str_map<V>::resize(size_t k) {
}

template<class V>
typename kinky::memory::str_map<V>::iterator kinky::memory::str_map<V>::begin() {
	return iterator(this->_hash_values);
}

template<class V>
typename kinky::memory::str_map<V>::iterator kinky::memory::str_map<V>::end() {
	return iterator(this->_hash_values, this->_hash_values->size());
}

template<class V>
typename kinky::memory::str_map<V>::iterator kinky::memory::str_map<V>::begin_by_key() {
	return iterator(this->_hash_values);
}

template<class V>
typename kinky::memory::str_map<V>::iterator kinky::memory::str_map<V>::end_by_key() {
	return iterator(this->_hash_values, this->_hash_values->size());
}

template<class V>
typename kinky::memory::str_map<V>::ordered_iterator kinky::memory::str_map<V>::begin_by_value() {
	return ordered_iterator(this->_hash_values);
}

template<class V>
typename kinky::memory::str_map<V>::ordered_iterator kinky::memory::str_map<V>::end_by_value() {
	return ordered_iterator(this->_hash_values, this->_hash_values->size());
}

template<class V>
void kinky::memory::str_map<V>::insert(pair<string, V> v) {
	this->insert(v.first, v.second);
}

template<class V>
void kinky::memory::str_map<V>::insert(pair<size_t, V> v) {
	this->insert(v.first, v.second);
}

template<class V>
void kinky::memory::str_map<V>::insert(string k, V v) {
	unsigned int hash = kinky::memory::djb2(k);

	vector<duo<string, V>*>* bucket = this->_hash_table[hash];
	if (bucket != NULL) {
		for (kinky::memory::str_map<V>::bucket_iterator kt = bucket->begin(); kt != bucket->end(); kt++) {
			if ((*kt)->first == k) {
				(*kt)->second = v;
				return;
			}
		}
	}
	else {
		bucket = new vector<duo<string, V>*>();
		this->_hash_table[hash] = bucket;
	}

	duo<string, V>* d = new duo<string, V>(k, v);
	bucket->push_back(d);
	d->third = this->_hash_values->size();
	this->_hash_values->push_back(d);
}

template<class V>
void kinky::memory::str_map<V>::insert(size_t k, V v) {
	string oss;
	kinky::tostr(oss, k);
	duo<string, V>* d = new duo<string, V>(oss, v);
	unsigned int hash = kinky::memory::djb2(d->first);

	vector<duo<string, V>*>* bucket = this->_hash_table[hash];
	if (bucket == NULL) {
		bucket = new vector<duo<string, V>*>();
		this->_hash_table[hash] = bucket;
	}
	bucket->push_back(d);
	d->third = k;

	size_t init_pos = this->size() - 1;
	for (size_t it = init_pos; it >= k; --it) {
		duo<string, V>* val = this->_hash_values->at(it);
		this->_rellocate(val, 1);
	}

	this->_hash_values->insert(this->_hash_values->begin() + k, d);
}

template<class V>
V& kinky::memory::str_map<V>::at(size_t idx) {
	return this->_hash_values->at(idx)->second;
}

template<class V>
typename kinky::memory::str_map<V>::iterator kinky::memory::str_map<V>::find(string key) {
	unsigned int hash = kinky::memory::djb2(key);

	vector<duo<string, V>*>* bucket = this->_hash_table[hash];
	if (bucket == NULL) {
		return this->end();
	}

	for (kinky::memory::str_map<V>::bucket_iterator k = bucket->begin(); k != bucket->end(); k++) {
		if ((*k)->first == key) {
			return this->begin() + (*k)->third;
		}
	}

	return this->end();
}

template<class V>
void kinky::memory::str_map<V>::erase(kinky::memory::str_map<V>::iterator i) {
	unsigned int hash = kinky::memory::djb2(i->first);

	vector<duo<string, V>*>* bucket = this->_hash_table[hash];
	if (bucket == NULL) {
		delete *i;
		return;
	}

	for (kinky::memory::str_map<V>::bucket_iterator k = bucket->begin(); k != bucket->end(); k++) {
		if ((*k)->first == i->first) {
			bucket->erase(k);
			break;
		}
	}

	size_t pos = i->third;
	delete *i;
	this->_hash_values->erase(this->_hash_values->begin() + pos);
	for (size_t k = 0; k != this->_hash_values->size(); k++) {
		this->_hash_values->at(k)->third = k;
	}
}

template<class V>
size_t kinky::memory::str_map<V>::size() {
	return this->_hash_values->size();
}

template<class V>
void kinky::memory::str_map<V>::sort() {
	std::sort(this->_hash_values->begin(), this->_hash_values->end(), kinky::memory::_compare<V>);
	for (size_t k = 0; k != this->_hash_values->size(); k++) {
		this->_hash_values->at(k)->third = k;
	}

}

template<class V>
void kinky::memory::str_map<V>::move(string name, int offset) {
	unsigned int hash = kinky::memory::djb2(name);

	vector<duo<string, V>*>* bucket = this->_hash_table[hash];
	if (bucket == NULL) {
		return;
	}

	duo<string, V>* val = NULL;
	for (kinky::memory::str_map<V>::bucket_iterator k = bucket->begin(); k != bucket->end(); k++) {
		if ((*k)->first == name) {
			val = *k;
			break;
		}
	}
	if (val == NULL) {
		return;
	}

	size_t step = 0;
	if (offset < 0) {
		step = -1;
	}
	else {
		step = 1;
	}
	for (size_t it = val->third + step; it != val->third + offset; it = it + step) {
		duo<string, V>* other = this->_hash_values->at(it);
		this->_rellocate(other, step);
	}

	this->_hash_values->erase(this->_hash_values->begin() + val->third);
	val->third += offset;
	this->_hash_values->insert(this->_hash_values->begin() + val->third, val);
}

template<class V>
void kinky::memory::str_map<V>::assign(kinky::memory::str_map<V>& other) {
}

template<class V>
void kinky::memory::str_map<V>::assign(kinky::memory::str_map<V>* other) {
}

template<class V>
V& kinky::memory::str_map<V>::operator[](int idx) {
	return this->at((size_t) idx);
}

template<class V>
V& kinky::memory::str_map<V>::operator[](string name) {
	return this->find(name)->second;
}

template<class V>
void kinky::memory::str_map<V>::print(ostream& out) {
	out << "str_map<" << ((void*) this) << "> _underlying<" << this->_hash_values << ":" << this->_hash_values->size() << ">" << endl << flush;
	for (size_t k = 0; k != this->_hash_values->size(); k++) {
		out << "\t[" << k << "]<" << this->_hash_values->at(k) << ">" << endl << flush;
	}
}

template<class V>
kinky::memory::duo<string, V>* kinky::memory::str_map<V>::_at(size_t idx) {
	return this->_hash_values->at(idx);
}

template<class V>
typename kinky::memory::str_map<V>::hash_iterator kinky::memory::str_map<V>::_find(string key) {
	unsigned int hash = kinky::memory::djb2(key);

	vector<duo<string, V>*>* bucket = this->_hash_table[hash];
	if (bucket == NULL) {
		return this->_hash_values->end();
	}

	for (kinky::memory::str_map<V>::bucket_iterator k = bucket->begin(); k != bucket->end(); k++) {
		if ((*k)->first == key) {
			return this->_hash_values->begin() + (*k)->third;
		}
	}

	return this->_hash_values->end();
}

template<class V>
bool kinky::memory::_compare(duo<string, V>* lh, duo<string, V>* rh) {
	return lh->first.compare(rh->first) <= 0;
}

template<class V>
void kinky::memory::str_map<V>::_rellocate(duo<string, V>* val, size_t offset) {
	unsigned int hash = kinky::memory::djb2(val->first);

	vector<duo<string, V>*>* bucket = this->_hash_table[hash];
	for (kinky::memory::str_map<V>::bucket_iterator kt = bucket->begin(); kt != bucket->end(); kt++) {
		if ((*kt)->first == val->first) {
			bucket->erase(kt);
			break;
		}
	}

	val->third += offset;
	kinky::tostr(*val->_first, val->third);
	hash = kinky::memory::djb2(val->first);

	bucket = this->_hash_table[hash];
	if (bucket == NULL) {
		bucket = new vector<duo<string, V>*>();
		this->_hash_table[hash] = bucket;
	}
	bucket->push_back(val);
}

