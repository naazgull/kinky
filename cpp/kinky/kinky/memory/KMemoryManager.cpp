#include <kinky/memory/KMemoryManager.h>

#include <gperftools/malloc_extension.h>
#include <kinky/log/KLogThread.h>

pthread_key_t kinky::memory::k_mem_key;

#define KMEMORY_HASH_SIZE 100

// >>>
// abstract_manager_ptr Namespace
//
//
//

kinky::memory::abstract_managed_ptr::abstract_managed_ptr() throw () {
}

kinky::memory::abstract_managed_ptr::~abstract_managed_ptr() {

}

kinky::memory::abstract_managed_ptr::operator void*() {
	return this->pointed();
}

void kinky::memory::abstract_managed_ptr::release() {
}

void kinky::memory::abstract_managed_ptr::destroy() {
}

void kinky::memory::abstract_managed_ptr::unlink() {
}

bool kinky::memory::abstract_managed_ptr::protect() {
	return false;
}

void kinky::memory::abstract_managed_ptr::protect(bool flag) {
}

void* kinky::memory::abstract_managed_ptr::pointed() {
	return NULL;
}

void kinky::memory::abstract_managed_ptr::prettify(ostream& oss) {
	oss << "*********************************" << endl << "ADDRESS: " << this << endl << "CONTENT ADDRESS: " << this->pointed() << endl << "*********************************";
}
//
//
// <<<

// >>>
// KMemory Class
//
//
//
kinky::memory::KMemory::KMemory() :
		pointees(new KMemPointees()), pointers(new KMemPointers()), data(NULL) {
	string keypointees = "null";
	this->pointees->set_deleted_key(keypointees);
	string keypointers = "null";
	this->pointers->set_deleted_key(keypointers);
	this->data = NULL;
	this->currentip = new string();
}

kinky::memory::KMemory::~KMemory() {
	for (KMemPointers::iterator it = this->pointers->begin(); it != this->pointers->end(); it++) {
		delete it->second;
	}
	this->pointees->clear();
	this->pointees->resize(0);
	delete this->pointees;
	this->pointers->clear();
	this->pointers->resize(0);
	delete this->pointers;
	delete this->currentip;
}

void kinky::memory::KMemory::addPointer(void* pointee, abstract_managed_ptr* pointer) {
	ostringstream oss;
	oss << pointee << flush;
	string key(oss.str());

	oss.str("");
	oss << pointer << flush;
	string pointerAddr(oss.str());

	KMemPointees::iterator att = this->pointees->find(key);
	if (att != this->pointees->end()) {
		att->second++;
	}
	else {
		this->pointees->insert(pair<string, int>(key, 1));
	}

	this->pointers->insert(pair<string, abstract_managed_ptr*>(pointerAddr, pointer));
}

void kinky::memory::KMemory::releasePointee(void* pointee) {
	ostringstream oss;

	if (pointee != NULL) {
		oss.str("");
		oss << pointee << flush;
		string key(oss.str());
		KMemPointees::iterator att = this->pointees->find(key);

		if (att != this->pointees->end()) {
			this->pointees->erase(att);
		}
	}
}

void kinky::memory::KMemory::releasePointer(void* pointee, abstract_managed_ptr* pointer) {
	if (pointee != NULL) {
		ostringstream oss;
		oss << pointee << flush;
		string key(oss.str());
		KMemPointees::iterator att = this->pointees->find(key);

		if (att != this->pointees->end() && att->second != 0) {
			att->second--;
			if (att->second == 0) {
				this->pointees->erase(att);
				pointer->destroy();
			}
		}
	}

	ostringstream oss;
	oss << pointer << flush;
	string pointerAddr(oss.str());
	KMemPointers::iterator ptr = this->pointers->find(pointerAddr);
	if (ptr != this->pointers->end()) {
		this->pointers->erase(ptr);
	}
	delete pointer;
}

void kinky::memory::KMemory::unlinkPointer(void* pointee, abstract_managed_ptr* pointer) {
	ostringstream oss;

	if (pointee != NULL) {
		oss.str("");
		oss << pointee << flush;
		string key(oss.str());
		KMemPointees::iterator att = this->pointees->find(key);

		if (att != this->pointees->end() && att->second != 0) {
			att->second--;
			if (att->second == 0) {
				this->pointees->erase(att);
			}
		}
	}

	oss.str("");
	oss << pointer << flush;
	string pointerAddr(oss.str());
	KMemPointers::iterator ptr = this->pointers->find(pointerAddr);
	if (ptr != this->pointers->end()) {
		this->pointers->erase(ptr);
	}
	delete pointer;
}
int kinky::memory::KMemory::getPointers(void* pointee) {
	if (pointee != NULL) {
		ostringstream oss;
		oss.str("");
		oss << pointee << flush;
		string key(oss.str());
		KMemPointees::iterator att = this->pointees->find(key);

		if (att != this->pointees->end()) {
			return att->second;
		}
	}
	return 0;
}

bool kinky::memory::KMemory::disposed(abstract_managed_ptr* ptr) {
	ostringstream oss;
	oss << ptr << flush;
	string pointerAddr(oss.str());
	return this->pointers->find(pointerAddr) == this->pointers->end();
}

bool kinky::memory::KMemory::nilled(void* ptr) {
	ostringstream oss;
	oss << ptr << flush;
	string key(oss.str());
	return this->pointees->find(key) == this->pointees->end();
}

int kinky::memory::KMemory::gc() {
	unsigned int ret = (unsigned int) pthread_self();
	vector<string> keys;
	for (KMemPointers::iterator it = this->pointers->begin(); it != this->pointers->end(); it++) {
		keys.push_back(string(it->first));
	}
	for (vector<string>::iterator it = keys.begin(); it != keys.end(); it++) {
		if (this->pointers->size() == 0) {
			break;
		}

		KMemPointers::iterator found = this->pointers->find(*it);
		if (found != this->pointers->end()) {
			abstract_managed_ptr* ptr = found->second;
			ptr->release();
		}
	}
	return (int) ret;
}

int** f = new int*[20];

void kinky::memory::KMemory::status() {
#ifdef DEBUG_MEMORY
	char buffer[2048];
	MallocExtension::instance()->GetStats(buffer, 2048);
	cout << buffer << endl << flush;
#endif
}
//
//
// <<<

// >>>
// KMemoryManager Namespace
//
//
//
void kinky::memory::KMemoryManager::init() {
	pthread_key_create(&kinky::memory::k_mem_key, NULL);
}

void kinky::memory::KMemoryManager::alloc() {
	KMemory* kmm = NULL;
	if ((kmm = static_cast<KMemory*>(pthread_getspecific(kinky::memory::k_mem_key))) == NULL) {
		kmm = new KMemory();
		pthread_setspecific(kinky::memory::k_mem_key, kmm);
	}
}

void kinky::memory::KMemoryManager::dealloc() {
	KMemory* kmm = NULL;
	if ((kmm = static_cast<KMemory*>(pthread_getspecific(kinky::memory::k_mem_key))) != NULL) {
		delete kmm;
		pthread_setspecific(kinky::memory::k_mem_key, NULL);
	}
}

kinky::memory::KMemory* kinky::memory::KMemoryManager::getMemory() {
	return static_cast<KMemory*>(pthread_getspecific(kinky::memory::k_mem_key));
}
//
//
// <<<

