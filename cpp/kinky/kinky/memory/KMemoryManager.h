#pragma once

#include <map>
#include <vector>
#include <memory>
#include <iostream>
#include <sstream>
#include <string>
#ifdef SYSTEM_SPARSEHASH
#include <google/sparse_hash_map>
#else
#include <sparsehash/sparse_hash_map>
#endif

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	namespace memory {

		class abstract_managed_ptr {
			public:
				abstract_managed_ptr() throw ();
				virtual ~abstract_managed_ptr();

				virtual operator void*();

				virtual void release();

				virtual void destroy();

				virtual void* pointed();

				virtual void unlink();

				virtual bool protect();

				virtual void protect(bool flag);

				friend ostream& operator<<(ostream& os, kinky::memory::abstract_managed_ptr& f) {
					f.prettify(os);
					return os;
				}

				virtual void prettify(ostream& oss);
		};

		extern pthread_key_t k_mem_key;

		class KMemory {

			public:
				typedef google::sparse_hash_map<string, int> KMemPointees;
				typedef google::sparse_hash_map<string, abstract_managed_ptr*> KMemPointers;

				KMemory();
				virtual ~KMemory();

				void addPointer(void* pointee, abstract_managed_ptr* pointer);

				void releasePointee(void* pointee);

				void releasePointer(void* pointee, abstract_managed_ptr* pointer);

				void unlinkPointer(void* pointee, abstract_managed_ptr* pointer);

				int getPointers(void* pointee);

				bool disposed(abstract_managed_ptr* ptr);

				bool nilled(void* ptr);

				int gc();

				void status();

			private:
				KMemPointees* pointees;
				KMemPointers* pointers;

			public:
				void* data;
				string* currentip;
		};

		namespace KMemoryManager {

			void init();

			void alloc();

			void dealloc();

			KMemory* getMemory();
		}

		template<class T>
		class managed_ptr: public abstract_managed_ptr {

			public:
				typedef T element_type;

				managed_ptr(T* pointeeParam) throw ();
				managed_ptr(abstract_managed_ptr* other) throw ();
				virtual ~managed_ptr();

				T& operator*() throw ();

				T* operator->() throw ();

				operator void*();

				operator T*();

				T* get() throw ();

				void release();

				void destroy();

				void unlink();

				void* pointed();

				bool protect();

				void protect(bool flag);

				void prettify(ostream& oss);

			private:
				T* pointee;
				bool isProtected;

		};

	}

}

template<class T>
kinky::memory::managed_ptr<T>::managed_ptr(T* pointeeParam) throw () {
	this->pointee = pointeeParam;
	if (this->pointee != NULL) {
		KMemoryManager::getMemory()->addPointer(this->pointee, this);
	}
#ifdef DEBUG_MANAGED_MEMORY
	(*kinky::io::out) << *this;
#endif
}

template<class T>
kinky::memory::managed_ptr<T>::managed_ptr(abstract_managed_ptr* other) throw () {
	this->pointee = (T*)(other->pointed());
	if (this->pointee != NULL) {
		KMemoryManager::getMemory()->addPointer(this->pointee, this);
	}
#ifdef DEBUG_MANAGED_MEMORY
	(*kinky::io::out) << *this;
#endif
}

template<class T>
kinky::memory::managed_ptr<T>::~managed_ptr() {
}

template<class T>
T& kinky::memory::managed_ptr<T>::operator*() throw () {
	return *this->pointee;
}

template<class T>
T* kinky::memory::managed_ptr<T>::operator->() throw () {
	return this->pointee;
}

template<class T>
kinky::memory::managed_ptr<T>::operator void*() {
	return this->pointee;
}

template<class T>
kinky::memory::managed_ptr<T>::operator T*() {
	return this->pointee;
}

template<class T>
T* kinky::memory::managed_ptr<T>::get() throw () {
	return this->pointee;
}

template<class T>
void kinky::memory::managed_ptr<T>::release() {
	KMemoryManager::getMemory()->releasePointer(this->pointee, this);
}

template<class T>
void kinky::memory::managed_ptr<T>::destroy() {
#ifdef DEBUG_MANAGED_MEMORY
	(*kinky::io::out) << "DELETING: " << pthread_self() << "/" << this->pointee << endl << flush;
#endif
	delete this->pointee;
	this->pointee = NULL;
}

template<class T>
void kinky::memory::managed_ptr<T>::unlink() {
	KMemoryManager::getMemory()->unlinkPointer(this->pointee, this);
}

template<class T>
void* kinky::memory::managed_ptr<T>::pointed() {
	return this->pointee;
}

template<class T>
bool kinky::memory::managed_ptr<T>::protect() {
	return this->isProtected;
}

template<class T>
void kinky::memory::managed_ptr<T>::protect(bool flag) {
	this->isProtected = flag;
}

template<class T>
void kinky::memory::managed_ptr<T>::prettify(ostream& oss) {
	if (this->pointee != NULL) {
		oss << "ADDR: " << pthread_self() << "/" << this->pointee << "; SRC: " << *this->pointee << endl << flush;
	}
}
