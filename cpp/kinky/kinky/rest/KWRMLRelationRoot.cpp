#include <kinky/rest/KWRMLRelationRoot.h>

kinky::rest::KWRMLRelationRoot::KWRMLRelationRoot(string wrmlURIParam) :
		KRestOperation("^/wrml\\-edit/relations/(.+)$"), wrmlURI(new string(wrmlURIParam.data())) {
}

kinky::rest::KWRMLRelationRoot::~KWRMLRelationRoot() {
	delete this->wrmlURI;
}

kinky::lang::KObject* kinky::rest::KWRMLRelationRoot::getRelation(string url) {
	if (url == "") {
		KObject* result = new KObject();

		ostringstream file;
		file << *(this->wrmlURI) << "/links.json";

		ifstream in(file.str().data());
		KObject* links = NULL;
		try {
			links = KJSON::parse(in);
		}
		catch (KParseException* e) {
			KRestException* toThrow = new KRestException(KHTTPResponse::INTERNAL_SERVER_ERROR, e->what());
			delete e;
			throw toThrow;
		}

		for (KObject::KObjAttributes::iterator it = links->begin(); it != links->end(); it++) {
			string opURL = (**it)["rel"];
			opURL = opURL.substr(opURL.find('/', 2));

			ostringstream fileRel;
			fileRel << *(this->wrmlURI) << opURL << "/relation.json";

			ifstream inRel(fileRel.str().data());
			try {
				KObject* rel = KJSON::parse(inRel);
				(*result) << ((string) (*rel)["name"]) << KOBJECT(
						"href" << (string) (**it)["href"] <<
						"rel" << rel
				);
			}
			catch (KParseException* e) {
				delete e;
			}
		}

		delete links;
		return result;
	}
	else {
		ostringstream file;
		file << *(this->wrmlURI) << url << "/relation.json";
		ifstream in(file.str().data());
		try {
			return KJSON::parse(in);
		}
		catch (KParseException* e) {
			delete e;
		}
		return NULL;
	}
}

kinky::lang::KObject* kinky::rest::KWRMLRelationRoot::addRelation(KObject* relation, string url) {
	KObject* root = NULL;

	ostringstream file;
	file << *(this->wrmlURI) << url;
	file << "/relation.json";

	ifstream in(file.str().data());
	if (in.is_open()) {
		try {
			root = KJSON::parse(in);
		}
		catch (KParseException* e) {
			delete e;
			root = new KObject("json");
		}
	}
	else {
		root = new KObject("json");
	}

	string name = (*relation)["name"];

	kinky::utils::filesystem::mkdir_recursive(url.data(), 0777, this->wrmlURI->data());

	ofstream out(file.str().data());

	KObject* add = KOBJECT(
			"name" << (string) (*relation)["name"] <<
			"method" << (string) (*relation)["method"] <<
			"title" << (string) (*relation)["title"] <<
			"description" << (string) (*relation)["description"]
	);
	if ((*relation)["responseTypes"] != NULL_KOBJECT) {
		(*add) << "responseTypes" << ((KObject*) (*relation)["responseTypes"]);
	}
	if ((*relation)["requestTypes"] != NULL_KOBJECT) {
		(*add) << "requestTypes" << ((KObject*) (*relation)["requestTypes"]);
	}

	(*root) << name << add;

	out << *root << flush;
	out.close();

	KObject* ret = KOBJECT(
			"links" << KOBJECT(
					"href" << ((string) (*relation)["href"])
			)
	);
	ret->addHeader("Location", (*relation)["href"]);
	ret->setStatus(KHTTPResponse::CREATED);
	return ret;
}

kinky::lang::KObject* kinky::rest::KWRMLRelationRoot::updateRelation(KObject* relation, string url) {
	KObject* ret = this->addRelation(relation, url);
	ret->setStatus(KHTTPResponse::OK);
	return ret;
}

kinky::lang::KObject* kinky::rest::KWRMLRelationRoot::deleteRelation(KObject* relation, string url) {
	KObject* root = NULL;

	ostringstream file;
	file << *(this->wrmlURI) << url;
	file << "/relation.json";

	ifstream in(file.str().data());
	if (in.is_open()) {
		try {
			root = KJSON::parse(in);
		}
		catch (KParseException* e) {
			delete e;
			root = new KObject("json");
		}
	}
	else {
		root = new KObject("json");
	}

	ofstream out(file.str().data());
	if ((*relation)["name"] != NULL_KOBJECT) {
		root->unset((string) (*relation)["name"]);
	}

	out << *root << flush;
	out.close();

	KObject* ret = KHTTP_OK();
	ret->addHeader("Location", (*relation)["href"]);
	return ret;
}
kinky::lang::KObject* kinky::rest::KWRMLRelationRoot::process(KObject* request, int directive, string* url, KToken* token) {
	switch (directive) {
		case KHTTPRequest::GET: {

			// >>>
			// List of all relations
			if (url->find('/', 12) == string::npos) {
				return this->getRelation();
			}
			// <<<

			// >>>
			// Specific relation
			else {
				return this->getRelation(url->substr(20));
			}
			// <<<

			return NULL;
		}
		case KHTTPRequest::POST: {

			// >>>
			// Schema Link for a given operation is request, return schema description
			return this->addRelation(request, url->substr(20));
			// <<<

		}
		case KHTTPRequest::PUT: {

			// >>>
			// Document Root is requested, return service list
			return this->updateRelation(request, url->substr(20));
			// <<<

		}
		case KHTTPRequest::DELETE: {

			// >>>
			// Document Root is requested, return service list
			return this->deleteRelation(request, url->substr(20));
			// <<<

		}
		default: {
			throw new KRestException(KHTTPResponse::METHOD_NOT_ALLOWED, "the provided path '" + *url + "' could not be found");
		}
	}
}

void kinky::rest::wrml::getElementLinksURI(KRestOperation* op, KObject* response, KObject* request, int directive, string field_uri) {
	if ((*response)["elements"] == NULL_KOBJECT) {
		return;
	}
	KObject* elements = (KObject*) (*response)["elements"];
	for (KObject::KObjAttributes::iterator i = elements->begin(); i != elements->end(); i++) {
		KObject* ele = (KObject*) (**i);

		if ((*ele)[field_uri] != NULL_KOBJECT && ((string) (*ele)[field_uri]) != "") {
			(*ele) << "links" << op->getResourceLinks(ele, request, (string) (*ele)[field_uri], directive);
		}
	}
}

void kinky::rest::wrml::getElementLinksData(KRestOperation* op, KObject* response, KObject* request, int directive, string field_data, string field_data_name, string rel) {
	if ((*response)["elements"] == NULL_KOBJECT) {
		return;
	}
	KObject* elements = (KObject*) (*response)["elements"];
	for (KObject::KObjAttributes::iterator i = elements->begin(); i != elements->end(); i++) {
		KObject* ele = (KObject*) (**i);

		if ((*ele)[field_data] != NULL_KOBJECT) {
			(*ele) << "links" << KOBJECT(
				field_data_name << KOBJECT(
					"href" << (string) (*ele)[field_data] <<
					"rel" << rel
				)
			);
		}
	}
}
