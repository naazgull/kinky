#pragma once

#include <exception>
#include <kinky/log/KLogThread.h>
#include <kinky/memory/KMemoryManager.h>
#include <kinky/lang/KObject.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace memory;
	using namespace log;

	namespace rest {

		class KRestException : public exception {
			public:
				KRestException(int codeParam, string messageParam);
				KRestException(int codeParam, string messageParam, int errcode);
				KRestException(int codeParam, string messageParam, KObject* otherParamsParam);
				KRestException(int codeParam, string messageParam, int errcode, KObject* otherParamsParam);
				virtual ~KRestException() throw ();

				string* getMessage();

				int getCode();

				int getErrorCode();

				KObject* getOtherParams();

				friend ostream& operator<<(ostream& os, KRestException& f) {
					os << "REST EXCEPTION >> code: " << f.code << " || message: " << f.getMessage()->data() << endl << flush;
					return os;
				}

				friend KLogStream& operator<<(KLogStream& os, KRestException& f) {
					os << "REST EXCEPTION >> code: " << f.code << " || message: " << f.getMessage()->data() << endl << flush;
					return os;
				}

				virtual string what() throw ();

			private:
				int code;
				int errcode;
				managed_ptr<string>* message;
				managed_ptr<KObject>* otherParams;
		};

	}
}
