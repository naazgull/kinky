#pragma once

#include <sstream>
#include <string>
#include <curl/curl.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/rest/KRestException.h>

using namespace std;
using namespace __gnu_cxx;

#ifndef CRLF
#define CRLF "\r\n"
#endif

namespace kinky {

	using namespace lang;

	namespace rest {

		namespace http {

			std::string* curl(int* responseCode, int directive, string server, string url, string* body = NULL, kinky::rest::http::KHTTPRequest::KHTTPHeaders* headers = NULL, kinky::rest::http::KHTTPResponse::KHTTPHeaders* replyheaders = NULL, bool assync = false);

			size_t curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer);

			size_t curlReader(void *ptr, size_t size, size_t nmemb, void *userdata);

			void getCURLDirective(CURL* curl, int directive);
		}
	}
}

