#pragma once

#include <map>
#include <string>
#include <iostream>
#include <sstream>
#ifdef USE_SPARSEHASH
#include <sparsehash/sparse_hash_map>
#else
#include <kinky/memory/KMaps.h>
#endif
#include <kinky/log/KLogThread.h>
#include <kinky/memory/KMemoryManager.h>
#include <kinky/lang/KObject.h>

#ifndef CRLF
#define CRLF "\r\n"
#endif

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace memory;
	using namespace lang;

	namespace rest {

		namespace http {

			class KHTTPResponse {
				public:

					const static int OK = 200;
					const static int CREATED = 201;
					const static int ACCEPTED = 202;
					const static int NO_CONTENT = 204;

					const static int FOUND = 302;
					const static int SEE_OTHER = 303;
					const static int TEMPORARY_REDIRECT = 307;

					const static int BAD_REQUEST = 400;
					const static int UNAUTHORIZED = 401;
					const static int FORBIDDEN = 403;
					const static int NOT_FOUND = 404;
					const static int METHOD_NOT_ALLOWED = 405;
					const static int REQUEST_TIMEOUT = 408;
					const static int PRECONDITION_FAILED = 412;

					const static int INTERNAL_SERVER_ERROR = 500;
					const static int SERVICE_UNAVAILABLE = 503;

#ifdef USE_SPARSEHASH
					typedef google::sparse_hash_map<string, managed_ptr<string>*> KHTTPHeaders;
#else
					typedef str_map<managed_ptr<string>*> KHTTPHeaders;
#endif

					KHTTPResponse() ;
					virtual ~KHTTPResponse() ;

					void setCode(int code) ;

					void setHeader(string name, string value, bool force = true) ;

					void setHeader(string name, string* value, bool force = true) ;

					void setBody(string* body) ;

					int getCode() ;

					string* getHeader(string name) ;

					KHTTPResponse::KHTTPHeaders* getHeaders() ;

					void removeHeader(string name);

					string* getBody();

					void setProtocol(string protocol);

					void stringify(ostream& oss) ;

					void log(ostream& oss, bool withColors = false);

					friend ostream& operator<<(ostream& os, KHTTPResponse& f) {
						f.stringify(os);
						return os;
					}

					static string getStatusLine(int code);

					static void parseHead(string head, KHTTPResponse::KHTTPHeaders* headers);

				private:
					int code;
					KHTTPResponse::KHTTPHeaders* headers;
					managed_ptr<string>* body;
					string* protocol;
			};
		}
	}
}
