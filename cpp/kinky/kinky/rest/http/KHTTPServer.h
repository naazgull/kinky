#pragma once

#include <kinky/rest/KRest.h>
#include <kinky/rest/net/KSocketServer.h>
#include <kinky/rest/net/KClientSocketThread.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/rest/http/KHTTPSocket.h>
#include <kinky/rest/net/KClientSocketThreadPool.h>

namespace kinky {

	namespace rest {

		using namespace net;
		using namespace thread;

		namespace http {

			class KHTTPServer: public KSocketServer {
				public:
					KHTTPServer(KRest* restParam);
					virtual ~KHTTPServer();

					void initThreadPool(size_t maxThreads, string semkey, int project_id = -1);

					void createReplyThread(int sockfd, sockaddr_in* address);

				private:
					KRest* rest;
					KClientSocketThreadPool* pool;

			};

		}
	}
}
