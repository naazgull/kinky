#include <kinky/rest/http/KCurl.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

std::string* kinky::rest::http::curl(int* responseCode, int directive, string server, string url, string* body, kinky::rest::http::KHTTPRequest::KHTTPHeaders* headers, kinky::rest::http::KHTTPResponse::KHTTPHeaders* replyheaders, bool assync) {
	char errorBuffer[CURL_ERROR_SIZE];
	string buffer("");
	string headersbuffer;

	CURL* curl = curl_easy_init();
	CURLcode result;
	string uri = "";
	uri.insert(0, url);
	uri.insert(0, server);

	struct curl_slist *slist = NULL;

	bool hasAccept = true;
	if (headers != NULL) {
		for (KObject::KObjHeaders::iterator it = headers->begin(); it != headers->end(); it++) {
			if (it->first == "content-length" || it->first == "host" || it->first == "ip-address" || it->first == "cookie" || it->first == "x-request-source") {
				continue;
			}
			if (it->first == "accept" ) {
				hasAccept = true;
			}

			ostringstream oss;
			string key(it->first);
			kinky::utils::prettify_header_name(key);
			oss << key << ": " << **(it->second);
			slist = curl_slist_append(slist, oss.str().data());
		}
	}

	if (!hasAccept) {
		slist = curl_slist_append(slist, "Accept: */*");
	}
	if (assync) {
		slist = curl_slist_append(slist, "Warning: 601 Assynchronous Request");
	}
	if (body == NULL) {
		slist = curl_slist_append(slist, "Content-Length: 0");
	}

	slist = curl_slist_append(slist, "Expect:");

	kinky::rest::http::getCURLDirective(curl, directive);
	if (body != NULL && directive != KHTTPRequest::GET && directive != KHTTPRequest::HEAD && directive != KHTTPRequest::OPTIONS) {
		if (directive == KHTTPRequest::POST) {
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body->data());
			curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, body->length());
		}
		else {
			curl_easy_setopt(curl, CURLOPT_READDATA, body);
			curl_easy_setopt(curl, CURLOPT_READFUNCTION, curlReader);
			curl_easy_setopt(curl, CURLOPT_INFILESIZE, body->length());
			curl_easy_setopt(curl, CURLOPT_UPLOAD, 1);
		}
	}
	else if (body == NULL) {
		if (directive == KHTTPRequest::POST) {
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
			curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, 0);
		}
		else if (directive == KHTTPRequest::PUT || directive == KHTTPRequest::DELETE || directive == KHTTPRequest::PATCH) {
			curl_easy_setopt(curl, CURLOPT_INFILESIZE, 0);
			curl_easy_setopt(curl, CURLOPT_UPLOAD, 1);
		}
	}
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);

	OpenSSL_add_all_algorithms();
	// curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
	curl_easy_setopt(curl, CURLOPT_URL, uri.data());
	curl_easy_setopt(curl, CURLOPT_HEADER, 0);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriter);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_setopt(curl, CURLOPT_WRITEHEADER, &headersbuffer);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	result = curl_easy_perform(curl);
	if (result == CURLE_OK) {
		if (!assync) {
			long httpCode = 0;
			curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
			*responseCode = httpCode;

			curl_slist_free_all(slist);
			curl_easy_cleanup(curl);
			EVP_cleanup();

			if (headersbuffer.length() != 0 && replyheaders != NULL) {
				string replyHeadersStr(headersbuffer.data(), headersbuffer.length());
				KHTTPResponse::parseHead(replyHeadersStr, replyheaders);
			}

			if (buffer.length() != 0) {
				return new string(buffer.data(), buffer.length());
			}
			else {
				return NULL;
			}

		}
	}
	else {
		curl_slist_free_all(slist);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		throw new KRestException(result, errorBuffer, -1);
	}

	return NULL;
}

size_t kinky::rest::http::curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer) {
	size_t result = 0;

	if (buffer != NULL) {
		buffer->insert(buffer->length(), data, size * nmemb);
		result = size * nmemb;
	}

	return result;
}

size_t kinky::rest::http::curlReader(void *ptr, size_t size, size_t nmemb, void *userdata) {
	if (userdata != NULL) {
		string* buffer = static_cast<string*>(userdata);

		if (buffer != NULL && buffer->length() != 0) {
			size_t result = buffer->length() > size * nmemb ? size * nmemb : buffer->length();
			memcpy(ptr, buffer->c_str(), result);

			buffer->erase(0, result);

			return result;
		}
	}
	return 0;
}

void kinky::rest::http::getCURLDirective(CURL* curl, int directive) {
	switch (directive) {
		case KHTTPRequest::PUT: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
			break;
		}
		case KHTTPRequest::SYNC: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "SYNC");
			break;
		}
		case KHTTPRequest::APPLY: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "APPLY");
			break;
		}
		case KHTTPRequest::DELETE: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
			break;
		}
		case KHTTPRequest::GET: {
			curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
			break;
		}
		case KHTTPRequest::POST: {
			curl_easy_setopt(curl, CURLOPT_POST, 1);
			break;
		}
		case KHTTPRequest::HEAD: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "HEAD");
			curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
			break;
		}
		case KHTTPRequest::OPTIONS: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "OPTIONS");
			break;
		}
		default: {
			curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
			break;
		}
	}
}
