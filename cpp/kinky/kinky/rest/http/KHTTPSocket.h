#pragma once

#include <string.h>
#include <time.h>
#include <unistd.h>
#include <kinky/memory/KMemoryManager.h>
#include <kinky/rest/KRest.h>
#include <kinky/rest/net/KSocket.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/rest/KRestAssynchronousRequest.h>

namespace kinky {

	using namespace memory;

	namespace rest {

		using namespace net;

		namespace http {

			class KHTTPSocket: public KSocket<KHTTPRequest, KHTTPResponse> {
				public:
					KHTTPSocket(int sockfd, sockaddr_in* address, KRest* restParam);
					virtual ~KHTTPSocket();

					managed_ptr<KHTTPRequest>* consume();

					void preconsume();

					managed_ptr<KHTTPResponse>* process(KHTTPRequest* req);

					managed_ptr<KHTTPResponse>* process(KRestException* e);

					managed_ptr<KHTTPResponse>* process(exception e);

					managed_ptr<string>* stringify(managed_ptr<KHTTPResponse>* rep);

				private:
					KRest* rest;
					string* __processed;
			};
		}
	}
}
