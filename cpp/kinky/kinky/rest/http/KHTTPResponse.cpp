#include <kinky/rest/http/KHTTPResponse.h>

#include <kinky/utils/KLocale.h>
#include <kinky/rest/http/KHTTPRequest.h>

kinky::rest::http::KHTTPResponse::KHTTPResponse() :
		code(0), headers(new KHTTPHeaders()), body(NULL), protocol(NULL) {
	string keyheader = "X-Null-Header";
	this->headers->set_deleted_key(keyheader);
}
kinky::rest::http::KHTTPResponse::~KHTTPResponse() {
	for (KHTTPResponse::KHTTPHeaders::iterator i = this->headers->begin(); i != this->headers->end(); i++) {
		if (!KMemoryManager::getMemory()->disposed(i->second)) {
			i->second->release();
		}
	}
	this->headers->clear();
	this->headers->resize(0);
	delete this->headers;
	if (this->body != NULL && !KMemoryManager::getMemory()->disposed(this->body)) {
		this->body->release();
	}
	if (this->protocol != NULL) {
		delete this->protocol;
	}
}

void kinky::rest::http::KHTTPResponse::setCode(int code) {
	this->code = code;
}

void kinky::rest::http::KHTTPResponse::setHeader(string name, string value, bool force) {
	this->setHeader(name, new string(value), force);
}

void kinky::rest::http::KHTTPResponse::setHeader(string name, string* value, bool force) {
	KHTTPHeaders::iterator header = this->headers->find(name);
	if (header != this->headers->end()) {
		if (force) {
			header->second->release();
			this->headers->erase(header);
		}
		else {
			delete value;
			return;
		}
	}
	if (name == "Pragma" && *value == "no-cache") {
		KHTTPHeaders::iterator found = this->headers->find("Cache-Control");
		if (found != this->headers->end() && (*(*found->second)) != "no-cache") {
			found->second->release();
			this->headers->erase(found);
		}

		found = this->headers->find("Expires");
		if (found != this->headers->end()) {
			found->second->release();
			this->headers->erase(found);
		}
	}
#ifdef USE_SPARSEHASH
	this->headers->insert(pair<string, managed_ptr<string>*>(name, new managed_ptr<string>(value)));
#else
	this->headers->insert(name, new managed_ptr<string>(value));
#endif
}

void kinky::rest::http::KHTTPResponse::setBody(string* body) {
	if (body == NULL) {
		KHTTPHeaders::iterator header = this->headers->find("Content-Length");
		if (header != this->headers->end()) {
			header->second->release();
			this->headers->erase(header);
		}
	}
	else if (this->code != 204) {
		ostringstream oss;
		oss << body->size();
		string contentLength("Content-Length");
		KHTTPHeaders::iterator header = this->headers->find(contentLength);
		if (header != this->headers->end()) {
			header->second->release();
			this->headers->erase(header);
		}
#ifdef USE_SPARSEHASH
		this->headers->insert(pair<string, managed_ptr<string>*>(contentLength, new managed_ptr<string>(new string(oss.str()))));
#else
		this->headers->insert(contentLength, new managed_ptr<string>(new string(oss.str())));
#endif
	}
	if (this->body != NULL && this->body->get() != body) {
		this->body->release();
	}
	if (body != NULL) {
		this->body = new managed_ptr<string>(body);
	}
	else {
		this->body = NULL;
	}
}

void kinky::rest::http::KHTTPResponse::setProtocol(string protocol) {
	this->protocol = new string(protocol.data());
}

int kinky::rest::http::KHTTPResponse::getCode() {
	return this->code;
}

string* kinky::rest::http::KHTTPResponse::getHeader(string name) {
	KHTTPHeaders::iterator header = this->headers->find(name);
	if (header != this->headers->end()) {
		return header->second->get();
	}
	return NULL;
}

kinky::rest::http::KHTTPResponse::KHTTPHeaders* kinky::rest::http::KHTTPResponse::getHeaders() {
	return this->headers;
}

void kinky::rest::http::KHTTPResponse::removeHeader(string name) {
	if (this->headers == NULL) {
		return;
	}

	KHTTPHeaders::iterator header = this->headers->find(name);
	if (header != this->headers->end()) {
		header->second->release();
		this->headers->erase(header);
	}
}

string* kinky::rest::http::KHTTPResponse::getBody() {
	if (this->body != NULL) {
		return this->body->get();
	}
	return NULL;
}

void kinky::rest::http::KHTTPResponse::stringify(ostream& oss) {
	oss << (this->protocol != NULL ? this->protocol->data() : "HTTP/1.1") << " " << KHTTPResponse::getStatusLine(this->code) << CRLF;

	for (KHTTPResponse::KHTTPHeaders::iterator i = this->headers->begin(); i != this->headers->end(); i++) {
		string name(i->first.data());
		kinky::utils::prettify_header_name(name);
		oss << name;
		oss << ": ";
		oss << **(i->second);
		oss << CRLF;
	}

	oss << CRLF;
	if (this->body != NULL) {
		oss << (**this->body) << flush;
	}
}

void kinky::rest::http::KHTTPResponse::log(ostream& oss, bool withColors) {
#ifdef DEBUG_KINKY
	if (withColors) {
		if (this->code == KHTTPResponse::OK || this->code == KHTTPResponse::CREATED || this->code == KHTTPResponse::ACCEPTED || this->code == KHTTPResponse::NO_CONTENT) {
			oss << "\033[1;32m";
		}
		else if (this->code == KHTTPResponse::TEMPORARY_REDIRECT) {
			oss << "\033[1;33m";
		}
		else {
			oss << "\033[1;31m";
		}
	}
	oss << (this->protocol != NULL ? this->protocol->data() : "HTTP/1.1") << " " << KHTTPResponse::getStatusLine(this->code) << CRLF;
	if (withColors) {
		oss << "\033[0m";
	}
	if (withColors) {
		oss << "\033[1;30m";
	}
	for (KHTTPResponse::KHTTPHeaders::iterator i = this->headers->begin(); i != this->headers->end(); i++) {
		string name(i->first.data());
		kinky::utils::prettify_header_name(name);
		oss << name;
		oss << ": ";
		oss << **(i->second);
		oss << CRLF;
	}
	if (withColors) {
		oss << "\033[0m";
	}

	oss << CRLF;
	string* mimeType = this->getHeader("Content-Type");
	if (this->body != NULL && this->body->get() != NULL && (mimeType == NULL || mimeType->find("json") != string::npos || mimeType->find("xml") != string::npos || mimeType->find("html") != string::npos)) {
		oss << (**this->body) << endl;
	}
	if (withColors) {
		oss << "\033[1;35m";
	}
	if (withColors) {
		oss << "\033[0m" << flush;
	}

#else
	if (withColors) {
		oss << "\033[1;30m";
	}
	oss << " -> ";
	if (withColors) {
	}
	oss << "\033[0m";
	if (withColors) {
		if (this->code < 300) {
			oss << "\033[1;32m";
		}
		else if (this->code < 400) {
			oss << "\033[1;33m";
		}
		else {
			oss << "\033[1;31m";
		}
	}
	oss << (this->protocol != NULL ? this->protocol->data() : "HTTP/1.1") << " " << KHTTPResponse::getStatusLine(this->code);
	if (withColors) {
		oss << "\033[0m";
		oss << "\033[0;37m";
	}
	oss << " [content-length: " << (this->body != NULL ? this->body->get()->length() : 0) << " bytes]";
	if (withColors) {
		oss << "\033[0m";
	}
	oss << endl << flush;
#endif

}

string kinky::rest::http::KHTTPResponse::getStatusLine(int code) {
	switch (code) {
		case KHTTPResponse::OK: {
			return "200 OK";
		}
		case KHTTPResponse::CREATED: {
			return "201 Created";
		}
		case KHTTPResponse::ACCEPTED: {
			return "202 Accepted";
		}
		case KHTTPResponse::NO_CONTENT: {
			return "204 No Content";
		}
		case KHTTPResponse::FOUND: {
			return "302 Found";
		}
		case KHTTPResponse::SEE_OTHER: {
			return "303 See Other";
		}
		case KHTTPResponse::TEMPORARY_REDIRECT: {
			return "307 Temporary Redirect";
		}
		case KHTTPResponse::BAD_REQUEST: {
			return "400 Bad Request";
		}
		case KHTTPResponse::UNAUTHORIZED: {
			return "401 Unauthorized";
		}
		case KHTTPResponse::FORBIDDEN: {
			return "403 Forbidden";
		}
		case KHTTPResponse::NOT_FOUND: {
			return "404 Not Found";
		}
		case KHTTPResponse::METHOD_NOT_ALLOWED: {
			return "405 Method Not Allowed";
		}
		case KHTTPResponse::REQUEST_TIMEOUT: {
			return "408 Request Timeout";
		}
		case KHTTPResponse::PRECONDITION_FAILED: {
			return "412 Precondition Failed";
		}
		case KHTTPResponse::INTERNAL_SERVER_ERROR: {
			return "500 Internal Server Error";
		}
		case KHTTPResponse::SERVICE_UNAVAILABLE: {
			return "503 Service Unavailable";
		}
	}
	return "500 Internal Server Error";
}

void kinky::rest::http::KHTTPResponse::parseHead(string head, KHTTPResponse::KHTTPHeaders* headers) {
	if (headers == NULL) {
		headers = new KHTTPResponse::KHTTPHeaders();
	}

	istringstream iss(head, ios_base::in);
	size_t separator = 0;
	string line;
	while (iss.good()) {
		string name;
		string* value = NULL;

		std::getline(iss, line);
		separator = line.find(":");
		if (separator != string::npos) {
			name = line.substr(0, separator);
			value = new string(line.substr(separator + 2, line.size() - separator - 3));
			kinky::utils::trim(name);
#ifdef USE_SPARSEHASH
			headers->insert(pair<string, managed_ptr<string>*>(name, new managed_ptr<string>(value)));
#else
			headers->insert(name, new managed_ptr<string>(value));
#endif
		}
	}

}
