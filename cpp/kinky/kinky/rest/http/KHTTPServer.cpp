#include <kinky/rest/http/KHTTPServer.h>
#include <gperftools/malloc_extension.h>

kinky::rest::http::KHTTPServer::KHTTPServer(KRest* restParam) :
		rest(restParam), pool(NULL) {
}

kinky::rest::http::KHTTPServer::~KHTTPServer() {
	if (this->pool != NULL) {
		delete this->pool;
	}
}

void kinky::rest::http::KHTTPServer::initThreadPool(size_t maxThreads, string semKeyFile, int project_id) {
	this->pool = new KClientSocketThreadPool(maxThreads, semKeyFile, project_id);
}

void kinky::rest::http::KHTTPServer::createReplyThread(int sockfd, sockaddr_in* address) {
	KHTTPSocket* channel = new KHTTPSocket(sockfd, address, this->rest);
	if (this->pool != NULL && this->pool->getMaxThreads() != (size_t) -1) {
		//-------------> IF CONSUMING POLICY IS TO WAIT FOR PROCESSOR TO CONSUME SOCKET DATA UNCOMMENT THIS
		//this->pool->borrow(channel);

		//-------------> ELSE IF CONSUMING POLICY IS TO CONSUME SOCKET DATA FIRST UNCOMMENT THIS
		this->pool->distribute(channel);
	}
	else {
		KClientSocketThread<KHTTPRequest, KHTTPResponse>* thread = new KClientSocketThread<KHTTPRequest, KHTTPResponse>(channel);
		thread->run();
	}
	MallocExtension::instance()->ReleaseFreeMemory();
}

