#include <kinky/rest/http/ssl/KHTTPSSocket.h>

kinky::rest::http::ssl::KHTTPSSocket::KHTTPSSocket(int sockfd, sockaddr_in* address, SSL_CTX* sslParam, KRest* restParam) :
		KSSLSocket<KHTTPRequest, KHTTPResponse>(sockfd, address, sslParam), rest(restParam) , __processed(NULL) {
}
kinky::rest::http::ssl::KHTTPSSocket::~KHTTPSSocket() {
}

kinky::memory::managed_ptr<kinky::rest::http::KHTTPRequest>* kinky::rest::http::ssl::KHTTPSSocket::consume() {
	KHTTPRequest* req = NULL;
	kinky::memory::managed_ptr<string>* reqStr = NULL;

	if (this->__processed != NULL) {
		reqStr = new managed_ptr<string>(this->__processed);
		this->__processed = NULL;
	}
	else {
		reqStr = this->readRequest();
	}

	if (reqStr == NULL) {
		return NULL;
	}

	req = KHTTPRequest::parse(reqStr->get());
	reqStr->release();
	req->setHeader("IP-Address", this->getClientAddress());

	size_t contentLength = 0;
	if (req->getHeader("Content-Length") != NULL) {
		stringstream iss;
		iss << *(req->getHeader("Content-Length"));
		iss >> contentLength;
	}

	if (contentLength != 0) {
		string* body = req->getBody();
		if (body == NULL) {
			body = new string();
		}
		time_t initial = time(NULL);
		while (contentLength != body->length()) {
			managed_ptr<string>* requestBody = this->readRequest(contentLength);
			if (requestBody != NULL) {
				body->insert(body->length(), requestBody->get()->data(), requestBody->get()->length());
				requestBody->release();
			}
			else {
				if (initial + KSOCKET_TIMEOUT > time(NULL)) {
					delete req;
					delete body;
					throw new KRestException(KHTTPResponse::REQUEST_TIMEOUT, "Content-Length and request body do not match");
				}
				else {
					sleep(5);
				}
			}
		}
		req->setBody(body);
	}

#ifdef LOG
#ifdef LOG_COLORS
	req->logRequestLine((*kinky::io::out), true);
#else
	req->logRequestLine((*kinky::io::out));
#endif
#endif
	return new managed_ptr<KHTTPRequest>(req);
}

void kinky::rest::http::ssl::KHTTPSSocket::preconsume() {
	kinky::memory::managed_ptr<string>* reqStr = this->readRequest();
	if (reqStr == NULL) {
		return;
	}

	string* req = new string(static_cast<string*>(reqStr->pointed())->data());
	reqStr->release();

	size_t contentLength = 0;
	size_t clindex = string::npos;
	if ((clindex = req->find("Content-Length:")) != string::npos) {
		clindex += 16;
		string cl(req->substr(clindex, req->find(CRLF, clindex) - clindex));
		stringstream iss;
		iss << cl;
		iss >> contentLength;
	}

	if (contentLength != 0) {
		string body;
		size_t head_sep = req->find(HEADER_SEPARATOR);
		if (head_sep != string::npos) {
			body.assign(req->substr(head_sep + 4));
			req->erase(head_sep + 4);
		}

		time_t initial = time(NULL);
		while (contentLength != body.length()) {
			managed_ptr<string>* requestBody = this->readRequest(contentLength);
			if (requestBody != NULL) {
				body.insert(body.length(), requestBody->get()->data(), requestBody->get()->length());
				requestBody->release();
			}
			else {
				if (initial + KSOCKET_TIMEOUT > time(NULL)) {
					delete req;
					throw new KRestException(KHTTPResponse::REQUEST_TIMEOUT, "Content-Length and request body do not match");
				}
				else {
					sleep(5);
				}
			}
		}
		req->insert(req->length(), body.data());
	}

	this->__processed = req;
}

kinky::memory::managed_ptr<kinky::rest::http::KHTTPResponse>* kinky::rest::http::ssl::KHTTPSSocket::process(KHTTPRequest* req) {
	if (req->getHeader("Warning") != NULL && *(req->getHeader("Warning")) == string("601 Assynchronous Request")) {
		KHTTPResponse* rep = new KHTTPResponse();
		KRest::preprocess(rep);
		rep->setCode(KHTTPResponse::ACCEPTED);
		rep->setBody(new string(""));
		req->removeHeader("Warning");

		throw new KRestAssynchronousRequest<KHTTPResponse>(rep);
	}
	return this->rest->process(req);
}

kinky::memory::managed_ptr<kinky::rest::http::KHTTPResponse>* kinky::rest::http::ssl::KHTTPSSocket::process(KRestException* e) {
	KHTTPResponse* ret = KRest::preprocess(NULL);
	ret->setCode(e->getCode());
	ret->setBody(new string(e->getMessage()->data()));
	return new managed_ptr<KHTTPResponse>(ret);

}

kinky::memory::managed_ptr<kinky::rest::http::KHTTPResponse>* kinky::rest::http::ssl::KHTTPSSocket::process(exception e) {
	KHTTPResponse* ret = KRest::preprocess(NULL);
	ret->setCode(KHTTPResponse::INTERNAL_SERVER_ERROR);
	ret->setBody(new string(e.what()));
	return new managed_ptr<KHTTPResponse>(ret);
}

kinky::memory::managed_ptr<string>* kinky::rest::http::ssl::KHTTPSSocket::stringify(managed_ptr<KHTTPResponse>* rep) {
	ostringstream oss;
	oss << **rep;
	return new managed_ptr<string>(new string(oss.str()));
}
