#pragma once

#include <string.h>
#include <time.h>
#include <unistd.h>
#include <kinky/memory/KMemoryManager.h>
#include <kinky/rest/KRest.h>
#include <kinky/rest/net/ssl/KSSLSocket.h>
#include <kinky/rest/http/KHTTPSocket.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/rest/KRestAssynchronousRequest.h>

namespace kinky {

	using namespace memory;

	namespace rest {

		using namespace net;
		using namespace net::ssl;

		namespace http {

			namespace ssl {
				class KHTTPSSocket: public KSSLSocket<KHTTPRequest, KHTTPResponse> {
					public:
						KHTTPSSocket(int sockfd, sockaddr_in* address, SSL_CTX* sslContext, KRest* restParam);
						virtual ~KHTTPSSocket();

						managed_ptr<KHTTPRequest>* consume();

						void preconsume();

						managed_ptr<KHTTPResponse>* process(KHTTPRequest* req);

						managed_ptr<KHTTPResponse>* process(KRestException* e);

						managed_ptr<KHTTPResponse>* process(exception e);

						managed_ptr<string>* stringify(managed_ptr<KHTTPResponse>* rep);

					private:
						KRest* rest;
						SSL* ssl;
						string* __processed;
				};
			}
		}
	}
}
