#pragma once

#include <kinky/rest/KRest.h>
#include <kinky/rest/net/ssl/KSSLSocketServer.h>
#include <kinky/rest/net/ssl/KClientSSLSocketThread.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/rest/http/ssl/KHTTPSSocket.h>
#include <kinky/rest/net/ssl/KClientSSLSocketThreadPool.h>

namespace kinky {

	namespace rest {

		using namespace net::ssl;

		namespace http {

			namespace ssl {

				class KHTTPSServer: public KSSLSocketServer {
					public:
						KHTTPSServer(KRest* restParam, string certificate, string key, bool useChain = false);
						virtual ~KHTTPSServer();

						void createReplyThread(int sockfd, sockaddr_in* address);

						void initThreadPool(size_t maxThreads, string semkey, int project_id = -1);

					private:
						KRest* rest;
						KClientSSLSocketThreadPool* pool;
				};
			}
		}
	}
}
