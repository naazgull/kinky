#include <kinky/rest/http/ssl/KHTTPSServer.h>

kinky::rest::http::ssl::KHTTPSServer::KHTTPSServer(KRest* restParam, string certificate, string key, bool useChain) :
		kinky::rest::net::ssl::KSSLSocketServer(certificate, key), rest(restParam) {
}

kinky::rest::http::ssl::KHTTPSServer::~KHTTPSServer() {
}

void kinky::rest::http::ssl::KHTTPSServer::initThreadPool(size_t maxThreads, string semkey, int project_id) {
	this->pool = new KClientSSLSocketThreadPool(maxThreads, semkey, project_id);
}

void kinky::rest::http::ssl::KHTTPSServer::createReplyThread(int sockfd, sockaddr_in* address) {
	KHTTPSSocket* channel = new KHTTPSSocket(sockfd, address, this->sslContext, this->rest);
	if (this->pool != NULL && this->pool->getMaxThreads() != (size_t) -1) {
		//-------------> IF CONSUMING POLICY IS TO WAIT FOR PROCESSOR TO CONSUME SOCKET DATA UNCOMMENT THIS
		//this->pool->borrow(channel);

		//-------------> ELSE IF CONSUMING POLICY IS TO CONSUME SOCKET DATA FIRST UNCOMMENT THIS
		this->pool->distribute(channel);
	}
	else {
		KClientSSLSocketThread<KHTTPRequest, KHTTPResponse>* thread = new KClientSSLSocketThread<KHTTPRequest, KHTTPResponse>(channel);
		thread->run();
	}
}
