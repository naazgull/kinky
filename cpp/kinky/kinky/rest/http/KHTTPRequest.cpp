#include <kinky/rest/http/KHTTPRequest.h>

#include <kinky/utils/KDateTime.h>
#include <kinky/lang/KJSON.h>
#include <fstream>

using namespace kinky::lang;

kinky::rest::http::KHTTPRequest::KHTTPRequest() :
		directive(6), url(NULL), headers(), params(NULL), cookie(NULL), body(NULL), protocol(NULL), query(NULL) {
}

kinky::rest::http::KHTTPRequest::KHTTPRequest(int directiveParam, string* urlParam, KHTTPRequest::KHTTPHeaders* headersParam, KHTTPRequest::KHTTPParams* paramsParam, string* bodyParam, KHTTPRequest::KHTTPCookie* cookieParam) :
		directive(directiveParam), url(urlParam), headers(headersParam), params(paramsParam), cookie(cookieParam), body(new managed_ptr<string>(bodyParam)), protocol(NULL), query(NULL) {
	if (this->headers != NULL) {
		string keyheader = "X-Null-Header";
		this->headers->set_deleted_key(keyheader);
	}
	if (this->params != NULL) {
		string keyparams = "null";
		this->params->set_deleted_key(keyparams);
	}
	this->cookie = NULL;
}

kinky::rest::http::KHTTPRequest::~KHTTPRequest() {
	if (this->headers != NULL) {
		for (KHTTPRequest::KHTTPHeaders::iterator i = this->headers->begin(); i != this->headers->end(); i++) {
			if (!KMemoryManager::getMemory()->disposed(i->second)) {
				i->second->release();
			}
		}
		this->headers->clear();
		this->headers->resize(0);
		delete this->headers;
	}
	if (this->params != NULL) {
		for (KHTTPRequest::KHTTPParams::iterator k = this->params->begin(); k != this->params->end(); k++) {
			if (!KMemoryManager::getMemory()->disposed(k->second)) {
				k->second->release();
			}
		}
		this->params->clear();
		this->params->resize(0);
		delete this->params;
	}

	if (this->url != NULL) {
		delete this->url;
	}
	if (this->body != NULL && !KMemoryManager::getMemory()->disposed(this->body)) {
		this->body->release();
	}
	if (this->protocol != NULL) {
		delete this->protocol;
	}
	if (this->query != NULL) {
		delete this->query;
	}
	if (this->cookie != NULL) {
		for (KHTTPRequest::KHTTPCookie::iterator k = this->cookie->begin(); k != this->cookie->end(); k++) {
			if (!KMemoryManager::getMemory()->disposed(k->second)) {
				k->second->release();
			}
		}
		this->cookie->clear();
		this->cookie->resize(0);
		delete this->cookie;
	}
}

string* kinky::rest::http::KHTTPRequest::getURL() {
	return this->url;
}

void kinky::rest::http::KHTTPRequest::setURL(string* url) {
	if (this->url != NULL) {
		delete this->url;
	}
	this->url = url;
}

int kinky::rest::http::KHTTPRequest::getDirective() {
	return this->directive;
}

void kinky::rest::http::KHTTPRequest::setDirective(int directive) {
	this->directive = directive;
}

string* kinky::rest::http::KHTTPRequest::getHeader(string name) {
	if (this->headers == NULL) {
		return NULL;
	}
	std::transform(name.begin(), name.end(), name.begin(), ::tolower);
	kinky::utils::trim(name);

	KHTTPHeaders::iterator header = this->headers->find(name);
	if (header != this->headers->end()) {
		return header->second->get();
	}
	return NULL;
}

kinky::rest::http::KHTTPRequest::KHTTPHeaders* kinky::rest::http::KHTTPRequest::getHeaders() {
	return this->headers;
}

void kinky::rest::http::KHTTPRequest::setHeaders(kinky::rest::http::KHTTPRequest::KHTTPHeaders* headers) {
	this->headers = headers;
}

void kinky::rest::http::KHTTPRequest::setHeader(string name, string* value) {
	if (this->headers == NULL) {
		this->headers = new KHTTPRequest::KHTTPHeaders();
	}
	std::transform(name.begin(), name.end(), name.begin(), ::tolower);
	kinky::utils::trim(name);

	KHTTPHeaders::iterator header = this->headers->find(name);
	if (header != this->headers->end()) {
		header->second->release();
		this->headers->erase(header);
	}
#ifdef USE_SPARSEHASH
	this->headers->insert(pair<string, managed_ptr<string>*>(name, new managed_ptr<string>(value)));
#else
	this->headers->insert(name, new managed_ptr<string>(value));
#endif
}

void kinky::rest::http::KHTTPRequest::setBody(string* body) {
	if (this->body != NULL && this->body->get() != body) {
		this->body->release();
	}
	this->body = new managed_ptr<string>(body);
}

void kinky::rest::http::KHTTPRequest::setProtocol(string protocol) {
	if (this->protocol != NULL) {
		delete this->protocol;
	}
	this->protocol = new string(protocol.data());
}

void kinky::rest::http::KHTTPRequest::removeHeader(string name) {
	if (this->headers == NULL) {
		return;
	}

	std::transform(name.begin(), name.end(), name.begin(), ::tolower);
	kinky::utils::trim(name);

	KHTTPHeaders::iterator header = this->headers->find(name);
	if (header != this->headers->end()) {
		header->second->release();
		this->headers->erase(header);
	}
}

string* kinky::rest::http::KHTTPRequest::getParam(string name) {
	if (this->params == NULL) {
		return NULL;
	}

	KHTTPParams::iterator param = this->params->find(name);
	if (param != this->params->end()) {
		return param->second->get();
	}
	return NULL;
}

void kinky::rest::http::KHTTPRequest::setParam(string name, string* value) {
	if (this->params == NULL) {
		this->params = new KHTTPRequest::KHTTPParams();
	}

	KHTTPParams::iterator param = this->params->find(name);
	if (param == this->params->end()) {
#ifdef USE_SPARSEHASH
		this->params->insert(pair<string, managed_ptr<string>*>(name, new managed_ptr<string>(value)));
#else
		this->params->insert(name, new managed_ptr<string>(value));
#endif
	}
	else {
		param->second->release();
		this->params->erase(param);
#ifdef USE_SPARSEHASH
		this->params->insert(pair<string, managed_ptr<string>*>(name, new managed_ptr<string>(value)));
#else
		this->params->insert(name, new managed_ptr<string>(value));
#endif
	}
}

void kinky::rest::http::KHTTPRequest::removeParam(string name) {
	if (this->params == NULL) {
		return;
	}

	KHTTPParams::iterator param = this->params->find(name);
	if (param != this->params->end()) {
		param->second->release();
		this->params->erase(param);
	}
}

kinky::rest::http::KHTTPRequest::KHTTPParams* kinky::rest::http::KHTTPRequest::getParams() {
	return this->params;
}

void kinky::rest::http::KHTTPRequest::setParams(kinky::rest::http::KHTTPRequest::KHTTPParams* params) {
	this->params = params;
}

string* kinky::rest::http::KHTTPRequest::getCookie(string name) {
	if (this->cookie == NULL) {
		string* cookie = NULL;
		if ((cookie = this->getHeader("cookie")) != NULL) {
			this->cookie = kinky::utils::strip_cookie(*cookie);
		}
		else {
			return NULL;
		}
	}
	KHTTPCookie::iterator param = this->cookie->find(name);
	if (param != this->cookie->end()) {
		return param->second->get();
	}
	return NULL;
}

kinky::rest::http::KHTTPRequest::KHTTPCookie* kinky::rest::http::KHTTPRequest::getCookies() {
	if (this->cookie == NULL) {
		string* cookie = NULL;
		if ((cookie = this->getHeader("cookie")) != NULL) {
			this->cookie = kinky::utils::strip_cookie(*cookie);
		}
		else {
			return NULL;
		}
	}
	return this->cookie;
}

string* kinky::rest::http::KHTTPRequest::getQueryString() {
	return this->query;
}

string* kinky::rest::http::KHTTPRequest::getBody() {
	if (this->body == NULL) {
		return NULL;
	}
	return this->body->get();
}

void kinky::rest::http::KHTTPRequest::stringify(ostream& oss, bool onlyHeader) {
	oss << KHTTPRequest::getDirective(this->directive) << " " << (*this->url);

	if (this->params->size() != 0) {
		oss << "?";
		for (KHTTPRequest::KHTTPParams::iterator k = this->params->begin(); k != this->params->end(); k++) {
			if (k != this->params->begin()) {
				oss << "&";
			}
			oss << k->first;
			oss << "=";
			oss << **(k->second);
		}
	}
	oss << " " << (this->protocol != NULL ? this->protocol->data() : "HTTP/1.1") << CRLF;

	for (KHTTPRequest::KHTTPHeaders::iterator i = this->headers->begin(); i != this->headers->end(); i++) {
		string name(i->first.data());
		kinky::utils::prettify_header_name(name);
		oss << name;
		oss << ": ";

		if (i->second != NULL) {
			oss << **(i->second);
		}
		else {
			oss << "NULL";
		}
		oss << CRLF;
	}

	oss << CRLF;
	if (!onlyHeader && this->body != NULL && this->body->get() != NULL) {
		oss << (**this->body);
	}
}

void kinky::rest::http::KHTTPRequest::log(ostream& oss, bool withColors) {
#ifdef DEBUG_KINKY
	if (withColors) {
		oss << "\033[1;35m";
	}
	oss << "[T" << (unsigned int) pthread_self() << " IP" << *this->getHeader("IP-Address") << " " << kinky::utils::date::format("D%Y-%m-%dT%H:%M:%S") << " REQUEST -> RESPONSE] ";
	if (withColors) {
		oss << "\033[0m" << flush;
	}
	if (withColors) {
		oss << "\033[1;34m";
	}
	oss << endl << KHTTPRequest::getDirective(this->directive) << " " << (*this->url);

	if (this->params->size() != 0) {
		oss << "?";
		KHTTPRequest::KHTTPParams::iterator k;
		for (k = this->params->begin(); k != this->params->end(); k++) {
			if (k != this->params->begin()) {
				oss << "&";
			}
			oss << k->first;
			oss << "=";
			oss << **(k->second);
		}
	}
	oss << " " << (this->protocol != NULL ? this->protocol->data() : "HTTP/1.1") << CRLF << flush;
	if (withColors) {
		oss << "\033[0m" << flush;
	}

	if (withColors) {
		oss << "\033[1;30m";
	}
	KHTTPRequest::KHTTPHeaders::iterator i;
	for (i = this->headers->begin(); i != this->headers->end(); i++) {
		string name(i->first.data());
		kinky::utils::prettify_header_name(name);
		oss << name;
		oss << ": ";

		if (i->second != NULL) {
			oss << **(i->second);
		}
		else {
			oss << "NULL";
		}
		oss << CRLF;
	}

	if (withColors) {
		oss << "\033[0m" << flush;
	}
	oss << CRLF;
	oss << (**this->body);
	oss << endl << flush;
#else
	if (withColors) {
		oss << "\033[1;35m";
	}
	oss << "[T" << (unsigned int) pthread_self() << " IP" << *this->getHeader("IP-Address") << " " << kinky::utils::date::format("D%Y-%m-%dT%H:%M:%S") << " ] ";
	if (withColors) {
		oss << "\033[0m" << flush;
	}
	if (withColors) {
		oss << "\033[1;34m";
	}
	oss << KHTTPRequest::getDirective(this->directive) << " " << (*this->url);
	if (this->params->size() != 0) {
		oss << "?";
		for (KHTTPRequest::KHTTPParams::iterator k = this->params->begin(); k != this->params->end(); k++) {
			if (k != this->params->begin()) {
				oss << "&";
			}
			oss << k->first;
			oss << "=";
			oss << **(k->second);
		}
	}
	oss << " " << (this->protocol != NULL ? this->protocol->data() : "HTTP/1.1") << flush;
	if (withColors) {
		oss << "\033[0m" << flush;
	}
#endif

}

void kinky::rest::http::KHTTPRequest::logRequestLine(ostream& oss, bool withColors) {
	if (withColors) {
		oss << "\033[1;37m";
	}
	oss << "[T" << (unsigned int) pthread_self() << " IP" << *this->getHeader("IP-Address") << " " << kinky::utils::date::format("D%Y-%m-%dT%H:%M:%S") << " REQ. HTTP] " << KHTTPRequest::getDirective(this->directive) << " " << (*this->url) << flush;
	if (withColors) {
		oss << "\033[0m" << flush;
	}
	oss << endl << flush;
}

void kinky::rest::http::KHTTPRequest::logRequestLine(kinky::log::KLogStream& oss, bool withColors) {
	if (withColors) {
		oss << "\033[1;37m";
	}
	oss << "[T" << (unsigned int) pthread_self() << " IP" << *this->getHeader("IP-Address") << " " << kinky::utils::date::format("D%Y-%m-%dT%H:%M:%S") << " REQ. HTTP] " << KHTTPRequest::getDirective(this->directive) << " " << (*this->url) << flush;
	if (withColors) {
		oss << "\033[0m" << flush;
	}
	oss << endl << flush;
}

kinky::rest::http::KHTTPRequest* kinky::rest::http::KHTTPRequest::parse(std::ifstream& in) {
	if (in.is_open()) {
		in.seekg(0, ios::end);
		size_t length = in.tellg();
		in.seekg(0, ios::beg);
		char* buffer = new char[length];
		memset(buffer, 0, length);
		in.read(buffer, length);
		in.close();
		string* toparse = new string(buffer, length);
		KHTTPRequest* ret = KHTTPRequest::parse(toparse);
		delete toparse;
		delete[] buffer;
		return ret;
	}
	throw new kinky::lang::KParseException(500, new managed_ptr<string>(new string("file is not opened")));
}

kinky::rest::http::KHTTPRequest* kinky::rest::http::KHTTPRequest::parse(string* reqStr) {
	KHTTPRequest::KHTTPHeaders* headers = new KHTTPRequest::KHTTPHeaders();
	KHTTPRequest::KHTTPParams* params = new KHTTPRequest::KHTTPParams();

	size_t separator = reqStr->find("\r\n\r\n");
	string head;
	string* body = NULL;
	if (separator != string::npos) {
		head = reqStr->substr(0, separator + 2);
		body = new string(reqStr->substr(separator + 4));
	}
	else {
		head = *reqStr;
		body = new string();
	}

	istringstream iss(head, ios_base::in);

	string directiveStr;
	iss >> directiveStr;
	int directive = KHTTPRequest::getDirective(directiveStr);

	string* url = NULL;
	string tempurl;
	iss >> tempurl;

	string paramsStr;
	separator = tempurl.find("?");
	if (separator != string::npos) {
		paramsStr.assign(tempurl.substr(separator + 1));
		ostringstream oss;
		kinky::utils::url_decode(tempurl.substr(0, separator), oss);
		url = new string(oss.str());
		KHTTPRequest::stripParams(paramsStr, params);
	}
	else {
		ostringstream oss;
		kinky::utils::url_decode(tempurl, oss);
		url = new string(oss.str());
	}

	string httpVersion;
	iss >> httpVersion;

	string line;
	while (iss.good()) {
		string name;
		string* value = NULL;

		std::getline(iss, line);
		separator = line.find(":");
		if (separator != string::npos) {
			name = line.substr(0, separator);
			value = new string(line.substr(separator + 2, line.size() - separator - 3));
			std::transform(name.begin(), name.end(), name.begin(), ::tolower);
			kinky::utils::trim(name);
#ifdef USE_SPARSEHASH
			headers->insert(pair<string, managed_ptr<string>*>(name, new managed_ptr<string>(value)));
#else
			headers->insert(name, new managed_ptr<string>(value));
#endif
		}
	}

	KHTTPRequest* req = new KHTTPRequest(directive, url, headers, params, body);
	req->query = new string(paramsStr);
	return req;
}

kinky::rest::http::KHTTPRequest::KHTTPParams* kinky::rest::http::KHTTPRequest::stripParams(string paramsStr, KHTTPRequest::KHTTPParams* params) {
	if (params == NULL) {
		params = new KHTTPRequest::KHTTPParams();
		string keyparams = "null";
		params->set_deleted_key(keyparams);
	}
	stringstream ss(paramsStr);
	string item;
	size_t separator = 0;

	while (std::getline(ss, item, '&')) {
		separator = item.find("=");
		if (separator != string::npos) {
			string name;
			ostringstream oss;
			kinky::utils::url_decode(item.substr(0, separator), oss);
			name = string(oss.str());
			oss.str("");
			kinky::utils::url_decode(item.substr(separator + 1), oss);
			string* value = new string(oss.str());

			KHTTPRequest::KHTTPParams::iterator found = params->find(name);
			if (found != params->end()) {
				KObject* obj = NULL;
				string* oldv = found->second->get();

				if (oldv->find("KARRAY:") != string::npos) {
					oldv->erase(0, 7);
					try {
						obj = kinky::lang::KJSON::parse(oldv);
					}
					catch (KParseException* e) {
						delete e;
						obj = new KObject(KObject::ARRAY);
					}
					(*obj) << *value;
					delete value;
				}
				else {
					obj = KOBJECT_ARRAY(*oldv << *value);
					delete value;
				}
				ostringstream oss;
				oss << *obj;
				oldv->assign(oss.str());
				oldv->insert(0, "KARRAY:");
			}
			else {
#ifdef USE_SPARSEHASH
				params->insert(pair<string, managed_ptr<string>*>(name, new managed_ptr<string>(value)));
#else
				params->insert(name, new managed_ptr<string>(value));
#endif
			}
		}
	}
	return params;
}

kinky::rest::http::KHTTPRequest::KHTTPParams* kinky::rest::http::KHTTPRequest::stripFormData(string boundary, string paramsStr, string tmpPath, KHTTPRequest::KHTTPParams* params) {
	if (params == NULL) {
		params = new KHTTPRequest::KHTTPParams();
		string keyparams = "null";
		params->set_deleted_key(keyparams);
	}
	kinky::utils::trim(boundary);
	kinky::utils::trim(paramsStr);
	size_t sepBoundary = 0;
	size_t last = paramsStr.find(boundary)  + boundary.length() + 2;

	while ((sepBoundary = paramsStr.find(boundary, last)) != string::npos) {
		string headers = paramsStr.substr(last, sepBoundary - last + boundary.length());
		string* body = NULL;

		kinky::utils::trim(headers);

		size_t sepHeaderBody = headers.find("\r\n\r\n");
		string head;
		size_t bodyLen = 0;
		if (sepHeaderBody != string::npos) {
			head.assign(headers.substr(0, sepHeaderBody));
			bodyLen = headers.find(boundary) - sepHeaderBody - 8;
			body = new string(headers.substr(sepHeaderBody + 4, bodyLen).data(), bodyLen);
		}
		else {
			continue;
		}

		istringstream iss(head, ios_base::in);
		string line;
		string name;
		bool base64 = false;
		while (iss.good()) {
			std::getline(iss, line);
			size_t sepHeaderName = line.find(":");
			if (sepHeaderName != string::npos) {
				string n = line.substr(0, sepHeaderName);
				if (n == "Content-Encoding" || n == "Content-encoding" || n == "content-enconding") {
					if (line.find("Base64") != string::npos) {
						base64 = true;
					}
				}
				else if (n == "Content-Disposition" || n == "Content-disposition" || n == "content-disposition") {
					if (line.find("filename=") != string::npos) {
						size_t sepName = line.find("name=") + 5;
						name.assign(line.substr(sepName, line.find(";", sepName) - sepName));
						kinky::utils::replaceAll(name, "\"", " ");
						kinky::utils::trim(name);

						string filename = line.substr(line.find("filename=") + 9);
						kinky::utils::replaceAll(filename, "\"", " ");
						kinky::utils::trim(filename);

						ostringstream oss;
						oss << tmpPath << time(NULL) << filename << flush;
						string tmpFile = oss.str();

						if (base64) {
							istringstream oss;
							oss.str(*body);
							ostringstream out;
							kinky::utils::base64_decode(oss, out);
							body->assign(out.str());
							bodyLen = body->length();
						}

						ofstream ofs;
						ofs.open(tmpFile.data());
						ofs.write(body->c_str(), bodyLen);
						ofs.flush();
						ofs.close();

						delete body;
						body = new string(filename);

						oss.str("");
						oss << name << "_tmp" << flush;
						KHTTPRequest::KHTTPParams::iterator ntmp = params->find(oss.str());
						if (ntmp != params->end()) {
							params->erase(ntmp);
						}
#ifdef USE_SPARSEHASH
						params->insert(pair<string, managed_ptr<string>*>(oss.str().data(), new managed_ptr<string>(new string(tmpFile))));
#else
						params->insert(oss.str(), new managed_ptr<string>(new string(tmpFile)));
#endif
					}
					else {
						name.assign(line.substr(line.find("name=") + 5));
						kinky::utils::replaceAll(name, "\"", " ");
						kinky::utils::trim(name);
					}
					break;
				}
			}
		}

		KHTTPRequest::KHTTPParams::iterator found = params->find(name);
		if (found != params->end()) {
			KObject* obj = NULL;
			string* value = found->second->get();

			if (value->find("KARRAY:") != string::npos) {
				value->erase(0, 7);
				try {
					obj = kinky::lang::KJSON::parse(value);
				}
				catch (KParseException* e) {
					delete e;
					obj = new KObject(KObject::ARRAY);
				}
				(*obj) << *body;
				delete body;
			}
			else {
				obj = KOBJECT_ARRAY(*value << *body);
				delete body;
			}
			ostringstream oss;
			oss << *obj;
			value->assign(oss.str());
			value->insert(0, "KARRAY:");
		}
		else {
#ifdef USE_SPARSEHASH
			params->insert(pair<string, managed_ptr<string>*>(name.data(), new managed_ptr<string>(body)));
#else
			params->insert(name.data(), new managed_ptr<string>(body));
#endif
		}
		last = sepBoundary + boundary.length() + 2;
	}
	return params;
}

int kinky::rest::http::KHTTPRequest::getDirective(string* text) {
	return KHTTPRequest::getDirective(text->data());
}

int kinky::rest::http::KHTTPRequest::getDirective(string text) {
	if (text == "PUT") {
		return KHTTPRequest::PUT;
	}
	if (text == "DELETE") {
		return KHTTPRequest::DELETE;
	}
	if (text == "GET") {
		return KHTTPRequest::GET;
	}
	if (text == "POST") {
		return KHTTPRequest::POST;
	}
	if (text == "HEAD") {
		return KHTTPRequest::HEAD;
	}
	if (text == "OPTIONS") {
		return KHTTPRequest::OPTIONS;
	}
	if (text == "SYNC") {
		return KHTTPRequest::SYNC;
	}
	if (text == "APPLY") {
		return KHTTPRequest::APPLY;
	}
	if (text == "PATCH") {
		return KHTTPRequest::PATCH;
	}
	return -1;
}

string kinky::rest::http::KHTTPRequest::getDirective(int code) {
	switch (code) {
		case KHTTPRequest::PUT: {
			return "PUT";
		}
		case KHTTPRequest::DELETE: {
			return "DELETE";
		}
		case KHTTPRequest::GET: {
			return "GET";
		}
		case KHTTPRequest::POST: {
			return "POST";
		}
		case KHTTPRequest::HEAD: {
			return "HEAD";
		}
		case KHTTPRequest::OPTIONS: {
			return "OPTIONS";
		}
		case KHTTPRequest::SYNC: {
			return "SYNC";
		}
		case KHTTPRequest::APPLY: {
			return "APPLY";
		}
		case KHTTPRequest::PATCH: {
			return "PATCH";
		}
	}
	return "";
}

string* kinky::rest::http::KHTTPRequest::getAuthentication(KHTTPRequest* request) {
	string* authHeader = request->getHeader("Authorization");
	if (authHeader != NULL) {
		size_t separator = authHeader->find("OAuth2.0 ");
		if (separator != string::npos) {
			string* token = new string(authHeader->substr(9, authHeader->length() - 9));
			(new kinky::memory::managed_ptr<string>(token));
			return token;
		}
	}
	else {
		authHeader = request->getParam("access_token");
		if (authHeader != NULL) {
			string* token = new string(authHeader->data());
			(new kinky::memory::managed_ptr<string>(token));
			return token;
		}
	}
	return NULL;
}

string* kinky::rest::http::KHTTPRequest::getHost(KHTTPRequest* request, bool with_http) {
	string* instanceroot = new string();
	string* referer = request->getHeader("X-Forwarded-Host");
	if (referer == NULL) {
		referer = request->getHeader("Host");
		if (referer == NULL) {
			referer = request->getHeader("Referer");
			if (referer != NULL) {
				size_t dslash = referer->find("://") + 3;
				size_t slash = referer->find('/', dslash);
				instanceroot->assign(referer->substr(0, slash));
			}
			else {
				instanceroot->assign(request->getHeader("IP-Address")->data());
			}
		}
		else {
			instanceroot->assign(referer->data());
		}
	}
	else {
		instanceroot->assign(referer->data());
	}

	if (with_http) {
		instanceroot->insert(0, "http://");
	}

	(new kinky::memory::managed_ptr<string>(instanceroot));
	return instanceroot;
}

#ifdef USE_SPARSEHASH
string* kinky::rest::http::KHTTPRequest::getHost(KObject* request, bool with_http) {
	string* instanceroot = new string();
	string* referer = request->getHeader("X-Forwarded-Host");
	if (referer == NULL) {
		referer = request->getHeader("Host");
		if (referer == NULL) {
			referer = request->getHeader("Referer");
			if (referer != NULL) {
				size_t dslash = referer->find("://") + 3;
				size_t slash = referer->find('/', dslash);
				instanceroot->assign(referer->substr(dslash, slash));
			}
			else {
				instanceroot->assign(request->getHeader("IP-Address")->data());
			}
		}
		else {
			instanceroot->assign(referer->data());
		}
	}
	else {
		instanceroot->assign(referer->data());
	}

	if (with_http) {
		instanceroot->insert(0, "http://");
	}

	(new kinky::memory::managed_ptr<string>(instanceroot));
	return instanceroot;
}
#endif
