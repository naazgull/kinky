#pragma once

#include <map>
#include <string>
#include <string.h>
#include <sstream>
#include <curl/curl.h>

#ifdef USE_SPARSEHASH
#include <sparsehash/sparse_hash_map>
#include <kinky/lang/KObject.h>
#else
#include <kinky/memory/KMaps.h>
#endif
#include <kinky/memory/KMemoryManager.h>
#include <kinky/utils/KLocale.h>
#include <kinky/log/KLogThread.h>

#ifndef CRLF
#define CRLF "\r\n"
#endif

#ifndef HEADER_SEPARATOR
#define HEADER_SEPARATOR "\r\n\r\n"
#endif

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace memory;

	namespace rest {

		namespace http {
			class KHTTPRequest {
				public:
					const static int PUT = 0;
					const static int DELETE = 1;
					const static int GET = 2;
					const static int POST = 3;
					const static int SYNC = 4;
					const static int APPLY = 5;
					const static int HEAD = 6;
					const static int OPTIONS = 7;
					const static int PATCH = 8;

#ifdef USE_SPARSEHASH
					typedef google::sparse_hash_map<string, managed_ptr<string>*> KHTTPHeaders;
					typedef google::sparse_hash_map<string, managed_ptr<string>*> KHTTPParams;
					typedef google::sparse_hash_map<string, managed_ptr<string>*> KHTTPCookie;
#else
					typedef kinky::memory::str_map<managed_ptr<string>*> KHTTPHeaders;
					typedef kinky::memory::str_map<managed_ptr<string>*> KHTTPParams;
					typedef kinky::memory::str_map<managed_ptr<string>*> KHTTPCookie;
#endif

					KHTTPRequest();

					KHTTPRequest(int directiveParam, string* urlParam, KHTTPRequest::KHTTPHeaders* headersParam, KHTTPRequest::KHTTPParams* paramsParam, string* bodyParam, KHTTPRequest::KHTTPCookie* cookieParam = NULL);

					virtual ~KHTTPRequest();

					string* getURL();

					void setURL(string* url);

					int getDirective();

					void setDirective(int directive);

					string* getHeader(string name);

					KHTTPRequest::KHTTPHeaders* getHeaders();

					void setHeaders(kinky::rest::http::KHTTPRequest::KHTTPHeaders* headers);

					void setHeader(string name, string* value);

					void setBody(string* body);

					void setProtocol(string protocol);

					void removeHeader(string name);

					string* getParam(string name);

					void setParam(string name, string* value);

					void removeParam(string name);

					KHTTPRequest::KHTTPParams* getParams();

					void setParams(KHTTPRequest::KHTTPParams* params);

					string* getCookie(string name);

					KHTTPRequest::KHTTPCookie* getCookies();

					string* getQueryString();

					string* getBody();

					void stringify(ostream& oss, bool onlyheader = false);

					void log(ostream& oss, bool withColors = false);

					void logRequestLine(ostream& oss, bool withColors = false);

					void logRequestLine(kinky::log::KLogStream& oss, bool withColors = false);

					friend ostream& operator<<(ostream& os, KHTTPRequest& f) {
						f.stringify(os);
						return os;
					}

					friend kinky::log::KLogStream& operator<<(kinky::log::KLogStream& os, KHTTPRequest& f) {
						((KHTTPRequest*) &f)->stringify(*os.msg);
						return os;
					}

					static KHTTPRequest* parse(std::ifstream& in);

					static KHTTPRequest* parse(string* reqStr);

					static KHTTPRequest::KHTTPParams* stripParams(string paramsStr, KHTTPRequest::KHTTPParams* params = NULL);

					static KHTTPRequest::KHTTPParams* stripFormData(string boundary, string paramsStr, string tmpPath, KHTTPRequest::KHTTPParams* params = NULL);

					static int getDirective(string* text);

					static int getDirective(string text);

					static string getDirective(int code);

					static string* getAuthentication(KHTTPRequest* request);

					static string* getHost(KHTTPRequest* request, bool with_http = true);

#ifdef USE_SPARSEHASH
					static string* getHost(kinky::lang::KObject* request, bool with_http = true);
#endif

				protected:
					int directive;
					string* url;

				protected:
					KHTTPRequest::KHTTPHeaders* headers;
					KHTTPRequest::KHTTPParams* params;
					KHTTPRequest::KHTTPCookie* cookie;

				private:
					managed_ptr<string>* body;
					string* protocol;
					string* query;
			};
		}
	}
}

