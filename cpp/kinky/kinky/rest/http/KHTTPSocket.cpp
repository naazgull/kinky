#include <kinky/rest/http/KHTTPSocket.h>

kinky::rest::http::KHTTPSocket::KHTTPSocket(int sockfd, sockaddr_in* address, KRest* restParam) :
		KSocket<KHTTPRequest, KHTTPResponse>(sockfd, address), rest(restParam), __processed(NULL) {
}
kinky::rest::http::KHTTPSocket::~KHTTPSocket() {
	if (this->__processed != NULL) {
		delete this->__processed;
	}
}

kinky::memory::managed_ptr<kinky::rest::http::KHTTPRequest>* kinky::rest::http::KHTTPSocket::consume() {
	KHTTPRequest* req = NULL;
	string* reqStr = NULL;

	if (this->__processed != NULL) {
		reqStr = this->__processed;
		this->__processed = NULL;
	}
	else {
		managed_ptr<string>* reqPtr = this->readRequest();
		if (reqPtr == NULL) {
			return NULL;
		}
		reqStr = new string(*((string*) (reqPtr->pointed())));
		reqPtr->release();

	}

	req = KHTTPRequest::parse(reqStr);
	req->setHeader("IP-Address", this->getClientAddress());
	delete reqStr;

	size_t contentLength = 0;
	if (req->getHeader("Content-Length") != NULL) {
		string str(*(req->getHeader("Content-Length")));
		kinky::fromstr(str, &contentLength);
	}

	if (contentLength != 0) {
		string* body = req->getBody();
		if (body == NULL) {
			body = new string();
		}
		time_t initial = time(NULL);
		while (contentLength != body->length()) {
			managed_ptr<string>* requestBody = this->readRequest(contentLength);
			if (requestBody != NULL) {
				body->insert(body->length(), requestBody->get()->data(), requestBody->get()->length());
				requestBody->release();
			}
			else {
				if (initial + KSOCKET_TIMEOUT > time(NULL)) {
					delete req;
					delete body;
					throw new KRestException(KHTTPResponse::REQUEST_TIMEOUT, "Content-Length and request body do not match");
				}
				else {
					sleep(5);
				}
			}
		}
		req->setBody(body);
	}

#ifdef LOG
#ifdef LOG_COLORS
	req->logRequestLine((*kinky::io::out), true);
#else
	req->logRequestLine((*kinky::io::out));
#endif
#endif
	return new managed_ptr<KHTTPRequest>(req);
}

void kinky::rest::http::KHTTPSocket::preconsume() {
	kinky::memory::managed_ptr<string>* reqStr = this->readRequest();
	if (reqStr == NULL) {
		return;
	}

	string* req = new string(*((string*) (reqStr->pointed())));
	reqStr->release();

	size_t contentLength = 0;
	size_t clindex = string::npos;
	if ((clindex = req->find("Content-Length:")) != string::npos) {
		clindex += 16;
		string cl(req->substr(clindex, req->find(CRLF, clindex) - clindex));
		kinky::fromstr(cl, &contentLength);
	}

	if (contentLength != 0) {
		string body;
		size_t head_sep = req->find(HEADER_SEPARATOR);
		if (head_sep != string::npos) {
			body.assign(req->substr(head_sep + 4));
			req->erase(head_sep + 4);
		}

		time_t initial = time(NULL);
		while (contentLength != body.length()) {
			managed_ptr<string>* requestBody = this->readRequest(contentLength);
			if (requestBody != NULL) {
				body.insert(body.length(), requestBody->get()->data(), requestBody->get()->length());
				requestBody->release();
			}
			else {
				if (initial + KSOCKET_TIMEOUT > time(NULL)) {
					delete req;
					throw new KRestException(KHTTPResponse::REQUEST_TIMEOUT, "Content-Length and request body do not match");
				}
				else {
					sleep(5);
				}
			}
		}
		req->insert(req->length(), body.data(), body.length());
	}

	this->__processed = req;
}

kinky::memory::managed_ptr<kinky::rest::http::KHTTPResponse>* kinky::rest::http::KHTTPSocket::process(KHTTPRequest* req) {
	if (req->getHeader("Warning") != NULL && *(req->getHeader("Warning")) == string("601 Assynchronous Request")) {
		KHTTPResponse* rep = new KHTTPResponse();
		KRest::preprocess(rep);
		rep->setCode(KHTTPResponse::ACCEPTED);
		rep->setBody(new string(""));
		req->removeHeader("Warning");

		throw new KRestAssynchronousRequest<KHTTPResponse>(rep);
	}
	return this->rest->process(req);
}

kinky::memory::managed_ptr<kinky::rest::http::KHTTPResponse>* kinky::rest::http::KHTTPSocket::process(KRestException* e) {
	KHTTPResponse* ret = KRest::preprocess(NULL);
	ret->setCode(e->getCode());
	ret->setBody(new string(e->getMessage()->data()));
	return new managed_ptr<KHTTPResponse>(ret);

}

kinky::memory::managed_ptr<kinky::rest::http::KHTTPResponse>* kinky::rest::http::KHTTPSocket::process(exception e) {
	KHTTPResponse* ret = KRest::preprocess(NULL);
	ret->setCode(KHTTPResponse::INTERNAL_SERVER_ERROR);
	ret->setBody(new string(e.what()));
	return new managed_ptr<KHTTPResponse>(ret);
}

kinky::memory::managed_ptr<string>* kinky::rest::http::KHTTPSocket::stringify(managed_ptr<KHTTPResponse>* rep) {
	ostringstream oss;
	oss << **rep << flush;
	return new managed_ptr<string>(new string(oss.str()));
}
