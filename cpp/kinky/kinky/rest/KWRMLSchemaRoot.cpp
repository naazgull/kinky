#include <kinky/rest/KWRMLSchemaRoot.h>

kinky::rest::KWRMLSchemaRoot::KWRMLSchemaRoot(string wrmlURIParam) :
		KRestOperation("^/wrml\\-edit/schemas(/(.+))*$"), wrmlURI(new string(wrmlURIParam.data())) {
}

kinky::rest::KWRMLSchemaRoot::~KWRMLSchemaRoot() {
	delete this->wrmlURI;
}

kinky::lang::KObject* kinky::rest::KWRMLSchemaRoot::getSchema(string url) {
	if (url == "") {
		KObject* result = new KObject(KObject::ARRAY);

		ostringstream file;
		file << *(this->wrmlURI) << "/links.json";

		ifstream in(file.str().data());
		KObject* links = NULL;
		try {
			links = KJSON::parse(in);
		}
		catch (KParseException* e) {
			KRestException* toThrow = new KRestException(KHTTPResponse::INTERNAL_SERVER_ERROR, e->what());
			delete e;
			throw toThrow;
		}

		for (KObject::KObjAttributes::iterator it = links->begin(); it != links->end(); it++) {
			string opURL = (**it)["rel"];
			opURL = opURL.substr(opURL.find('/', 2));

			ostringstream fileRel;
			fileRel << *(this->wrmlURI) << opURL << "/schema.json";

			ifstream inRel(fileRel.str().data());
			try {
				(*result) << KOBJECT(
						"href" << (string) (**it)["href"] <<
						"schema" << KJSON::parse(inRel)
				);
			}
			catch (KParseException* e) {
				delete e;
			}
		}

		delete links;
		return result;
	}
	else {
		ostringstream file;
		file << *(this->wrmlURI) << url << "/schema.json";
		ifstream in(file.str().data());
		try {
			return KJSON::parse(in);
		}
		catch (KParseException* e) {
			delete e;
		}
		return NULL;
	}
}

kinky::lang::KObject* kinky::rest::KWRMLSchemaRoot::addSchema(KObject* schema, string url) {
	KObject* root = NULL;
	KObject* fields = NULL;

	ostringstream file;
	file << *(this->wrmlURI) << url;
	file << "/schema.json";

	ifstream in(file.str().data());
	if (in.is_open()) {
		try {
			root = KJSON::parse(in);
			fields = (*root)["fields"];
		}
		catch (KParseException* e) {
			delete e;
			root = new KObject("json");
			fields = new KObject("json");
			(*root) <<
					"name" << (string) (*schema)["schemaName"] <<
					"version" << 1 <<
					"stateFacts" << KOBJECT_ARRAY(
							"Empty" <<
							"Identifiable"
					) <<
					"fields" << fields;

		}
	}
	else {
		root = new KObject("json");
		fields = new KObject("json");
		(*root) <<
				"name" << (string) (*schema)["schemaName"] <<
				"version" << 1 <<
				"stateFacts" << KOBJECT_ARRAY(
						"Empty" <<
						"Identifiable"
				) <<
				"fields" << fields;
	}

	if ((*schema)["extends"] != NULL_KOBJECT) {
		KObject* schemaExtends = (*schema)["extends"];
		KObject* extends = new KObject(KObject::ARRAY);
		(*root) << "extends" << extends;

		for (KObject::KObjAttributes::iterator i = schemaExtends->begin(); i != schemaExtends->end(); i++) {
			string schemaURI = *static_cast<string*>((*i)->value->pointed());
			schemaURI.insert(0, "/wrml-schemas");
			(*extends) << schemaURI;
		}
	}

	ofstream out(file.str().data());
	if ((*schema)["name"] != NULL_KOBJECT) {
		string name = (*schema)["name"];

		kinky::utils::filesystem::mkdir_recursive(url.data(), 0777, this->wrmlURI->data());

		KObject* add = KOBJECT(
				"name" << (string) (*schema)["name"] <<
				"type" << (string) (*schema)["type"] <<
				"defaultValue" << (string) (*schema)["defaultValue"] <<
				"readOnly" << (bool) (*schema)["readOnly"] <<
				"required" << (bool) (*schema)["required"] <<
				"hidden" << (bool) (*schema)["hidden"] <<
				"description" << (string) (*schema)["description"]
		);
		if ((*schema)["responseTypes"] != NULL_KOBJECT) {
			(*add) << "responseTypes" << ((KObject*) (*schema)["responseTypes"]);
		}
		if ((*schema)["requestTypes"] != NULL_KOBJECT) {
			(*add) << "requestTypes" << ((KObject*) (*schema)["requestTypes"]);
		}
		if ((*schema)["constraints"] != NULL_KOBJECT) {
			(*add) << "constraints" << ((KObject*) (*schema)["constraints"]);
		}
		if ((*schema)["options"] != NULL_KOBJECT) {
			(*add) << "options" << ((KObject*) (*schema)["options"]);
		}

		(*fields) << name << add;
	}

	out << *root << flush;
	out.close();

	KObject* ret = KOBJECT(
			"links" << KOBJECT(
					"href" << ((string) (*schema)["href"])
			)
	);
	ret->addHeader("Location", (*schema)["href"]);
	ret->setStatus(KHTTPResponse::CREATED);
	return ret;
}

kinky::lang::KObject* kinky::rest::KWRMLSchemaRoot::updateSchema(KObject* schema, string url) {
	KObject* ret = this->addSchema(schema, url);
	ret->setStatus(KHTTPResponse::OK);
	return ret;
}

kinky::lang::KObject* kinky::rest::KWRMLSchemaRoot::deleteSchema(KObject* schema, string url) {
	KObject* root = NULL;
	KObject* fields = NULL;

	ostringstream file;
	file << *(this->wrmlURI) << url;
	file << "/schema.json";

	ifstream in(file.str().data());
	if (in.is_open()) {
		try {
			root = KJSON::parse(in);
			fields = (*root)["fields"];
		}
		catch (KParseException* e) {
			delete e;
			root = new KObject("json");
			fields = new KObject("json");
			(*root) <<
					"name" << (string) (*schema)["schemaName"] <<
					"version" << 1 <<
					"stateFacts" << KOBJECT_ARRAY(
							"Empty" <<
							"Identifiable"
					) <<
					"fields" << fields;

		}
	}
	else {
		root = new KObject("json");
		fields = new KObject("json");
		(*root) <<
				"name" << (string) (*schema)["schemaName"] <<
				"version" << 1 <<
				"stateFacts" << KOBJECT_ARRAY(
						"Empty" <<
						"Identifiable"
				) <<
				"fields" << fields;
	}

	ofstream out(file.str().data());
	if ((*schema)["name"] != NULL_KOBJECT) {
		string name = (*schema)["name"];
		fields->unset((string) (*schema)["name"]);
	}

	out << *root << flush;
	out.close();

	KObject* ret = KHTTP_OK();
	ret->addHeader("Location", (*schema)["href"]);
	return ret;
}

kinky::lang::KObject* kinky::rest::KWRMLSchemaRoot::process(KObject* request, int directive, string* url, KToken* token) {
	switch (directive) {
		case KHTTPRequest::GET: {

			// >>>
			// Document Root is requested, return service list
			if (url->find('/', 12) == string::npos) {
				return this->getSchema();
			}
			// <<<

			// >>>
			// Schema Link for a given operation is request, return schema description
			else {
				return this->getSchema(url->substr(18));
			}
			// <<<

			return NULL;
		}
		case KHTTPRequest::POST: {

			// >>>
			// Schema Link for a given operation is request, return schema description
			return this->addSchema(request, url->substr(18));
			// <<<
		}
		case KHTTPRequest::PUT: {

			// >>>
			// Document Root is requested, return service list
			return this->updateSchema(request, url->substr(18));
			// <<<
		}
		case KHTTPRequest::DELETE: {

			// >>>
			// Document Root is requested, return service list
			return this->deleteSchema(request, url->substr(18));
			// <<<
		}
		default: {
			throw new KRestException(KHTTPResponse::METHOD_NOT_ALLOWED, "the provided path '" + *url + "' could not be found");
		}
	}
}
