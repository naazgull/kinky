#pragma once

#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <kinky/memory/KMemoryManager.h>
#include<errno.h>
#include <string.h>
#include <gperftools/malloc_extension.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace memory;

	namespace rest {

		namespace thread {
			template<class T>
			class KThread {
				public:
					KThread(T* dataParam);
					virtual ~KThread();

					static void* start(void* thread);
					static void* init(void* thread);
					T* getTarget();
					size_t pending();
					void run();
					void initialize(size_t id, size_t max, string semkey, int project_id = -1);
					virtual void execute() = 0;
					void set(size_t id, size_t max, string semkey, int project_id = -1);
					void wait(int seconds);
					void wait();
					void exec(T* dataParam);
					void exec();
					virtual void printm(const char* prefix = NULL);
				public:
					T* data;
					int id;
					int max;
					string semkey;
					int project_id;

				private:
					vector<T*>* stack;

				public:
					pthread_mutex_t* mutex;

				private:
					pthread_t* thread;
					pthread_mutexattr_t attr;
					int sem;
			};
		}
	}
}

template<class T>
kinky::rest::thread::KThread<T>::KThread(T* dataParam) :
		data(dataParam) {
	this->mutex = new pthread_mutex_t();
	this->thread = new pthread_t();
	pthread_mutexattr_init(&this->attr);
	pthread_mutex_init(this->mutex, &this->attr);
	this->stack = NULL;
	if (dataParam == NULL) {
		this->stack = new vector<T*>();
	}
}

template<class T>
kinky::rest::thread::KThread<T>::~KThread() {
	if (this->data != NULL) {
		delete this->data;
	}
	if (this->stack != NULL) {
		delete this->stack;
	}
	pthread_mutexattr_destroy(&this->attr);
	pthread_mutex_destroy(this->mutex);
	delete this->mutex;
	delete this->thread;
	pthread_exit(NULL);
}

template<class T>
void* kinky::rest::thread::KThread<T>::start(void* thread) {
	KThread* running = static_cast<KThread*>(thread);

	KMemoryManager::alloc();
	running->execute();
	KMemoryManager::getMemory()->gc();
	MallocExtension::instance()->ReleaseFreeMemory();
	running->printm();
	KMemoryManager::dealloc();
	delete running;
	return NULL;
}

template<class T>
void* kinky::rest::thread::KThread<T>::init(void* thread) {
	KThread* running = static_cast<KThread*>(thread);

	KMemoryManager::alloc();
	struct sembuf ops[1] = { { (short unsigned int) running->id, -1 } };
	while (true) {
		semop(running->sem, ops, 1);

		pthread_mutex_lock(running->mutex);
		running->data = NULL;
		if (running->stack->size() != 0) {
			running->data = running->stack->at(0);
			running->stack->erase(running->stack->begin());
		}
		pthread_mutex_unlock(running->mutex);
		if (running->data != NULL) {
			KMemoryManager::getMemory()->gc();
			running->execute();
			if (running->data != NULL) {
				delete running->data;
			}
			running->data = NULL;
			KMemoryManager::getMemory()->gc();
			MallocExtension::instance()->ReleaseFreeMemory();
			running->printm("\n\n>> AFTER CLEAN: ");
		}

	}
	KMemoryManager::dealloc();
	delete running;

	return NULL;
}

template<class T>
void kinky::rest::thread::KThread<T>::printm(const char* prefix) {
#ifdef DEBUG_MEMORY
	if (prefix != NULL) {
		cout << prefix << flush;
	}
	KMemoryManager::getMemory()->status();
#endif
}

template<class T>
T* kinky::rest::thread::KThread<T>::getTarget() {
	return this->data;
}

template<class T>
size_t kinky::rest::thread::KThread<T>::pending() {
	pthread_mutex_lock(this->mutex);
	if (this->stack == NULL) {
		return 0;
	}
	size_t ret = this->stack->size();
	pthread_mutex_unlock(this->mutex);
	return ret;
}

template<class T>
void kinky::rest::thread::KThread<T>::run() {
	pthread_create(this->thread, 0, &KThread::start, this);
}

template<class T>
void kinky::rest::thread::KThread<T>::initialize(size_t id, size_t max, string semkey, int project_id) {
	if (project_id == -1) {
		project_id = this->max;
	}
	this->id = id;
	this->max = max;
	this->semkey = semkey;
	this->project_id = project_id;
	key_t key = ftok(this->semkey.data(), this->project_id);
	this->sem = semget(key, this->max, IPC_CREAT | 0777);
	if (this->sem == 0) {
	}
	pthread_create(this->thread, 0, &KThread::init, this);
}

template<class T>
void kinky::rest::thread::KThread<T>::set(size_t id, size_t max, string semkey, int project_id) {
	if (project_id == -1) {
		project_id = this->max;
	}
	this->id = id;
	this->max = max;
	this->semkey = semkey;
	this->project_id = project_id;
	key_t key = ftok(this->semkey.data(), this->project_id);
	this->sem = semget(key, this->max, IPC_CREAT | 0777);
	if (this->sem == 0) {
	}
}

template<class T>
void kinky::rest::thread::KThread<T>::exec(T* dataParam) {
	pthread_mutex_lock(this->mutex);
	this->stack->push_back(dataParam);
	pthread_mutex_unlock(this->mutex);

	struct sembuf ops[1] = { { (short unsigned int) this->id, 1 } };
	semop(this->sem, ops, 1);
}

template<class T>
void kinky::rest::thread::KThread<T>::exec() {
	struct sembuf ops[1] = { { (short unsigned int) this->id, 1 } };
	semop(this->sem, ops, 1);
}

template<class T>
void kinky::rest::thread::KThread<T>::wait() {
	struct sembuf ops[1] = { { (short unsigned int) this->id, -1 } };
	semop(this->sem, ops, 1);
}

template<class T>
void kinky::rest::thread::KThread<T>::wait(int seconds) {
	sleep(seconds);
}

