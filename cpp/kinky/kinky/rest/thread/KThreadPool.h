#pragma once

#include <unistd.h>
#include <vector>
#include <kinky/rest/thread/KThread.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace memory;

	namespace rest {

		namespace thread {

			template<class T>
			class KThreadPool {
				public:
					KThreadPool(size_t max = 1000);
					virtual ~KThreadPool();

					size_t getMaxThreads();

				private:
					size_t max;
			};
		}
	}
}

template<class T>
kinky::rest::thread::KThreadPool<T>::KThreadPool(size_t maxParam) :
		max(maxParam){
}

template<class T>
kinky::rest::thread::KThreadPool<T>::~KThreadPool() {
}

template<class T>
size_t kinky::rest::thread::KThreadPool<T>::getMaxThreads() {
	return this->max;
}
