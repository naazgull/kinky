#pragma once

#include <kinky/lang/KObject.h>
#include <kinky/oauth/KToken.h>
#include <kinky/rest/KRest.h>
#include <kinky/rest/KRestOperation.h>
#include <kinky/utils/KFileSystem.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace oauth;

	namespace rest {

		class KWRMLSchemaRoot: public KRestOperation {
			public:

				KWRMLSchemaRoot(string wrmlURIParam);
				virtual ~KWRMLSchemaRoot();

				virtual KObject* getSchema(string url = "");

				virtual KObject* addSchema(KObject* schema, string url);

				virtual KObject* updateSchema(KObject* schema, string url);

				virtual KObject* deleteSchema(KObject* schema, string url);

				virtual KObject* process(KObject* request, int directive, string* url, KToken* token);

			protected:
				string* wrmlURI;
		};
	}
}
