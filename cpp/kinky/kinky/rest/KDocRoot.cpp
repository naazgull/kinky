#include <kinky/rest/KDocRoot.h>

kinky::rest::KDocRoot::KDocRoot(string wrmlURIParam) :
		KRestOperation("(^/$)|(^/wrml\\-links/(.+)$)|(^/wrml\\-relations/(.+)$)|(^/wrml\\-schemas((/(.+))*)$)"), wrmlURI(new string(wrmlURIParam.data())) {
}

kinky::rest::KDocRoot::~KDocRoot() {
	delete this->wrmlURI;
}

kinky::lang::KObject* kinky::rest::KDocRoot::getResourceLinks(string url, bool notflat) {
	if (!notflat) {
		KObject* root = new KObject("json");
		vector<string> linkFiles;
		kinky::utils::filesystem::glob(*this->wrmlURI, linkFiles, "links\\.json", true);
		for (vector<string>::iterator i = linkFiles.begin(); i != linkFiles.end(); i++) {
			ifstream in(i->data());
			if (in.is_open()) {
				KObject* links = NULL;
				try {
					links = KJSON::parse(in);
				}
				catch (KParseException* e) {
					delete e;
				}
				if (links == NULL) {
					delete root;
					return NULL;
				}
				for (KObject::KObjAttributes::iterator j = links->begin(); j != links->end(); j++) {
					(*root) << (*j)->key << ((KObject*) (*j)->value->pointed());
				}
				delete links;
			}
		}
		return root;
	}
	else {
		ostringstream file;
		file << *(this->wrmlURI) << url << "/links.json";

		ifstream in(file.str().data());
		if (!in.is_open()) {
			return NULL;
		}
		try {
			return KJSON::parse(in);
		}
		catch (KParseException* e) {
			KRestException* toThrow = new KRestException(KHTTPResponse::INTERNAL_SERVER_ERROR, e->what());
			delete e;
			throw toThrow;
		}
	}
	return NULL;
}

kinky::lang::KObject* kinky::rest::KDocRoot::getResourceSchema(string url, bool notflat) {
	if (url == "") {
		KObject* root = new KObject(KObject::ARRAY, "json");
		vector<string> linkFiles;
		kinky::utils::filesystem::glob(*this->wrmlURI, linkFiles, "schema\\.json", true);
		ostringstream oss;
		for (vector<string>::iterator i = linkFiles.begin(); i != linkFiles.end(); i++) {
			oss.str("");
			oss << "/wrml-schemas" <<  i->substr(this->wrmlURI->length(), i->length() - this->wrmlURI->length() - 12);
			(*root) << oss.str();
		}
		return root;
	}
	else {
		ostringstream file;
		file << *(this->wrmlURI) << url << "/schema.json";

		ifstream in(file.str().data());
		if (!in.is_open()) {
			return NULL;
		}
		try {
			KObject* schema = KJSON::parse(in);
			this->resolveDependencies(schema);
			return schema;
		}
		catch (KParseException* e) {
			KRestException* toThrow = new KRestException(KHTTPResponse::INTERNAL_SERVER_ERROR, e->what());
			delete e;
			throw toThrow;
		}
		return NULL;
	}
}

kinky::lang::KObject* kinky::rest::KDocRoot::getResourceRelations(string url, bool notflat) {
	if (url == "") {
		KObject* result = new KObject();

		ostringstream file;
		file << *(this->wrmlURI) << "/links.json";

		ifstream in(file.str().data());
		KObject* links = NULL;
		try {
			links = KJSON::parse(in);
		}
		catch (KParseException* e) {
			KRestException* toThrow = new KRestException(KHTTPResponse::INTERNAL_SERVER_ERROR, e->what());
			delete e;
			throw toThrow;
		}

		for (KObject::KObjAttributes::iterator it = links->begin(); it != links->end(); it++) {
			string opURL = (**it)["rel"];
			opURL = opURL.substr(opURL.find('/', 2));

			ostringstream fileRel;
			fileRel << *(this->wrmlURI) << opURL << "/relation.json";

			ifstream inRel(fileRel.str().data());
			try {
				KObject* rel = KJSON::parse(inRel);
				(*result) << ((string) (*rel)["name"]) << KOBJECT(
						"href" << (string) (**it)["href"] <<
						"rel" << rel
				);
			}
			catch (KParseException* e) {
				delete e;
			}
		}

		delete links;
		return result;
	}
	else {
		ostringstream file;
		file << *(this->wrmlURI) << url << "/relation.json";

		ifstream in(file.str().data());
		if (!in.is_open()) {
			return NULL;
		}
		try {
			return KJSON::parse(in);
		}
		catch (KParseException* e) {
			KRestException* toThrow = new KRestException(KHTTPResponse::INTERNAL_SERVER_ERROR, e->what());
			delete e;
			throw toThrow;
		}
		return NULL;
	}
}

kinky::lang::KObject* kinky::rest::KDocRoot::addResource(KObject* resource) {
	string url = (*resource)["href"];
	KObject* root = NULL;
	KObject* add = NULL;
	url = url.substr(0, url.find_last_of('/'));

	ostringstream file;
	file << *(this->wrmlURI) << url;
	file << "/links.json";

	ifstream in(file.str().data());
	if (in.is_open()) {
		try {
			root = KJSON::parse(in);
		}
		catch (KParseException* e) {
			KRestException* toThrow = new KRestException(KHTTPResponse::INTERNAL_SERVER_ERROR, e->what());
			delete e;
			throw toThrow;
		}

		for (KObject::KObjAttributes::iterator it = root->begin(); it != root->end(); it++) {
			if ((*it)->key == ((string) (*resource)["name"])) {
				add = (KObject*) (*root)[(*it)->key];
				break;
			}
		}
		if (add == NULL) {
			add = new KObject();
			(*root) << ((string) (*resource)["name"]) << add;
		}
	}
	else {
		add = new KObject();
		root = new KObject();
		(*root) << ((string) (*resource)["name"]) << add;
	}

	ostringstream relss;
	relss << "/wrml-relations" << (*resource)["href"];/* << "/" << directive;*/
	ostringstream scss;
	scss << "application/wrml;schema=\"/wrml-schemas/object\"";

	add->setString("name", (*resource)["name"]);
	add->setString("href", (*resource)["href"]);
	add->setString("rel", relss.str());
	add->setString("title", (*resource)["title"]);
	add->setString("description", (*resource)["description"]);
	add->setObject("requestTypes", KOBJECT_ARRAY(
			scss.str() <<
			"application/json" <<
			"application/xml"
	));
	add->setObject("responseTypes", KOBJECT_ARRAY(
			"application/json" <<
			"application/xml" <<
			"application/vnd.google-earth.kml+xml"
	));

	kinky::utils::filesystem::mkdir_recursive(url.data(), 0777, this->wrmlURI->data());

	ofstream out(file.str().data());
	out << *root << flush;
	out.close();

	delete root;

	KObject* ret = KOBJECT(
		"links" << KOBJECT(
			"href" << ((string) (*resource)["href"]) <<
			"rel" << relss.str()
		)
	);
	ret->addHeader("Location", ((string) (*resource)["href"]));
	ret->setStatus(KHTTPResponse::CREATED);
	return ret;
}

kinky::lang::KObject* kinky::rest::KDocRoot::updateResource(KObject* resource) {
	KObject* ret = this->addResource(resource);
	ret->setStatus(KHTTPResponse::OK);
	return ret;
}

kinky::lang::KObject* kinky::rest::KDocRoot::deleteResource(KObject* resource) {
	return NULL;
}

kinky::lang::KObject* kinky::rest::KDocRoot::process(KObject* request, int directive, string* url, KToken* token) {
	switch (directive) {
		case KHTTPRequest::GET: {
			bool notflat = (*request)["flat"] == NULL_KOBJECT || !((bool) (*request)["flat"]);

			// >>>
			// Document Root is requested, return service list
			if (*url == "/") {
				KObject* links = this->getResourceLinks("/", notflat);
				if (links != NULL) {
					KObject* reply = new KObject();
					(*reply) << "links" << links;
					reply->addHeader("Pragma", "no-cache");
					reply->addHeader("Cache-Control", "no-store");
					return reply;
				}
				else {
					return NULL;
				}
			}
			// <<<

			// >>>
			// Links for a given operation is request, return links list
			else if (url->find(kinky::rest::WRML_LINKS) != string::npos) {
				string opURL = url->substr(url->find('/', 2));
				KRestOperation* op = KRest::getOperation(opURL);
				KObject* links = NULL;

				if (op != NULL) {
					links = op->getResourceLinks(NULL, request, opURL, directive);
				}
				if (links == NULL) {
					links = this->getResourceLinks(opURL);
				}
				if (links != NULL) {
					links = KOBJECT("links" << links);
					links->addHeader("Pragma", "no-cache");
					links->addHeader("Cache-Control", "no-store");
				}
				return links;
			}
			// <<<

			// >>>
			// Relation Link for a given operation is request, return relation description
			else if (url->find(kinky::rest::WRML_RELATIONS) != string::npos) {
				string opURL = url->substr(url->find('/', 2));
				KObject* linkRelation = NULL;

				linkRelation = this->getResourceRelations(opURL, notflat);
				if (linkRelation != NULL) {
					linkRelation->addHeader("Pragma", "no-cache");
					linkRelation->addHeader("Cache-Control", "no-store");
				}
				return linkRelation;
			}
			// <<<

			// >>>
			// Schema Link for a given operation is request, return schema description
			else if (url->find(kinky::rest::WRML_SCHEMAS) != string::npos) {
				size_t slash = url->find('/', 2);
				string opURL = slash != string::npos ? url->substr(slash) : "";
				KObject* linkSchema = NULL;

				linkSchema = this->getResourceSchema(opURL, notflat);
				if (linkSchema != NULL) {
					linkSchema->addHeader("Pragma", "no-cache");
					linkSchema->addHeader("Cache-Control", "no-store");
				}
				return linkSchema;
			}
			// <<<

			return NULL;
		}
		case KHTTPRequest::POST: {

			// >>>
			// Adds a new resource to the system
			return this->addResource(request);
			// <<<

		}
		case KHTTPRequest::PUT: {

			// >>>
			// Changes a resource from the system
			return this->updateResource(request);
			// <<<

		}
		case KHTTPRequest::DELETE: {

			// >>>
			// Deletes a resource from the system
			return this->deleteResource(request);
			// <<<

		}
		case KHTTPRequest::HEAD: {

			// >>>
			// Document Root is requested, return service list
			return KHTTP_OK();
			// <<<

		}
		default: {
			throw new KRestException(KHTTPResponse::METHOD_NOT_ALLOWED, "the provided HTTP method is not allowed for this uri");
		}
	}
}

void kinky::rest::KDocRoot::resolveDependencies(KObject* schema) {
	if ((*schema)["extends"] != NULL_KOBJECT) {
		KObject* extends = (*schema)["extends"];
		KObject* baseFields = (*schema)["fields"];

		for (KObject::KObjAttributes::iterator i = extends->begin(); i != extends->end(); i++) {
			size_t slash = static_cast<string*>((*i)->value->pointed())->find('/', 2);
			if (slash != string::npos) {
				string opURL = static_cast<string*>((*i)->value->pointed())->substr(slash);
				ostringstream file;
				file << *(this->wrmlURI) << opURL << "/schema.json";

				ifstream in(file.str().data());
				if (in.is_open()) {
					try {
						KObject* superSchema = KJSON::parse(in);
						this->resolveDependencies(superSchema);

						if ((*superSchema)["fields"] != NULL_KOBJECT) {
							KObject* fields = (*superSchema)["fields"];
							for (KObject::KObjAttributes::iterator j = fields->begin(); j != fields->end(); j++) {
								(*baseFields) << (*j)->key << static_cast<KObject*>((*j)->value->pointed());
							}
						}
					}
					catch (KParseException* e) {
						delete e;
					}
				}
			}
		}
	}
}
