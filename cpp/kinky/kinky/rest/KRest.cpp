#include <kinky/rest/KRest.h>

#include <magic.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <memory.h>
#include <kinky/utils/KDateTime.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <time.h>
#include <kinky/rest/http/KCurl.h>
#include <curl/curl.h>

#ifdef MOD_DOCS
#include <kinky/rest/KDocRoot.h>
#include <kinky/rest/KWRMLRelationRoot.h>
#include <kinky/rest/KWRMLSchemaRoot.h>
#endif

const char* kinky::rest::WRML_SCOPES = "/wrml-scopes/";
const char* kinky::rest::WRML_LINKS = "/wrml-links/";
const char* kinky::rest::WRML_RELATIONS = "/wrml-relations/";
const char* kinky::rest::WRML_SCHEMAS = "/wrml-schemas";
const char* kinky::rest::WRML_EDIT = "/wrml-edit/";
const char* kinky::rest::WRML_MODULES = "/wrml-modules";
const char* kinky::rest::DOC_ROOT = "/";
const char* kinky::rest::KINKY_LOG = "/kinky/log";
void* kinky::rest::k_rest;
struct in_addr *kinky::rest::in_addr = new struct in_addr();

kinky::rest::KRest::KRest() :
	libs(new KRest::KRestLibTable()), version(NULL), hostname(NULL), port(NULL) {
	KMemoryManager::init();
	KMemoryManager::alloc();
	k_rest = this;
	curl_global_init(CURL_GLOBAL_ALL);
	kinky::initSSLMutex();

	ostringstream oss;
	oss << "Kinky Rest API " << KVERSION << " (build time: " << __DATE__ << " " << __TIME__ << ")";
	this->version = new string(oss.str());

	struct ifaddrs *myaddrs, *ifa;

	if (getifaddrs(&myaddrs) != 0) {
	}

	for (ifa = myaddrs; ifa != NULL; ifa = ifa->ifa_next) {
		if (ifa->ifa_addr == NULL) {
			continue;
		}
		if (!(ifa->ifa_flags & IFF_UP)) {
			continue;
		}

		switch (ifa->ifa_addr->sa_family) {
			case AF_INET: {
				struct sockaddr_in *s4 = (struct sockaddr_in *) ifa->ifa_addr;
				kinky::rest::in_addr->s_addr = s4->sin_addr.s_addr;
				break;
			}
			default:
				continue;
		}

	}
	freeifaddrs(myaddrs);

	this->log_user = new string("");
	this->log_pass = new string("");
}

kinky::rest::KRest::~KRest() {
	for (KRest::KRestLibTable::iterator i = this->libs->begin(); i != this->libs->end(); i++) {
		dlclose(i->second->getLibHandle());
		delete i->second;
	}
	delete this->libs;
	KMemoryManager::dealloc();
	curl_global_cleanup();
	kinky::deinitSSLMutex();
	delete this->version;
	if (this->hostname != NULL) {
		delete this->hostname;
	}
	if (this->port != NULL) {
		delete this->port;
	}

	delete this->log_user;
	delete this->log_pass;
}

void kinky::rest::KRest::registerApplication(string config) {
	ifstream* confFile = new ifstream();
	confFile->open(config.data(), ios::in);

	if (confFile->is_open()) {
		string line;

		while (confFile->good()) {
			std::getline(*confFile, line, '=');
			string apiStr(line.data());

			std::getline(*confFile, line);
			string apiConfStr(line.data());

			string* libFile = new string("lib");
			libFile->append(apiStr.data());
			libFile->append(".so");

			if (libFile->length() > 6) {
				void *hndl = dlopen(libFile->data(), RTLD_NOW);
				if (hndl == NULL) {
					cerr << dlerror() << endl;
				}
				else {
					KRestAPI* (*init)();
					init = (KRestAPI* (*)()) dlsym(hndl, "initAPI");

KRestAPI					* api = (KRestAPI*) init();
					api->setLibName(apiStr.data());
					api->setLibHandle(hndl);
					api->setConfFile(apiConfStr.data());
					api->init();

					KRestOperation* docRootOp = api->getDocRoot(true);
					if (docRootOp != NULL) {
						api->registerOperation(docRootOp);
					}
					KRestOperation* relationsOp = api->getRelationRoot(true);
					if (relationsOp != NULL) {
						api->registerOperation(relationsOp);
					}
					KRestOperation* schemasOp = api->getSchemaRoot(true);
					if (schemasOp != NULL) {
						api->registerOperation(schemasOp);
					}

					if (api->getConfiguration()->getString("krest_log_user") != NULL) {
						this->log_user->assign(api->getConfiguration()->get("krest_log_user").data());
						this->log_pass->assign(api->getConfiguration()->get("krest_log_pass").data());
					}

					kinky::utils::trim(apiStr);
					this->libs->insert(pair<string, KRestAPI*>(apiStr, api));
				}
			}
			delete libFile;
		}
		confFile->close();
	}
	delete confFile;
}

const char* kinky::rest::KRest::getVersion() {
	return this->version->data();
}

KRest* kinky::rest::KRest::instance() {
	return (KRest*) (kinky::rest::k_rest);
}

void kinky::rest::KRest::unregisterApplication(string config) {
	ifstream* confFile = new ifstream();
	confFile->open(config.data(), ios::in);

	if (confFile->is_open()) {
		string line;

		while (confFile->good()) {
			std::getline(*confFile, line);
			string apiStr(line.data());
			KRestLibTable::iterator api = this->libs->find(apiStr);
			if (api != this->libs->end()) {
				delete api->second;
				this->libs->erase(api);
			}
		}
		confFile->close();
	}
	delete confFile;
}

KRestAPI* kinky::rest::KRest::getApplication(string api) {
	KRest::KRestLibTable::iterator found = this->libs->find(api);
	if (found != this->libs->end()) {
		return found->second;
	}
	return NULL;
}

kinky::lang::KObject* kinky::rest::KRest::exec(int directive, string* server, string *url, KToken& token, KObject* body, bool assync) {
	ostringstream tkss;
	tkss << token << flush;
	string stoken = tkss.str();
	return KRest::exec(directive, *server, *url, &stoken, body, assync);
}

kinky::lang::KObject* kinky::rest::KRest::exec(int directive, string server, string url, KToken& token, KObject* body, bool assync) {
	ostringstream tkss;
	tkss << token << flush;
	string stoken = tkss.str();
	return KRest::exec(directive, server, url, &stoken, body, assync);
}

kinky::lang::KObject* kinky::rest::KRest::exec(int directive, string* server, string *url, string *token, KObject* body, bool assync) {
	return KRest::exec(directive, *server, *url, token, body, assync);
}

kinky::lang::KObject* kinky::rest::KRest::exec(int directive, string server, string url, string* token, KObject* body, bool assync) {
	size_t begin = server.find("://") + 3;
	size_t end = server.find(":", begin);
	string port;
	if (end == string::npos) {
		end = server.find("/", begin);
		if (end == string::npos) {
			end = server.length();
		}
	}
	else {
		port.assign(server.substr(end + 1));
		if (port.find('/') != string::npos) {
			port.erase(port.length() - 1, 1);
		}
	}
	string hostname = server.substr(begin, end - begin);

	if ((hostname == *static_cast<KRest*>(k_rest)->hostname || hostname == "localhost") && *static_cast<KRest*>(k_rest)->port == port) {
		bool deleteBody = false;
		size_t interr = 0;
		if (body == NULL) {
			body = new KObject();
			deleteBody = true;
		}
		body->addHeader("IP-Address", "127.0.0.1");
		body->addHeader("X-Request-On-Behalf", *kinky::memory::KMemoryManager::getMemory()->currentip);
		string httphost(hostname.data());
		httphost.insert(httphost.length(), ":");
		httphost.insert(httphost.length(), port);
		body->addHeader("Host", httphost);
		string realURL;
		if ((interr = url.find("?")) != string::npos) {
			KHTTPRequest::KHTTPParams* params = KHTTPRequest::stripParams(url.substr(interr + 1), NULL);
			KHTTPRequest::KHTTPParams::iterator param;

			if ((param = params->find("access_token")) != params->end()) {
				string auth("OAuth2.0 ");
				auth.insert(auth.length(), *param->second->get());
				body->addHeader("Authorization", auth);
				param->second->release();
				params->erase(param);
			}
			if ((param = params->find("callback")) != params->end()) {
				body->addHeader("X-JSONP-Callback", *param->second->get());
				param->second->release();
				params->erase(param);
			}
			if ((param = params->find("embed")) != params->end()) {
				body->addHeader("X-Rest-Embed", *param->second->get());
				param->second->release();
				params->erase(param);
			}
			if ((param = params->find("fields")) != params->end()) {
				body->addHeader("X-Rest-Fields", *param->second->get());
				param->second->release();
				params->erase(param);
			}

			KRest::initRequestParams(body, params);
			realURL.assign(url.substr(0, interr));
		}
		else {
			realURL.assign(url);
		}
		body->setDirective(directive);
		KObject* reply = static_cast<KRest*>(k_rest)->processLocal(directive, realURL, token, body);
		if (deleteBody) {
			delete body;
		}
		if (!assync) {
			return reply;
		}
		else {
			delete reply;
			return NULL;
		}
	}

	KObject* toReturn = NULL;
	string* bodyStr = NULL;
	string uri(url);

	if (directive == KHTTPRequest::GET && body != NULL) {
		ostringstream uriss;
		bool first = true;
		for (KObject::KObjAttributes::iterator i = body->begin(); i != body->end(); i++) {
			if (!first) {
				uriss << "&";
			}
			first = false;
			uriss << (*i)->key << "=";
			switch ((*i)->type) {
				case KObject::OBJECT: {
					uriss << *static_cast<KObject*>((*i)->value->pointed());
					break;
				}
				case KObject::ARRAY: {
					uriss << *static_cast<KObject*>((*i)->value->pointed());
					break;
				}
				case KObject::BOOL: {
					uriss << *static_cast<bool*>((*i)->value->pointed());
					break;
				}
				case KObject::INT: {
					uriss << *static_cast<int*>((*i)->value->pointed());
					break;
				}
				case KObject::DOUBLE: {
					uriss << *static_cast<double*>((*i)->value->pointed());
					break;
				}
				case KObject::STRING: {
					uriss << *static_cast<string*>((*i)->value->pointed());
					break;
				}
				case KObject::NIL: {
					uriss << "null";
					break;
				}
			}
		}
		string qstring(uriss.str());
		if (qstring.length() != 0) {
			uri.append("?");
			uri.append(qstring);
		}
	}
	else if (body != NULL) {
		body->addHeader("Content-Type", "application/json");
		ostringstream oss;
		oss << *body << flush;
		bodyStr = new string(oss.str());
	}

	kinky::rest::http::KHTTPRequest::KHTTPHeaders* headers = NULL;
	if (body != NULL) {
		headers = body->getHeaders();
	}
	if (token != NULL) {
		if (headers == NULL) {
			headers = new kinky::rest::http::KHTTPRequest::KHTTPHeaders();
		}
		string* oauth = new string(*token);
		oauth->insert(0, "OAuth2.0 ");
		headers->insert("authorization", new managed_ptr<string>(oauth));
	}

	int responseCode = 0;
	string* replyStr = kinky::rest::http::curl(&responseCode, directive, server, uri, bodyStr, headers, NULL, assync);

	if (!assync) {
		if (replyStr != NULL) {
			try {
				toReturn = KJSON::parse(replyStr);
			}
			catch (KParseException* e) {
				toReturn = new KObject("json");
				delete e;
			}
		}
		else {
			toReturn = new KObject("json");
		}
		toReturn->setStatus(responseCode);
	}

	if (replyStr != NULL) {
		delete replyStr;
	}

	if (bodyStr != NULL) {
		delete bodyStr;
	}

	return toReturn;
}

kinky::lang::KObject* kinky::rest::KRest::processLocal(int directive, string url, string* accessToken, KObject* request) {
#ifdef LOG_INTERNAL
	ostringstream oss;

#ifdef LOG_COLORS
	(*kinky::io::out) << "\033[1;30m";
	(*kinky::io::out) << "[T" << (unsigned int) pthread_self() << " IP127.0.0.1" << " " << kinky::utils::date::format("D%Y-%m-%dT%H:%M:%S") << " REQ. INTERNAL] " << KHTTPRequest::getDirective(directive) << " " << url << flush;
	(*kinky::io::out) << "\033[0m" << flush;
	(*kinky::io::out) << endl << flush;
#else
	(*kinky::io::out) << "[T" << (unsigned int) pthread_self() << " IP127.0.0.1" << " " << kinky::utils::date::format("D%Y-%m-%dT%H:%M:%S") << " REQ. INTERNAL] " << KHTTPRequest::getDirective(directive) << " " << url << flush;
	(*kinky::io::out)<< endl << flush;
#endif
#endif

#ifdef LOG_INTERNAL
	clock_t start = clock();

#ifdef LOG_COLORS
	oss << "\033[1;35m";
	oss << "[T" << (unsigned int) pthread_self() << " IP127.0.0.1 " << kinky::utils::date::format("D%Y-%m-%dT%H:%M:%S") << " ] ";
	oss << "\033[0m" << flush;

	oss << "\033[1;30m";
	oss << KHTTPRequest::getDirective(directive) << " " << (url);
	oss << " INTERNAL/0.0" << flush;
	oss << "\033[0m" << flush;
#else
	oss << "[T" << (unsigned int) pthread_self() << " IP127.0.0.1 " << kinky::utils::date::format("D%Y-%m-%dT%H:%M:%S") << " ] ";
	oss << KHTTPRequest::getDirective(directive) << " " << (url);
	oss << " INTERNAL/0.0" << flush;
#endif
#endif

	string objType;
	string contentType;
	string* acceptType = (request->getString("format") != NULL ? request->getString("format") : request->getHeader("Accept"));
	if (acceptType == NULL) {
		objType.assign("json");
	}
	else {
		if (acceptType->find("json") != string::npos) {
			objType.assign("json");
			contentType.assign("application/json; charset=utf-8");
		}
		else if (acceptType->find("html") != string::npos) {
			objType.assign("html");
			contentType.assign("text/html; charset=utf-8");
		}
		else if (acceptType->find("xml") != string::npos) {
			objType.assign("xml");
			contentType.assign("application/xml; charset=utf-8");
		}
		else {
			objType.assign("json");
			contentType.assign("application/json; charset=utf-8");
		}
	}
	request->setHeader("X-Request-Source", new string("internal"));
	if (request->getHeader("Accept-Language") != NULL && request->getHeader("X-Accept-Locale") == NULL) {
		string locale;
		kinky::utils::extractLocaleFromAcceptLanguage(*request->getHeader("Accept-Language"), &locale);
		request->setHeader("X-Accept-Locale", new string(locale));
	}
	if (request->getHeader("Accept-Language") != NULL && request->getHeader("X-Accept-Region") == NULL) {
		string locale;
		kinky::utils::extractRegionFromAcceptLanguage(*request->getHeader("Accept-Language"), &locale);
		request->setHeader("X-Accept-Region", new string(locale));
	}

	managed_ptr<KObject>* reqPtr = new managed_ptr<KObject>(request);

	KObject* reply = new KObject();
	KObject* replies = new KObject(KObject::ARRAY);
	bool hasOperation = false;
	bool authenticate = false;
	bool docRoot = (url == "/");

	replies->base(objType);
	for (KRest::KRestLibTable::iterator i = this->libs->begin(); i != this->libs->end(); i++) {
		KRestOperation* op = i->second->operationFor(&url);
		if (op != NULL) {
			hasOperation = true;
			KToken* token = NULL;

			try {
				token = i->second->allowURL(&url, accessToken);
			}
			catch (KRestException* e) { // >>> NOT MINE EXPCETION: this URL does not belong to this API
				delete e;
				continue;
			}

			if (token != NULL) {
				authenticate = false;
				token->setIPAddress(new string(*request->getHeader("IP-Address")));

				KObject* replyObj = NULL;
				KHTTPResponse* httpRep = NULL;
				try {
					httpRep = new KHTTPResponse();
					httpRep->setHeader("Content-Type", contentType);

#ifndef MOD_DOCS
					op->preprocess(httpRep, request, directive, &url, token);
					if (httpRep->getBody() != NULL && httpRep->getBody()->length() != 0) {
						try {
							replyObj = KJSON::parse(httpRep->getBody());
						}
						catch (KParseException* e) {
							delete e;
						}
					}
					else {
						replyObj = op->process(request, directive, &url, token);
					}

					if (replyObj != NULL) {
						op->postprocess(replyObj, request, directive, &url, token);
#endif

						// >>>
						// Documentation mode for operations
#ifdef MOD_DOCS
						bool allowed = ((dynamic_cast<KDocRoot*>(op)) != NULL) || ((dynamic_cast<KWRMLRelationRoot*>(op)) != NULL) || ((dynamic_cast<KWRMLSchemaRoot*>(op)) != NULL);
						if (
							(url.find(kinky::rest::WRML_EDIT) != string::npos ||
								url.find(kinky::rest::WRML_LINKS) != string::npos ||
								url.find(kinky::rest::WRML_RELATIONS) != string::npos ||
								url.find(kinky::rest::WRML_SCHEMAS) != string::npos ||
								url.find(kinky::rest::WRML_MODULES) != string::npos ||
								url == "/documentation/get-token" ||
								url == kinky::rest::KINKY_LOG ||
								url == kinky::rest::DOC_ROOT) && (allowed)
						) {
							op->preprocess(httpRep, request, directive, &url, token);
							replyObj = op->process(request, directive, &url, token);
						}
						delete httpRep;
						httpRep = NULL;

						if (replyObj != NULL) {
#endif
						// <<<
						replyObj->base(objType);
						if (!docRoot) {
							KObject* links = op->getResourceLinks(replyObj, request, url, directive);
							if (links != NULL) {
								if ((*replyObj)["links"] != NULL_KOBJECT) {
									KObject* currentLinks = (*replyObj)["links"];
									for (KObject::KObjAttributes::iterator it = links->begin(); it != links->end(); it++) {
										if ((*currentLinks)[(*it)->key] != NULL_KOBJECT) {
											currentLinks->unset((*it)->key);
										}
										(*currentLinks) << (*it)->key << ((KObject*) (*links)[(*it)->key]);
									}
									delete links;
								}
								else {
									(*replyObj) << "links" << links;
								}
							}
						}

						if ((request->getHeader("X-Rest-Embed") != NULL || (*request)["embed"] != NULL_KOBJECT)) {
							string fieldStr;
							if (request->getHeader("X-Rest-Embed") != NULL) {
								fieldStr.assign(*request->getHeader("X-Rest-Embed"));
							}
							else {
								fieldStr.assign((string) (*request)["embed"]);
							}
							KRestArg* arg = KRestArg::parse(fieldStr);
							map<string, KObject*> cache;
							KRest::embed(replyObj, request, url, arg, token, cache);
							delete arg;
						}

						if (request->getHeader("X-Rest-Fields") != NULL || (*request)["fields"] != NULL_KOBJECT) {
							string fieldStr;
							if (request->getHeader("X-Rest-Fields") != NULL) {
								fieldStr.assign(*request->getHeader("X-Rest-Fields"));
							}
							else {
								fieldStr.assign((string) (*request)["fields"]);
							}
							if (fieldStr != "*") {
								KRestArg* arg = KRestArg::parse(fieldStr);
								KRest::fields(replyObj, arg);
								delete arg;
							}
						}

						(*replies) << replyObj;

						if (docRoot) {
							break;
						}
					}
					else {
						if (httpRep->getCode() != 0) {
							reply->setStatus(httpRep->getCode());
							reply->setBody(httpRep->getBody());

							KHTTPRequest::KHTTPHeaders* headers = httpRep->getHeaders();
							for (KHTTPRequest::KHTTPHeaders::iterator h = headers->begin(); h != headers->end(); h++) {
								reply->addHeader((*h)->first, (*h)->second->get()->data());
							}
						}
						op->postprocess(reply, request, directive, &url, token);
					}

					delete httpRep;
					httpRep = NULL;

				}
				catch (KRestException* e) {
					if (replyObj != NULL) {
						delete replyObj;
					}
					if (httpRep != NULL) {
						delete httpRep;
					}
					replyObj = new KObject(objType);
					replyObj->setStatus(e->getCode());
					if (e->getCode() == KHTTPResponse::TEMPORARY_REDIRECT || e->getCode() == KHTTPResponse::SEE_OTHER) {
						string redirerr(e->getMessage() != NULL ? *e->getMessage() : "/");
						ostringstream errss;
						if (redirerr.find("?") == string::npos) {
							errss << redirerr << "?status=error";
						}
						else {
							errss << redirerr << "&status=error";
						}
						errss << "&error=true&error_code=" << e->getErrorCode() << flush;
						replyObj->setHeader("Location", new string(errss.str()));
					}
					else {
						replyObj->setBoolean("error", true);
						replyObj->setInteger("code", e->getCode());
						replyObj->setInteger("error_code", e->getErrorCode());
						replyObj->setString("message", e->getMessage()->data());
						if (e->getOtherParams() != NULL) {
							replyObj->setObject("detail", e->getOtherParams());
						}
					}
					(*replies) << replyObj;
					delete e;
				}
				catch (exception& e) {
					if (replyObj != NULL) {
						delete replyObj;
					}
					if (httpRep != NULL) {
						delete httpRep;
					}
					replyObj = new KObject(objType);
					replyObj->setStatus(KHTTPResponse::INTERNAL_SERVER_ERROR);
					replyObj->setBoolean("error", true);
					replyObj->setInteger("code", KHTTPResponse::INTERNAL_SERVER_ERROR);
					replyObj->setInteger("error_code", -1);
					replyObj->setString("message", e.what());
					(*replies) << replyObj;
				}

				delete token;
			}
			else {
				authenticate = true;
			}
		}
	}

	KObject* ret = NULL;

	if (authenticate) {
		reply->setStatus(KHTTPResponse::UNAUTHORIZED);
		ret = reply;
		delete replies;
	}
	else if (!hasOperation) {
		reply->setStatus(KHTTPResponse::NOT_FOUND);
		(*reply) <<
		                    "error" << true <<
		                    "code" << KHTTPResponse::NOT_FOUND <<
		                    "error_code" << 900 <<
		                    "message" << "The requested resource was not found on this backend.";
		ret = reply;
		delete replies;
	}
	else if (replies->size() == 0 && reply->getBody() == NULL) {
		reply->setStatus(KHTTPResponse::NO_CONTENT);
		ret = reply;
		delete replies;
	}
	else if (replies->size() == 1) {
		KObject* oneReply = (KObject*) (*replies)[0];
		managed_ptr<KObject>* repPtr = new managed_ptr<KObject>(oneReply);
		delete replies;
		delete reply;
		ret = oneReply;
		repPtr->unlink();
	}
	else if (reply->getBody() != NULL) {
		ret = reply;
		delete replies;
	}
	else {
		delete reply;
		replies->setStatus(KHTTPResponse::OK);
		ret = replies;
	}

	if (reqPtr != NULL) {
		reqPtr->unlink();
	}

#ifdef LOG_INTERNAL
#ifdef LOG_COLORS
	oss << "\033[1;30m";
	oss << " -> ";
	oss << "\033[0m";
	if (ret->getStatus() < 300) {
		oss << "\033[1;32m";
	}
	else if (ret->getStatus() >= 300 && ret->getStatus() < 400) {
		oss << "\033[1;33m";
	}
	else {
		oss << "\033[1;31m";
	}
	oss << "INTERNAL/0.0" << " " << KHTTPResponse::getStatusLine(ret->getStatus());
	oss << "\033[0m";
	oss << "\033[0;37m";
	oss << " [object size: " << ret->size() << " attributes]";
	oss << "\033[0m";
	oss << endl << flush;
#else
	oss << " -> ";
	oss << "INTERNAL/0.0" << " " << KHTTPResponse::getStatusLine(ret->getStatus());
	oss << " [object size: " << ret->size() << " attributes]";
	oss << endl << flush;
#endif

#endif

#ifdef LOG_INTERNAL
	clock_t end = clock();
	float delta = (((float) end - start) / CLOCKS_PER_SEC) * 1000;
	(*kinky::io::out) << "[Ellapsed: " << setprecision(6) << (delta > 59000 ? delta / 60000 : (delta > 3559000 ? delta / 3600000 : (delta > 999 ? delta / 1000 : delta))) << " " << (delta > 59000 ? "min(s)" : (delta > 3559000 ? "hour(s)" : (delta > 999 ? "sec(s)" : "msec(s)"))) << "] " << oss.str() << flush;
#endif

	return ret;
}

kinky::rest::KRestOperation* kinky::rest::KRest::getOperation(string url) {
	KRest* rest = static_cast<KRest*>(k_rest);
	for (KRest::KRestLibTable::iterator i = rest->libs->begin(); i != rest->libs->end(); i++) {
		KRestOperation* op = i->second->operationFor(&url);
		if (op != NULL) {
			return op;
		}
	}
	return NULL;
}

kinky::memory::managed_ptr<kinky::rest::http::KHTTPResponse>* kinky::rest::KRest::process(KHTTPRequest* request) {
	if (request->getDirective() == KHTTPRequest::OPTIONS) {
		return KRest::processOPTIONS(request);
	}
	if (*request->getURL() == "/kinky/log") {
		string* auth = NULL;
		bool authenticate = true;

		if ((auth = request->getHeader("Authorization")) != NULL) {
			ostringstream oss;
			istringstream ss;
			ss.str(auth->data());
			kinky::utils::base64_decode(ss, oss);
			string credentials(oss.str());
			size_t i = string::npos;
			if ((i = credentials.find("|")) != string::npos) {
				string user(credentials.substr(0, i));
				string pass;
				if (i < credentials.length() - 1) {
					pass.assign(credentials.substr(i + 1));
				}
				if (user == *this->log_user && pass == *this->log_pass) {
					authenticate = false;
				}
			}
		}

		if (authenticate) {
			KHTTPResponse* reply = KRest::preprocess(NULL, request->getHeader("Origin"));
			time_t rawtime;
			struct tm * ptm;
			char buffer[80];
			time(&rawtime);
			ptm = gmtime(&rawtime);
			ptm->tm_min += 1;
			strftime(buffer, 80, "%a, %d %b %Y %X %Z", ptm);

			reply->setHeader("Expires", buffer);
			reply->setHeader("WWW-Authenticate", "OAuth2.0 realm=\"MyOAuth\"");
			reply->setCode(KHTTPResponse::UNAUTHORIZED);
			return new managed_ptr<KHTTPResponse>(reply);
		}
		throw new kinky::log::KLogRequestException();
	}
	else {
		KHTTPResponse* reply = KRest::preprocess(NULL, request->getHeader("Origin"));
		int directive = request->getDirective();

		string* atparam = NULL;
		if ((atparam = request->getParam("access_token")) != NULL) {
			string* auth = new string("OAuth2.0 ");
			auth->insert(auth->length(), *atparam);
			request->setHeader("Authorization", auth);
			request->removeParam("access_token");
		}
		string* fmtparam = NULL;
		if ((fmtparam = request->getParam("format")) != NULL) {
			request->setHeader("Accept", new string(fmtparam->data()));
			request->removeParam("format");
		}
		string* cbparam = NULL;
		if ((cbparam = request->getParam("callback")) != NULL) {
			request->setHeader("X-JSONP-Callback", new string(cbparam->data()));
			request->removeParam("callback");
		}
		string* cbmparam = NULL;
		if ((cbmparam = request->getParam("callback_method")) != NULL) {
			cbmparam = new string(cbmparam->data());

			std::transform(cbmparam->begin(), cbmparam->end(), cbmparam->begin(), ::toupper);
			request->setHeader("X-JSONP-Callback-Method", cbmparam);
			request->removeParam("callback_method");
			if (request->getParam("callback") != NULL) {
				directive = KHTTPRequest::getDirective(cbmparam);
			}
		}
		string* cbrparam = NULL;
		if ((cbrparam = request->getParam("callback_redirect")) != NULL) {
			request->setHeader("X-JSONP-Callback-Redirect", new string(cbrparam->data()));
			request->removeParam("callback_redirect");
		}
		string* ebparam = NULL;
		if ((ebparam = request->getParam("embed")) != NULL) {
			request->setHeader("X-Rest-Embed", new string(ebparam->data()));
			request->removeParam("embed");
		}
		string* fdparam = NULL;
		if ((fdparam = request->getParam("fields")) != NULL) {
			request->setHeader("X-Rest-Fields", new string(fdparam->data()));
			request->removeParam("fields");
		}
		request->setHeader("X-Request-Source", new string("external"));
		request->setHeader("X-Request-On-Behalf", (request->getHeader("X-Forwarded-For") != NULL ? new string(request->getHeader("X-Forwarded-For")->data()) : new string(kinky::memory::KMemoryManager::getMemory()->currentip->data())));
		if (request->getHeader("Accept-Language") != NULL && request->getHeader("X-Accept-Locale") == NULL) {
			string locale;
			kinky::utils::extractLocaleFromAcceptLanguage(*request->getHeader("Accept-Language"), &locale);
			request->setHeader("X-Accept-Locale", new string(locale));
		}
		if (request->getHeader("Accept-Language") != NULL && request->getHeader("X-Accept-Region") == NULL) {
			string locale;
			kinky::utils::extractRegionFromAcceptLanguage(*request->getHeader("Accept-Language"), &locale);
			request->setHeader("X-Accept-Region", new string(locale));
		}
		string* callback = request->getHeader("X-JSONP-Callback");

		string objType;
		string* acceptType = (request->getParam("format") != NULL ? request->getParam("format") : request->getHeader("Accept"));
		if (acceptType == NULL || callback != NULL) {
			acceptType = new string("application/json; charset=utf-8");
			objType.assign("json");
		}
		else {
			if (acceptType->find("json") != string::npos) {
				acceptType = new string("application/json; charset=utf-8");
				objType.assign("json");
			}
			else if (acceptType->find("html") != string::npos) {
				acceptType = new string("text/html; charset=utf-8");
				objType.assign("html");
			}
			else if (acceptType->find("xml") != string::npos) {
				acceptType = new string("application/xml; charset=utf-8");
				objType.assign("xml");
			}
			else {
				acceptType = new string("application/json; charset=utf-8");
				objType.assign("json");
			}
		}

		string* contentType = (request->getHeader("Content-Type") != NULL ? request->getHeader("Content-Type") : acceptType);
		if (contentType->find("json") != string::npos) {
			contentType = new string("application/json; charset=utf-8");
		}
		else if (contentType->find("html") != string::npos) {
			contentType = new string("text/html; charset=utf-8");
		}
		else if (contentType->find("xml") != string::npos) {
			contentType = new string("application/xml; charset=utf-8");
		}
		else if (contentType->find("x-www-form-urlencoded") != string::npos) {
			contentType = new string("text/html; charset=utf-8");
		}
		else if (contentType->find("form-data") != string::npos) {
			contentType = new string("text/html; charset=utf-8");
		}
		else {
			contentType = new string("application/json; charset=utf-8");
		}

		KObject* reqObj = NULL;
		managed_ptr<KObject>* reqPtr = NULL;

		KObject* replies = new KObject(KObject::ARRAY);
		string* url = request->getURL();
		bool hasOperation = false;
		bool authenticate = false;
		bool docRoot = (*url) == "/";

		/*{
		 string* accessToken = KHTTPRequest::getAuthentication(request);
		 if (accessToken != NULL) {
		 kout << endl << *(KToken(*accessToken).prettify()) << endl << endl << flush;
		 }
		 }*/

		replies->base(objType);
		for (KRest::KRestLibTable::iterator i = this->libs->begin(); i != this->libs->end(); i++) {
			KObject* replyObj = NULL;

			kinky::fileserve::KProxyPassController* pp = NULL;
			if ((pp = i->second->getProxyPassController()) != NULL && url->find("/proxy-pass") == 0 && i->second->getConfiguration() != NULL && i->second->getConfiguration()->get("krest_proxypass_mode") == "on") {
				string uri(url->substr(11));
				try {
					if (pp->pass(reply, &uri, directive, request)) {
						authenticate = false;
						hasOperation = true;
						break;
					}
				}
				catch (KRestException* e) {
					delete e;
				}
				catch (exception& e) {
				}
				continue;
			}

			KRestOperation* op = i->second->operationFor(url);
			if (op != NULL) {

				hasOperation = true;
				KToken* token = NULL;

				try {
					token = i->second->allowURL((reqObj != NULL ? (KHTTPRequest*) reqObj : request));
				}
				catch (KRestException* e) { // >>> NOT MINE EXPCETION: this URL does not belong to this API
					delete e;
					continue;
				}

				if (token != NULL) {
					authenticate = false;
					token->setIPAddress(new string((reqObj != NULL ? *reqObj->getHeader("IP-Address") : *request->getHeader("IP-Address"))));

					if (reqObj == NULL) {
						reply->setHeader("Content-Type", acceptType->data(), false);

						if (request->getBody() != NULL && request->getBody()->length() != 0 && (directive == KHTTPRequest::DELETE || directive == KHTTPRequest::PUT || directive == KHTTPRequest::POST || directive == KHTTPRequest::SYNC || directive == KHTTPRequest::APPLY)) {
							if (contentType->find("json") != string::npos) {
								string* body = request->getBody();
								try {
									reqObj = KJSON::parse(body);
								}
								catch (KParseException* e) {
									reqObj = new KObject("json");
									delete e;
								}
							}
							else if (contentType->find("html") != string::npos || contentType->find("*/*") != string::npos) {
								reqObj = new KObject("html");

								string* reqContentType = request->getHeader("Content-Type");
								if (reqContentType != NULL && reqContentType->find("x-www-form-urlencoded") != string::npos) {
									KHTTPRequest::stripParams(*request->getBody(), request->getParams());
								}
								else if (reqContentType != NULL && reqContentType->find("form-data") != string::npos && i->second->getConfiguration()->getString("krest_file_upload_path") != NULL) {
									reqObj = new KObject("json");

									string boundary = reqContentType->substr(reqContentType->find("boundary=") + 9);

									KHTTPRequest::stripFormData(boundary, *request->getBody(), *i->second->getConfiguration()->getString("krest_file_upload_path"), request->getParams());
								}
							}
							else if (contentType->find("xml") != string::npos) {
								string* body = request->getBody();
								try {
									reqObj = KXML::parse(body);
								}
								catch (KParseException* e) {
									reqObj = new KObject("xml");
									delete e;
								}
							}
							else {
								reqObj = new KObject("json");
							}
						}
						else {
							reqObj = new KObject("json");
						}
						reqPtr = new managed_ptr<KObject>(reqObj);

						KRest::initRequestParams(reqObj, request->getParams());
						KRest::initRequestHeaders(reqObj, request->getHeaders());
						request->setHeaders(NULL);
						request->setParams(NULL);
						reqObj->setURL(new string(request->getURL()->data()));
						reqObj->setDirective(directive);
					}

					try {
						// >>>
						// Normal operation mode

#ifndef MOD_DOCS
						op->preprocess(reply, (KHTTPRequest*) reqObj, directive, url, token);
						op->preprocess(reply, reqObj, directive, url, token);
						replyObj = op->process(reqObj, directive, url, token);
#endif
						// <<<

						// >>>
						// Documentation mode for operations
#ifdef MOD_DOCS
						replyObj = NULL;
						if (
							url->find(kinky::rest::WRML_EDIT) != string::npos ||
							url->find(kinky::rest::WRML_LINKS) != string::npos ||
							url->find(kinky::rest::WRML_RELATIONS) != string::npos ||
							url->find(kinky::rest::WRML_SCHEMAS) != string::npos ||
							url->find(kinky::rest::WRML_MODULES) != string::npos ||
							*url == "/documentation/get-token" ||
							*url == kinky::rest::KINKY_LOG ||
							*url == kinky::rest::DOC_ROOT
						) {
							op->preprocess(reply, (KHTTPRequest*) reqObj, directive, url, token);
							op->preprocess(reply, reqObj, directive, url, token);
							replyObj = op->process(reqObj, directive, url, token);
						}
						else {
							replyObj = op->processDocumentation(reqObj, directive, url, token);
						}
#endif
						// <<<
						if (replyObj != NULL) {
#ifndef MOD_DOCS
							op->postprocess(replyObj, reqObj, directive, url, token);
#endif

							replyObj->base(objType);
#ifndef MOD_DOCS
							if (!docRoot) {
								KObject* links = op->getResourceLinks(replyObj, reqObj, *url, directive);
								if (links != NULL) {
									if ((*replyObj)["links"] != NULL_KOBJECT) {
										KObject* currentLinks = (*replyObj)["links"];
										for (KObject::KObjAttributes::iterator it = links->begin(); it != links->end(); it++) {
											if ((*currentLinks)[(*it)->key] != NULL_KOBJECT) {
												currentLinks->unset((*it)->key);
											}
											(*currentLinks) << (*it)->key << ((KObject*) (*links)[(*it)->key]);
										}
										delete links;
									}
									else {
										(*replyObj) << "links" << links;
									}
								}
							}
#endif

							if (reqObj->getHeader("X-Rest-Embed") != NULL) {
								string fieldStr;
								fieldStr.assign(*reqObj->getHeader("X-Rest-Embed"));
								KRestArg* arg = KRestArg::parse(fieldStr);
								map<string, KObject*> cache;
								KRest::embed(replyObj, reqObj, *url, arg, token, cache);
								delete arg;
							}

							if (reqObj->getHeader("X-Rest-Fields") != NULL) {
								string fieldStr;
								fieldStr.assign(*reqObj->getHeader("X-Rest-Fields"));
								if (fieldStr != "*") {
									KRestArg* arg = KRestArg::parse(fieldStr);
									KRest::fields(replyObj, arg);
									delete arg;
								}
							}

							(*replies) << replyObj;
							KObject::KObjHeaders* headers = replyObj->getHeaders();
							if (headers != NULL && headers->size() != 0) {
								for (KObject::KObjHeaders::iterator i = headers->begin(); i != headers->end(); i++) {
									reply->setHeader(i->first, *i->second, true);
								}
							}

							if (docRoot) {
								break;
							}
						}
						else {
							op->postprocess(reply, (KHTTPRequest*) reqObj, directive, url, token);
						}

					}
					catch (KRestException* e) {
						if (replyObj != NULL) {
							delete replyObj;
						}
						replyObj = new KObject(objType);
						replyObj->setStatus(e->getCode());
						if (e->getCode() == KHTTPResponse::TEMPORARY_REDIRECT || e->getCode() == KHTTPResponse::SEE_OTHER) {
							string redirerr(e->getMessage() != NULL ? *e->getMessage() : "/");
							ostringstream errss;
							if (redirerr.find("?") == string::npos) {
								errss << redirerr << "?status=error";
							}
							else {
								errss << redirerr << "&status=error";
							}
							errss << "&error=true&error_code=" << e->getErrorCode() << flush;
							reply->setHeader("Location", new string(errss.str()));
						}
						else {
							replyObj->setBoolean("error", true);
							replyObj->setInteger("code", e->getCode());
							replyObj->setInteger("error_code", e->getErrorCode());
							replyObj->setString("message", e->getMessage()->data());
							if (e->getOtherParams() != NULL) {
								replyObj->setObject("detail", e->getOtherParams());
							}
						}
						(*replies) << replyObj;
						delete e;
					}
					catch (exception& e) {
						if (replyObj != NULL) {
							delete replyObj;
						}
						replyObj = new KObject(objType);
						replyObj->setStatus(KHTTPResponse::INTERNAL_SERVER_ERROR);
						replyObj->setBoolean("error", true);
						replyObj->setInteger("code", KHTTPResponse::INTERNAL_SERVER_ERROR);
						replyObj->setInteger("error_code", -1);
						replyObj->setString("message", e.what());
						(*replies) << replyObj;
					}
					delete token;
				}
				else {
					authenticate = true;
				}
			}
#ifdef DEPRECATED
			else if (i->second->getConfiguration() != NULL && i->second->getConfiguration()->get("krest_fileserve_mode") == "on" && i->second->getConfiguration()->getString("krest_fileserve_path") != NULL) {
				KToken* token = NULL;
				try {
					token = i->second->allowURL((reqObj != NULL ? (KHTTPRequest*) reqObj : request));
				}
				catch (KRestException* e) { // >>> NOT MINE EXPCETION: this URL does not belong to this API
					if (token != NULL) {
						delete token;
						token = NULL;
					}
					delete e;
					continue;
				}
				KFileServeController* fileServe = i->second->getFileServeController();
				if (fileServe != NULL && fileServe->searchFile(reply, url, directive, token)) {
					hasOperation = true;
					break;
				}

				if (token != NULL) {
					delete token;
					token = NULL;
				}
			}
#endif

		}

		string* replyStr = NULL;

		if (authenticate) {
			time_t rawtime;
			struct tm * ptm;
			char buffer[80];
			time(&rawtime);
			ptm = gmtime(&rawtime);
			ptm->tm_min += 1;
			strftime(buffer, 80, "%a, %d %b %Y %X %Z", ptm);

			reply->setHeader("Expires", buffer);
			reply->setHeader("WWW-Authenticate", "OAuth2.0 realm=\"MyOAuth\"");
			reply->setCode(KHTTPResponse::UNAUTHORIZED);
			replyStr = new string("");
		}
		else if (!hasOperation) {
			reply->setCode(KHTTPResponse::NOT_FOUND);
			reply->setHeader("Content-Type", "application/json; charset=utf8");
			KObject* oneReply = KOBJECT(
				"error" << true <<
				"code" << KHTTPResponse::NOT_FOUND <<
				"error_code" << 900 <<
				"message" << "The requested resource was not found on this backend."
			);
			ostringstream oss;
			oss << *oneReply << flush;
			replyStr = new string(oss.str());

			delete oneReply;
		}
		else if (replies->size() == 0 && reply->getBody() == NULL) {
			reply->setCode(KHTTPResponse::NO_CONTENT);
			replyStr = new string("");
		}
		else if (replies->size() == 0 && reply->getBody() != NULL) {
			replyStr = NULL;
		}
		else if (replies->size() == 1) {
			KObject* oneReply = (KObject*) (*replies)[0];
			reply->setCode(oneReply->getStatus());
			if (oneReply->getStatus() != 204) {
				ostringstream oss;
				oss << *oneReply << flush;
				replyStr = new string(oss.str());
			}
			else {
				replyStr = NULL;
			}
		}
		else {
			reply->setCode(KHTTPResponse::OK);
			ostringstream oss;
			oss << *replies << flush;
			replyStr = new string(oss.str());
		}

		if (reply->getCode() == KHTTPResponse::TEMPORARY_REDIRECT && (reqObj != NULL ? reqObj->getHeader("X-Requested-With") : request->getHeader("X-Requested-With")) != NULL && (reqObj != NULL ? *reqObj->getHeader("X-Requested-With") : *request->getHeader("X-Requested-With")) == "XHR") {
			reply->setCode(KHTTPResponse::OK);
			reply->setHeader("X-Replied-With", "Temporary Redirect");
			reply->setHeader("X-Replied-With-Status", "307");
		}

		delete replies;
		delete contentType;
		delete acceptType;

		if (callback != NULL) {
			bool wasnull = false;
			if (replyStr == NULL) {
				replyStr = reply->getBody();
				wasnull = true;
			}

			string* callbackredir = (reqObj != NULL ? reqObj->getHeader("X-JSONP-Callback-Redirect") : request->getHeader("X-JSONP-Callback-Redirect"));
			if (callbackredir != NULL) {
				reply->setCode(KHTTPResponse::SEE_OTHER);

				ostringstream oss;
				kinky::utils::url_encode(*replyStr, oss);
				replyStr->assign(oss.str());

				replyStr->insert(0, "&data=");
				replyStr->insert(0, *callback);
				replyStr->insert(0, "?callback=");
				replyStr->insert(0, *callbackredir);

				reply->setHeader("Location", replyStr);
				replyStr = NULL;
			}
			else {
				if (replyStr != NULL) {
					reply->setCode(KHTTPResponse::OK);
					replyStr->insert(0, "(");
					replyStr->insert(0, *callback);
					if (directive == KHTTPRequest::GET) {
						reply->setHeader("Content-Type", "text/javascript");
						replyStr->insert(replyStr->length(), ");");
					}
					else {
						reply->setHeader("Content-Type", "text/html");
						replyStr->insert(0, "<script>\n");
						replyStr->insert(replyStr->length(), ");\n</script>");
					}
				}
			}

			if (wasnull) {
				replyStr = NULL;
			}
		}

		if (reqPtr != NULL) {
			reqPtr->release();
		}

		if (request->getDirective() == KHTTPRequest::HEAD) {
			string size;
			kinky::tostr(size, replyStr != NULL ? replyStr->length() : 0);

			reply->setBody(NULL);
			if (replyStr != NULL) {
				delete replyStr;
				replyStr = NULL;
			}
			reply->setHeader("Content-Length", size);
		}
		else if (replyStr != NULL) {
			reply->setBody(replyStr);
		}

		if (reply->getCode() > 203) {
			reply->setHeader("Pragma", "no-cache");
			reply->setHeader("Cache-Control", "no-store");
		}

		return new managed_ptr<KHTTPResponse>(reply);
	}
}

void kinky::rest::KRest::fields(KObject* obj, KRestArg* fields) {
	map<string, bool> names;

	for (size_t i = 0; i != fields->size(); i++) {
		KRestArg* field = fields->at(i);
		string cur;
		if (field->get() != NULL) {
			cur.assign(field->get()->data());
		}
		names.insert(pair<string, bool>(cur, true));

		if (cur == "*") {
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				KRest::fields(((KObject*) (*obj)[(*it)->key]), field);
			}
		}
		else if (cur == "+") {
		}
		else if ((*obj)[cur] != NULL_KOBJECT && ((*obj)[cur].type == KObject::OBJECT || (*obj)[cur].type == KObject::ARRAY)) {
			KRest::fields(((KObject*) (*obj)[cur]), field);
		}
	}

	if (names.find("*") != names.end() || names.find("+") != names.end() || fields->size() == 0) {
		return;
	}

#ifdef USE_SPARSEHASH
	for (int i = 0; i != obj->size(); i++) {
#else
	for (size_t i = 0; i != obj->size(); i++) {
#endif
		if (names.find((*obj)[i].key) == names.end()) {
			obj->unset(i);
			i--;
		}
	}
}

#ifdef USE_SPARSEHASH
void kinky::rest::KRest::embed(KObject* obj, KObject* req, string referer, KRestArg* embeds, KToken* token) {
	for (size_t i = 0; i != embeds->size(); i++) {
		KRestArg* field = embeds->at(i);
		string cur;
		if (field->get() != NULL) {
			cur.assign(field->get()->data());
		}

		if (cur == "*") {
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				KRest::embed(((KObject*) (*obj)[(*it)->key]), req, referer, field, token);
			}
		}
		else {
			if ((*obj)[cur] != NULL_KOBJECT) {
				if ((*obj)[cur].type == KObject::OBJECT || (*obj)[cur].type == KObject::ARRAY) {
					KRest::embed(((KObject*) (*obj)[cur]), req, referer, field, token);
				}
			}
			else if ((*obj)["links"] != NULL_KOBJECT && (*obj)["links"][cur] != NULL_KOBJECT) {
				string host(req->getHeader("Host")->data());
				size_t end = host.find(":");
				if (end == string::npos) {
					host.assign("http://localhost");
				}
				else {
					host.assign(host.substr(end));
					host.insert(0, "http://localhost");
				}

				KObject* body = new KObject();
				KRest::initRequestHeaders(body, req->getHeaders());
				body->addHeader("Referer", referer);

				ostringstream uri;
				uri << (string) (*obj)["links"][cur]["href"] << flush;
				field->getRESTfulURI(uri);

				KObject* element = KRest::exec(KHTTPRequest::GET, host, uri.str(), *token, body);
				if (element->getStatus() == KHTTPResponse::OK) {
					(*obj) << cur << element;
				}
				((KObject*) (*obj)["links"])->unset(cur);

				if ((*obj)[cur] != NULL_KOBJECT) {
					KRest::embed(((KObject*) (*obj)[cur]), req, referer, field, token);
				}
			}
		}
	}
}
#endif

void kinky::rest::KRest::embed(KObject* obj, KHTTPRequest* req, string referer, KRestArg* embeds, KToken* token, map<string, KObject*>& cache) {
	for (size_t i = 0; i != embeds->size(); i++) {
		KRestArg* field = embeds->at(i);
		string cur;
		if (field->get() != NULL) {
			cur.assign(field->get()->data());
		}

		if (cur == "*") {
			for (KObject::KObjAttributes::iterator it = obj->begin(); it != obj->end(); it++) {
				KRest::embed(((KObject*) (*obj)[(*it)->key]), req, referer, field, token, cache);
			}
		}
		else {
			if ((*obj)[cur] != NULL_KOBJECT) {
				if ((*obj)[cur].type == KObject::OBJECT || (*obj)[cur].type == KObject::ARRAY) {
					KRest::embed(((KObject*) (*obj)[cur]), req, referer, field, token, cache);
				}
			}
			else if ((*obj)["links"] != NULL_KOBJECT && (*obj)["links"][cur] != NULL_KOBJECT) {
				KObject* body = new KObject();
				KHTTPRequest::KHTTPHeaders* headers = req->getHeaders();
				for (KHTTPRequest::KHTTPHeaders::iterator i = headers->begin(); i != headers->end(); i++) {
					if (i->first.find("x-") != string::npos) {
						continue;
					}
					body->addHeader(i->first, *i->second->get());
				}
				body->addHeader("Referer", referer);

				ostringstream uri;
				uri << (string) (*obj)["links"][cur]["href"] << flush;
				field->getRESTfulURI(uri);

				string uristr = uri.str();
				map<string, KObject*>::iterator found;
				if ((found = cache.find(uristr)) != cache.end()) {
					(*obj) << cur << found->second;
				}
				else {
					size_t question = string::npos;
					if ((question = uristr.find("?")) != string::npos) {
						string querystring = uristr.substr(question + 1);
						uristr.erase(question);
						KHTTPRequest::KHTTPParams* params = KHTTPRequest::stripParams(querystring);
						KRest::initRequestParams(body, params);
					}

					string tk;
					token->stringify(&tk);
					KObject* element = KRest::instance()->processLocal(KHTTPRequest::GET, uristr, &tk, body);
					delete body;
					if (element != NULL) {
						if (element->getStatus() == KHTTPResponse::OK) {
							(*obj) << cur << element;
							cache.insert(pair<string, KObject*>(uristr, element));
						}
						else {
							delete element;
						}
					}
				}
				((KObject*) (*obj)["links"])->unset(cur);

				if ((*obj)[cur] != NULL_KOBJECT) {
					KRest::embed(((KObject*) (*obj)[cur]), req, referer, field, token, cache);
				}
			}
		}
	}
}

kinky::rest::http::KHTTPResponse* kinky::rest::KRest::preprocess(KHTTPResponse* reply, string* allowOrigin) {
	if (reply == NULL) {
		reply = new KHTTPResponse();
	}

	reply->setHeader("Connection", "close", false);
	reply->setHeader("Server", static_cast<KRest*>(k_rest)->getVersion(), false);
	reply->setHeader("Cache-Control", "max-age=3600", false);
	reply->setHeader("Vary", "Accept-Language,Accept-Encoding,X-Access-Token,Authorization,E-Tag", false);

	if (allowOrigin != NULL) {
		reply->setHeader("Access-Control-Allow-Origin", allowOrigin->data(), false);
		reply->setHeader("Access-Control-Expose-Headers", KREST_ACCESS_CONTROL_HEADERS, false);
	}

	time_t rawtime = time(NULL);
	struct tm ptm;
	char buffer[80];
	localtime_r(&rawtime, &ptm);
	strftime(buffer, 80, "%a, %d %b %Y %X %Z", &ptm);
	reply->setHeader("Date", buffer, false);

	ptm.tm_hour += 1;
	strftime(buffer, 80, "%a, %d %b %Y %X %Z", &ptm);
	reply->setHeader("Expires", buffer, false);
	return reply;
}

kinky::memory::managed_ptr<kinky::rest::http::KHTTPResponse>* kinky::rest::KRest::processOPTIONS(KHTTPRequest* request) {
	KHTTPResponse* reply = new KHTTPResponse();
	reply->setCode(KHTTPResponse::OK);
	reply->setHeader("Connection", "close", false);
	reply->setHeader("Server", static_cast<KRest*>(k_rest)->getVersion(), false);
	reply->setHeader("Vary", "Accept-Language,Accept-Encoding,X-Access-Token,Authorization,E-Tag", false);

	string* allowOrigin = request->getHeader("Origin");
	if (allowOrigin != NULL) {
		reply->setHeader("Access-Control-Allow-Origin", allowOrigin->data(), false);
		reply->setHeader("Access-Control-Allow-Methods", "POST,GET,PUT,DELETE,OPTIONS,HEAD,SYNC,APPLY", false);
		reply->setHeader("Access-Control-Allow-Headers", KREST_ACCESS_CONTROL_HEADERS, false);
		reply->setHeader("Access-Control-Expose-Headers", KREST_ACCESS_CONTROL_HEADERS, false);
		reply->setHeader("Access-Control-Max-Age", "1728000", false);
	}
	reply->setBody(new string(""));
	return new managed_ptr<KHTTPResponse>(reply);
}

#ifdef USE_SPARSEHASH
void kinky::rest::KRest::initRequestParams(KObject* obj, KHTTPRequest::KHTTPParams* params) {
	for (KHTTPRequest::KHTTPParams::iterator i = params->begin(); i != params->end(); i++) {
		if ((**i->second).find("KARRAY:") != string::npos) {
			KObject* arr = NULL;
			try {
				arr = kinky::lang::KJSON::parse((**i->second).substr(7));
			}
			catch (KParseException* e) {
				delete e;
				arr = new KObject(KObject::ARRAY);
			}
			string name(i->first.data());
			if (name.find("[]") != string::npos) {
				name.erase(name.length() - 2);
			}
			obj->setArray(name, arr);
		}
		else {
			obj->setString(i->first, **i->second);
		}
	}
}

void kinky::rest::KRest::initRequestHeaders(KObject* obj, KHTTPRequest::KHTTPHeaders* params) {
	for (KHTTPRequest::KHTTPHeaders::iterator i = params->begin(); i != params->end(); i++) {
		obj->addHeader(i->first, **i->second);
	}
}

void kinky::rest::KRest::initRequestHeaders(KObject* obj, KObject::KObjHeaders* params) {
	for (KObject::KObjHeaders::iterator i = params->begin(); i != params->end(); i++) {
		obj->addHeader(i->first, *i->second);
	}
}
#else
void kinky::rest::KRest::initRequestParams(KObject* obj, KHTTPRequest::KHTTPParams* params) {
	for (KHTTPRequest::KHTTPParams::iterator i = params->begin(); i != params->end(); i++) {
		if ((**(*i)->second).find("KARRAY:") != string::npos) {
			KObject* arr = NULL;
			try {
				string sarr((**(*i)->second).substr(7));
				arr = kinky::lang::KJSON::parse(sarr);
			}
			catch (KParseException* e) {
				delete e;
				arr = new KObject(KObject::ARRAY);
			}
			string name(i->first.data());
			if (name.find("[]") != string::npos) {
				name.erase(name.length() - 2);
			}
			obj->setArray(name, arr);
		}
		else {
			obj->setString(i->first, *i->second->get());
		}
	}
	obj->setParams(params);
}

void kinky::rest::KRest::initRequestHeaders(KObject* obj, KHTTPRequest::KHTTPHeaders* params) {
	obj->setHeaders(params);
}
#endif

void kinky::rest::KRest::getCURLDirective(CURL* curl, int directive) {
	switch (directive) {
		case KHTTPRequest::PUT: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
			break;
		}
		case KHTTPRequest::SYNC: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "SYNC");
			break;
		}
		case KHTTPRequest::APPLY: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "APPLY");
			break;
		}
		case KHTTPRequest::DELETE: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
			break;
		}
		case KHTTPRequest::GET: {
			curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
			break;
		}
		case KHTTPRequest::POST: {
			curl_easy_setopt(curl, CURLOPT_POST, 1);
			break;
		}
		case KHTTPRequest::HEAD: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "HEAD");
			break;
		}
		case KHTTPRequest::OPTIONS: {
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "OPTIONS");
			break;
		}
		default: {
			curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
			break;
		}
	}
}

size_t kinky::rest::KRest::curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer) {
	size_t result = 0;

	if (buffer != NULL) {
		buffer->append(data, size * nmemb);
		result = size * nmemb;
	}

	return result;
}

size_t kinky::rest::KRest::curlReader(void *ptr, size_t size, size_t nmemb, void *userdata) {
	if (userdata != NULL) {
		string* buffer = static_cast<string*>(userdata);

		if (buffer != NULL && buffer->length() != 0) {
			size_t result = buffer->length() > size * nmemb ? size * nmemb : buffer->length();
			memcpy(ptr, buffer->c_str(), result);

			buffer->erase(0, result);

			return result;
		}
	}
	return 0;
}
