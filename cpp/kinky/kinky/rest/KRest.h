
#pragma once

#include <map>
#include <iostream>
#include <fstream>
#include <vector>
#include <memory.h>
#include <string>
#include <dlfcn.h>
#include <time.h>
#include <curl/curl.h>
#include <netinet/in.h>
#include <kinky/lang/KJSON.h>
#include <kinky/lang/KXML.h>
#include <kinky/lang/KObject.h>
#include <kinky/memory/KMemoryManager.h>
#include <kinky/oauth/KToken.h>
#include <kinky/rest/KRestAPI.h>
#include <kinky/rest/KRestException.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/lang/KRestArg.h>

#define KHTTP_OK() (new KObject(KObject::OBJECT, KHTTPResponse::OK))
#define KHTTP_CREATED() (new KObject(KObject::OBJECT, KHTTPResponse::CREATED))
#define KHTTP_ACCEPTED() (new KObject(KObject::OBJECT, KHTTPResponse::ACCEPTED))
#define KHTTP_NO_CONTENT() (new KObject(KObject::OBJECT, KHTTPResponse::NO_CONTENT))
#define KHTTP_NOT_FOUND() (new KObject(KObject::OBJECT, KHTTPResponse::NOT_FOUND))
#define KHTTP_PRECONDITION_FAILED() (new KObject(KObject::OBJECT, KHTTPResponse::PRECONDITION_FAILED))
#define KHTTP_INTERNAL_SERVER_ERROR() (new KObject(KObject::OBJECT, KHTTPResponse::INTERNAL_SERVER_ERROR))
#define KHTTP_METHOD_NOT_ALLOWED() (new KObject(KObject::OBJECT, KHTTPResponse::METHOD_NOT_ALLOWED))
#define KHTTP_UNAUTHORIZED() (new KObject(KObject::OBJECT, KHTTPResponse::UNAUTHORIZED))
#define KHTTP_FORBIDDEN() (new KObject(KObject::OBJECT, KHTTPResponse::FORBIDDEN))
#define KHTTP_TEMPORARY_REDIRECT() (new KObject(KObject::OBJECT, KHTTPResponse::TEMPORARY_REDIRECT))

#define KREST_ACCESS_CONTROL_HEADERS "X-Access-Token,X-Access-Token-Expires,X-Error-Reason,X-Error,X-Rest-Embed,X-Rest-Fields,X-Fields-Nillable,X-Client-ID,X-Use-Work-Version,Authorization,Accept,Accept-Language,Cache-Control,Connection,Content-Length,Content-Type,Cookie,Date,Expires,Location,Origin,Server,X-Requested-With,X-Replied-With,X-Replied-With-Status,Pragma,Cache-Control,E-Tag"

#define KVERSION "0.9.1"

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace oauth;
	using namespace memory;

	namespace rest {

		extern const char* WRML_SCOPES;
		extern const char* WRML_LINKS;
		extern const char* WRML_RELATIONS;
		extern const char* WRML_SCHEMAS;
		extern const char* WRML_EDIT;
		extern const char* WRML_MODULES;
		extern const char* DOC_ROOT;
		extern const char* KINKY_LOG;
		extern void* k_rest;
		extern struct in_addr *in_addr;

		using namespace http;

		class KRest {

			public:
				typedef map<string, KRestAPI*> KRestLibTable;

				KRest();

				virtual ~KRest();

				void registerApplication(string config);

				void unregisterApplication(string config);

				KRestAPI* getApplication(string api);

				managed_ptr<KHTTPResponse>* process(KHTTPRequest* request);

				KObject* processLocal(int directive, string url, string* accessToken, KObject* request);

				const char* getVersion();

				static KRest* instance();

				static KObject* exec(int directive, string* server, string *url, KToken& token, KObject* body, bool assync = false);

				static KObject* exec(int directive, string server, string url, KToken& token, KObject* body, bool assync = false);

				static KObject *exec(int directive, string* server, string *url, string *token, KObject* body, bool assync = false);

				static KObject *exec(int directive, string server, string url, string* token, KObject* body, bool assync = false);

				static KRestOperation* getOperation(string url);

				static KHTTPResponse* preprocess(KHTTPResponse* reply, string* allowOrigi1n = NULL);

				static managed_ptr<KHTTPResponse>* processOPTIONS(KHTTPRequest* request);

				static void initRequestParams(KObject* obj, KHTTPRequest::KHTTPParams* params);

				static void initRequestHeaders(KObject* obj, KHTTPRequest::KHTTPHeaders* params);

#ifdef USE_SPARSEHASH
				static void initRequestHeaders(KObject* obj, KObject::KObjHeaders* params);

				static void embed(KObject* replyObj, KObject* req, string referer, KRestArg* enbed, KToken* token);
#endif

				static void embed(KObject* replyObj, KHTTPRequest* req, string referer, KRestArg* enbed, KToken* token, map<string, KObject*>& cache);

				static void fields(KObject* obj, KRestArg* field);

				static void getEmbedTarget(string field, KObject* replyObj, map<string, pair<string, KObject*> >& process);

				static void getCURLDirective(CURL* curl, int directive);

				static size_t curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer);

				static size_t curlReader(void *ptr, size_t size, size_t nmemb, void *userdata);

			private:
				KRest::KRestLibTable* libs;
				string* version;

			public:
				string* hostname;
				string* port;

			private:
				string* log_user;
				string* log_pass;

		};
	}
}
