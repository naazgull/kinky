#pragma once

#include <exception>
#include <regex.h>
#include <kinky/oauth/KToken.h>
#include <kinky/lang/KObject.h>
#include <kinky/memory/KMemoryManager.h>
#include <kinky/lang/KProperties.h>
#include <kinky/rest/KRestException.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/error_codes.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace oauth;
	using namespace memory;

	namespace rest {

		class KRestAPI;

		using namespace http;

		class KRestOperation {
			public:
				const static int DOC_ROOT = 1;
				const static int DOCUMENT = 2;
				const static int COLLECTION = 4;
				const static int STORE = 8;
				const static int CONTROLLER = 16;

				KRestOperation(string urlParam, int type = DOCUMENT);

				KRestOperation(string urlParam, string linkDescriptionParam, int type = DOCUMENT);

				virtual ~KRestOperation();

				virtual regex_t* getURL();

				virtual bool isDocRoot();

				virtual bool isDocument();

				virtual bool isCollection();

				virtual bool isStore();

				virtual bool isController();

				virtual KObject* getResourceLinks(KObject* response, KObject* request, string url, int directive = 2);

				virtual void init();

				virtual void preprocess(KHTTPResponse* response, KHTTPRequest* request, int directive, string* url, KToken* accessToken);

				virtual void preprocess(KHTTPResponse* response, KObject* request, int directive, string* url, KToken* accessToken);

				virtual KObject* process(KObject* request, int directive, string* url, KToken* accessToken);

				virtual void postprocess(KObject* response, KObject* request, int directive, string* url, KToken* accessToken);

				virtual void postprocess(KHTTPResponse* response, KHTTPRequest* request, int directive, string* url, KToken* accessToken);

				// >>>
				// Documentation mode for operations
#ifdef MOD_DOCS
				kinky::lang::KObject* processDocumentation(KObject* request, int directive, string* url, KToken* accessToken);
				void getWRMLClass(KObject* widget, string* outclass);
				KObject* getSchemaInstance(KRestOperation* rootop, string schema);
#endif
				// <<<

				virtual void setPermissionByte(int permission);

				virtual int getPermissionByte();

				virtual void setPermissions(KObject* permissions);

				virtual KObject* getPermissions();

				virtual bool hasPermission(string url, int directive, KToken* token, string resource_owner = "", string not_owner_scope = "_", string perm_prefix = "default");

				virtual void setAPI(KRestAPI* api);

				virtual KRestAPI* getAPI();

				virtual KProperties* getConfiguration();

			private:
				int permissionByte;
				string* linkDescription;
				int type;

			protected:
				KObject* permissions;
				regex_t* url;
				KRestAPI* api;
		};
	}
}
