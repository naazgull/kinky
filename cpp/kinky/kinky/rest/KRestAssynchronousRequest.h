#pragma once

#include <kinky/memory/KMemoryManager.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace memory;

	namespace rest {

		template<class T>
		class KRestAssynchronousRequest {
			public:
				KRestAssynchronousRequest(T* targetParam);
				virtual ~KRestAssynchronousRequest();

				managed_ptr<T>* target;
		};

	}

}

template<class T>
kinky::rest::KRestAssynchronousRequest<T>::KRestAssynchronousRequest(T* targetParam) :
		target(new managed_ptr<T>(targetParam)) {
}

template<class T>
kinky::rest::KRestAssynchronousRequest<T>::~KRestAssynchronousRequest() {
}
