#include <kinky/rest/KRestOperation.h>
#include <kinky/rest/KRestAPI.h>

kinky::rest::KRestOperation::KRestOperation(string urlParam, int typeParam) :
		permissionByte(-1), linkDescription(NULL), type(typeParam), permissions(NULL) {

	this->url = new regex_t();
	if (regcomp(this->url, urlParam.c_str(), REG_EXTENDED | REG_NOSUB) != 0) {
		throw new KRestException(500, "the url regular expression is not well defined.");
	}
}

kinky::rest::KRestOperation::KRestOperation(string urlParam, string linkDescriptionParam, int typeParam) :
		permissionByte(-1), linkDescription(new string(linkDescriptionParam.data())), type(typeParam), permissions(NULL) {

	this->url = new regex_t();
	if (regcomp(this->url, urlParam.c_str(), REG_EXTENDED | REG_NOSUB) != 0) {
		throw new KRestException(500, "the url regular expression is not well defined.");
	}
}

kinky::rest::KRestOperation::~KRestOperation() {
	regfree(this->url);
	delete this->url;
	if (this->linkDescription != NULL) {
		delete this->linkDescription;
	}
}

bool kinky::rest::KRestOperation::isDocRoot() {
	return (this->type & KRestOperation::DOC_ROOT) == KRestOperation::DOC_ROOT;
}

bool kinky::rest::KRestOperation::isDocument() {
	return (this->type & KRestOperation::DOCUMENT) == KRestOperation::DOCUMENT;
}

bool kinky::rest::KRestOperation::isCollection() {
	return (this->type & KRestOperation::COLLECTION) == KRestOperation::COLLECTION;
}

bool kinky::rest::KRestOperation::isStore() {
	return (this->type & KRestOperation::STORE) == KRestOperation::STORE;
}

bool kinky::rest::KRestOperation::isController() {
	return (this->type & KRestOperation::CONTROLLER) == KRestOperation::CONTROLLER;
}

regex_t* kinky::rest::KRestOperation::getURL() {
	return this->url;
}

void kinky::rest::KRestOperation::init() {
}

kinky::lang::KObject* kinky::rest::KRestOperation::getResourceLinks(KObject* response, KObject* request, string url, int directive) {
	return NULL;
}

void kinky::rest::KRestOperation::preprocess(KHTTPResponse* response, KHTTPRequest* request, int directive, string* url, KToken* accessToken) {
}

void kinky::rest::KRestOperation::preprocess(KHTTPResponse* response, KObject* request, int directive, string* url, KToken* accessToken) {
}

kinky::lang::KObject* kinky::rest::KRestOperation::process(KObject* request, int directive, string* url, KToken* accessToken) {
	return NULL;
}

void kinky::rest::KRestOperation::postprocess(KObject* response, KObject* request, int directive, string* url, KToken* accessToken) {
}

void kinky::rest::KRestOperation::postprocess(KHTTPResponse* response, KHTTPRequest* request, int directive, string* url, KToken* accessToken) {
}

// >>>
// Documentation mode for operations
#ifdef MOD_DOCS
#include <kinky/rest/KDocRoot.h>
#include <kinky/utils/KDateTime.h>

kinky::lang::KObject* kinky::rest::KRestOperation::processDocumentation(KObject* request, int directive, string* url, KToken* accessToken) {
	KDocRoot* rootop = (KDocRoot*) this->api->getDocRoot();
	if (rootop == NULL) {
		return NULL;
	}

	KObject* links = rootop->getResourceLinks();
	KObject* link = NULL;
	string href;

	int urlc = kinky::utils::count(*url, '/');
	for (KObject::KObjAttributes::iterator i = links->begin(); i != links->end(); i++) {
		href.assign((*(KObject*) (**i))["href"]);
		regex_t* re = this->getURL();
		if (regexec(re, href.c_str(), (size_t) (0), NULL, 0) == 0 && kinky::utils::count(href, '/') == urlc) {
			link = (KObject*) (*links)[(*i)->key];
			(new managed_ptr<KObject>(link));
			break;
		}
	}

	delete links;
	if (link != NULL) {
		if ((*request)["instance"] != NULL_KOBJECT && (bool) (*request)["instance"]) {
			KObject* instance = NULL;
			string schema;
			KRestOperation::getWRMLClass(link, &schema);

			if (schema.length() != 0) {
				instance = this->getSchemaInstance(rootop, schema);
			}

			instance->addHeader("Pragma", "no-cache");
			instance->addHeader("Cache-Control", "no-store");
			return instance;
		}

		link->base(string(request->base()->data()));
		string module = (string) (*link)["module_href"];
		module.insert(0, "/wrml-modules/");

		string schema;
		KRestOperation::getWRMLClass(link, &schema);
		string relation((string) (*link)["href"]);
		relation.insert(0, "/wrml-relations");

		KObject* types = new KObject(KObject::ARRAY);
		(*link) << "types" << types;
		if (this->isDocument()) {
			(*types) << "document";
		}
		if (this->isCollection()) {
			(*types) << "collection";
		}
		if (this->isStore()) {
			(*types) << "store";
		}
		if (this->isController()) {
			(*types) << "controller";
		}
		if (this->isDocRoot()) {
			(*types) << "docroot";
		}

		(*link) << "allowed_scopes" << this->api->getScopeController()->getScopesFor(this);

		if (href.find('}') != href.length() - 1) {
			href.insert(href.length(), "/{id}");
		}
		KObject* instance = this->getSchemaInstance(rootop, schema);
		if (instance == NULL) {
			instance = new KObject();
		}
		KObject* embed = this->getResourceLinks(instance, request, href, directive);
		delete instance;

		if (embed != NULL) {
			for (KObject::KObjAttributes::iterator f = embed->begin(); f != embed->end(); f++) {
				KObject* field = (KObject*) (**f);
				(*field) << "name" << (**f).key;
			}
			(*link) << "embeddable" << embed;
		}

		(*link) << "links" << KOBJECT(
			"relations" << KOBJECT(
				"href" << relation
			) <<
			"schema" << KOBJECT(
				"href" << schema
			) <<
			"module" << KOBJECT(
				"href" << module
			)
		);

		link->addHeader("Pragma", "no-cache");
		link->addHeader("Cache-Control", "no-store");
		return link;
	}

	return NULL;
}

void kinky::rest::KRestOperation::getWRMLClass(KObject* widget, string* outclass) {
	KObject* wtypes = (KObject*) (*widget)["requestTypes"];
	if (wtypes == NULL || wtypes == NULL_KOBJECT) {
		wtypes = (KObject*) (*widget)["data"]["requestTypes"];
		if (wtypes == NULL || wtypes == NULL_KOBJECT) {
			wtypes = (KObject*) (*widget)["responseTypes"];
			if (wtypes == NULL || wtypes == NULL_KOBJECT) {
				wtypes = (KObject*) (*widget)["data"]["responseTypes"];
				if (wtypes == NULL || wtypes == NULL_KOBJECT) {
					return;
				}
			}
		}
	}

	for (KObject::KObjAttributes::iterator i = wtypes->begin(); i != wtypes->end(); i++) {
		string type = (string) (*wtypes)[(*i)->key];
		if (type.find("application/wrml;schema") != string::npos) {
			outclass->assign(type.substr(25, type.length() - 26));
			return;
		}
	}
}

KObject* kinky::rest::KRestOperation::getSchemaInstance(KRestOperation* op, string url) {
	KDocRoot* rootop = (KDocRoot*) op;
	kinky::utils::replaceAll(url, "/wrml-schemas", "");
	KObject* schema = rootop->getResourceSchema(url);
	KObject* instance = NULL;
	if (schema != NULL) {
		instance = new KObject();
		KObject* fields = (KObject*) (*schema)["fields"];

		if (fields != NULL && fields != NULL_KOBJECT) {
			for (KObject::KObjAttributes::iterator f = fields->begin(); f != fields->end(); f++) {
				KObject* field = (KObject*) (**f);
				string name = (string) (*field)["name"];

				if ((bool) (*field)["required"]) {
					name.insert(name.length(), "*");
				}

				string type = (string) (*field)["type"];
				if (type == "Schema") {
					string fschema;
					KRestOperation::getWRMLClass(field, &fschema);
					KObject* finstance = KRestOperation::getSchemaInstance(op, fschema);
					if (finstance != NULL) {
						(*instance) << name << finstance;
					}
				}
				else {
					if (type == "Boolean") {
						(*instance) << name << false;
					}
					if (type == "Choice") {
						if ((*field)["options"] != NULL_KOBJECT) {
							KObject* options = (KObject*) (*field)["options"];
							string val("one of: ");
							bool first = true;
							for (KObject::KObjAttributes::iterator o = options->begin(); o != options->end(); o++) {
								if (!first) {
									val.insert(val.length(), ", ");
									first = false;
								}
								val.insert(val.length(), (string) (**o));
							}
							(*instance) << name << val;
						}
						else {
							(*instance) << name << "some option (none defined in the schema)";
						}
					}
					if (type == "DateTime") {
						(*instance) << name << kinky::utils::date::format("%Y-%m-%d %H:%M:%S");
					}
					if (type == "Double") {
						(*instance) << name << 0.0;
					}
					if (type == "Integer") {
						(*instance) << name << 0;
					}
					if (type == "List") {
						(*instance) << name << (new KObject(KObject::ARRAY));
					}
					if (type == "Text") {
						(*instance) << name << "text";
					}
				}
			}
		}
		delete schema;
	}

	return instance;
}
#endif
// <<<

void kinky::rest::KRestOperation::setPermissionByte(int permission) {
	this->permissionByte = permission;
}

int kinky::rest::KRestOperation::getPermissionByte() {
	return this->permissionByte;
}

void kinky::rest::KRestOperation::setPermissions(KObject* permissions) {
	this->permissions = *(new managed_ptr<KObject>(permissions));
}

kinky::lang::KObject* kinky::rest::KRestOperation::getPermissions() {
	if (this->permissions == NULL) {
		return NULL;
	}
	return this->permissions;
}

bool kinky::rest::KRestOperation::hasPermission(string url, int directive, KToken* token, string resource_owner, string not_owner_scope, string perm_prefix) {
	if (this->permissions == NULL) {
		return true;
	}
	return this->api->getScopeController()->hasPermission(this, url, directive, token, resource_owner, not_owner_scope, perm_prefix);
}

void kinky::rest::KRestOperation::setAPI(KRestAPI* api) {
	this->api = api;
}

KRestAPI* kinky::rest::KRestOperation::getAPI() {
	return this->api;
}

KProperties* kinky::rest::KRestOperation::getConfiguration(){
	return this->api->getConfiguration();
}
