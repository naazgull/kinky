
# pragma once

#include <iostream>
#include <string>
#include <exception>
#include <dlfcn.h>
#include <vector>
#include <regex.h>
#include <kinky/rest/KRestOperation.h>
#include <kinky/oauth/KToken.h>
#include <kinky/oauth/KOAuthController.h>
#include <kinky/scopes/KScopeController.h>
#ifdef DEPRECATED
#include <kinky/fileserve/KFileServeController.h>
#endif
#include <kinky/fileserve/KProxyPassController.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/lang/KProperties.h>
#include <kinky/utils/KXORCipher.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace oauth;
	using namespace scopes;
	using namespace fileserve;

	namespace rest {

		using namespace http;

		class KRestAPI {
			public:
				typedef vector<KRestOperation*> KRestOpTable;

				KRestAPI();

				virtual ~KRestAPI();

				virtual KRestOperation* getDocRoot(bool initialization = false);

				virtual KRestOperation* getRelationRoot(bool initialization = false);

				virtual KRestOperation* getSchemaRoot(bool initialization = false);

				void setOAuthController(kinky::oauth::KOAuthController* oauthController);

				virtual KOAuthController* getOAuthController();

				void setScopeController(kinky::scopes::KScopeController* tokenController);

				virtual KScopeController* getScopeController();

#ifdef DEPRECATED
				void setFileServeController(kinky::fileserve::KFileServeController* fileServeController);

				virtual KFileServeController* getFileServeController();
#endif

				void setProxyPassController(kinky::fileserve::KProxyPassController* fileServeController);

				virtual KProxyPassController* getProxyPassController();

				void* getLibHandle();

				void setLibHandle(void *handle);

				string* getLibName();

				void setLibName(string name);

				void setConfFile(string file);

				 int registerOperation(KRestOperation *operation);

				KProperties* getConfiguration();

				KRestOperation *operationFor(string *url);

				void getRegisteredOperations(vector<KRestOperation*>* list);

				virtual void registerOAuth();

				virtual void registerSocial();

				virtual void registerMedia();

				virtual void registerCloudStorage();

				virtual void registerFileServe();

				virtual KToken* allowURL(KHTTPRequest *request) = 0;

				virtual KToken* allowURL(string* url, string* token) = 0;

				virtual void init() = 0;

				/* DEPRECATED: user "new KToken(string token, string privKey)" instead*/
				static KToken* unserializeToken(string* token, string privKey);

				/* DEPRECATED: user "ostringstream oss; oss << *token << flush;" instead*/
				static string* serializeToken(KToken* params, string privKey);

			private:
				KRestAPI::KRestOpTable* operations;
				string* libName;
				void* libHandle;
				int nOperations;
				KProperties* configuration;
				KOAuthController* oauthController;
				KScopeController* tokenController;
#ifdef DEPRECATED
				KFileServeController* fileServeController;
#endif
				KProxyPassController* proxyPassController;
		};

	}
}
