#pragma once

#include <sys/stat.h>
#include <sys/types.h>
#include <kinky/lang/KObject.h>
#include <kinky/rest/KRest.h>
#include <kinky/rest/KRestOperation.h>
#include <kinky/utils/KFileSystem.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;

	namespace rest {

		class KDocRoot: public KRestOperation {
			public:

				KDocRoot(string wrmlURIParam);
				virtual ~KDocRoot();

				virtual KObject* getResourceLinks(string url = "", bool notflat = false);

				virtual KObject* getResourceSchema(string url = "", bool notflat = false);

				virtual KObject* getResourceRelations(string url = "", bool notflat = false);

				virtual KObject* addResource(KObject* resource);

				virtual KObject* updateResource(KObject* resource);

				virtual KObject* deleteResource(KObject* resource);

				KObject* process(KObject* request, int directive, string* url, KToken* token);

			protected:
				string* wrmlURI;

				void resolveDependencies(KObject* schema);
		};
	}
}
