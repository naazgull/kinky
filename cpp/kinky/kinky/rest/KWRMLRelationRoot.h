#pragma once

#include <kinky/lang/KObject.h>
#include <kinky/oauth/KToken.h>
#include <kinky/rest/KRest.h>
#include <kinky/rest/KRestOperation.h>
#include <kinky/utils/KFileSystem.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace oauth;

	namespace rest {

		class KWRMLRelationRoot: public KRestOperation {
			public:

				KWRMLRelationRoot(string wrmlURIParam);
				virtual ~KWRMLRelationRoot();

				virtual KObject* getRelation(string url = "");

				virtual KObject* addRelation(KObject* relation, string url);

				virtual KObject* updateRelation(KObject* relation, string url);

				virtual KObject* deleteRelation(KObject* relation, string url);

				virtual KObject* process(KObject* request, int directive, string* url, KToken* token);

			protected:
				string* wrmlURI;
		};

		namespace wrml {
			void getElementLinksURI(KRestOperation* op, KObject* response, KObject* request, int directive, string field_uri);

			void getElementLinksData(KRestOperation* op, KObject* response, KObject* request, int directive, string field_data, string field_data_name, string rel = "");

		}
	}
}
