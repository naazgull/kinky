#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <kinky/rest/net/KSocketTimeoutException.h>
#include <kinky/memory/KMemoryManager.h>
#include <kinky/rest/KRestException.h>

using namespace std;
using namespace __gnu_cxx;

#ifndef HEADER_SEPARATOR
#define HEADER_SEPARATOR "\r\n\r\n"
#endif

#define KSOCKET_TIMEOUT 60

namespace kinky {

	using namespace memory;

	namespace rest {

		namespace net {

			template<class T, class R>
			class KSocket {
				public:
					KSocket(int sockfdParam, struct sockaddr_in* addressParam);
					virtual ~KSocket();

					virtual void reply(managed_ptr<string>* rep);

					virtual managed_ptr<string>* readRequest(size_t contentLength = -1);

					string* getClientAddress();

					virtual managed_ptr<T>* consume() = 0;

					virtual managed_ptr<R>* process(T*) = 0;

					virtual managed_ptr<R>* process(KRestException* e) = 0;

					virtual managed_ptr<R>* process(exception e) = 0;

					virtual managed_ptr<string>* stringify(managed_ptr<R>*) = 0;

				public:
					int sockfd;

				protected:
					struct sockaddr_in* address;
			};
		}
	}
}

template<class T, class R>
kinky::rest::net::KSocket<T, R>::KSocket(int sockfdParam, struct sockaddr_in* addressParam) :
		sockfd(sockfdParam), address(addressParam) {
}
template<class T, class R>
kinky::rest::net::KSocket<T, R>::~KSocket() {
	delete this->address;
	if (this->sockfd != -1) {
		shutdown(this->sockfd, SHUT_WR);
		close(this->sockfd);
	}
}

template<class T, class R>
void kinky::rest::net::KSocket<T, R>::reply(managed_ptr<string>* rep) {
	try {
		int opts = fcntl(this->sockfd, F_GETFL);
		opts ^= O_NONBLOCK;
		fcntl(this->sockfd, F_SETFL, opts);

		while ((*rep)->length() != 0) {
			size_t size = write(this->sockfd, (*rep)->data(), (*rep)->length());
			if (size == (size_t) -1) {
				return;
			}
			(*rep)->erase(0, size);
		}
	}
	catch (exception& e) {
	}
}

template<class T, class R>
kinky::memory::managed_ptr<string>* kinky::rest::net::KSocket<T, R>::readRequest(size_t contentLength) {
	char* buffer = new char[256];
	bzero(buffer, 256);

	int nRead = -1;
	string* toReturn = new string();

	int opts = fcntl(this->sockfd, F_GETFL);
	opts = opts & ~O_NONBLOCK;
	if (fcntl(this->sockfd, F_SETFL, opts) < 0) {
	}

	fd_set fds;
	struct timeval timeout;

	timeout.tv_sec = KSOCKET_TIMEOUT;
	timeout.tv_usec = 0;
	FD_ZERO(&fds);
	FD_SET(this->sockfd, &fds);

	if (select(this->sockfd + 1, &fds, NULL, NULL, &timeout) <= 0) {
		throw new KSocketTimeoutException();
	}

	do {
		bzero(buffer, 256);
		nRead = read(this->sockfd, buffer, 255);
		if (nRead <= 0) {
			break;
		}
		toReturn->append(buffer, nRead);

		bool headerDone = toReturn->find(HEADER_SEPARATOR) != string::npos;
		if (headerDone || contentLength != (size_t) -1) {
			opts = (opts | O_NONBLOCK);
			fcntl(this->sockfd, F_SETFL, opts);
		}
		else if (!headerDone) {
			nRead = 255;
		}
	}
	while ((nRead == 255 && contentLength == (size_t) -1) || (contentLength != (size_t) -1 && contentLength != toReturn->length()));

	delete[] buffer;
	if (toReturn->length() == 0) {
		delete toReturn;
		return NULL;
	}
	return new managed_ptr<string>(toReturn);
}

template<class T, class R>
string* kinky::rest::net::KSocket<T, R>::getClientAddress() {
	ostringstream oss;
	oss << int(this->address->sin_addr.s_addr & 0xFF) << "." << int((this->address->sin_addr.s_addr & 0xFF00) >> 8) << "." << int((this->address->sin_addr.s_addr & 0xFF0000) >> 16) << "." << int((this->address->sin_addr.s_addr & 0xFF000000) >> 24);
	return new string(oss.str());
}
