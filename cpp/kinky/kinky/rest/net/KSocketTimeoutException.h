#pragma once

#include <exception>
#include <string>
#include <iostream>
#include <sstream>
#include <kinky/memory/KMemoryManager.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	namespace lang {

		class KSocketTimeoutException: public exception {
			public:
				KSocketTimeoutException();
				virtual ~KSocketTimeoutException() throw();

				virtual string what() throw ();

				friend ostream& operator<<(ostream& os, kinky::lang::KSocketTimeoutException& f) {
					os << "SOCKET TIMEOUT >> code: 0 || message: client socket read wait timed out" << endl << flush;
					return os;
				}


		};

	}
}
