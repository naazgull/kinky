#include <kinky/rest/net/KClientSocketThreadPool.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <limits>

#ifndef DISTRIBUTION_ALGORITHM
#define DISTRIBUTION_ALGORITHM 2
#endif

vector<KClientSocketThread<KHTTPRequest, KHTTPResponse>*>* kinky::rest::net::processors;
vector<KClientSocketThreadDistributor*>* kinky::rest::net::distributors;

kinky::rest::net::KClientSocketThreadPool::KClientSocketThreadPool(size_t maxParam, string semKeyFile, int project_id) :
	KThreadPool<KClientSocketThread<KHTTPRequest, KHTTPResponse>*>(maxParam), semkey(semKeyFile.data()), project_id(project_id) {
	this->last = 0;
	this->last_processor = 0;
	this->max_processors = ::ceil((float) (((float) maxParam) / 5));

	kinky::rest::net::processors = new vector<KClientSocketThread<KHTTPRequest, KHTTPResponse>*>();
	kinky::rest::net::distributors = new vector<KClientSocketThreadDistributor*>();

	this->mutex = new pthread_mutex_t();
	pthread_mutexattr_init(&this->attr);
	pthread_mutex_init(this->mutex, &this->attr);
}

kinky::rest::net::KClientSocketThreadPool::~KClientSocketThreadPool() {
	for (vector<KClientSocketThread<KHTTPRequest, KHTTPResponse>*>::iterator it = kinky::rest::net::processors->begin(); it != kinky::rest::net::processors->end(); it++) {
		KClientSocketThread<KHTTPRequest, KHTTPResponse>* thread = *it;
		delete thread;
	}
	delete kinky::rest::net::processors;
	key_t key = ftok(this->semkey.data(), this->project_id);
	int sem = semget(key, this->getMaxThreads(), 0777);
	semctl(sem, 0, IPC_RMID);

	for (vector<KClientSocketThreadDistributor*>::iterator it = kinky::rest::net::distributors->begin(); it != kinky::rest::net::distributors->end(); it++) {
		KClientSocketThreadDistributor* thread = *it;
		delete thread;
	}
	delete kinky::rest::net::distributors;
	key_t key2 = ftok(this->semkey.data(), this->project_id + 1000);
	int sem2 = semget(key2, this->max_processors, 0777);
	semctl(sem2, 0, IPC_RMID);

	pthread_mutexattr_destroy(&this->attr);
	pthread_mutex_destroy(this->mutex);
	delete this->mutex;
}

void kinky::rest::net::KClientSocketThreadPool::distribute(KHTTPSocket* channel) {
	if (kinky::rest::net::distributors->size() < this->max_processors) {
		KClientSocketThreadDistributor* distributor = new KClientSocketThreadDistributor(this);
		kinky::rest::net::distributors->push_back(distributor);
		distributor->initialize(this->last_processor, this->max_processors, this->semkey, this->project_id + 1000);
		this->last_processor++;
		distributor->exec(channel);
	}
	else {
		this->last_processor++;
		if (this->last_processor >= this->max_processors) {
			this->last_processor = 0;
		}
		kinky::rest::net::distributors->at(this->last_processor)->exec(channel);
	}
}

void kinky::rest::net::KClientSocketThreadPool::borrow(KHTTPSocket* channel) {
	pthread_mutex_lock(this->mutex);
	if (kinky::rest::net::processors->size() < this->getMaxThreads()) {
		KClientSocketThread<KHTTPRequest, KHTTPResponse>* thread = new KClientSocketThread<KHTTPRequest, KHTTPResponse>(NULL);
		kinky::rest::net::processors->push_back(thread);
		thread->initialize(this->last, this->getMaxThreads(), this->semkey, this->project_id);
		this->last++;
		pthread_mutex_unlock(this->mutex);

		thread->exec(channel);
	}
	else {
		if (DISTRIBUTION_ALGORITHM == 2) {
			size_t max = std::numeric_limits<size_t>::max();
			size_t proposed = 0;
			for (size_t it = 0; it !=this->getMaxThreads(); it++) {
				size_t pending = kinky::rest::net::processors->at(it)->pending();
				if (pending == 0) {
					proposed = it;
					break;
				}
				if (max > pending) {
					max = pending;
					proposed = it;
				}
			}
			pthread_mutex_unlock(this->mutex);

			kinky::rest::net::processors->at(proposed)->exec(channel);
		}
		else {
			this->last++;
			if (this->last >= this->getMaxThreads()) {
				this->last = 0;
			}
			size_t last = this->last;
			pthread_mutex_unlock(this->mutex);

			kinky::rest::net::processors->at(last)->exec(channel);
		}
	}

}


kinky::rest::net::KClientSocketThreadDistributor::KClientSocketThreadDistributor(KClientSocketThreadPool* poolParam) :
		KThread<KHTTPSocket>(NULL), pool(poolParam) {
}

kinky::rest::net::KClientSocketThreadDistributor::~KClientSocketThreadDistributor() {
}

void kinky::rest::net::KClientSocketThreadDistributor::printm(const char* prefix) {
}

void kinky::rest::net::KClientSocketThreadDistributor::execute() {
	KHTTPSocket* channel =  this->getTarget();
	channel->preconsume();
	this->data = NULL;
	this->pool->borrow(channel);
}
