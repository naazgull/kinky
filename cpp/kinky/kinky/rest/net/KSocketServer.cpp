#include <kinky/rest/net/KSocketServer.h>

kinky::rest::net::KSocketServer::KSocketServer() {
	this->sockfd = -1;
}

kinky::rest::net::KSocketServer::~KSocketServer() {
	close(this->sockfd);
}

void kinky::rest::net::KSocketServer::connect(int port) {
	this->sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (this->sockfd < 0) {
		throw new KRestException(1, "Could not create server socket");
	}

	int opt = 1;
	if (setsockopt(this->sockfd, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt)) == SO_ERROR) {
		close(this->sockfd);
		this->sockfd = -1;
		throw new KRestException(2, "Could not bind to the provided port");
	}

	struct sockaddr_in serv_addr;
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	if (bind(this->sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		close(this->sockfd);
		this->sockfd = -1;
		throw new KRestException(3, "Could not bind to the provided port");
	}
	listen(this->sockfd, 100);
}

void kinky::rest::net::KSocketServer::wait() {
	if (this->sockfd != -1) {
		struct sockaddr_in* cli_addr = new struct sockaddr_in();
		socklen_t clilen = sizeof(struct sockaddr_in);
		int newsockfd = accept(this->sockfd, (struct sockaddr *) cli_addr, &clilen);

		if (newsockfd < 0) {
			//throw new KRestException(3, "Could not accept client socket");
			return;
		}

		struct linger so_linger;
		so_linger.l_onoff = 1;
		so_linger.l_linger = 30;
		setsockopt(newsockfd,SOL_SOCKET, SO_LINGER, &so_linger, sizeof so_linger);
		this->createReplyThread(newsockfd, cli_addr);
	}
}
