#pragma once

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <kinky/rest/net/KSocketTimeoutException.h>
#include <kinky/memory/KMemoryManager.h>
#include <kinky/rest/KRestException.h>
#include <kinky/rest/net/KSocket.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace memory;

	namespace rest {

		namespace net {

			namespace ssl {

				template<class T, class R>
				class KSSLSocket: public kinky::rest::net::KSocket<T, R> {
					public:
						KSSLSocket(int sockfdParam, struct sockaddr_in* addressParam, SSL_CTX* ssl);
						virtual ~KSSLSocket();

						void reply(managed_ptr<string>* rep);

						managed_ptr<string>* readRequest(size_t contentLength = -1);

					protected:
						SSL* sslstream;
				};
			}
		}
	}
}

template<class T, class R>
kinky::rest::net::ssl::KSSLSocket<T, R>::KSSLSocket(int sockfdParam, struct sockaddr_in* addressParam, SSL_CTX* sslParam) :
		kinky::rest::net::KSocket<T, R>(sockfdParam, addressParam) {
	this->sslstream = SSL_new(sslParam);
	SSL_set_fd(this->sslstream, this->sockfd);
}
template<class T, class R>
kinky::rest::net::ssl::KSSLSocket<T, R>::~KSSLSocket() {
	SSL_free(this->sslstream);
}

template<class T, class R>
void kinky::rest::net::ssl::KSSLSocket<T, R>::reply(managed_ptr<string>* rep) {
	try {
		int opts = fcntl(this->sockfd, F_GETFL);
		opts ^= O_NONBLOCK;
		fcntl(this->sockfd, F_SETFL, opts);

		int size = SSL_write(this->sslstream, (*rep)->data(), (*rep)->length());
		if (size == -1) {
		}
	}
	catch (exception& e) {
	}
}

template<class T, class R>
kinky::memory::managed_ptr<string>* kinky::rest::net::ssl::KSSLSocket<T, R>::readRequest(size_t contentLength) {
	char* buffer = new char[256];
	int nRead = -1;
	string* toReturn = new string();

	int opts = fcntl(this->sockfd, F_GETFL);
	opts = opts & ~O_NONBLOCK;
	if (fcntl(this->sockfd, F_SETFL, opts) < 0) {
	}

	fd_set fds;
	struct timeval timeout;

	timeout.tv_sec = KSOCKET_TIMEOUT;
	timeout.tv_usec = 0;
	FD_ZERO(&fds);
	FD_SET(this->sockfd, &fds);

	if (select(this->sockfd + 1, &fds, NULL, NULL, &timeout) <= 0) {
		throw new KSocketTimeoutException();
	}

	if (contentLength == (size_t) -1 && SSL_accept(this->sslstream) == -1) {
		if (SSL_get_error(this->sslstream, -1) == SSL_ERROR_WANT_READ) {
			return NULL;
		}
		else {
			ERR_print_errors_fp(stdout);
			return NULL;
		}
	}


	do {
		bzero(buffer, 256);
		nRead = SSL_read(this->sslstream, buffer, 255);
		if (nRead <= 0) {
			break;
		}
		toReturn->append(buffer, nRead);

		bool headerDone = toReturn->find(HEADER_SEPARATOR) != string::npos;
		if (headerDone || contentLength != (size_t) -1) {
			opts = (opts | O_NONBLOCK);
			fcntl(this->sockfd, F_SETFL, opts);
		}
		else if (!headerDone) {
			nRead = 255;
		}
	}
	while ((nRead == 255 && contentLength == (size_t) -1) || (contentLength != (size_t) -1 && contentLength != toReturn->length()));

	delete[] buffer;
	if (toReturn->length() == 0) {
		delete toReturn;
		return NULL;
	}

	return new managed_ptr<string>(toReturn);
}
