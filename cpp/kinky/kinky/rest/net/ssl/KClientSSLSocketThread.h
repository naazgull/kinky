#pragma once

#include <kinky/memory/KMemoryManager.h>
#include <kinky/rest/KRestException.h>
#include <kinky/rest/net/ssl/KSSLSocket.h>
#include <kinky/rest/thread/KThread.h>
#include <kinky/rest/net/KSocketTimeoutException.h>
#include <kinky/rest/KRestAssynchronousRequest.h>
#include <kinky/utils/KDateTime.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <time.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace memory;

	namespace rest {

		using namespace thread;

		namespace net {

			namespace ssl {

				template<class T, class R>
				class KClientSSLSocketThread: public KThread<KSSLSocket<T, R> > {
					public:
						KClientSSLSocketThread(KSSLSocket<T, R>* socket);
						virtual ~KClientSSLSocketThread();

						void execute();

						static void processException(KRestException* e, KSSLSocket<T, R>* target, ostream& oss);

						static void processException(exception& e, KSSLSocket<T, R>* target, ostream& oss);
				};
			}
		}
	}
}

template<class T, class R>
kinky::rest::net::ssl::KClientSSLSocketThread<T, R>::KClientSSLSocketThread(KSSLSocket<T, R>* socket) :
		KThread<KSSLSocket<T, R> >(socket) {
}
template<class T, class R>
kinky::rest::net::ssl::KClientSSLSocketThread<T, R>::~KClientSSLSocketThread() {
}

template<class T, class R>
void kinky::rest::net::ssl::KClientSSLSocketThread<T, R>::execute() {
#ifdef LOG
	clock_t start = clock();
#endif
	managed_ptr<T> *req = NULL;
	KSSLSocket<T, R> *target = this->getTarget();

	ostringstream oss;
	try {
		while ((req = target->consume()) != NULL) {

#ifdef LOG
#ifdef LOG_COLORS
			(**req).log(oss, true);
#else
			(**req).log(oss);
#endif
#endif

			try {

				managed_ptr<R> *rep = target->process(req->get());

#ifdef LOG
#ifdef LOG_COLORS
				(**rep).log(oss, true);
#else
				(**rep).log(oss);
#endif
#endif

				managed_ptr<string> *repStr = target->stringify(rep);
				target->reply(repStr);

				repStr->release();
				rep->release();
				req->release();
			}
			catch (KRestAssynchronousRequest<R>* e) {
				managed_ptr<R> *rep = e->target;

				managed_ptr<string> *repStr = target->stringify(rep);
				target->reply(repStr);
				repStr->release();
				rep->release();

				try {
					managed_ptr<R> *repAssync = target->process(req->get());
#ifdef LOG
#ifdef LOG_COLORS
					(**repAssync).log(oss, true);
#else
					(**repAssync).log(oss);
#endif
#endif
					repAssync->release();
					req->release();
				}
				catch (KRestException* ex) {
					KClientSSLSocketThread::processException(ex, target, oss);
					req->release();
					delete ex;
				}
				catch (exception& ex) {
					KClientSSLSocketThread::processException(ex, target, oss);
					req->release();
				}
				delete e;
			}
			catch (KRestException* e) {
				KClientSSLSocketThread::processException(e, target, oss);
				req->release();
				delete e;
			}
			catch (exception& e) {
				KClientSSLSocketThread::processException(e, target, oss);
				req->release();
			}
			break;
		}
	}
	catch (KSocketTimeoutException* e) {
		delete e;
#ifdef LOG
#ifdef LOG_COLORS
		oss << "\033[1;30m";
#endif

		oss << "[T" << (unsigned int) pthread_self() << " IP" << *target->getClientAddress() << " " << kinky::utils::date::format("D%Y-%m-%dT%H:%M:%S") << " CONNECTION TIMED OUT] " << endl << flush;

#ifdef LOG_COLORS
		oss << "\033[0m" << flush;
#endif
#endif
	}

#ifdef LOG
	clock_t end = clock();
	float delta = (((float) end - start) / CLOCKS_PER_SEC) * 1000;
	(*kinky::io::out) << "[Ellapsed: " << setprecision(6) << (delta > 59000 ? delta / 60000 : (delta > 3559000 ? delta / 3600000 : (delta > 999 ? delta / 1000 : delta))) << " " << (delta > 59000 ? "min(s)" : (delta > 3559000 ? "hour(s)" : (delta > 999 ? "sec(s)" : "msec(s)"))) << "] " << oss.str() << flush;
#endif

}

template<class T, class R>
void kinky::rest::net::ssl::KClientSSLSocketThread<T, R>::processException(KRestException* e, KSSLSocket<T, R>* target, ostream& oss) {
	managed_ptr<R> *rep = target->process(e);

#ifdef LOG
#ifdef LOG_COLORS
	(**rep).log(oss, true);
#else
	(**rep).log(oss);
#endif
#endif

	managed_ptr<string> *repStr = target->stringify(rep);
	target->reply(repStr);

	delete e;
	rep->release();
	repStr->release();

}

template<class T, class R>
void kinky::rest::net::ssl::KClientSSLSocketThread<T, R>::processException(exception& e, KSSLSocket<T, R>* target, ostream& oss) {
	managed_ptr<R> *rep = target->process(e);

#ifdef LOG
#ifdef LOG_COLORS
	(**rep).log(oss, true);
#else
	(**rep).log(oss);
#endif
#endif

	managed_ptr<string>* repStr = target->stringify(rep);
	target->reply(repStr);

	rep->release();
	repStr->release();
}
