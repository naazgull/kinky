#pragma once

#include <kinky/rest/thread/KThreadPool.h>
#include <kinky/rest/net/ssl/KClientSSLSocketThread.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/rest/http/ssl/KHTTPSSocket.h>

namespace kinky {

	namespace rest {

		using namespace thread;
		using namespace http;
		using namespace http::ssl;

		namespace net {

			namespace ssl {

				class KClientSSLSocketThreadPool;

				class KClientSSLSocketThreadDistributor: public KThread<KHTTPSSocket>  {
					public:
						KClientSSLSocketThreadDistributor(KClientSSLSocketThreadPool* pool);
						virtual ~KClientSSLSocketThreadDistributor();

						virtual void execute();

					private:
						KClientSSLSocketThreadPool* pool;
				};

				extern vector<KClientSSLSocketThread<KHTTPRequest, KHTTPResponse>*>* processors;
				extern vector<KClientSSLSocketThreadDistributor*>* distributors;

				class KClientSSLSocketThreadPool: public KThreadPool<KClientSSLSocketThread<KHTTPRequest, KHTTPResponse>*> {
					public:
						KClientSSLSocketThreadPool(size_t max = 1000, string semKeyFile = "/dev/null", int project_id = time(NULL));
						virtual ~KClientSSLSocketThreadPool();

						void distribute(KHTTPSSocket* channel);

						void borrow(KHTTPSSocket* channel);

						pthread_mutex_t* mutex;
						pthread_mutexattr_t attr;

					private:
						string semkey;
						int project_id;
						size_t last;
						size_t last_processor;
						size_t max_processors;
				};
			}
		}
	}
}
