#pragma once

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include <kinky/rest/net/KSocket.h>
#include <kinky/rest/thread/KThread.h>
#include <kinky/rest/net/KSocketServer.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	namespace rest {

		namespace net {

			namespace ssl {

				class KSSLSocketServer: public KSocketServer {
					public:
						KSSLSocketServer(string certificate, string key, bool useChain = false);
						virtual ~KSSLSocketServer();

						virtual void connect(int port);

						virtual void wait();

						virtual void createReplyThread(int sockfd, sockaddr_in* address) = 0;

						SSL_CTX* getCTX();

						void loadCertificates(SSL_CTX* ctx, string certFile, string keyFile);

						void showCerts(SSL* ssl);

					protected:
						SSL_CTX* sslContext;
						string sslCertificate;
						string sslKey;
						bool useChain;
				};
			}
		}
	}
}
