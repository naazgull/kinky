#include <kinky/rest/net/ssl/KSSLSocketServer.h>

kinky::rest::net::ssl::KSSLSocketServer::KSSLSocketServer(string certificate, string key, bool useChainParam)  : sslCertificate(certificate.data()), sslKey(key.data()), useChain(useChainParam){
	this->sockfd = -1;
	SSL_library_init();
	this->sslContext = this->getCTX();
}

kinky::rest::net::ssl::KSSLSocketServer::~KSSLSocketServer() {
	close(this->sockfd);
	EVP_cleanup();
	SSL_CTX_free(this->sslContext);
}

void kinky::rest::net::ssl::KSSLSocketServer::connect(int port) {
	KSocketServer::connect(port);
	this->loadCertificates(this->sslContext, this->sslCertificate, this->sslKey);
}

SSL_CTX* kinky::rest::net::ssl::KSSLSocketServer::getCTX() {
	SSL_CTX *ctx;

	OpenSSL_add_all_algorithms();
	SSL_load_error_strings();
	ctx = SSL_CTX_new(SSLv23_server_method());
	if (ctx == NULL) {
		ERR_print_errors_fp(stderr);
		abort();
	}
	return ctx;
}

void kinky::rest::net::ssl::KSSLSocketServer::loadCertificates(SSL_CTX* ctx, string certFile, string keyFile) {
	if (this->useChain) {
		if (SSL_CTX_use_certificate_chain_file(ctx, certFile.data()) <= 0) {
			ERR_print_errors_fp(stderr);
			abort();
		}
	}
	else {
		if (SSL_CTX_use_certificate_file(ctx, certFile.data(), SSL_FILETYPE_PEM) <= 0) {
			ERR_print_errors_fp(stderr);
			abort();
		}
	}
	if (SSL_CTX_use_PrivateKey_file(ctx, keyFile.data(), SSL_FILETYPE_PEM) <= 0) {
		ERR_print_errors_fp(stderr);
		abort();
	}
	if (!SSL_CTX_check_private_key(ctx)) {
		fprintf(stderr, "Private key does not match the public certificate\n");
		abort();
	}
}

void kinky::rest::net::ssl::KSSLSocketServer::wait() {
	if (this->sockfd != -1) {
		struct sockaddr_in* cli_addr = new struct sockaddr_in();
		socklen_t clilen = sizeof(struct sockaddr_in);
		int newsockfd = accept(this->sockfd, (struct sockaddr *) cli_addr, &clilen);

		if (newsockfd < 0) {
			//throw new KRestException(3, "Could not accept client socket");
			return;
		}

		this->createReplyThread(newsockfd, cli_addr);
	}
}
