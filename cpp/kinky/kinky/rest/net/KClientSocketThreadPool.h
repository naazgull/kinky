#pragma once

#include <kinky/rest/thread/KThreadPool.h>
#include <kinky/rest/net/KClientSocketThread.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/rest/http/KHTTPSocket.h>

namespace kinky {

	namespace rest {

		using namespace thread;
		using namespace http;

		namespace net {
			class KClientSocketThreadPool;

			class KClientSocketThreadDistributor: public KThread<KHTTPSocket>  {
				public:
					KClientSocketThreadDistributor(KClientSocketThreadPool* pool);
					virtual ~KClientSocketThreadDistributor();

					virtual void execute();
					virtual void printm(const char* prefix = NULL);

				private:
					KClientSocketThreadPool* pool;
			};

			extern vector<KClientSocketThread<KHTTPRequest, KHTTPResponse>*>* processors;
			extern vector<KClientSocketThreadDistributor*>* distributors;

			class KClientSocketThreadPool: public KThreadPool<KClientSocketThread<KHTTPRequest, KHTTPResponse>*> {
				public:
					KClientSocketThreadPool(size_t max = 1000, string semKeyFile = "/dev/null", int project_id = time(NULL));
					virtual ~KClientSocketThreadPool();

					void distribute(KHTTPSocket* channel);

					void borrow(KHTTPSocket* channel);

					pthread_mutex_t* mutex;
					pthread_mutexattr_t attr;

				private:
					string semkey;
					int project_id;
					size_t last;
					size_t last_processor;
					size_t max_processors;
			};
		}
	}
}
