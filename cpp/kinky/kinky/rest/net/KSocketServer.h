#pragma once

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <kinky/rest/net/KSocket.h>
#include <kinky/rest/thread/KThread.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	namespace rest {

		namespace net {
			class KSocketServer {
				public:
					KSocketServer();
					virtual ~KSocketServer();

					virtual void connect(int port);

					virtual void wait();

					virtual void initThreadPool(size_t maxThreads, string semKeyFile, int projec_id = -1) = 0;

					virtual void createReplyThread(int sockfd, sockaddr_in* address) = 0;

				protected:
					int sockfd;
			};
		}
	}
}
