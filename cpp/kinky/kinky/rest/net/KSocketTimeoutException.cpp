#include <kinky/rest/net/KSocketTimeoutException.h>

kinky::lang::KSocketTimeoutException::KSocketTimeoutException() {
}

kinky::lang::KSocketTimeoutException::~KSocketTimeoutException() throw () {
}

string kinky::lang::KSocketTimeoutException::what() throw () {
	ostringstream oss;
	oss << *this << endl << flush;
	kinky::memory::managed_ptr<string>* ret = new kinky::memory::managed_ptr<string>(new string(oss.str()));
	return *static_cast<string*>(ret->pointed());
}
