#include <kinky/rest/KRestException.h>

kinky::rest::KRestException::KRestException(int codeParam, string messageParam) :
		code(codeParam), errcode(0), message(new managed_ptr<string>(new string(messageParam))), otherParams(NULL) {
}

kinky::rest::KRestException::KRestException(int codeParam, string messageParam, KObject* otherParamsParam) :
		code(codeParam), errcode(0), message(new managed_ptr<string>(new string(messageParam))), otherParams(new managed_ptr<KObject>(otherParamsParam)) {
}

kinky::rest::KRestException::KRestException(int codeParam, string messageParam, int errcodeParam) :
		code(codeParam), errcode(errcodeParam), message(new managed_ptr<string>(new string(messageParam))), otherParams(NULL) {
}

kinky::rest::KRestException::KRestException(int codeParam, string messageParam, int errcodeParam, KObject* otherParamsParam) :
		code(codeParam), errcode(errcodeParam), message(new managed_ptr<string>(new string(messageParam))), otherParams(new managed_ptr<KObject>(otherParamsParam)) {
}

kinky::rest::KRestException::~KRestException() throw () {
	this->message->release();
	if (this->otherParams != NULL) {
		this->otherParams->release();
	}
}

string* kinky::rest::KRestException::getMessage() {
	return this->message->get();
}

int kinky::rest::KRestException::getCode() {
	return this->code;
}

int kinky::rest::KRestException::getErrorCode() {
	return this->errcode;
}

kinky::lang::KObject* kinky::rest::KRestException::getOtherParams() {
	if (this->otherParams != NULL) {
		return this->otherParams->get();
	}
	else {
		return NULL;
	}
}

string kinky::rest::KRestException::what() throw () {
	ostringstream oss;
	oss << *this << endl << flush;
	return **(new managed_ptr<string>(new string(oss.str())));
}
