#include <kinky/rest/KRestAPI.h>
#include <kinky/rest/KDocRoot.h>
#include <kinky/rest/KWRMLRelationRoot.h>
#include <kinky/rest/KWRMLSchemaRoot.h>

#include <kinky/oauth/controllers/AuthorizeController.h>
#include <kinky/oauth/controllers/LoginController.h>
#include <kinky/oauth/controllers/TokenController.h>

#include <kinky/media/controllers/KMailSelf.h>

#include <kinky/social/controllers/FacebookSelf.h>
#include <kinky/social/controllers/FlickrSelf.h>
#include <kinky/social/controllers/GoogleSelf.h>
#include <kinky/social/controllers/InstagramSelf.h>
#include <kinky/social/controllers/TumblrSelf.h>
#include <kinky/social/controllers/TwitterSelf.h>
#include <kinky/social/controllers/YahooSelf.h>
#include <kinky/social/controllers/YoutubeSelf.h>
#include <kinky/social/controllers/LinkedinSelf.h>

#include <kinky/cloudstorage/controllers/DropboxSelf.h>
#include <kinky/cloudstorage/controllers/BoxSelf.h>
#include <kinky/cloudstorage/controllers/CopySelf.h>
#include <kinky/cloudstorage/controllers/BitbucketSelf.h>

#ifdef DEPRECATED
kinky::rest::KRestAPI::KRestAPI() :
		operations(new KRestAPI::KRestOpTable()), libName(NULL), libHandle(NULL), nOperations(0), configuration(NULL), oauthController(NULL), tokenController(NULL), fileServeController(NULL), proxyPassController(NULL) {
#endif
kinky::rest::KRestAPI::KRestAPI() :
		operations(new KRestAPI::KRestOpTable()), libName(NULL), libHandle(NULL), nOperations(0), configuration(NULL), oauthController(NULL), tokenController(NULL), proxyPassController(NULL) {
	kinky::initSSLMutex();
}

kinky::rest::KRestAPI::~KRestAPI() {
	for (KRestOpTable::iterator op = this->operations->begin(); op != this->operations->end(); op++) {
		delete (*op);
	}
	delete this->operations;
	dlclose(this->libHandle);
	if (this->libName != NULL) {
		delete this->libName;
	}
	if (this->configuration != NULL) {
		delete this->configuration;
	}
	if (this->oauthController != NULL) {
		delete this->oauthController;
	}
	if (this->tokenController != NULL) {
		delete this->tokenController;
	}
#ifdef DEPRECATED
	if (this->fileServeController != NULL) {
		delete this->fileServeController;
	}
#endif
	if (this->proxyPassController != NULL) {
		delete this->proxyPassController;
	}
	kinky::deinitSSLMutex();
}

KRestOperation* kinky::rest::KRestAPI::getDocRoot(bool initialization) {
	string docRoot = "/";
	KRestOperation* op = NULL;
	if ((op = this->operationFor(&docRoot)) == NULL) {
		return (new kinky::rest::KDocRoot((*this->configuration)["wrml_uri"] != NULL_KOBJECT ? this->configuration->get("wrml_uri") : ""));
	}
	else {
		return op;
	}
	return NULL;
}

KRestOperation* kinky::rest::KRestAPI::getRelationRoot(bool initialization) {
	string docRoot = "/wrml-edit/relations";
	KRestOperation* op = NULL;
	if ((op = this->operationFor(&docRoot)) == NULL) {
		return (new kinky::rest::KWRMLRelationRoot((*this->configuration)["wrml_uri"] != NULL_KOBJECT ? this->configuration->get("wrml_uri") : ""));
	}
	else {
		return op;
	}
	return NULL;
}

KRestOperation* kinky::rest::KRestAPI::getSchemaRoot(bool initialization) {
	string docRoot = "/wrml-edit/schemas";
	KRestOperation* op = NULL;
	if ((op = this->operationFor(&docRoot)) == NULL) {
		return (new kinky::rest::KWRMLSchemaRoot((*this->configuration)["wrml_uri"] != NULL_KOBJECT ? this->configuration->get("wrml_uri") : ""));
	}
	else {
		return op;
	}
	return NULL;
}

void* kinky::rest::KRestAPI::getLibHandle() {
	return this->libHandle;
}

void kinky::rest::KRestAPI::setLibHandle(void *handle) {
	this->libHandle = handle;
}

string* kinky::rest::KRestAPI::getLibName() {
	return this->libName;
}

void kinky::rest::KRestAPI::setLibName(string name) {
	this->libName = new string(name.data());
}

void kinky::rest::KRestAPI::setConfFile(string file) {
	this->configuration = KProperties::parse(file);
}

int kinky::rest::KRestAPI::registerOperation(KRestOperation *operation) {
	this->operations->push_back(operation);
	operation->setAPI(this);
	operation->init();
	return this->nOperations++;
}

KProperties* kinky::rest::KRestAPI::getConfiguration() {
	return this->configuration;
}

kinky::oauth::KOAuthController* kinky::rest::KRestAPI::getOAuthController() {
	if (this->oauthController == NULL) {
		this->oauthController = new KOAuthController();
		this->oauthController->setAPI(this);
	}
	return this->oauthController;
}

void kinky::rest::KRestAPI::setOAuthController(kinky::oauth::KOAuthController* oauthController) {
	if (this->oauthController != NULL) {
		delete this->oauthController;
	}
	this->oauthController = oauthController;
	this->oauthController->setAPI(this);
}

kinky::scopes::KScopeController* kinky::rest::KRestAPI::getScopeController() {
	if (this->tokenController == NULL) {
		this->tokenController = new kinky::scopes::KScopeController();
		this->tokenController->setAPI(this);
	}
	return this->tokenController;
}

void kinky::rest::KRestAPI::setScopeController(kinky::scopes::KScopeController* tokenController) {
	if (this->tokenController != NULL) {
		delete this->tokenController;
	}
	this->tokenController = tokenController;
	this->tokenController->setAPI(this);
}

kinky::fileserve::KProxyPassController* kinky::rest::KRestAPI::getProxyPassController() {
	return this->proxyPassController;
}

void kinky::rest::KRestAPI::setProxyPassController(kinky::fileserve::KProxyPassController* fileServeController) {
	if (this->proxyPassController != NULL) {
		delete this->proxyPassController;
	}
	this->proxyPassController = fileServeController;
	this->proxyPassController->setAPI(this);
}

#ifdef DEPRECATED
kinky::fileserve::KFileServeController* kinky::rest::KRestAPI::getFileServeController() {
	if (this->fileServeController == NULL && this->getConfiguration()->get("krest_fileserve_mode") == "on" && this->getConfiguration()->getString("krest_fileserve_path") != NULL) {
		this->fileServeController = new kinky::fileserve::KFileServeController(this->getConfiguration()->get("krest_fileserve_path"), "index.html");
		this->fileServeController->setAPI(this);
	}
	return this->fileServeController;
}

void kinky::rest::KRestAPI::setFileServeController(kinky::fileserve::KFileServeController* fileServeController) {
	if (this->fileServeController != NULL) {
		delete this->fileServeController;
	}
	this->fileServeController = fileServeController;
	this->fileServeController->setAPI(this);
}
#endif

KRestOperation* kinky::rest::KRestAPI::operationFor(string *url) {
	for (KRestOpTable::iterator op = this->operations->begin(); op != this->operations->end(); op++) {
		regex_t* re = (*op)->getURL();
		if (regexec(re, url->c_str(), (size_t) (0), NULL, 0) == 0) {
			return (*op);
		}
	}

	return NULL;
}

void kinky::rest::KRestAPI::getRegisteredOperations(vector<KRestOperation*>* list) {
	for (KRestOpTable::iterator op = this->operations->begin(); op != this->operations->end(); op++) {
		list->push_back(*op);
	}
}

void kinky::rest::KRestAPI::registerOAuth() {
	KRestOperation* op;

	op = new kinky::oauth::controllers::LoginController();
	this->registerOperation(op);

	op = new kinky::oauth::controllers::AuthorizeController();
	this->registerOperation(op);

	op = new kinky::oauth::controllers::TokenController();
	this->registerOperation(op);
}

void kinky::rest::KRestAPI::registerMedia() {
	KRestOperation* op;

	op = new kinky::media::controllers::KMailSelf();
	this->registerOperation(op);

}

void kinky::rest::KRestAPI::registerSocial() {
	KRestOperation* op;

	op = new kinky::social::controllers::FacebookSelf();
	this->registerOperation(op);

	op = new kinky::social::controllers::FlickrSelf();
	this->registerOperation(op);

	op = new kinky::social::controllers::GoogleSelf();
	this->registerOperation(op);

	op = new kinky::social::controllers::InstagramSelf();
	this->registerOperation(op);

	op = new kinky::social::controllers::TumblrSelf();
	this->registerOperation(op);

	op = new kinky::social::controllers::TwitterSelf();
	this->registerOperation(op);

	op = new kinky::social::controllers::YahooSelf();
	this->registerOperation(op);

	op = new kinky::social::controllers::YoutubeSelf();
	this->registerOperation(op);

	op = new kinky::social::controllers::LinkedinSelf();
	this->registerOperation(op);
}

void kinky::rest::KRestAPI::registerCloudStorage() {
	KRestOperation* op;

	op = new kinky::cloudstorage::controllers::DropboxSelf();
	this->registerOperation(op);

	op = new kinky::cloudstorage::controllers::BoxSelf();
	this->registerOperation(op);

	op = new kinky::cloudstorage::controllers::CopySelf();
	this->registerOperation(op);

	op = new kinky::cloudstorage::controllers::BitbucketSelf();
	this->registerOperation(op);
}

void kinky::rest::KRestAPI::registerFileServe() {
	if (this->proxyPassController == NULL && this->getConfiguration()->get("krest_proxypass_mode") == "on" ) {
		this->setProxyPassController(new kinky::fileserve::KProxyPassController());
	}
}

/* DEPRECATED: user "new KToken(string token, string privKey)" instead*/
kinky::oauth::KToken* kinky::rest::KRestAPI::unserializeToken(string* token, string privKey) {
	int indexOfPipe = token->find('|');
	string toUnserialize = token->substr(indexOfPipe + 1);
	istringstream in(toUnserialize);
	ostringstream out;
	kinky::utils::base64_decode(in, out);

	pair<string, size_t>* decrypted = kinky::utils::encryption::KXORCipher::decipher(out.str(), privKey);

	stringstream ss(decrypted->first);
	KToken* ktoken = new KToken();

	string *item = new string();
	std::getline(ss, *item, ':');
	ktoken->setClientID(item);

	item = new string();
	std::getline(ss, *item, ':');
	ktoken->setClientSecret(item);

	item = new string();
	std::getline(ss, *item, ':');
	ktoken->setPermissions(item);

	item = new string();
	std::getline(ss, *item, ':');
	ktoken->setUserID(item);

	item = new string();
	std::getline(ss, *item, ':');
	if (item->length() != 0) {
		istringstream tss(*item);
		long timestamp;
		tss >> timestamp;
		ktoken->setExpirationDate(timestamp);
		delete item;

		item = new string();
		std::getline(ss, *item, ':');
		if (item->length() != 0) {
			ktoken->setData(item);
		}
		else {
			delete item;
		}
	}
	else {
		delete item;
	}

	delete decrypted;

	return ktoken;
}

/* DEPRECATED: user "ostringstream oss; oss << *token << flush;" instead*/
string* kinky::rest::KRestAPI::serializeToken(KToken* params, string privKey) {
	string *serializedToken = params->prettify(":");
	string padded = kinky::utils::encryption::KXORCipher::pad(serializedToken->data(), 128, ':');

	pair<string, size_t>* encrypted = kinky::utils::encryption::KXORCipher::cipher(padded, privKey);
	stringstream in(encrypted->first);
	ostringstream out;
	out << encrypted->second << "|" << flush;
	kinky::utils::base64_encode(in, out);
	out << flush;

	delete encrypted;

	return new string(out.str());
}

