#include <kinky/social/KSocialTokenExpiredException.h>

kinky::social::KSocialTokenExpiredException::KSocialTokenExpiredException(string messageParam) :
		message(new kinky::memory::managed_ptr<string>(new string(messageParam))) {
}

kinky::social::KSocialTokenExpiredException::KSocialTokenExpiredException(kinky::memory::managed_ptr<string>* messageParam) :
		message(messageParam) {
}

kinky::social::KSocialTokenExpiredException::~KSocialTokenExpiredException() throw () {
	if (this->message != NULL) {
		this->message->release();
	}
}

string* kinky::social::KSocialTokenExpiredException::getMessage() {
	return this->message->get();
}

kinky::memory::managed_ptr<string>* kinky::social::KSocialTokenExpiredException::get() {
	return this->message;
}

string kinky::social::KSocialTokenExpiredException::what() throw () {
	ostringstream oss;
	oss << *this << endl << flush;
	return **(new kinky::memory::managed_ptr<string>(new string(oss.str())));
}
