#include <kinky/social/Twitter.h>

/* Deprecated: use kinky::social::common::curlWriter instead */
int kinky::social::twitter::curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer) {
	return kinky::social::common::curlWriter(data, size, nmemb, buffer);
}

kinky::lang::KObject* kinky::social::twitter::get(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params) {
	string* url = new string("https://api.twitter.com");
	url->append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
			"oauth_consumer_key" << consumerKey <<
			"oauth_signature_method" << "HMAC-SHA1" <<
			"oauth_token" << accessToken <<
			"oauth_version" << "1.0" <<
			"oauth_nonce" << nonce->data() <<
			"oauth_timestamp" << timestamp->data();

	params->sort();

	KObject* profile_data = kinky::social::oauth1::get(url->data(), params, consumerSecret, tokenSecret);

	delete nonce;
	delete timestamp;
	delete url;

	return profile_data;
}

kinky::lang::KObject* kinky::social::twitter::post(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params, KObject* body) {
	string* url = new string("https://api.twitter.com");
	url->append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0";

	params->sort();

	ostringstream oss;
	oss << *body << flush;
	string strbody(oss.str());

	KObject* profile_data = kinky::social::oauth1::post(url->data(), params, consumerSecret, tokenSecret, &strbody);

	delete nonce;
	delete timestamp;
	delete url;

	return profile_data;
}

kinky::lang::KObject* kinky::social::twitter::getURL(const char* hash, KObject* params) {
	string url;
	url.append(hash);

	for (KObject::KObjAttributes::iterator it = params->begin(); it != params->end(); it++) {
		string key = (*it)->key;
		void* val = (*it)->value->pointed();

		string escaped_key = kinky::utils::KUrlEncodeRfc3986::encode(key);
		string escaped_value = kinky::utils::KUrlEncodeRfc3986::encode(static_cast<string*>(val)->data());

		if (url.find("?") != string::npos) {
			url.append("&");
		}
		else {
			url.append("?");
		}

		url.append(escaped_key);
		url.append("=");
		url.append(escaped_value);
	}


	KObject* profile_data = KRest::exec(KHTTPRequest::GET, "https://api.twitter.com/1", url, NULL, NULL);


	return profile_data;
}

pair<const char*, const char*>* kinky::social::twitter::getOAuthToken(KObject* response, const char* consumerKey, const char* consumerSecret, const char* redirectURI, bool force_login) {
	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	KObject* params = KOBJECT(
			"oauth_callback" << (string(redirectURI) + (string(redirectURI).find('?') != string::npos ? '&' : '?') + "key=" + *nonce).data() <<
			"oauth_consumer_key" << consumerKey <<
			"oauth_nonce" << nonce->data() <<
			"oauth_signature_method" << "HMAC-SHA1" <<
			"oauth_timestamp" << timestamp->data() <<
			"oauth_version" << "1.0");

	// TODO Por vezes crasha na primeira utilizacao, porque sera?
	KObject* oauth_data = kinky::social::oauth1::post("https://api.twitter.com/oauth/request_token", params, consumerSecret, "", NULL);

	delete timestamp;
	delete params;

	pair<const char*, const char*>* ret = NULL;
	if ((*oauth_data)["oauth_token"] != NULL_KOBJECT) {
		response->addHeader("Location", ("http://api.twitter.com/oauth/authenticate?"
				"oauth_token=" + *(oauth_data->getString("oauth_token")) + (force_login ? "&force_login=true" : "")).data());

		response->setStatus(KHTTPResponse::TEMPORARY_REDIRECT);
		ret = new pair<const char*, const char*>(nonce->data(), ((string) ((*oauth_data)["oauth_token_secret"])).data());
	}

	delete oauth_data;
	delete nonce;

	return ret;
}

kinky::lang::KObject* kinky::social::twitter::getAccessToken(const char* consumerKey, const char* consumerSecret, const char* oauth_token, const char* oauth_verifier, const char* oauth_token_secret) {
	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	KObject* params = KOBJECT(
			"oauth_consumer_key" << consumerKey <<
			"oauth_nonce" << nonce->data() <<
			"oauth_signature_method" << "HMAC-SHA1" <<
			"oauth_timestamp" << timestamp->data() <<
			"oauth_token" << oauth_token <<
			"oauth_verifier" << oauth_verifier <<
			"oauth_version" << "1.0");

	KObject* oauth_data = kinky::social::oauth1::post("https://api.twitter.com/oauth/access_token", params, consumerSecret, oauth_token_secret, NULL);

	delete nonce;
	delete timestamp;
	delete params;

	return oauth_data;
}

