#pragma once

#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/social/OAuth2.h>
#include <kinky/social/Common.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest::http;

	namespace social {

		namespace youtube {

			size_t curlWriter(char *data, size_t size, size_t nmemb, ofstream *buffer);

			size_t curlTextWriter(char *data, size_t size, size_t nmemb, string *buffer);

			size_t curlReader(void *ptr, size_t size, size_t nmemb, void *userdata);

			bool retrieveServerVideo(string* localfilelocation, string videourl, string* protocol, string* server, string* serverpath, string* user, string* pass, bool deleteafter);

			void deleteServerVideo(string* protocol, string* user, string* pass, string* server, string* serverpath, string videofilename);

			KObject* directUploadVideo(string localfile, string videofilename, string* playlisttoadd, string* requestsource, string* videotitle, string videocategory, string videokeywords, string authotizationtoken, string developerkey);

			KObject* retrieveUserPlaylists(string authorizationtoken, string developerkey);

			KObject* requestApplicationAccess(string clientID, string redirectURI, string scope, string accessType);

			KObject* getAccessToken(string code, string clientID, string clientSecret, string redirectURI, string grantType);

			KObject* refreshAccessToken(string clientID, string clientSecret, string refreshToken, string grantType);

			KObject* retrievePlaylistFeed(string playlistID, string format);

			KObject* searchPlaylistFeed(string playlistID, KObject* searchfields);

			KObject* oEmbed(string youtubeUrl, string format);


		}
	}
}
