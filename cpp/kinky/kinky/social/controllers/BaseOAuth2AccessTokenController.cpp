#include <kinky/social/controllers/BaseOAuth2AccessTokenController.h>

#include <kinky.h>

using namespace std;
using namespace __gnu_cxx;

using namespace kinky::oauth;
using namespace kinky::lang;
using namespace kinky::utils;
using namespace kinky::rest;
using namespace kinky::rest::http;

namespace kinky {

	namespace social {

		namespace controllers {

			BaseOAuth2AccessTokenController::BaseOAuth2AccessTokenController(string urlParam) :
				KRestOperation(urlParam, CONTROLLER) {
			}

			BaseOAuth2AccessTokenController::BaseOAuth2AccessTokenController(string urlParam, string linkDescriptionParam) :
				KRestOperation(urlParam, linkDescriptionParam, CONTROLLER) {
			}

			string BaseOAuth2AccessTokenController::getCode(KObject* request, string* url) {
				return request->getString("code") != NULL ? request->get("code") : "";
			}

			string BaseOAuth2AccessTokenController::getRedirectURI(KObject* request, string* url) {
				return request->getString("redirect_uri") != NULL ? request->get("redirect_uri") : (request->getString("state") != NULL ? request->get("state") : "");
			}

			bool BaseOAuth2AccessTokenController::getForceLogin(KObject* request, string* url) {
				return request->getString("force_login") != NULL && (request->get("force_login") == "true" || request->get("force_login") == "1");
			}

			void BaseOAuth2AccessTokenController::fillOtherAuthorizationRequestParams(ostringstream& oss, KObject* request, string* url) {
			}

			void BaseOAuth2AccessTokenController::fillOtherAccessTokenRequestParams(KObject* params, KObject* request, string* url) {
			}

			KObject* BaseOAuth2AccessTokenController::requestCode(KObject* request, string* url) {
				ostringstream oss;

				KObject* to_return = new KObject();
				string myurl(this->getConfiguration()->get("krest_server_external_url"));
				myurl.insert(myurl.length(), *url);

				string scope(this->getScope(request, url));
				oss.str("");
				oss << this->getAuthorizationEndpoint();
				oss << "?response_type=code";
				oss << "&client_id=" << this->getClientID(request, url);
				if (scope.length() != 0) {
					oss << "&scope=" << scope;
				}
				oss << "&redirect_uri=" << myurl;
				oss << "&state=" << this->getRedirectURI(request, url) << "/" << (!!(*request)["client_id"] ? (string) (*request)["client_id"] : this->getConfiguration()->get("led_rest_api_default_client_id"));

				this->fillOtherAuthorizationRequestParams(oss, request, url);

				oss << flush;

				to_return->addHeader("Location", oss.str());

				to_return->setStatus(KHTTPResponse::TEMPORARY_REDIRECT);

				return to_return;
			}

			KObject* BaseOAuth2AccessTokenController::requestAccessToken(string code, KObject* request, string* url) {
				string myurl(this->getConfiguration()->get("krest_server_external_url"));
				myurl.insert(myurl.length(), *url);

				KObject* params = KOBJECT(
					"grant_type" << "authorization_code" <<
					"client_id" << this->getClientID(request, url) <<
					"client_secret" << this->getClientSecret(request, url) <<
					"redirect_uri" << myurl <<
					"code" << code);

				this->fillOtherAccessTokenRequestParams(params, request, url);

				KObject* access_token_data = kinky::social::oauth2::post(this->getAccessTokenEndpoint().data(), params);

				delete params;

				return access_token_data;
			}

			KObject* BaseOAuth2AccessTokenController::processAccountData(KObject* access_token, KObject* request, string* url, KToken* token) {
				return new KObject();
			}

			KObject* BaseOAuth2AccessTokenController::process(KObject* request, int directive, string* url, KToken* token) {
				KRestException* to_throw = NULL;

				try {
					switch (directive) {
						case KHTTPRequest::GET:
						case KHTTPRequest::POST: {
							if (request->getString("error") != NULL && *request->getString("error") == "access_denied") {
								to_throw = new KRestException(KHTTPResponse::UNAUTHORIZED, KEXCEPTION_OAUTH2_ACCESS_DENIED);
							}
							else {
								string code = this->getCode(request, url);

								if (code.length() == 0) {
									return this->requestCode(request, url);
								}
								else {
									if (!!(*request)["state"]) {
										string state((string) (*request)["state"]);
										(*request) << "state" << state.substr(0, state.rfind("/"));
										(*request) << "client_id" << state.substr(state.rfind("/") + 1);
									}

									/* Get access_token */
									KObject* access_token = this->requestAccessToken(code, request, url);
									kinky::memory::managed_ptr<KObject>* ptr = new kinky::memory::managed_ptr<KObject>(access_token);

									KObject* ret = this->processAccountData(access_token, request, url, token);
									ptr->unlink();
									delete access_token;

									return ret;
								}
							}

							break;
						}

						default: {
							to_throw = new KRestException(KHTTPResponse::SEE_OTHER, this->getRedirectURI(request, url), 904);
							break;
						}
					}
				}
				catch (KRestException* kre) {
					to_throw = new KRestException(KHTTPResponse::SEE_OTHER, this->getRedirectURI(request, url), kre->getErrorCode());
					delete kre;
				}
				catch (std::exception& e) {
					to_throw = new KRestException(KHTTPResponse::SEE_OTHER, this->getRedirectURI(request, url), 800);
				}

				if (to_throw == NULL) {
					to_throw = new KRestException(KHTTPResponse::SEE_OTHER, this->getRedirectURI(request, url), 800);
				}

				throw to_throw;
			}
		}
	}
}
