#include <kinky/social/controllers/BaseOAuth1AccessTokenController.h>

#include <kinky.h>

using namespace std;
using namespace __gnu_cxx;

using namespace kinky::oauth;
using namespace kinky::lang;
using namespace kinky::utils;
using namespace kinky::rest;
using namespace kinky::rest::http;

namespace kinky {
	namespace social {
		namespace controllers {
			BaseOAuth1AccessTokenController::BaseOAuth1AccessTokenController(string urlParam) :
				KRestOperation(urlParam, CONTROLLER) {
			}

			BaseOAuth1AccessTokenController::BaseOAuth1AccessTokenController(string urlParam, string linkDescriptionParam) :
				KRestOperation(urlParam, linkDescriptionParam, CONTROLLER) {
			}

			string BaseOAuth1AccessTokenController::getKey(KObject* request, string* url) {
				return request->getString("key") != NULL ? request->get("key") : "";
			}

			string BaseOAuth1AccessTokenController::getOauthToken(KObject* request, string* url) {
				return request->getString("oauth_token") != NULL ? request->get("oauth_token") : "";
			}

			string BaseOAuth1AccessTokenController::getOauthVerifier(KObject* request, string* url) {
				return request->getString("oauth_verifier") != NULL ? request->get("oauth_verifier") : "";
			}

			string BaseOAuth1AccessTokenController::getRedirectURI(KObject* request, string* url) {
				return request->getString("redirect_uri") != NULL ? request->get("redirect_uri") : "";
			}

			bool BaseOAuth1AccessTokenController::getForceLogin(KObject* request, string* url) {
				return request->getString("force_login") != NULL && (request->get("force_login") == "true" || request->get("force_login") == "1");
			}

			KObject* BaseOAuth1AccessTokenController::requestOAuthToken(KObject* request, string* url) {
				KObject* to_return = new KObject();

				string* nonce = new string();
				string* timestamp = new string();

				kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

				string redir = this->getRedirectURI(request, url);

				ostringstream oss;
				oss << redir;
				oss << (redir.find('?') == string::npos ? '?' : '&') << "key=" << *nonce << "&state=" << *url << "/" << (!!(*request)["client_id"] ? (string) (*request)["client_id"] : this->getConfiguration()->get("led_rest_api_default_client_id"));
				oss << flush;

				KObject* params = KOBJECT(
					"oauth_callback" << oss.str() <<
					"oauth_consumer_key" << this->getConsumerKey(request, url) <<
					"oauth_nonce" << nonce->data() <<
					"oauth_signature_method" << "HMAC-SHA1" <<
					"oauth_timestamp" << timestamp->data() <<
					"oauth_version" << "1.0");

				// TODO Por vezes crasha na primeira utilizacao, porque sera?
				KObject* oauth_data = kinky::social::oauth1::post(this->getRequestTokenEndpoint().data(), params, this->getConsumerSecret(request, url).data(), "", NULL);

				delete timestamp;
				delete params;

				if ((*oauth_data)["oauth_token"] != NULL_KOBJECT) {
					ostringstream oss;
					oss << this->getAuthenticateEndpoint();
					oss << "?oauth_token=" << oauth_data->get("oauth_token");

					if (this->getForceLogin(request, url)) {
						oss << "&force_login=true";
					}

					oss << flush;

					to_return->addHeader("Location", oss.str());

					to_return->setStatus(KHTTPResponse::TEMPORARY_REDIRECT);

					this->saveSessionData(*nonce, oauth_data->get("oauth_token_secret"));
				}

				delete oauth_data;
				delete nonce;

				to_return->addHeader("Cache-Control", "no-store");
				to_return->addHeader("Pragma", "no-cache");
				return to_return;
			}

			KObject* BaseOAuth1AccessTokenController::requestAccessToken(string oauth_token, string oauth_verifier, string oauth_token_secret, KObject* request, string* url) {
				string* nonce = new string();
				string* timestamp = new string();

				kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

				KObject* params = KOBJECT(
					"oauth_consumer_key" << this->getConsumerKey(request, url) <<
					"oauth_nonce" << nonce->data() <<
					"oauth_signature_method" << "HMAC-SHA1" <<
					"oauth_timestamp" << timestamp->data() <<
					"oauth_token" << oauth_token <<
					"oauth_verifier" << oauth_verifier <<
					"oauth_version" << "1.0");

				KObject* oauth_data = kinky::social::oauth1::post(this->getRequestAccessTokenEndpoint().data(), params, this->getConsumerSecret(request, url).data(), oauth_token_secret.data(), NULL);

				delete nonce;
				delete timestamp;
				delete params;

				oauth_data->addHeader("Cache-Control", "no-store");
				oauth_data->addHeader("Pragma", "no-cache");
				return oauth_data;
			}

			KObject* BaseOAuth1AccessTokenController::processAccountData(KObject* access_token, KObject* request, string* url, KToken* token) {
				return new KObject();
			}

			KObject* BaseOAuth1AccessTokenController::process(KObject* request, int directive, string* url, KToken* token) {
				KRestException* to_throw = NULL;

				try {
					switch (directive) {
						case KHTTPRequest::GET:
						case KHTTPRequest::POST: {
							if (request->getString("denied") != NULL) {
								to_throw = new KRestException(KHTTPResponse::UNAUTHORIZED, KEXCEPTION_OAUTH1_ACCESS_DENIED);
							}
							else {
								string key = this->getKey(request, url);

								if (key == "") {
									/* Get oauth_data */
									return this->requestOAuthToken(request, url);
								}
								else {
									string oauth_token = this->getOauthToken(request, url);
									string oauth_verifier = this->getOauthVerifier(request, url);

									string oauth_token_secret = this->recoverSessionData(key);
									size_t pipe = 0;
									if ((pipe = oauth_token_secret.find("|")) != string::npos) {
										string other(oauth_token_secret.substr(pipe + 1));
										oauth_token_secret.assign(oauth_token_secret.substr(0, pipe));

										stringstream ss(other);
										string item;
										size_t separator = 0;
										while (std::getline(ss, item, '&')) {
											separator = item.find("=");
											if (separator != string::npos) {
												ostringstream oss;
												kinky::utils::url_decode(item.substr(0, separator), oss);
												string name(oss.str());
												oss.str("");
												kinky::utils::url_decode(item.substr(separator + 1), oss);
												string value(oss.str());

												(*request) << name << value;
											}
										}
									}

									if (oauth_token_secret.length() == 0 || oauth_token == "" || oauth_verifier == "") {
										to_throw = new KRestException(KHTTPResponse::SEE_OTHER, this->getRedirectURI(request, url), 925);
									}
									else {
										if (!!(*request)["state"]) {
											string state((string) (*request)["state"]);
											(*request) << "state" << state.substr(0, state.rfind("/"));
											(*request) << "client_id" << state.substr(state.rfind("/") + 1);
										}

										/* Get oauth_data */
										KObject* access_token = this->requestAccessToken(oauth_token, oauth_verifier, oauth_token_secret, request, url);
										kinky::memory::managed_ptr<KObject>* ptr = new kinky::memory::managed_ptr<KObject>(access_token);

										KObject* ret = this->processAccountData(access_token, request, url, token);

										ptr->unlink();
										delete access_token;

										ret->addHeader("Cache-Control", "no-store");
										ret->addHeader("Pragma", "no-cache");
										return ret;
									}
								}
							}

							break;
						}

						default: {
							to_throw = new KRestException(KHTTPResponse::SEE_OTHER, this->getRedirectURI(request, url), 904);
							break;
						}
					}
				}
				catch (KRestException* kre) {
					to_throw = new KRestException(KHTTPResponse::SEE_OTHER, this->getRedirectURI(request, url), kre->getErrorCode());
					delete kre;
				}
				catch (std::exception& e) {
					to_throw = new KRestException(KHTTPResponse::SEE_OTHER, this->getRedirectURI(request, url), 800);
				}

				if (to_throw == NULL) {
					to_throw = new KRestException(KHTTPResponse::SEE_OTHER, this->getRedirectURI(request, url), 800);
				}

				throw to_throw;
			}
		}
	}
}
