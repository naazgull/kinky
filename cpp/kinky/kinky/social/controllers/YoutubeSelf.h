#pragma once

#include <kinky.h>
#include <stdlib.h>


using namespace std;
using namespace __gnu_cxx;

using namespace kinky::oauth;
using namespace kinky::lang;
using namespace kinky::utils;
using namespace kinky::rest;
using namespace kinky::rest::http;

namespace kinky {

	namespace social {

		namespace controllers {
			class YoutubeSelf: public KRestOperation {
				public:
					YoutubeSelf();
					virtual ~YoutubeSelf();

					KObject* process(KObject* request, int directive, string* url, KToken* token);

			};
		}
	}
}
