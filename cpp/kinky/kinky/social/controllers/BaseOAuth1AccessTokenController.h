#pragma once

#include <string>

#include <kinky/lang/KObject.h>
#include <kinky/oauth/KToken.h>
#include <kinky/rest/KRestOperation.h>

using namespace std;
using namespace kinky::lang;
using namespace kinky::rest;
using namespace kinky::oauth;

namespace kinky {
	namespace social {
		namespace controllers {
			class BaseOAuth1AccessTokenController: public KRestOperation {
				public:
					BaseOAuth1AccessTokenController(string urlParam);
					BaseOAuth1AccessTokenController(string urlParam, string linkDescriptionParam);

					virtual ~BaseOAuth1AccessTokenController() {
					}


					/* endpoints */

					virtual string getRequestTokenEndpoint() = 0;
					virtual string getAuthenticateEndpoint() = 0;
					virtual string getRequestAccessTokenEndpoint() = 0;


					/* application credentials */

					virtual string getConsumerKey(KObject* request, string* url) = 0;
					virtual string getConsumerSecret(KObject* request, string* url) = 0;


					/* access parameters */

					/*
					 * The URL to redirect to after the user clicks a button in the third party dialog.
					 *
					 * This implementation uses request parameter "redirect_uri".
					 * Can be overrided.
					 */
					virtual string getRedirectURI(KObject* request, string* url);

					/*
					 * Indicates if the user should be re-prompted for login and consent.
					 *
					 * This implementation uses request parameter "force_login" and defaults do "false".
					 * Can be overrided.
					 */
					virtual bool getForceLogin(KObject* request, string* url);


					/* return data */

					virtual string getOauthToken(KObject* request, string* url);
					virtual string getOauthVerifier(KObject* request, string* url);


					/* session data */

					virtual string getKey(KObject* request, string* url);
					virtual string recoverSessionData(string key) = 0;
					virtual void saveSessionData(string key, string value) = 0;


					/* interaction */

					virtual KObject* requestOAuthToken(KObject* request, string* url);
					virtual KObject* requestAccessToken(string oauth_token, string oauth_verifier, string oauth_token_secret, KObject* request, string* url);


					/* do stuff */

					virtual KObject* processAccountData(KObject* access_token, KObject* request, string* url, KToken* token);

					KObject* process(KObject* request, int directive, string* url, KToken* token);
			};
		}
	}
}
