#pragma once

#include <string>

#include <kinky/lang/KObject.h>
#include <kinky/oauth/KToken.h>
#include <kinky/rest/KRestOperation.h>

using namespace std;
using namespace kinky::lang;
using namespace kinky::rest;
using namespace kinky::oauth;

namespace kinky {
	namespace social {
		namespace controllers {
			class BaseOAuth2AccessTokenController: public KRestOperation {
				public:
					BaseOAuth2AccessTokenController(string urlParam);
					BaseOAuth2AccessTokenController(string urlParam, string linkDescriptionParam);

					virtual ~BaseOAuth2AccessTokenController() {
					}


					/* endpoints */

					virtual string getAuthorizationEndpoint() = 0;
					virtual string getAccessTokenEndpoint() = 0;


					/* application credentials */

					virtual string getClientID(KObject* request, string* url) = 0;
					virtual string getClientSecret(KObject* request, string* url) = 0;


					/* access parameters */

					/*
					 * The URL to redirect to after the user clicks a button in the third party dialog.
					 *
					 * This implementation uses request parameter "redirect_uri".
					 * Can be overrided.
					 */
					virtual string getRedirectURI(KObject* request, string* url);

					/*
					 * A list of permission which the user will be asked to grant to the application.
					 *
					 * Must be overrided.
					 */
					virtual string getScope(KObject* request, string* url) = 0;

					/*
					 * Indicates if the user should be re-prompted for login and consent.
					 *
					 * This implementation uses request parameter "force_login" and defaults do "false".
					 * Can be overrided.
					 */
					virtual bool getForceLogin(KObject* request, string* url);

					/*
					 * Creates other request parameters specific of the third party.
					 *
					 * Must be overrided.
					 */
					virtual void fillOtherAuthorizationRequestParams(ostringstream& oss, KObject* request, string* url);
					virtual void fillOtherAccessTokenRequestParams(KObject* params, KObject* request, string* url);


					/* return data */

					/*
					 * The authorization code returned from the initial request.
					 */
					virtual string getCode(KObject* request, string* url);

					/* interaction */

					virtual KObject* requestCode(KObject* request, string* url);
					virtual KObject* requestAccessToken(string code, KObject* request, string* url);


					/* do stuff */

					virtual KObject* processAccountData(KObject* access_token, KObject* request, string* url, KToken* token);

					virtual KObject* process(KObject* request, int directive, string* url, KToken* token);
			};
		}
	}
}
