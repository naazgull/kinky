#include <kinky/social/controllers/InstagramSelf.h>

kinky::social::controllers::InstagramSelf::InstagramSelf() :
		KRestOperation("^/instagram(/(.+))*$") {

	this->setPermissions(KOBJECT(
		"GET" << KOBJECT_ARRAY(
			"admin" <<
			"view_social"
		) <<
		"PUT" << KOBJECT_ARRAY(
			"admin" <<
			"manage_social"
		) <<
		"DELETE" << KOBJECT_ARRAY(
			"admin" <<
			"manage_social"
		) <<
		"POST" << KOBJECT_ARRAY(
			"admin" <<
			"manage_social"
		)
	));
}

kinky::social::controllers::InstagramSelf::~InstagramSelf() {
}

kinky::lang::KObject* kinky::social::controllers::InstagramSelf::process(KObject* request, int directive, string* url, KToken* token) {
	if (!this->hasPermission(*url, directive, token)) {
		throw new KRestException(KHTTPResponse::UNAUTHORIZED, KEXCEPTION_NOT_ENOUGH_SCOPES);
	}

	KObject* ret = NULL;
	KRestException* thr = NULL;

	size_t pos = url->find('/', 1);
	string fbURL;
	if (pos == string::npos) {
		thr = new KRestException(KHTTPResponse::PRECONDITION_FAILED, KEXCEPTION_NOT_ENOUGH_PARAMETERS, KOBJECT(
			"required_url_part" << KOBJECT_ARRAY(
				"instagram paht, e.g. '/instagram/users/self"
			)
		));
	}
	else {
		fbURL.assign(url->substr(pos));
		fbURL.insert(0, "https://api.instagram.com/v1");
	}

	if (fbURL.length() != 0) {

		if ((*request)["instagram_token"] != NULL_KOBJECT) {
			(*request) << "access_token" << (string) (*request)["instagram_token"];
			request->remove("instagram_token");
		}
		else {
			string uid;
			kinky::scopes::KScopeController::extractMe(token, &uid);
			string uri(uid);
			if (uid.find("/accounts") != (size_t) 0) {
				uri.insert(uri.length(), "/accounts");
			}
			uri.insert(uri.length(), "?type=instagram");
			uid.insert(0, "default:");
			KToken admtoken(*token->getClientID(), "", uid, "admin", time(NULL) + 3600 * 24 * 60);
			KObject* accounts = KRest::exec(KHTTPRequest::GET, this->getConfiguration()->get("krest_server_url"), uri, admtoken, NULL);

			if (accounts != NULL && accounts->getStatus() == KHTTPResponse::OK) {
				if ((*accounts)["elements"] != NULL_KOBJECT) {
					(*request) << "access_token" << (string) (*accounts)["elements"][0]["authdata"]["access_token"];
				}
				else {
					(*request) << "access_token" << (string) (*accounts)["authdata"]["access_token"];
				}
			}

			if (accounts != NULL) {
				delete accounts;
			}
		}

		switch (directive) {
			case KHTTPRequest::GET: {
				try {
					KObject* send = request->clone();
					ret = kinky::social::oauth2::get(fbURL.data(), send);
					delete send;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}
			case KHTTPRequest::POST: {
				try {
					KObject* send = request->clone();
					ret = kinky::social::oauth2::post(fbURL.data(), send);
					delete send;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}

			default: {
				thr = new KRestException(KHTTPResponse::METHOD_NOT_ALLOWED, KEXCEPTION_INVALID_METHOD);
			}
		}
	}

	if (thr != NULL) {
		throw thr;
	}

	return ret;
}
