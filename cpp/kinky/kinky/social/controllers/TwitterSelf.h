#pragma once

#include <kinky.h>
#include <stdlib.h>


using namespace std;
using namespace __gnu_cxx;

using namespace kinky::oauth;
using namespace kinky::lang;
using namespace kinky::utils;
using namespace kinky::rest;
using namespace kinky::rest::http;

namespace kinky {

	namespace social {

		namespace controllers {
			class TwitterSelf: public KRestOperation {
				public:
					TwitterSelf();
					virtual ~TwitterSelf();

					KObject* process(KObject* request, int directive, string* url, KToken* token);

			};
		}
	}
}
