#include <kinky/social/controllers/TumblrSelf.h>

kinky::social::controllers::TumblrSelf::TumblrSelf() :
		KRestOperation("^/tumblr(/(.+))*$") {

	this->setPermissions(KOBJECT(
		"GET" << KOBJECT_ARRAY(
			"admin" <<
			"view_social"
		) <<
		"PUT" << KOBJECT_ARRAY(
			"admin" <<
			"manage_social"
		) <<
		"DELETE" << KOBJECT_ARRAY(
			"admin" <<
			"manage_social"
		) <<
		"POST" << KOBJECT_ARRAY(
			"admin" <<
			"manage_social"
		)
	));
}

kinky::social::controllers::TumblrSelf::~TumblrSelf() {
}

kinky::lang::KObject* kinky::social::controllers::TumblrSelf::process(KObject* request, int directive, string* url, KToken* token) {
	if (!this->hasPermission(*url, directive, token)) {
		throw new KRestException(KHTTPResponse::UNAUTHORIZED, KEXCEPTION_NOT_ENOUGH_SCOPES);
	}

	KObject* ret = NULL;
	KRestException* thr = NULL;

	size_t pos = url->find('/', 1);
	string fbURL;
	if (pos == string::npos) {
		thr = new KRestException(KHTTPResponse::PRECONDITION_FAILED, KEXCEPTION_NOT_ENOUGH_PARAMETERS, KOBJECT(
			"required_url_part" << KOBJECT_ARRAY(
				"tumblr path, e.g. '/tumblr/user/info"
			)
		));
	}
	else {
		fbURL.assign(url->substr(pos));
	}

	if (fbURL.length() != 0) {

		string access_token;
		string oauth_token_secret;

		if ((*request)["oauth_token"] != NULL_KOBJECT && (*request)["oauth_token_secret"] != NULL_KOBJECT) {
			access_token.assign((string) (*request)["oauth_token"]);
			oauth_token_secret.assign((string) (*request)["oauth_token_secret"]);
			request->remove("oauth_token");
			request->remove("oauth_token_secret");
		}
		else {
			string uid;
			kinky::scopes::KScopeController::extractMe(token, &uid);
			string uri(uid);
			if (uid.find("/accounts") != (size_t) 0) {
				uri.insert(uri.length(), "/accounts");
			}
			uri.insert(uri.length(), "?type=tumblr");
			uid.insert(0, "default:");
			KToken admtoken(*token->getClientID(), "", uid, "admin", time(NULL) + 3600 * 24 * 60);
			KObject* accounts = KRest::exec(KHTTPRequest::GET, this->getConfiguration()->get("krest_server_url"), uri, admtoken, NULL);

			if (accounts != NULL && accounts->getStatus() == KHTTPResponse::OK && (*accounts)["elements"] != NULL_KOBJECT) {
				access_token.assign((string) (*accounts)["elements"][0]["authdata"]["oauth_token"]);
				oauth_token_secret.assign((string) (*accounts)["elements"][0]["authdata"]["oauth_token_secret"]);
			}

			if (accounts != NULL) {
				delete accounts;
			}
		}

		switch (directive) {
			case KHTTPRequest::GET: {
				try {
					KObject* send = request->clone();
					ret = kinky::social::tumblr::get(fbURL.data(), this->getConfiguration()->get("led_oauththirdparty_tumblr_consumer_key").data(), this->getConfiguration()->get("led_oauththirdparty_tumblr_consumer_secret").data(), access_token.data(), oauth_token_secret.data(), send);
					delete send;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}

			case KHTTPRequest::POST: {
				try {
					KObject* params = new KObject();
					KObject* send = request->clone();
					ret = kinky::social::tumblr::post(fbURL.data(), this->getConfiguration()->get("led_oauththirdparty_tumblr_consumer_key").data(), this->getConfiguration()->get("led_oauththirdparty_tumblr_consumer_secret").data(), access_token.data(), oauth_token_secret.data(), params, send);
					delete send;
					delete params;
				}
				catch (kinky::social::KSocialTokenExpiredException* e) {
					thr = new KRestException(KHTTPResponse::FORBIDDEN, e->what() + string(": ") + KEXCEPTION_INVALID_FB_TOKEN);
					delete e;
				}
				break;
			}

			default: {
				thr = new KRestException(KHTTPResponse::METHOD_NOT_ALLOWED, KEXCEPTION_INVALID_METHOD);
			}
		}

	}

	if (thr != NULL) {
		throw thr;
	}

	return ret;
}
