#include <kinky/social/Flickr.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

kinky::lang::KObject* kinky::social::flickr::_get(const char* hash, KObject* params, const char* consumerSecret, const char* oauthToken) {
	string url = hash;

	string base_signature = "GET&";
	base_signature.append(kinky::utils::KUrlEncodeRfc3986::encode(url));
	base_signature.append("&");

	string fields = "";
	string signature_fields = "";

	bool first = true;
	bool first_fields = true;

	for (KObject::KObjAttributes::iterator it = params->begin(); it != params->end(); it++) {
		string key = (*it)->key;
		void* val = (*it)->value->pointed();

		string escaped_key = kinky::utils::KUrlEncodeRfc3986::encode(key);
		string escaped_value = kinky::utils::KUrlEncodeRfc3986::encode(static_cast<string*>(val)->data());

		if (!first) {
			signature_fields.append("&");
		}

		if (!first_fields) {
			fields.append("&");
		}
		first_fields = false;

		fields.append(escaped_key);
		fields.append("=");
		fields.append(escaped_value);

		first = false;

		signature_fields.append(escaped_key);
		signature_fields.append("=");
		signature_fields.append(escaped_value);
	}

	base_signature.append(kinky::utils::KUrlEncodeRfc3986::encode(signature_fields));

	string secretSigningKey;
	secretSigningKey.append(consumerSecret);
	secretSigningKey.append("&");
	secretSigningKey.append(oauthToken);

	CHMAC_SHA1 objHMACSHA1;
	unsigned char hashed_signature[1024];

	memset(hashed_signature, 0, 1024);

	objHMACSHA1.HMAC_SHA1((unsigned char*) base_signature.c_str(), base_signature.length(), (unsigned char*) secretSigningKey.c_str(), secretSigningKey.length(), hashed_signature);

	istringstream in((const char*) hashed_signature);
	ostringstream out;
	kinky::utils::base64_encode(in, out);
	string encoded_hashed_signature = out.str();

	string escaped_encoded_hashed_signature = kinky::utils::KUrlEncodeRfc3986::encode(encoded_hashed_signature.data());

	fields.append("&oauth_signature=");
	fields.append(escaped_encoded_hashed_signature);

	struct curl_slist* headers = NULL;
	headers = curl_slist_append(headers, "Expect:");

	string buffer;
	char errorBuffer[CURL_ERROR_SIZE];

	CURL* curl = curl_easy_init();

	url.insert(url.length(), "?");
	url.insert(url.length(), fields);
	OpenSSL_add_all_algorithms();
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_URL, url.data());
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, social::common::curlWriter);

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);

	CURLcode result = curl_easy_perform(curl);

	if (result == CURLE_OK) {
		KObject* toReturn = kinky::social::common::parseResponse(curl, &buffer, params);

		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		return toReturn;
	}
	else {
		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		throw new KRestException(result, errorBuffer);
	}
}

kinky::lang::KObject* kinky::social::flickr::get(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params) {
	string* url = new string("http://api.flickr.com/services/rest");

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
		"nojsoncallback" << "1"<<
		"format" << "json" <<
		"method" << hash <<
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0";

	params->sort();

	KObject* profile_data = kinky::social::flickr::_get(url->data(), params, consumerSecret, tokenSecret);

	delete nonce;
	delete timestamp;
	delete url;

	return profile_data;
}


kinky::lang::KObject* kinky::social::flickr::post(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params, KObject* body) {
	string* url = new string("http://social.flickrapis.com/v1");
	url->append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
		"format" << "json" <<
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0";

	params->sort();

	ostringstream oss;
	oss << *body << flush;
	string strbody(oss.str());

	KObject* profile_data = kinky::social::oauth1::post(url->data(), params, consumerSecret, tokenSecret, &strbody);

	delete nonce;
	delete timestamp;
	delete url;

	return profile_data;
}
