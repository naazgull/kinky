#pragma once

#include <curl/curl.h>
#include <kinky/lang/KJSON.h>
#include <kinky/lang/KXML.h>
#include <kinky/lang/KObject.h>
#include <kinky/social/Common.h>
#include <kinky/lang/KParseException.h>
#include <kinky/rest/KRestException.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest;

	namespace social {

		namespace openid {

			KObject* parseResponse(CURL* curl, string* buffer, KObject* params);

			KObject* post(const char* hash, KObject* params);
		}
	}
}
