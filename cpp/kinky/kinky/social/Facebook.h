#pragma once

#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/social/OAuth2.h>
#include <kinky/social/Common.h>
#include <kinky/social/KSocialTokenExpiredException.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest::http;

	namespace social {
		/* Deprecated: use kinky::social::oauth2 instead */
		namespace facebook {

			/* Deprecated: use kinky::social::common::curlWriter instead */
			int curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer);

			KObject* get(string hash, KObject* params);

			KObject* post(string hash, KObject* params, bool postAccessToken = false);

			string getAuthorizeURI(string client_id, string redirect_uri, string scope);

			void getCode(KObject* response, string clientID, string redirectURI, string scope, bool force_login);

			KObject* getAccessToken(string clientID, string clientSecret, string redirectURI, string fbCode);

			KObject* getLongLivedAccessToken(string clientID, string clientSecret, string currentToken);

			KObject* unserializeSignedRequest(string signedRequest);

			void updateCache(string oguri);

		}
	}
}
