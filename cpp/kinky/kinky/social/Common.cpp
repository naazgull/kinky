#include <kinky/social/Common.h>

int kinky::social::common::curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer) {
	int result = 0;

	if (buffer != NULL) {
		buffer->append(data, size * nmemb);
		result = size * nmemb;
	}

	return result;
}

size_t kinky::social::common::curlReader(void *ptr, size_t size, size_t nmemb, void *userdata) {
	if (userdata != NULL) {
		string* buffer = static_cast<string*>(userdata);

		if (buffer != NULL && buffer->length() != 0) {
			size_t result = buffer->length() > size * nmemb ? size * nmemb : buffer->length();
			memcpy(ptr, buffer->c_str(), result);

			buffer->erase(0, result);

			return result;
		}
	}
	return 0;
}

kinky::lang::KObject* kinky::social::common::parseResponse(CURL* curl, string* buffer, KObject* params) {
	long contentLength = -1;
	curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &contentLength);

	char* contentTypeChar;
	curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &contentTypeChar);

	KObject* toReturn = NULL;

	if (contentTypeChar != NULL && contentLength != -1) {
		string contentType = contentTypeChar;
		// Tambem aceita o contentType vazio porque era o comportamento por defeito
		if (contentType == "" || contentType.find("json") != string::npos || contentType.find("javascript") != string::npos) {
			try {
				toReturn = KJSON::parse(buffer);
			}
			catch (KParseException* e) {
				delete e;
			}
		}

		if (toReturn == NULL && contentType.find("/xml") != string::npos) {
			try {
				toReturn = KXML::parse(buffer);

			}
			catch (KParseException* e) {
				delete e;
			}
		}

		if (toReturn == NULL) {
			stringstream ss(*buffer);

			string item;

			toReturn = new KObject();

			while (std::getline(ss, item, '&')) {
				size_t separator = item.find("=");

				if (separator != string::npos) {
					string name = item.substr(0, separator);
					string* value = new string(curl_unescape(item.substr(separator + 1).data(), 0));

					toReturn->setString(name.data(), value->data());

					delete value;
				}
			}
		}

		if (toReturn == NULL)
		toReturn = new KObject("json");
	}
	else {
		toReturn = new KObject(params->base());
	}

	long httpCode = 0;
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
	toReturn->setStatus(httpCode);

	return toReturn;
}
