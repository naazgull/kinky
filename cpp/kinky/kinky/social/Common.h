#pragma once

#include <curl/curl.h>
#include <kinky/lang/KJSON.h>
#include <kinky/lang/KXML.h>
#include <kinky/lang/KObject.h>
#include <kinky/lang/KParseException.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;

	namespace social {

		namespace common {

			int curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer);

			size_t curlReader(void *ptr, size_t size, size_t nmemb, void *userdata);

			KObject* parseResponse(CURL* curl, string* buffer, KObject* params);
		}
	}
}
