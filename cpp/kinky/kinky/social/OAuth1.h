#pragma once

#include <curl/curl.h>
#include <kinky/rest/KRestException.h>
#include <kinky/social/Common.h>
#include <kinky/utils/HMAC_SHA1.h>
#include <kinky/utils/KUrlEncodeRfc3986.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest;

	namespace social {

		namespace oauth1 {
			KObject* get(const char* hash, KObject* params, const char* consumerSecret, const char* oauthToken);

			KObject* post(const char* hash, KObject* params, const char* consumerSecret, const char* oauthToken, string* body);

			void fillNonceAndTimestamp(string* nonce, string* timestamp);
		}
	}
}
