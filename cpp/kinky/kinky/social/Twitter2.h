#pragma once

#include <kinky/rest/KRest.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/social/OAuth1.h>
#include <kinky/social/OAuth2.h>
#include <kinky/social/Common.h>
#include <kinky/utils/KUrlEncodeRfc3986.h>
#include <kinky/utils/HMAC_SHA1.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest::http;

	namespace social {

		/* Deprecated: use kinky::social::oauth2 instead */
		namespace twitter2 {

			/* Deprecated: use kinky::social::common::curlWriter instead */
			int curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer);

			KObject* get(const char* hash, const char* consumerKey, const char* accessToken, KObject* params);

			KObject* post(const char* hash, const char* consumerKey, const char* accessToken, KObject* params);

			pair<const char*, const char*>* getOAuthToken(KObject* response, const char* consumerKey, const char* redirectURI, bool force_login);

			KObject* getAccessToken(const char* consumerKey, const char* consumerSecret, const char* x_auth_username, const char* x_auth_password, const char* oauth_token);

			KObject* getURL(const char* hash, KObject* params);
		}
	}
}
