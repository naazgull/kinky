#include <kinky/social/OpenId.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

kinky::lang::KObject* kinky::social::openid::parseResponse(CURL* curl, string* buffer, KObject* params) {
	long contentLength = -1;
	curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &contentLength);

	char* contentTypeChar;
	curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &contentTypeChar);
	string contentType = contentTypeChar;

	KObject* toReturn = NULL;
	(*kinky::io::out) << "\n\n\n----- '' -----\n\n\n" << contentType << "\n\n" << *buffer << "\n\n\n----- '' -----\n\n\n" << flush;
	if (contentLength != -1) {
		stringstream ss(*buffer);

		string item;

		toReturn = new KObject();

		while (std::getline(ss, item, '\n')) {
			size_t separator = item.find(":");

			if (separator != string::npos) {
				string name = item.substr(0, separator);
				string* value = new string(curl_unescape(item.substr(separator + 1).data(), 0));

				toReturn->setString(name.data(), value->data());

				delete value;
			}
		}

		if (toReturn == NULL)
		toReturn = new KObject("json");
	}
	else {
		toReturn = new KObject(params->base());
	}

	long httpCode = 0;
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
	toReturn->setStatus(httpCode);

	return toReturn;
}

kinky::lang::KObject* kinky::social::openid::post(const char* hash, KObject* params) {
	string* url = new string(hash);

	string* postVars = new string("");

	bool first = true;
	for (KObject::KObjAttributes::iterator it = params->begin(); it != params->end(); it++) {
		void* val = (*it)->value->pointed();
		string key = (*it)->key;

		if (!first) {
			postVars->append("&");
		}

		first = false;
		postVars->append(key);
		postVars->append("=");
		postVars->append(curl_escape(static_cast<string*>(val)->data(), static_cast<string*>(val)->length()));
	}

	string buffer;
	char errorBuffer[CURL_ERROR_SIZE];

	CURL* curl = curl_easy_init();

	OpenSSL_add_all_algorithms();
	//curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, KINKY_CURL_TIMEOUT);
	curl_easy_setopt(curl, CURLOPT_POST, 1);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postVars->data());
	curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, postVars->length());
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
	curl_easy_setopt(curl, CURLOPT_URL, url->data());
	curl_easy_setopt(curl, CURLOPT_HEADER, 0);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, social::common::curlWriter);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	CURLcode result = curl_easy_perform(curl);

	if (result == CURLE_OK) {
		KObject* toReturn = kinky::social::openid::parseResponse(curl, &buffer, params);

		curl_easy_cleanup(curl);
		EVP_cleanup();

		return toReturn;
	}
	else {
		curl_easy_cleanup(curl);
		EVP_cleanup();

		throw new KRestException(result, errorBuffer);
	}
}
