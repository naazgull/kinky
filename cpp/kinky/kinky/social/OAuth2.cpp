#include <kinky/social/OAuth2.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

kinky::lang::KObject* kinky::social::oauth2::get(const char* hash, KObject* params, string headerToken) {
	string* url = new string(hash);
	string buffer;
	char errorBuffer[CURL_ERROR_SIZE];

	CURL* curl = curl_easy_init();

	struct curl_slist *slist = NULL;

	if (params != NULL) {
		KObject::KObjHeaders* headers = params->getHeaders();
		if (headers != NULL) {
			for (KObject::KObjHeaders::iterator it = headers->begin(); it != headers->end(); it++) {
				ostringstream oss;
				string name(it->first);
				kinky::utils::prettify_header_name(name);
				oss << name << ": " << **(it->second);
				slist = curl_slist_append(slist, oss.str().data());
			}
		}

		for (KObject::KObjAttributes::iterator it = params->begin(); it != params->end(); it++) {
			void* val = (*it)->value->pointed();
			string key = (*it)->key;

			if (url->find("?") == string::npos) {
				url->append("?");
			}
			else {
				url->append("&");
			}

			if (headerToken.length() != 0 && key == string("access_token")) {
				ostringstream oss;
				oss << "Authorization: " << headerToken << " " << *(static_cast<string*>(val));
				slist = curl_slist_append(slist, oss.str().data());
			}
			else {
				url->append(key);
				url->append("=");
				url->append(*(static_cast<string*>(val)));
			}
		}
	}

	OpenSSL_add_all_algorithms();
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
	curl_easy_setopt(curl, CURLOPT_URL, url->data());
	curl_easy_setopt(curl, CURLOPT_HEADER, 0);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, social::common::curlWriter);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	CURLcode result = curl_easy_perform(curl);

	if (result == CURLE_OK) {
		KObject* toReturn = kinky::social::common::parseResponse(curl, &buffer, params);

		curl_slist_free_all(slist);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		return toReturn;
	}
	else {
		curl_slist_free_all(slist);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		throw new KRestException(result, errorBuffer);
	}
}

kinky::lang::KObject* kinky::social::oauth2::post(const char* hash, KObject* params, bool postAccessToken, string headerToken) {
	string* url = new string(hash);
	string* postVars = new string("");
	string buffer;
	char errorBuffer[CURL_ERROR_SIZE];

	CURL* curl = curl_easy_init();

	struct curl_slist *slist = NULL;

	if (params != NULL) {
		KObject::KObjHeaders* headers = params->getHeaders();
		if (headers != NULL) {
			for (KObject::KObjHeaders::iterator it = headers->begin(); it != headers->end(); it++) {
				ostringstream oss;
				string name(it->first);
				kinky::utils::prettify_header_name(name);
				oss << name << ": " << **(it->second);
				slist = curl_slist_append(slist, oss.str().data());
			}
		}

		bool first = true;
		for (KObject::KObjAttributes::iterator it = params->begin(); it != params->end(); it++) {
			void* val = (*it)->value->pointed();
			string key = (*it)->key;

			if (!first) {
				postVars->append("&");
			}

			if (headerToken.length() != 0 && key == string("access_token")) {
				ostringstream oss;
				oss << "Authorization: " << headerToken << " " << *(static_cast<string*>(val));
				slist = curl_slist_append(slist, oss.str().data());
			}
			else if (string(key) == "access_token" && !postAccessToken) {
				url->append("?access_token=");
				url->append(*(static_cast<string*>(val)));
			}
			else {
				first = false;
				postVars->append(key);
				postVars->append("=");
				postVars->append(curl_escape(static_cast<string*>(val)->data(), static_cast<string*>(val)->length()));
			}
		}
	}

	OpenSSL_add_all_algorithms();
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
	curl_easy_setopt(curl, CURLOPT_POST, 1);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postVars->data());
	curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, postVars->length());
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
	curl_easy_setopt(curl, CURLOPT_URL, url->data());
	curl_easy_setopt(curl, CURLOPT_HEADER, 0);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, social::common::curlWriter);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	CURLcode result = curl_easy_perform(curl);

	if (result == CURLE_OK) {
		KObject* toReturn = kinky::social::common::parseResponse(curl, &buffer, params);

		curl_slist_free_all(slist);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		delete url;
		delete postVars;

		return toReturn;
	}
	else {
		curl_slist_free_all(slist);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		delete url;
		delete postVars;

		throw new KRestException(result, errorBuffer);
	}
}

kinky::lang::KObject* kinky::social::oauth2::put(const char* hash, KObject* params, bool postAccessToken, string headerToken) {
	string* url = new string(hash);
	string buffer;
	char errorBuffer[CURL_ERROR_SIZE];

	CURL* curl = curl_easy_init();

	struct curl_slist *slist = NULL;
	string* body = new string();

	if (params != NULL) {
		KObject::KObjHeaders* headers = params->getHeaders();
		if (headers != NULL) {
			for (KObject::KObjHeaders::iterator it = headers->begin(); it != headers->end(); it++) {
				ostringstream oss;
				string name(it->first);
				kinky::utils::prettify_header_name(name);
				oss << name << ": " << **(it->second);
				slist = curl_slist_append(slist, oss.str().data());
			}
		}

		if (headerToken.length() != 0 && (*params)["access_token"] != NULL_KOBJECT) {
			ostringstream oss;
			oss << "Authorization: " << headerToken << " " << (string) (*params)["access_token"];
			slist = curl_slist_append(slist, oss.str().data());
			params->remove("access_token");
		}
		ostringstream oss;
		oss << *params;
		body->assign(oss.str());
	}

	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
	curl_easy_setopt(curl, CURLOPT_READDATA, body);
	curl_easy_setopt(curl, CURLOPT_READFUNCTION, social::common::curlReader);
	curl_easy_setopt(curl, CURLOPT_INFILESIZE, body->length());
	curl_easy_setopt(curl, CURLOPT_UPLOAD, 1);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
	curl_easy_setopt(curl, CURLOPT_URL, url->data());
	curl_easy_setopt(curl, CURLOPT_HEADER, 0);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, social::common::curlWriter);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	CURLcode result = curl_easy_perform(curl);

	if (result == CURLE_OK) {
		KObject* toReturn = kinky::social::common::parseResponse(curl, &buffer, params);

		curl_slist_free_all(slist);
		curl_easy_cleanup(curl);
		delete url;
		delete body;

//		(*toReturn) << kinky::pretty;
//		kout << *toReturn << endl << flush;

		return toReturn;
	}
	else {
		curl_slist_free_all(slist);
		curl_easy_cleanup(curl);
		delete url;
		delete body;

		throw new KRestException(result, errorBuffer);
	}
}
