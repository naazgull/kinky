#pragma once

#include <kinky/rest/KRest.h>
#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/social/OAuth1.h>
#include <kinky/social/Common.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest::http;

	namespace social {

		/* Deprecated: use kinky::social::oauth2 instead */
		namespace twitter {

			/* Deprecated: use kinky::social::common::curlWriter instead */
			int curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer);

			KObject* get(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params);

			KObject* post(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params, KObject* body);

			pair<const char*, const char*>* getOAuthToken(KObject* response, const char* consumerKey, const char* consumerSecret, const char* redirectURI, bool force_login);

			KObject* getAccessToken(const char* consumerKey, const char* consumerSecret, const char* oauth_token, const char* oauth_verifier, const char* oauth_token_secret);

			KObject* getURL(const char* hash, KObject* params);


		}
	}
}
