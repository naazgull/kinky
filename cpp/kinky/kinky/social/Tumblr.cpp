#include <kinky/social/Tumblr.h>

kinky::lang::KObject* kinky::social::tumblr::get(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params) {
	string* url = new string("https://api.tumblr.com/v2");
	url->append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0";

	params->sort();

	if (url->find("/user") == string::npos) {
		(*params) << "api_key" << consumerKey;
	}
	KObject* profile_data = kinky::social::oauth1::get(url->data(), params, consumerSecret, tokenSecret);

	delete nonce;
	delete timestamp;
	delete url;

	return profile_data;
}


kinky::lang::KObject* kinky::social::tumblr::post(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params, KObject* body) {
	string* url = new string("https://api.tumblr.com/v2");
	url->append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0";

	params->sort();

	if (url->find("/user") == string::npos) {
		if (url->find("?") == string::npos) {
			url->insert(url->length(), "?");
		}
		else {
			url->insert(url->length(), "&");
		}
		url->insert(url->length(), "api_key=");
		url->insert(url->length(), consumerKey);
	}

	ostringstream oss;
	oss << *body << flush;
	string strbody(oss.str());

	KObject* profile_data = kinky::social::oauth1::post(url->data(), params, consumerSecret, tokenSecret, &strbody);

	delete nonce;
	delete timestamp;
	delete url;

	return profile_data;
}
