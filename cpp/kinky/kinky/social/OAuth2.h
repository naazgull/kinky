#pragma once

#include <curl/curl.h>
#include <kinky/rest/KRestException.h>
#include <kinky/social/Common.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest;

	namespace social {

		namespace oauth2 {

			KObject* get(const char* hash, KObject* params, string headerToken = "");

			KObject* post(const char* hash, KObject* params, bool postAccessToken = false, string headerToken = "");

			KObject* put(const char* hash, KObject* params, bool postAccessToken = false, string headerToken = "");
		}
	}
}
