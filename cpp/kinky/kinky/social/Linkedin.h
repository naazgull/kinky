#pragma once

#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/social/OAuth2.h>
#include <kinky/social/Common.h>
#include <kinky/social/KSocialTokenExpiredException.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest::http;

	namespace social {
		/* Deprecated: use kinky::social::oauth2 instead */
		namespace linkedin {


			KObject* get(string hash, KObject* params);

			KObject* post(string hash, KObject* params, bool postAccessToken = false);

		}
	}
}
