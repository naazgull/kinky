#pragma once

#include <exception>
#include <string>
#include <iostream>
#include <sstream>
#include <kinky/memory/KMemoryManager.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	namespace social {

		class KSocialTokenExpiredException: public exception {
			public:
				KSocialTokenExpiredException(string messageParam);
				KSocialTokenExpiredException(kinky::memory::managed_ptr<string>* messageParam);
				virtual ~KSocialTokenExpiredException() throw();

				string* getMessage();

				kinky::memory::managed_ptr<string>* get();

				friend ostream& operator<<(ostream& os, KSocialTokenExpiredException& f) {
					os << "SOCIAL EXCEPTION >> message: " << f.getMessage()->data() << endl << flush;
					return os;
				}


				virtual string what() throw ();

			private:
				kinky::memory::managed_ptr<string>* message;
		};

	}
}
