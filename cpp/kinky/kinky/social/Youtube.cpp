#include <kinky/social/Youtube.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

size_t kinky::social::youtube::curlWriter(char *data, size_t size, size_t nmemb, ofstream *buffer) {
	size_t result = 0;

	if (buffer != NULL) {
		buffer->write(data, size * nmemb);
		result = size * nmemb;
	}

	return result;
}

size_t kinky::social::youtube::curlTextWriter(char *data, size_t size, size_t nmemb, string *buffer) {
	size_t result = 0;

	if (buffer != NULL) {
		buffer->append(data, size * nmemb);
		result = size * nmemb;
	}

	return result;
}

size_t kinky::social::youtube::curlReader(void *ptr, size_t size, size_t nmemb, void *userdata) {
if (userdata != NULL) {
		string* buffer = static_cast<string*>(userdata);

		if (buffer != NULL && buffer->length() != 0) {
			size_t result = buffer->length() > size * nmemb ? size * nmemb : buffer->length();
			memcpy(ptr, buffer->c_str(), result);

			buffer->erase(0, result);

			return result;
		}
	}
	return 0;
}

bool kinky::social::youtube::retrieveServerVideo(string* localfilelocation, string videofilename, string* protocol, string* server, string* serverpath, string* user, string* pass, bool deleteafter){
	bool videodownloaded = false;

	CURL *curldownloadvideo;
	CURLcode resdownloadvideo;

	ofstream* ftpfile = new ofstream();
	ostringstream localfile;
	localfile << localfilelocation->data() << videofilename.data();
	ftpfile->open(localfile.str().data());

	curldownloadvideo = curl_easy_init();
	if(curldownloadvideo) {

		ostringstream serverurl;
		if (!user->empty() && !pass->empty()){
			serverurl << protocol->data() << "://" << user->data() << ":" << pass->data() << "@" << server->data() << "/" << serverpath->data() << "/" << videofilename.data();
		}else{
			serverurl << protocol->data() << "://" << server->data() << "/" << serverpath->data() << "/" << videofilename.data();
		}

		//curl_easy_setopt(curldownloadvideo, CURLOPT_CONNECTTIMEOUT, KINKY_CURL_TIMEOUT);
		curl_easy_setopt(curldownloadvideo, CURLOPT_URL, serverurl.str().data());
		curl_easy_setopt(curldownloadvideo, CURLOPT_WRITEFUNCTION, curlWriter);
		curl_easy_setopt(curldownloadvideo, CURLOPT_WRITEDATA, ftpfile);

		resdownloadvideo = curl_easy_perform(curldownloadvideo);

		curl_easy_cleanup(curldownloadvideo);

		if(CURLE_OK != resdownloadvideo) {
		  fprintf(stderr, "curl told us %d\n", resdownloadvideo);
		}else{
			videodownloaded = true;
			if (deleteafter == true){
				kinky::social::youtube::deleteServerVideo(protocol, user, pass, server, serverpath, videofilename);
			}
		}
	}

	ftpfile->close();
	delete ftpfile;

	return videodownloaded;

}

void kinky::social::youtube::deleteServerVideo(string* protocol, string* user, string* pass, string* server, string* serverpath, string videofilename){

	CURL *curldeletevideo;
	CURLcode resdeletevideo;

	curldeletevideo = curl_easy_init();
	if(curldeletevideo) {

		struct curl_slist *list = NULL;
		string ftpCommand2 = "DELE ";
		ftpCommand2.append(serverpath->data());
		ftpCommand2.append("/");
		ftpCommand2.append(videofilename.data());
		list = curl_slist_append(list, ftpCommand2.data());
		curl_easy_reset(curldeletevideo);

		ostringstream serverurl1;
		serverurl1 << protocol->data() << "://" << user->data() << ":" << pass->data() << "@" << server->data() << "/";
		//curl_easy_setopt(curldeletevideo, CURLOPT_CONNECTTIMEOUT, KINKY_CURL_TIMEOUT);
		curl_easy_setopt(curldeletevideo, CURLOPT_URL, serverurl1.str().data());
		curl_easy_setopt(curldeletevideo, CURLOPT_NOPROGRESS, 1);
		curl_easy_setopt(curldeletevideo, CURLOPT_QUOTE, list);
		resdeletevideo = curl_easy_perform(curldeletevideo);
		if (CURLE_OK != resdeletevideo){
			fprintf(stderr, "curl told us %d\n", resdeletevideo);
		}

		curl_slist_free_all(list);
	}
	curl_easy_cleanup(curldeletevideo);
}

kinky::lang::KObject* kinky::social::youtube::directUploadVideo(string localfile, string videofilename, string* playlisttoadd, string* requestsource, string* videotitle, string videocategory, string videokeywords, string authorizationtoken, string developerkey){

	KObject* ret = new KObject();
	KRestException* thr = NULL;

	//POST PARA YOUTUBE - info em https://developers.google.com/youtube/2.0/developers_guide_protocol_direct_uploading
	CURL *curluploadvideo;
	CURLcode resuploadvideo;
	curluploadvideo = curl_easy_init();
	if(curluploadvideo) {
		ostringstream boundarystring;
		boundarystring << "--f93dcbA3";

		struct curl_slist *slist = NULL;
		slist = curl_slist_append(slist, "Host: uploads.gdata.youtube.com");
		ostringstream authheader;
		authheader << "Authorization: Bearer " <<  authorizationtoken.data();
		slist = curl_slist_append(slist, authheader.str().data());
		slist = curl_slist_append(slist, "GData-Version: 2");
		ostringstream userkey;
		userkey << "X-GData-Key: key=" << developerkey.data();
		slist = curl_slist_append(slist, userkey.str().data());
		ostringstream slug;
		slug << "Slug: " << videofilename.data();
		slist = curl_slist_append(slist, slug.str().data());
		ostringstream boundaryheader;
		boundaryheader << "Content-Type: multipart/related; boundary=f93dcbA3";
		slist = curl_slist_append(slist, boundaryheader.str().data());
		slist = curl_slist_append(slist, "Connection: close");

		ostringstream postbody;
		postbody << boundarystring.str() << CRLF;
		postbody << "Content-Type: application/atom+xml; charset=UTF-8" << CRLF << CRLF;
		postbody << "<?xml version=\"1.0\"?>";
		postbody << "<entry xmlns=\"http://www.w3.org/2005/Atom\" ";
		postbody << "xmlns:media=\"http://search.yahoo.com/mrss/\" ";
		postbody << "xmlns:yt=\"http://gdata.youtube.com/schemas/2007\">";
		postbody << "<media:group>";
		postbody << "<media:title type=\"plain\">" << videotitle->data() << "</media:title>";
		postbody << "<media:description type=\"plain\">";
		postbody << requestsource->data();
		postbody << "</media:description>";
		postbody << "<media:category scheme=\"http://gdata.youtube.com/schemas/2007/categories.cat\">" << videocategory << "</media:category>";
		postbody << "<media:keywords>" << videokeywords << "</media:keywords>";
		postbody << "<yt:private/>";
		postbody << "</media:group>";
		postbody << "<yt:playlistId>" << playlisttoadd->data() << "</yt:playlistId>";
		postbody << "</entry>" << CRLF;
		postbody << boundarystring.str() << CRLF;
		postbody << "Content-Type: video/x-flv" << CRLF;
		postbody << "Content-Transfer-Encoding: binary" << CRLF << CRLF;

		//OBTER O VÍDEO
		size_t size;
		char * memblock;
		ifstream file (localfile.data(), ios::in|ios::binary|ios::ate);
		if (file.is_open()){
			file.seekg(0, ios::end);
			size = file.tellg();
			file.seekg(0, ios::beg);
			memblock = new char [size];
			file.read(memblock, size);
			file.close();
			remove(localfile.data());
		}else{
			thr = new KRestException(KHTTPResponse::PRECONDITION_FAILED, "Video not found");
			throw thr;
		}

		for (size_t c = 0; c != size; c++) {
			postbody << memblock[c] << flush;
		}

		postbody << CRLF << boundarystring.str() << "--" << flush;
		size_t bodyLen = postbody.str().length();
		string* body = new string(postbody.str());
		string buffer;
		//curl_easy_setopt(curluploadvideo, CURLOPT_CONNECTTIMEOUT, KINKY_CURL_TIMEOUT);
		curl_easy_setopt(curluploadvideo, CURLOPT_URL, "https://uploads.gdata.youtube.com/feeds/api/users/default/uploads");
		curl_easy_setopt(curluploadvideo, CURLOPT_NOSIGNAL, 1);
		curl_easy_setopt(curluploadvideo, CURLOPT_NOPROGRESS, 1);
		curl_easy_setopt(curluploadvideo, CURLOPT_AUTOREFERER, 1);
		curl_easy_setopt(curluploadvideo, CURLOPT_FOLLOWLOCATION, 1);
		curl_easy_setopt(curluploadvideo, CURLOPT_CUSTOMREQUEST, "POST");
		curl_easy_setopt(curluploadvideo, CURLOPT_SSL_VERIFYPEER, 1);
		curl_easy_setopt(curluploadvideo, CURLOPT_SSL_VERIFYHOST, 2);
		curl_easy_setopt(curluploadvideo, CURLOPT_HEADER, 0);
		ostringstream bodylenght;
		curl_easy_setopt(curluploadvideo, CURLOPT_HTTPHEADER, slist);
		curl_easy_setopt(curluploadvideo, CURLOPT_READDATA, body);
		curl_easy_setopt(curluploadvideo, CURLOPT_READFUNCTION, curlReader);
		curl_easy_setopt(curluploadvideo, CURLOPT_INFILESIZE, bodyLen);
		curl_easy_setopt(curluploadvideo, CURLOPT_UPLOAD, 1);
		curl_easy_setopt(curluploadvideo, CURLOPT_WRITEFUNCTION, curlTextWriter);
		curl_easy_setopt(curluploadvideo, CURLOPT_WRITEDATA, &buffer);

		resuploadvideo = curl_easy_perform(curluploadvideo);
		if (resuploadvideo == CURLE_OK) {

			long httpCode = 0;
			curl_easy_getinfo(curluploadvideo, CURLINFO_RESPONSE_CODE, &httpCode);

			switch (httpCode){
				case 201:
					try {
						ret = KXML::parse(&buffer);
					}
					catch(KParseException* e) {
						delete e;
					}
					ret->base("json");
					ret->setStatus(httpCode);
					break;
				case 400:
					ret->setStatus(httpCode);
					break;
				case 401:
					ret->setStatus(httpCode);
					break;
				case 403:
					ret->setStatus(httpCode);
					break;
				default:
					ret->setStatus(httpCode);
			}
		}

		curl_slist_free_all(slist);

		delete[] memblock;
		delete body;

	}

	curl_easy_cleanup(curluploadvideo);

	return ret;

}

kinky::lang::KObject* kinky::social::youtube::retrieveUserPlaylists(string authorizationtoken, string developerkey){

	KObject* ret = new KObject();

	CURL *curluserplaylists;
	CURLcode resuserplaylists;
	curluserplaylists = curl_easy_init();
	if(curluserplaylists) {
		ostringstream userkey;
		ostringstream authheader;
		struct curl_slist *slist = NULL;
		userkey << "X-GData-Key: key=" << developerkey.data();
		slist = curl_slist_append(slist, "Host: gdata.youtube.com");
		authheader << "Authorization: Bearer " <<  authorizationtoken.data();
		slist = curl_slist_append(slist, authheader.str().data());
		slist = curl_slist_append(slist, userkey.str().data());

		//curl_easy_setopt(curluserplaylists, CURLOPT_CONNECTTIMEOUT, KINKY_CURL_TIMEOUT);
		curl_easy_setopt(curluserplaylists, CURLOPT_HEADER, 0);
		curl_easy_setopt(curluserplaylists, CURLOPT_HTTPHEADER, slist);
		curl_easy_setopt(curluserplaylists, CURLOPT_URL, "https://gdata.youtube.com/feeds/api/users/default/playlists?v=2");
		curl_easy_setopt(curluserplaylists, CURLOPT_WRITEFUNCTION, curlTextWriter);
		string buffer;
		curl_easy_setopt(curluserplaylists, CURLOPT_WRITEDATA, &buffer);

		resuserplaylists = curl_easy_perform(curluserplaylists);

		if (resuserplaylists == CURLE_OK) {

			long httpCode = 0;
			curl_easy_getinfo(curluserplaylists, CURLINFO_RESPONSE_CODE, &httpCode);

			switch (httpCode){
				case 200:
					try {
						ret = KXML::parse(&buffer);
					}
					catch(KParseException* e) {
						delete e;
					}
					ret->base("json");
					ret->setStatus(httpCode);
					break;
				case 201:
					try {
						ret = KXML::parse(&buffer);
					}
					catch(KParseException* e) {
						delete e;
					}
					ret->base("json");
					ret->setStatus(httpCode);
					break;
				case 400:
					ret->setStatus(httpCode);
					break;
				case 401:
					ret->setStatus(httpCode);
					break;
				case 403:
					ret->setStatus(httpCode);
					break;
				default:
					ret->setStatus(httpCode);
			}
		}

		curl_slist_free_all(slist);

	}

	curl_easy_cleanup(curluserplaylists);
	return ret;

}

kinky::lang::KObject* kinky::social::youtube::requestApplicationAccess(string clientID, string redirectURI, string scope, string accessType){

	KObject* params = KOBJECT(
		"client_id" << clientID <<
		"redirect_uri" << redirectURI <<
		"response_type" << "code" <<
		"scope" << scope <<
		"approval_prompt" << "auto" <<
		"access_type" << accessType);

	KObject* application_access_data = kinky::social::oauth2::get("https://accounts.google.com/o/oauth2/auth", params);

		delete params;
		return application_access_data;

}

kinky::lang::KObject* kinky::social::youtube::getAccessToken(string code, string clientID, string clientSecret, string redirectURI, string grantType){

	KObject* params = KOBJECT(
		"code" << code <<
		"client_id" << clientID <<
		"client_secret" << clientSecret <<
		"redirect_uri" << redirectURI <<
		"grant_type" << grantType);

	KObject* access_token_data = kinky::social::oauth2::post("https://accounts.google.com/o/oauth2/token", params);

		delete params;
		return access_token_data;

}

kinky::lang::KObject* kinky::social::youtube::refreshAccessToken(string clientID, string clientSecret, string refreshToken, string grantType){

	KObject* params = KOBJECT(
		"client_id" << clientID <<
		"client_secret" << clientSecret <<
		"refresh_token" << refreshToken <<
		"grant_type" << grantType);

	KObject* access_token_data = kinky::social::oauth2::post("https://accounts.google.com/o/oauth2/token", params);

		delete params;
		return access_token_data;

}

kinky::lang::KObject* kinky::social::youtube::retrievePlaylistFeed(string playlistID, string format){

	KObject* params = NULL;
	if (!format.empty()){
		params = KOBJECT(
				"alt" << format);
	}

	string hash = "http://gdata.youtube.com/feeds/api/playlists/";
	hash.append(playlistID);

	KObject* playlist_feed_data = kinky::social::oauth2::get(hash.c_str(), params);

	delete params;
	return playlist_feed_data;

}

kinky::lang::KObject* kinky::social::youtube::searchPlaylistFeed(string playlistID, KObject* searchfields){

	KObject* params = new KObject();
	bool first = true;
	ostringstream oss;
	oss.str("");
	for (KObject::KObjAttributes::iterator it = searchfields->begin(); it != searchfields->end(); it++) {
		string searchfieldterm = (string) (**it)["term"];
		string searchfieldvalue = (string) (**it)["value"];
		if (first) {
			oss << "?" << searchfieldterm << "=" << searchfieldvalue;
			first = false;
		}else{
			oss << "&" << searchfieldterm << "=" << searchfieldvalue;
		}
	}

	string hash = "http://gdata.youtube.com/feeds/api/playlists/";
	hash.append(playlistID);
	hash.append(oss.str());

	KObject* playlist_feed_data = kinky::social::oauth2::get(hash.c_str(), params);

	return playlist_feed_data;

}

kinky::lang::KObject* kinky::social::youtube::oEmbed(string youtubeUrl, string format){

	KObject* ret = new KObject();
	KObject* params = new KObject();

	ostringstream youtubeOembedUrl;
	youtubeOembedUrl << "http://www.youtube.com/oembed?url=" << youtubeUrl << "&format=" << format << flush;

	ret = kinky::social::oauth2::get(youtubeOembedUrl.str().data(), params);

	delete params;

	return ret;

}
