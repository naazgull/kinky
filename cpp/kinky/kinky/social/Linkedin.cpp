#include <kinky/social/Linkedin.h>

#include <kinky/utils/KDateTime.h>

#define TOKEN_EXPIRED "has expired"
#define CHANGED_PASSWORD "changed the password"

kinky::lang::KObject* kinky::social::linkedin::get(string hash, KObject* params) {
	string url("https://api.linkedin.com/v1");
	url.append(hash);

	(*params) 	<<
		"oauth2_access_token" << (string) (*params)["access_token"] <<
		"format" << "json" <<
		"secure-urls" << "true";

	params->remove("access_token");
	if ((*params)["fields"] != NULL_KOBJECT) {
		url.insert(url.length(), (string) (*params)["fields"]);
		params->remove("fields");
	}

	KObject* r = kinky::social::oauth2::get(url.data(), params);

	return r;
}

kinky::lang::KObject* kinky::social::linkedin::post(string hash, KObject* params, bool postAccessToken) {
	string url("https://api.linkedin.com/v1");
	url.append(hash);

	(*params) 	<<
		"oauth2_access_token" << (string) (*params)["access_token"] <<
		"format" << "json" <<
		"secure-urls" << "true";

	params->remove("access_token");
	if ((*params)["fields"] != NULL_KOBJECT) {
		url.insert(url.length(), (string) (*params)["fields"]);
		params->remove("fields");
	}

	KObject* r = kinky::social::oauth2::post(url.data(), params, postAccessToken);

	return r;
}
