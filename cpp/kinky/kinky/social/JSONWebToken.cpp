#include <kinky/social/JSONWebToken.h>

#include <openssl/pkcs12.h>
#include <openssl/err.h>

void kinky::social::jsonwebtoken::formJWT(string header_alg, KObject* claimset_params, string privatekey_file_location, string privatekey_file_pass, string* ret) {

	ostringstream jwt_header_oss_in;
	jwt_header_oss_in << "{" << "\"alg\"" << ":" << "\"" << header_alg << "\"" << "," <<
	                    "\"typ\"" << ":" << "\"JWT\"" <<
	                    "}";

	time_t iat = time(0) - 300;
	time_t exp = iat + 3600;

	ostringstream jwt_claimset_oss_in;
	jwt_claimset_oss_in << "{" << "\"iss\"" << ":" << "\"" << (*claimset_params)["iss"] << "\"" << "," <<
	                    "\"scope\"" << ":" << "\"" << (*claimset_params)["scope"] << "\"" << "," <<
	                    "\"aud\"" << ":" << "\"" << (*claimset_params)["aud"] << "\"" << "," <<
	                    "\"exp\"" << ":" << exp << "," <<
	                    "\"iat\"" << ":" << iat <<
	                    "}";

	istringstream jwt_header_in(jwt_header_oss_in.str());
	ostringstream jwt_header_oss;
	kinky::utils::base64url_encode(jwt_header_in, jwt_header_oss);

	istringstream jwt_claimset_in(jwt_claimset_oss_in.str());
	ostringstream jwt_claimset_oss;
	kinky::utils::base64url_encode(jwt_claimset_in, jwt_claimset_oss);

	ostringstream jwt_header_claimset_oss;
	jwt_header_claimset_oss << jwt_header_oss.str() << "." << jwt_claimset_oss.str();

	ostringstream jwt_header_claimset_utf8_oss;
	kinky::utils::utf8_encode(jwt_header_claimset_oss.str(), jwt_header_claimset_utf8_oss);
	jwt_header_claimset_utf8_oss << flush;

	string jwt_signature;
	kinky::social::jsonwebtoken::signWithRsaSha256(jwt_header_claimset_utf8_oss.str(), privatekey_file_location, privatekey_file_pass, &jwt_signature);

	istringstream jwt_signature_in(jwt_signature);
	ostringstream jwt_signature_oss;
	kinky::utils::base64url_encode(jwt_signature_in, jwt_signature_oss);

	ret->assign(jwt_header_claimset_oss.str() + "." + jwt_signature_oss.str());

}

#define OPENSSL_FUNCTION_CALL(a) \
if (!a) \
{ \
    char buffer[120]; \
    ERR_error_string(ERR_get_error(), buffer); \
    char s[256]; \
    sprintf(s, ""#a" function call failed.\n%s", buffer); \
    throw s; \
} \

void kinky::social::jsonwebtoken::signWithRsaSha256(string input, string private_key_location, string private_key_pass, string* ret)
                    {

	std::ifstream in(private_key_location.data());
	std::string private_key((std::istreambuf_iterator<char>(in)),
	                    std::istreambuf_iterator<char>());

	EVP_PKEY *pkey = 0;
	BIO *bp = 0;
	EVP_MD_CTX *ctx = 0;
	const EVP_MD *sha256Md = 0;
	unsigned char sig[256];
	unsigned int s(0);

	PKCS12 *p12 = 0;
	X509 *cert = 0;

	OpenSSL_add_all_ciphers();
	OpenSSL_add_all_digests();

	ctx = EVP_MD_CTX_create();
	EVP_MD_CTX_init(ctx);
	sha256Md = EVP_sha256();
	bp = BIO_new_mem_buf((void*) private_key.data(), private_key.length());

	try {
		OPENSSL_FUNCTION_CALL(EVP_SignInit(ctx, sha256Md));
		OPENSSL_FUNCTION_CALL(EVP_SignUpdate(ctx, input.data(), input.length()));
		OPENSSL_FUNCTION_CALL(d2i_PKCS12_bio(bp, &p12));
		OPENSSL_FUNCTION_CALL(PKCS12_parse(p12, private_key_pass.data(), &pkey, &cert, NULL));

		s = EVP_PKEY_size(pkey);
		OPENSSL_FUNCTION_CALL(EVP_SignFinal(ctx, sig, &s, pkey));

		ret->assign(string((const char*) sig, s));

	}
	catch (const char *s)
	{
		ret->assign(string("error"));
	}
	EVP_MD_CTX_destroy(ctx);
	BIO_free(bp);
	X509_free(cert);
	EVP_cleanup();

}

size_t kinky::social::jsonwebtoken::curlTextWriter(char *data, size_t size, size_t nmemb, string *buffer) {
	size_t result = 0;

	if (buffer != NULL) {
		buffer->append(data, size * nmemb);
		result = size * nmemb;
	}

	return result;
}

kinky::lang::KObject* kinky::social::jsonwebtoken::getAccessToken(string grant_type, string assertion, string content_type, string uri) {

	ostringstream postbody_oss;

	KObject* params = NULL;

	if (content_type == "application/json") {
		KObject* params = KOBJECT("grant_type" << grant_type << "assertion" << assertion);
		kinky::lang::KObject::stringifyJSON(params, postbody_oss);
	}
	else if (content_type == "application/x-www-form-urlencoded") {
		postbody_oss << "grant_type=" << grant_type << "&assertion=" << assertion;
	}

	KObject* ret = new KObject();

	char errorBuffer[CURL_ERROR_SIZE];
	string buffer;

	CURL *curl_service_account = curl_easy_init();
	CURLcode res_service_account;

	OpenSSL_add_all_algorithms();
	if (curl_service_account) {
		struct curl_slist *slist = NULL;
		slist = curl_slist_append(slist, "Accept: */*");
		slist = curl_slist_append(slist, "Expect:");

		ostringstream content_type_oss;
		content_type_oss << "Content-Type: " << content_type;
		slist = curl_slist_append(slist, content_type_oss.str().c_str());

		string* postVars = new string(postbody_oss.str());

		//curl_easy_setopt(curl_service_account, CURLOPT_VERBOSE, 1L);
		curl_easy_setopt(curl_service_account, CURLOPT_POST, 1);
		curl_easy_setopt(curl_service_account, CURLOPT_HTTPHEADER, slist);
		curl_easy_setopt(curl_service_account, CURLOPT_POSTFIELDS, postVars->data());
		curl_easy_setopt(curl_service_account, CURLOPT_POSTFIELDSIZE, postVars->length());
		curl_easy_setopt(curl_service_account, CURLOPT_NOSIGNAL, 1);
		curl_easy_setopt(curl_service_account, CURLOPT_NOPROGRESS, 1);
		curl_easy_setopt(curl_service_account, CURLOPT_AUTOREFERER, 1);
		curl_easy_setopt(curl_service_account, CURLOPT_ERRORBUFFER, errorBuffer);
		curl_easy_setopt(curl_service_account, CURLOPT_URL, uri.data());
		curl_easy_setopt(curl_service_account, CURLOPT_HEADER, 0);
		curl_easy_setopt(curl_service_account, CURLOPT_FOLLOWLOCATION, 1);
		curl_easy_setopt(curl_service_account, CURLOPT_WRITEFUNCTION, curlTextWriter);
		curl_easy_setopt(curl_service_account, CURLOPT_WRITEDATA, &buffer);
		curl_easy_setopt(curl_service_account, CURLOPT_SSL_VERIFYHOST, 2);
		curl_easy_setopt(curl_service_account, CURLOPT_SSL_VERIFYPEER, 1);

		res_service_account = curl_easy_perform(curl_service_account);

		if (res_service_account == CURLE_OK) {

			long httpCode = 0;
			curl_easy_getinfo(curl_service_account, CURLINFO_RESPONSE_CODE, &httpCode);

			try {
				ret = KJSON::parse(&buffer);
			}
			catch (KParseException* e) {
				delete e;
				ret = new KObject();
				ret->base("json");
			}

			ret->setStatus(httpCode);
		}
		delete postVars;

		curl_slist_free_all(slist);

	}

	EVP_cleanup();
	curl_easy_cleanup(curl_service_account);

	if (params != NULL) {
		delete params;
	}
	return ret;

}
