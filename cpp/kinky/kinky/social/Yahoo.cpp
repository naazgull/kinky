#include <kinky/social/Yahoo.h>

#include <kinky/social/KSocialTokenExpiredException.h>

kinky::lang::KObject* kinky::social::yahoo::get(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params) {
	string* url = new string("http://social.yahooapis.com/v1");
	url->append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
		"format" << "json" <<
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0";

	params->sort();

	KObject* profile_data = kinky::social::oauth1::get(url->data(), params, consumerSecret, tokenSecret);

	delete nonce;
	delete timestamp;
	delete url;

	if (profile_data->getStatus() == KHTTPResponse::UNAUTHORIZED && (*profile_data)["error"] != NULL_KOBJECT && (*profile_data)["error"]["description"] != NULL_KOBJECT && ((string) (*profile_data)["error"]["description"]).find("oauth_problem=\"token_expired\"") != string::npos) {
		kinky::social::KSocialTokenExpiredException* e = new kinky::social::KSocialTokenExpiredException(((string) (*profile_data)["error"]["description"]));
		delete profile_data;
		throw e;
	}

	return profile_data;
}


kinky::lang::KObject* kinky::social::yahoo::post(const char* hash, const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, KObject* params, KObject* body) {
	string* url = new string("http://social.yahooapis.com/v1");
	url->append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
		"format" << "json" <<
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0";

	params->sort();

	ostringstream oss;
	oss << *body << flush;
	string strbody(oss.str());

	KObject* profile_data = kinky::social::oauth1::post(url->data(), params, consumerSecret, tokenSecret, &strbody);

	delete nonce;
	delete timestamp;
	delete url;

	if (profile_data->getStatus() == KHTTPResponse::UNAUTHORIZED && (*profile_data)["error"] != NULL_KOBJECT && (*profile_data)["error"]["description"] != NULL_KOBJECT && ((string) (*profile_data)["error"]["description"]).find("oauth_problem=\"token_expired\"") != string::npos) {
		kinky::social::KSocialTokenExpiredException* e = new kinky::social::KSocialTokenExpiredException(((string) (*profile_data)["error"]["description"]));
		delete profile_data;
		throw e;
	}

	return profile_data;
}

kinky::lang::KObject* kinky::social::yahoo::refreshToken(const char* consumerKey, const char* consumerSecret, const char* accessToken, const char* tokenSecret, const char* session_handle) {
	string* url = new string("https://api.login.yahoo.com/oauth/v2/get_token");

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	KObject* params = KOBJECT(
		"oauth_consumer_key" << consumerKey <<
		"oauth_nonce" << nonce->data() <<
		"oauth_session_handle" << session_handle <<
		"oauth_signature_method" << "HMAC-SHA1" <<
		"oauth_timestamp" << timestamp->data() <<
		"oauth_token" << accessToken <<
		"oauth_version" << "1.0"
	);
	params->sort();

	KObject* profile_data = kinky::social::oauth1::post(url->data(), params, consumerSecret, tokenSecret, NULL);

	delete params;
	delete nonce;
	delete timestamp;
	delete url;

	return profile_data;
}
