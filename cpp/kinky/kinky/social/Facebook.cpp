#include <kinky/social/Facebook.h>

#include <kinky/utils/KDateTime.h>
#include <kinky/rest/http/KCurl.h>

#define TOKEN_EXPIRED "has expired"
#define CHANGED_PASSWORD "changed the password"

/* Deprecated: use kinky::social::common::curlWriter instead */
int kinky::social::facebook::curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer) {
	return kinky::social::common::curlWriter(data, size, nmemb, buffer);
}

kinky::lang::KObject* kinky::social::facebook::get(string hash, KObject* params) {
	string url("https://graph.facebook.com");
	url.append(hash);

	KObject* r = kinky::social::oauth2::get(url.data(), params);
	if ((*params)["access_token"] != NULL_KOBJECT && (*r)["error"] != NULL_KOBJECT && ((string) (*r)["error"]["type"]) == "OAuthException" && (((string) (*r)["error"]["message"]).find(TOKEN_EXPIRED) != string::npos || ((string) (*r)["error"]["message"]).find(CHANGED_PASSWORD) != string::npos)) {
		kinky::social::KSocialTokenExpiredException* e = new kinky::social::KSocialTokenExpiredException(((string) (*r)["error"]["message"]));
		delete r;
		throw e;
	}

	return r;
}

kinky::lang::KObject* kinky::social::facebook::post(string hash, KObject* params, bool postAccessToken) {
	string url("https://graph.facebook.com");
	url.append(hash);

	KObject* r = kinky::social::oauth2::post(url.data(), params, postAccessToken);

	if ((*params)["access_token"] != NULL_KOBJECT && (*r)["error"] != NULL_KOBJECT && ((string) (*r)["error"]["type"]) == "OAuthException" && (((string) (*r)["error"]["message"]).find(TOKEN_EXPIRED) != string::npos || ((string) (*r)["error"]["message"]).find(TOKEN_EXPIRED) != string::npos)) {
		kinky::social::KSocialTokenExpiredException* e = new kinky::social::KSocialTokenExpiredException(((string) (*r)["error"]["message"]));
		delete r;
		throw e;
	}

	return r;
}

string kinky::social::facebook::getAuthorizeURI(string client_id, string redirect_uri, string scope) {
	ostringstream r;
	r << "https://graph.facebook.com/oauth/authorize?client_id=" << client_id << "&redirect_uri=" << redirect_uri << "&scope=" << scope;
	managed_ptr<string>* ret = new managed_ptr<string>(new string(r.str()));
	return **ret;
}

void kinky::social::facebook::getCode(KObject* response, string clientID, string redirectURI, string scope, bool force_login) {
	response->addHeader("Location", ("https://graph.facebook.com/oauth/authorize?client_id=" +
			string(clientID) + "&redirect_uri=" + string(redirectURI) +
			"&scope=" + string(scope) + (force_login ? "&auth_type=reauthenticate" : "")));

	response->setStatus(KHTTPResponse::TEMPORARY_REDIRECT);
}

kinky::lang::KObject* kinky::social::facebook::getAccessToken(string clientID, string clientSecret, string redirectURI, string fbCode) {
	KObject* params = KOBJECT(
			"client_id" << clientID <<
			"redirect_uri" << redirectURI <<
			"client_secret" << clientSecret <<
			"code" << fbCode);


	KObject* access_token_data = kinky::social::oauth2::get("https://graph.facebook.com/oauth/access_token", params);
	delete params;
	return access_token_data;
}

kinky::lang::KObject* kinky::social::facebook::getLongLivedAccessToken(string clientID, string clientSecret, string currentToken) {
	KObject* params = KOBJECT(
			"client_id" << clientID <<
			"client_secret" << clientSecret <<
			"grant_type" << "fb_exchange_token" <<
			"fb_exchange_token" << currentToken);

	KObject* access_token_data = kinky::social::oauth2::get("https://graph.facebook.com/oauth/access_token", params);

	delete params;
	return access_token_data;
}

kinky::lang::KObject* kinky::social::facebook::unserializeSignedRequest(string signedRequest) {
	KObject* ret = NULL;
	size_t dot = signedRequest.find('.');
	string encSig = signedRequest.substr(0, dot);
	string payload = signedRequest.substr(dot + 1);
	kinky::utils::replaceAll(encSig, "-", "+");
	kinky::utils::replaceAll(encSig, "_", "/");
	kinky::utils::replaceAll(payload, "-", "+");
	kinky::utils::replaceAll(payload, "_", "/");

	istringstream in(encSig);
	ostringstream out;
	kinky::utils::base64_decode(in, out);
	string sig = out.str();

	in.str(payload);
	out.str("");
	kinky::utils::base64_decode(in, out);
	string b64(out.str());

	ret = KJSON::parse(b64);

	string algo = (string) (*ret)["algorithm"];
	std::transform(algo.begin(), algo.begin() + 1, algo.begin(), ::toupper);
	if (algo != "HMAC-SHA256") {
		throw new KRestException(KHTTPResponse::BAD_REQUEST, "Unknow algorithm. Expected HMAC-SHA256");
	}

	return ret;

	/*function parse_signed_request($signed_request, $secret) {
		list ( $encoded_sig, $payload ) = explode ( '.', $signed_request, 2 );

		// decode the data
		$sig = base64_url_decode ( $encoded_sig );
		$data = json_decode ( base64_url_decode ( $payload ), true );

		if (strtoupper ( $data ['algorithm'] ) !== '') {
			error_log ( 'Unknown algorithm. Expected HMAC-SHA256' );
			return null;
		}

		// check sig
		$expected_sig = hash_hmac ( 'sha256', $payload, $secret, $raw = true );
		if ($sig !== $expected_sig) {
			error_log ( 'Bad Signed JSON signature!' );
			return null;
		}

		return $data;
	}

	function base64_url_decode($input) {
	  return base64_decode(strtr($input, '-_', '+/')	);
	}
	*/
}

void kinky::social::facebook::updateCache(string oguri) {
	string uri("/?scrape=true&id=");
	/*ostringstream uriss;
	kinky::utils::url_encode(oguri, uriss);
	uri.insert(uri.length(), uriss.str());*/
	uri.insert(uri.length(), oguri);

	int responseCode = 0;
	string* replyStr = kinky::rest::http::curl(&responseCode, KHTTPRequest::POST, "https://graph.facebook.com", uri, NULL, NULL, NULL, false);
	delete replyStr;
}

