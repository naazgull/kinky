#include <kinky/social/Twitter2.h>

/* Deprecated: use kinky::social::common::curlWriter instead */
int kinky::social::twitter2::curlWriter(char *data, size_t size, size_t nmemb, std::string *buffer) {
	return kinky::social::common::curlWriter(data, size, nmemb, buffer);
}

kinky::lang::KObject* kinky::social::twitter2::get(const char* hash, const char* consumerKey, const char* accessToken, KObject* params) {
	string* url = new string("https://api.twitter.com/1.1");
	url->append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
			"oauth_consumer_key" << consumerKey <<
			"oauth_signature_method" << "HMAC-SHA1" <<
			"oauth_token" << accessToken <<
			"oauth_version" << "1.0" <<
			"oauth_nonce" << nonce->data() <<
			"oauth_timestamp" << timestamp->data();

	params->sort();

	KObject* profile_data = kinky::social::oauth2::get(url->data(), params);

	delete nonce;
	delete timestamp;
	delete url;

	return profile_data;
}

kinky::lang::KObject* kinky::social::twitter2::post(const char* hash, const char* consumerKey, const char* accessToken, KObject* params) {
	string* url = new string("https://api.twitter.com/1.1");
	url->append(hash);

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	(*params) <<
			"oauth_consumer_key" << consumerKey <<
			"oauth_signature_method" << "HMAC-SHA1" <<
			"oauth_token" << accessToken <<
			"oauth_version" << "1.0" <<
			"oauth_nonce" << nonce->data() <<
			"oauth_timestamp" << timestamp->data();

	params->sort();

	KObject* profile_data = kinky::social::oauth2::get(url->data(), params);

	delete nonce;
	delete timestamp;
	delete url;

	return profile_data;
}

kinky::lang::KObject* kinky::social::twitter2::getURL(const char* hash, KObject* params) {
	string url;
	url.append(hash);

	for (KObject::KObjAttributes::iterator it = params->begin(); it != params->end(); it++) {
		string key = (*it)->key;
		void* val = (*it)->value->pointed();

		string escaped_key = kinky::utils::KUrlEncodeRfc3986::encode(key);
		string escaped_value = kinky::utils::KUrlEncodeRfc3986::encode(static_cast<string*>(val)->data());

		if (url.find("?") != string::npos) {
			url.append("&");
		}
		else {
			url.append("?");
		}

		url.append(escaped_key);
		url.append("=");
		url.append(escaped_value);
	}


	KObject* profile_data = KRest::exec(KHTTPRequest::GET, "https://api.twitter.com/1", url, NULL, NULL);


	return profile_data;
}

pair<const char*, const char*>* kinky::social::twitter2::getOAuthToken(KObject* response, const char* consumerKey, const char* redirectURI, bool force_login) {
	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	KObject* params = KOBJECT(
			"oauth_nonce" << nonce->data() <<
			"oauth_callback" << (string(redirectURI) + (string(redirectURI).find('?') != string::npos ? '&' : '?') + "key=" + *nonce).data() <<
			"oauth_signature_method" << "HMAC-SHA1" <<
			"oauth_timestamp" << timestamp->data() <<
			"oauth_consumer_key" << consumerKey <<
			"oauth_version" << "1.0");

	// TODO Por vezes crasha na primeira utilizacao, porque sera?
	KObject* oauth_data = kinky::social::oauth2::post("https://api.twitter.com/oauth/request_token", params);

	delete timestamp;
	delete params;

	pair<const char*, const char*>* ret = NULL;
	if ((*oauth_data)["oauth_token"] != NULL_KOBJECT) {
		response->addHeader("Location", ("http://api.twitter.com/oauth/authenticate?"
				"oauth_token=" + *(oauth_data->getString("oauth_token")) + (force_login ? "&force_login=true" : "")).data());

		response->setStatus(KHTTPResponse::TEMPORARY_REDIRECT);
		ret = new pair<const char*, const char*>(nonce->data(), ((string) ((*oauth_data)["oauth_token_secret"])).data());
	}

	delete oauth_data;
	delete nonce;

	return ret;
}

kinky::lang::KObject* kinky::social::twitter2::getAccessToken(const char* consumerKey, const char* consumerSecret, const char* x_auth_username, const char* x_auth_password, const char* oauth_token) {

	const char* hash = "https://api.twitter.com/1.1";
	string url = hash;
	string signature_fields = "";

	string base_signature = "GET&";
	base_signature.append(kinky::utils::KUrlEncodeRfc3986::encode(url));
	base_signature.append("&");
	base_signature.append(kinky::utils::KUrlEncodeRfc3986::encode(signature_fields));

	/*
	 * Note that there are some flows, such as when obtaining a request token, where the token secret is not yet known. In this case, the signing key should consist of the percent encoded consumer secret followed by an ampersand character '&'.
	 */
	string secretSigningKey;
	secretSigningKey.append(consumerSecret);
	secretSigningKey.append("&");

	CHMAC_SHA1 objHMACSHA1;
	unsigned char hashed_signature[1024];
	memset(hashed_signature, 0, 1024);

	objHMACSHA1.HMAC_SHA1((unsigned char*) base_signature.c_str(), base_signature.length(), (unsigned char*) secretSigningKey.c_str(), secretSigningKey.length(), hashed_signature);

	istringstream in((const char*) hashed_signature);
	ostringstream out;
	kinky::utils::base64_encode(in, out);
	string encoded_hashed_signature = out.str();

	string escaped_encoded_hashed_signature = kinky::utils::KUrlEncodeRfc3986::encode(encoded_hashed_signature.data());

	string* nonce = new string();
	string* timestamp = new string();

	kinky::social::oauth1::fillNonceAndTimestamp(nonce, timestamp);

	KObject* params = new KObject();
	ostringstream oss;

	oss << "OAuth "
			"oauth_consumer_key=\"" << consumerKey << "\", " <<
			"oauth_nonce=\"" << *nonce << "\", " <<
			"oauth_signature=\"" << escaped_encoded_hashed_signature << "\", " <<
			"oauth_signature_method=\"" << "HMAC-SHA1" << "\", " <<
			"oauth_timestamp=\"" << *timestamp << "\", " <<
			"oauth_token=\"" << oauth_token << "\", " <<
			"oauth_version=\"1.0\"";

	params->addHeader("Authorization", oss.str());

	string s = oss.str();
	printf("%s\n",s.c_str());

	(*params) <<"x_auth_username" << x_auth_username << "\", " <<
				"x_auth_password" << x_auth_password << "\", " <<
				"x_auth_mode=\"client_auth\"";

	string s1 = oss.str();
	printf("%s\n",s1.c_str());

	KObject* oauth_data = kinky::social::oauth2::post("https://api.twitter.com/oauth/access_token", params);

	delete nonce;
	delete timestamp;
	delete params;

	return oauth_data;
}
