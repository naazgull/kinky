#include <kinky/social/OAuth1.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

kinky::lang::KObject* kinky::social::oauth1::get(const char* hash, KObject* params, const char* consumerSecret, const char* oauthToken) {
	string url = hash;

	string base_signature = "GET&";
	base_signature.append(kinky::utils::KUrlEncodeRfc3986::encode(url));
	base_signature.append("&");

	string fields = "";
	string oauth_fields = "";
	string signature_fields = "";

	bool first = true;
	bool first_fields = true;
	bool first_oauth = true;

	for (KObject::KObjAttributes::iterator it = params->begin(); it != params->end(); it++) {
		string key = (*it)->key;
		void* val = (*it)->value->pointed();

		string escaped_key = kinky::utils::KUrlEncodeRfc3986::encode(key);
		string escaped_value = kinky::utils::KUrlEncodeRfc3986::encode(static_cast<string*>(val)->data());

		if (!first) {
			signature_fields.append("&");
		}

		if (string(key).compare(0, 6, "oauth_") == 0 || string(key).compare(0, 7, "xoauth_") == 0) {
			if (!first_oauth) {
				oauth_fields.append(", ");
			}

			first_oauth = false;

			oauth_fields.append(key);
			oauth_fields.append("=\"");
			oauth_fields.append(escaped_value);
			oauth_fields.append("\"");
		}
		else {
			if (!first_fields) {
				fields.append("&");
			}

			first_fields = false;

			fields.append(escaped_key);
			fields.append("=");
			fields.append(escaped_value);
		}

		first = false;

		signature_fields.append(escaped_key);
		signature_fields.append("=");
		signature_fields.append(escaped_value);
	}

	base_signature.append(kinky::utils::KUrlEncodeRfc3986::encode(signature_fields));

	string secretSigningKey;
	secretSigningKey.append(consumerSecret);
	secretSigningKey.append("&");
	secretSigningKey.append(oauthToken);

	CHMAC_SHA1 objHMACSHA1;
	unsigned char hashed_signature[1024];

	memset(hashed_signature, 0, 1024);

	objHMACSHA1.HMAC_SHA1((unsigned char*) base_signature.c_str(), base_signature.length(), (unsigned char*) secretSigningKey.c_str(), secretSigningKey.length(), hashed_signature);

	istringstream in((const char*) hashed_signature);
	ostringstream out;
	kinky::utils::base64_encode(in, out);
	string encoded_hashed_signature = out.str();

	string escaped_encoded_hashed_signature = kinky::utils::KUrlEncodeRfc3986::encode(encoded_hashed_signature.data());

	oauth_fields.append(", oauth_signature=\"");
	oauth_fields.append(escaped_encoded_hashed_signature);
	oauth_fields.append("\"");

	string auth_header = string();
	auth_header.append("Authorization: OAuth ");
	auth_header.append(oauth_fields);

	struct curl_slist* headers = NULL;
	KObject::KObjHeaders* reqheaders = params->getHeaders();
	if (reqheaders != NULL) {
		for (KObject::KObjHeaders::iterator it = reqheaders->begin(); it != reqheaders->end(); it++) {
			ostringstream oss;
			string name(it->first);
			kinky::utils::prettify_header_name(name);
			oss << name << ": " << **(it->second);
			headers = curl_slist_append(headers, oss.str().data());
		}
	}

	headers = curl_slist_append(headers, auth_header.data());
	headers = curl_slist_append(headers, "Expect:");

	string buffer;
	char errorBuffer[CURL_ERROR_SIZE];

	CURL* curl = curl_easy_init();
	url.insert(url.length(), (fields.length() > 0 ? "?" + fields : ""));

	OpenSSL_add_all_algorithms();
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_URL, url.data());
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, social::common::curlWriter);

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);

	CURLcode result = curl_easy_perform(curl);

	if (result == CURLE_OK) {
		KObject* toReturn = kinky::social::common::parseResponse(curl, &buffer, params);

		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		return toReturn;
	}
	else {
		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		throw new KRestException(result, errorBuffer);
	}
}

kinky::lang::KObject* kinky::social::oauth1::post(const char* hash, KObject* params, const char* consumerSecret, const char* oauthToken, string* body) {
	string url = hash;

	string base_signature = "POST&";
	base_signature.append(kinky::utils::KUrlEncodeRfc3986::encode(url));
	base_signature.append("&");

	string fields = "";
	string oauth_fields = "";
	string signature_fields = "";

	bool first = true;
	bool first_fields = true;
	bool first_oauth = true;

	for (KObject::KObjAttributes::iterator it = params->begin(); it != params->end(); it++) {
		string key = (*it)->key;
		void* val = (*it)->value->pointed();

		string escaped_key = kinky::utils::KUrlEncodeRfc3986::encode(key);
		string escaped_value = kinky::utils::KUrlEncodeRfc3986::encode(static_cast<string*>(val)->data());

		if (!first) {
			signature_fields.append("&");
		}

		if (string(key).compare(0, 6, "oauth_") == 0 || string(key).compare(0, 7, "xoauth_") == 0) {
			if (!first_oauth) {
				oauth_fields.append(", ");
			}

			first_oauth = false;

			oauth_fields.append(key);
			oauth_fields.append("=\"");
			oauth_fields.append(escaped_value);
			oauth_fields.append("\"");
		}
		else {
			if (!first_fields) {
				fields.append("&");
			}

			first_fields = false;

			fields.append(escaped_key);
			fields.append("=");
			fields.append(escaped_value);
		}

		first = false;

		signature_fields.append(escaped_key);
		signature_fields.append("=");
		signature_fields.append(escaped_value);
	}

	base_signature.append(kinky::utils::KUrlEncodeRfc3986::encode(signature_fields));

	string secretSigningKey;
	secretSigningKey.append(consumerSecret);
	secretSigningKey.append("&");
	secretSigningKey.append(oauthToken);

	CHMAC_SHA1 objHMACSHA1;
	unsigned char hashed_signature[1024];

	memset(hashed_signature, 0, 1024);

	objHMACSHA1.HMAC_SHA1((unsigned char*) base_signature.c_str(), base_signature.length(), (unsigned char*) secretSigningKey.c_str(), secretSigningKey.length(), hashed_signature);

	istringstream in((const char*) hashed_signature);
	ostringstream out;
	kinky::utils::base64_encode(in, out);
	string encoded_hashed_signature = out.str();

	string escaped_encoded_hashed_signature = kinky::utils::KUrlEncodeRfc3986::encode(encoded_hashed_signature.data());

	oauth_fields.append(", oauth_signature=\"");
	oauth_fields.append(escaped_encoded_hashed_signature);
	oauth_fields.append("\"");

	string auth_header = string();
	auth_header.append("Authorization: OAuth ");
	auth_header.append(oauth_fields);

	string buffer;
	char errorBuffer[CURL_ERROR_SIZE];


	CURL* curl = curl_easy_init();

	OpenSSL_add_all_algorithms();
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_URL, url.data());
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_POST, 1);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, social::common::curlWriter);

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

	struct curl_slist* headers = NULL;

	KObject::KObjHeaders* reqheaders = params->getHeaders();
	if (reqheaders != NULL) {
		for (KObject::KObjHeaders::iterator it = reqheaders->begin(); it != reqheaders->end(); it++) {
			ostringstream oss;
			string name(it->first);
			kinky::utils::prettify_header_name(name);
			oss << name << ": " << **(it->second);
			headers = curl_slist_append(headers, oss.str().data());
		}
	}

	if (body == NULL || *body == "") {
		curl_easy_setopt(curl, CURLOPT_COPYPOSTFIELDS, fields.data());
		headers = curl_slist_append(headers, "Content-Type: application/x-www-form-urlencoded; charset=UTF-8");
	}
	else {
		curl_easy_setopt(curl, CURLOPT_COPYPOSTFIELDS, body->data());
		headers = curl_slist_append(headers, "Content-Type: application/json; charset=UTF-8");
	}

	headers = curl_slist_append(headers, auth_header.data());
	headers = curl_slist_append(headers, "Expect:");

	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	CURLcode result = curl_easy_perform(curl);

	if (result == CURLE_OK) {
		string* toParse = new string(buffer);
		KObject* toReturn = kinky::social::common::parseResponse(curl, toParse, params);

		delete toParse;

		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		return toReturn;
	}
	else {
		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
		EVP_cleanup();

		throw new KRestException(result, errorBuffer);
	}
}

void kinky::social::oauth1::fillNonceAndTimestamp(string* nonce, string* timestamp) {
	char szTime[1024];
	char szRand[1024];
	memset(szTime, 0, 1024);
	memset(szRand, 0, 1024);

	srand(time(NULL));

	sprintf(szRand, "%x", rand() % 1000);
	sprintf(szTime, "%ld", time(NULL));

	nonce->assign(szTime);
	nonce->append(szRand);
	timestamp->assign(szTime);
}
