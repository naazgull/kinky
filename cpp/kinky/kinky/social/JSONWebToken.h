#pragma once

#include <curl/curl.h>
#include <kinky/rest/KRestException.h>
#include <kinky/social/Common.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest;

	namespace social {

		namespace jsonwebtoken {

			void formJWT(string header_alg, KObject* claimset_params, string privatekey_file_location, string privatekey_file_pass, string* ret);

			void signWithRsaSha256(string input, string private_key_location, string private_key_pass, string* ret);

			size_t curlTextWriter(char *data, size_t size, size_t nmemb, string *buffer);

			KObject* getAccessToken(string grant_type, string assertion, string content_type, string uri);

		}
	}
}
