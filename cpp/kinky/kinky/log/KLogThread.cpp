#include <kinky/log/KLogThread.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <stdio.h>

kinky::log::KLogStream* kinky::io::k_logger = NULL;
kinky::log::KLogStream* kinky::io::out= NULL;

kinky::log::KLogThread::KLogThread() :
		KThread<vector<ostream*> >(new vector<ostream*>()), fds(new vector<int>()) {
}

kinky::log::KLogThread::~KLogThread() {
	delete this->buffer;
	delete this->fds;
}

void kinky::log::KLogThread::setBuffer(KLogStream* buf) {
	this->buffer = buf;
}

void kinky::log::KLogThread::addStream(ostream* stream) {
	pthread_mutex_lock(this->mutex);
	this->getTarget()->push_back(stream);
	pthread_mutex_unlock(this->mutex);
}

void kinky::log::KLogThread::addSocket(int fd) {
	pthread_mutex_lock(this->mutex);
	this->fds->push_back(fd);
	pthread_mutex_unlock(this->mutex);
}

void kinky::log::KLogThread::printm(const char* prefix) {
}

void kinky::log::KLogThread::execute() {
	string* b = NULL;

	while(true) {
		this->wait();
		pthread_mutex_lock(this->mutex);
		(*this->buffer->getBuffer()) << flush;
		b = new string(this->buffer->getBuffer()->str().data());
		this->buffer->getBuffer()->str("");
		pthread_mutex_unlock(this->mutex);

		for (vector<ostream*>::iterator it = this->data->begin(); it != this->data->end(); it++) {
			(**it) << *b  << flush;
		}

		for (vector<int>::iterator it = this->fds->begin(); it != this->fds->end(); it++) {
			try {
				string rep(b->data());
				int opts = fcntl(*it, F_GETFL);
				opts ^= O_NONBLOCK;
				fcntl(*it, F_SETFL, opts);

				while (rep.length() != 0) {
					size_t size = write(*it, rep.data(), rep.length());
					if (size == (size_t) -1) {
						shutdown(*it, SHUT_WR);
						close(*it);
						this->fds->erase(it);
						it--;
						break;
					}
					rep.erase(0, size);
				}
			}
			catch (exception& e) {
			}
		}
		delete b;
		b = NULL;
		cout << flush;
	}
}

kinky::log::KLogStream::KLogStream(KLogThread* loggerParam) : logger(loggerParam) {
	this->msg = new ostringstream();
}

kinky::log::KLogStream::~KLogStream() {
}

ostringstream* kinky::log::KLogStream::getBuffer() {
	return this->msg;
}

void kinky::log::KLogStream::addStream(ostream* stream) {
	this->logger->addStream(stream);
}

void kinky::log::KLogStream::addSocket(int fd) {
	this->logger->addSocket(fd);
}

kinky::log::KLogRequestException::KLogRequestException() {
}

kinky::log::KLogRequestException::~KLogRequestException() throw () {

}

string kinky::log::KLogRequestException::what() throw () {
	return "";
}
