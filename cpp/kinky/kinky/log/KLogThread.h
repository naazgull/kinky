#pragma once

#include <kinky/rest/thread/KThread.h>
#include <time.h>
#define kout (*kinky::io::out)

namespace kinky {

	using namespace rest::thread;
	using namespace rest;

	namespace log {

		class KLogStream;

		class KLogThread: public KThread< vector<ostream*> >  {
			public:
				KLogThread();
				virtual ~KLogThread();

				void setBuffer(KLogStream* buf);

				void addStream(ostream* stream);

				void addSocket(int fd);

				virtual void execute();

				virtual void printm(const char* prefix = NULL);

				KLogStream* buffer;
				vector<int>* fds;

		};

		class KLogStream {
			public:
				KLogStream(KLogThread* logger);
				virtual ~KLogStream();

				template<typename T>
				inline KLogStream& operator<<(const T& t) {
					pthread_mutex_lock(this->logger->mutex);
					(*this->msg) << t;
					pthread_mutex_unlock(this->logger->mutex);
					return *this;
				}
				inline KLogStream& operator<<(std::ostream& (*t)(std::ostream&)) {
					pthread_mutex_lock(this->logger->mutex);
					(*this->msg) << t;
					pthread_mutex_unlock(this->logger->mutex);
					this->logger->exec();
					return *this;
				}
				inline KLogStream& operator<<(std::ios& (*t)(std::ios&)) {
					pthread_mutex_lock(this->logger->mutex);
					(*this->msg) << t;
					pthread_mutex_unlock(this->logger->mutex);
					return *this;
				}
				inline KLogStream& operator<<(std::ios_base& (*t)(std::ios_base&)) {
					pthread_mutex_lock(this->logger->mutex);
					(*this->msg) << t;
					pthread_mutex_unlock(this->logger->mutex);
					return *this;
				}

				ostringstream* getBuffer();

				void addStream(ostream* stream);

				void addSocket(int fd);

				KLogThread* logger;
				std::ostringstream* msg;
		};

		class KLogRequestException : public exception {
			public:
				KLogRequestException();
				virtual ~KLogRequestException() throw ();

				virtual string what() throw ();

		};
	}

	namespace io {
		extern kinky::log::KLogStream* k_logger;
		extern kinky::log::KLogStream* out;
	}
}
