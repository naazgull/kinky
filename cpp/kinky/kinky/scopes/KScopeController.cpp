#include <kinky/scopes/KScopeController.h>
#include <kinky/rest/KRestAPI.h>

kinky::scopes::KScopeController::KScopeController() {
}

kinky::scopes::KScopeController::~KScopeController() {
}

void kinky::scopes::KScopeController::addPermissions(KRestOperation* op, string href) {

}

bool kinky::scopes::KScopeController::hasPermission(KRestOperation* op, string url, int directive, KToken* token, string resource_owner, string not_owner_scope, string perm_prefix) {
	bool ret = false;
	time_t now = time(NULL);

	if (token->getExpirationDate() > 0 && now > token->getExpirationDate()) {
		throw new KRestException(KHTTPResponse::UNAUTHORIZED, "access token has expired.");
	}

	string method = KHTTPRequest::getDirective(directive);
	KObject* allowedScopes = (*op->getPermissions())[method];

	if (allowedScopes == NULL_KOBJECT) {
		ret = true;
	}
	else if (token->getScopes() == NULL || token->getScopes()->length() == 0) {
		ret = false;
	}
	else {
		string scopes = *token->getScopes();
		for (KObject::KObjAttributes::iterator it = allowedScopes->begin(); it != allowedScopes->end(); it++) {
			string allowed = (string) (*allowedScopes)[(*it)->key];
			if (scopes.find(allowed) != string::npos) {
				ret = true;
			}
		}
	}

	if (ret) {
		string uid;
		KScopeController::extractMe(token, &uid, perm_prefix);
		if (resource_owner.length() != 0 && token->getScopes()->find(not_owner_scope) == string::npos && uid != resource_owner) {
			ret = false;
		}
	}
	return ret;
}


kinky::lang::KObject* kinky::scopes::KScopeController::getScopesFor(KRestOperation* op) {
	return op->getPermissions();
}

void kinky::scopes::KScopeController::getScopesFor(string userID, string* scopes) {
}

bool kinky::scopes::KScopeController::findScope(KToken* token, string scopes) {
	if (token->getScopes() == NULL) {
		return false;
	}

	string tscopes = *token->getScopes();

	stringstream iss;
	iss << scopes;

	string line;
	while (iss.good()) {
		std::getline(iss, line, ',');
		string scope(line.data());

		if (tscopes.find(scope) != string::npos) {
			return true;
		}
	}
	return false;
}

bool kinky::scopes::KScopeController::scopeToRegexp(KToken* token, ostream& oss) {
	if (token->getScopes() == NULL) {
		return false;
	}

	string tscopes = token->getScopes()->substr(token->getScopes()->find(":") + 1);
	stringstream iss;
	iss << tscopes;

	string line;
	bool first = true;
	while (iss.good()) {
		std::getline(iss, line, ',');
		string scope(line.data());
		if (!first) {
			oss << "|" << flush;
		}
		first = false;
		oss << scope << flush;
	}
	return true;
}

bool kinky::scopes::KScopeController::isValid(KToken* token) {
	return token->isValid();
}

void kinky::scopes::KScopeController::setAPI(kinky::rest::KRestAPI* api) {
	this->api = api;
}

kinky::rest::KRestAPI* kinky::scopes::KScopeController::getAPI() {
	return this->api;
}

bool kinky::scopes::KScopeController::extractMe(kinky::oauth::KToken* token, string* me, string prefix) {
	if (token->getUserID() == NULL) {
		return false;
	}

	stringstream iss;
	iss << *token->getUserID();

	string mid;
	string line;
	while (iss.good()) {
		std::getline(iss, line, ',');
		string module(line.data());
		string id(module.substr(module.find(":") + 1));
		module.assign(module.substr(0, module.find(":")));
		if (module == prefix) {
			me->assign(id.data());
		}
		else if (module == "default") {
			mid.assign(id.data());
		}
	}
	if (me->length() == 0) {
		me->assign(mid.data());
	}

	return me->length() != 0;
}

