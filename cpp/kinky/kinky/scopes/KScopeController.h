#pragma once

#include <time.h>
#include <string>
#include <iostream>
#include <kinky/lang/KObject.h>
#include <kinky/rest/KRestOperation.h>
#include <kinky/oauth/KToken.h>
#include <kinky/utils/SHA1.h>

using namespace std;
using namespace __gnu_cxx;

namespace kinky {

	using namespace lang;
	using namespace rest;
	using namespace oauth;

	namespace rest {
		class KRestAPI;
	}

	namespace scopes {

		class  KScopeController {
			public:
				 KScopeController();
				 virtual ~KScopeController();

				 virtual void addPermissions(KRestOperation* op, string href);

				 virtual bool hasPermission(KRestOperation* op, string url, int directive, KToken* token, string resource_owner = "", string not_owner_scope = "_", string perm_prefix = "default");

				 virtual kinky::lang::KObject* getScopesFor(KRestOperation* op);

				 virtual void getScopesFor(string userID, string* scopes);

				 bool findScope(KToken* token, string scopes);

				 bool scopeToRegexp(KToken* token, ostream& oss);

				 virtual bool isValid(KToken* token);

				 void setAPI(kinky::rest::KRestAPI* api);

				 kinky::rest::KRestAPI* getAPI();

				 static string encryptClientSecret(string& password);

				 static bool extractMe(kinky::oauth::KToken* token, string* me, string prefix = "");

			private:
				 kinky::rest::KRestAPI* api;

		};
	}
}
