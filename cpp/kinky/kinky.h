#pragma once

#ifndef KINKY_CURL_TIMEOUT
	#define KINKY_CURL_TIMEOUT 5
#endif

#include <stddef.h>
#include <iostream>
#include <time.h>

#include <kinky/memory/KMemoryManager.h>

#include <kinky/oauth/KToken.h>
#include <kinky/oauth/KOAuthController.h>
#include <kinky/oauth/controllers/AuthorizeController.h>
#include <kinky/oauth/controllers/LoginController.h>
#include <kinky/oauth/controllers/TokenController.h>

#include <kinky/rest/http/KHTTPRequest.h>
#include <kinky/rest/http/KHTTPResponse.h>
#include <kinky/rest/http/KHTTPServer.h>
#include <kinky/rest/http/KCurl.h>
#include <kinky/rest/http/KHTTPSocket.h>
#include <kinky/rest/http/ssl/KHTTPSServer.h>
#include <kinky/rest/http/ssl/KHTTPSSocket.h>

#include <kinky/rest/net/KClientSocketThread.h>
#include <kinky/rest/net/KSocket.h>
#include <kinky/rest/net/KSocketServer.h>
#include <kinky/rest/net/ssl/KSSLSocket.h>
#include <kinky/rest/net/ssl/KSSLSocketServer.h>

#include <kinky/rest/thread/KThread.h>

#include <kinky/manip.h>
#include <kinky/lang/KObject.h>
#include <kinky/lang/KJSON.h>
#include <kinky/lang/KXML.h>
#include <kinky/lang/KProperties.h>
#include <kinky/lang/KRestArg.h>

#include <kinky/rest/KRest.h>
#include <kinky/rest/KRestAPI.h>
#include <kinky/rest/KRestOperation.h>
#include <kinky/rest/KRestException.h>
#include <kinky/rest/KRestAssynchronousRequest.h>
#include <kinky/rest/KDocRoot.h>
#include <kinky/rest/KWRMLRelationRoot.h>
#include <kinky/rest/KWRMLSchemaRoot.h>

#include <kinky/utils/KLocale.h>
#include <kinky/utils/KDateTime.h>
#include <kinky/utils/KSendMail.h>
#include <kinky/utils/KXORCipher.h>
#include <kinky/utils/KQuotedPrintable.h>
#include <kinky/utils/KFileSystem.h>

#include <kinky/media/KImage.h>
#include <kinky/media/KBrowser.h>

#include <kinky/social/Common.h>
#include <kinky/social/OAuth1.h>
#include <kinky/social/OAuth2.h>
#include <kinky/social/OpenId.h>
#include <kinky/social/Facebook.h>
#include <kinky/social/Twitter.h>
#include <kinky/social/Twitter2.h>
#include <kinky/social/Youtube.h>
#include <kinky/social/Yahoo.h>
#include <kinky/social/Flickr.h>
#include <kinky/social/Tumblr.h>
#include <kinky/social/Linkedin.h>

#include <kinky/social/controllers/FacebookSelf.h>
#include <kinky/social/controllers/FlickrSelf.h>
#include <kinky/social/controllers/GoogleSelf.h>
#include <kinky/social/controllers/InstagramSelf.h>
#include <kinky/social/controllers/TumblrSelf.h>
#include <kinky/social/controllers/TwitterSelf.h>
#include <kinky/social/controllers/YahooSelf.h>
#include <kinky/social/controllers/LinkedinSelf.h>

#include <kinky/cloudstorage/Box.h>
#include <kinky/cloudstorage/Copy.h>
#include <kinky/cloudstorage/Dropbox.h>
#include <kinky/cloudstorage/Bitbucket.h>

#include <kinky/cloudstorage/controllers/BoxSelf.h>
#include <kinky/cloudstorage/controllers/CopySelf.h>
#include <kinky/cloudstorage/controllers/DropboxSelf.h>
#include <kinky/cloudstorage/controllers/BitbucketSelf.h>

#include <kinky/log/KLogThread.h>

