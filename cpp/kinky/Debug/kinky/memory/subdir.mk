################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../kinky/memory/KMaps.cpp \
../kinky/memory/KMemoryManager.cpp 

OBJS += \
./kinky/memory/KMaps.o \
./kinky/memory/KMemoryManager.o 

CPP_DEPS += \
./kinky/memory/KMaps.d \
./kinky/memory/KMemoryManager.d 


# Each subdirectory must supply rules for building sources it contributes
kinky/memory/%.o: ../kinky/memory/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DLOG -DLOG_INTERNAL -DUSE_SPARSEHASH -DMOD_DOCS -DDEBUG_KINKY -DDEBUG_MEMORY -DLOG_COLORS -DKINKY_JSON_INTERNAL -DSYSTEM_SPARSEHASH -UDEBUG_KINKY -UDEBUG_MEMORY -UUSE_SPARSEHASH -UMOD_DOCS -I../ -O0 -g3 -p -pg -Wall -c -fmessage-length=0 -fno-omit-frame-pointer -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


