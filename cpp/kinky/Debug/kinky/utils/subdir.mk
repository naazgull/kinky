################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../kinky/utils/HMAC_SHA1.cpp \
../kinky/utils/KDateTime.cpp \
../kinky/utils/KFileSystem.cpp \
../kinky/utils/KLocale.cpp \
../kinky/utils/KQuotedPrintable.cpp \
../kinky/utils/KSendMail.cpp \
../kinky/utils/KUrlEncodeRfc3986.cpp \
../kinky/utils/KXORCipher.cpp \
../kinky/utils/SHA1.cpp 

OBJS += \
./kinky/utils/HMAC_SHA1.o \
./kinky/utils/KDateTime.o \
./kinky/utils/KFileSystem.o \
./kinky/utils/KLocale.o \
./kinky/utils/KQuotedPrintable.o \
./kinky/utils/KSendMail.o \
./kinky/utils/KUrlEncodeRfc3986.o \
./kinky/utils/KXORCipher.o \
./kinky/utils/SHA1.o 

CPP_DEPS += \
./kinky/utils/HMAC_SHA1.d \
./kinky/utils/KDateTime.d \
./kinky/utils/KFileSystem.d \
./kinky/utils/KLocale.d \
./kinky/utils/KQuotedPrintable.d \
./kinky/utils/KSendMail.d \
./kinky/utils/KUrlEncodeRfc3986.d \
./kinky/utils/KXORCipher.d \
./kinky/utils/SHA1.d 


# Each subdirectory must supply rules for building sources it contributes
kinky/utils/%.o: ../kinky/utils/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DLOG -DLOG_INTERNAL -DUSE_SPARSEHASH -DMOD_DOCS -DDEBUG_KINKY -DDEBUG_MEMORY -DLOG_COLORS -DKINKY_JSON_INTERNAL -DSYSTEM_SPARSEHASH -UDEBUG_KINKY -UDEBUG_MEMORY -UUSE_SPARSEHASH -UMOD_DOCS -I../ -O0 -g3 -p -pg -Wall -c -fmessage-length=0 -fno-omit-frame-pointer -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


