################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../kinky/rest/net/ssl/KClientSSLSocketThread.cpp \
../kinky/rest/net/ssl/KClientSSLSocketThreadPool.cpp \
../kinky/rest/net/ssl/KSSLSocket.cpp \
../kinky/rest/net/ssl/KSSLSocketServer.cpp 

OBJS += \
./kinky/rest/net/ssl/KClientSSLSocketThread.o \
./kinky/rest/net/ssl/KClientSSLSocketThreadPool.o \
./kinky/rest/net/ssl/KSSLSocket.o \
./kinky/rest/net/ssl/KSSLSocketServer.o 

CPP_DEPS += \
./kinky/rest/net/ssl/KClientSSLSocketThread.d \
./kinky/rest/net/ssl/KClientSSLSocketThreadPool.d \
./kinky/rest/net/ssl/KSSLSocket.d \
./kinky/rest/net/ssl/KSSLSocketServer.d 


# Each subdirectory must supply rules for building sources it contributes
kinky/rest/net/ssl/%.o: ../kinky/rest/net/ssl/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DLOG -DLOG_INTERNAL -DUSE_SPARSEHASH -DMOD_DOCS -DDEBUG_KINKY -DDEBUG_MEMORY -DLOG_COLORS -DKINKY_JSON_INTERNAL -DSYSTEM_SPARSEHASH -UDEBUG_KINKY -UDEBUG_MEMORY -UUSE_SPARSEHASH -UMOD_DOCS -I../ -O0 -g3 -p -pg -Wall -c -fmessage-length=0 -fno-omit-frame-pointer -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


