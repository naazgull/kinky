################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../kinky/rest/net/KClientSocketThread.cpp \
../kinky/rest/net/KClientSocketThreadPool.cpp \
../kinky/rest/net/KSocket.cpp \
../kinky/rest/net/KSocketServer.cpp \
../kinky/rest/net/KSocketTimeoutException.cpp 

OBJS += \
./kinky/rest/net/KClientSocketThread.o \
./kinky/rest/net/KClientSocketThreadPool.o \
./kinky/rest/net/KSocket.o \
./kinky/rest/net/KSocketServer.o \
./kinky/rest/net/KSocketTimeoutException.o 

CPP_DEPS += \
./kinky/rest/net/KClientSocketThread.d \
./kinky/rest/net/KClientSocketThreadPool.d \
./kinky/rest/net/KSocket.d \
./kinky/rest/net/KSocketServer.d \
./kinky/rest/net/KSocketTimeoutException.d 


# Each subdirectory must supply rules for building sources it contributes
kinky/rest/net/%.o: ../kinky/rest/net/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DLOG -DLOG_INTERNAL -DUSE_SPARSEHASH -DMOD_DOCS -DDEBUG_KINKY -DDEBUG_MEMORY -DLOG_COLORS -DKINKY_JSON_INTERNAL -DSYSTEM_SPARSEHASH -UDEBUG_KINKY -UDEBUG_MEMORY -UUSE_SPARSEHASH -UMOD_DOCS -I../ -O0 -g3 -p -pg -Wall -c -fmessage-length=0 -fno-omit-frame-pointer -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


