################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../kinky/rest/KDocRoot.cpp \
../kinky/rest/KRest.cpp \
../kinky/rest/KRestAPI.cpp \
../kinky/rest/KRestAssynchronousRequest.cpp \
../kinky/rest/KRestException.cpp \
../kinky/rest/KRestOperation.cpp \
../kinky/rest/KWRMLRelationRoot.cpp \
../kinky/rest/KWRMLSchemaRoot.cpp 

OBJS += \
./kinky/rest/KDocRoot.o \
./kinky/rest/KRest.o \
./kinky/rest/KRestAPI.o \
./kinky/rest/KRestAssynchronousRequest.o \
./kinky/rest/KRestException.o \
./kinky/rest/KRestOperation.o \
./kinky/rest/KWRMLRelationRoot.o \
./kinky/rest/KWRMLSchemaRoot.o 

CPP_DEPS += \
./kinky/rest/KDocRoot.d \
./kinky/rest/KRest.d \
./kinky/rest/KRestAPI.d \
./kinky/rest/KRestAssynchronousRequest.d \
./kinky/rest/KRestException.d \
./kinky/rest/KRestOperation.d \
./kinky/rest/KWRMLRelationRoot.d \
./kinky/rest/KWRMLSchemaRoot.d 


# Each subdirectory must supply rules for building sources it contributes
kinky/rest/%.o: ../kinky/rest/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DLOG -DLOG_INTERNAL -DUSE_SPARSEHASH -DMOD_DOCS -DDEBUG_KINKY -DDEBUG_MEMORY -DLOG_COLORS -DKINKY_JSON_INTERNAL -DSYSTEM_SPARSEHASH -UDEBUG_KINKY -UDEBUG_MEMORY -UUSE_SPARSEHASH -UMOD_DOCS -I../ -O0 -g3 -p -pg -Wall -c -fmessage-length=0 -fno-omit-frame-pointer -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


