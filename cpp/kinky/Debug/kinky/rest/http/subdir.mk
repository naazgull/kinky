################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../kinky/rest/http/KCurl.cpp \
../kinky/rest/http/KHTTPRequest.cpp \
../kinky/rest/http/KHTTPResponse.cpp \
../kinky/rest/http/KHTTPServer.cpp \
../kinky/rest/http/KHTTPSocket.cpp 

OBJS += \
./kinky/rest/http/KCurl.o \
./kinky/rest/http/KHTTPRequest.o \
./kinky/rest/http/KHTTPResponse.o \
./kinky/rest/http/KHTTPServer.o \
./kinky/rest/http/KHTTPSocket.o 

CPP_DEPS += \
./kinky/rest/http/KCurl.d \
./kinky/rest/http/KHTTPRequest.d \
./kinky/rest/http/KHTTPResponse.d \
./kinky/rest/http/KHTTPServer.d \
./kinky/rest/http/KHTTPSocket.d 


# Each subdirectory must supply rules for building sources it contributes
kinky/rest/http/%.o: ../kinky/rest/http/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DLOG -DLOG_INTERNAL -DUSE_SPARSEHASH -DMOD_DOCS -DDEBUG_KINKY -DDEBUG_MEMORY -DLOG_COLORS -DKINKY_JSON_INTERNAL -DSYSTEM_SPARSEHASH -UDEBUG_KINKY -UDEBUG_MEMORY -UUSE_SPARSEHASH -UMOD_DOCS -I../ -O0 -g3 -p -pg -Wall -c -fmessage-length=0 -fno-omit-frame-pointer -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


