#! /bin/bash

### BEGIN INIT INFO
# Provides:             kinky
# Default-Start:        2 3 4 5
# Default-Stop:         1
# Short-Description:    Kinky
### END INIT INFO

pushd /etc/kinky/scripts-enabled &> /dev/null

if [ "$2" != "" ]
then
        case "$1" in
          run)
                echo -n "Running app $2 "

                ./$2.sh
                ;;
          start)
                echo -n "Starting app $2 "

                ./$2.sh &> /dev/null &
                echo "DONE"
                ;;
          stop)
                PORT=$(more $2.sh)
                PORT=${PORT##*INSTANCEPORT=}
                PORT=${PORT%%[^[:digit:]]*}

                echo -n "Stopping app $2 "

                fuser -n tcp $PORT -k -15 -s
                echo "DONE"
                ;;
        esac
        popd &> /dev/null

        exit 0
fi

case "$1" in
  start)
        echo "Starting all apps: "
        for FILE in *.sh
        do
                NAME=${FILE%%.sh}
                ./$FILE &> /dev/null &
                echo "  * app $NAME started"
        done
        echo "DONE"
        ;;
  stop)
        echo "Stopping all apps "
        for FILE in *.sh
        do
                NAME=${FILE%%.sh}

                PORT=$(more $FILE)
                PORT=${PORT##*INSTANCEPORT=}
                PORT=${PORT%%[^[:digit:]]*}

                fuser  -n tcp $PORT -k -15 -s
                echo "  * app $NAME stopped"
        done
        echo "DONE"
        ;;
esac
popd &> /dev/null

exit 0
