################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../kinky/cloudstorage/Bitbucket.cpp \
../kinky/cloudstorage/Box.cpp \
../kinky/cloudstorage/Copy.cpp \
../kinky/cloudstorage/Dropbox.cpp 

OBJS += \
./kinky/cloudstorage/Bitbucket.o \
./kinky/cloudstorage/Box.o \
./kinky/cloudstorage/Copy.o \
./kinky/cloudstorage/Dropbox.o 

CPP_DEPS += \
./kinky/cloudstorage/Bitbucket.d \
./kinky/cloudstorage/Box.d \
./kinky/cloudstorage/Copy.d \
./kinky/cloudstorage/Dropbox.d 


# Each subdirectory must supply rules for building sources it contributes
kinky/cloudstorage/%.o: ../kinky/cloudstorage/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DLOG -DLOG_INTERNAL -DUSE_SPARSEHASH -DMOD_DOCS -DDEBUG_KINKY -DDEBUG_MEMORY -DLOG_COLORS -DKINKY_JSON_INTERNAL -DSYSTEM_SPARSEHASH -UDEBUG_KINKY -UDEBUG_MEMORY -UUSE_SPARSEHASH -UMOD_DOCS -I../ -O0 -g3 -p -pg -Wall -c -fmessage-length=0 -fno-omit-frame-pointer -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


