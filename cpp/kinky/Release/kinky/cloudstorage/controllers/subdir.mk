################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../kinky/cloudstorage/controllers/BitbucketSelf.cpp \
../kinky/cloudstorage/controllers/BoxSelf.cpp \
../kinky/cloudstorage/controllers/CopySelf.cpp \
../kinky/cloudstorage/controllers/DropboxSelf.cpp 

OBJS += \
./kinky/cloudstorage/controllers/BitbucketSelf.o \
./kinky/cloudstorage/controllers/BoxSelf.o \
./kinky/cloudstorage/controllers/CopySelf.o \
./kinky/cloudstorage/controllers/DropboxSelf.o 

CPP_DEPS += \
./kinky/cloudstorage/controllers/BitbucketSelf.d \
./kinky/cloudstorage/controllers/BoxSelf.d \
./kinky/cloudstorage/controllers/CopySelf.d \
./kinky/cloudstorage/controllers/DropboxSelf.d 


# Each subdirectory must supply rules for building sources it contributes
kinky/cloudstorage/controllers/%.o: ../kinky/cloudstorage/controllers/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DLOG -DLOG_INTERNAL -DUSE_SPARSEHASH -DMOD_DOCS -DDEBUG_KINKY -DDEBUG_MEMORY -DLOG_COLORS -DKINKY_JSON_INTERNAL -DSYSTEM_SPARSEHASH -UDEBUG_KINKY -UDEBUG_MEMORY -UUSE_SPARSEHASH -UMOD_DOCS -I../ -O0 -g3 -p -pg -Wall -c -fmessage-length=0 -fno-omit-frame-pointer -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


