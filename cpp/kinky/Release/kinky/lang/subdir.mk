################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../kinky/lang/KJSON.cpp \
../kinky/lang/KObject.cpp \
../kinky/lang/KObjectHashMap.cpp \
../kinky/lang/KObjectNULLException.cpp \
../kinky/lang/KObjectStrMap.cpp \
../kinky/lang/KParseException.cpp \
../kinky/lang/KProperties.cpp \
../kinky/lang/KRestArg.cpp \
../kinky/lang/KXML.cpp \
../kinky/lang/block_allocator.cpp \
../kinky/lang/json.cpp 

OBJS += \
./kinky/lang/KJSON.o \
./kinky/lang/KObject.o \
./kinky/lang/KObjectHashMap.o \
./kinky/lang/KObjectNULLException.o \
./kinky/lang/KObjectStrMap.o \
./kinky/lang/KParseException.o \
./kinky/lang/KProperties.o \
./kinky/lang/KRestArg.o \
./kinky/lang/KXML.o \
./kinky/lang/block_allocator.o \
./kinky/lang/json.o 

CPP_DEPS += \
./kinky/lang/KJSON.d \
./kinky/lang/KObject.d \
./kinky/lang/KObjectHashMap.d \
./kinky/lang/KObjectNULLException.d \
./kinky/lang/KObjectStrMap.d \
./kinky/lang/KParseException.d \
./kinky/lang/KProperties.d \
./kinky/lang/KRestArg.d \
./kinky/lang/KXML.d \
./kinky/lang/block_allocator.d \
./kinky/lang/json.d 


# Each subdirectory must supply rules for building sources it contributes
kinky/lang/%.o: ../kinky/lang/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DLOG -DLOG_INTERNAL -DUSE_SPARSEHASH -DMOD_DOCS -DDEBUG_KINKY -DDEBUG_MEMORY -DLOG_COLORS -DKINKY_JSON_INTERNAL -DSYSTEM_SPARSEHASH -UDEBUG_KINKY -UDEBUG_MEMORY -UUSE_SPARSEHASH -UMOD_DOCS -I../ -O0 -g3 -p -pg -Wall -c -fmessage-length=0 -fno-omit-frame-pointer -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


