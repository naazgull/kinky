################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../kinky/social/controllers/BaseOAuth1AccessTokenController.cpp \
../kinky/social/controllers/BaseOAuth2AccessTokenController.cpp \
../kinky/social/controllers/FacebookSelf.cpp \
../kinky/social/controllers/FlickrSelf.cpp \
../kinky/social/controllers/GoogleSelf.cpp \
../kinky/social/controllers/InstagramSelf.cpp \
../kinky/social/controllers/LinkedinSelf.cpp \
../kinky/social/controllers/TumblrSelf.cpp \
../kinky/social/controllers/TwitterSelf.cpp \
../kinky/social/controllers/YahooSelf.cpp \
../kinky/social/controllers/YoutubeSelf.cpp 

OBJS += \
./kinky/social/controllers/BaseOAuth1AccessTokenController.o \
./kinky/social/controllers/BaseOAuth2AccessTokenController.o \
./kinky/social/controllers/FacebookSelf.o \
./kinky/social/controllers/FlickrSelf.o \
./kinky/social/controllers/GoogleSelf.o \
./kinky/social/controllers/InstagramSelf.o \
./kinky/social/controllers/LinkedinSelf.o \
./kinky/social/controllers/TumblrSelf.o \
./kinky/social/controllers/TwitterSelf.o \
./kinky/social/controllers/YahooSelf.o \
./kinky/social/controllers/YoutubeSelf.o 

CPP_DEPS += \
./kinky/social/controllers/BaseOAuth1AccessTokenController.d \
./kinky/social/controllers/BaseOAuth2AccessTokenController.d \
./kinky/social/controllers/FacebookSelf.d \
./kinky/social/controllers/FlickrSelf.d \
./kinky/social/controllers/GoogleSelf.d \
./kinky/social/controllers/InstagramSelf.d \
./kinky/social/controllers/LinkedinSelf.d \
./kinky/social/controllers/TumblrSelf.d \
./kinky/social/controllers/TwitterSelf.d \
./kinky/social/controllers/YahooSelf.d \
./kinky/social/controllers/YoutubeSelf.d 


# Each subdirectory must supply rules for building sources it contributes
kinky/social/controllers/%.o: ../kinky/social/controllers/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DLOG -DLOG_INTERNAL -DUSE_SPARSEHASH -DMOD_DOCS -DDEBUG_KINKY -DDEBUG_MEMORY -DLOG_COLORS -DKINKY_JSON_INTERNAL -DSYSTEM_SPARSEHASH -UDEBUG_KINKY -UDEBUG_MEMORY -UUSE_SPARSEHASH -UMOD_DOCS -I../ -O0 -g3 -p -pg -Wall -c -fmessage-length=0 -fno-omit-frame-pointer -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


