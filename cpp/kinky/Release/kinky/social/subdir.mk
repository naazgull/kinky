################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../kinky/social/Common.cpp \
../kinky/social/Facebook.cpp \
../kinky/social/Flickr.cpp \
../kinky/social/JSONWebToken.cpp \
../kinky/social/KSocialTokenExpiredException.cpp \
../kinky/social/Linkedin.cpp \
../kinky/social/OAuth1.cpp \
../kinky/social/OAuth2.cpp \
../kinky/social/OpenId.cpp \
../kinky/social/Tumblr.cpp \
../kinky/social/Twitter.cpp \
../kinky/social/Twitter2.cpp \
../kinky/social/Yahoo.cpp \
../kinky/social/Youtube.cpp 

OBJS += \
./kinky/social/Common.o \
./kinky/social/Facebook.o \
./kinky/social/Flickr.o \
./kinky/social/JSONWebToken.o \
./kinky/social/KSocialTokenExpiredException.o \
./kinky/social/Linkedin.o \
./kinky/social/OAuth1.o \
./kinky/social/OAuth2.o \
./kinky/social/OpenId.o \
./kinky/social/Tumblr.o \
./kinky/social/Twitter.o \
./kinky/social/Twitter2.o \
./kinky/social/Yahoo.o \
./kinky/social/Youtube.o 

CPP_DEPS += \
./kinky/social/Common.d \
./kinky/social/Facebook.d \
./kinky/social/Flickr.d \
./kinky/social/JSONWebToken.d \
./kinky/social/KSocialTokenExpiredException.d \
./kinky/social/Linkedin.d \
./kinky/social/OAuth1.d \
./kinky/social/OAuth2.d \
./kinky/social/OpenId.d \
./kinky/social/Tumblr.d \
./kinky/social/Twitter.d \
./kinky/social/Twitter2.d \
./kinky/social/Yahoo.d \
./kinky/social/Youtube.d 


# Each subdirectory must supply rules for building sources it contributes
kinky/social/%.o: ../kinky/social/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DLOG -DLOG_INTERNAL -DUSE_SPARSEHASH -DMOD_DOCS -DDEBUG_KINKY -DDEBUG_MEMORY -DLOG_COLORS -DKINKY_JSON_INTERNAL -DSYSTEM_SPARSEHASH -UDEBUG_KINKY -UDEBUG_MEMORY -UUSE_SPARSEHASH -UMOD_DOCS -I../ -O0 -g3 -p -pg -Wall -c -fmessage-length=0 -fno-omit-frame-pointer -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


