function KRestClientForm(parent, hash, service) {
	if (parent != null) {
		KShellPart.call(this, parent, hash, service);
	}
}

KSystem.include([
	'KInput',
	'KCombo',
	'KPanel',
	'KButton',
	'KText',
	'KFileDropPanel',
	'KLink',
	'KShellPart',
	'KRestClientSchemaForm'
], function() {
	KRestClientForm.prototype = new KShellPart();
	KRestClientForm.prototype.constructor = KRestClientForm;
	
	KRestClientForm.prototype.onLoad = function(data) {
		KSystem.merge(this.data, data);
// this.kinky.get(this, this.data.schemaService, {}, 'onLoadSchema');
		this.draw();
	};
	
	KRestClientForm.prototype.onLoadSchema = function(data) {
		KSystem.merge(this.data, data);
		this.draw();
	};
	
	KRestClientForm.prototype.draw = function() {
		this.setTitle('<div style="font-size: 24px; margin-bottom: 25px;">' + this.data.href + '</div>', true);
		
		var customFields = new KRestClientSchemaForm(this, this.data.href, this.data.href, this.data.schemaService);
		{
			customFields.hash = '/custom-fields';
			customFields.data.relations = this.data.relations;
		}
		this.appendChild(customFields);
		
		var descriptionText = new KText(this);
		{
			descriptionText.hash = '/relation-description';
			descriptionText.setTitle('Description');
			descriptionText.setStyle({
				padding : '5px',
				borderRadius : '5px 5px 5px 5px',
				backgroundColor : '#eeeeee',
				border : '1px solid #242424',
				width : '457px',
				clear : 'both',
				marginRight : '10px',
				marginBottom : '10px',
				cssFloat : 'left'
			});
			descriptionText.setText('--');
		}
		this.appendChild(descriptionText);
		
		var relationLinks = new KPanel(this);
		{
			relationLinks.hash = '/relation-links';
			relationLinks.loadService = true;
			relationLinks.setTitle('Links');
			relationLinks.setStyle({
				padding : '5px',
				borderRadius : '5px 5px 5px 5px',
				backgroundColor : '#eeeeee',
				border : '1px solid #242424',
				width : '457px',
				clear : 'both',
				marginRight : '10px',
				cssFloat : 'left'
			});
			
			relationLinks.load = function() {
				this.kinky.get(this, this.parent.data.schemaService.replace('wrml-schemas', 'wrml-links'), {});
			};
			
			relationLinks.onLoad = function(data) {
				var hasLink = false;
				for ( var relation in data.links) {
					var link = new KLink(this, '#' + data.links[relation].href);
					link.setLinkText(data.links[relation].title);
					link.childDiv(KLink.LINK_ELEMENT).rel = data.links[relation].rel;
					hasLink = true;
					this.appendChild(link);
				}
				if (!hasLink) {
					var noLink = new KText(this);
					noLink.setText('<span style="font-style: italic; color: #444444;">No sublinks in resource hierarchy.</span>');
					this.appendChild(noLink);
				}
				this.draw();
			};
			relationLinks.onNoContent = function(data) {
				var noLink = new KText(this);
				noLink.setText('<span style="font-style: italic; color: #444444;">No sublinks in resource hierarchy.</span>');
				this.appendChild(noLink);
				this.draw();
			};
		}
		this.appendChild(relationLinks);
		this.clearBoth();
		
		var go = new KButton(this, 'GO!', 'go', KRestClientForm.sendRest);
		this.appendChild(go);
		
		this.clearBoth();
		
		this.requestPanel = new KPanel(this);
		{
			this.requestPanel.hash = '/request-panel';
			this.requestPanel.addDiv(KWidget.TITLE_DIV);
			this.requestPanel.setTitle('HTTP Request');
			this.requestPanel.setStyle({
				backgroundColor : '#EEF8F8',
				minHeight : '150px',
				padding : '10px',
				marginTop : '10px',
				marginRight : '10px',
				borderRadius : '5px 5px 5px 5px',
				border : '1px solid #000000',
				fontSize : '10px'
			});
		}
		this.appendChild(this.requestPanel);
		
		this.responsePanel = new KPanel(this);
		{
			this.responsePanel.hash = '/response-panel';
			this.responsePanel.addDiv(KWidget.TITLE_DIV);
			this.responsePanel.setTitle('HTTP Response');
			this.responsePanel.setStyle({
				backgroundColor : '#EEEEEE',
				minHeight : '300px',
				padding : '10px',
				marginTop : '10px',
				marginRight : '10px',
				borderRadius : '5px 5px 5px 5px',
				border : '1px solid #000000',
				fontSize : '10px'
			});
		}
		this.appendChild(this.responsePanel);
		
		KShellPart.prototype.draw.call(this);
	};
	
	KRestClientForm.prototype.sync = function(widget, service, params, callback, user, password) {
		this.writeRequest('SYNC', service, params, callback, user, password);
		params.format = 'application/json+formated';
		this.kinky.perform(widget, 'sync', service, params, callback, user, password);
	};
	
	KRestClientForm.prototype.apply = function(widget, service, params, callback, user, password) {
		this.writeRequest('APPLY', service, params, callback, user, password);
		params.format = 'application/json+formated';
		this.kinky.perform(widget, 'apply', service, params, callback, user, password);
	};
	
	KRestClientForm.prototype.get = function(widget, service, params, callback, user, password) {
		this.writeRequest('GET', service, params, callback, user, password);
		params.format = 'application/json+formated';
		this.kinky.get(widget, service, params, callback, user, password);
	};
	
	KRestClientForm.prototype.save = function(widget, service, params, callback, user, password) {
		this.writeRequest('POST', service, params, callback, user, password);
		params.format = 'application/json+formated';
		this.kinky.save(widget, service, params, callback, user, password);
	};
	
	KRestClientForm.prototype.remove = function(widget, service, params, callback, user, password) {
		this.writeRequest('DELETE', service, params, callback, user, password);
		params.format = 'application/json+formated';
		this.kinky.remove(widget, service, params, callback, user, password);
	};
	
	KRestClientForm.prototype.put = function(widget, service, params, callback, user, password) {
		this.writeRequest('PUT', service, params, callback, user, password);
		params.format = 'application/json+formated';
		this.kinky.put(widget, service, params, callback, user, password);
	};
	
	KRestClientForm.prototype.post = function(widget, service, params, callback, user, password) {
		this.writeRequest('POST', service, params, callback, user, password);
		params.format = 'application/json+formated';
		this.kinky.post(widget, service, params, callback, user, password);
	};
	
	KRestClientForm.prototype.writeRequest = function(performative, service, params, callback, user, password) {
		var serviceURL = service.substring(service.lastIndexOf(':') + 1);
		var host = Kinky.SERVICES_URL.rest.substring(Kinky.SERVICES_URL.rest.indexOf('://') + 3);
		host = host.substring(0, host.indexOf('/'));
		var prefix = Kinky.SERVICES_URL.rest.replace('http:\/\/' + host, '');
		prefix = prefix.substring(0, prefix.length - 1);
		
		var content = null;
		switch (performative) {
			case 'GET': {
				var toSend = '';
				for ( var paramName in params) {
					if (toSend.length != 0) {
						toSend += '&';
					}
					else {
						toSend += '?';
					}
					toSend += paramName + '=' + params[paramName];
				}
				serviceURL += toSend;
				break;
			}
			case 'SYNC':
			case 'APPLY':
			case 'POST':
			case 'DELETE':
			case 'PUT': {
				content = params;
				break;
			}
		}
		var body = '';
		if (content) {
			var insideStr = false;
			var escaped = false;
			body = JSON.stringify(content).replace(/\{|\[|\}|\]|\,|"|\\/g, function(wholematch, match) {
				if (insideStr && (wholematch != '"')) {
					return wholematch;
				}
				switch (wholematch) {
					case '{':
					case '[': {
						escaped = false;
						var toReturn = '<br>';
						for ( var i = 0; i != KSystem.ident; i++) {
							toReturn += KSystem.identation;
						}
						toReturn += wholematch + '<br>';
						KSystem.ident++;
						for ( var i = 0; i != KSystem.ident; i++) {
							toReturn += KSystem.identation;
						}
						return toReturn;
					}
					case '}':
					case ']': {
						escaped = false;
						KSystem.ident--;
						var toReturn = '<br>';
						for ( var i = 0; i != KSystem.ident; i++) {
							toReturn += KSystem.identation;
						}
						toReturn += wholematch + '<br>';
						for ( var i = 0; i != KSystem.ident; i++) {
							toReturn += KSystem.identation;
						}
						return toReturn;
					}
					case '\\': {
						var toReturn = '';
						toReturn += '\\';
						escaped = true;
						return toReturn;
					}
					case '"': {
						var toReturn = '';
						toReturn += '"';
						if (!escaped && insideStr) {
							insideStr = false;
						}
						else {
							insideStr = true;
						}
						escaped = false;
						return toReturn;
					}
					case ',': {
						escaped = false;
						var toReturn = '';
						toReturn += ',<br>';
						for ( var i = 0; i != KSystem.ident; i++) {
							toReturn += KSystem.identation;
						}
						return toReturn;
					}
					default: {
						return wholematch;
					}
				}
			});
		}
		
		this.responsePanel.childDiv(KWidget.CONTENT_DIV).innerHTML = '';
		this.responsePanel.setStyle({
			backgroundColor : '#EEEEEE'
		});
		this.requestPanel.childDiv(KWidget.CONTENT_DIV).innerHTML = '<div style="font-size: 11px; word-wrap: break-word; vertical-align: baseline;"><span style="font-size: 15px;"><b>' + performative + '</b> <b><i>' + prefix + serviceURL + '</i></b> ' + 'HTTP/1.1</span><br><b>Host:</b> ' + host + '<br><b>Origin:</b> ' + Kinky.SITE_URL + (Kinky.getAccessToken() ? '<br><b>Authorization:</b> ' + Kinky.getAccessToken() : '') + '<br>' + body + '</div>';
		
	};
	
	KRestClientForm.prototype.writeResponse = function(data, color) {
		if (!this.responsePanel) {
			return;
		}
		var headers = data.httpHeaders;
		delete data.httpHeaders;
		var first = true;
		
		var contentType = (headers.indexOf('Content-Type') != -1 ? headers.split('Content-Type: ')[1].split(';')[0] : 'json');
		if (contentType.indexOf('json') != -1) {
			contentType = 'json';
		}
		else if (contentType.indexOf('plain') != -1) {
			contentType = 'json';
		}
		else if (contentType.indexOf('xml') != -1) {
			contentType = 'xml';
		}
		else if (contentType.indexOf('kml') != -1) {
			contentType = 'kml';
		}
		else if (contentType.indexOf('html') != -1) {
			contentType = 'html';
		}
		
		headers = '<span style="font-size: 15px;"><b>' + headers.replace(/\n|:/g, function(wholematch, match) {
			switch (wholematch) {
				case '\n': {
					if (first) {
						first = false;
						return '</b></span><br><b>';
					}
					return '<br><b>';
				}
				case ':': {
					return '</b>:';
				}
				default: {
					return wholematch;
				}
			}
		});
		var body = null;
		if (data.type) {
			this.responsePanel.setTitle('HTTP Response <a style="line-height: 20px; display: block; float: right; text-transform: none; font-size: 12px;" href="data:application/octetstream;base64,' + Base64.encode(data.text) + '" target="_blank" title="result.' + contentType + '">download contents</a>', true);
			body = data.text;
		}
		else {
			this.responsePanel.setTitle('HTTP Response <a style="line-height: 20px; display: block; float: right; text-transform: none; font-size: 12px;" href="data:application/octetstream;base64,' + Base64.encode(JSON.stringify(data)) + '" target="_blank" title="result.' + contentType + '">download contents</a>', true);
			body = JSON.stringify(data).replace(/{|\[|}|\]|\,/g, function(wholematch, match) {
				switch (wholematch) {
					case '{':
					case '[': {
						var toReturn = '<br>';
						for ( var i = 0; i != KSystem.ident; i++) {
							toReturn += KSystem.identation;
						}
						toReturn += wholematch + '<br>';
						KSystem.ident++;
						for ( var i = 0; i != KSystem.ident; i++) {
							toReturn += KSystem.identation;
						}
						return toReturn;
					}
					case '}':
					case ']': {
						KSystem.ident--;
						var toReturn = '<br>';
						for ( var i = 0; i != KSystem.ident; i++) {
							toReturn += KSystem.identation;
						}
						toReturn += wholematch + '<br>';
						for ( var i = 0; i != KSystem.ident; i++) {
							toReturn += KSystem.identation;
						}
						return toReturn;
					}
					case ',': {
						var toReturn = '';
						toReturn += ',<br>';
						for ( var i = 0; i != KSystem.ident; i++) {
							toReturn += KSystem.identation;
						}
						return toReturn;
					}
					default: {
						return wholematch;
					}
				}
			});
		}
		this.responsePanel.setStyle({
			backgroundColor : color,
			border : '1px solid #000000'
		});
		
		this.responsePanel.childDiv(KWidget.CONTENT_DIV).innerHTML = '<div style="font-size: 11px; word-wrap: break-word; vertical-align: baseline;">' + headers + '</b>' + body + '</div>';
	};
	
	// (success ? '#ECF9EC' : '#F0D7D8')
	KRestClientForm.prototype.onGet = function(data) {
		this.writeResponse(data, '#C7FFD0');
	};
	
	KRestClientForm.prototype.onCreated = function(data) {
		this.writeResponse(data, '#C7FFD0');
	};
	
	KRestClientForm.prototype.onNotFound = function(data) {
		this.writeResponse(data, '#F0D7D8');
	};
	
	KRestClientForm.prototype.onUnauthorized = function(data) {
		this.writeResponse(data, '#FFE0BF');
	};
	
	KRestClientForm.prototype.onMethodNotAllowed = function(data) {
		this.writeResponse(data, '#FFE0BF');
	};
	
	KRestClientForm.prototype.onForbidden = function(data) {
		this.writeResponse(data, '#FFE0BF');
	};
	
	KRestClientForm.prototype.onAccepted = function(data) {
		this.writeResponse(data, '#C3ECFF');
	};
	
	KRestClientForm.prototype.onBadRequest = function(data) {
		this.writeResponse(data, '#FFE0BF');
	};
	
	KRestClientForm.prototype.onError = function(data) {
		this.writeResponse(data, '#F0D7D8');
	};
	
	KRestClientForm.prototype.onNoContent = function(data) {
		this.writeResponse(data, '#C3ECFF');
	};
	
	KRestClientForm.sendRest = function(event) {
		var widget = KSystem.getEventWidget(event, 'KRestClientForm');
		var url = widget.data.href.replace(/</g, '{').replace(/>/g, '}');
		var custom = widget.childWidget('/custom-fields');
		var params = custom.getRequest(url);
		if (!!params) {
			eval('widget.' + params.performative.replace(/delete/g, 'remove') + '(widget, \'rest:\' + params.url, params.params, \'onGet\')');
		}
	};
	
	KSystem.included('KRestClientForm');
});