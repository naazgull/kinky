function KRestClient(parent) {
	if (parent != null) {
		KShell.call(this, parent);
	}
}

KSystem.include([
	'KInput',
	'KCombo',
	'KConnectionREST',
	'KShell'
], function() {
	KRestClient.prototype = new KShell();
	KRestClient.prototype.constructor = KRestClient;
	
	KRestClient.prototype.load = function() {
		var restServer = new KInput(this, 'text', 'RESTful Server Address', 'serverURL');
		{
			restServer.hash = '/server-url';
			restServer.setValue(Kinky.REST_URL);
			restServer.addEventListener('keyup', function(event) {
				var keyEvent = KSystem.getEvent(event);
				var widget = KSystem.getEventWidget(event);
				if (keyEvent.keyCode == 13) {
					var connection = KConnection.getConnection('rest');
					
					Kinky.REST_URL = widget.getValue();
					Kinky.SERVICES_URL.rest = Kinky.REST_URL;
					Kinky.DATA_URL.rest = Kinky.REST_URL;
					
					KSystem.setCookie("KINKY_CLIENT_REST_URL", {
						url : Kinky.REST_URL
					});
					
					connection.setURL(Kinky.REST_URL);
					connection.setDataURL(Kinky.REST_URL);
					Kinky.shell.refresh();
				}
			});
			restServer.setStyle({
				position : 'absolute',
				left : '140px',
				width : '600px'
			});
		}
		this.appendTitleChild(restServer);
		restServer.go();
		
		KShell.prototype.load.call(this);
	};
	
	KRestClient.prototype.onLoad = function(data) {
		for ( var element in data.links) {
			data.links[element].feClass = 'KRestClientForm';
			data.links[element].feService = 'rest:' + data.links[element].rel;
			data.links[element].urlHashText = data.links[element].href;
			
			for ( var format in data.links[element].requestTypes) {
				if (data.links[element].requestTypes[format].indexOf('application/wrml') != -1) {
					var indexOfSchema = data.links[element].requestTypes[format].indexOf('schema="');
					var indexOfQuote = data.links[element].requestTypes[format].indexOf('"', indexOfSchema + 8);
					data.links[element].schemaService = 'rest:' + data.links[element].requestTypes[format].substring(indexOfSchema + 8, indexOfQuote);
					break;
				}
			}
		}
		data.elements = data.links;
		KShell.prototype.onLoad.call(this, data);
	};
	
	KRestClient.prototype.draw = function() {
		
		if (Kinky.REST_URL) {
			var operations = new KCombo(this, '', 'opURL');
			{
				operations.hash = '/op-url';
				operations.addCSSClass('KRestClientOperationSelect');
				operations.addCSSClass('KRestClientOperationSelectPanel', KCombo.OPTIONS_DIV);
				
				operations.addOption('', '--');
				for ( var element in this.data.links) {
// if (/\/([^\/]+)\/.*/.test(this.data.links[element].urlHashText)) {
// continue;
// }
					operations.addOption(this.data.links[element].urlHashText, this.data.links[element].title, this.data.links[element].urlHashText == this.getHash());
				}
				
				KSystem.addEventListener(operations, 'change', function(event) {
					KBreadcrumb.dispatchURL({
						hash : operations.getValue()
					});
				});
				operations.setStyle({
					position : 'absolute',
					left : '187px',
					top : '32px',
					width : '600px'
				});
			}
			this.appendTitleChild(operations);
			
			this.setStyle({
				minHeight : (KSystem.getBrowserHeight() - 40) + 'px'
			});
		}
		KShell.prototype.draw.call(this);
	};
	
	KSystem.included('KRestClient');
});
