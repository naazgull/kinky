<?php

header('Content-Type: text/css');

function includeCSS($root) {
	$files = glob($root . '/*.css');
	foreach ($files as $file) {
		if (strpos($file, 'SumolSnowtripFBSite.css') === false && strpos($file, 'fbSnowtripFooter.css') === false) {
			readfile($file);
			echo "\n";
		}
	}
	$files = glob($root . '/*');
	foreach ($files as $file) {
		if (is_dir($file) && strpos($file, '.') !== 0) {
			includeCSS($file);
		}
	}
}

includeCSS(dirname(__FILE__));
